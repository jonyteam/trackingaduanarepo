﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aduana.Dto
{
    public class UsuarioCargaTrabajo
    {
        public UsuarioCargaTrabajo()
        {
        }

        public string Rol { set; get; }
        public string RolReal { set; get; }
        public string Ubicacion { set; get; }
        public string IdUsuario { set; get; }
        public string Usuario { set; get; }
        public string CantDiaMasCargado { set; get; }
        public string DiaMasCargado { set; get; }
        public string Desempeno { set; get; }
        public decimal Lunes { set; get; }
        public decimal Martes { set; get; }
        public decimal Miercoles { set; get; }
        public decimal Jueves { set; get; }
        public decimal Viernes { set; get; }
        public decimal Sabado { set; get; }
        public decimal Domingo { set; get; }
        public int Total { set; get; }
        public int DiaMaximoCarga { set; get; }
        public string Eliminado { set; get; }
    }
}
