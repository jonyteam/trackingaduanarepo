﻿using System;
using System.Collections.Generic;
using System.Linq;
using EntityFramework.Extensions;
using System.Data.Entity;
using Aduanas.Data;
using Aduana.Dto;


namespace AduanasNuevo.Repository
{
    public class ReporteCargaDeTrabajoRepository
    {

        public List<UsuarioCargaTrabajo> CargarGridPrincipal( DateTime fechaInicio, DateTime fechaFinal)
        {
            float lunes = 0;
            float martes = 0;
            float miercoles = 0;
            float jueves = 0;
            float viernes = 0;
            float sabado = 0;
            float domingo = 0;
            float cantLunes = 0;
            float cantMartes = 0;
            float cantMiercoles = 0;
            float cantJueves = 0;
            float cantViernes = 0;
            float cantSabado = 0;
            float cantDomingo = 0;
            float total = 0;
            string nombreAduana = string.Empty;
            string[] datosPalabras = new string[8];
            float[] datosCalculados = new float[7];
            List<UsuarioCargaTrabajo> listaUsuarios = new List<UsuarioCargaTrabajo>();
            Int32 CantDiaMasCargado = 0;
            DateTime DiaMasCargado = DateTime.Now;


            //Contar los dias que tiene el periodo de tiempo (Por Nombre)
            for (DateTime i = fechaInicio; i < fechaFinal; i = i.AddDays(1))
            {
                string Dia = i.DayOfWeek.ToString();

                if (Dia == "Monday")
                {
                    cantLunes += 1;
                }
                else if (Dia == "Tuesday")
                {
                    cantMartes += 1;
                }
                else if (Dia == "Wednesday")
                {
                    cantMiercoles += 1;
                }
                else if (Dia == "Thursday")
                {
                    cantJueves += 1;
                }
                else if (Dia == "Friday")
                {
                    cantViernes += 1;
                }
                else if (Dia == "Saturday")
                {
                    cantSabado += 1;
                }
                else if (Dia == "Sunday")
                {
                    cantDomingo += 1;
                }
            }

            float[] cantDias = { cantLunes, cantMartes, cantMiercoles, cantJueves, cantViernes, cantSabado, cantDomingo };

            using (var context = new AduanasNuevaEntities()) 
            {
                var usuariosTramitadores = context.UsuariosRoles.AsQueryable().Join(context.Usuarios,
                ur => ur.IdUsuario,
                us => us.IdUsuario,
                (ur, us) => new
                {
                    usuarioRoles = ur,
                    usuario = us
                }
                ).Where(u => u.usuarioRoles.Eliminado == "0" && u.usuario.CodPais == "H")
                .Join(context.Roles,
                ur => ur.usuarioRoles.IdRol,
                r => r.IdRol,
                (ur, r) => new
                {
                    usuarioRol = ur,
                    rol = r
                }
                ).Where(r => r.rol.Eliminado == "0"
                        && (r.rol.IdRol == 3
                        || r.rol.IdRol == 24
                        || r.rol.IdRol == 22
                        || r.rol.IdRol == 23))
                .Join(context.Aduanas,
                us => us.usuarioRol.usuario.CodigoAduana,
                a => a.CodigoAduana,
                (us, a) => new
                {
                    usuario = us,
                    aduana = a
                }
                ).Where(w => w.aduana.CodPais == "H")
                .Select(u => new
                {
                    IdUsuario = u.usuario.usuarioRol.usuario.IdUsuario,
                    Rol = u.usuario.rol.IdRol,
                    Cargo = u.usuario.rol.Descripcion,
                    Nombre = u.usuario.usuarioRol.usuario.Nombre + " " + u.usuario.usuarioRol.usuario.Apellido,
                    Aduana = u.aduana.NombreAduana,
                    CodAduana = u.aduana.CodigoAduana,
                    Eliminado = u.usuario.usuarioRol.usuario.Eliminado.ToString(),
                }).OrderBy(o => o.Cargo.ToString()).ThenBy(o => o.Aduana.ToString()).AsNoTracking().Future();


               var usuariosOficialesDeCuenta = context.UsuariosRoles.Join(context.Usuarios,
               ur => ur.IdUsuario,
               us => us.IdUsuario,
               (ur, us) => new
               {
                   usuarioRoles = ur,
                   usuario = us
               }
               ).Where(u => u.usuarioRoles.Eliminado == "0" && u.usuario.CodPais == "H")
               .Join(context.Roles,
               ur => ur.usuarioRoles.IdRol,
               r => r.IdRol,
               (ur, r) => new
               {
                   usuarioRol = ur,
                   rol = r
               }
               ).Where(r => r.rol.Eliminado == "0"
                        && r.rol.IdRol != 3
                        && r.rol.IdRol != 24
                        && r.rol.IdRol != 22
                        && r.rol.IdRol != 23)
               .Select(u => new
               {
                   IdUsuario = u.usuarioRol.usuario.IdUsuario,
                   Rol = u.rol.IdRol,
                   Cargo = u.rol.Descripcion,
                   Nombre = u.usuarioRol.usuario.Nombre + " " + u.usuarioRol.usuario.Apellido,
                   Aduana = "",
                   CodAduana = "",
                   Eliminado = u.usuarioRol.usuario.Eliminado.ToString()
               }).OrderBy(o => o.Cargo.ToString()).ThenBy(o => o.Aduana.ToString()).AsNoTracking().Future();

                var aduanas = context.Aduanas.Where(w => w.CodEstado == "0" && w.CodPais == "H").Future();

                var instruccionesTFC = context.TiemposFlujoCarga
                   .Where(tfc => tfc.IdEstado == "9" && tfc.Eliminado == false
                          && (tfc.Fecha >= fechaInicio
                          && tfc.Fecha <= fechaFinal)).AsNoTracking().Future();

                var instrucciones = context.Instrucciones.Where(w => w.CodEstado != "100" && w.IdEstadoFlujo == "99"
                                                                && (w.FechaFinalFlujo >= fechaInicio
                                                                && w.FechaFinalFlujo <= fechaFinal)
                                                                && w.CodRegimen != "9999"
                                                                && w.CodRegimen != "CB").AsNoTracking().Future();

                
                var instruccionesUsuarios1 = instruccionesTFC.Join(instrucciones,
                                    insTFC => insTFC.IdInstruccion,
                                    ins => ins.IdInstruccion,
                                    (insTFC, ins) => new
                                    {
                                        insTFC = insTFC,
                                        ins = ins
                                    }
                                    ).ToList();

                var aduanasTemp = aduanas.ToList();

                //Instrucciones solo en aduanas Hondureñas para sacar oficiales de cuenta
                var instruccionesFinalizadas = instrucciones.Join(aduanasTemp,
                    i => i.CodigoAduana,
                    a => a.CodigoAduana,
                    (i, a) => new
                    {
                        ins = i,
                        aduana = a
                    }
                    ).ToList();


                var instruccionesUsuariosOC1SinOC = instruccionesFinalizadas.Where(w => w.ins.IdOficialCuenta == null).ToList();

                var instruccionesUsuariosOC = instruccionesFinalizadas.Where(w => w.ins.IdOficialCuenta != null).ToList();

                var usuariosTramitadoresTemp = usuariosTramitadores.ToList();
                for (int i = 0; i < usuariosTramitadoresTemp.Count; i++)
                {
                    var usuario = usuariosTramitadoresTemp[i].IdUsuario.ToString();
                    var Cargo = usuariosTramitadoresTemp[i].Cargo.ToString();
                    var Nombre = usuariosTramitadoresTemp[i].Nombre.ToString();
                    var CodAduana = usuariosTramitadoresTemp[i].CodAduana.ToString();
                    var Aduana = usuariosTramitadoresTemp[i].Aduana.ToString();
                    lunes = 0;
                    martes = 0;
                    miercoles = 0;
                    jueves = 0;
                    viernes = 0;
                    sabado = 0;
                    domingo = 0;
                    total = 0;

                     CantDiaMasCargado = 0;
                     DiaMasCargado = DateTime.Now;

                    var instruccionesPorUsuario1 = instruccionesUsuarios1.Where(w => w.insTFC.IdUsuario.ToString() == usuario).ToList();
                    var instruccionesPorUsuario2 = instruccionesFinalizadas.Where(w => w.ins.Tramitador.ToString() == usuario).ToList();

                    //Obtener el dia fecha donde realizo mas tramites
                    var InstruccionesFechas1 = instruccionesPorUsuario1.GroupBy(w => w.ins.FechaFinalFlujo.Value.Date).Distinct().ToList();
                    var InstruccionesFechas2 = instruccionesPorUsuario2.GroupBy(w => w.ins.FechaFinalFlujo.Value.Date).Distinct().ToList();

                    List<String> fechas1 = new List<String>();
                    List<String> fechas2 = new List<String>();
                    for (int h = 0; h < InstruccionesFechas1.Count(); h++)
                    {
                        fechas1.Add(InstruccionesFechas1[h].Key.Date.ToString());
                    }
                    for (int h = 0; h < InstruccionesFechas2.Count(); h++)
                    {
                        fechas2.Add(InstruccionesFechas2[h].Key.Date.ToString());
                    }

                    for (int u = 0; u < fechas1.Count; u++)
                    {
                        int ins = 0;
                        var temp1 = instruccionesPorUsuario1.Where(x => x.ins.FechaFinalFlujo.Value.Date == Convert.ToDateTime(fechas1[u]).Date).ToList();

                        for (int g = 0; g < fechas2.Count; g++)
                        {
                            var temp2 = instruccionesPorUsuario2.Where(x => x.ins.FechaFinalFlujo.Value.Date == Convert.ToDateTime(fechas2[g]).Date).ToList();
                            ins = temp2.Count;

                            if (Convert.ToDateTime(fechas1[u]).Date == Convert.ToDateTime(fechas2[g]).Date)
                            {
                                if (CantDiaMasCargado < (ins + temp1.Count))
                                {
                                    CantDiaMasCargado = (ins + temp1.Count);
                                    DiaMasCargado = Convert.ToDateTime(fechas1[u]).Date;
                                }
                            }
                            else
                            {
                                if (CantDiaMasCargado < (temp1.Count))
                                {
                                    CantDiaMasCargado = temp1.Count;
                                    DiaMasCargado = Convert.ToDateTime(fechas1[u]).Date;
                                }
                            }
                        }

                        if (fechas2.Count == 0)
                        {
                            if (CantDiaMasCargado < temp1.Count)
                            {
                                CantDiaMasCargado = temp1.Count;
                                DiaMasCargado = Convert.ToDateTime(fechas1[u]).Date;
                            }
                        }
                    }

                    if (fechas1.Count == 0)
                    {
                        for (int u = 0; u < fechas2.Count; u++)
                        {
                            var temp = instruccionesPorUsuario2.Where(x => x.ins.FechaFinalFlujo.Value.Date == Convert.ToDateTime(fechas2[u]).Date).ToList();

                            if (CantDiaMasCargado < temp.Count)
                            {
                                CantDiaMasCargado = temp.Count;
                                DiaMasCargado = Convert.ToDateTime(fechas2[u]).Date;
                            }
                        }
                    }

                    //Sacar las instrucciones por dia
                    for (int j = 0; j < instruccionesPorUsuario1.Count(); j++)
                    {
                        string Dia = instruccionesPorUsuario1[j].insTFC.Fecha.DayOfWeek.ToString();

                        if (Dia == "Monday")
                        {
                            lunes += 1;
                        }
                        else if (Dia == "Tuesday")
                        {
                            martes += 1;
                        }
                        else if (Dia == "Wednesday")
                        {
                            miercoles += 1;
                        }
                        else if (Dia == "Thursday")
                        {
                            jueves += 1;
                        }
                        else if (Dia == "Friday")
                        {
                            viernes += 1;
                        }
                        else if (Dia == "Saturday")
                        {
                            sabado += 1;
                        }
                        else if (Dia == "Sunday")
                        {
                            domingo += 1;
                        }
                    }

                    for (int j = 0; j < instruccionesPorUsuario2.Count; j++)
                    {
                        string Dia = instruccionesPorUsuario2[j].ins.FechaFinalFlujo.Value.DayOfWeek.ToString();

                        if (Dia == "Monday")
                        {
                            lunes += 1;
                        }
                        else if (Dia == "Tuesday")
                        {
                            martes += 1;
                        }
                        else if (Dia == "Wednesday")
                        {
                            miercoles += 1;
                        }
                        else if (Dia == "Thursday")
                        {
                            jueves += 1;
                        }
                        else if (Dia == "Friday")
                        {
                            viernes += 1;
                        }
                        else if (Dia == "Saturday")
                        {
                            sabado += 1;
                        }
                        else if (Dia == "Sunday")
                        {
                            domingo += 1;
                        }
                    }


                    total = lunes + martes + miercoles + jueves + viernes + sabado + domingo;

                    if (Cargo == "Tramitador Documentos" || Cargo == "Tramitador OPC" || Cargo == "Tramitador DEI")
                    {
                        Cargo = "Tramitadores";
                    }

                    if (total != 0)
                    {
                        datosCalculados[0] = lunes;
                        datosCalculados[1] = martes;
                        datosCalculados[2] = miercoles;
                        datosCalculados[3] = jueves;
                        datosCalculados[4] = viernes;
                        datosCalculados[5] = sabado;
                        datosCalculados[6] = domingo;

                        datosPalabras[0] = Cargo;
                        datosPalabras[1] = usuariosTramitadoresTemp[i].Rol.ToString();
                        datosPalabras[2] = Aduana;
                        datosPalabras[3] = Nombre;
                        datosPalabras[4] = usuariosTramitadoresTemp[i].Eliminado.ToString();
                        datosPalabras[5] = CantDiaMasCargado.ToString();
                        datosPalabras[6] = DiaMasCargado.ToShortDateString() + " (" + NombrarDia(DiaMasCargado.DayOfWeek.ToString()) + ")";
                        datosPalabras[7] = usuariosTramitadoresTemp[i].IdUsuario.ToString();

                        listaUsuarios.Add(CrearUsuario(datosPalabras, cantDias, datosCalculados));
                    }
                }

                var usuariosOficialesDeCuentaTemp = usuariosOficialesDeCuenta.ToList();

                //Sacando los tramites de los Oficiales de Cuenta
                for (int j = 0; j < usuariosOficialesDeCuentaTemp.Count; j++)
                {
                    var NombreUsuario = usuariosOficialesDeCuentaTemp[j].Nombre.ToString();
                    var Usuario = usuariosOficialesDeCuentaTemp[j].IdUsuario.ToString();
                    var Cargo = "Oficial de Cuenta";
                    var Nombre = usuariosOficialesDeCuentaTemp[j].Nombre.ToString();
                    var AduanaNombre = "Todas Las Aduanas";
                    lunes = 0;
                    martes = 0;
                    miercoles = 0;
                    jueves = 0;
                    viernes = 0;
                    sabado = 0;
                    domingo = 0;
                    total = 0;
                    CantDiaMasCargado = 0;
                    DiaMasCargado = DateTime.Now;

                    //Sacando los tramites de cada Oficial de cuenta

                    var instruccionesUsuarioOCActual = instruccionesUsuariosOC.Where(w => w.ins.IdOficialCuenta.ToString() == Usuario).ToList();

                    //Obtener el dia fecha donde hizo mas tramites
                    var InstruccionesFechas = instruccionesUsuarioOCActual.GroupBy(w => w.ins.FechaFinalFlujo.Value.Date).Distinct().ToList();

                    List<String> fechas = new List<String>();
                    for (int h = 0; h < InstruccionesFechas.Count(); h++)
                    {
                        fechas.Add(InstruccionesFechas[h].Key.Date.ToString());
                    }

                    for (int u = 0; u < fechas.Count; u++)
                    {
                        var temp = instruccionesUsuarioOCActual.Where(x => x.ins.FechaFinalFlujo.Value.Date == Convert.ToDateTime(fechas[u]).Date).ToList();
                        if (CantDiaMasCargado < temp.Count)
                        {
                            CantDiaMasCargado = temp.Count;
                            DiaMasCargado = Convert.ToDateTime(fechas[u]).Date;
                        }
                    }

                    for (int k = 0; k < instruccionesUsuarioOCActual.Count; k++)
                    {
                        string Dia = instruccionesUsuarioOCActual[k].ins.FechaFinalFlujo.Value.DayOfWeek.ToString();

                        if (Dia == "Monday")
                        {
                            lunes += 1;
                        }
                        else if (Dia == "Tuesday")
                        {
                            martes += 1;
                        }
                        else if (Dia == "Wednesday")
                        {
                            miercoles += 1;
                        }
                        else if (Dia == "Thursday")
                        {
                            jueves += 1;
                        }
                        else if (Dia == "Friday")
                        {
                            viernes += 1;
                        }
                        else if (Dia == "Saturday")
                        {
                            sabado += 1;
                        }
                        else if (Dia == "Sunday")
                        {
                            domingo += 1;
                        }
                    }

                    total = lunes + martes + miercoles + jueves + viernes + sabado + domingo;

                    datosCalculados[0] = lunes;
                    datosCalculados[1] = martes;
                    datosCalculados[2] = miercoles;
                    datosCalculados[3] = jueves;
                    datosCalculados[4] = viernes;
                    datosCalculados[5] = sabado;
                    datosCalculados[6] = domingo;

                    datosPalabras[0] = Cargo;
                    datosPalabras[1] = usuariosOficialesDeCuentaTemp[j].Rol.ToString();
                    datosPalabras[2] = AduanaNombre;
                    datosPalabras[3] = NombreUsuario;
                    datosPalabras[4] = usuariosOficialesDeCuentaTemp[j].Eliminado.ToString();
                    datosPalabras[5] = CantDiaMasCargado.ToString();
                    datosPalabras[6] = DiaMasCargado.ToShortDateString() + " (" + NombrarDia(DiaMasCargado.DayOfWeek.ToString()) + ")";
                    datosPalabras[7] = usuariosOficialesDeCuentaTemp[j].IdUsuario.ToString();

                    if (total != 0)
                    {
                        listaUsuarios.Add(CrearUsuario(datosPalabras, cantDias, datosCalculados));
                    }
                }


                //Llenar Tramites que no tienen Oficial de Cuenta
                CantDiaMasCargado = 0;
                DiaMasCargado = DateTime.Now;

                var InstruccionesFechasSinOC = instruccionesUsuariosOC1SinOC.GroupBy(w => w.ins.FechaFinalFlujo.Value.Date).Distinct().ToList();
                List<String> fechasSinOc = new List<String>();
                
                for (int h = 0; h < InstruccionesFechasSinOC.Count(); h++)
                {
                    fechasSinOc.Add(InstruccionesFechasSinOC[h].Key.Date.ToString());
                }

                for (int u = 0; u < fechasSinOc.Count; u++)
                {
                    var temp = instruccionesUsuariosOC1SinOC.Where(x => x.ins.FechaFinalFlujo.Value.Date == Convert.ToDateTime(fechasSinOc[u]).Date).ToList();
                    if (CantDiaMasCargado < temp.Count)
                    {
                        CantDiaMasCargado = temp.Count;
                        DiaMasCargado = Convert.ToDateTime(fechasSinOc[u]).Date;
                    }
                }

                for (int k = 0; k < instruccionesUsuariosOC1SinOC.Count; k++)
                {
                    string Dia = instruccionesUsuariosOC1SinOC[k].ins.FechaFinalFlujo.Value.DayOfWeek.ToString();

                    if (Dia == "Monday")
                    {
                        lunes += 1;
                    }
                    else if (Dia == "Tuesday")
                    {
                        martes += 1;
                    }
                    else if (Dia == "Wednesday")
                    {
                        miercoles += 1;
                    }
                    else if (Dia == "Thursday")
                    {
                        jueves += 1;
                    }
                    else if (Dia == "Friday")
                    {
                        viernes += 1;
                    }
                    else if (Dia == "Saturday")
                    {
                        sabado += 1;
                    }
                    else if (Dia == "Sunday")
                    {
                        domingo += 1;
                    }
                }

                total = lunes + martes + miercoles + jueves + viernes + sabado + domingo;

                datosCalculados[0] = lunes;
                datosCalculados[1] = martes;
                datosCalculados[2] = miercoles;
                datosCalculados[3] = jueves;
                datosCalculados[4] = viernes;
                datosCalculados[5] = sabado;
                datosCalculados[6] = domingo;

                datosPalabras[0] = "Oficial de Cuenta";
                datosPalabras[1] = "0";
                datosPalabras[2] = "Todas Las Aduanas";
                datosPalabras[3] = "S/O";
                datosPalabras[4] = "0";
                datosPalabras[5] = CantDiaMasCargado.ToString();
                datosPalabras[6] = DiaMasCargado.ToShortDateString() + " (" + NombrarDia(DiaMasCargado.DayOfWeek.ToString()) + ")";
                datosPalabras[7] = "0";

                if (total != 0)
                {
                    listaUsuarios.Add(CrearUsuario(datosPalabras, cantDias, datosCalculados));
                }
              

                return listaUsuarios;

             }       

        }

        private string NombrarDia(string Dia)
        {
            string Dia2 = "";
            if (Dia == "1" || Dia == "Monday")
            {
                Dia2 = "Lunes";
            }
            else if (Dia == "2" || Dia == "Tuesday")
            {
                Dia2 = "Martes";
            }
            else if (Dia == "3" || Dia == "Wednesday")
            {
                Dia2 = "Miercoles";
            }
            else if (Dia == "4" || Dia == "Thursday")
            {
                Dia2 = "Jueves";
            }
            else if (Dia == "5" || Dia == "Friday")
            {
                Dia2 = "Viernes";
            }
            else if (Dia == "6" || Dia == "Saturday")
            {
                Dia2 = "Sabado";
            }
            else if (Dia == "0" || Dia == "Sunday")
            {
                Dia2 = "Domingo";
            }

            return Dia2;
        }

        private UsuarioCargaTrabajo CrearUsuario(string[] datosPalabras, float[] cantDias, float[] datosCalculados)
        {
            float total = 0;
            for (int i = 0; i < datosCalculados.Length; i++)
            {
                total += datosCalculados[i];
            }

            UsuarioCargaTrabajo usuarioActual = new UsuarioCargaTrabajo()
            {
                Rol = datosPalabras[0].ToString(),
                RolReal = datosPalabras[1].ToString(),
                Ubicacion = datosPalabras[2].ToString(),
                Usuario = datosPalabras[3].ToString(),
                Eliminado = datosPalabras[4].ToString(),
                CantDiaMasCargado = datosPalabras[5].ToString(),
                DiaMasCargado = datosPalabras[6].ToString(),
                IdUsuario = datosPalabras[7].ToString(),
                Lunes = Convert.ToDecimal((datosCalculados[0] / cantDias[0])),
                Martes = Convert.ToDecimal((datosCalculados[1] / cantDias[1])),
                Miercoles = Convert.ToDecimal((datosCalculados[2] / cantDias[2])),
                Jueves = Convert.ToDecimal((datosCalculados[3] / cantDias[3])),
                Viernes = Convert.ToDecimal((datosCalculados[4] / cantDias[4])),
                Sabado = Convert.ToDecimal((datosCalculados[5] / cantDias[5])),
                Domingo = Convert.ToDecimal((datosCalculados[6] / cantDias[6])),
                Total = Convert.ToInt32(total),
            };

            return usuarioActual;
        }

    }
}
