﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="IngresoTarifas.aspx.cs"
    Inherits="MantenimientoUsuarios" MasterPageFile="~/Site.master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
    .style1
    {
        width: 20%;
    }
    .style2
    {
        width: 19%;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" Width="99%">
        <telerik:RadPageView ID="pvUsuarios" runat="server" Width="100%">
            <telerik:RadGrid ID="rgUsuarios" runat="server" AllowSorting="True"
                AutoGenerateColumns="False" GridLines="Horizontal" Width="100%" Height="550px"
                OnNeedDataSource="rgUsuarios_NeedDataSource" OnUpdateCommand="rgUsuarios_UpdateCommand"
                OnDeleteCommand="rgUsuarios_DeleteCommand" OnInsertCommand="rgUsuarios_InsertCommand"
                AllowPaging="True" ShowFooter="True" ShowStatusBar="True" PageSize="20" OnItemDataBound="rgUsuarios_ItemDataBound"
                OnItemCreated="rgUsuarios_ItemCreated" 
                OnItemCommand="rgUsuarios_ItemCommand" CellSpacing="0">
                <HeaderContextMenu EnableTheming="True">
                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                </HeaderContextMenu>
                <PagerStyle NextPagesToolTip="Páginas Siguientes" NextPageToolTip="Página Siguiente"
                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                <MasterTableView CommandItemDisplay="<%# Ingresar ? GridCommandItemDisplay.Top : GridCommandItemDisplay.None %>"
                    NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay Usuarios definidos.">
                    <CommandItemSettings AddNewRecordText="Agregar Tasas de Cambio" RefreshText="Volver a Cargar Datos" />
                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridEditCommandColumn AutoPostBackOnFilter="True" CancelText="Cancelar"
                            EditText="Editar" InsertText="Insertar" UpdateText="Grabar" 
                            ButtonType="ImageButton" Visible="False">
                            <HeaderStyle Width="75px" />
                            <ItemStyle Width="5%" />
                        </telerik:GridEditCommandColumn>
                        <telerik:GridButtonColumn ConfirmText="¿Esta seguro de que desea eliminar este usuario?"
                            ConfirmDialogType="RadWindow" ConfirmTitle="Eliminar Usuario" UniqueName="DeleteColumn"
                            ButtonType="ImageButton" CommandName="Delete">
                            <HeaderStyle Width="75px" />
                            <ItemStyle Width="5%" />
                        </telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="FechaInicio" HeaderText="Fecha Inicio" 
                            UniqueName="FechaInicio" FilterControlAltText="Filter FechaInicio column" DataFormatString="{0:dd-MM-yyyy}">
                            <HeaderStyle Width="75px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FechaFinal" HeaderText="Fecha Final" 
                            UniqueName="FechaFinal" FilterControlAltText="Filter FechaFinal column" DataFormatString="{0:dd-MM-yyyy}">
                            <HeaderStyle Width="75px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="GASOLINASUPER" HeaderText="Gasolina Super" 
                            UniqueName="GASOLINASUPER" FilterControlAltText="Filter GASOLINASUPER column">
                            <HeaderStyle Width="75px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="GASOLINAREGULAR" 
                            HeaderText="Gasolina Reular" UniqueName="GASOLINAREGULAR" 
                            FilterControlAltText="Filter GASOLINAREGULAR column">
                            <HeaderStyle Width="75px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="DIESEL" HeaderText="Diesel" UniqueName="DIESEL"
                            Visible="false" FilterControlAltText="Filter DIESEL column">
                            <HeaderStyle Width="75px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="AVJET" 
                            FilterControlAltText="Filter AVJET column" HeaderText="Av Jet" 
                            UniqueName="AVJET">
                            <HeaderStyle Width="75px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="BUNKER" 
                            FilterControlAltText="Filter BUNKER column" HeaderText="Bunker" 
                            UniqueName="BUNKER">
                            <HeaderStyle Width="75px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="KEROSENO" 
                            FilterControlAltText="Filter KEROSENO column" HeaderText="Keroseno" 
                            UniqueName="KEROSENO">
                            <HeaderStyle Width="75px" />
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings EditFormType="Template" CaptionDataField="Categoria" CaptionFormatString="">
                        <EditColumn FilterControlAltText="Filter EditCommandColumn1 column" 
                            UniqueName="EditCommandColumn1">
                        </EditColumn>
                        <FormTemplate>
                            <asp:Label ID="Label1" runat="server" Font-Bold="true" Font-Size="Medium" 
                                Text='<%# ((Container is GridEditFormInsertItem) ? "Agregar Nueva Tasa de Cambio" : "Editar Usuario Existente") %>' />
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 10%">
                                    </td>
                                    <td style="width: 90%" colspan="4">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="lbGasolinaSuper" runat="server" Font-Bold="True" 
                                            Text="Gasolina Super" />
                                    </td>
                                    <td style="width: 90%" colspan="4">
                                        <telerik:RadNumericTextBox ID="edGasolinaSuper" Runat="server" 
                                            EmptyMessage="GASOLINASUPER" Width="50%" 
                                            Text='<%# Bind("GASOLINASUPER") %>' Culture="es-HN" DbValueFactor="1" 
                                            LabelWidth="40%" Type="Currency">
                                            <NumberFormat DecimalDigits="4" ZeroPattern="L. n" />
                                        </telerik:RadNumericTextBox>
                                        <asp:RequiredFieldValidator ID="rfGasolinaSuper" runat="server" 
                                            ControlToValidate="edGasolinaSuper" Display="Dynamic" EnableClientScript="true" 
                                            ErrorMessage="Favor Ingresar una Tasa de Cambio" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="lbGasolinaRegular" runat="server" Font-Bold="True" 
                                            Text="Gasolina Regular" />
                                    </td>
                                    <td style="width: 90%" colspan="4">
                                        <telerik:RadNumericTextBox ID="edGasolinaRegular" Runat="server" 
                                            EmptyMessage="GASOLINAREGULAR" Width="50%" 
                                            Text='<%# Bind("GASOLINAREGULAR") %>' Culture="es-HN" DbValueFactor="1" 
                                            LabelWidth="40%" Type="Currency">
                                            <NumberFormat DecimalDigits="4" ZeroPattern="L. n" />
                                        </telerik:RadNumericTextBox>
                                        <asp:RequiredFieldValidator ID="rfedGasolinaRegular" runat="server" 
                                            ControlToValidate="edGasolinaRegular" Display="Dynamic" 
                                            EnableClientScript="true" ErrorMessage="Favor Ingresar una Tasa de Cambio" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="lbDiesel" runat="server" Font-Bold="True" Text="Diesel" />
                                    </td>
                                    <td style="width: 90%" colspan="4">
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="El Apellido no puede estar vacío" ControlToValidate="edApellido" />--%>
                                        <telerik:RadNumericTextBox ID="edDisel" Runat="server" EmptyMessage="DIESEL" 
                                            Width="50%" Text='<%# Bind("DIESEL") %>' Culture="es-HN" DbValueFactor="1" 
                                            LabelWidth="40%" Type="Currency">
                                            <NumberFormat DecimalDigits="4" ZeroPattern="L. n" />
                                        </telerik:RadNumericTextBox>
                                        <asp:RequiredFieldValidator ID="rfedDisel" runat="server" 
                                            ControlToValidate="edDisel" Display="Dynamic" EnableClientScript="true" 
                                            ErrorMessage="Favor Ingresar una Tasa de Cambio" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="lbAvJet" runat="server" Font-Bold="True" Text="Av Jet" />
                                    </td>
                                    <td style="width: 90%" colspan="4">
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="La Identidad no puede estar vacío" ControlToValidate="edIdentidad" />--%>
                                        <telerik:RadNumericTextBox ID="edAVJET" Runat="server" EmptyMessage="AV JET" 
                                            Width="50%" Text='<%# Bind("AVJET") %>' Culture="es-HN" DbValueFactor="1" 
                                            LabelWidth="40%" Type="Currency">
                                            <NumberFormat DecimalDigits="4" ZeroPattern="L. n" />
                                        </telerik:RadNumericTextBox>
                                        <asp:RequiredFieldValidator ID="rfedAVJET" runat="server" 
                                            ControlToValidate="edAVJET" Display="Dynamic" EnableClientScript="true" 
                                            ErrorMessage="Favor Ingresar una Tasa de Cambio" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="lbBunker" runat="server" Font-Bold="True" Text="Bunker" />
                                    </td>
                                    <td style="width: 90%" colspan="4">
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="El eMail no puede estar vacío" ControlToValidate="edEmail" />--%>
                                        <telerik:RadNumericTextBox ID="edBunker" Runat="server" EmptyMessage="BUNKER" 
                                            Width="50%" Text='<%# Bind("BUNKER") %>' Culture="es-HN" DbValueFactor="1" 
                                            LabelWidth="40%" Type="Currency">
                                            <NumberFormat DecimalDigits="4" ZeroPattern="L. n" />
                                        </telerik:RadNumericTextBox>
                                        <asp:RequiredFieldValidator ID="rfedBunker" runat="server" 
                                            ControlToValidate="edBunker" Display="Dynamic" EnableClientScript="true" 
                                            ErrorMessage="Favor Ingresar una Tasa de Cambio" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="lbKeroseno" runat="server" Font-Bold="True" Text="Keroseno" />
                                    </td>
                                    <td style="width: 90%" colspan="4">
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="El Celular no puede estar vacío" ControlToValidate="edCelular" />--%>
                                        <telerik:RadNumericTextBox ID="edKerosen" runat="server" EmptyMessage="KEROSENO" 
                                        Width="50%" Text='<%# Bind("KEROSENO") %>' Culture="es-HN" DbValueFactor="1" 
                                            LabelWidth="40%" Type="Currency">
                                            <NumberFormat DecimalDigits="4" ZeroPattern="L. n" />
                                        </telerik:RadNumericTextBox>
                                        <asp:RequiredFieldValidator ID="rfedKerosen" runat="server" 
                                            ControlToValidate="edKerosen" Display="Dynamic" EnableClientScript="true" 
                                            ErrorMessage="Favor Ingresar una Tasa de Cambio" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        &nbsp;</td>
                                    <td class="style1">
                                        <asp:Label ID="lbFechaInicio" runat="server" Font-Bold="True" 
                                            Text="Fecha Inicio" />
                                        :
                                        <telerik:RadDatePicker ID="edFechaInicio" Runat="server" MinDate="2014-01-01"  >
                                        
                                        </telerik:RadDatePicker>
                                        <asp:RequiredFieldValidator ID="rfedFechaInicio" runat="server" 
                                            ControlToValidate="edFechaInicio" Display="Dynamic" EnableClientScript="true" 
                                            ErrorMessage="Favor Ingresar una de Inicio" />
                                    </td>
                                    <td class="style1">
                                    </td>
                                    <td class="style1">
                                        <asp:Label ID="lblFechaFinal" runat="server" Font-Bold="True" 
                                            Text="Fecha Final" />
                                        :
                                        <telerik:RadDatePicker ID="edFechaFinal" Runat="server" MinDate="2014-01-01" >
                                        </telerik:RadDatePicker>
                                        <asp:RequiredFieldValidator ID="rfedFechaFinal" runat="server" 
                                            ControlToValidate="edFechaFinal" Display="Dynamic" EnableClientScript="true" 
                                            ErrorMessage="Favor Ingresar una de Fin" />
                                    </td>
                                    <td style="width: 90%">
                                    </td>
                                </tr>
                            </table>
                            <asp:ImageButton ID="btnSave" runat="server" 
                                AlternateText='<%# (Container is GridEditFormInsertItem) ? "Agregar Usuario" : "Actualizar Usuario" %>' 
                                CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>' 
                                ImageUrl="Images/16/check2_16.png" />
                            &nbsp;
                            <asp:ImageButton ID="btnCancel" runat="server" AlternateText="Cancelar" 
                                CausesValidation="false" CommandName="Cancel" 
                                ImageUrl="Images/16/delete2_16.png" />
                        </FormTemplate>
                    </EditFormSettings>
                </MasterTableView>
                <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                    <Selecting AllowRowSelect="True" />
                    <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                        DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                        DropHereToReorder="Suelte aquí para Re-Ordenar" />
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                </ClientSettings>
                <FilterMenu EnableTheming="True">
                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                </FilterMenu>
                <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
            </telerik:RadGrid>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgUsuarios">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgUsuarios" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rgRoles">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgRoles" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>
