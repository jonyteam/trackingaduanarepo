﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;
using System.Data;

using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;

public partial class GestionDocumentos : Utilidades.PaginaBase
{
    private NameValueCollection mParamethers;
    private GrupoLis.Login.Login logAppAduanas;

    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Gestion Documentos";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        mParamethers = ConfigurationManager.AppSettings;
        SetGridFilterMenu(rgInstrucciones.FilterMenu);
        rgInstrucciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgInstrucciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        SetGridFilterMenu(rgDocumentos.FilterMenu);
        rgDocumentos.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgDocumentos.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Gestion Documentos", "Gestion Documentos y Permisos");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");

            mpInstrucciones.SelectedIndex = 0;
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    #region Instrucciones
    protected void rgInstrucciones_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridInstrucciones();
    }

    private void llenarGridInstrucciones()
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            if (User.IsInRole("Administradores"))
                i.loadInstruccionesCliente();
            else if (User.IsInRole("Administrador Pais") || User.IsInRole("Administrador Pais SVGT") || User.IsInRole("Oficial de Cuenta") || User.Identity.Name == "davila" || User.Identity.Name == "spolanco" || User.IsInRole("Jefe Centralización"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                i.loadInstruccionesXPais(u.CODPAIS);
            }
            else if (User.IsInRole("Operaciones") && User.Identity.Name != "davila" || User.IsInRole("Operaciones") && User.Identity.Name != "spolanco")
            {
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.totalRegistros > 0)
                    i.loadInstruccionesXPais(u.CODPAIS);
            }
            else if (User.IsInRole("Jefes Aduana") || User.IsInRole("Analista de Documentos"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.totalRegistros > 0)
                    i.loadInstruccionXAduana(u.CODIGOADUANA);
            }
            else if (User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT"))
            {
                string idU = Session["IdUsuario"].ToString();
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.totalRegistros > 0)
                    if (u.CODPAIS == "H")
                        if (User.Identity.Name == "eizaguirre")
                        { i.loadInstruccionXAduanaUsuario("016", idU); }
                        else
                        { i.loadInstruccionXAduanaUsuario(u.CODIGOADUANA, idU); }



                    else
                        //i.loadInstruccionXUsuario(idU);
                        i.loadInstruccionesXPais(u.CODPAIS);
            }
            rgInstrucciones.DataSource = i.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void rgInstrucciones_Init(object sender, System.EventArgs e)
    {
        GridFilterMenu menu = rgInstrucciones.FilterMenu;
        menu.Items.RemoveAt(rgInstrucciones.FilterMenu.Items.Count - 2);
    }

    protected void rgInstrucciones_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            GridEditableItem editedItem = e.Item as GridEditableItem;
            InstruccionesBO i = new InstruccionesBO(logApp);
            InstruccionesBO ins = new InstruccionesBO(logApp);
            MovimientosEspeciesFiscalesBO mef = new MovimientosEspeciesFiscalesBO(logApp);
            if (e.CommandName == "Gestion")
            {
                idInstruccion.Value = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdInstruccion"]);
                llenarDatos();
                llenarGridDocuementos();
                llenarGridOtrosDocuementos();
                llenarGridPermisos();
                llenarCheckBox();
                llenarCheckBoxPermisos();
                llenarDatosEspeciesFiscales();
                mpInstrucciones.SelectedIndex = 1;
                ins.loadInstruccionModificar(idInstruccion.Value);
                ClientesBO clbo = new ClientesBO(logApp);
                string mensaje = "", mensaje2 = "";
                clbo.loadClienteMensaje(ins.IDCLIENTE);
                if (clbo.totalRegistros > 0)
                {
                    mensaje = "X";
                    mensaje2 = clbo.MENSAJE;
                }
                if (mensaje == "X")
                {
                    registrarMensaje(mensaje2);
                }

            }
            else if (rgInstrucciones.Items.Count <= 0 & e.CommandName == RadGrid.ExportToExcelCommandName)
                e.Canceled = true;
            else if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                ConfigureExport();
            }
        }
        finally
        {
            desconectar();
        }
    }

    private void ConfigureExport()
    {
        rgInstrucciones.Columns[0].Visible = false;
        String filename = "Instrucciones" + "_" + DateTime.Now.ToShortDateString();
        rgInstrucciones.GridLines = GridLines.Both;
        rgInstrucciones.ExportSettings.FileName = filename;
        rgInstrucciones.ExportSettings.IgnorePaging = true;
        rgInstrucciones.ExportSettings.OpenInNewWindow = false;
        rgInstrucciones.ExportSettings.ExportOnlyData = true;
    }
    #endregion

    #region Documentos
    private void llenarDatos()
    {
        try
        {
            conectar();
            DatosGestionDocumentosBO dgd = new DatosGestionDocumentosBO(logApp);
            dgd.loadDatosGestionDocumentos(idInstruccion.Value);
            if (dgd.totalRegistros > 0)
            {
                txtNoManifiesto.Text = dgd.NOMANIFIESTO;
                txtEmpresa.Text = dgd.EMPRESA;
                txtMotorista.Text = dgd.MOTORISTA;
                txtPlaca.Text = dgd.PLACA;// marca
                txtFurgon.Text = dgd.FURGON; //placa furgon 
                txtCabezal.Text = dgd.CABEZAL; //placa cabezal
                txtTelefono.Text = dgd.CELULAR;
                txtobservaciongeneral.Text = dgd.OBSERVACIONGENERAL;
            }
            else
                limpiarControles();
        }
        catch { }
    }

    protected void rgDocumentos_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenarGridDocuementos();
    }

    private void llenarGridDocuementos()
    {
        try
        {
            conectar();
            InstruccionesBO ins = new InstruccionesBO(logApp);
            DocumentosRegimenBO dr = new DocumentosRegimenBO(logApp);
            CodigosBO c = new CodigosBO(logApp);
            DataTable dt = new DataTable();
            DataRow row;
            ins.loadInstruccionGD(idInstruccion.Value.Trim());
            dr.loadDocumentosRegimenALLXCodRegimen(ins.CODPAISHOJARUTA, ins.CODREGIMEN);
            /////////codigo nuevo//////////
            dt.Columns.Add("CodDocumento", typeof(String));
            dt.Columns.Add("Documento", typeof(String));
            for (int i = 0; i < dr.totalRegistros; i++)
            {
                row = dt.NewRow();
                row["CodDocumento"] = dr.CODDOCUMENTO;
                string Prueba = dr.TABLA.Rows[i]["Documento"].ToString();
                row["Documento"] = dr.TABLA.Rows[i]["Documento"].ToString();
                dt.Rows.Add(row);
                dr.regSiguiente();
            }
            c.loadAllCampos("DOCUMENTOEMBARQUE", ins.CODDOCUMENTOEMBARQUE);
            row = dt.NewRow();
            row["CodDocumento"] = c.CODIGO;
            row["Documento"] = c.DESCRIPCION;
            dt.Rows.Add(row);
            ///////////////////////////////
            rgDocumentos.DataSource = dt;
            rgDocumentos.DataBind();

            txtInstruccion.Text = idInstruccion.Value;
            c.loadAllCampos("PAISES", ins.CODPAISHOJARUTA);
            c.loadAllCampos("REGIMEN" + c.DESCRIPCION, ins.CODREGIMEN);
            txtRegimen.Text = c.CODIGO + " | " + c.DESCRIPCION;
        }
        catch (Exception)
        { }
    }

    private void llenarCheckBox()
    {
        try
        {
            conectar();
            DocumentosInstruccionesBO di = new DocumentosInstruccionesBO(logApp);
            di.loadDocumentosXInstrucciones(idInstruccion.Value.Trim());
            if (di.totalRegistros > 0)
            {
                for (int i = 0; i < di.totalRegistros; i++)
                {
                    //llena los checkbox en el grid Documentos
                    foreach (GridDataItem gridItem in rgDocumentos.Items)
                    {
                        string codDocumento = gridItem.Cells[2].Text;
                        CheckBox chkDocOriginal = (CheckBox)gridItem.FindControl("ckbDocumentoOriginal");
                        CheckBox chkDocCopia = (CheckBox)gridItem.FindControl("ckbDocumentoCopia");
                        RadTextBox edValor = (RadTextBox)gridItem.FindControl("edValor");
                        RadTextBox edObservacion = (RadTextBox)gridItem.FindControl("edObservacion");
                        if (codDocumento == di.CODDOCUMENTO)
                        {
                            if (di.ORIGINAL == "X")
                            {
                                chkDocOriginal.Checked = true;
                                edValor.Text = di.DOCUMENTONO;
                                edObservacion.Text = di.OBSERVACION;
                                //nuevo código
                                if (User.IsInRole("Administradores"))
                                {
                                    chkDocOriginal.Enabled = true;
                                    chkDocCopia.Enabled = true;
                                    edValor.Enabled = true;
                                    edObservacion.Enabled = true;
                                }
                                else
                                {
                                    chkDocOriginal.Enabled = false;
                                    chkDocCopia.Enabled = false;
                                    edValor.Enabled = false;
                                    edObservacion.Enabled = false;
                                }
                            }
                            else
                            {
                                chkDocOriginal.Checked = false;
                                edValor.Text = "";
                                edObservacion.Text = "";
                            }
                        }

                        if (codDocumento == di.CODDOCUMENTO)
                        {
                            if (di.COPIA == "X")
                            {
                                chkDocCopia.Checked = true;
                                edValor.Text = di.DOCUMENTONO;
                                edObservacion.Text = di.OBSERVACION;
                                //nuevo código
                                if (User.IsInRole("Administradores"))
                                {
                                    chkDocOriginal.Enabled = true;
                                    chkDocCopia.Enabled = true;
                                    edValor.Enabled = true;
                                    edObservacion.Enabled = true;
                                }
                                else
                                {
                                    chkDocCopia.Enabled = false;
                                    edValor.Enabled = false;
                                    edObservacion.Enabled = false;
                                }
                            }
                            else
                            {
                                chkDocCopia.Checked = false;
                            }
                        }
                    }

                    //llena los checkbox en el grid OtrosDocumentos
                    foreach (GridDataItem gridItem in rgOtrosDocumentos.Items)
                    {
                        //Int64 tamano = rgDocumentos.Items.Count;
                        string[] arreglo = new string[1];
                        arreglo[0] = gridItem.Cells[2].Text;
                        //string valor = gridItem.Cells[3].Text;
                        CheckBox chkDocOriginal = (CheckBox)gridItem.FindControl("ckbDocumentoOriginal");
                        CheckBox chkDocCopia = (CheckBox)gridItem.FindControl("ckbDocumentoCopia");
                        RadTextBox edValor = (RadTextBox)gridItem.FindControl("edValor");
                        RadTextBox edObservacion = (RadTextBox)gridItem.FindControl("edObservacion");
                        foreach (string itemGrid in arreglo)
                        {
                            if (itemGrid.ToString() == di.CODDOCUMENTO)
                            {
                                if (di.ORIGINAL == "X")
                                {
                                    chkDocOriginal.Checked = true;
                                    edValor.Text = di.DOCUMENTONO;
                                    edObservacion.Text = di.OBSERVACION;
                                    //nuevo código
                                    if (User.IsInRole("Administradores"))
                                    {
                                        chkDocOriginal.Enabled = true;
                                        chkDocCopia.Enabled = true;
                                        edValor.Enabled = true;
                                        edObservacion.Enabled = true;
                                    }
                                    else
                                    {
                                        chkDocOriginal.Enabled = false;
                                        chkDocCopia.Enabled = false;
                                        edValor.Enabled = false;
                                        edObservacion.Enabled = false;
                                    }
                                }
                                else
                                {
                                    chkDocOriginal.Checked = false;
                                    edValor.Text = "";
                                    edObservacion.Text = "";
                                }
                            }
                        }

                        chkDocCopia = (CheckBox)gridItem.FindControl("ckbDocumentoCopia");
                        foreach (string itemGrid in arreglo)
                        {
                            if (itemGrid.ToString() == di.CODDOCUMENTO)
                            {
                                if (di.COPIA == "X")
                                {
                                    chkDocCopia.Checked = true;
                                    edValor.Text = di.DOCUMENTONO;
                                    edObservacion.Text = di.OBSERVACION;
                                    //nuevo código
                                    if (User.IsInRole("Administradores"))
                                    {
                                        chkDocOriginal.Enabled = true;
                                        chkDocCopia.Enabled = true;
                                        edValor.Enabled = true;
                                        edObservacion.Enabled = true;
                                    }
                                    else
                                    {
                                        chkDocCopia.Enabled = false;
                                        edValor.Enabled = false;
                                        edObservacion.Enabled = false;
                                    }
                                }
                                else
                                {
                                    chkDocCopia.Checked = false;
                                }
                            }
                        }
                    }

                    di.regSiguiente();
                }
            }

            else
            {
                conectar();
                InstruccionesBO In = new InstruccionesBO(logApp);
                In.loadInstruccionModificar(idInstruccion.Value.Trim());
                for (int i = 0; i < In.totalRegistros; i++)
                {
                    if (In.NUMEROFACTURA != "")
                    {
                        //llena los checkbox en el grid Documentos
                        foreach (GridDataItem gridItem in rgDocumentos.Items)
                        {
                            string codDocumento = gridItem.Cells[2].Text;
                            CheckBox chkDocOriginal = (CheckBox)gridItem.FindControl("ckbDocumentoOriginal");
                            CheckBox chkDocCopia = (CheckBox)gridItem.FindControl("ckbDocumentoCopia");
                            RadTextBox edValor = (RadTextBox)gridItem.FindControl("edValor");
                            RadTextBox edObservacion = (RadTextBox)gridItem.FindControl("edObservacion");
                            if (codDocumento == "1")
                            {

                                chkDocCopia.Checked = true;
                                edValor.Text = In.NUMEROFACTURA;
                                edObservacion.Text = "Asignada en la Creacion";
                                //nuevo código
                                if (User.IsInRole("Administradores"))
                                {
                                    chkDocOriginal.Enabled = true;
                                    chkDocCopia.Enabled = true;
                                    edValor.Enabled = true;
                                    edObservacion.Enabled = true;
                                }
                                else
                                {
                                    chkDocCopia.Enabled = false;
                                    edValor.Enabled = false;
                                    edObservacion.Enabled = false;
                                }

                            }
                        }

                    }
                }
            }
        }
        catch (Exception)
        { }
    }

    protected void rgOtrosDocumentos_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenarGridOtrosDocuementos();
    }

    private void llenarGridOtrosDocuementos()
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            CodigosBO c = new CodigosBO(logApp);
            i.loadInstruccionGD(idInstruccion.Value.Trim());
            c.loadOtrosDocumentos(i.CODREGIMEN, i.CODPAISHOJARUTA);
            rgOtrosDocumentos.DataSource = c.TABLA;
            rgOtrosDocumentos.DataBind();
        }
        catch (Exception)
        { }
    }

    protected void rgDocumentos_ItemCreated(object sender, GridItemEventArgs e)
    {
        try
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem editForm = e.Item as GridDataItem;
                CheckBox ckbDocumentoOriginal = editForm.FindControl("ckbDocumentoOriginal") as CheckBox;
                CheckBox ckbDocumentoCopia = editForm.FindControl("ckbDocumentoCopia") as CheckBox;
                ckbDocumentoOriginal.CheckedChanged += new EventHandler(ckbDocumentoOriginal_CheckedChanged);
                ckbDocumentoCopia.CheckedChanged += new EventHandler(ckbDocumentoCopia_CheckedChanged);
            }
        }
        catch { }
    }

    void ckbDocumentoOriginal_CheckedChanged(object sender, EventArgs e)
    {
        foreach (GridDataItem gridItem in rgDocumentos.Items)
        {
            CheckBox ckbDocumentoOriginal = gridItem.FindControl("ckbDocumentoOriginal") as CheckBox;
            CheckBox ckbDocumentoCopia = gridItem.FindControl("ckbDocumentoCopia") as CheckBox;
            RadTextBox edValor = gridItem.FindControl("edValor") as RadTextBox;
            RadTextBox edObservacion = gridItem.FindControl("edObservacion") as RadTextBox;
            if (ckbDocumentoOriginal.Checked == true & String.IsNullOrEmpty(edValor.Text.Trim()))
            {
                ckbDocumentoOriginal.Checked = false;
                edValor.Enabled = true;
                registrarMensaje("El valor para este documento no puede estar vacío");
            }
            if (ckbDocumentoOriginal.Checked == true & !String.IsNullOrEmpty(edValor.Text.Trim()))
                edValor.Enabled = false;
            if (ckbDocumentoOriginal.Checked == false & ckbDocumentoCopia.Checked == false)
            {
                edValor.Text = "";
                edObservacion.Text = "";
                edValor.Enabled = true;
            }
        }
    }

    void ckbDocumentoCopia_CheckedChanged(object sender, EventArgs e)
    {
        foreach (GridDataItem gridItem in rgDocumentos.Items)
        {
            CheckBox ckbDocumentoOriginal = gridItem.FindControl("ckbDocumentoOriginal") as CheckBox;
            CheckBox ckbDocumentoCopia = gridItem.FindControl("ckbDocumentoCopia") as CheckBox;
            RadTextBox edValor = gridItem.FindControl("edValor") as RadTextBox;
            RadTextBox edObservacion = gridItem.FindControl("edObservacion") as RadTextBox;
            if (ckbDocumentoCopia.Checked == true & String.IsNullOrEmpty(edValor.Text.Trim()))
            {
                ckbDocumentoCopia.Checked = false;
                edValor.Enabled = true;
                registrarMensaje("El valor para este documento no puede estar vacío");
            }
            if (ckbDocumentoCopia.Checked == true & !String.IsNullOrEmpty(edValor.Text.Trim()))
                edValor.Enabled = false;
            if (ckbDocumentoOriginal.Checked == false & ckbDocumentoCopia.Checked == false)
            {
                edValor.Text = "";
                edObservacion.Text = "";
                edValor.Enabled = true;
            }
        }
    }

    protected void rgOtrosDocumentos_ItemCreated(object sender, GridItemEventArgs e)
    {
        try
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem editForm = e.Item as GridDataItem;
                CheckBox ckbDocumentoOriginalO = editForm.FindControl("ckbDocumentoOriginal") as CheckBox;
                CheckBox ckbDocumentoCopiaO = editForm.FindControl("ckbDocumentoCopia") as CheckBox;
                ckbDocumentoOriginalO.CheckedChanged += new EventHandler(ckbDocumentoOriginalO_CheckedChanged);
                ckbDocumentoCopiaO.CheckedChanged += new EventHandler(ckbDocumentoCopiaO_CheckedChanged);
            }
        }
        catch { }
    }

    void ckbDocumentoOriginalO_CheckedChanged(object sender, EventArgs e)
    {
        foreach (GridDataItem gridItem in rgOtrosDocumentos.Items)
        {
            CheckBox ckbDocumentoOriginal = gridItem.FindControl("ckbDocumentoOriginal") as CheckBox;
            CheckBox ckbDocumentoCopia = gridItem.FindControl("ckbDocumentoCopia") as CheckBox;
            RadTextBox edValor = gridItem.FindControl("edValor") as RadTextBox;
            RadTextBox edObservacion = gridItem.FindControl("edObservacion") as RadTextBox;
            if (ckbDocumentoOriginal.Checked == true & String.IsNullOrEmpty(edValor.Text.Trim()))
            {
                ckbDocumentoOriginal.Checked = false;
                edValor.Enabled = true;
                registrarMensaje("El valor para este documento no puede estar vacío");
            }
            if (ckbDocumentoOriginal.Checked == true & !String.IsNullOrEmpty(edValor.Text.Trim()))
                edValor.Enabled = false;
            if (ckbDocumentoOriginal.Checked == false & ckbDocumentoCopia.Checked == false)
            {
                edValor.Text = "";
                edObservacion.Text = "";
                edValor.Enabled = true;
            }
        }
    }

    void ckbDocumentoCopiaO_CheckedChanged(object sender, EventArgs e)
    {
        foreach (GridDataItem gridItem in rgOtrosDocumentos.Items)
        {
            CheckBox ckbDocumentoOriginal = gridItem.FindControl("ckbDocumentoOriginal") as CheckBox;
            CheckBox ckbDocumentoCopia = gridItem.FindControl("ckbDocumentoCopia") as CheckBox;
            RadTextBox edValor = gridItem.FindControl("edValor") as RadTextBox;
            RadTextBox edObservacion = gridItem.FindControl("edObservacion") as RadTextBox;
            if (ckbDocumentoCopia.Checked == true & String.IsNullOrEmpty(edValor.Text.Trim()))
            {
                ckbDocumentoCopia.Checked = false;
                edValor.Enabled = true;
                registrarMensaje("El valor para este documento no puede estar vacío");
            }
            if (ckbDocumentoCopia.Checked == true & !String.IsNullOrEmpty(edValor.Text.Trim()))
                edValor.Enabled = false;
            if (ckbDocumentoOriginal.Checked == false & ckbDocumentoCopia.Checked == false)
            {
                edValor.Text = "";
                edObservacion.Text = "";
                edValor.Enabled = true;
            }
        }
    }

    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            conectar();
            conectarAduanas();
            DocumentosInstruccionesBO di = new DocumentosInstruccionesBO(logApp);
            DocumentosRegimenBO dr = new DocumentosRegimenBO(logApp);
            GestionCargaBO gc = new GestionCargaBO(logApp);
            InstruccionesBO ins = new InstruccionesBO(logApp);
            PermisosInstruccionesBO pi = new PermisosInstruccionesBO(logApp);
            EspeciesFiscalesInstruccionesBO efi = new EspeciesFiscalesInstruccionesBO(logApp);
            DatosGestionDocumentosBO dgd = new DatosGestionDocumentosBO(logApp);
            if (!string.IsNullOrEmpty(txtInstruccion.Text.Trim()))
            {
                #region codigo
                dgd.loadDatosGestionDocumentos(txtInstruccion.Text);
                if (dgd.totalRegistros <= 0)
                {
                    //if (!String.IsNullOrEmpty(txtNoManifiesto.Text.Trim()))
                    //{
                    dgd.newLine();
                    dgd.IDINSTRUCCION = txtInstruccion.Text.Trim();
                    dgd.NOMANIFIESTO = txtNoManifiesto.Text.Trim();
                    dgd.EMPRESA = txtEmpresa.Text.Trim();
                    dgd.MOTORISTA = txtMotorista.Text.Trim();
                    dgd.CELULAR = txtTelefono.Text.Trim();
                    dgd.PLACA = txtPlaca.Text.Trim();
                    dgd.FURGON = txtFurgon.Text.Trim();
                    dgd.CABEZAL = txtCabezal.Text.Trim();
                    dgd.FECHA = DateTime.Now.ToString();
                    dgd.ELIMINADO = "0";
                    dgd.IDUSUARIO = Session["IdUsuario"].ToString();
                    dgd.OBSERVACIONGENERAL = txtobservaciongeneral.Text;
                    dgd.commitLine();
                    dgd.actualizar();

                    try
                    {
                        //conectarAduanas();
                        HojaRutaDatosBO hrd = new HojaRutaDatosBO(logAppAduanas);
                        hrd.loadAllCamposHojaRutaDatos("-1");
                        if (!String.IsNullOrEmpty(txtPlaca.Text.Trim()))
                        {
                            hrd.newLine();
                            hrd.IDHOJARUTA = txtInstruccion.Text.Trim();
                            hrd.CODIGO = "1";
                            hrd.DATOS = txtPlaca.Text.Trim();
                            hrd.commitLine();
                            hrd.actualizar();
                        }
                        if (!String.IsNullOrEmpty(txtCabezal.Text.Trim()))
                        {
                            hrd.newLine();
                            hrd.IDHOJARUTA = txtInstruccion.Text.Trim();
                            hrd.CODIGO = "2";
                            hrd.DATOS = txtCabezal.Text.Trim();
                            hrd.commitLine();
                            hrd.actualizar();
                        }
                        if (!String.IsNullOrEmpty(txtFurgon.Text.Trim()))
                        {
                            hrd.newLine();
                            hrd.IDHOJARUTA = txtInstruccion.Text.Trim();
                            hrd.CODIGO = "3";
                            hrd.DATOS = txtFurgon.Text.Trim();
                            hrd.commitLine();
                            hrd.actualizar();
                        }
                        if (!String.IsNullOrEmpty(txtMotorista.Text.Trim()))
                        {
                            hrd.newLine();
                            hrd.IDHOJARUTA = txtInstruccion.Text.Trim();
                            hrd.CODIGO = "4";
                            hrd.DATOS = txtMotorista.Text.Trim();
                            hrd.commitLine();
                            hrd.actualizar();
                        }
                        if (!String.IsNullOrEmpty(txtEmpresa.Text.Trim()))
                        {
                            hrd.newLine();
                            hrd.IDHOJARUTA = txtInstruccion.Text.Trim();
                            hrd.CODIGO = "5";
                            hrd.DATOS = txtEmpresa.Text.Trim();
                            hrd.commitLine();
                            hrd.actualizar();
                        }
                        if (!String.IsNullOrEmpty(txtTelefono.Text.Trim()))
                        {
                            hrd.newLine();
                            hrd.IDHOJARUTA = txtInstruccion.Text.Trim();
                            hrd.CODIGO = "6";
                            hrd.DATOS = txtTelefono.Text.Trim();
                            hrd.commitLine();
                            hrd.actualizar();
                        }
                    }
                    catch (Exception ex)
                    {
                        logError(ex.Message, Session["IdUsuario"].ToString(), "GestionDocumentos, crear, HojaRutaDatos");
                    }
                    finally
                    {
                        //desconectarAduanas();
                    }
                    //}
                }
                else
                {
                    dgd.NOMANIFIESTO = txtNoManifiesto.Text.Trim();
                    dgd.EMPRESA = txtEmpresa.Text.Trim();
                    dgd.MOTORISTA = txtMotorista.Text.Trim();
                    dgd.PLACA = txtPlaca.Text.Trim();
                    dgd.FURGON = txtFurgon.Text.Trim();
                    dgd.CABEZAL = txtCabezal.Text.Trim();
                    dgd.CELULAR = txtTelefono.Text.Trim();
                    dgd.OBSERVACIONGENERAL = txtobservaciongeneral.Text;
                    dgd.actualizar();

                    try
                    {
                        //conectarAduanas();
                        HojaRutaDatosBO hrd = new HojaRutaDatosBO(logAppAduanas);
                        hrd.loadBusquedaHojaRuta(txtInstruccion.Text.Trim(), "1");
                        if (hrd.totalRegistros > 0)
                        {
                            hrd.DATOS = txtPlaca.Text.Trim();
                            hrd.actualizar();
                        }
                        else
                        {
                            hrd.newLine();
                            hrd.IDHOJARUTA = txtInstruccion.Text.Trim();
                            hrd.CODIGO = "1";
                            hrd.DATOS = txtPlaca.Text.Trim();
                            hrd.commitLine();
                            hrd.actualizar();
                        }
                        hrd.loadBusquedaHojaRuta(txtInstruccion.Text.Trim(), "2");
                        if (hrd.totalRegistros > 0)
                        {
                            hrd.DATOS = txtCabezal.Text.Trim();
                            hrd.actualizar();
                        }
                        else
                        {
                            hrd.newLine();
                            hrd.IDHOJARUTA = txtInstruccion.Text.Trim();
                            hrd.CODIGO = "2";
                            hrd.DATOS = txtCabezal.Text.Trim();
                            hrd.commitLine();
                            hrd.actualizar();
                        }
                        hrd.loadBusquedaHojaRuta(txtInstruccion.Text.Trim(), "3");
                        if (hrd.totalRegistros > 0)
                        {
                            hrd.DATOS = txtFurgon.Text.Trim();
                            hrd.actualizar();
                        }
                        else
                        {
                            hrd.newLine();
                            hrd.IDHOJARUTA = txtInstruccion.Text.Trim();
                            hrd.CODIGO = "3";
                            hrd.DATOS = txtFurgon.Text.Trim();
                            hrd.commitLine();
                            hrd.actualizar();
                        }
                        hrd.loadBusquedaHojaRuta(txtInstruccion.Text.Trim(), "4");
                        if (hrd.totalRegistros > 0)
                        {
                            hrd.DATOS = txtMotorista.Text.Trim();
                            hrd.actualizar();
                        }
                        else
                        {
                            hrd.newLine();
                            hrd.IDHOJARUTA = txtInstruccion.Text.Trim();
                            hrd.CODIGO = "4";
                            hrd.DATOS = txtMotorista.Text.Trim();
                            hrd.commitLine();
                            hrd.actualizar();
                        }
                        hrd.loadBusquedaHojaRuta(txtInstruccion.Text.Trim(), "5");
                        if (hrd.totalRegistros > 0)
                        {
                            hrd.DATOS = txtEmpresa.Text.Trim();
                            hrd.actualizar();
                        }
                        else
                        {
                            hrd.newLine();
                            hrd.IDHOJARUTA = txtInstruccion.Text.Trim();
                            hrd.CODIGO = "5";
                            hrd.DATOS = txtEmpresa.Text.Trim();
                            hrd.commitLine();
                            hrd.actualizar();
                        }
                        hrd.loadBusquedaHojaRuta(txtInstruccion.Text.Trim(), "6");
                        if (hrd.totalRegistros > 0)
                        {
                            hrd.DATOS = txtTelefono.Text.Trim();
                            hrd.actualizar();
                        }
                        else
                        {
                            hrd.newLine();
                            hrd.IDHOJARUTA = txtInstruccion.Text.Trim();
                            hrd.CODIGO = "6";
                            hrd.DATOS = txtTelefono.Text.Trim();
                            hrd.commitLine();
                            hrd.actualizar();
                        }
                    }
                    catch (Exception ex)
                    {
                        logError(ex.Message, Session["IdUsuario"].ToString(), "GestionDocumentos, crear, HojaRutaDatos");
                    }
                    finally
                    {
                        //desconectarAduanas();
                    }
                }

                //guarda o actualiza los documentos mínimos originiales o copias
                foreach (GridDataItem gridItem in rgDocumentos.Items)
                {
                    ins.loadInstruccion(txtInstruccion.Text.Trim());
                    //guarda o actualiza registros de los documentos originales chequeados
                    CheckBox ckbDocumentoOriginal = (CheckBox)gridItem.FindControl("ckbDocumentoOriginal");
                    CheckBox ckbDocumentoCopia = (CheckBox)gridItem.FindControl("ckbDocumentoCopia");
                    RadTextBox edValor = (RadTextBox)gridItem.FindControl("edValor");
                    RadTextBox edObservacion = (RadTextBox)gridItem.FindControl("edObservacion");

                    if (ckbDocumentoOriginal.Checked == true)
                    {
                        string codDocumento = (gridItem.Cells[2].Text);
                        di.loadDocumentosXInstrucciones(txtInstruccion.Text.Trim(), codDocumento);
                        if (di.totalRegistros == 1)
                        {
                            if (di.ORIGINAL != "X")
                            {
                                di.ORIGINAL = "X";
                                di.DOCUMENTONO = edValor.Text;
                                di.OBSERVACION = edObservacion.Text;
                                di.FECHAORIGINAL = DateTime.Now.ToString();
                                di.IDUSUARIOORIGINAL = Session["IdUsuario"].ToString();
                                di.REQUERIDO = "1";
                                di.actualizar();
                                if (codDocumento == "1")
                                {
                                    ins.NUMEROFACTURA = edValor.Text;
                                    ins.commitLine();
                                    ins.actualizar();
                                }
                            }
                        }
                        else if (di.totalRegistros <= 0)
                        {
                            di.newLine();
                            di.IDINSTRUCCION = txtInstruccion.Text;
                            di.CODDOCUMENTO = codDocumento;
                            di.ORIGINAL = "X";
                            di.DOCUMENTONO = edValor.Text;
                            di.OBSERVACION = edObservacion.Text;
                            di.FECHAORIGINAL = DateTime.Now.ToString();
                            di.IDUSUARIOORIGINAL = Session["IdUsuario"].ToString();
                            di.ELIMINADO = "0";
                            di.REQUERIDO = "1";
                            di.commitLine();
                            di.actualizar();
                            //Darwin lo agrego para guardar el numero de factura en Instrucciones por si era editado
                            if (codDocumento == "1")
                            {
                                ins.NUMEROFACTURA = edValor.Text;
                                ins.commitLine();
                                ins.actualizar();
                            }
                            //consultar si ya ingresó todos los documentos mínimos, si es así registrar comienzo del flujo
                            gc.loadGestionCargaXInstruccionEvento(di.IDINSTRUCCION, "0");
                            if (gc.totalRegistros <= 0)
                            {
                                dr.loadDocumentosXRegimenPais(ins.CODREGIMEN, ins.CODPAISHOJARUTA);
                                di.loadDocumentosMinimosXInstruccion(txtInstruccion.Text.Trim());
                                if ((dr.totalRegistros + 1) == di.totalRegistros)
                                {
                                    gc.newLine();
                                    gc.IDINSTRUCCION = txtInstruccion.Text.Trim();
                                    gc.IDESTADOFLUJO = "0";
                                    gc.ESTADO = "0";
                                    gc.FECHA = DateTime.Now.ToString();
                                    gc.FECHAINGRESO = gc.FECHA;
                                    //gc.OBSERVACION = "";
                                    gc.ELIMINADO = "0";
                                    gc.IDUSUARIO = Session["IdUsuario"].ToString();
                                    gc.commitLine();
                                    gc.actualizar();
                                }
                            }
                            //ingresar registro de recepcion de documentos en tramite aduanero de EsquemaTramites2
                            try
                            {
                                TramiteAduaneroBO ta = new TramiteAduaneroBO(logAppAduanas);
                                ta.loadTramiteAduanero("-1");
                                ta.newLine();
                                ta.IDHOJARUTA = txtInstruccion.Text.Trim();
                                ta.CODIGO = "2";
                                ta.OBSERVACION = "Aduanas";
                                ta.FECHA = DateTime.Now.ToString();
                                ta.ESTADO = "0";
                                ta.commitLine();
                                ta.actualizar();
                            }
                            catch { }
                        }
                        //el siguiente codigo es para guardar el numero de factura de la instruccion en HojaRuta de Aduanas
                        if (codDocumento.Trim() == "1")
                        {
                            try
                            {
                                //conectarAduanas();
                                HojaRutaBO hr = new HojaRutaBO(logAppAduanas);
                                hr.loadHojaRuta(txtInstruccion.Text.Trim());
                                if (hr.totalRegistros > 0)
                                {
                                    hr.NUMFACTURA = edValor.Text;
                                    hr.actualizar();
                                }
                            }
                            catch { }
                        }
                    }
                    else
                    {
                        string codDocumento = (gridItem.Cells[2].Text);
                        di.loadDocumentosXInstrucciones(txtInstruccion.Text.Trim(), codDocumento);
                        if (di.totalRegistros == 1)
                        {
                            if (!String.IsNullOrEmpty(di.ORIGINAL.Trim()))
                            {
                                //hacer consulta para inicializar el registro de documentos originales
                                int a = di.inicializarDocumentoOriginal(txtInstruccion.Text.Trim(), codDocumento);
                            }
                        }
                    }

                    //guarda o actualiza registros de los documentos copias chequeados
                    ckbDocumentoCopia = (CheckBox)gridItem.FindControl("ckbDocumentoCopia");
                    if (ckbDocumentoCopia.Checked == true)
                    {
                        string codDocumento = (gridItem.Cells[2].Text);
                        di.loadDocumentosXInstrucciones(txtInstruccion.Text.Trim(), codDocumento);
                        if (di.totalRegistros == 1)
                        {
                            if (di.COPIA != "X")
                            {
                                di.COPIA = "X";
                                di.DOCUMENTONO = edValor.Text;
                                di.OBSERVACION = edObservacion.Text;
                                di.FECHACOPIA = DateTime.Now.ToString();
                                di.IDUSUARIOCOPIA = Session["IdUsuario"].ToString();
                                di.REQUERIDO = "1";
                                di.actualizar();
                                if (codDocumento == "1")
                                {
                                    ins.NUMEROFACTURA = edValor.Text;
                                    ins.commitLine();
                                    ins.actualizar();
                                }
                            }
                        }
                        else if (di.totalRegistros <= 0)
                        {
                            di.newLine();
                            di.IDINSTRUCCION = txtInstruccion.Text;
                            di.CODDOCUMENTO = codDocumento;
                            di.COPIA = "X";
                            di.DOCUMENTONO = edValor.Text;
                            di.OBSERVACION = edObservacion.Text;
                            di.FECHACOPIA = DateTime.Now.ToString();
                            di.IDUSUARIOCOPIA = Session["IdUsuario"].ToString();
                            di.ELIMINADO = "0";
                            di.REQUERIDO = "1";
                            di.commitLine();
                            di.actualizar();
                            if (codDocumento == "1")
                            {
                                ins.NUMEROFACTURA = edValor.Text;
                                ins.commitLine();
                                ins.actualizar();
                            }
                            //consultar si ya ingresó todos los documentos mínimos, si es así registrar comienzo del flujo
                            gc.loadGestionCargaXInstruccionEvento(di.IDINSTRUCCION, "0");
                            if (gc.totalRegistros <= 0)
                            {
                                dr.loadDocumentosXRegimenPais(ins.CODREGIMEN, ins.CODPAISHOJARUTA);
                                di.loadDocumentosMinimosXInstruccion(txtInstruccion.Text.Trim());
                                if ((dr.totalRegistros + 1) == di.totalRegistros)
                                {
                                    gc.newLine();
                                    gc.IDINSTRUCCION = txtInstruccion.Text.Trim();
                                    gc.IDESTADOFLUJO = "0";
                                    gc.ESTADO = "0";
                                    gc.FECHA = DateTime.Now.ToString();
                                    gc.FECHAINGRESO = gc.FECHA;
                                    //gc.OBSERVACION = "";
                                    gc.ELIMINADO = "0";
                                    gc.IDUSUARIO = Session["IdUsuario"].ToString();
                                    gc.commitLine();
                                    gc.actualizar();
                                }
                            }
                            //ingresar registro de recepcion de documentos en tramite aduanero de EsquemaTramites2
                            try
                            {
                                TramiteAduaneroBO ta = new TramiteAduaneroBO(logAppAduanas);
                                ta.loadTramiteAduanero("-1");
                                ta.newLine();
                                ta.IDHOJARUTA = txtInstruccion.Text.Trim();
                                ta.CODIGO = "2";
                                ta.OBSERVACION = "Aduanas";
                                ta.FECHA = DateTime.Now.ToString();
                                ta.ESTADO = "0";
                                ta.commitLine();
                                ta.actualizar();
                            }
                            catch { }
                        }
                        //el siguiente codigo es para guardar el numero de factura de la instruccion en HojaRuta de Aduanas
                        if (codDocumento.Trim() == "1")
                        {
                            try
                            {
                                //conectarAduanas();
                                HojaRutaBO hr = new HojaRutaBO(logAppAduanas);
                                hr.loadHojaRuta(txtInstruccion.Text.Trim());
                                if (hr.totalRegistros > 0)
                                {
                                    hr.NUMFACTURA = edValor.Text;
                                    hr.actualizar();
                                }
                            }
                            catch { }
                        }
                    }
                    else
                    {
                        string codDocumento = (gridItem.Cells[2].Text);
                        di.loadDocumentosXInstrucciones(txtInstruccion.Text.Trim(), codDocumento);
                        if (di.totalRegistros == 1)
                        {
                            if (!String.IsNullOrEmpty(di.COPIA.Trim()))
                            {
                                //hacer consulta para inicializar el registro de documentos copia
                                int a = di.inicializarDocumentoCopia(txtInstruccion.Text.Trim(), codDocumento);
                            }
                        }
                    }

                    if (ckbDocumentoOriginal.Checked == false & ckbDocumentoCopia.Checked == false)
                    {
                        string codDocumento = (gridItem.Cells[2].Text);
                        di.loadDocumentosXInstrucciones(txtInstruccion.Text.Trim(), codDocumento);
                        if (di.totalRegistros == 1)
                        {
                            di.ELIMINADO = "1";
                            di.actualizar();
                        }
                    }
                }

                //guarda o actualiza algunos de los otros documentos originiales o copias
                foreach (GridDataItem gridItem in rgOtrosDocumentos.Items)
                {
                    //guarda o actualiza registros de los documentos originales chequeados
                    CheckBox ckbDocumentoOriginal = (CheckBox)gridItem.FindControl("ckbDocumentoOriginal");
                    CheckBox ckbDocumentoCopia = (CheckBox)gridItem.FindControl("ckbDocumentoCopia");
                    RadTextBox edValor = (RadTextBox)gridItem.FindControl("edValor");
                    RadTextBox edObservacion = (RadTextBox)gridItem.FindControl("edObservacion");
                    if (ckbDocumentoOriginal.Checked == true)
                    {
                        string codDocumento = (gridItem.Cells[2].Text);
                        di.loadDocumentosXInstrucciones(txtInstruccion.Text.Trim(), codDocumento);
                        if (di.totalRegistros == 1)
                        {
                            if (di.ORIGINAL != "X")
                            {
                                di.ORIGINAL = "X";
                                di.DOCUMENTONO = edValor.Text;
                                di.OBSERVACION = edObservacion.Text;
                                di.FECHAORIGINAL = DateTime.Now.ToString();
                                di.IDUSUARIOORIGINAL = Session["IdUsuario"].ToString();
                                di.REQUERIDO = "0";
                                di.actualizar();
                            }
                        }
                        else if (di.totalRegistros <= 0)
                        {
                            di.newLine();
                            di.IDINSTRUCCION = txtInstruccion.Text;
                            di.CODDOCUMENTO = codDocumento;
                            di.ORIGINAL = "X";
                            di.DOCUMENTONO = edValor.Text;
                            di.OBSERVACION = edObservacion.Text;
                            di.FECHAORIGINAL = DateTime.Now.ToString();
                            di.IDUSUARIOORIGINAL = Session["IdUsuario"].ToString();
                            di.ELIMINADO = "0";
                            di.REQUERIDO = "0";
                            di.commitLine();
                            di.actualizar();
                        }
                        //el siguiente codigo es para guardar el numero de factura de la instruccion en HojaRuta de Aduanas
                        if (codDocumento.Trim() == "1")
                        {
                            try
                            {
                                //conectarAduanas();
                                HojaRutaBO hr = new HojaRutaBO(logAppAduanas);
                                hr.loadHojaRuta(txtInstruccion.Text.Trim());
                                if (hr.totalRegistros > 0)
                                {
                                    hr.NUMFACTURA = edValor.Text;
                                    hr.actualizar();
                                }
                            }
                            catch { }
                        }
                    }
                    else
                    {
                        string codDocumento = (gridItem.Cells[2].Text);
                        di.loadDocumentosXInstrucciones(txtInstruccion.Text.Trim(), codDocumento);
                        if (di.totalRegistros == 1)
                        {
                            if (!String.IsNullOrEmpty(di.ORIGINAL.Trim()))
                            {
                                //hacer consulta para inicializar el registro de documentos originales
                                int a = di.inicializarDocumentoOriginal(txtInstruccion.Text.Trim(), codDocumento);
                            }
                        }
                    }

                    //guarda o actualiza registros de los documentos copias chequeados
                    ckbDocumentoCopia = (CheckBox)gridItem.FindControl("ckbDocumentoCopia");
                    if (ckbDocumentoCopia.Checked == true)
                    {
                        string codDocumento = (gridItem.Cells[2].Text);
                        di.loadDocumentosXInstrucciones(txtInstruccion.Text.Trim(), codDocumento);
                        if (di.totalRegistros == 1)
                        {
                            if (di.COPIA != "X")
                            {
                                di.COPIA = "X";
                                di.DOCUMENTONO = edValor.Text;
                                di.OBSERVACION = edObservacion.Text;
                                di.FECHACOPIA = DateTime.Now.ToString();
                                di.IDUSUARIOCOPIA = Session["IdUsuario"].ToString();
                                di.REQUERIDO = "0";
                                di.actualizar();
                            }
                        }
                        else if (di.totalRegistros <= 0)
                        {
                            di.newLine();
                            di.IDINSTRUCCION = txtInstruccion.Text;
                            di.CODDOCUMENTO = codDocumento;
                            di.COPIA = "X";
                            di.DOCUMENTONO = edValor.Text;
                            di.OBSERVACION = edObservacion.Text;
                            di.FECHACOPIA = DateTime.Now.ToString();
                            di.IDUSUARIOCOPIA = Session["IdUsuario"].ToString();
                            di.ELIMINADO = "0";
                            di.REQUERIDO = "0";
                            di.commitLine();
                            di.actualizar();
                        }
                        //el siguiente codigo es para guardar el numero de factura de la instruccion en HojaRuta de Aduanas
                        if (codDocumento.Trim() == "1")
                        {
                            try
                            {
                                //conectarAduanas();
                                HojaRutaBO hr = new HojaRutaBO(logAppAduanas);
                                hr.loadHojaRuta(txtInstruccion.Text.Trim());
                                if (hr.totalRegistros > 0)
                                {
                                    hr.NUMFACTURA = edValor.Text;
                                    hr.actualizar();
                                }
                            }
                            catch { }
                        }
                    }
                    else
                    {
                        string codDocumento = (gridItem.Cells[2].Text);
                        di.loadDocumentosXInstrucciones(txtInstruccion.Text.Trim(), codDocumento);
                        if (di.totalRegistros == 1)
                        {
                            if (!String.IsNullOrEmpty(di.COPIA.Trim()))
                            {
                                //hacer consulta para inicializar el registro de documentos copia
                                int a = di.inicializarDocumentoCopia(txtInstruccion.Text.Trim(), codDocumento);
                            }
                        }
                    }

                    if (ckbDocumentoOriginal.Checked == false & ckbDocumentoCopia.Checked == false)
                    {
                        string codDocumento = (gridItem.Cells[2].Text);
                        di.loadDocumentosXInstrucciones(txtInstruccion.Text.Trim(), codDocumento);
                        if (di.totalRegistros == 1)
                        {
                            di.ELIMINADO = "1";
                            di.actualizar();
                        }
                    }
                }

                //guarda o actualiza algunos de los permisos 
                foreach (GridDataItem gridItem in rgPermisos.Items)
                {
                    //guarda o actualiza registros de los permisos
                    CheckBox ckbPermiso = (CheckBox)gridItem.FindControl("ckbPermiso");
                    RadTextBox permisoNo = (RadTextBox)gridItem.FindControl("edPermisoNo");
                    RadTextBox observacion = (RadTextBox)gridItem.FindControl("edObservacion");
                    if (ckbPermiso.Checked == true & permisoNo.Enabled == true)
                    {
                        string codPermiso = (gridItem.Cells[2].Text);
                        pi.loadPermisosInstrucciones(txtInstruccion.Text.Trim(), codPermiso);
                        if (pi.totalRegistros == 1)
                        {
                            pi.NECESARIO = "1";
                            pi.PERMISONO = permisoNo.Text.Trim();
                            pi.OBSERVACION = observacion.Text.Trim();
                            pi.FECHA = DateTime.Now.ToString();
                            pi.IDUSUARIO = Session["IdUsuario"].ToString();
                            pi.actualizar();
                        }
                        else if (di.totalRegistros <= 0)
                        {
                            pi.newLine();
                            pi.IDINSTRUCCION = txtInstruccion.Text;
                            pi.CODPERMISO = codPermiso;
                            pi.PERMISONO = permisoNo.Text.Trim();
                            pi.NECESARIO = "1";
                            pi.OBSERVACION = observacion.Text.Trim();
                            pi.FECHA = DateTime.Now.ToString();
                            pi.ELIMINADO = "0";
                            pi.IDUSUARIO = Session["IdUsuario"].ToString();
                            pi.commitLine();
                            pi.actualizar();
                        }
                    }
                    else if (ckbPermiso.Checked == false & observacion.Enabled == true)
                    {
                        string codPermiso = (gridItem.Cells[2].Text);
                        pi.loadPermisosInstrucciones(txtInstruccion.Text.Trim(), codPermiso);
                        if (pi.totalRegistros == 1)
                        {
                            //hacer consulta para inicializar el registro de permisos
                            int a = pi.inicializarPermiso(txtInstruccion.Text.Trim(), codPermiso);
                        }
                    }
                }

                //guarda o actualiza algunos de las especies fiscales 
                foreach (GridDataItem gridItem in rgEspeciesFiscales.Items)
                {
                    CheckBox ckbCliente = (CheckBox)gridItem.FindControl("ckbCliente");
                    RadTextBox edSerieEF = (RadTextBox)gridItem.FindControl("edSerieEF");
                    RadTextBox observacion = (RadTextBox)gridItem.FindControl("edObservacion");
                    if (ckbCliente.Checked == true & !String.IsNullOrEmpty(edSerieEF.Text.Trim()) & edSerieEF.Enabled == true)
                    {
                        string codEspecieFiscal = gridItem.Cells[2].Text;
                        //guarda registro de las especies fiscales por instruccion
                        efi.loadEspeciesFiscalesInstruccionesXInstruccion("-1");
                        efi.newLine();
                        efi.IDINSTRUCCION = idInstruccion.Value;
                        efi.CODESPECIEFISCAL = codEspecieFiscal;
                        //efi.NECESARIO = "X";
                        efi.CLIENTE = "X";
                        efi.SERIE = edSerieEF.Text;
                        efi.OBSERVACION = observacion.Text;
                        efi.FECHA = DateTime.Now.ToString();
                        efi.ELIMINADO = "0";
                        efi.IDUSUARIO = Session["IdUsuario"].ToString();
                        //efi.IDMOVIMIENTO = mef.IDMOVIMIENTO;
                        efi.CARGA = "0";
                        efi.commitLine();
                        efi.actualizar();

                        //guarda valor de la serie 
                        if (codEspecieFiscal == "F")
                        {
                            try
                            {
                                //conectarAduanas();
                                ComplementoRecepcionBO cr = new ComplementoRecepcionBO(logAppAduanas);
                                cr.loadComplementoRecepcion(idInstruccion.Value);
                                if (cr.totalRegistros > 0)
                                {
                                    cr.VALORSERIE = edSerieEF.Text;
                                    cr.actualizar();
                                }
                            }
                            catch { }
                            finally
                            {
                                //desconectarAduanas();
                            }
                        }
                        /////
                    }
                }
                #endregion

                registrarMensaje("Documentos y/o permisos registrados exitosamente");
                mpInstrucciones.SelectedIndex = 0;
                llenarBitacora("Documentos y/o permisos registrados exitosamente", Session["IdUsuario"].ToString(), txtInstruccion.Text);
                limpiarControles();
                rgDocumentos.Rebind();
                rgOtrosDocumentos.Rebind();
            }
            else
                registrarMensaje("Instrucción no valida");
        }
        catch (Exception)
        { }
        finally
        {
            desconectar();
            desconectarAduanas();
        }
    }

    protected void btnBack_Click(object sender, ImageClickEventArgs e)
    {
        mpInstrucciones.SelectedIndex = 0;
    }

    private void limpiarControles()
    {
        txtNoManifiesto.Text = "";
        txtEmpresa.Text = "";
        txtMotorista.Text = "";
        txtPlaca.Text = "";
        txtFurgon.Text = "";
        txtCabezal.Text = "";
    }
    #endregion

    #region permisos
    protected void rgPermisos_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenarGridPermisos();
    }

    private void llenarGridPermisos()
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            CodigosBO c = new CodigosBO(logApp);
            i.loadInstruccionGD(idInstruccion.Value.Trim());
            c.loadAllCampos("PAISES", i.CODPAISHOJARUTA);
            c.loadAllCampos("PERMISOS" + c.DESCRIPCION);
            rgPermisos.DataSource = c.TABLA;
            rgPermisos.DataBind();
        }
        catch (Exception)
        { }
    }

    //public RadTextBox edPermisoNo;
    //public CheckBox ckbPermiso;
    //public RadTextBox edObservacion;
    protected void rgPermisos_ItemCreated(object sender, GridItemEventArgs e)
    {
        try
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem editForm = e.Item as GridDataItem;
                RadTextBox edPermisoNo = editForm.FindControl("edPermisoNo") as RadTextBox;
                edPermisoNo.TextChanged += new EventHandler(edPermisoNo_TextChanged);
            }
        }
        catch { }
    }

    void edPermisoNo_TextChanged(object sender, EventArgs e)
    {
        try
        {
            GridDataItem editForm = (sender as RadTextBox).NamingContainer as GridDataItem;
            RadTextBox edPermisoNo = editForm.FindControl("edPermisoNo") as RadTextBox;
            CheckBox ckbPermiso = editForm.FindControl("ckbPermiso") as CheckBox;
            RadTextBox edObservacion = editForm.FindControl("edObservacion") as RadTextBox;
            if (edPermisoNo.Text.Trim().Length > 0)
            {
                ckbPermiso.Checked = true;
                edObservacion.Enabled = false;
            }
            else
            {
                ckbPermiso.Checked = false;
                edObservacion.Text = "";
                edObservacion.Enabled = true;
            }
        }
        catch (Exception)
        { }
    }

    private void llenarCheckBoxPermisos()
    {
        try
        {
            conectar();
            PermisosInstruccionesBO pi = new PermisosInstruccionesBO(logApp);
            pi.loadPermisosInstruccionesXInstruccion(idInstruccion.Value.Trim());
            if (pi.totalRegistros > 0)
            {
                for (int i = 0; i < pi.totalRegistros; i++)
                {
                    //llena los checkbox en el grid Permisos
                    foreach (GridDataItem gridItem in rgPermisos.Items)
                    {
                        string[] arreglo = new string[1];
                        arreglo[0] = gridItem.Cells[2].Text;
                        CheckBox ckbPermiso = (CheckBox)gridItem.FindControl("ckbPermiso");
                        RadTextBox permisoNo = (RadTextBox)gridItem.FindControl("edPermisoNo");
                        RadTextBox observacion = (RadTextBox)gridItem.FindControl("edObservacion");
                        foreach (string itemGrid in arreglo)
                        {
                            if (itemGrid.ToString() == pi.CODPERMISO)
                            {
                                if (pi.NECESARIO == "1")
                                {
                                    ckbPermiso.Checked = true;
                                    permisoNo.Text = pi.PERMISONO;
                                    observacion.Text = pi.OBSERVACION;
                                }
                                else
                                    ckbPermiso.Checked = false;
                                if (User.IsInRole("Administradores"))
                                {
                                    permisoNo.Enabled = true;
                                    observacion.Enabled = true;
                                }
                                else
                                {
                                    permisoNo.Enabled = false;
                                    observacion.Enabled = false;
                                }
                            }
                        }
                    }
                    pi.regSiguiente();
                }
            }
        }
        catch (Exception)
        { }
    }
    #endregion

    #region Especies Fiscales
    protected void rgEspeciesFiscales_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenarGridEspeciesFiscales();
    }

    private void llenarGridEspeciesFiscales()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            if (User.IsInRole("Administradores") || User.Identity.Name == "davila" || User.Identity.Name == "eizaguirre" || User.Identity.Name == "spolanco")
            {
                c.loadAllCampos("ESPECIESFISCALES");
            }
            else
            {
                c.loadEspeciesnoAdmin();
            }
            rgEspeciesFiscales.DataSource = c.TABLA;
            rgEspeciesFiscales.DataBind();
        }
        catch (Exception)
        { }
    }

    protected void rgEspeciesFiscales_ItemCreated(object sender, GridItemEventArgs e)
    {
        try
        {
            if (e.Item is GridDataItem)
            {
                GridDataItem editForm = e.Item as GridDataItem;
                CheckBox ckbEspecieFiscal = editForm.FindControl("ckbEspecieFiscal") as CheckBox;
                CheckBox ckbCliente = editForm.FindControl("ckbCliente") as CheckBox;
                ckbEspecieFiscal.CheckedChanged += new EventHandler(ckbEspecieFiscal_CheckedChanged);
                ckbCliente.CheckedChanged += new EventHandler(ckbCliente_CheckedChanged);
            }
        }
        catch { }
    }

    void ckbEspecieFiscal_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            bool Es_Adicion = false;
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            MovimientosEspeciesFiscalesBO mef = new MovimientosEspeciesFiscalesBO(logApp);
            MovimientosEspeciesFiscalesBO mef2 = new MovimientosEspeciesFiscalesBO(logApp);
            MovimientosEspeciesFiscalesBO mefe = new MovimientosEspeciesFiscalesBO(logApp);
            EspeciesFiscalesInstruccionesBO efi = new EspeciesFiscalesInstruccionesBO(logApp);
            EspeciesFiscalesInstruccionesBO efi1 = new EspeciesFiscalesInstruccionesBO(logApp);
            EspeciesFiscalesClientesBO efc = new EspeciesFiscalesClientesBO(logApp);
            EspeciesFiscalesInstruccionesBO efi2 = new EspeciesFiscalesInstruccionesBO(logApp);

            foreach (GridDataItem gridItem in rgEspeciesFiscales.Items)
            {
                CheckBox ckbEspecieFiscal = gridItem.FindControl("ckbEspecieFiscal") as CheckBox;
                CheckBox ckbCliente = gridItem.FindControl("ckbCliente") as CheckBox;
                RadTextBox edObservacionEF = gridItem.FindControl("edObservacion") as RadTextBox;
                RadTextBox serieEF = (RadTextBox)gridItem.FindControl("edSerieEF");
                RadNumericTextBox edCantidad = (RadNumericTextBox)gridItem.FindControl("edCantidad");
                string codigo = gridItem.Cells[2].Text;
                string especieFiscal = gridItem.Cells[3].Text;
                if (ckbEspecieFiscal.Checked == true && serieEF.Text != "" && especieFiscal.ToString() == "Sellos de Seguridad Bimbo")
                {
                    asignarsellobimbo(idInstruccion.Value, serieEF.Text, edObservacionEF.Text);
                }
                if (ckbEspecieFiscal.Checked == true & String.IsNullOrEmpty(serieEF.Text.Trim()) & ckbCliente.Checked == false)
                {
                    i.loadInstruccionGD(idInstruccion.Value);
                    //////// codigo para validar que se rebajen especies fiscales de estos almacenes
                    #region Determinacion de Aduanas
                    string codAduana = "";
                    if (i.CODPAISHOJARUTA == "H" & User.Identity.Name == "davila" & (codigo == "F" || codigo == "F2"))
                        codAduana = "DEYSI";
                    else if (i.CODPAISHOJARUTA == "H" & User.Identity.Name == "eizaguirre" & (codigo == "DVA" || codigo == "P" || codigo == "DVA2"))
                        codAduana = "UNO";
                    else if (i.CODPAISHOJARUTA == "H" & User.Identity.Name == "spolanco" & (codigo == "F" || codigo == "F2"))
                        codAduana = "SAMAN";
                    else if (i.CODPAISHOJARUTA == "H" & User.Identity.Name == "jgarcia" & codigo == "F")
                        codAduana = "JOHNY";
                    else if (i.CODPAISHOJARUTA == "H" & User.Identity.Name == "ncontreras" & codigo == "F")
                        codAduana = "NADIA";
                    else if (i.CODPAISHOJARUTA == "H" & User.Identity.Name == "afonseca" & codigo == "P")
                        codAduana = "NADIA";
                    else if (i.CODPAISHOJARUTA == "H" & User.Identity.Name == "cmorales" & (codigo == "MI" || codigo == "DTI"))
                        codAduana = "CARLO";
                    else if (i.CODPAISHOJARUTA == "G")
                        codAduana = "ALMAC";
                    else
                        codAduana = i.CODIGOADUANA;

                    #endregion
                    //codAduana = "17";
                    ////////

                    /////evitar duplicados en la asignacion de especies fiscales
                    efi2.loadEspeciesFiscalesInstruccionesInstrucciones(idInstruccion.Value, codigo);
                    #region Buscar Registros
                    if (efi2.totalRegistros <= 0)
                    {
                        /////
                        #region Determinacion de especies
                        Session["temp"] = "";
                        if (codigo == "SS2" && ckbEspecieFiscal.Checked == true && ckbCliente.Checked == false)
                        {
                            Es_Adicion = true;
                            Session["temp"] = codigo.ToString();
                            codigo = "SS";
                        }
                        if (codigo == "DA2" && ckbEspecieFiscal.Checked == true && ckbCliente.Checked == false)
                        {
                            Es_Adicion = true;
                            Session["temp"] = codigo.ToString();
                            codigo = "DA";
                        }

                        if (codigo == "DVA2" && ckbEspecieFiscal.Checked == true && ckbCliente.Checked == false)
                        {
                            Es_Adicion = true;
                            Session["temp"] = codigo.ToString();
                            codigo = "DVA";
                        }

                        if (codigo == "F2" && ckbEspecieFiscal.Checked == true && ckbCliente.Checked == false)
                        {
                            Es_Adicion = true;
                            Session["temp"] = codigo.ToString();
                            codigo = "F";
                        }
                        if (codigo == "HN-MN2" && ckbEspecieFiscal.Checked == true && ckbCliente.Checked == false)
                        {
                            Es_Adicion = true;
                            Session["temp"] = codigo.ToString();
                            codigo = "HN-MN";
                        }
                        #endregion
                        #region DVA
                        if (codigo == "DVA")
                        {
                            //si la especie fiscal es DVA verificar cuantas solicita y si estan disponibles
                            int cantidadTotal = 0, cantidadDisponible = 0, cantidadSolicitada = int.Parse(edCantidad.Text);
                            mef.loadEFXAduanaClienteALL(codAduana, codigo, i.IDCLIENTE);    //cargar todas
                            if (mef.totalRegistros > 0)
                            {
                                for (int j = 0; j < mef.totalRegistros; j++)
                                {
                                    cantidadTotal += mef.CANTIDAD;
                                    mef.regSiguiente();
                                }
                                if (cantidadTotal >= cantidadSolicitada)
                                {
                                    DateTime fecha = DateTime.Now;      //20120109.171835.761    
                                    string series = "", item = "";
                                    string idMovimiento = fecha.Year.ToString() + fecha.Month.ToString("00") + fecha.Day.ToString("00") + "." + fecha.Hour.ToString("00") + fecha.Minute.ToString("00") + fecha.Second.ToString("00") + "." + DateTime.Now.Millisecond.ToString("000");
                                    mef.loadEFXAduanaClienteALL(codAduana, codigo, i.IDCLIENTE);    //cargar todas
                                    for (int j = 0; j < mef.totalRegistros; j++)
                                    {
                                        item = mef.ITEM;
                                        cantidadDisponible += mef.CANTIDAD;
                                        if (cantidadDisponible > cantidadSolicitada)  //txtCantidad.Value
                                        {
                                            //guardar nuevo registro con las series que quedan
                                            mefe.loadMovimientosEspeciesFiscales("-1");
                                            mefe.newLine();
                                            mefe.IDMOVIMIENTO = idMovimiento;
                                            mefe.IDESPECIEFISCAL = codigo;
                                            mefe.CODPAIS = mef.CODPAIS;
                                            mefe.CODIGOADUANA = mef.CODIGOADUANA;
                                            mefe.RANGOINICIAL = (int.Parse(mef.RANGOINICIAL) + cantidadSolicitada).ToString();
                                            mefe.RANGOFINAL = mef.RANGOFINAL;
                                            mefe.CANTIDAD = mef.CANTIDAD - cantidadSolicitada;
                                            mefe.FECHA = DateTime.Now.ToString();
                                            mefe.CODTIPOMOVIMIENTO = mef.CODTIPOMOVIMIENTO;   //Usada
                                            mefe.CODESTADO = mef.CODESTADO;
                                            mefe.IDUSUARIO = Session["IdUsuario"].ToString();
                                            mefe.IDCOMPRA = mef.IDCOMPRA;
                                            mefe.commitLine();
                                            mefe.actualizar();

                                            efc.loadEspeciesFiscalesClientesXIdMov("-1");
                                            efc.newLine();
                                            efc.IDMOVIMIENTO = idMovimiento;
                                            efc.IDCLIENTE = i.IDCLIENTE;
                                            efc.OBSERVACION = edObservacionEF.Text;
                                            efc.commitLine();
                                            efc.actualizar();
                                        }
                                        //DateTime fecha1 = DateTime.Now;
                                        //string idMovimientoEnvio = fecha1.Year.ToString() + fecha1.Month.ToString("00") + fecha1.Day.ToString("00") + "." + fecha1.Hour.ToString("00") + fecha1.Minute.ToString("00") + fecha1.Second.ToString("00") + "." + fecha1.Millisecond.ToString("000");
                                        if (cantidadDisponible >= cantidadSolicitada)
                                        {
                                            //cambia el estado a eliminado
                                            mef2.loadMovimientosEspeciesFiscalesItem(item);
                                            if (mef.totalRegistros > 0)
                                            {
                                                mef2.CODESTADO = "100";
                                                mef2.actualizar();
                                            }

                                            //Almaceno las series que se van a utilizar                                    
                                            for (int h = 0; h < cantidadSolicitada; h++)
                                            {
                                                series += (Int64.Parse(mef.RANGOINICIAL) + h) + ",";
                                            }
                                            //series = series.Substring(0, series.Trim().Length - 1);
                                            serieEF.Text = series.Substring(0, series.Trim().Length - 1);
                                            ckbEspecieFiscal.Enabled = false;
                                            edObservacionEF.Enabled = false;
                                            ckbCliente.Enabled = false;
                                            edCantidad.Enabled = false;
                                            //guarda registro de las especies fiscales por instruccion
                                            if (Session["temp"].ToString() == "DVA2")
                                            {
                                                efi1.CargarEspeciesxInstruccionParaEliminar("DVA", idInstruccion.Value);
                                                if (efi1.totalRegistros == 1)
                                                {
                                                    //efi1.CargarEspeciesxInstruccionParaEliminar("SS", idInstruccion.Value);
                                                    //string hoja = efi1.IDINSTRUCCION;
                                                    efi1.SERIE = (efi1.SERIE + "," + serieEF.Text);
                                                    if (edObservacionEF.Text != "")
                                                    {
                                                        efi1.OBSERVACION = (efi1.OBSERVACION + " | " + edObservacionEF.Text);
                                                    }
                                                    efi1.IDMOVIMIENTO = (efi1.IDMOVIMIENTO + "," + mef.IDMOVIMIENTO);
                                                    efi1.commitLine();
                                                    efi1.actualizar();
                                                }
                                            }
                                            else
                                            {
                                                efi.loadEspeciesFiscalesInstruccionesXInstruccion("-1");
                                                efi.newLine();
                                                efi.IDINSTRUCCION = idInstruccion.Value;
                                                efi.CODESPECIEFISCAL = codigo;
                                                efi.NECESARIO = "X";
                                                efi.SERIE = serieEF.Text;
                                                efi.OBSERVACION = edObservacionEF.Text;
                                                efi.FECHA = DateTime.Now.ToString();
                                                efi.ELIMINADO = "0";
                                                efi.IDUSUARIO = Session["IdUsuario"].ToString();
                                                efi.IDMOVIMIENTO = mef.IDMOVIMIENTO;
                                                efi.CARGA = "0";
                                                efi.commitLine();
                                                efi.actualizar();
                                            }
                                            break;
                                        }
                                        else
                                        {
                                            //cambia el estado a eliminado
                                            mef2.loadMovimientosEspeciesFiscalesItem(item);
                                            if (mef.totalRegistros > 0)
                                            {
                                                mef2.CODESTADO = "100";
                                                mef2.actualizar();
                                            }

                                            //Almaceno las series que se van a utilizar                                    
                                            for (int h = 0; h < mef.CANTIDAD; h++)
                                            {
                                                series += (Int64.Parse(mef.RANGOINICIAL) + h) + ",";
                                            }

                                            cantidadSolicitada -= mef.CANTIDAD;
                                            cantidadDisponible -= mef.CANTIDAD;
                                            mef.regSiguiente();
                                        }
                                    }   //fin del for

                                    registrarMensaje(especieFiscal + " No. " + serieEF.Text + " asignada(s) a la instrucción " + idInstruccion.Value + " exitosamente");
                                    //    return;
                                    //llenarBitacora("Se ingresó la compra de " + cmbEspecieFiscal.Text + " con un rango del " + cef.RANGOINICIAL + " al " + cef.RANGOFINAL, cef.IDUSUARIO, nuevoId);
                                }
                                else
                                {
                                    registrarMensaje("La cantidad solicitada no esta disponible, solo existen " + cantidadTotal + " series de esta especie fiscal");
                                    ckbEspecieFiscal.Checked = false;
                                    edObservacionEF.Text = "";
                                }
                            }
                            else
                            {
                                ckbEspecieFiscal.Checked = false;
                                edObservacionEF.Text = "";
                                registrarMensaje("Este cliente no tiene series para esta especie fiscal");
                            }
                        }
                        #endregion
                        #region Faucas
                        else if (codigo == "F")
                        {
                            string item = "";
                            mef.loadRangoInicialEFXAduanaClienteNew(codAduana, codigo, i.IDCLIENTE);
                            if (mef.totalRegistros > 0)
                            {

                                serieEF.Text = mef.RANGOINICIAL;
                                item = mef.ITEM;
                                ckbEspecieFiscal.Enabled = false;
                                edObservacionEF.Enabled = false;
                                ckbCliente.Enabled = false;

                                if (Session["temp"].ToString() != "F2")
                                {
                                    //guarda registro de las especies fiscales por instruccion
                                    efi.loadEspeciesFiscalesInstruccionesXInstruccion("-1");
                                    efi.newLine();
                                    efi.IDINSTRUCCION = idInstruccion.Value;
                                    efi.CODESPECIEFISCAL = codigo;
                                    efi.NECESARIO = "X";
                                    efi.SERIE = serieEF.Text;
                                    efi.OBSERVACION = edObservacionEF.Text;
                                    efi.FECHA = DateTime.Now.ToString();
                                    efi.ELIMINADO = "0";
                                    efi.IDUSUARIO = Session["IdUsuario"].ToString();
                                    efi.IDMOVIMIENTO = mef.IDMOVIMIENTO;
                                    efi.CARGA = "0";
                                    efi.commitLine();
                                    efi.actualizar();

                                    //guarda valor de la serie 
                                    if (codigo == "F")
                                    {
                                        try
                                        {
                                            conectarAduanas();
                                            ComplementoRecepcionBO cr = new ComplementoRecepcionBO(logAppAduanas);
                                            cr.loadComplementoRecepcion(idInstruccion.Value);
                                            if (cr.totalRegistros > 0)
                                            {
                                                cr.VALORSERIE = serieEF.Text;
                                                cr.actualizar();
                                            }
                                        }
                                        catch { }
                                        finally
                                        {
                                            desconectarAduanas();
                                        }
                                    }
                                }

                                else
                                {
                                    efi1.CargarEspeciesxInstruccionParaEliminar("F", idInstruccion.Value);
                                    if (efi1.totalRegistros == 1)
                                    {
                                        //efi1.CargarEspeciesxInstruccionParaEliminar("SS", idInstruccion.Value);
                                        //string hoja = efi1.IDINSTRUCCION;
                                        efi1.SERIE = (efi1.SERIE + "," + serieEF.Text);
                                        if (edObservacionEF.Text != "")
                                        {
                                            efi1.OBSERVACION = (efi1.OBSERVACION + " | " + edObservacionEF.Text);
                                        }
                                        efi1.IDMOVIMIENTO = (efi1.IDMOVIMIENTO + "," + mef.IDMOVIMIENTO);
                                        efi1.commitLine();
                                        efi1.actualizar();
                                    }
                                }

                                /////

                                if ((mef.CANTIDAD - 1) > 0)
                                {
                                    DateTime fecha = DateTime.Now;     //20120109.171835.761                                            
                                    string idMovimiento = fecha.Year.ToString() + fecha.Month.ToString("00") + fecha.Day.ToString("00") + "." + fecha.Hour.ToString("00") + fecha.Minute.ToString("00") + fecha.Second.ToString("00") + "." + DateTime.Now.Millisecond.ToString("000");
                                    //guardar nuevo registro con las series que quedan
                                    mefe.loadMovimientosEspeciesFiscales("-1");
                                    mefe.newLine();
                                    mefe.IDMOVIMIENTO = idMovimiento;
                                    if (Session["temp"].ToString() != "F2")
                                    {
                                        mefe.IDESPECIEFISCAL = efi.CODESPECIEFISCAL;
                                    }
                                    else
                                    {
                                        mefe.IDESPECIEFISCAL = "F";
                                    }
                                    mefe.CODPAIS = mef.CODPAIS;
                                    mefe.CODIGOADUANA = mef.CODIGOADUANA;
                                    mefe.RANGOINICIAL = (int.Parse(mef.RANGOINICIAL) + 1).ToString();
                                    mefe.RANGOFINAL = mef.RANGOFINAL;
                                    mefe.CANTIDAD = mef.CANTIDAD - 1;
                                    mefe.FECHA = DateTime.Now.ToString();
                                    mefe.CODTIPOMOVIMIENTO = mef.CODTIPOMOVIMIENTO;   //Usada
                                    mefe.CODESTADO = mef.CODESTADO;
                                    mefe.IDUSUARIO = Session["IdUsuario"].ToString();
                                    mefe.IDCOMPRA = mef.IDCOMPRA;
                                    mefe.commitLine();
                                    mefe.actualizar();

                                    efc.loadEspeciesFiscalesClientesXIdMov("-1");
                                    efc.newLine();
                                    efc.IDMOVIMIENTO = idMovimiento;
                                    efc.IDCLIENTE = i.IDCLIENTE;
                                    efc.OBSERVACION = edObservacionEF.Text;
                                    efc.commitLine();
                                    efc.actualizar();
                                }

                                //cambiar a estado eliminado el rango anterior
                                mef.loadMovimientosEspeciesFiscalesItem(item);
                                mef.CODESTADO = "100";
                                mef.actualizar();

                                registrarMensaje(especieFiscal + " No. " + serieEF.Text + " asignada a la instrucción " + idInstruccion.Value + " exitosamente");
                                //  return;
                            }
                            else
                            {
                                ckbEspecieFiscal.Checked = false;
                                edObservacionEF.Text = "";
                                registrarMensaje("Este cliente no tiene series para esta especie fiscal");
                            }
                        }
                        #endregion
                        #region Da HH-MN SS
                        else if (codigo == "DA" || codigo == "HN-MN" || codigo == "SS")
                        {

                            //si la especie fiscal es DVA verificar cuantas solicita y si estan disponibles
                            int cantidadTotal = 0, cantidadDisponible = 0, cantidadSolicitada = int.Parse(edCantidad.Text);
                            mef.loadEFXAduanaALL(codAduana, codigo);
                            if (mef.totalRegistros > 0)
                            {
                                for (int j = 0; j < mef.totalRegistros; j++)
                                {
                                    cantidadTotal += mef.CANTIDAD;
                                    mef.regSiguiente();
                                }
                                if (cantidadTotal >= cantidadSolicitada)
                                {
                                    DateTime fecha = DateTime.Now;      //20120109.171835.761    
                                    string series = "", item = "";
                                    string idMovimiento = fecha.Year.ToString() + fecha.Month.ToString("00") + fecha.Day.ToString("00") + "." + fecha.Hour.ToString("00") + fecha.Minute.ToString("00") + fecha.Second.ToString("00") + "." + DateTime.Now.Millisecond.ToString("000");
                                    mef.loadEFXAduanaALL(codAduana, codigo);
                                    for (int j = 0; j < mef.totalRegistros; j++)
                                    {
                                        item = mef.ITEM;
                                        cantidadDisponible += mef.CANTIDAD;
                                        if (cantidadDisponible > cantidadSolicitada)  //txtCantidad.Value
                                        {
                                            //guardar nuevo registro con las series que quedan
                                            mefe.loadMovimientosEspeciesFiscales("-1");
                                            mefe.newLine();
                                            mefe.IDMOVIMIENTO = idMovimiento;
                                            mefe.IDESPECIEFISCAL = codigo;
                                            mefe.CODPAIS = mef.CODPAIS;
                                            mefe.CODIGOADUANA = mef.CODIGOADUANA;
                                            mefe.RANGOINICIAL = (int.Parse(mef.RANGOINICIAL) + cantidadSolicitada).ToString();
                                            mefe.RANGOFINAL = mef.RANGOFINAL;
                                            mefe.CANTIDAD = mef.CANTIDAD - cantidadSolicitada;
                                            mefe.FECHA = DateTime.Now.ToString();
                                            mefe.CODTIPOMOVIMIENTO = mef.CODTIPOMOVIMIENTO;   //Usada
                                            mefe.CODESTADO = mef.CODESTADO;
                                            mefe.IDUSUARIO = Session["IdUsuario"].ToString();
                                            mefe.IDCOMPRA = mef.IDCOMPRA;
                                            mefe.commitLine();
                                            mefe.actualizar();

                                            efc.loadEspeciesFiscalesClientesXIdMov("-1");
                                            efc.newLine();
                                            efc.IDMOVIMIENTO = idMovimiento;
                                            efc.IDCLIENTE = i.IDCLIENTE;
                                            efc.OBSERVACION = edObservacionEF.Text;
                                            efc.commitLine();
                                            efc.actualizar();
                                        }
                                        //DateTime fecha1 = DateTime.Now;
                                        //string idMovimientoEnvio = fecha1.Year.ToString() + fecha1.Month.ToString("00") + fecha1.Day.ToString("00") + "." + fecha1.Hour.ToString("00") + fecha1.Minute.ToString("00") + fecha1.Second.ToString("00") + "." + fecha1.Millisecond.ToString("000");
                                        if (cantidadDisponible >= cantidadSolicitada)
                                        {
                                            //cambia el estado a eliminado
                                            mef2.loadMovimientosEspeciesFiscalesItem(item);
                                            if (mef.totalRegistros > 0)
                                            {
                                                mef2.CODESTADO = "100";
                                                mef2.actualizar();
                                            }

                                            //Almaceno las series que se van a utilizar                                    
                                            for (int h = 0; h < cantidadSolicitada; h++)
                                            {
                                                series += (Int64.Parse(mef.RANGOINICIAL) + h) + ",";
                                            }
                                            //series = series.Substring(0, series.Trim().Length - 1);
                                            serieEF.Text = series.Substring(0, series.Trim().Length - 1);
                                            ckbEspecieFiscal.Enabled = false;
                                            edObservacionEF.Enabled = false;
                                            ckbCliente.Enabled = false;
                                            edCantidad.Enabled = false;

                                            if (Session["temp"].ToString() == "SS2")
                                            {
                                                efi1.CargarEspeciesxInstruccionParaEliminar("SS", idInstruccion.Value);
                                                if (efi1.totalRegistros == 1)
                                                {
                                                    //efi1.CargarEspeciesxInstruccionParaEliminar("SS", idInstruccion.Value);
                                                    //string hoja = efi1.IDINSTRUCCION;
                                                    efi1.SERIE = (efi1.SERIE + "," + serieEF.Text);
                                                    if (edObservacionEF.Text != "")
                                                    {
                                                        efi1.OBSERVACION = (efi1.OBSERVACION + " | " + edObservacionEF.Text);
                                                    }
                                                    efi1.IDMOVIMIENTO = (efi1.IDMOVIMIENTO + "," + mef.IDMOVIMIENTO);
                                                    efi1.commitLine();
                                                    efi1.actualizar();
                                                }
                                            }
                                            if (Session["temp"].ToString() == "DA2")
                                            {
                                                efi1.CargarEspeciesxInstruccionParaEliminar("DA", idInstruccion.Value);
                                                if (efi1.totalRegistros == 1)
                                                {
                                                    //efi1.CargarEspeciesxInstruccionParaEliminar("SS", idInstruccion.Value);
                                                    //string hoja = efi1.IDINSTRUCCION;
                                                    efi1.SERIE = (efi1.SERIE + "," + serieEF.Text);
                                                    if (edObservacionEF.Text != "")
                                                    {
                                                        efi1.OBSERVACION = (efi1.OBSERVACION + " | " + edObservacionEF.Text);
                                                    }
                                                    efi1.IDMOVIMIENTO = (efi1.IDMOVIMIENTO + "," + mef.IDMOVIMIENTO);
                                                    efi1.commitLine();
                                                    efi1.actualizar();
                                                }
                                            }
                                            else
                                            {
                                                //guarda registro de las especies fiscales por instruccion
                                                efi.loadEspeciesFiscalesInstruccionesXInstruccion("-1");
                                                efi.newLine();
                                                efi.IDINSTRUCCION = idInstruccion.Value;
                                                efi.CODESPECIEFISCAL = codigo;
                                                efi.NECESARIO = "X";
                                                efi.SERIE = serieEF.Text;
                                                efi.OBSERVACION = edObservacionEF.Text;
                                                efi.FECHA = DateTime.Now.ToString();
                                                efi.ELIMINADO = "0";
                                                efi.IDUSUARIO = Session["IdUsuario"].ToString();
                                                efi.IDMOVIMIENTO = mef.IDMOVIMIENTO;
                                                efi.CARGA = "0";
                                                efi.commitLine();
                                                efi.actualizar();
                                            }
                                            break;
                                        }
                                        else
                                        {
                                            //cambia el estado a eliminado
                                            mef2.loadMovimientosEspeciesFiscalesItem(item);
                                            if (mef.totalRegistros > 0)
                                            {
                                                mef2.CODESTADO = "100";
                                                mef2.actualizar();
                                            }

                                            //Almaceno las series que se van a utilizar                                    
                                            for (int h = 0; h < mef.CANTIDAD; h++)
                                            {
                                                series += (Int64.Parse(mef.RANGOINICIAL) + h) + ",";
                                            }

                                            cantidadSolicitada -= mef.CANTIDAD;
                                            cantidadDisponible -= mef.CANTIDAD;
                                            mef.regSiguiente();
                                        }
                                    }   //fin del for

                                    registrarMensaje(especieFiscal + " No. " + serieEF.Text + " asignada(s) a la instrucción " + idInstruccion.Value + " exitosamente");
                                    //     return;
                                    //llenarBitacora("Se ingresó la compra de " + cmbEspecieFiscal.Text + " con un rango del " + cef.RANGOINICIAL + " al " + cef.RANGOFINAL, cef.IDUSUARIO, nuevoId);
                                }
                                else
                                {
                                    registrarMensaje("La cantidad solicitada no esta disponible, solo existen " + cantidadTotal + " series de esta especie fiscal");
                                    ckbEspecieFiscal.Checked = false;
                                    edObservacionEF.Text = "";
                                }
                            }
                            else
                            {
                                ckbEspecieFiscal.Checked = false;
                                edObservacionEF.Text = "";
                                registrarMensaje("Este cliente no tiene series para esta especie fiscal");
                            }
                        }
                        #endregion
                        #region Buscqueda de Codigos
                        else
                        {
                            mef.loadRangoInicialEFXAduana(codAduana, codigo);
                            if (mef.totalRegistros > 0)
                            {
                                serieEF.Text = mef.RANGOINICIAL;
                                ckbEspecieFiscal.Enabled = false;
                                edObservacionEF.Enabled = false;
                                ckbCliente.Enabled = false;
                                //guarda registro de las especies fiscales por instruccion
                                efi.loadEspeciesFiscalesInstruccionesXInstruccion("-1");
                                efi.newLine();
                                efi.IDINSTRUCCION = idInstruccion.Value;
                                efi.CODESPECIEFISCAL = codigo;
                                efi.NECESARIO = "X";
                                efi.SERIE = serieEF.Text;
                                efi.OBSERVACION = edObservacionEF.Text;
                                efi.FECHA = DateTime.Now.ToString();
                                efi.ELIMINADO = "0";
                                efi.IDUSUARIO = Session["IdUsuario"].ToString();
                                efi.IDMOVIMIENTO = mef.IDMOVIMIENTO;
                                efi.CARGA = "0";
                                efi.commitLine();
                                efi.actualizar();

                                //guarda valor de la serie 
                                if (codigo == "P")
                                {
                                    try
                                    {
                                        conectarAduanas();
                                        ComplementoRecepcionBO cr = new ComplementoRecepcionBO(logAppAduanas);
                                        cr.loadComplementoRecepcion(idInstruccion.Value);
                                        if (cr.totalRegistros > 0)
                                        {
                                            cr.VALORSERIE = serieEF.Text;
                                            cr.actualizar();
                                        }
                                    }
                                    catch { }
                                    finally
                                    {
                                        desconectarAduanas();
                                    }
                                }
                                /////

                                if ((mef.CANTIDAD - 1) > 0)
                                {
                                    DateTime fecha = DateTime.Now;     //20120109.171835.761                                            
                                    string idMovimiento = fecha.Year.ToString() + fecha.Month.ToString("00") + fecha.Day.ToString("00") + "." + fecha.Hour.ToString("00") + fecha.Minute.ToString("00") + fecha.Second.ToString("00") + "." + DateTime.Now.Millisecond.ToString("000");
                                    //guardar nuevo registro con las series que quedan
                                    mefe.loadMovimientosEspeciesFiscales("-1");
                                    mefe.newLine();
                                    mefe.IDMOVIMIENTO = idMovimiento;
                                    mefe.IDESPECIEFISCAL = efi.CODESPECIEFISCAL;
                                    mefe.CODPAIS = mef.CODPAIS;
                                    mefe.CODIGOADUANA = mef.CODIGOADUANA;
                                    mefe.RANGOINICIAL = (int.Parse(mef.RANGOINICIAL) + 1).ToString();
                                    mefe.RANGOFINAL = mef.RANGOFINAL;
                                    mefe.CANTIDAD = mef.CANTIDAD - 1;
                                    mefe.FECHA = DateTime.Now.ToString();
                                    mefe.CODTIPOMOVIMIENTO = "3";   //Usada
                                    mefe.CODESTADO = mef.CODESTADO;
                                    mefe.IDUSUARIO = Session["IdUsuario"].ToString();
                                    mefe.IDCOMPRA = mef.IDCOMPRA;
                                    mefe.commitLine();
                                    mefe.actualizar();
                                }

                                //cambiar a estado eliminado el rango anterior
                                mef.CODESTADO = "100";
                                mef.actualizar();
                                registrarMensaje(especieFiscal + " No. " + serieEF.Text + " asignada a la instrucción " + idInstruccion.Value + " exitosamente");
                                //  return;
                            }
                            else if (codigo == "RT")
                            {
                                ckbEspecieFiscal.Enabled = false;
                                edObservacionEF.Enabled = false;
                                //guarda registro de las especies fiscales por instruccion
                                efi.loadEspeciesFiscalesInstruccionesXInstruccion("-1");
                                efi.newLine();
                                efi.IDINSTRUCCION = idInstruccion.Value;
                                efi.CODESPECIEFISCAL = codigo;
                                efi.NECESARIO = "X";
                                efi.SERIE = serieEF.Text;
                                efi.OBSERVACION = edObservacionEF.Text;
                                efi.FECHA = DateTime.Now.ToString();
                                efi.ELIMINADO = "0";
                                efi.IDUSUARIO = Session["IdUsuario"].ToString();
                                efi.IDMOVIMIENTO = "-1";
                                efi.CARGA = "0";
                                efi.commitLine();
                                efi.actualizar();

                                registrarMensaje("Recibo Talonario asignado a la instrucción " + idInstruccion.Value + " exitosamente");
                            }
                            else
                            {
                                ckbEspecieFiscal.Checked = false;
                                edObservacionEF.Text = "";
                                registrarMensaje("No hay series para esta especie fiscal");
                            }
                        }
                        #endregion/////aqui






                    }
                    #region Agragar a Correciones

                    if (Es_Adicion == true)
                    {

                        conectar();
                        CorreccionesBO Correccion = new CorreccionesBO((logApp));
                        Correccion.LoadCorrecionesXId("-1");
                        Correccion.newLine();
                        //Correccion.CODADUANA=                
                        Correccion.IDINSTRUCCION = idInstruccion.Value;
                        Correccion.CODESPECIE = codigo;
                        //Correccion.NECESARIO = "X";
                        Correccion.SERIE = serieEF.Text;
                        //Correccion. = edObservacionEF.Text;
                        Correccion.FECHARESOLVIO = DateTime.Now.ToString();
                        Correccion.FECHASOLICITUD = DateTime.Now.ToString();
                        if (efi1.CARGA == "0")
                        {
                            Correccion.ESTADO = "1";//No genera Carga porque Carga esta Pendiente
                        }
                        else
                        {
                            Correccion.ESTADO = "0";// Si genera Carga porque ya se realizo carga
                        }
                        Correccion.USUARIOSOLICITO = Session["IdUsuario"].ToString();
                        Correccion.USUARIORESOLVIO = Session["IdUsuario"].ToString();
                        Correccion.USUARIOASIGNOESPECIE = Session["IdUsuario"].ToString();
                        //Correccion.IDMOVIMIENTO = mef.IDMOVIMIENTO;
                        Correccion.PARTIDA = "3";
                        Correccion.commitLine();
                        Correccion.actualizar();
                        desconectar();

                    }



                    #endregion
                    else
                        registrarMensaje("Salga y vuelva a ingresar al módulo de gestión documentos");
                    #endregion
                }


            }



        }
        catch (Exception)
        { }
    }

    void ckbCliente_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            conectar();

            foreach (GridDataItem gridItem in rgEspeciesFiscales.Items)
            {
                CheckBox ckbCliente = gridItem.FindControl("ckbCliente") as CheckBox;
                CheckBox ckbEspecieFiscal = gridItem.FindControl("ckbEspecieFiscal") as CheckBox;
                RadTextBox serieEF = (RadTextBox)gridItem.FindControl("edSerieEF");
                RadTextBox observacion = (RadTextBox)gridItem.FindControl("edObservacion");
                string idEspecieFiscal = gridItem.Cells[2].Text;
                string especieFiscal = gridItem.Cells[3].Text;
                if (especieFiscal.ToString() == "Faucas" || especieFiscal.Contains("Marchamos") || especieFiscal.ToString() == "Poliza Zona Libre" || especieFiscal.ToString() == "D.V.A." || especieFiscal.ToString() == "Polizas" || especieFiscal.ToString() == "Sellos de Seguridad Bimbo" || especieFiscal.ToString() == "Sellos de Seguridad")
                {
                    if (ckbCliente.Checked == true & String.IsNullOrEmpty(serieEF.Text.Trim()))
                    {
                        ckbEspecieFiscal.Enabled = false;
                        serieEF.Enabled = true;
                    }
                    else if (ckbCliente.Checked == true & !String.IsNullOrEmpty(serieEF.Text.Trim()))
                    {
                        ckbEspecieFiscal.Enabled = false;
                        serieEF.Enabled = false;
                    }
                    else
                    {
                        ckbEspecieFiscal.Enabled = true;
                        serieEF.Enabled = false;
                        observacion.Text = "";
                        observacion.Enabled = true;
                        ////codigo para eliminar especie fiscal del cliente
                        //EspeciesFiscalesInstruccionesBO efi = new EspeciesFiscalesInstruccionesBO(logApp);
                        //efi.loadEspeciesFiscalesInstruccionesInstrucciones(idInstruccion.Value, idEspecieFiscal, serieEF.Text);
                        //if (efi.totalRegistros > 0)
                        //{
                        //    efi.ELIMINADO = "1";    //elimina especie fiscal de la instruccion
                        //    efi.actualizar();
                        //    registrarMensaje(especieFiscal + " del cliente eliminada exitosamente");

                        //    //guarda valor de la serie 
                        //    if (especieFiscal.Contains("Faucas"))
                        //    {
                        //        try
                        //        {
                        //            conectarAduanas();
                        //            ComplementoRecepcionBO cr = new ComplementoRecepcionBO(logAppAduanas);
                        //            cr.loadComplementoRecepcion(idInstruccion.Value);
                        //            if (cr.totalRegistros > 0)
                        //            {
                        //                cr.VALORSERIE = "";
                        //                cr.actualizar();
                        //            }
                        //        }
                        //        catch { }
                        //        finally
                        //        {
                        //            desconectarAduanas();
                        //        }
                        //    }
                        //    /////
                        //}
                        //serieEF.Text = "";
                    }
                }
            }
        }
        catch { }
    }

    private void asignarsellobimbo(string hoja, string Serie, string Observacion)
    {
        try
        {
            conectar();
            MovimientosEspeciesFiscalesBO mef = new MovimientosEspeciesFiscalesBO(logApp);
            MovimientosEspeciesFiscalesBO mef2 = new MovimientosEspeciesFiscalesBO(logApp);
            MovimientosEspeciesFiscalesBO mefe = new MovimientosEspeciesFiscalesBO(logApp);
            MovimientosEspeciesFiscalesBO mefe1 = new MovimientosEspeciesFiscalesBO(logApp);
            EspeciesFiscalesInstruccionesBO efi = new EspeciesFiscalesInstruccionesBO(logApp);
            EspeciesFiscalesInstruccionesBO efi1 = new EspeciesFiscalesInstruccionesBO(logApp);
            DateTime fecha = DateTime.Now;      //20120109.171835.761    
            string item = "", aprobacion = "";
            int rango = int.Parse(Serie);
            string idMovimiento = fecha.Year.ToString() + fecha.Month.ToString("00") + fecha.Day.ToString("00") + "." + fecha.Hour.ToString("00") + fecha.Minute.ToString("00") + fecha.Second.ToString("00") + "." + DateTime.Now.Millisecond.ToString("000");
            string idMovimiento2 = fecha.Year.ToString() + fecha.Month.ToString("00") + fecha.Day.ToString("00") + "." + fecha.Hour.ToString("00") + fecha.Minute.ToString("00") + fecha.Second.ToString("00") + "." + DateTime.Now.Millisecond.ToString("001");
            mef.loadEFXAduanaSello(Serie, hoja.Substring(2, 3), "SSB");
            for (int j = 0; j < mef.totalRegistros; j++)
            {
                item = mef.ITEM;

                #region Igual Primer Rango
                if (int.Parse(Serie) == int.Parse(mef.RANGOINICIAL) && int.Parse(Serie) < int.Parse(mef.RANGOFINAL))
                {
                    mef2.loadMovimientosEspeciesFiscalesItem(item);
                    if (mef.totalRegistros > 0)
                    {
                        mef2.CODESTADO = "100";
                        mef2.actualizar();
                    }

                    mefe1.loadMovimientosEspeciesFiscales("-1");
                    mefe1.newLine();
                    mefe1.IDMOVIMIENTO = idMovimiento2;
                    mefe1.IDESPECIEFISCAL = "SSB";
                    mefe1.CODPAIS = mef.CODPAIS;
                    mefe1.CODIGOADUANA = mef.CODIGOADUANA;
                    mefe1.RANGOINICIAL = Convert.ToString(rango + 1);
                    mefe1.RANGOFINAL = mef.RANGOFINAL;
                    mefe1.CANTIDAD = (int.Parse(mef.RANGOFINAL) - int.Parse(Serie));
                    mefe1.FECHA = DateTime.Now.ToString();
                    mefe1.CODTIPOMOVIMIENTO = "1";   //Usada
                    mefe1.CODESTADO = mef.CODESTADO;
                    mefe1.IDUSUARIO = Session["IdUsuario"].ToString();
                    mefe1.IDCOMPRA = mef.IDCOMPRA;
                    mefe1.commitLine();
                    mefe1.actualizar();
                    aprobacion = "X";
                }
                #endregion

                #region Igual al Ultimo Rango
                else if (int.Parse(Serie) == int.Parse(mef.RANGOFINAL) && int.Parse(Serie) > int.Parse(mef.RANGOINICIAL))
                {
                    mef2.loadMovimientosEspeciesFiscalesItem(item);
                    if (mef.totalRegistros > 0)
                    {
                        mef2.CODESTADO = "100";
                        mef2.actualizar();
                    }
                    mefe.loadMovimientosEspeciesFiscales("-1");
                    mefe.newLine();
                    mefe.IDMOVIMIENTO = idMovimiento;
                    mefe.IDESPECIEFISCAL = "SSB";
                    mefe.CODPAIS = mef.CODPAIS;
                    mefe.CODIGOADUANA = mef.CODIGOADUANA;
                    mefe.RANGOINICIAL = mef.RANGOINICIAL;
                    mefe.RANGOFINAL = Convert.ToString(rango - 1);
                    mefe.CANTIDAD = ((int.Parse(Serie)) - int.Parse(mef.RANGOINICIAL));
                    mefe.FECHA = DateTime.Now.ToString();
                    mefe.CODTIPOMOVIMIENTO = mef.CODTIPOMOVIMIENTO;
                    mefe.CODESTADO = "1";
                    mefe.IDUSUARIO = Session["IdUsuario"].ToString();
                    mefe.IDCOMPRA = mef.IDCOMPRA;
                    mefe.commitLine();
                    mefe.actualizar();
                    aprobacion = "X";
                }
                #endregion

                #region Entre los Rango
                else if (int.Parse(Serie) < int.Parse(mef.RANGOFINAL) && int.Parse(Serie) > int.Parse(mef.RANGOINICIAL))
                {
                    mef2.loadMovimientosEspeciesFiscalesItem(item);
                    if (mef.totalRegistros > 0)
                    {
                        mef2.CODESTADO = "100";
                        mef2.actualizar();
                    }
                    mefe.loadMovimientosEspeciesFiscales("-1");
                    mefe.newLine();
                    mefe.IDMOVIMIENTO = idMovimiento;
                    mefe.IDESPECIEFISCAL = "SSB";
                    mefe.CODPAIS = mef.CODPAIS;
                    mefe.CODIGOADUANA = mef.CODIGOADUANA;
                    mefe.RANGOINICIAL = mef.RANGOINICIAL;
                    mefe.RANGOFINAL = Convert.ToString(rango - 1);
                    mefe.CANTIDAD = ((int.Parse(Serie)) - int.Parse(mef.RANGOINICIAL));
                    mefe.FECHA = DateTime.Now.ToString();
                    mefe.CODTIPOMOVIMIENTO = mef.CODTIPOMOVIMIENTO;   //Usada
                    mefe.CODESTADO = "1";
                    mefe.IDUSUARIO = Session["IdUsuario"].ToString();
                    mefe.IDCOMPRA = mef.IDCOMPRA;
                    mefe.commitLine();
                    mefe.actualizar();

                    mefe1.loadMovimientosEspeciesFiscales("-1");
                    mefe1.newLine();
                    mefe1.IDMOVIMIENTO = idMovimiento2;
                    mefe1.IDESPECIEFISCAL = "SSB";
                    mefe1.CODPAIS = mef.CODPAIS;
                    mefe1.CODIGOADUANA = mef.CODIGOADUANA;
                    mefe1.RANGOINICIAL = Convert.ToString(rango + 1);
                    mefe1.RANGOFINAL = mef.RANGOFINAL;
                    mefe1.CANTIDAD = (int.Parse(mef.RANGOFINAL) - int.Parse(Serie));
                    mefe1.FECHA = DateTime.Now.ToString();
                    mefe1.CODTIPOMOVIMIENTO = mef.CODTIPOMOVIMIENTO;   //Usada
                    mefe1.CODESTADO = "1";
                    mefe1.IDUSUARIO = Session["IdUsuario"].ToString();
                    mefe1.IDCOMPRA = mef.IDCOMPRA;
                    mefe1.commitLine();
                    mefe1.actualizar();
                    aprobacion = "X";
                }
                #endregion

                #region Cuando Solo hay una Serie
                else
                {
                    mef2.loadMovimientosEspeciesFiscalesItem(item);
                    if (mef.totalRegistros > 0)
                    {
                        mef2.CODESTADO = "100";
                        mef2.actualizar();
                    }
                    aprobacion = "X";
                }
                #endregion

                if (aprobacion == "X")
                {
                    efi1.CargarEspeciesxInstruccionParaEliminar("SS", idInstruccion.Value);
                    if (efi1.totalRegistros == 1)
                    {
                        efi1.SERIE = (efi1.SERIE + "," + Serie);
                        if (Observacion != "")
                        {
                            efi1.OBSERVACION = (efi1.OBSERVACION + " | " + Observacion);
                        }
                        efi1.IDMOVIMIENTO = (efi1.IDMOVIMIENTO + "," + mef.IDMOVIMIENTO);
                        efi1.commitLine();
                        efi1.actualizar();
                        registrarMensaje("El Sello de Seguridad" + " No. " + Serie + " asignada(s) a la instrucción " + idInstruccion.Value + " exitosamente");
                    }
                    if (efi1.totalRegistros == 0)
                    {
                        efi.loadEspeciesFiscalesInstruccionesXInstruccion("-1");
                        efi.newLine();
                        efi.IDINSTRUCCION = idInstruccion.Value;
                        efi.CODESPECIEFISCAL = "SS";
                        efi.NECESARIO = "X";
                        efi.SERIE = Serie;
                        efi.OBSERVACION = Observacion;
                        efi.FECHA = DateTime.Now.ToString();
                        efi.ELIMINADO = "0";
                        efi.IDUSUARIO = Session["IdUsuario"].ToString();
                        efi.IDMOVIMIENTO = mef.IDMOVIMIENTO;
                        efi.CARGA = "0";
                        efi.commitLine();
                        efi.actualizar();

                        registrarMensaje("El Sello de Seguridad" + " No. " + Serie + " asignada(s) a la instrucción " + idInstruccion.Value + " exitosamente");
                    }
                }
                if (aprobacion == "")
                {
                    registrarMensaje("El Sello de Seguridad" + " No. " + Serie + " no esta disponible en el inventario, Favor Verificar");
                }
                mef.regSiguiente();
            }
            if (mef.totalRegistros == 0)
            {
                registrarMensaje("El Sello de Seguridad" + " No. " + Serie + " no esta disponible en el inventario, Favor Verificar");
            }
            return;
        }
        catch (Exception)
        {

        }

    }
    private void llenarDatosEspeciesFiscales()
    {
        try
        {
            conectar();
            //InstruccionesBO i = new InstruccionesBO(logApp);
            EspeciesFiscalesInstruccionesBO efi = new EspeciesFiscalesInstruccionesBO(logApp);
            MovimientosEspeciesFiscalesBO mef = new MovimientosEspeciesFiscalesBO(logApp);
            //i.loadInstruccion(idInstruccion.Value);

            ////////limpiar grid
            rgEspeciesFiscales.Rebind();
            ////////
            efi.loadEspeciesFiscalesInstruccionesXInstruccion(idInstruccion.Value.Trim());
            if (efi.totalRegistros > 0)
            {
                #region codigo
                string valor = "";
                string valorDA = "";
                string valorDVA = "";
                string valorF = "";
                for (int i = 0; i < efi.totalRegistros; i++)
                {
                    //llena los checkbox en el grid especies fiscales
                    foreach (GridDataItem gridItem in rgEspeciesFiscales.Items)
                    {

                        string codEspecieFiscal = gridItem.Cells[2].Text;
                        CheckBox ckbEspecieFiscal = (CheckBox)gridItem.FindControl("ckbEspecieFiscal");
                        CheckBox ckbCliente = (CheckBox)gridItem.FindControl("ckbCliente");
                        RadTextBox edSerieEF = (RadTextBox)gridItem.FindControl("edSerieEF");
                        RadTextBox edObservacion = (RadTextBox)gridItem.FindControl("edObservacion");
                        RadNumericTextBox edCantidad = (RadNumericTextBox)gridItem.FindControl("edCantidad");
                        if (codEspecieFiscal == efi.CODESPECIEFISCAL)
                        {

                            if (efi.NECESARIO != "X" & efi.CLIENTE != "X")
                            {
                                edSerieEF.Enabled = false;
                                edSerieEF.Text = "";
                                ckbEspecieFiscal.Checked = false;
                                ckbEspecieFiscal.Enabled = true;
                                ckbCliente.Checked = false;
                                ckbCliente.Enabled = true;
                                edObservacion.Text = "";
                                edObservacion.Enabled = true;
                            }
                            else
                            {
                                if (efi.NECESARIO == "X")
                                {
                                    edSerieEF.Enabled = false;
                                    edSerieEF.Text = efi.SERIE;
                                    ckbEspecieFiscal.Checked = true;
                                    ckbEspecieFiscal.Enabled = false;
                                    ckbCliente.Checked = false;
                                    ckbCliente.Enabled = false;
                                    edObservacion.Text = efi.OBSERVACION;
                                    edObservacion.Enabled = false;
                                    // se asgina valor a la variables para luego utilizarla
                                    // si es sello entonces se asigna
                                    if (codEspecieFiscal == "SS")
                                    {
                                        valor = "";
                                        valor = "X";

                                    }
                                    if (codEspecieFiscal == "DA")
                                    {
                                        valorDA = "";
                                        valorDA = "X";

                                    }
                                    if (codEspecieFiscal == "DVA")
                                    {
                                        valorDVA = "";
                                        valorDVA = "X";

                                    }
                                    if (codEspecieFiscal == "F")
                                    {
                                        valorF = "";
                                        valorF = "X";

                                    }
                                    if (codEspecieFiscal == "HN-MN")
                                    {
                                        valorF = "";
                                        valorF = "X";

                                    }
                                    /////nuevo
                                    string[] arreglo = efi.SERIE.Trim().Split(",".ToCharArray());
                                    edCantidad.Value = arreglo.Length;
                                    edCantidad.Enabled = false;
                                    /////
                                    /////

                                    //////
                                }
                                if (efi.CLIENTE == "X")
                                {
                                    edSerieEF.Enabled = false;
                                    edSerieEF.Text = efi.SERIE;
                                    ckbEspecieFiscal.Checked = false;
                                    ckbEspecieFiscal.Enabled = false;
                                    ckbCliente.Checked = true;
                                    ckbCliente.Enabled = true;
                                    edObservacion.Text = efi.OBSERVACION;
                                    edObservacion.Enabled = false;
                                }

                            }

                        }
                        // valida que el codigo sea SS2 y que el sello tenga asignado especies
                        // entonces se habilita la segunda opcion
                        if (codEspecieFiscal == "SS2" && valor == "X" || codEspecieFiscal == "DA2" && valorDA == "X" || codEspecieFiscal == "DVA2" && valorDVA == "X" || codEspecieFiscal == "F2" && valorF == "X" || codEspecieFiscal == " HN-MN2" && valorF == "X")
                        {
                            edSerieEF.Text = "";
                            ckbEspecieFiscal.Checked = false;
                            edObservacion.Text = "";
                            ckbEspecieFiscal.Enabled = true;
                            edObservacion.Enabled = true;
                            ckbCliente.Checked = false;
                            ckbCliente.Enabled = true;
                            edCantidad.Enabled = true;

                        }
                        if (codEspecieFiscal == "SSB")
                        {
                            edSerieEF.Text = "";
                            edSerieEF.Enabled = true;
                            ckbEspecieFiscal.Checked = false;
                            edObservacion.Text = "";
                            edObservacion.Enabled = true;
                            ckbCliente.Checked = false;
                            ckbCliente.Enabled = false;
                            ckbCliente.Visible = false;
                            edCantidad.Visible = false;
                        }
                        //if (codEspecieFiscal == "DVA2" && valorDVA == "X")
                        //{
                        //    edSerieEF.Text = "";
                        //    ckbEspecieFiscal.Checked = false;
                        //    edObservacion.Text = "";
                        //    ckbEspecieFiscal.Enabled = true;
                        //    edObservacion.Enabled = true;
                        //    //ckbCliente.Checked = false;
                        //    //ckbCliente.Enabled = true;
                        //    edCantidad.Enabled = true;

                        //}
                        //if (codEspecieFiscal == "F2" && valorF == "X")
                        //{
                        //    edSerieEF.Text = "";
                        //    ckbEspecieFiscal.Checked = false;
                        //    edObservacion.Text = "";
                        //    ckbEspecieFiscal.Enabled = true;
                        //    edObservacion.Enabled = true;
                        //    //ckbCliente.Checked = false;
                        //    //ckbCliente.Enabled = true;
                        //    edCantidad.Enabled = true;

                        //}
                        // valida que el codigo sea SS2 y que el sello no tenga asignado especies
                        // se bloquea porque todavia se puede asignar sellos de manera normal
                        if (codEspecieFiscal == "SS2" && valor == "" || codEspecieFiscal == "DA2" && valorDA == "" || codEspecieFiscal == "DVA2" && valorDVA == "" || codEspecieFiscal == "F2" && valorF == "" || codEspecieFiscal == "HN-MN" && valorF == "")
                        {
                            edSerieEF.Text = "";
                            ckbEspecieFiscal.Checked = true;
                            ckbEspecieFiscal.Enabled = false;
                            edObservacion.Text = "";
                            edObservacion.Enabled = false;
                            ckbCliente.Checked = true;
                            ckbCliente.Enabled = false;
                            edCantidad.Enabled = false;
                        }
                        //if (codEspecieFiscal == "DA2" && valorDA == "")
                        //{
                        //    edSerieEF.Text = "";
                        //    ckbEspecieFiscal.Checked = true;
                        //    edObservacion.Text = "";
                        //    ckbEspecieFiscal.Enabled = false;
                        //    edObservacion.Enabled = false;
                        //    ckbCliente.Checked = true;
                        //    ckbCliente.Enabled = false;
                        //    edCantidad.Enabled = false;
                        //}
                        //if (codEspecieFiscal == "DVA2" && valorDVA == "")
                        //{
                        //    edSerieEF.Text = "";
                        //    ckbEspecieFiscal.Checked = true;
                        //    edObservacion.Text = "";
                        //    ckbEspecieFiscal.Enabled = false;
                        //    edObservacion.Enabled = false;
                        //    ckbCliente.Checked = true;
                        //    ckbCliente.Enabled = false;
                        //    edCantidad.Enabled = false;
                        //}
                        //if (codEspecieFiscal == "F2" && valorF == "")
                        //{
                        //    edSerieEF.Text = "";
                        //    ckbEspecieFiscal.Checked = true;
                        //    edObservacion.Text = "";
                        //    ckbEspecieFiscal.Enabled = false;
                        //    edObservacion.Enabled = false;
                        //    ckbCliente.Checked = false;
                        //    ckbCliente.Enabled = false;
                        //    edCantidad.Enabled = false;
                        //}
                    }


                    efi.regSiguiente();
                }
                #endregion
            }
            else
            {
                //resetear los checkbox en el grid especies fiscales
                foreach (GridDataItem gridItem in rgEspeciesFiscales.Items)
                {
                    string codEspecieFiscal = gridItem.Cells[2].Text;
                    CheckBox ckbEspecieFiscal = (CheckBox)gridItem.FindControl("ckbEspecieFiscal");
                    RadTextBox edSerieEF = (RadTextBox)gridItem.FindControl("edSerieEF");
                    RadTextBox edObservacion = (RadTextBox)gridItem.FindControl("edObservacion");
                    CheckBox ckbCliente = (CheckBox)gridItem.FindControl("ckbCliente");
                    RadNumericTextBox edCantidad = (RadNumericTextBox)gridItem.FindControl("edCantidad");
                    edSerieEF.Text = "";
                    ckbEspecieFiscal.Checked = false;
                    edObservacion.Text = "";
                    ckbEspecieFiscal.Enabled = true;
                    edObservacion.Enabled = true;

                    // se bloquea cuando no hay ningnuna especie asignada
                    if (codEspecieFiscal == "SS2" || codEspecieFiscal == "DA2" || codEspecieFiscal == "DVA2" || codEspecieFiscal == "F2" || codEspecieFiscal == "HN-MN")
                    {
                        edSerieEF.Text = "";
                        ckbEspecieFiscal.Checked = true;
                        ckbEspecieFiscal.Enabled = false;
                        edObservacion.Text = "";
                        edObservacion.Enabled = false;
                        ckbCliente.Checked = true;
                        ckbCliente.Enabled = false;
                        edCantidad.Enabled = false;
                    }
                    if (codEspecieFiscal == "SSB")
                    {
                        edSerieEF.Text = "";
                        edSerieEF.Enabled = true;
                        ckbEspecieFiscal.Checked = false;
                        edObservacion.Text = "";
                        edObservacion.Enabled = true;
                        ckbCliente.Checked = false;
                        ckbCliente.Enabled = false;
                        ckbCliente.Visible = false;
                        edCantidad.Visible = false;
                    }
                    //if (codEspecieFiscal == "DVA2")
                    //{
                    //    edSerieEF.Text = "";
                    //    ckbEspecieFiscal.Checked = true;
                    //    edObservacion.Text = "";
                    //    ckbEspecieFiscal.Enabled = false;
                    //    edObservacion.Enabled = false;
                    //    ckbCliente.Checked = true;
                    //    ckbCliente.Enabled = false;
                    //    edCantidad.Enabled = false;

                    //}
                    //if (codEspecieFiscal == "F2")
                    //{
                    //    edSerieEF.Text = "";
                    //    ckbEspecieFiscal.Checked = true;
                    //    edObservacion.Text = "";
                    //    ckbEspecieFiscal.Enabled = false;
                    //    edObservacion.Enabled = false;
                    //    ckbCliente.Checked = true;
                    //    ckbCliente.Enabled = false;
                    //    edCantidad.Enabled = false;

                    //}

                }
            }
        }
        catch (Exception)
        { }
    }

    protected void rgEspeciesFiscales_ItemDataBound(object sender, GridItemEventArgs e)
    {
        try
        {
            foreach (GridDataItem gridItem in rgEspeciesFiscales.Items)
            {
                string especieFiscal = gridItem.Cells[3].Text;
                CheckBox ckbCliente = (CheckBox)gridItem.FindControl("ckbCliente");
                CheckBox ckbEspecieFiscal = (CheckBox)gridItem.FindControl("ckbEspecieFiscal");
                RadNumericTextBox edCantidad = (RadNumericTextBox)gridItem.FindControl("edCantidad");
                if (especieFiscal.Contains("Faucas") || especieFiscal.Contains("Marchamos") || especieFiscal.Contains("Sellos de Seguridad") || especieFiscal.Contains("Poliza Zona Libre") || especieFiscal.Contains("D.V.A.") || especieFiscal.Contains("Polizas"))
                    ckbCliente.Visible = true;
                else
                    ckbCliente.Visible = false;
                if (especieFiscal.Contains("D.V.A.") || especieFiscal.Contains("Extenciones Poliza") || especieFiscal.Contains("Marchamos Nacionales") || especieFiscal.Contains("Sellos de Seguridad") || especieFiscal.Contains("Polizas"))
                    edCantidad.Visible = true;
                else
                    edCantidad.Visible = false;

            }
        }
        catch { }
    }
    #endregion

    #region Conexion
    private void conectarAduanas()
    {
        if (logAppAduanas == null)
            logAppAduanas = new GrupoLis.Login.Login();
        if (!logAppAduanas.conectado)
        {
            logAppAduanas.tipoConexion = TipoConexion.SQL_SERVER;
            logAppAduanas.SERVIDOR = mParamethers.Get("Servidor");
            logAppAduanas.DATABASE = mParamethers.Get("DatabaseEsquemaTramites2");
            logAppAduanas.USER = mParamethers.Get("User");
            logAppAduanas.PASSWD = mParamethers.Get("Password");
            logAppAduanas.conectar();
        }
    }

    private void desconectarAduanas()
    {
        if (logAppAduanas.conectado)
            logAppAduanas.desconectar();
    }
    #endregion
}