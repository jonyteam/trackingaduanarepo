﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;

using System.Drawing;
using System.Drawing.Printing;
using Telerik.Reporting.Processing;
using Telerik.Web.UI.GridExcelBuilder;
using System.Text;
using System.IO;

using System.Windows.Forms;


public partial class ReporteInstrucciones : Utilidades.PaginaBase
{
    public static ReportesAduanas.ReporteGeneral reporGeneral = new ReportesAduanas.ReporteGeneral();
    public static ReportesAduanas.Facturacion reporFact = new ReportesAduanas.Facturacion();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Instrucciones", "Reporte Instrucciones");
            try
            {
                conectar();
                string idInstruccion = Request.QueryString.Get("IdInstruccion").ToString().Trim();

                InstruccionesBO i = new InstruccionesBO(logApp);
                HistorialImpresionesBO hi = new HistorialImpresionesBO(logApp);

                i.ejecutarSPDatosInstruccion(idInstruccion);
                reporGeneral = new ReportesAduanas.ReporteGeneral();
                reporGeneral.DataSource = i.TABLA;
                ReportViewer1.Report = reporGeneral;
                ReportViewer1.Visible = true;
                reporFact.DataSource = i.TABLA;
                ReportViewer2.Report = reporFact;
                ReportViewer2.Visible = true;

                //string descImpresion = "";
                //hi.loadHistorialImpresionesXGuia(codCPCTT);
                //if (hi.totalRegistros > 0)
                //    descImpresion = "Reimpresión";
                //else
                //    descImpresion = "Impresión";

                //hi.loadHistorialImpresiones("-1");
                //hi.newLine();
                //hi.CODIGOGUIA = codCPCTT;
                //hi.DESCRIPCION = descImpresion;
                //hi.FECHA = DateTime.Now.ToString();
                //hi.IMPRESORA = "";  // NombreImpresora;
                //hi.ELIMINADO = "0";
                //hi.IDUSUARIO = Session["IdUsuario"].ToString();
                //hi.commitLine();
                //hi.actualizar();

                //hi.loadHistorialImpresionesXDocumento(idInstruccion);

                //ReportProcessor rp = new ReportProcessor();
                //RenderingResult result = rp.RenderReport("PDF", reporGeneral, null);
                //FileStream fs = new FileStream("C:\\Documento\\Instrucciones\\" + idInstruccion + "-" + hi.totalRegistros + ".pdf", FileMode.Create);
                //fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
                //fs.Flush();
                //fs.Close();

                //llenarBitacora("Se imprimió la Instrucción " + idInstruccion, Session["IdUsuario"].ToString(), idInstruccion);
            }
            catch { }
            finally
            {
                desconectar();
            }
            mpReporte.SelectedIndex = 0;
        }
    }

    public override bool CanGoBack { get { return false; } }

    protected void btnBack_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Response.Redirect("InstruccionesFinalizadas.aspx");
        }
        catch { }
    }

    //private System.Drawing.Printing.PrintDocument docToPrint = new System.Drawing.Printing.PrintDocument();
    //protected void btnImprimir_Click(object sender, ImageClickEventArgs e)
    //{
    //    // Allow the user to choose the page range he or she would 
    //    // like to print.         
    //    PrintDialog printDlg = new PrintDialog();
    //    //PrintDocument printDoc = new PrintDocument();

    //    // Show the help button. 
    //    printDlg.ShowHelp = true;

    //    // Set the Document property to the PrintDocument for  
    //    // which the PrintPage Event has been handled. To display the 
    //    // dialog, either this property or the PrinterSettings property  
    //    // must be set  
    //    printDlg.Document = docToPrint;

    //    DialogResult result = printDlg.ShowDialog();

    //    // If the result is OK then print the document. 
    //    if (result == DialogResult.OK)
    //    {
    //        docToPrint.Print();
    //    }
    //}


    //// The PrintDialog will print the document 
    //// by handling the document's PrintPage event. 
    //private void document_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
    //{

    //    // Insert code to render the page here. 
    //    // This code will be called when the control is drawn. 

    //    // The following code will render a simple 
    //    // message on the printed document. 
    //    string text = "In document_PrintPage method.";
    //    System.Drawing.Font printFont = new System.Drawing.Font
    //        ("Arial", 35, System.Drawing.FontStyle.Regular);

    //    // Draw the content. 
    //    e.Graphics.DrawString(text, printFont,
    //        System.Drawing.Brushes.Black, 10, 10);


    //}


    ////void pd_PrintPage(object sender, PrintPageEventArgs e)
    ////{
    ////    try
    ////    {
    ////        Font unaFuente = new Font("Gill Sans MT Condensed", 7, FontStyle.Regular);
    ////        e.Graphics.PageUnit = GraphicsUnit.Millimeter;
    ////        e.Graphics.DrawString(reporGeneral.ToString(), unaFuente, Brushes.Black, 100, 100, System.Drawing.StringFormat.GenericDefault);
    ////        e.HasMorePages = false;
    ////    }
    ////    catch (Exception ex)
    ////    {
    ////        logError(ex.Message, Session["IdUsuario"].ToString(), "ReporteInstrucciones");
    ////    }

    ////}

}