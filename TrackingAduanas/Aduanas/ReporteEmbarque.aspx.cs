﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.OleDb;
using System.Text;
using System.Configuration;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;

using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.SessionState;
using System.Collections.Specialized;
using GrupoLis.Login;
using System.Text.RegularExpressions;
using System.Security.Principal;
using Telerik.Web;
using System.Security.Cryptography;

public partial class ReporteGestionCarga : Utilidades.PaginaBase
{
    protected bool flag = true;
    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgInstrucciones.FilterMenu);
        rgInstrucciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgInstrucciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Embarque", "Reporte Embarque");
            cargarDatosInicio();
        }
    }

    protected void cargarDatosInicio()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            ClientesBO cl = new ClientesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (u.CODPAIS == "H")
                c.loadPaisesHojaRuta();
            else
                c.loadAllCampos("PAISES", u.CODPAIS);
            cmbPais.DataSource = c.TABLA;
            cmbPais.DataBind();
            cl.loadAllCamposClientesXPais(cmbPais.SelectedValue);
            cmbCliente.DataSource = cl.TABLA;
            cmbCliente.DataBind();
        }
        catch { }
    }

    public override bool CanGoBack { get { return false; } }

    private void Salir()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("top.location.href = LoginTracking.aspx;");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Salir", sb.ToString(), false);
    }

    protected void cmbPais_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            conectar();
            ClientesBO cl = new ClientesBO(logApp);
            cl.loadAllCamposClientesXPais(cmbPais.SelectedValue);
            cmbCliente.DataSource = cl.TABLA;
            cmbCliente.DataBind();
        }
        catch { }
    }

    #region Gestion Embarque
    protected void rgInstrucciones_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridInstrucciones();
    }

    private void llenarGridInstrucciones()
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            i.loadInstruccionEmbarqueDiario(dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"), cmbCliente.SelectedValue, cmbPais.SelectedItem.Text.Trim());
            rgInstrucciones.DataSource = i.TABLA;
            if (flag)
                rgInstrucciones.DataBind();
        }
        catch (Exception) { }
        finally { desconectar(); }
    }

    protected void rgInstrucciones_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            if (rgInstrucciones.Items.Count <= 0 & e.CommandName == RadGrid.ExportToExcelCommandName)
                e.Canceled = true;
            else if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                ConfigureExport();
                flag = false;
            }
            else if (e.CommandName == "Page")
                flag = false;
        }
        catch (Exception)
        { }
    }

    private void ConfigureExport()
    {
        //rgInstrucciones.Columns[4].Visible = false;
        //rgInstrucciones.Columns[5].Visible = false;
        //rgInstrucciones.Columns[6].Visible = false;
        //rgInstrucciones.Columns[7].Visible = false;
        //rgInstrucciones.Columns[8].Visible = false;
        //rgInstrucciones.Columns[9].Visible = false;
        //rgInstrucciones.Columns[10].Visible = false;
        //rgInstrucciones.Columns[11].Visible = false;
        //rgInstrucciones.Columns[12].Visible = false;
        String filename = "Embarque" + "_" + DateTime.Now.ToShortDateString();
        rgInstrucciones.GridLines = GridLines.Both;
        rgInstrucciones.ExportSettings.FileName = filename;
        rgInstrucciones.ExportSettings.IgnorePaging = true;
        rgInstrucciones.ExportSettings.OpenInNewWindow = false;
        rgInstrucciones.ExportSettings.ExportOnlyData = true;
    }
    #endregion

    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        if (dpFechaInicio.SelectedDate <= dpFechaFinal.SelectedDate)
        {
            flag = true;
            llenarGridInstrucciones();
            rgInstrucciones.Visible = true;
        }
        else
            registrarMensaje2("Fecha inicio no puede ser mayor que fecha final");
    }

    private void registrarMensaje2(string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("alert('" + mensaje + "');");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "mensaje", sb.ToString(), false);
    }

    protected void rgInstrucciones_Init(object sender, System.EventArgs e)
    {
        GridFilterMenu menu = rgInstrucciones.FilterMenu;
        menu.Items.RemoveAt(rgInstrucciones.FilterMenu.Items.Count - 2);
    }
}