﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Text.RegularExpressions;
using System.IO;
using Microsoft.Office.Interop;

public partial class CargarSolicitudesLCL : Utilidades.PaginaBase
{
    protected string filename = "";
    private GrupoLis.Login.Login logAppPortal;

    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Facturas Cargill";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        filename = parameterForm1("filename", "");
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Cargar Facturas Cargill", "Cargar Facturas Cargill");
            if (!(Anular || Ingresar || Modificar))
            {
                redirectTo("Inicio.aspx");
            }
            DirectoryInfo DIR = new DirectoryInfo(fileUpload.TargetPhysicalFolder);
            if (!DIR.Exists)
            {
                DIR.Create();
            }
        }
    }
    protected void SetVisible(TableCellCollection cells, String commandName, bool visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is ImageButton)
                {
                    ImageButton btn = (ImageButton)control;

                    if (btn.CommandName == commandName)
                        btn.Visible = visible;
                }
            }
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void btnCargar_Click(object sender, EventArgs e)
    {
        try
        {

            LabelMensajes.Visible = false;
            RadButtonProcesar.Enabled = false;
            if (fileUpload.UploadedFiles.Count > 0)
            {
                UploadedFile file = fileUpload.UploadedFiles[0];
                if (file.ContentLength <= fileUpload.MaxFileSize)
                {
                    if (!Object.Equals(file, null))
                    {
                        LooongMethodWhichUpdatesTheProgressContext(file);
                    }

                    repeaterResults.DataSource = fileUpload.UploadedFiles;
                    repeaterResults.DataBind();
                    repeaterResults.Visible = true;
                    System.Web.UI.WebControls.Label lbl = (System.Web.UI.WebControls.Label)repeaterResults.Controls[0].FindControl("lblTitulo");
                    filename = file.GetName();

                    file.SaveAs(fileUpload.TargetPhysicalFolder + filename, true);
                    lbl.Text = "Archivo Cargado Exitosamente: ";
                    RadButtonProcesar.Enabled = true;
                    labelNoResults.Text = "";
                }
            }
            else
            {
                if (fileUpload.InvalidFiles.Count > 0)
                {
                    repeaterResults.Visible = true;
                    repeaterResults.DataSource = fileUpload.InvalidFiles;
                    repeaterResults.DataBind();
                    System.Web.UI.WebControls.Label lbl = (System.Web.UI.WebControls.Label)repeaterResults.Controls[0].FindControl("lblTitulo");
                    lbl.Text = "Archivo Invalido:";
                }
                else
                {
                    repeaterResults.Visible = false;
                    registrarMensaje("Debe Seleccionar el archivo a cargar");
                }
                filename = "";
            }
        }
        catch (Exception ex)
        {
            btnCargar.Enabled = true;
            registrarMensaje(ex.Message);
        }
    }

    protected void readExcel()
    {
        try
        {
            conectarPortal();
            Microsoft.Office.Interop.Excel.Application xlApp;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            Microsoft.Office.Interop.Excel.Range range;

            string str;
            int rCnt = 0;
            int cCnt = 0;

            xlApp = new Microsoft.Office.Interop.Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Open(@"J:\Files1\Aduanas\" + filename, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            string[] campos = new string[5];

            range = xlWorkSheet.UsedRange;
            for (rCnt = 2; rCnt <= range.Rows.Count; rCnt++)
            {
                for (cCnt = 1; cCnt <= range.Columns.Count; cCnt++)
                {
                        str = (range.Cells[rCnt, cCnt] as Microsoft.Office.Interop.Excel.Range).Text.ToString();
                        campos[cCnt] = str.Trim();
                }
                if (campos[1].Substring(0, 1) == "H")
                {
                    guardardatosHCC(campos);
                }
            }

            xlWorkBook.Close(false, null, null);
            xlApp.Quit();

            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);

            LabelMensajes.Text = "Archivo procesado exitosamente...";
        }
        catch (Exception ex)
        {
            RadButtonProcesar.Enabled = true;
            LabelMensajes.Visible = false;
        }
        finally
        {
            desconectar();
        }
    }
    protected void guardardatosHCC(string[] campos)
    {
        //InstruccionesBO i = new InstruccionesBO(logAppPortal);
        InstruccionesAnexosRepository iar = new InstruccionesAnexosRepository();

        var instruccion = iar.LoadInstruccion(campos[1]);
        if (instruccion != null)
        {
            if (instruccion.IdCliente != "8000423")
            {
                //verificar si existe la Hr en la tabla InstruccionesAnexos

                var existe = iar.ExisteInstruccion(campos[1]);
                if (existe == null)
                {

                    var registro = new InstruccionesAnexo()
                    {
                        IdInstruccion = campos[1],
                        Factura = campos[2],
                        FechaFactura = Convert.ToDateTime(campos[3]),
                        NotasRembolso = campos[4],
                        IdUsuario = Convert.ToInt32(Session["IdUsuario"]),
                        Fecha = DateTime.Now,
                        Eliminado = false
                    };

                    iar.InsertarFacturaFecha(registro);

                }
                else if (existe.IdInstruccion != null)
                {
                    var registro = new InstruccionesAnexo()
                    {
                        IdInstruccion = campos[1],
                        Factura = campos[2],
                        FechaFactura = Convert.ToDateTime(campos[3]),
                    };

                    iar.Actualizar(registro);
                }
            }
            else
            {
                //Codigo para facturas Cargil
                HCCBO H = new HCCBO(logAppPortal);
                H.loadInstruccion(campos[1]);
                if (H.totalRegistros == 1)
                {
                    H.FACTURAVESTA = campos[2];
                    H.FECHAENTREGACARGILL = campos[3].ToString();
                    H.actualizar();
                    llenarBitacora("Se ingreso la factura N. " + H.FACTURAVESTA + " Y la fecha " + H.FECHAENTREGACARGILL, Session["IdUsuario"].ToString(), H.IDINSTRUCCION);
                }
                else if (H.totalRegistros == 0)
                {
                    H.newLine();
                    H.IDINSTRUCCION = campos[1];
                    H.FACTURAVESTA = campos[2];
                    H.FECHAENTREGACARGILL = campos[3];
                    H.commitLine();
                    H.actualizar();
                    llenarBitacora("Se ingreso la factura N. " + H.FACTURAVESTA + " Y la fecha " + H.FECHAENTREGACARGILL, Session["IdUsuario"].ToString(), H.IDINSTRUCCION);
                }
                else
                {
                    registrarMensaje("Favor consulte con el adminitrador ya que no todas las Hojas se pudieron actualizar");
                }
            }
        }
        


    }

    private void LooongMethodWhichUpdatesTheProgressContext(UploadedFile file)
    {
        try
        {
            const int total = 100;
            RadProgressContext progress = RadProgressContext.Current;
            for (int i = 0; i < total; i++)
            {
                progress.PrimaryTotal = 1;
                progress.PrimaryValue = 1;
                progress.PrimaryPercent = 100;
                progress.SecondaryTotal = total;
                progress.SecondaryValue = i;
                progress.SecondaryPercent = i;

                progress.CurrentOperationText = file.GetName() + " se está procesando...";

                if (!Response.IsClientConnected)
                {
                    break;
                }
                System.Threading.Thread.Sleep(100);
            }
        }
        catch (Exception ex)
        {
            registrarMensaje(ex.Message);
        }
    }
    protected string parameterForm1(string name, string startup)
    {
        if (Page.Request.Form.Get(name) != null && Page.Request.Form.Get(name).Length > 0)
            if (!Regex.IsMatch(Page.Request.Form.Get(name), @"^[\w-=,. ]{1,70}$"))
                throw new ArgumentException("Invalid name parameter");
        return (null != Page.Request.Form.Get(name)) ? Page.Request.Form.Get(name) : startup;
    }
    private void releaseObject(object obj)
    {
        try
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            obj = null;
        }
        catch (Exception ex)
        {
            obj = null;
            registrarMensaje("Unable to release the Object " + ex.ToString());
        }
        finally
        {
            GC.Collect();
        }
    }

    protected void RadButtonProcesar_Click(object sender, EventArgs e)
    {
        LabelMensajes.Visible = true;
        LabelMensajes.Text = "Procesando... Espere por favor.";
        RadButtonProcesar.Enabled = false;
        readExcel();
    }
    #region Conexion
    private void conectarPortal()
    {
        if (logAppPortal == null)
            logAppPortal = new GrupoLis.Login.Login();
        if (!logAppPortal.conectado)
        {
            logAppPortal.tipoConexion = TipoConexion.SQL_SERVER;
            logAppPortal.SERVIDOR = mParamethers.Get("Servidor");
            logAppPortal.DATABASE = mParamethers.Get("DatabasePC");
            logAppPortal.USER = mParamethers.Get("User");
            logAppPortal.PASSWD = mParamethers.Get("Password");
            logAppPortal.conectar();
        }
    }

    private void desconectarPortal()
    {
        if (logAppPortal.conectado)
            logAppPortal.desconectar();
    }
    #endregion
}