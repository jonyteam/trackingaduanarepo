﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Telerik.Web.UI;

public partial class MantenimientoUsuarios : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Mantenimiento Usuarios";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgUsuarios.FilterMenu);
        rgUsuarios.Skin = Skin;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Ingreso Tarifas UNO", "Usuarios");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void SetVisible(TableCellCollection cells, String commandName, bool visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is ImageButton)
                {
                    ImageButton btn = (ImageButton)control;

                    if (btn.CommandName == commandName)
                        btn.Visible = visible;
                }
            }
        }
    }

    #region Usuarios
    private void loadGridUsuarios()
    {
        try
        {
            conectar();
            TasadeCambioUNOBO TCU = new TasadeCambioUNOBO(logApp);
            int year = Convert.ToInt32(DateTime.Now.Year.ToString());
            int mes = Convert.ToInt32(DateTime.Now.Month.ToString());
            TCU.CargarTasasDelYear((year-1),mes);
            rgUsuarios.DataSource = TCU.TABLA;
            rgUsuarios.DataBind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected Object GetUsuarios()
    {
        try
        {
            conectar();

            UsuariosBO bo = new UsuariosBO(logApp);
            bo.loadUsuariosNombreCompleto();

            return bo.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }

        return null;
    }

    protected Object GetPaises()
    {
        try
        {
            conectar();
            CodigosBO cod = new CodigosBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (User.IsInRole("Administradores") & u.CODPAIS == "H")
                cod.loadPaisesHojaRuta();
            else
                cod.loadAllCampos("PAISES", u.CODPAIS);
            return cod.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }
        return null;
    }

    protected void rgUsuarios_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        loadGridUsuarios();
    }

    protected bool GetDatosEditados(TasadeCambioUNOBO TCU, TasadeCambioUNOBO TCU1, TasadeCambioUNOBO TCU2, GridItem item, string oldUsuario)
    {
        bool resp = false;
        GridEditableItem editedItem = item as GridEditableItem;
        RadNumericTextBox Super = (RadNumericTextBox)editedItem.FindControl("edGasolinaSuper");
        RadNumericTextBox Regular = (RadNumericTextBox)editedItem.FindControl("edGasolinaRegular");
        RadNumericTextBox Disel = (RadNumericTextBox)editedItem.FindControl("edDisel");
        RadNumericTextBox Jet = (RadNumericTextBox)editedItem.FindControl("edAVJET");
        RadNumericTextBox Bunker = (RadNumericTextBox)editedItem.FindControl("edBunker");
        RadNumericTextBox Kerosen = (RadNumericTextBox)editedItem.FindControl("edKerosen");
        RadDatePicker FechaInicio = (RadDatePicker)editedItem.FindControl("edFechaInicio");
        RadDatePicker FechaFinal = (RadDatePicker)editedItem.FindControl("edFechaFinal");
        TCU1.VerificarFechaInicio(FechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"));
        TCU2.VerificarFechaFinal(FechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"));
        if (TCU1.totalRegistros <= 0 )
        {
            if (TCU2.totalRegistros <= 0)
            {

                TCU.GASOLINASUPER = Convert.ToDouble(Super.Text);
                TCU.GASOLINAREGULAR = Convert.ToDouble(Regular.Text);
                TCU.DIESEL = Convert.ToDouble(Disel.Text);
                TCU.AVJET = Convert.ToDouble(Jet.Text);
                TCU.BUNKER = Convert.ToDouble(Bunker.Text);
                TCU.KEROSENO = Convert.ToDouble(Kerosen.Text);
                TCU.FECHAINICIO = FechaInicio.SelectedDate.Value.ToString();
                TCU.FECHAFINAL = FechaFinal.SelectedDate.Value.ToString();
                TCU.FECHA = DateTime.Now.ToString();
                TCU.IDUSUARIO = Session["IdUsuario"].ToString();
                TCU.CODESTADO = "0";
                resp = true;
            }
            else
            {
                registrarMensaje("Para esta fecha " + FechaFinal + " ya se tiene una tasa de cambio, favor ingrese otra fecha");
            }
        }
        else
            registrarMensaje("Para esta fecha " + FechaInicio + " ya se tiene una tasa de cambio, favor ingrese otra fecha");
        return resp;
    }

    protected void rgUsuarios_InsertCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            GridEditableItem editedItem = e.Item as GridEditableItem;

                TasadeCambioUNOBO TCU = new TasadeCambioUNOBO(logApp);
                TasadeCambioUNOBO TCU1 = new TasadeCambioUNOBO(logApp);
                TasadeCambioUNOBO TCU2 = new TasadeCambioUNOBO(logApp);
                TCU.loadtasa();
                TCU.newLine();
                bool resp = GetDatosEditados(TCU, TCU1, TCU2, e.Item, "");
                if (resp)
                {
                    TCU.FECHA = DateTime.Now.ToString();
                    TCU.IDUSUARIO = Session["IdUsuario"].ToString();
                    TCU.CODESTADO = "0";
                    TCU.commitLine();
                    TCU.actualizar();
                    registrarMensaje("Fecha ingresada exitosamente");
                    llenarBitacora("Se ingresaron las tasas de cambio de " + TCU.FECHAINICIO +" al "+ TCU.FECHAFINAL, Session["IdUsuario"].ToString(), "");
                }
            //}
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "IngresoTarifas");
        }
        finally
        {
            desconectar();
        }
    }

    protected void rgUsuarios_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();
            GridEditableItem editedItem = e.Item as GridEditableItem;
            TasadeCambioUNOBO TCU = new TasadeCambioUNOBO(logApp);
            TasadeCambioUNOBO TCU1 = new TasadeCambioUNOBO(logApp);
            TasadeCambioUNOBO TCU2 = new TasadeCambioUNOBO(logApp);
            TCU.loadtasa();
            TCU.newLine();
            bool resp = GetDatosEditados(TCU, TCU1, TCU2, e.Item, "");
            if (resp)
            {
                TCU.actualizar();
                registrarMensaje("Usuario modificado exitósamente");
                //llenarBitacora("Se modificó el usuario " + oldUsuario + " por " + bo.USUARIO, Session["IdUsuario"].ToString(), bo.IDUSUARIO);
            }
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoUsuarios");
        }
        finally
        {
            desconectar();
        }
    }

    protected void rgUsuarios_DeleteCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridDataItem editedItem = e.Item as GridDataItem;

            String IdUsuario = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdUsuario"]);

            UsuariosBO bo = new UsuariosBO(logApp);
            bo.loadUsuario(IdUsuario);
            bo.ELIMINADO = "1";
            bo.actualizar();
            registrarMensaje("Usuario eliminado exitósamente");

            llenarBitacora("Se eliminó el usuario " + bo.USUARIO, Session["IdUsuario"].ToString(), bo.IDUSUARIO);
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoUsuarios");
        }
        finally
        {
            desconectar();
        }

    }
    public RadDatePicker FechaInicio;
    public RadDatePicker FechaFinal;

    protected void rgUsuarios_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if ((e.Item.ItemType == GridItemType.Item) || (e.Item.ItemType == GridItemType.AlternatingItem) || (e.Item.ItemType == GridItemType.SelectedItem))
        {
            SetVisible(e.Item.Cells, "Edit", Modificar);
            SetVisible(e.Item.Cells, "Delete", Anular);
        }

        if (e.Item is GridEditFormInsertItem)
        {
            //conectar();
            //AduanasBO a = new AduanasBO(logApp);
            //GridEditFormItem editForm = e.Item as GridEditFormItem;
            //edAduana = editForm.FindControl("edAduana") as RadComboBox;

            //a.loadPaisAduanas(edPais.SelectedValue);
            //edAduana.DataSource = a.TABLA;
            //edAduana.DataBind();+
            //string hola = "";
        }
        else if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
        {
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////PENDIENTE REVISAR/////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            conectar();
            TasadeCambioUNOBO TCU = new TasadeCambioUNOBO(logApp);
            GridEditFormItem editForm = e.Item as GridEditFormItem;
            RadDatePicker FechaInicio = (RadDatePicker)editForm.FindControl("edFechaInicio");
            RadDatePicker FechaFinal = (RadDatePicker)editForm.FindControl("edFechaFinal");
            
            //FechaInicio = editForm.FindControl("edFechaInicio") as RadDatePicker;
            //FechaFinal = editForm.FindControl("edFechaFinal") as RadDatePicker;

            TCU.LoadFecha(FechaInicio.SelectedDate.Value.ToString("yyyy-mm-dd"), FechaFinal.SelectedDate.Value.ToString());
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////PENDIENTE REVISAR///////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        }
    }

    protected void rgUsuarios_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
        {
            try
            {
                //conectar();
                //GridEditFormItem editForm = e.Item as GridEditFormItem;
                //edPais = editForm.FindControl("edPais") as RadNumericTextBox;
                //edPais.AutoPostBack = true;
                //edPais.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(edPais_SelectedIndexChanged);
            }
            catch { }

        }
        //if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
        //{
        //    conectar();
        //    GridEditFormItem editForm = e.Item as GridEditFormItem;
        //    edMonitoreoFin = editForm.FindControl("edMonitoreoRemotoFin") as RadComboBox;
        //    edMonitoreoFin.AutoPostBack = true;
        //    edMonitoreoFin.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(edMonitoreoFin_SelectedIndexChanged);
        //}
    }
    protected void rgUsuarios_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            UsuariosBO u = new UsuariosBO(logApp);
            GridDataItem editedItem = e.Item as GridDataItem;
            if (e.CommandName == "Reset")
            {
                String usuario = e.Item.Cells[4].Text;
                u.loadUsuarioXUsuario(usuario.Trim());
                u.PASSWORD = Utilidades.PaginaBase.hashMD5(String.Format("{0}1234$", usuario));
                u.VENCIMIENTO = DateTime.Now.AddDays(-1.0);
                u.actualizar();
                registrarMensaje("El password del usuario " + usuario + " fue reseteado a: " + usuario + "1234$");
            }
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "Instrucciones");
        }
        finally
        {
            desconectar();
        }
    }

    #endregion
}
