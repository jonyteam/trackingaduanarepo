﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LoginTracking.aspx.cs" Inherits="Login" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Grupo Vesta - Aduanas</title>
    
    <%--<link rel="shortcut icon" href="~/favicon.ico" type="image/x-icon"/>
    <link rel="icon" href="~/favicon.ico" type="image/x-icon"/>--%>
</head>
<body>
    <form id="form1" runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <div style="width: 100%; height: 100%;">
          <table style="width: 100%; height: 100%;">
            <tr>
                <td style="width: 35%;" rowspan="2"></td>
                <td rowspan="3" style="width: 30%" align="center">
                    <table>
                        <tr>
                            <td align="center" colspan="3" style="height: 91px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="height: 19px; text-align: center">
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/LogoVesta.jpg" 
                                    Height="140px"/>
                            </td>
                        </tr>
                         <tr>
                            <td colspan="3" align="center" 
                                 
                                 style="font-weight: bold; font-size: 0.9em; color: white; background-color: #FF2424">
                             Log In</td>
                         </tr>
            		     <tr><td class="panetext">
                            Usuario:</td><td style="width: 162px" align="left">
                            <asp:TextBox ID="txtUsuario" runat="server" Width="149px"></asp:TextBox>&nbsp;
                            </td>
                             <td>
                            <asp:RequiredFieldValidator ID="txtUsuarioRequired" runat="server" ControlToValidate="txtUsuario"
                             ErrorMessage="User Name is required." ToolTip="User Name is required.">*</asp:RequiredFieldValidator></td>
                         </tr>
                         <tr><td class="panetext">
                            Password:</td><td style="width: 162px" align="left">
                            <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="149px"></asp:TextBox></td>
                             <td>
                            <asp:RequiredFieldValidator ID="txtPasswordRequired" runat="server" ControlToValidate="txtPassword"
                            ErrorMessage="Password is required." ToolTip="Password is required.">*</asp:RequiredFieldValidator></td>
                         </tr>
                         <tr>
                            <td colspan="2" class="panetext" align="center" style="color: red">
                            <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                            </td>
                             <td align="center" class="panetext" colspan="1" style="color: red">
                             </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="panetext" align="right">
                                <asp:Button ID="btnLogin" runat="server" BackColor="White" BorderColor="#FF2424"
                                 BorderStyle="Solid" BorderWidth="1px" CommandName="Login" Font-Names="Verdana"
                                 Font-Size="0.8em" ForeColor="#FF2424" Text="Log In" OnClick="btnLogin_Click"/>
                             </td>
                            <td align="right" class="panetext" colspan="1">
                            </td>
                         </tr>
                    </table>
                </td>
                <td style="width: 35%; height: 121px;" align="right">
                            </td>
            </tr>
            <tr>
                <td style="width: 35%" align="right">
                </td>
            </tr>
            <tr>
                <td style="width: 35%; height: 65px">
                </td>
                <td style="width: 35%;" align="right">
                    &nbsp;</td>
            </tr>
        </table>
   </div>
    </form>
</body>
</html>

