﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="FcCargarArchivoSelloTiempos.aspx.cs" Inherits="FcCargarArchivoSelloTiempos" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadScriptBlock ID="scripts" runat="server">
        <script type="text/javascript">
            //<![CDATA[
            function requestStart(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
            }

            function mensaje(msg) {
                var retorno = confirm(msg);
                return retorno;
            }
        </script>
    </telerik:RadScriptBlock>
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
    </telerik:RadAjaxPanel>
    <asp:Panel ID="PanelSubirArchivo" runat="server" BorderStyle="Inset" Width="1060px">
        <table width="1060px" style="border: thin solid #0066FF">
            <tr>
                <td></td>
                <td></td>
                <td colspan="2" style="padding-left: 60px">
                    <br />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Buscar Archivo (Excel)"
                        Font-Bold="True"></asp:Label>
                </td>
                <td></td>
                <td>
                    <telerik:RadUpload ID="FileUpload" runat="server" AllowedFileExtensions="xlsx"
                        ControlObjectsVisibility="None" EnableFileInputSkinning="true" FocusOnLoad="true"
                        InitialFileInputsCount="1" MaxFileInputsCount="1" MaxFileSize="5242880" OverwriteExistingFiles="True"
                        Width="100%" Font-Bold="True">
                    </telerik:RadUpload>
                    <br />
                </td>
                <td>
                    <div class="smallModule">
                        <asp:Label ID="labelNoResults" runat="server">Aun No hay Archivos subidos...</asp:Label>
                        <asp:Repeater ID="repeaterResults" runat="server" Visible="False">
                            <HeaderTemplate>
                                <div>
                                    <asp:Label ID="lblTitulo" runat="server" Text=""></asp:Label>
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#DataBinder.Eval(Container.DataItem, "FileName")%>
                                <%#DataBinder.Eval(Container.DataItem, "ContentLength").ToString() + " bytes"%>
                                <br />
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td style="padding-left: 20px">
                    <asp:Button ID="btnCargar" runat="server" Text="Cargar Archivo"
                        OnClick="btnCargar_Click" Font-Bold="True" />
                    <br />
                    <br />
                    <br />
                    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
                        <input id="filename" name="filename" type="hidden" value="<%=Filename%>" />
                    </telerik:RadCodeBlock>
                    <telerik:RadProgressManager ID="RadProgressManager1" runat="server" />
                    <telerik:RadProgressArea ID="RadProgressArea1" runat="server"
                        HeaderText="Subiendo el archivo, espere por favor.">
                    </telerik:RadProgressArea>
                    <br />
                    <br />
                </td>
                <td>
                    <asp:Label ID="LabelMensajes" runat="server"
                        Text="Archivo procesado exitosamente..." Font-Bold="True" ForeColor="#339933"
                        Font-Size="14pt" Visible="False"></asp:Label>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
