﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteUNO.aspx.cs" Inherits="GenerarArchivoSAPDinant" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style12
        {
            width: 100%;
            height: 50px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="pnlRequestStarted">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">

                function pnlRequestStarted(ajaxPanel, eventArgs) {
                    try {
                        if ((eventArgs.EventTarget == "<%= btnSalvar.UniqueID %>")) {
                            eventArgs.EnableAjax = false;
                        }
                    }
                    catch (err)
                { }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                    }
                }                                  
            </script>
        </telerik:RadScriptBlock>
        <input id="cmbMaterial" runat="server" type="hidden"  />
        <input id="cmbMedioPago" runat="server" type="hidden"  />
        <input id="cmbPagarA" runat="server" type="hidden"  />
        <input id="txtMonto" runat="server" type="hidden"  />
        <input id="txtIVA" runat="server" type="hidden"  />
        <input id="txtMontoPagar" runat="server" type="hidden"  />
        <input id="cmbMoneda" runat="server" type="hidden"  />
        <input id="edFecha" runat="server" type="hidden" />
        <input id="edHora" runat="server" type="hidden" />
        
        <br />
        <table border="1" style="width: 45%">
            <tr>
                <td style="width: 15%">
                    <asp:Label ID="lblFechaInicio" runat="server" forecolor="Black" 
                        text="Fecha Inicio:">
                </asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadDatePicker ID="dpFechaInicio" runat="server" EnableTyping="False" 
                        Width="95%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        controltovalidate="dpFechaInicio" errormessage="Ingrese Fecha" 
                        forecolor="Red" />
                </td>
                <td style="width: 15%">
                    <asp:Label ID="lblFechaFinal" runat="server" forecolor="Black" 
                        text="Fecha Final:"></asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadDatePicker ID="dpFechaFinal" runat="server" EnableTyping="False" 
                        Width="95%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator ID="reqName" runat="server" 
                        controltovalidate="dpFechaFinal" errormessage="Ingrese Fecha" forecolor="Red" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4">
                    <asp:ImageButton ID="btnBuscar" runat="server" 
                        imageurl="~/Images/24/view_24.png" onclick="btnBuscar_Click" />
                </td>
            </tr>
        </table>
        <br />
        <telerik:RadGrid ID="rgReporte" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" BorderStyle="Solid" 
            CellSpacing="0" GridLines="None" 
            onneeddatasource="RadGrid1_NeedDataSource" PageSize="2000" 
            style="margin-right: 0px" Width="1500px">
            <ClientSettings AllowColumnsReorder="True">
                <Selecting AllowRowSelect="True" />
            </ClientSettings>
            <ExportSettings FileName="Reporte" HideStructureColumns="true" 
                OpenInNewWindow="True">
                <Excel AutoFitImages="True" Format="Biff" />
            </ExportSettings>
            <MasterTableView CommandItemDisplay="Top" >
                <CommandItemSettings ExportToPdfText="Export to PDF" 
                    ShowAddNewRecordButton="false" ShowExportToExcelButton="true" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                    Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                    Visible="True">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="Fecha" DataFormatString="{0:dd-MM-yyyy}" 
                        FilterControlAltText="Filter Fecha column" HeaderText="Fecha" 
                        UniqueName="Fecha">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="HojaRuta" 
                        FilterControlAltText="Filter HojaRuta column" HeaderText="Hoja Ruta" 
                        UniqueName="HojaRuta">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Cliente" 
                        FilterControlAltText="Filter Cliente column" HeaderText="Cliente" 
                        UniqueName="Cliente">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Regimen" 
                        FilterControlAltText="Filter Regimen column" HeaderText="Regimen" 
                        UniqueName="Regimen">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Serie" 
                        FilterControlAltText="Filter Serie column" HeaderText="Serie" 
                        UniqueName="Serie">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Correlativo" 
                        FilterControlAltText="Filter Correlativo column" HeaderText="Correlativo" 
                        UniqueName="Correlativo">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ItemAPV" 
                        FilterControlAltText="Filter ItemAPV column" HeaderText="ItemAPV" 
                        UniqueName="ItemAPV">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="APV" 
                        FilterControlAltText="Filter APV column" HeaderText="APV" UniqueName="APV">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Galones" 
                        FilterControlAltText="Filter Galones column" HeaderText="Galones" 
                        UniqueName="Galones">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Kilos" 
                        FilterControlAltText="Filter Kilos column" HeaderText="Kilos" 
                        UniqueName="Kilos">
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <ItemStyle BorderColor="Black" BorderStyle="Solid" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        <table class="style12">
            <tr>
                <td style="text-align: center">
                     <asp:ImageButton ID="btnGuardar" runat="server" 
                         ImageUrl="~/Images/24/disk_blue_ok_24.png" 
                         OnClientClick="radconfirm('¿Esta Seguro de Generar la Carga?',confirmCallBackSalvar, 300, 100); return false;" 
                         ToolTip="Salvar" Visible="false" />
                     <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="~/Images/gris.png" 
                         OnClick="btnSalvar_Click" />
                     <asp:Button ID="btnCargar" runat="server" Text="Cargar Archivo" 
                         Visible="false" />
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
    <%--<GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>--%>
</asp:Content>
