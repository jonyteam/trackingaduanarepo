﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="PagoTesoreria.aspx.cs" Inherits="GenerarArchivoSAPDinant" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style12
        {
            width: 100%;
            height: 50px;
        }
        .style13
        {
            width: 100%;
        }
        .style14
        {
            width: 268px;
        }
        .style15
        {
            width: 245px;
        }
        .style16
        {
            width: 228px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="pnlRequestStarted" Width="100%">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">

                function pnlRequestStarted(ajaxPanel, eventArgs) {
                    try {
                        if ((eventArgs.EventTarget == "<%= btnSalvar.UniqueID %>")) {
                            eventArgs.EnableAjax = false;
                        }
                    }
                    catch (err)
                { }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                    }
                }                                  
            </script>
        </telerik:RadScriptBlock>
        <input id="cmbMaterial" runat="server" type="hidden" />
        <input id="cmbMedioPago" runat="server" type="hidden" />
        <input id="cmbPagarA" runat="server" type="hidden" />
        <input id="txtMonto" runat="server" type="hidden" />
        <input id="txtIVA" runat="server" type="hidden" />
        <input id="txtMontoPagar" runat="server" type="hidden" />
        <input id="cmbMoneda" runat="server" type="hidden" />
        <input id="edFecha" runat="server" type="hidden" />
        <input id="edHora" runat="server" type="hidden" />
        <input id="txtNumeroSAP" runat="server" type="hidden" />
        <br />
        <table class="style12">
            <tr>
                <td style="text-align: center">
                   <%-- <telerik:RadWindowManager ID="RadWindowManager2" runat="server">
                    </telerik:RadWindowManager>--%>
                    <telerik:RadGrid ID="rgGastos" runat="server" AllowFilteringByColumn="True" AllowPaging="True"
                        AllowSorting="True" AutoGenerateColumns="False" BorderStyle="Solid" CellSpacing="0"
                        GridLines="None" OnNeedDataSource="RadGrid1_NeedDataSource" PageSize="20" Style="margin-right: 0px"
                        Width="100%" OnItemCommand="rgGastos_ItemCommand" Culture="es-ES"    OnDetailTableDataBind="rgIngresos_DetailTableDataBind" >
                        <ClientSettings AllowColumnsReorder="True">
                            <Selecting AllowRowSelect="True" />
                            <Selecting AllowRowSelect="True" />
                            <Selecting AllowRowSelect="True" />
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                        </ClientSettings>
                        <ExportSettings FileName="Reporte" HideStructureColumns="true" OpenInNewWindow="True">
                            <Excel AutoFitImages="True" Format="Biff" />
                            <Excel AutoFitImages="True" Format="Biff" />
                            <Excel AutoFitImages="True" Format="Biff" />
                        </ExportSettings>
                            <MasterTableView CommandItemDisplay="Top" DataKeyNames="" GroupLoadMode="Client" NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay ingresos.">
                             <DetailTables>
                                    <telerik:GridTableView runat="server" AllowFilteringByColumn="False">

                                        <CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="False" ShowExportToExcelButton="True" />
                                        <CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="False" ShowExportToExcelButton="True" />

                            <CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="false"
                                ShowExportToExcelButton="true" />
                            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                            </ExpandCollapseColumn>

                            
                            <Columns>
                                <telerik:GridTemplateColumn FilterControlAltText="Filter Descarga column" UniqueName="Descarga"
                                    AllowFiltering="False" Display="False" DataField="HojaRuta">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chbDescarga" runat="server" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn2 column"
                                    UniqueName="Boton" AllowFiltering="False">
                                    <ItemTemplate >
                                        <telerik:RadButton ID="ImageButton1" runat="server" CommandName="descarga"  ImageUrl="Images/16/index_down_16.png"
                                            OnClientClick="radconfirm('Desea Bajar el Documento de Respaldo?',confirmCallBackSalvar, 300, 100); return false;"
                                            ToolTip="Salvar" Visible="false"  Text="Descargar"  >
                                        </telerik:RadButton>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridButtonColumn FilterControlAltText="Filter descargar column" UniqueName="descargar" CommandName="descarga"  ImageUrl="Images/16/index_down_16.png" ButtonType="ImageButton" CommandArgument="radconfirm('Desea Bajar el Documento de Respaldo?',confirmCallBackSalvar, 300, 100); return false;"  >

                                </telerik:GridButtonColumn>

                                <telerik:GridBoundColumn DataField="HojaRuta" FilterControlAltText="Filter HojaRuta column" FilterControlWidth="70px" HeaderText="Hoja de Ruta" ItemStyle-HorizontalAlign="Left" UniqueName="HojaRuta">
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Material" FilterControlAltText="Filter Material column" FilterControlWidth="90px" HeaderText="Matearial" ItemStyle-HorizontalAlign="Left" UniqueName="Material">
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="MedioPago" FilterControlAltText="Filter MedioPago column" FilterControlWidth="100px" HeaderText="Medio Pago" ItemStyle-HorizontalAlign="Left" UniqueName="MedioPago">
                                    <ItemStyle HorizontalAlign="Center" />
                                </telerik:GridBoundColumn>
                                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="descarga" FilterControlAltText="Filter descarga column" ImageUrl="Images/16/index_down_16.png" UniqueName="descarga" Visible="False">
                                    <ItemStyle Width="90px" />
                                </telerik:GridButtonColumn>
                                <telerik:GridBoundColumn DataField="Monto" FilterControlAltText="Filter Monto column" HeaderText="Monto" UniqueName="Monto" Visible="False">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="IVA" FilterControlAltText="Filter IVA column" HeaderText="IVA" UniqueName="IVA" Visible="False">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Total" DataFormatString="{0:#,###.00}" FilterControlAltText="Filter Total column" FilterControlWidth="60px" HeaderText="Monto a Pagar" ItemStyle-HorizontalAlign="Left" UniqueName="Total">
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Clientes" FilterControlAltText="Filter Cliente column" FilterControlWidth="270px" HeaderText="Cliente" ItemStyle-HorizontalAlign="Left" UniqueName="Cliente">
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Observacion" FilterControlAltText="Filter Observacion column" FilterControlWidth="100px" HeaderText="Observacion" ItemStyle-HorizontalAlign="Left" UniqueName="Observacion">
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="FechaPago" DataFormatString="{0:dd-MM-yyyy}" Display="False" FilterControlAltText="Filter FechaPago column" FilterControlWidth="60px" HeaderText="Fecha Pago" ItemStyle-HorizontalAlign="Left" UniqueName="FechaPago">
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Aduana" Display="False" FilterControlAltText="Filter Aduana column" FilterControlWidth="100px" HeaderText="Aduana" ItemStyle-HorizontalAlign="Left" UniqueName="Aduana">
                                    <ItemStyle HorizontalAlign="Left" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="NumeroReciboDigitalizado" FilterControlAltText="Filter column column" HeaderText="Referencias" UniqueName="column">
                                </telerik:GridBoundColumn>
                            </Columns>


                                          <EditFormSettings>
                                              <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                              </EditColumn>
                                        </EditFormSettings>


                                          </telerik:GridTableView>
                                </DetailTables>
                            <EditFormSettings>
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                </EditColumn>
                            </EditFormSettings>
                                <CommandItemSettings ExportToPdfText="Export to PDF" />
                                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                                </ExpandCollapseColumn>
                                <Columns>

                                    <telerik:GridBoundColumn DataField="IdDigitalizacion" FilterControlAltText="Filter column6 column" FilterControlWidth="85%" HeaderText="Referencia Sistema" UniqueName="IdDigitalizacion"  HeaderStyle-HorizontalAlign="Left">
                                    <ItemStyle HorizontalAlign="Left" />
                                         </telerik:GridBoundColumn>
                                    
                                    <telerik:GridBoundColumn DataField="Total" DataFormatString="{0:#,###.00}" FilterControlAltText="Filter Total column" FilterControlWidth="45%" HeaderText="Monto a Pagar" UniqueName="Total" HeaderStyle-HorizontalAlign="Center">
                                        <ItemStyle HorizontalAlign="Center"/>
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Proveedor" FilterControlAltText="Filter Proveedor column" FilterControlWidth="85%" HeaderText="Proveedor"  UniqueName="Proveedor" HeaderStyle-HorizontalAlign="Left">
                                        <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridDateTimeColumn FilterControlAltText="Filter fechatesoreria column" HeaderText="Fecha de Pago" UniqueName="fechatesoreria" FilterControlWidth="50%" DataField="FechaPagoTesoreria" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center">
                                      <ItemStyle HorizontalAlign="Center" />
                                    </telerik:GridDateTimeColumn>
                                    <telerik:GridTemplateColumn AllowFiltering="False" FilterControlAltText="Filter TemplateColumn1 column"   HeaderText="No. SAP" UniqueName="TemplateColumn1" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <telerik:RadTextBox ID="txtNumeroSAP" runat="server" AutoPostBack="False" Width="100px">
                                            </telerik:RadTextBox>
                                        </ItemTemplate>
                                          <ItemStyle HorizontalAlign="Center" />
                                    </telerik:GridTemplateColumn>

                                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Guardar"
                                    ImageUrl="Images/16/lock2_16.png" UniqueName="Tiempo" HeaderText="Guardar" >
                                    <HeaderStyle Width="75px" />
                                          <ItemStyle HorizontalAlign="Left" />
                                    </telerik:GridButtonColumn>
                                </Columns>
                               
                        </MasterTableView>
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                        <PagerStyle HorizontalAlign="Left" />
                        <FilterMenu EnableImageSprites="False">
                        </FilterMenu>
                    </telerik:RadGrid>
                    <br />
                    <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="~/Images/gris.png" OnClick="btnSalvar_Click"   Visible="false" Display="false"/>
                    <asp:Button ID="btnCargar" runat="server" Text="Cargar Archivo" Visible="false" />
                    <telerik:RadButton ID="btnGuardar" runat="server" OnClick="btnGuardar_Click" Text="Guardar"  Display="false" Visible="false"
                        Height="26px">
                    </telerik:RadButton>
                    <br />
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
    <br />
    <%--<GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>--%>
    <table class="style13" __designer:mapid="60">
        <tr __designer:mapid="61">
            <td class="style15" __designer:mapid="62">
                &nbsp;
            </td>
            <td class="style16" __designer:mapid="63">
                &nbsp;
            </td>
            <td class="style14" __designer:mapid="65">
                &nbsp;
            </td>
            <td __designer:mapid="67">
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
