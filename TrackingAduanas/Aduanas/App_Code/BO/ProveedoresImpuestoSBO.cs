using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for EventosBO
/// </summary>
public class ProveedoresImpuestoSBO : CapaBase
{

    class Campos
    {

        public static string IDPROVEEDOR = "IdProveedor";
        public static string IDCUENTA = "IdCuenta";
        public static string P_ISR = "P_ISR";
        public static string P_ISV = "P_ISV";
        public static string MEDIOPAGO = "Mediopago";
        public static string IDGASTO = "Idgasto";


    }

    public ProveedoresImpuestoSBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from ProveedoresImpuestoS PIS";
        this.initializeSchema("ProveedoresImpuestoS");
    }

    public void loadAutorizaciones()
    {
        string sql = this.coreSQL;
        sql += " where Estado = '0'";
        this.loadSQL(sql);
    }

    #region QUERYS
    public void VerificarImpuesto(string IdProveedor, string IdGasto)
    {
        string sql = @"Select * From ProveedoresImpuestoS Where IdProveedor = '" + IdProveedor + "' and Idgasto = '" + IdGasto + "'";
        this.loadSQL(sql);
    }
    #endregion

    #region Campos
    public string IDPROVEEDOR
    {
        get
        {
            return (string)registro[Campos.IDPROVEEDOR].ToString();
        }
        set
        {
            registro[Campos.IDPROVEEDOR] = value;
        }
    }
    public string IDCUENTA
    {
        get
        {
            return (string)registro[Campos.IDCUENTA].ToString();
        }
        set
        {
            registro[Campos.IDCUENTA] = value;
        }
    }
    public int P_ISR
    {
        get
        {
            return int.Parse(registro[Campos.P_ISR].ToString());
        }
        set
        {
            registro[Campos.P_ISR] = value;
        }
    }
    public int P_ISV
    {
        get
        {
            return int.Parse(registro[Campos.P_ISV].ToString());
        }
        set
        {
            registro[Campos.P_ISV] = value;
        }
    }
    public string MEDIOPAGO
    {
        get
        {
            return (string)registro[Campos.MEDIOPAGO].ToString();
        }
        set
        {
            registro[Campos.MEDIOPAGO] = value;
        }
    }
    public string IDGASTO
    {
        get
        {
            return (string)registro[Campos.IDGASTO].ToString();
        }
        set
        {
            registro[Campos.IDGASTO] = value;
        }
    }
    #endregion
}
