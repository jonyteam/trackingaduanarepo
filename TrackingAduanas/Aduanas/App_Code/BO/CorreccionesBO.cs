﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;

using GrupoLis.Ebase;
using GrupoLis.Login;

public class CorreccionesBO : CapaBase
{
    class Campos
    {
        public static string IDCORRECCION = "IdCorreccion";
        public static string IDINSTRUCCION = "IdInstruccion";
        public static string CODADUANA = "CodAduana";
        public static string SERIE = "Series";
        public static string FECHASOLICITUD = "FechaSolicitud";
        public static string USUARIOSOLICITO = "UsuarioSolicito";
        public static string FECHARESOLVIO = "FechaResolvio";
        public static string USUARIORESOLVIO = "UsuarioResolvio";
        public static string ESTADO = "Estado";
        public static string PARTIDA = "Partida";
        public static string CODESPECIE = "CodEspecie";
        public static string USUARIOASIGNOESPECIE = "UsuarioAsignoEspecie";
    }

    public CorreccionesBO(GrupoLis.Login.Login conector)
        : base(conector, true)
    {
        coreSQL = " SELECT * FROM Correcciones ";
        initializeSchema("Correcciones");
    }

    public void LoadCorreosXPais(string codPais, string departamento)
    {
        string sql = this.coreSQL;
        sql += " WHERE Pais = '" + codPais + "' AND Estado = '0' and Departamento = '" + departamento + "' ";
        this.loadSQL(sql);
    }
    public void LoadCorrecionesXId(string Id)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdCorreccion = '" + Id + "' AND Estado = '0'";
        this.loadSQL(sql);
    }

    public void LoadCarga()
    {
        string sql = @"SELECT Convert(Varchar,A.FechaResolvio,104) as 'Fec.docu.' ,'SA' as 'Clase Dto.','9095' as Sociedad,Convert(Varchar,GETDATE(),104) as 'Fec.Con.','10' as Periodo
        ,'HNL' as Moneda,'1000010' as 'No.Dto.','USO ESPECIE' as Referencia,'USO ESPECIE FISCALES' +' '+ B.NombreAduana 'TxtCabDto','9095' as 'Divi.'
        , '50' as CLAVE, '40' as CLAVE2, case When A.CodEspecie in ('HN-MN','SS','HN-MI') Then '11312065'  else '11312030' end as Cuenta2,
        A.IdInstruccion as Asignacion2, A.IdInstruccion as Asignacion1
        ,Case When E.IdUsuario = '62' Then '11411010' When E.IdUsuario = '402' Then '11411027' Else C.Codigo End as Codigo
        ,[Series] as 'Referencia 3', 'Liberacion  '+D.Descripcion +' El Cliente '+ F.Nombre as Texto
        ,A.CodEspecie,A.FechaResolvio,A.UsuarioResolvio,A.Estado, Series, A.CodEspecie as CodigoEspecieFiscal,A.IdCorreccion
        FROM Correcciones  A Inner Join Aduanas B on ( Convert(Varchar(5) ,A.IdInstruccion) = ('H-'+B.CodigoAduana) and B.CodEstado = 0 and A.Series !='')
        Inner Join Codigos C on (C.Categoria = 'INVENTARIO' and C.Descripcion = B.NombreAduana and B.CodEstado = 0 and C.Eliminado = 0)
        Inner Join Codigos D on (D.Categoria = 'ESPECIESFISCALES' and D.Codigo = A.CodEspecie and D.Eliminado = 0)
        Inner Join Instrucciones E on (A.IdInstruccion = E.IdInstruccion and E.CodPaisHojaRuta = 'H')
        Inner Join Clientes F on (E.IdCliente = F.CodigoSAP and F.Eliminado = 0)
        Inner Join Instrucciones G on (A.IdInstruccion = G.IdInstruccion and G.CodPaisHojaRuta = 'H')
        Inner Join Clientes H on (G.IdCliente = H.CodigoSAP and F.Eliminado = 0)
        Where A.Estado = 0 and A.partida !='3' 
        order by A.IdCorreccion";
        this.loadSQL(sql);
    }
    public void CargaReasignacionCambioEstado()
    {
        string sql = @"Update Correcciones  Set Estado = 1
        FROM Correcciones  A Inner Join Aduanas B on ( Convert(Varchar(5) ,A.IdInstruccion) = ('H-'+B.CodigoAduana) and B.CodEstado = 0 and A.Series !='')
        Inner Join Codigos C on (C.Categoria = 'INVENTARIO' and C.Descripcion = B.NombreAduana and B.CodEstado = 0 and C.Eliminado = 0)
        Inner Join Codigos D on (D.Categoria = 'ESPECIESFISCALES' and D.Codigo = A.CodEspecie and D.Eliminado = 0)
        Inner Join Instrucciones E on (A.IdInstruccion = E.IdInstruccion and E.CodPaisHojaRuta = 'H')
        Inner Join Clientes F on (E.IdCliente = F.CodigoSAP and F.Eliminado = 0)
        Inner Join Instrucciones G on (A.IdInstruccion = G.IdInstruccion and G.CodPaisHojaRuta = 'H')
        Inner Join Clientes H on (G.IdCliente = H.CodigoSAP and F.Eliminado = 0)
        Where A.Estado = 0";
        this.loadSQL(sql);
    }

    public void CargaCorrecciones(string FechaInicio, string FechaFinal)
    {
        string sql = @"Select A.IdInstruccion as Hoja,C.Nombre as Cliente,D.NombreAduana as Aduana,Series as Serie, E.Descripcion as Especie From Correcciones A
                        Inner Join Instrucciones B on (A.IdInstruccion = B.IdInstruccion)
                        Inner Join Aduanas D on (B.CodigoAduana = D.CodigoAduana)
                        Inner Join Clientes C on (B.IdCliente = C.CodigoSAP)
                        Inner Join Codigos E on (E.Categoria = 'ESPECIESFISCALES' and E.Codigo= A.CodEspecie)
                        Where A.FechaResolvio >= '" + FechaInicio + "' and A.FechaResolvio <='" + FechaFinal + "'";
        this.loadSQL(sql);
    }


    public void Actualizar_Adiciones() { 
    string sql = @"Update Correcciones  Set Estado = 1
                FROM Correcciones  A Inner Join Aduanas B on ( Convert(Varchar(5) ,A.IdInstruccion) = ('H-'+B.CodigoAduana) and B.CodEstado = 0 and A.Series !='')
                Inner Join Codigos C on (C.Categoria = 'INVENTARIO' and C.Descripcion = B.NombreAduana and B.CodEstado = 0 and C.Eliminado = 0)
                Inner Join Codigos D on (D.Categoria = 'ESPECIESFISCALES' and D.Codigo = A.CodEspecie and  A.Estado = 0 and D.Eliminado = 0 and A.Partida = 3)  
                Inner Join Instrucciones E on (A.IdInstruccion = E.IdInstruccion and E.CodEstado in (0,1) and E.CodPaisHojaRuta = 'H')
                Inner Join Clientes F on (E.IdCliente = F.CodigoSAP and F.Eliminado = 0)";
    this.loadSQL(sql);
    }


    #region Campos

    public String CODESPECIE
    {
        get
        {
            return Convert.ToString(registro[Campos.CODESPECIE]);
        }
        set
        {
            registro[Campos.CODESPECIE] = value;
        }
    }
    public String IDCORRECCION
    {
        get
        {
            return Convert.ToString(registro[Campos.IDCORRECCION]);
        }
        set
        {
            registro[Campos.IDCORRECCION] = value;
        }
    }
    public String IDINSTRUCCION
    {
        get
        {
            return Convert.ToString(registro[Campos.IDINSTRUCCION]);
        }
        set
        {
            registro[Campos.IDINSTRUCCION] = value.Trim();
        }
    }
    public String CODADUANA
    {
        get
        {
            return Convert.ToString(registro[Campos.CODADUANA]);
        }
        set
        {
            registro[Campos.CODADUANA] = value.Trim();
        }
    }
    public String SERIE
    {
        get
        {
            return Convert.ToString(registro[Campos.SERIE]);
        }
        set
        {
            registro[Campos.SERIE] = value.Trim();
        }
    }
    public String FECHASOLICITUD
    {
        get
        {
            return Convert.ToString(registro[Campos.FECHASOLICITUD]);
        }
        set
        {
            registro[Campos.FECHASOLICITUD] = value.Trim();
        }
    }
    public String USUARIOSOLICITO
    {
        get
        {
            return Convert.ToString(registro[Campos.USUARIOSOLICITO]);
        }
        set
        {
            registro[Campos.USUARIOSOLICITO] = value.Trim();
        }
    }
    public String FECHARESOLVIO
    {
        get
        {
            return Convert.ToString(registro[Campos.FECHARESOLVIO]);
        }
        set
        {
            registro[Campos.FECHARESOLVIO] = value.Trim();
        }
    }
    public String USUARIORESOLVIO
    {
        get
        {
            return Convert.ToString(registro[Campos.USUARIORESOLVIO]);
        }
        set
        {
            registro[Campos.USUARIORESOLVIO] = value.Trim();
        }
    }
    public String ESTADO
    {
        get
        {
            return Convert.ToString(registro[Campos.ESTADO]);
        }
        set
        {
            registro[Campos.ESTADO] = value.Trim();
        }
    }
    public String PARTIDA
    {
        get
        {
            return Convert.ToString(registro[Campos.PARTIDA]);
        }
        set
        {
            registro[Campos.PARTIDA] = value.Trim();
        }
    }
    public String USUARIOASIGNOESPECIE
    {
        get
        {
            return Convert.ToString(registro[Campos.USUARIOASIGNOESPECIE]);
        }
        set
        {
            registro[Campos.USUARIOASIGNOESPECIE] = value.Trim();
        }
    }
    #endregion
}
