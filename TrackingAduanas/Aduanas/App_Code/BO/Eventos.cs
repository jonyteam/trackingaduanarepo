using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for EventosBO
/// </summary>
public class Eventos : CapaBase
{

    class Campos
    {

        public static string ID = "Id";
        public static string IDINSTRUCCION = "IdInstruccion";
        public static string IDEVENTO = "IdEvento";
        public static string FECHAINICIO = "FechaInicio";
        public static string FECHAFIN = "FechaFin";
        public static string FECHAINICIOINGRESO = "FechaInicioIngreso";
        public static string FECHAFININGRESO = "FechaFinIngreso";
        public static string OBSERVACIONINICIO = "ObservacionInicio";
        public static string OBSERVACIONFIN = "ObservacionFin";
        public static string ELIMINADO = "Eliminado";
        public static string IDUSUARIOINICIO = "IdUsuarioInicio";
        public static string IDUSUARIOFIN = "IdUsuarioFin";

    }

    public Eventos(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from Eventos";
        this.initializeSchema("Eventos");
    }

    public void loadAutorizaciones()
    {
        string sql = this.coreSQL;
        sql += " where Estado = '0'";
        this.loadSQL(sql);
    }

    public void cargargrid(string hojaruta)
    {
        string sql = "SELECT dbo.FlujoCarga.EstadoFlujo as [Gestion de Carga], dbo.GestionCarga.Fecha as [Fecha Gestor], dbo.GestionCarga.Observacion from [AduanasNueva].[dbo].[GestionCarga] inner join FlujoCarga on GestionCarga.IdEstadoFlujo = FlujoCarga.IdEstadoFlujo inner join Instrucciones on GestionCarga.IdInstruccion = Instrucciones.IdInstruccion where dbo.GestionCarga.IdInstruccion = '" + hojaruta + "'";
        this.loadSQL(sql);
    }

    public void ImpuestosEsquema(string hojaruta)
    {
        string sql = @" use EsquemaTramites2
SELECT 
      S.Concepto,H.MedioPago,H.Monto,H.ValorReal,H.NumeroSAP,H.NotaDebito
  FROM HojaRutaEsquemaTramites H 
  inner join ConceptoSAP S
  on H.IdConceptoSAP = S.ID and H.IdConceptoSAP  in (81,82,83,165,20,
  18,20,21,27,29,41,43,44,77,78,98,132,137,138,143,161,162,163,171,173,178,179)

and H.IdHojaRuta ='" + hojaruta + "'";
        this.loadSQL(sql);
    }

    public void ImpuestosAduanas(string hojaruta)
    {
        string sql = @"select  Impuesto,	ISV,	DAI,	Selectivo,	STD,	ProduccionConsumo,	OtrosImpuestos,	Liquidacion,	NoCorrelativo from Instrucciones where IdInstruccion='" + hojaruta + "'";
        this.loadSQL(sql);
    }




    public void loadAutorizacionesEstado()
    {
        this.loadSQL(this.coreSQL);
    }

    public void mostrardatos(string hojaruta)
    {
        string sql = "  Select C.Descripcion ,FechaInicio,FechaFin,ObservacionFin,ObservacionInicio,CL.Nombre,I.NumeroFactura,I.ReferenciaCliente from Codigos C";
        sql += " Inner Join Eventos E on (IdInstruccion ='" + hojaruta + "' and C.Codigo = IdEvento and C.Categoria = 'EVENTOS' and E.Eliminado = 1)";
        sql += " Inner Join Instrucciones I on (E.IdInstruccion = I.IdInstruccion and I.CodEstado != 100)";
        sql += " Inner Join Clientes CL on (I.IdCliente = CL.CodigoSAP)";
        this.loadSQL(sql);
    }

    public void mostrardatos2(string hojaruta)
    {
        string sql = @"SELECT ISNULL(FC.EstadoFlujo, 'Comienzo de Flujo/Doc Minimos') as GestionCarga, GC.Fecha as FechaGestor, I.Color,GC.Observacion as Observacion , fc.Orden  
                      from GestionCarga GC
                    left join FlujoCarga FC on (GC.IdEstadoFlujo = FC.IdEstadoFlujo) 
                    inner join Instrucciones I on ('" + hojaruta + "' = I.IdInstruccion and I.CodEstado != 100) " +
                    "where GC.IdInstruccion = '" + hojaruta + "' and GC.Eliminado = '0'"+


        @" 
           union select  
		   ISNULL( c.descripcion, 'Comienzo de Flujo/Doc Minimos') as GestionCarga 
		  ,tf.fecha  as FechaGestor,pf.CanalSelectividad as color, tf.Observacion as Observacion, c.Orden as orden 
		   from TiemposFlujoCarga  tf
		    inner join codigos c on (c.categoria='ESTADO FLUJO CARGA AUTOMATICO'  and c.Eliminado='0' and tf.Eliminado='0' and tf.idestado=c.Codigo )
		    left join  ProcesoFlujoCarga pf on ( pf.idinstruccion=tf.idinstruccion and pf.Eliminado='0')
			where tf.IdInstruccion ='" + hojaruta + "' and tf.Eliminado = '0' order by  FechaGestor";

        this.loadSQL(sql);
    }

    public void loadAutorizacionesRoles(string idRol)
    {
        string sql = this.coreSQL;
        sql += " where IdRol = " + idRol + " and Estado = '0'";
        this.loadSQL(sql);
    }


    public void loadAutorizacionesModulo(string idModulo)
    {
        string sql = this.coreSQL;
        sql += " where IdModulo = " + idModulo + " and Estado = '0'";
        this.loadSQL(sql);
    }

    public void loadAutorizacionesModulosRoles(string idRol, string idModulo)
    {
        string sql = this.coreSQL;
        sql += " where IdRol = " + idRol + " and IdModulo = " + idModulo + " and Estado = '0'";
        this.loadSQL(sql);
    }

    public void loadAutorizacionesModulosRolesEstado(string idRol, string idModulo)
    {
        string sql = this.coreSQL;
        sql += " where IdRol = " + idRol + " and IdModulo = " + idModulo;
        this.loadSQL(sql);
    }

    public void deleAutorizacionesModulos(string idRol, string idModulo)
    {
        string sql = this.coreSQL;
        sql += " where IdRole = " + idRol + " and IdModulo = " + idModulo;
        this.executeCustomSQL(String.Format("delete from AutorizacionesModulo where IdRol={0} and IdModulo={1}", idRol, idModulo));
    }

    //public string IDROL
    //{
    //    get
    //    {
    //        return (string)registro[Campos.IDROL].ToString();
    //    }
    //    set
    //    {
    //        registro[Campos.IDROL] = value;
    //    }
    //}

    //public string IDMODULO
    //{
    //    get
    //    {
    //        return (string)registro[Campos.IDMODULO].ToString();
    //    }
    //    set
    //    {
    //        registro[Campos.IDMODULO] = value;
    //    }
    //}

    //public string ESTADO
    //{
    //    get
    //    {
    //        return (string)registro[Campos.ESTADO].ToString();
    //    }
    //    set
    //    {
    //        registro[Campos.ESTADO] = value;
    //    }
    //}

    public int eliminarAutorizacionesModulo(string codigo)
    {
        return this.executeCustomSQL("delete from AutorizacionesModulo where IdRol = " + codigo);
    }

    public int darBajaAutorizacionesModulo(string codigo)
    {
        return this.executeCustomSQL("update AutorizacionesModulo set Estado = '1' where IdRol = " + codigo);
    }

}
