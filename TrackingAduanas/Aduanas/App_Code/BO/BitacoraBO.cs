﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for BitacoraBO
/// </summary>
public class BitacoraBO : CapaBase
{
    class Campos
    {
        public static string IDBITACORA = "IdBitacora";
        public static string IDUSUARIO = "IdUsuario";
        public static string ACCION = "Accion";
        public static string IDDOCUMENTO = "IdDocumento";
        public static string FECHA = "Fecha";
    }

    public BitacoraBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from Bitacora";
        this.initializeSchema("Bitacora");
    }

    public void loadBitacora()
    {
        string sql = this.coreSQL;
        this.loadSQL(sql);
    }

    public void loadBitacora(string idBitacora)
    {
        string sql = this.coreSQL + " WHERE IdBitacora = '" + idBitacora + "' ";
        this.loadSQL(sql);
    }

    #region campos
    public string IDBITACORA
    {
        get
        {
            return (string)registro[Campos.IDBITACORA].ToString();
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }

    public string ACCION
    {
        get
        {
            return (string)registro[Campos.ACCION].ToString();
        }
        set
        {
            registro[Campos.ACCION] = value;
        }
    }

    public string IDDOCUMENTO
    {
        get
        {
            return (string)registro[Campos.IDDOCUMENTO].ToString();
        }
        set
        {
            registro[Campos.IDDOCUMENTO] = value;
        }
    }

    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }
    #endregion

}