﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;

using GrupoLis.Ebase;
using GrupoLis.Login;

public class TramiteBO : CapaBase
{
    class Campos
    {
        public static string ID = "Id";
        public static string NTRAMITE = "NTramite";
        public static string NOMBREARCHIVO = "NombreArchivo";
        public static string ETA = "ETA";
        public static string BARCO = "Barco";
        public static string VIAJE = "Viaje";
        public static string BL = "BL";
        public static string CONTENEDOR = "Contenedor";
        public static string REGIMEN = "Regimen";
        public static string ADUANADESTINO = "AduanaDestino";
        public static string CONSIGNATARIO = "Consignatario";
        public static string PRIORIDAD = "Prioridad";
        public static string TAMANO = "Tamano";
        public static string CORRELATIVO = "Correlativo";
        public static string PREIMPRESO = "Preimpreso";
        public static string MARCHAMO = "Marchamo";
        public static string FACTURA = "Factura";
        public static string DISCREPANCIA = "Discrepancia";
        public static string IDNAVIERA = "IdNaviera";
        public static string IDUSUARIO = "IdUsuario";
        public static string FECHACARGA = "FechaCarga";
        public static string FECHAFACTURA = "FechaFactura";
        public static string FECHATRANSMTIR = "FechaTransmtir";
        public static string FECHAFINALIZAR = "FechaFinalizar";
        public static string DESPACHO = "Despacho";
        public static string DEFINITIVO = "Definitivo";
        public static string ESTADO = "Estado";
        public static string ELIMINADO = "Eliminado";
        public static string FECHAOPC = "FechaOpc";
        public static string FECHAEMBARQUE = "FechaEmbarque";
        public static string MANIFIESTO = "Manifiesto";
        public static string DIGITADOR = "Digitador";
        public static string EXTENSION = "Extension";
    }

    public TramiteBO(GrupoLis.Login.Login conector)
        : base(conector, true)
    {
        coreSQL = " SELECT * FROM Tramite ";
        initializeSchema("Tramite");
    }
    public void LoadTramite(string Tramite)
    {
        string sql = @"Select * From Tramite Where Estado in (0,1,2,3) and NTramite = '" + Tramite + "'";
        this.loadSQL(sql);
    }

    #region Campos
    public String NTRAMITE
    {
        get
        {
            return Convert.ToString(registro[Campos.NTRAMITE]);
        }
        set
        {
            registro[Campos.NTRAMITE] = value;
        }
    }

    public String PREIMPRESO
    {
        get
        {
            return Convert.ToString(registro[Campos.PREIMPRESO]);
        }
        set
        {
            registro[Campos.PREIMPRESO] = value.Trim();
        }
    }

    public String MARCHAMO
    {
        get
        {
            return Convert.ToString(registro[Campos.MARCHAMO]);
        }
        set
        {
            registro[Campos.MARCHAMO] = value;
        }
    }

    public String EXTENSION
    {
         get
        {
            return Convert.ToString(registro[Campos.EXTENSION]);
        }
        set
        {
            registro[Campos.EXTENSION] = value.Trim();
        }
    }

    public String ESTADO
    {
        get
        {
            return Convert.ToString(registro[Campos.ESTADO]);
        }
        set
        {
            registro[Campos.ESTADO] = value.Trim();
        }
    }
    #endregion
}
