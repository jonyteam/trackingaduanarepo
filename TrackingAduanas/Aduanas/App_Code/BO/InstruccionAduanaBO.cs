using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for AduanasBO
/// </summary>
public class InstruccionAduanaBO : CapaBase
{
    class Campos
    {
        public static string ITEM = "Item";
        public static string INSTRUCCION = "Instruccion";
        public static string CODIGOCLIENTE = "CodigoCliente";
        public static string PRODUCTO = "Producto";
        public static string TARIFA = "Tarifa";
        public static string ADUANA = "Aduana";
        public static string REGIMEN = "Regimen";
        public static string ORIGEN = "Origen";
        public static string PARTIDAS = "Partidas";
        public static string IMPUESTOS = "Impuestos";
        public static string GASTOSPORTUARIOS = "GastosPortuarios";
        public static string GASTOSOPERATIVOS = "GastosOperativos";
        public static string RADIO = "Radio";
        public static string FOTOCOPIAS = "Fotocopias";
        public static string ENCOMIENDA = "Encomienda";
        public static string MARCHAMOS = "Marchamos";
        public static string PAGARES = "Pagares";
        public static string GARANTIA = "Garantia";
        public static string GARANTIASNAVIERA = "GarantiasNaviera";
        public static string VETERINARIO = "Veterinario";
        public static string DIGITACION = "Digitacion";
        public static string POLIZA = "Poliza";
        public static string VALORADUANERO = "ValorAduanero";
        public static string BCH = "BCH";
        public static string DTI = "DTI";
        public static string FAUCA = "FAUCA";
        public static string SENASA = "SENASA";
        public static string EXTENSIONPOLIZA = "ExtensionPoliza";
        public static string COMISION = "Comision";
        public static string MANEJOADUANAL = "ManejoAduanal";
        public static string DESPACHOADUANAL = "DespachoAduanal";
        public static string CIF = "CIF";
        public static string PESOKG = "PesoKg";
        public static string DESCUENTOCOMISION = "DescuentoComision";
        public static string DESCUENTOMANEJO = "DescuentoManejo";
        public static string DESCUENTODESPACHO = "DescuentoDespacho";
        public static string FECHA = "Fecha";
        public static string OBSERVACION = "Observacion";
        public static string CODESTADO = "CodEstado";
        public static string IDUSUARIO = "IdUsuario";
        public static string RETROALIMENTACION = "Retroalimentacion";
        public static string NOMBRE = "Nombre";
        public static string PROVEEDOR = "Proveedor";
        public static string REFERENCIACLIENTE = "ReferenciaCliente";
    }

    public InstruccionAduanaBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from InstruccionAduana IA ";
        this.initializeSchema("InstruccionAduana");
    }

    public void loadInstruccionAduana()
    {
        this.loadSQL(this.coreSQL);
    }

    public void loadInstruccionesAduana(string baseDatos)
    {
        string sql = " Select I.IdInstruccion AS Instruccion, C.Nombre, I.ReferenciaCliente, I.CodEstado AS Estado, I.Item, DGD.NoManifiesto, I.NumeroFactura from " + baseDatos.Trim() + ".dbo.Instrucciones I ";
        sql += " INNER JOIN " + baseDatos.Trim() + ".dbo.Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        sql += "  LEFT JOIN " + baseDatos.Trim() + ".dbo.DatosGestionDocumentos DGD ON (DGD.IdInstruccion = I.IdInstruccion AND DGD.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('1','2') ";
        sql += " UNION ";
        sql += " Select CAST(Instruccion as varchar) AS Instruccion, C.NombreEmpresa COLLATE Modern_Spanish_CI_AS AS Nombre, ";
        sql += " '' AS ReferenciaCliente, '0' Estado, -1 AS Item, '' AS NoManifiesto,'' as 'I.NumeroFactura' from InstruccionAduana IA ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = CAST(IA.CodigoCliente as varchar) COLLATE Modern_Spanish_CI_AS) ";
        sql += " WHERE CodEstado = '0'";
        sql += " ORDER BY I.CodEstado DESC, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesAduana(string codPais, string baseDatos)
    {
        string sql = " Select I.IdInstruccion AS Instruccion, C.Nombre, I.ReferenciaCliente, I.CodEstado AS Estado, I.Item, DGD.NoManifiesto,I.NumeroFactura from " + baseDatos.Trim() + ".dbo.Instrucciones I ";
        sql += " INNER JOIN " + baseDatos.Trim() + ".dbo.Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        sql += "  LEFT JOIN " + baseDatos.Trim() + ".dbo.DatosGestionDocumentos DGD ON (DGD.IdInstruccion = I.IdInstruccion AND DGD.Eliminado = '0') ";
        sql += " WHERE I.CodEstado = '1' AND I.CodPaisHojaRuta = '" + codPais + "' ";
        sql += " UNION ";
        sql += " Select CAST(Instruccion as varchar) AS Instruccion, C.NombreEmpresa COLLATE Modern_Spanish_CI_AS AS Nombre, ";
        sql += " '' AS ReferenciaCliente, '0' Estado, -1 AS Item, '' AS NoManifiesto,'' as 'I.NumeroFactura' from InstruccionAduana IA ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = CAST(IA.CodigoCliente as varchar) COLLATE Modern_Spanish_CI_AS) ";
        sql += " WHERE CodEstado = '0'";
        sql += " ORDER BY I.CodEstado DESC, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesAduanaXAd(string codPais, string baseDatos, string codAduana)
    {
        string sql = " Select I.IdInstruccion AS Instruccion, C.Nombre, I.ReferenciaCliente, I.CodEstado AS Estado, I.Item, DGD.NoManifiesto,I.NumeroFactura from " + baseDatos.Trim() + ".dbo.Instrucciones I ";
        sql += " INNER JOIN " + baseDatos.Trim() + ".dbo.Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        sql += "  LEFT JOIN " + baseDatos.Trim() + ".dbo.DatosGestionDocumentos DGD ON (DGD.IdInstruccion = I.IdInstruccion AND DGD.Eliminado = '0') ";
        sql += " WHERE I.CodEstado = '1' AND I.CodPaisHojaRuta = '" + codPais + "' AND I.CodigoAduana = '" + codAduana + "' ";
        sql += " UNION ";
        sql += " Select CAST(Instruccion as varchar) AS Instruccion, C.NombreEmpresa COLLATE Modern_Spanish_CI_AS AS Nombre, ";
        sql += " '' AS ReferenciaCliente, '0' Estado, -1 AS Item, '' AS NoManifiesto,'' as 'I.NumeroFactura' from InstruccionAduana IA ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = CAST(IA.CodigoCliente as varchar) COLLATE Modern_Spanish_CI_AS) ";
        sql += " WHERE CodEstado = '0' AND IA.Aduana = '" + codAduana + "' ";
        sql += " ORDER BY I.CodEstado DESC, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesAduanaXUsuario(string codPais, string baseDatos, string idUsuario)
    {
        string sql = " Select I.IdInstruccion AS Instruccion, C.Nombre, I.ReferenciaCliente, I.CodEstado AS Estado, I.Item, DGD.NoManifiesto,I.NumeroFactura from " + baseDatos.Trim() + ".dbo.Instrucciones I ";
        sql += " INNER JOIN " + baseDatos.Trim() + ".dbo.Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        sql += "  LEFT JOIN " + baseDatos.Trim() + ".dbo.DatosGestionDocumentos DGD ON (DGD.IdInstruccion = I.IdInstruccion AND DGD.Eliminado = '0') ";
        sql += " WHERE I.CodEstado = '1' AND I.CodPaisHojaRuta = '" + codPais + "' AND I.IdUsuario = '" + idUsuario + "' ";
        sql += " UNION ";
        sql += " Select CAST(Instruccion as varchar) AS Instruccion, C.NombreEmpresa COLLATE Modern_Spanish_CI_AS AS Nombre, ";
        sql += " '' AS ReferenciaCliente, '0' Estado, -1 AS Item, '' AS NoManifiesto,'' as 'I.NumeroFactura' from InstruccionAduana IA ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = CAST(IA.CodigoCliente as varchar) COLLATE Modern_Spanish_CI_AS) ";
        sql += " WHERE CodEstado = '0' AND IA.IdUsuario = '" + idUsuario + "' ";
        sql += " ORDER BY I.CodEstado DESC, I.Item ";
        this.loadSQL(sql);
    }

    public void loadclienteIA()
    {
        string sql = " select it.*, c.NombreEmpresa from InstruccionAduana it";
        sql += " inner join Clientes c on c.IdCliente = it.CodigoCliente";
        sql += " where CodEstado = '0'";
        this.loadSQL(sql);
    }

    public void loadSolicitudesFletesGuias()
    {
        this.loadSQL(this.coreSQL + " WHERE CodEstado IN ('0','1') ");
    }

    public void loadSolicitudesFletesXCliente(string codCliente)
    {
        this.loadSQL(this.coreSQL + " WHERE CodigoCliente = '" + codCliente + "' AND CodEstado = '0' ");
    }

    public void loadSolicitudFlete(string Item)
    {
        this.loadSQL(this.coreSQL + " WHERE Item = '" + Item + "' AND CodEstado = '0' ");
    }
    public void correos (string Instruccion)
    {
        string sql = "Select Nombre,Proveedor,Producto,IdInstruccion,ReferenciaCliente from Instrucciones ";
               sql += " inner join Clientes on ( IdCliente = CodigoSAP)where IdInstruccion = '"+Instruccion+"'";
               this.loadSQL(sql);
    }


    #region Campos
    public string ITEM
    {
        get
        {
            return (string)registro[Campos.ITEM].ToString();
        }
        set
        {
            registro[Campos.ITEM] = value;
        }
    }

    public string INSTRUCCION
    {
        get
        {
            return (string)registro[Campos.INSTRUCCION].ToString();
        }
        set
        {
            registro[Campos.INSTRUCCION] = value;
        }
    }

    public string CODIGOCLIENTE
    {
        get
        {
            return (string)registro[Campos.CODIGOCLIENTE].ToString();
        }
        set
        {
            registro[Campos.CODIGOCLIENTE] = value;
        }
    }

    public string PRODUCTO
    {
        get
        {
            return (string)registro[Campos.PRODUCTO].ToString();
        }
        set
        {
            registro[Campos.PRODUCTO] = value;
        }
    }

    public string TARIFA
    {
        get
        {
            return (string)registro[Campos.TARIFA].ToString();
        }
        set
        {
            registro[Campos.TARIFA] = value;
        }
    }

    public string ADUANA
    {
        get
        {
            return (string)registro[Campos.ADUANA].ToString();
        }
        set
        {
            registro[Campos.ADUANA] = value;
        }
    }

    public string REGIMEN
    {
        get
        {
            return (string)registro[Campos.REGIMEN].ToString();
        }
        set
        {
            registro[Campos.REGIMEN] = value;
        }
    }

    public string ORIGEN
    {
        get
        {
            return (string)registro[Campos.ORIGEN].ToString();
        }
        set
        {
            registro[Campos.ORIGEN] = value;
        }
    }

    public string GASTOSPORTUARIOS
    {
        get
        {
            return (string)registro[Campos.GASTOSPORTUARIOS].ToString();
        }
        set
        {
            registro[Campos.GASTOSPORTUARIOS] = value;
        }
    }

    public string GASTOSOPERATIVOS
    {
        get
        {
            return (string)registro[Campos.GASTOSOPERATIVOS].ToString();
        }
        set
        {
            registro[Campos.GASTOSOPERATIVOS] = value;
        }
    }

    public string RADIO
    {
        get
        {
            return (string)registro[Campos.RADIO].ToString();
        }
        set
        {
            registro[Campos.RADIO] = value;
        }
    }

    public string PARTIDAS
    {
        get
        {
            return (string)registro[Campos.PARTIDAS].ToString();
        }
        set
        {
            registro[Campos.PARTIDAS] = value;
        }
    }

    public string FOTOCOPIAS
    {
        get
        {
            return (string)registro[Campos.FOTOCOPIAS].ToString();
        }
        set
        {
            registro[Campos.FOTOCOPIAS] = value;
        }
    }

    public string IMPUESTOS
    {
        get
        {
            return (string)registro[Campos.IMPUESTOS].ToString();
        }
        set
        {
            registro[Campos.IMPUESTOS] = value;
        }
    }

    public string ENCOMIENDA
    {
        get
        {
            return (string)registro[Campos.ENCOMIENDA].ToString();
        }
        set
        {
            registro[Campos.ENCOMIENDA] = value;
        }
    }

    public string MARCHAMOS
    {
        get
        {
            return (string)registro[Campos.MARCHAMOS].ToString();
        }
        set
        {
            registro[Campos.MARCHAMOS] = value;
        }
    }

    public string PAGARES
    {
        get
        {
            return (string)registro[Campos.PAGARES].ToString();
        }
        set
        {
            registro[Campos.PAGARES] = value;
        }
    }

    public string GARANTIA
    {
        get
        {
            return (string)registro[Campos.GARANTIA].ToString();
        }
        set
        {
            registro[Campos.GARANTIA] = value;
        }
    }

    public string GARANTIASNAVIERA
    {
        get
        {
            return (string)registro[Campos.GARANTIASNAVIERA].ToString();
        }
        set
        {
            registro[Campos.GARANTIASNAVIERA] = value;
        }
    }

    public string VETERINARIO
    {
        get
        {
            return (string)registro[Campos.VETERINARIO].ToString();
        }
        set
        {
            registro[Campos.VETERINARIO] = value;
        }
    }

    public string DIGITACION
    {
        get
        {
            return (string)registro[Campos.DIGITACION].ToString();
        }
        set
        {
            registro[Campos.DIGITACION] = value;
        }
    }

    public string POLIZA
    {
        get
        {
            return (string)registro[Campos.POLIZA].ToString();
        }
        set
        {
            registro[Campos.POLIZA] = value;
        }
    }

    public string VALORADUANERO
    {
        get
        {
            return (string)registro[Campos.VALORADUANERO].ToString();
        }
        set
        {
            registro[Campos.VALORADUANERO] = value;
        }
    }

    public string BCH
    {
        get
        {
            return (string)registro[Campos.BCH].ToString();
        }
        set
        {
            registro[Campos.BCH] = value;
        }
    }

    public string DTI
    {
        get
        {
            return (string)registro[Campos.DTI].ToString();
        }
        set
        {
            registro[Campos.DTI] = value;
        }
    }

    public string FAUCA
    {
        get
        {
            return (string)registro[Campos.FAUCA].ToString();
        }
        set
        {
            registro[Campos.FAUCA] = value;
        }
    }

    public string SENASA
    {
        get
        {
            return (string)registro[Campos.SENASA].ToString();
        }
        set
        {
            registro[Campos.SENASA] = value;
        }
    }

    public string EXTENSIONPOLIZA
    {
        get
        {
            return (string)registro[Campos.EXTENSIONPOLIZA].ToString();
        }
        set
        {
            registro[Campos.EXTENSIONPOLIZA] = value;
        }
    }

    public string COMISION
    {
        get
        {
            return (string)registro[Campos.COMISION].ToString();
        }
        set
        {
            registro[Campos.COMISION] = value;
        }
    }

    public string MANEJOADUANAL
    {
        get
        {
            return (string)registro[Campos.MANEJOADUANAL].ToString();
        }
        set
        {
            registro[Campos.MANEJOADUANAL] = value;
        }
    }

    public string DESPACHOADUANAL
    {
        get
        {
            return (string)registro[Campos.DESPACHOADUANAL].ToString();
        }
        set
        {
            registro[Campos.DESPACHOADUANAL] = value;
        }
    }

    public string CIF
    {
        get
        {
            return (string)registro[Campos.CIF].ToString();
        }
        set
        {
            registro[Campos.CIF] = value;
        }
    }

    public string PESOKG
    {
        get
        {
            return (string)registro[Campos.PESOKG].ToString();
        }
        set
        {
            registro[Campos.PESOKG] = value;
        }
    }

    public string DESCUENTOCOMISION
    {
        get
        {
            return (string)registro[Campos.DESCUENTOCOMISION].ToString();
        }
        set
        {
            registro[Campos.DESCUENTOCOMISION] = value;
        }
    }

    public string DESCUENTOMANEJO
    {
        get
        {
            return (string)registro[Campos.DESCUENTOMANEJO].ToString();
        }
        set
        {
            registro[Campos.DESCUENTOMANEJO] = value;
        }
    }

    public string DESCUENTODESPACHO
    {
        get
        {
            return (string)registro[Campos.DESCUENTODESPACHO].ToString();
        }
        set
        {
            registro[Campos.DESCUENTODESPACHO] = value;
        }
    }



    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }

    public string OBSERVACION
    {
        get
        {
            return (string)registro[Campos.OBSERVACION].ToString();
        }
        set
        {
            registro[Campos.OBSERVACION] = value;
        }
    }

    public string CODESTADO
    {
        get
        {
            return (string)registro[Campos.CODESTADO].ToString();
        }
        set
        {
            registro[Campos.CODESTADO] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }

    public string RETROALIMENTACION
    {
        get
        {
            return (string)registro[Campos.RETROALIMENTACION].ToString();
        }
        set
        {
            registro[Campos.RETROALIMENTACION] = value;
        }
    }
    public string NOMBRE
    {
        get
        {
            return (string)registro[Campos.NOMBRE].ToString();
        }
        set
        {
            registro[Campos.NOMBRE] = value;
        }
    }
    public string PROVEEDOR
    {
        get
        {
            return (string)registro[Campos.PROVEEDOR].ToString();
        }
        set
        {
            registro[Campos.PROVEEDOR] = value;
        }
    }
    public string REFERENCIACLIENTE
    {
        get
        {
            return (string)registro[Campos.REFERENCIACLIENTE].ToString();
        }
        set
        {
            registro[Campos.REFERENCIACLIENTE] = value;
        }
    }
    #endregion

}
