﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;

using GrupoLis.Ebase;
using GrupoLis.Login;

    public class CategoriasBO : CapaBase
    {
        class Campos
        {
            public static string ID = "Id";
            public static string CATEGORIA = "Categoria";
            public static string DESCRIPCION = "Descripcion";
        }

        public CategoriasBO(GrupoLis.Login.Login conector)
            : base(conector, true)
        {
            coreSQL = "SELECT * FROM Categorias";
            initializeSchema("Categorias");
        }

        public void loadAllCampos()
        {
            loadSQL(coreSQL + " WHERE (Eliminado=0)");
        }

        public void updateCategoria(Int32 id, Hashtable nuevosValores)
        {
            if ((nuevosValores != null) && (nuevosValores.Count > 0) && (id > 0))
            {
                StringBuilder sb = new StringBuilder("UPDATE Categorias SET ");
                List<Parametro> parametros = new List<Parametro>(nuevosValores.Count);

                bool cc = false;

                foreach (DictionaryEntry de in nuevosValores)
                {
                    if (cc) sb.Append(',');

                    string key = de.Key.ToString();

                    sb.AppendFormat("{0}=@{0}", key);
                    parametros.Add(new Parametro("@" + key, de.Value));

                    cc = true;
                }

                sb.Append(" WHERE IdCategoria=@Id");
                parametros.Add(new Parametro("@Id", id));

                executeCustomSQL(sb.ToString(), parametros.ToArray());
            }
        }

        public void insertCategoria(Hashtable nuevosValores)
        {
            if ((nuevosValores != null) && (nuevosValores.Count > 0))
            {
                StringBuilder sbf = new StringBuilder();
                StringBuilder sbv = new StringBuilder();

                List<Parametro> parametros = new List<Parametro>(nuevosValores.Count);

                bool cc = false;

                foreach (DictionaryEntry de in nuevosValores)
                {
                    if (cc) { sbf.Append(','); sbv.Append(','); }

                    string key = de.Key.ToString();

                    sbf.Append(key);
                    sbv.Append("@" + key);
                    parametros.Add(new Parametro("@" + key, de.Value));

                    cc = true;
                }

                executeCustomSQL(String.Format("INSERT INTO Categorias ({0}) VALUES ({1})", sbf.ToString(), sbv.ToString()), parametros.ToArray());
            }
        }

        public void deleteCategoria(Int32 id)
        {
            //executeCustomSQL("DELETE FROM EmpresaTransporte WHERE Id=@Id", new Parametro("@Id", id));
            executeCustomSQL("UPDATE Categorias SET Eliminado = 1 WHERE IdCategoria = @Id", new Parametro("@Id", id));
        }

        #region Campos
        public Int32 ID
        {
            get
            {
                return Convert.ToInt32(registro[Campos.ID]);
            }
            set
            {
                registro[Campos.ID] = value;
            }
        }

        public String CATEGORIA
        {
            get
            {
                return Convert.ToString(registro[Campos.CATEGORIA]);
            }
            set
            {
                registro[Campos.CATEGORIA] = value.Trim();
            }
        }

        public String DESCRIPCION
        {
            get
            {
                return Convert.ToString(registro[Campos.DESCRIPCION]);
            }
            set
            {
                registro[Campos.DESCRIPCION] = value.Trim();
            }
        }
       #endregion
    }
