using System;
using System.Data;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for ClientesSAP
/// </summary>
public class ProductosClientesBO : CapaBase
{
    class Campos
    {
        public static string ID = "Id";
        public static string CODIGOCLIENTE = "CodigoCliente";
        public static string DESCRIPCION = "Descripcion";
    }

    public ProductosClientesBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from ProductosClientes";
        this.initializeSchema("ProductosClientes");
    }

    public void loadAllProductosClientes()
    {
        this.loadSQL(this.coreSQL);
    }

    public void loadBusquedaProductosHojaRuta(string idHojaRuta)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("SELECT IdProducto as Id, CodigoCliente, Descripcion  FROM ProductosClientes as PC");
        sb.Append(" inner join HojaRutaProductos as HRP on (HRP.IdProducto=PC.Id)");
        sb.Append(" where HRP.IdHojaRuta=@IdHojaRuta");

        this.loadSQL(sb.ToString(), new Parametro("@IdHojaRuta", idHojaRuta.Trim()));
    }

    public void loadBusquedaProductosClientes(string codigoCliente)
    {
        this.loadSQL(String.Format("SELECT * FROM ProductosClientes WHERE CodigoCliente = '{0}' order by Descripcion", codigoCliente));
    }

    public void insertProductoCliente(string codigoCliente, string descripcion)
    {
        this.executeCustomSQL(String.Format("INSERT INTO ProductosClientes (CodigoCliente, Descripcion) VALUES ('{0}', '{1}')", codigoCliente, descripcion.ToUpper()));
    }

    public void deleteProductoCliente(string id, string codigoCliente)
    {
        this.executeCustomSQL(String.Format("DELETE FROM ProductosClientes WHERE Id={0} and CodigoCliente='{1}'", id, codigoCliente));
    }

    public string ID
    {
        get
        {
            return Convert.ToString(registro[Campos.ID]);
        }
        set
        {
            registro[Campos.ID] = value;
        }
    }

    public Decimal CODIGOCLIENTE
    {
        get
        {
            return Convert.ToDecimal(registro[Campos.CODIGOCLIENTE]);
        }
        set
        {
            registro[Campos.CODIGOCLIENTE] = value;
        }
    }

    public string DESCRIPCION
    {
        get
        {
            return Convert.ToString(registro[Campos.DESCRIPCION]);
        }
        set
        {
            registro[Campos.DESCRIPCION] = value;
        }
    }
}
