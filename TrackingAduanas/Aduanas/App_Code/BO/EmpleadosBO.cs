using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for EmpleadosBO
/// </summary>
public class EmpleadosBO : CapaBase
{
    class Campos
    {
        public static string IDEMPLEADO = "IdEmpleado";
        public static string NOMBREEMPLEADO = "NombreEmpleado";
        public static string CODIGOADUANA = "CodigoAduana";
        public static string EMAIL = "Email";
        public static string USUARIO = "Usuario";
        public static string PASSWORD = "Password";
        public static string IDROLE = "IdRole";
        public static string SEXO = "Sexo";
        public static string ESTADO = "Estado";
        public static string CODDEPARTAMENTO = "CodDepartamento";
    }
	
    public EmpleadosBO(GrupoLis.Login.Login log): base(log, true)
	{
        this.coreSQL = "Select * from Empleados";
        this.initializeSchema("Empleados");
    }

    public void loadNombreEmpleados(string nombreEmpleado)
    {
        string sql = "Select IdEmpleado, NombreEmpleado, CodigoAduana, Email, Usuario, IdRole from Empleados";
        sql += " where NombreEmpleado like '%" + nombreEmpleado + "%' and Estado = '0' order by NombreEmpleado";
        this.loadSQL(sql);
    }

    public void loadEmpleados()
    {
        string sql = "Select IdEmpleado, NombreEmpleado, CodigoAduana, Email, Usuario, IdRole from Empleados";
        sql += " where Estado = '0' order by NombreEmpleado";
        this.loadSQL(sql);
    }

    public void loadEmpleadosPais(string codigoPais)
    {
        string sql = "Select IdEmpleado, NombreEmpleado, e.CodigoAduana, Email, Usuario, IdRole from Empleados e";
        sql += " inner join Aduanas a on (e.CodigoAduana = a.CodigoAduana)";
        sql += " where a.CodigoPais = '" + codigoPais + "' and e.Estado = '0' order by NombreEmpleado";
        this.loadSQL(sql);
    }

    public void loadEmpleados(string idEmpleado)
    {
        string sql = "Select IdEmpleado, NombreEmpleado, CodigoAduana, Email, Usuario, IdRole, Sexo, CodDepartamento from Empleados";
        sql += " where IdEmpleado = " + idEmpleado + " and Estado = '0' order by NombreEmpleado";
        this.loadSQL(sql);
    }

    public void loadAduanaGestores(string codigoAduana)
    {
        string sql = "Select IdEmpleado, NombreEmpleado, CodigoAduana, Email, Usuario, IdRole from Empleados";
        sql += " where CodigoAduana = '" + codigoAduana + "' and IdRole = 8 and Estado = '0' order by NombreEmpleado";
        this.loadSQL(sql);
    }

    public void loadAduanaAforadores(string codigoAduana)
    {
        string sql = "Select IdEmpleado, NombreEmpleado, CodigoAduana, Email, Usuario, IdRole from Empleados";
        sql += " where CodigoAduana = '" + codigoAduana + "' and IdRole = 16 and Estado = '0' order by NombreEmpleado";
        this.loadSQL(sql);
    }

    public void loadAduanaTramitadores(string codigoAduana)
    {
        string sql = "Select IdEmpleado, NombreEmpleado, CodigoAduana, Email, Usuario, IdRole from Empleados";
        sql += " where CodigoAduana = '" + codigoAduana + "' and IdRole = 21 and Estado = '0' order by NombreEmpleado";
        this.loadSQL(sql);
    }

    public void loadAduanaJefes(string codigoAduana)
    {
        string sql = "Select IdEmpleado, NombreEmpleado, CodigoAduana, Email, Usuario, IdRole from Empleados";
        sql += " where CodigoAduana = '" + codigoAduana + "' and IdRole = 10 and Estado = '0' order by NombreEmpleado";
        this.loadSQL(sql);
    }

    ////////////temporal//////////
    public void loadAduanaJefesSV(string codPais)
    {
        string sql = " select * from Empleados E ";
        sql += " INNER JOIN Aduanas A ON (A.CodigoAduana = E.CodigoAduana) ";
        sql += " WHERE CodigoPais = 'SV' order by NombreEmpleado ";
        this.loadSQL(sql);
    }
    

    public void loadAduanaEmpleados(string codigoAduana)
    {
        string sql = "Select IdEmpleado, NombreEmpleado, CodigoAduana, Email, Usuario, IdRole from Empleados";
        sql += " where CodigoAduana = '" + codigoAduana + "' and IdRole <> 9 and Estado = '0' order by NombreEmpleado";
        this.loadSQL(sql);
    }

    public void loadUsuarioEmpleados(string usuario)
    {
        string sql = "Select IdEmpleado, NombreEmpleado, CodigoAduana, Email, Usuario, IdRole from Empleados";
        sql += " where Usuario like '%" + usuario + "%' and Estado = '0' order by NombreEmpleado";
        this.loadSQL(sql);
    }

    public void loadUsuario(string usuario)
    {
        string sql = "Select * from Empleados";
        sql += " where Usuario = '" + usuario + "' and Estado = '0' order by NombreEmpleado";
        this.loadSQL(sql);
    }

    public void loadAllCamposEmpleado(string nombreEmpleado, string codigoAduana, string email, string usuario)
    {
        string sql = "Select IdEmpleado, NombreEmpleado, CodigoAduana, Email, Usuario, IdRole from Empleados";
        sql += " where NombreEmpleado = '" + nombreEmpleado + "' and Usuario = '" + usuario + "' and Email = '" + email + "' and CodigoAduana = '" + codigoAduana + "' and Estado = '0' order by NombreEmpleado";
        this.loadSQL(sql);
    }

    public void makePassword(string pwd)
    {
        string sql = "DECLARE @cyphertext VARBINARY(MAX) SET @cyphertext = EncryptByPassphrase('grupolis', '" + pwd + "') select CAST(@cyphertext AS VARCHAR(MAX)) as Password";
        this.loadSQL(sql);
    }

    public void readPassword(string idEmpleado)
    {
        string sql = "SELECT CAST(DecryptByPassphrase('grupolis', Password) AS VARCHAR(MAX)) as Password from Empleados where IdEmpleado = " + idEmpleado;
        this.loadSQL(sql);
    }

    public string IDEMPLEADO
    {
        get
        {
            return (string)registro[Campos.IDEMPLEADO].ToString();
        }
    }


    public string NOMBREEMPLEADO
    {
        get
        {
            return (string)registro[Campos.NOMBREEMPLEADO].ToString();
        }
        set
        {
            registro[Campos.NOMBREEMPLEADO] = value;
        }
    }

    public string CODIGOADUANA
    {
        get
        {
            return (string)registro[Campos.CODIGOADUANA].ToString();
        }
        set
        {
            registro[Campos.CODIGOADUANA] = value;
        }
    }

    public string EMAIL
    {
        get
        {
            return (string)registro[Campos.EMAIL].ToString();
        }
        set
        {
            registro[Campos.EMAIL] = value;
        }
    }

    public string USUARIO
    {
        get
        {
            return (string)registro[Campos.USUARIO].ToString();
        }
        set
        {
            registro[Campos.USUARIO] = value;
        }
    }

    public string PASSWORD
    {
        get
        {
            return (string)registro[Campos.PASSWORD].ToString();
        }
        set
        {
            registro[Campos.PASSWORD] = value;
        }
    }

    public string IDROLE
    {
        get
        {
            return (string)registro[Campos.IDROLE].ToString();
        }
        set
        {
            registro[Campos.IDROLE] = value;
        }
    }

    public string ESTADO
    {
        get
        {
            return (string)registro[Campos.ESTADO].ToString();
        }
        set
        {
            registro[Campos.ESTADO] = value;
        }
    }

    public string SEXO
    {
        get
        {
            return (string)registro[Campos.SEXO].ToString();
        }
        set
        {
            registro[Campos.SEXO] = value;
        }
    }

    public string CODDEPARTAMENTO
    {
        get
        {
            return (string)registro[Campos.CODDEPARTAMENTO].ToString();
        }
        set
        {
            registro[Campos.CODDEPARTAMENTO] = value;
        }
    }

    public int darBajaEmpleado(string idEmpleado)
    {
        return this.executeCustomSQL("update Empleados set Estado = '1' where IdEmpleado = " + idEmpleado);
    }

    public int modificarEmpleado(string idEmpleado, string nombreEmpleado, string email, string usuario, string pwd, string idRole, string codigoAduana, string codDepto)
    {
        return this.executeCustomSQL("update Empleados set NombreEmpleado = '" + nombreEmpleado + "', Email = '" + email + "', Usuario = '" + usuario + "', Password = ENCRYPTBYPASSPHRASE('grupolis','" + pwd + "'), IdRole = " + idRole + ", CodigoAduana = '" + codigoAduana + "', CodDepartamento = '" + codDepto + "' where IdEmpleado = " + idEmpleado + " and Estado = '0'");
    }

    public int insertEmpleado(string NombreEmpleado, string CodigoAduana, string email, string usuario, string pwd, string idRole, string codDepto)
    {
        return this.executeCustomSQL("INSERT INTO Empleados VALUES ('" + NombreEmpleado + "', '" + CodigoAduana + "', '" + email + "', '" + usuario + "', ENCRYPTBYPASSPHRASE('grupolis','" + pwd +"'), " + idRole + ", 'M', '0','" + codDepto + "')");
    }

    public int modificarEmpleadoSinPassword(string idEmpleado, string nombreEmpleado, string email, string usuario, string idRole, string codigoAduana, string codDepto)
    {
        return this.executeCustomSQL("update Empleados set NombreEmpleado = '" + nombreEmpleado + "', Email = '" + email + "', Usuario = '" + usuario + "', IdRole = " + idRole + ", CodigoAduana = '" + codigoAduana + "', CodDepartamento = '" + codDepto + "' where IdEmpleado = " + idEmpleado + " and Estado = '0'");
    }

}
