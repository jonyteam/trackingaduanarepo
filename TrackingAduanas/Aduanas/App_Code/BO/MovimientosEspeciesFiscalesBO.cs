using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for MovimientosEspeciesFiscalesBO
/// </summary>
public class MovimientosEspeciesFiscalesBO : CapaBase
{
    class Campos
    {
        public static string ITEM = "Item";
        public static string IDMOVIMIENTO = "IdMovimiento";
        public static string IDESPECIEFISCAL = "IdEspecieFiscal";
        public static string CODPAIS = "CodPais";
        public static string CODIGOADUANA = "CodigoAduana";
        public static string RANGOINICIAL = "RangoInicial";
        public static string RANGOFINAL = "RangoFinal";
        public static string CANTIDAD = "Cantidad";
        public static string FECHA = "Fecha";
        public static string CODTIPOMOVIMIENTO = "CodTipoMovimiento";
        public static string CODESTADO = "CodEstado";
        public static string IDUSUARIO = "IdUsuario";
        public static string IDUSUARIORECIBO = "IdUsuarioRecibo";
        public static string IDCOMPRA = "IdCompra";
    }

    public MovimientosEspeciesFiscalesBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from MovimientosEspeciesFiscales MEF ";
        this.initializeSchema("MovimientosEspeciesFiscales");
    }

    public void loadMovimientosEspeciesFiscales()
    {
        string sql = this.coreSQL;
        sql += " WHERE CodEstado = '0' ";
        this.loadSQL(sql);
    }

    public void loadMovimientosEspeciesFiscalesItem(string item)
    {
        string sql = this.coreSQL;
        sql += " WHERE Item = '" + item + "' ";
        this.loadSQL(sql);
    }

    public void loadMovimientosEspeciesFiscales(string codigoAduana)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodigoAduana = '" + codigoAduana + "' AND CodEstado IN ('1','2') ";
        this.loadSQL(sql);
    }

    public void loadRangoInicialEFXAduana(string codigoAduana)
    {
        string sql = " Select MEF.Item, MEF.IdEspecieFiscal, MEF.CodigoAduana, MEF.RangoInicial from MovimientosEspeciesFiscales MEF ";
        sql += " WHERE MEF.RangoInicial IN (SELECT MIN(RangoInicial) FROM MovimientosEspeciesFiscales ";
        sql += " WHERE CodigoAduana = '" + codigoAduana + "' AND CodEstado IN ('1','2') ";
        sql += " GROUP BY IdEspecieFiscal, CodigoAduana) ";
        this.loadSQL(sql);
    }

    public void loadRangoInicialEFXAduana(string codigoAduana, string idEspecieFiscal)
    {
        string sql = " Select * from MovimientosEspeciesFiscales MEF ";
        sql += " WHERE MEF.RangoInicial IN (SELECT MIN(RangoInicial) FROM MovimientosEspeciesFiscales ";
        sql += " WHERE CodigoAduana = '" + codigoAduana + "' AND IdEspecieFiscal = '" + idEspecieFiscal + "' AND CodEstado IN ('1','2') ";
        sql += " GROUP BY IdEspecieFiscal, CodigoAduana) AND CodigoAduana = '" + codigoAduana + "' AND IdEspecieFiscal = '" + idEspecieFiscal + "' AND CodEstado IN ('1','2') ";
        this.loadSQL(sql);
    }
    public void IdMovimientoAnular(string Serie, string CodEspecie)
    {
        string sql = " Select top 1 IdMovimiento from MovimientosEspeciesFiscales MEF ";
        sql += " WHERE " + Serie + " between RangoInicial and RangoFinal and IdEspecieFiscal = '" + CodEspecie + "' and CodEstado = 100 order by Item desc";
        this.loadSQL(sql);
    }
    public void IdMovimientoAsignar(int RI, int RF, string CodEspecie)
    {
        string sql = " Select top 1 IdMovimiento from MovimientosEspeciesFiscales MEF ";
        sql += "  Where RangoInicial >= " + RI + " and RangoFinal <= " + RF + " and IdEspecieFiscal = '" + CodEspecie + "' and  CodEstado = 100  order by Item desc";
        this.loadSQL(sql);
    }
    public void loadRangoInicialEFXAduanaClienteNew(string codigoAduana, string idEspecieFiscal, string idCliente)
    {
        string sql = " Select * from MovimientosEspeciesFiscales MEF ";
        sql += " INNER JOIN EspeciesFiscalesClientes EFC ON (EFC.IdMovimiento = MEF.IdMovimiento) ";
        sql += " WHERE MEF.RangoInicial IN (SELECT MIN(MEF.RangoInicial) FROM MovimientosEspeciesFiscales MEF ";
        sql += " INNER JOIN EspeciesFiscalesClientes EFC ON (EFC.IdMovimiento = MEF.IdMovimiento) ";
        sql += " WHERE CodigoAduana = '" + codigoAduana + "' AND IdEspecieFiscal = '" + idEspecieFiscal + "' AND CodEstado IN ('3') AND IdCliente = '" + idCliente + "' ";
        sql += " GROUP BY IdEspecieFiscal, CodigoAduana) AND IdCliente = '" + idCliente + "' AND CodEstado IN ('3') ";
        this.loadSQL(sql);
    }

    public void loadEFXAduanaClienteALL(string codigoAduana, string idEspecieFiscal, string idCliente)
    {
        string sql = " Select * from MovimientosEspeciesFiscales MEF ";
        sql += " INNER JOIN EspeciesFiscalesClientes EFC ON (EFC.IdMovimiento = MEF.IdMovimiento) ";
        sql += " WHERE CodigoAduana = '" + codigoAduana + "' AND IdEspecieFiscal = '" + idEspecieFiscal + "' AND CodEstado IN ('3') ";
        sql += " AND IdCliente = '" + idCliente + "' ORDER BY RangoInicial ";
        this.loadSQL(sql);
    }

    public void loadEFXAduanaALL(string codigoAduana, string idEspecieFiscal)
    {
        string sql = " Select * from MovimientosEspeciesFiscales MEF ";
        sql += " WHERE CodigoAduana = '" + codigoAduana + "' AND IdEspecieFiscal = '" + idEspecieFiscal + "' AND CodEstado IN ('1','2') ";
        sql += " ORDER BY convert(int,RangoInicial) ";
        this.loadSQL(sql);
    }
    public void loadEFXAduanaSello(string serie, string codigoAduana, string idEspecieFiscal)
    {
        string sql = " Select * from MovimientosEspeciesFiscales MEF ";
        sql += " Where " + serie + " between rangoinicial and rangofinal and CodEstado not like 100 and IdEspecieFiscal = '" + idEspecieFiscal + "' and CodigoAduana = '" + codigoAduana + "'";
        sql += " ORDER BY RangoInicial ";
        this.loadSQL(sql);
    }

    public void loadMovimientosEspeciesFiscalesXIdMov(string idMovimiento)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdMovimiento = '" + idMovimiento + "' AND CodEstado IN ('0','1') "; //estado solo estaba en cero
        this.loadSQL(sql);
    }

    public void loadMovimientosEspeciesFiscalesCompras(string idCompra)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdCompra = '" + idCompra + "' AND CodTipoMovimiento = '0' AND CodEstado = '1' ";
        this.loadSQL(sql);
    }

    public void loadMovimientosEspeciesFiscales(string idEspecieFiscal, string codigoAduana)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdEspecieFiscal = '" + idEspecieFiscal + "' AND CodigoAduana = '" + codigoAduana + "' AND CodEstado IN ('1','2','3')ORDER BY RangoInicial ";
        this.loadSQL(sql);
    }
    public void loadMovimientosEspeciesFiscales2(string idEspecieFiscal, string codigoAduana, string rangos)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdEspecieFiscal = '" + idEspecieFiscal + "' AND CodigoAduana = '" + codigoAduana + "' AND CodEstado IN ('1','2','3') and Item in (" + rangos + ")ORDER BY RangoInicial ";
        this.loadSQL(sql);
    }
    public void loadMovimientosEspeciesFiscalesClientes(string idEspecieFiscal, string codigoAduana)//
    {
        string sql = this.coreSQL;
        sql += " WHERE IdEspecieFiscal = '" + idEspecieFiscal + "' AND CodigoAduana = '" + codigoAduana + "' ";
        sql += " AND CodEstado IN ('1','2') ORDER BY 6 ";   //AND CodTipoMovimiento != '5'
        this.loadSQL(sql);
    }

    public void loadMovimientosEspeciesFiscalesClientes2(string idEspecieFiscal, string codigoAduana, string rangos)//
    {
        string sql = this.coreSQL;
        sql += " WHERE IdEspecieFiscal = '" + idEspecieFiscal + "' AND CodigoAduana = '" + codigoAduana + "' AND CodEstado IN ('1','2') and Item in (" + rangos + ") ORDER BY 6 ";
      //  sql += " AND CodEstado IN ('1','2') ORDER BY 6 ";   //AND CodTipoMovimiento != '5'
        this.loadSQL(sql);
    }

    public void loadMovimientosEspeciesFiscalesVerificar(string rangoInicial, string rangoFinal, string codAduana, string codEspecie)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodEstado != '100' AND CodigoAduana = '" + codAduana + "' AND IdEspecieFiscal = '" + codEspecie + "' ";
        sql += " AND (RangoInicial BETWEEN " + rangoInicial + " AND " + rangoFinal + " OR RangoFinal BETWEEN " + rangoInicial + " AND " + rangoFinal + " ";
        sql += " OR (" + rangoInicial + " BETWEEN RangoInicial AND RangoFinal AND " + rangoFinal + " BETWEEN RangoInicial AND RangoFinal)) ";
        this.loadSQL(sql);
    }

    public void loadMovimientosEspeciesFiscalesVerificarLiberar(string rangoInicial, string rangoFinal, string codAduana, string codEspecie)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodEstado = '3' AND CodigoAduana = '" + codAduana + "' AND IdEspecieFiscal = '" + codEspecie + "' ";
        sql += " AND (RangoInicial BETWEEN " + rangoInicial + " AND " + rangoFinal + " OR RangoFinal BETWEEN " + rangoInicial + " AND " + rangoFinal + " ";
        sql += " OR (" + rangoInicial + " BETWEEN RangoInicial AND RangoFinal AND " + rangoFinal + " BETWEEN RangoInicial AND RangoFinal)) ";
        this.loadSQL(sql);
    }


    public void loadRangosEspeciesFiscalesEnvios(string codigoAduana, string idEspecieFiscal)
    {
        string sql = " Select MEF.*, A.NombreAduana, C.Descripcion AS Pais, C1.Descripcion AS EspecieFiscal, U.Usuario,CL.Nombre,MEF.Item  from MovimientosEspeciesFiscales MEF ";
        sql += " INNER JOIN Aduanas A ON (A.CodigoAduana = MEF.CodigoAduana) ";
        sql += " INNER JOIN Codigos C ON (C.Codigo = MEF.CodPais AND C.Categoria = 'PAISES') ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = MEF.IdEspecieFiscal AND C1.Categoria = 'ESPECIESFISCALES') ";
        sql += " INNER JOIN Usuarios U ON (U.IdUsuario = MEF.IdUsuario) ";
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////Agrego Darwin Bonilla el 18/03/2014//////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        sql += " Left Join EspeciesFiscalesClientes EFC on (MEF.IdMovimiento = EFC.IdMovimiento)";
        sql += " Left Join Clientes CL on (EFC.IdCliente = CL.CodigoSAP) ";
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        sql += " WHERE MEF.CodigoAduana = '" + codigoAduana + "' AND IdEspecieFiscal = '" + idEspecieFiscal + "' AND MEF.CodEstado IN ('1','2','3') AND A.CodEstado = '0' ";
        sql += " ORDER BY MEF.RangoInicial ";
        this.loadSQL(sql);
    }

    public void loadRangosEspeciesFiscalesEnviosCliente(string codigoAduana, string idEspecieFiscal)
    {
        string sql = " Select MEF.*, A.NombreAduana, C.Descripcion AS Pais, C1.Descripcion AS EspecieFiscal, U.Usuario from MovimientosEspeciesFiscales MEF ";
        sql += " INNER JOIN Aduanas A ON (A.CodigoAduana = MEF.CodigoAduana) ";
        sql += " INNER JOIN Codigos C ON (C.Codigo = MEF.CodPais AND C.Categoria = 'PAISES') ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = MEF.IdEspecieFiscal AND C1.Categoria = 'ESPECIESFISCALES') ";
        sql += " INNER JOIN Usuarios U ON (U.IdUsuario = MEF.IdUsuario) ";
        sql += " WHERE MEF.CodigoAduana = '" + codigoAduana + "' AND IdEspecieFiscal = '" + idEspecieFiscal + "' AND MEF.CodEstado IN ('1','2') AND A.CodEstado = '0' ";
        this.loadSQL(sql);
    }

    public void loadRangosEspeciesFiscalesEnviosCliente2(string codigoAduana, string idEspecieFiscal)
    {
        string sql = " Select MEF.*, A.NombreAduana, C.Descripcion AS Pais, C1.Descripcion AS EspecieFiscal, U.Usuario,MEF.Item from MovimientosEspeciesFiscales MEF ";
        sql += " INNER JOIN Aduanas A ON (A.CodigoAduana = MEF.CodigoAduana) ";
        sql += " INNER JOIN Codigos C ON (C.Codigo = MEF.CodPais AND C.Categoria = 'PAISES') ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = MEF.IdEspecieFiscal AND C1.Categoria = 'ESPECIESFISCALES') ";
        sql += " INNER JOIN Usuarios U ON (U.IdUsuario = MEF.IdUsuario) ";
        sql += " WHERE MEF.CodigoAduana = '" + codigoAduana + "' AND IdEspecieFiscal = '" + idEspecieFiscal + "' AND MEF.CodEstado IN ('1','2') AND A.CodEstado = '0' ";
        this.loadSQL(sql);
    }

    public void loadRangosEspeciesFiscalesAnulacion(string codigoAduana, string idEspecieFiscal)
    {
        string sql = @" Select distinct MEF.*, A.NombreAduana, C.Descripcion AS Pais, C1.Descripcion AS EspecieFiscal, U.Usuario, CL.Nombre, case When MEF.CodEstado = 0 
            Then 'No Han Sido Recibidas Todavia' Else 'Recibidas' End as Observacion from MovimientosEspeciesFiscales MEF  
            INNER JOIN Aduanas A ON (A.CodigoAduana = MEF.CodigoAduana)  
            INNER JOIN Codigos C ON (C.Codigo = MEF.CodPais AND C.Categoria = 'PAISES')  
            INNER JOIN Codigos C1 ON (C1.Codigo = MEF.IdEspecieFiscal AND C1.Categoria = 'ESPECIESFISCALES')  
            INNER JOIN Usuarios U ON (U.IdUsuario = MEF.IdUsuario)  
            LEFT JOIN EspeciesFiscalesClientes EFC ON (EFC.IdMovimiento = MEF.IdMovimiento)  
            LEFT JOIN Clientes CL ON (CL.CodigoSAP = EFC.IdCliente)";
            sql += " WHERE MEF.CodigoAduana = '" + codigoAduana + "' AND IdEspecieFiscal = '" + idEspecieFiscal + "' AND MEF.CodEstado IN ('0','1','2','3') AND A.CodEstado = '0'";  
            sql +=  " ORDER BY MEF.RangoInicial ";
        this.loadSQL(sql);
    }

    public void loadRangosEspeciesFiscalesRecibo(string opcion, string codigoAduana)
    {
        string sql = " Select MEF.*, A.NombreAduana, A2.NombreAduana AS AduanaOrigen, C.Descripcion AS Pais, C1.Descripcion AS EspecieFiscal, U.Usuario from MovimientosEspeciesFiscales MEF ";
        sql += " INNER JOIN Aduanas A ON (A.CodigoAduana = MEF.CodigoAduana) ";
        sql += " INNER JOIN Codigos C ON (C.Codigo = MEF.CodPais AND C.Categoria = 'PAISES') ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = MEF.IdEspecieFiscal AND C1.Categoria = 'ESPECIESFISCALES') ";
        sql += " INNER JOIN Usuarios U ON (U.IdUsuario = MEF.IdUsuario) ";
        sql += " RIGHT JOIN TrasladosEspeciesFiscales TEF ON (TEF.IdEnvio = MEF.IdMovimiento) ";
        sql += " RIGHT JOIN Aduanas A2 ON (A2.CodigoAduana = TEF.CodigoAduanaOrigen AND A2.CodEstado = '0') ";
        if (opcion == "Admin")
            sql += " WHERE MEF.CodEstado IN ('0') AND A.CodEstado = '0' ";
        else
            sql += " WHERE MEF.CodigoAduana = '" + codigoAduana + "' AND MEF.CodEstado IN ('0') AND A.CodEstado = '0' ";
        this.loadSQL(sql);
    }

    public void loadRangoEspeciesFiscalesXSerie(string serie, string codigoAduana, string idEspecieFiscal)
    {
        string sql = this.coreSQL;
        sql += " WHERE " + serie + " BETWEEN RangoInicial AND RangoFinal AND CodEstado != '100' ";
        sql += " AND CodigoAduana = '" + codigoAduana + "' AND IdEspecieFiscal = '" + idEspecieFiscal + "' ";
        this.loadSQL(sql);
    }

    public void CargarMovimiento(string rango, string aduana, string especie)
    {
        string sql = " SELECT B.Descripcion,case A.[CodPais] when 'H' then 'HONDURAS' ELSE 'Otro Pais' end as Pais,C.NombreAduana,[RangoInicial]";
        sql += " ,[RangoFinal],[Cantidad],Convert (varchar,A.[Fecha],103) as [Fecha Creado],case A.[CodEstado] when 7 then 'Deshabilitada' else 'Disponible' end as Estado";
        sql += " ,ISNULL (E.Nombre,'NO ESTA ASIGNADA A NINGUN CLIENTE') as Cliente,F.Nombre +' '+Apellido as [Creado Por] FROM MovimientosEspeciesFiscales A";
        sql += " Inner Join Codigos B on (B.Categoria = 'ESPECIESFISCALES' and A.IdEspecieFiscal = B.Codigo and B.Eliminado = 0)";
        sql += " Inner Join Aduanas C on ( A.CodigoAduana = C.CodigoAduana and A.CodPais = C.CodPais and C.CodEstado = 0 )";
        sql += " Inner Join Usuarios F on (A.IdUsuario = F.IdUsuario) Left Join EspeciesFiscalesClientes D on (A.IdMovimiento= D.IdMovimiento )";
        sql += " Left Join Clientes E on (D.IdCliente = E.CodigoSAP) Where IdEspecieFiscal = '" + especie + "' and A.CodigoAduana like '%" + aduana + "%'";
        sql += " and " + rango + " between rangoinicial and rangofinal and A.CodEstado != 100";
        this.loadSQL(sql);
    }
    // DARWIN 15-01-2016
    public void CargarInventarioXAduana(string CodigoAduana, bool Anuladas)
    {
        string sql = @"SELECT C.Descripcion,A.NombreAduana,RangoInicial,RangoFinal,Cantidad,MEF.Fecha,CodTipoMovimiento,MEF.CodEstado
                    ,U.Nombre+' '+U.Apellido as Nombre,CL.Nombre as Cliente,IdCompra,MEF.IdMovimiento,convert(date,GETDATE()) as FechaReporte
                    FROM MovimientosEspeciesFiscales MEF
                    Inner Join Codigos C on (C.Codigo = MEF.IdEspecieFiscal and C.Categoria ='ESPECIESFISCALES')
                    Inner Join Usuarios U on (U.IdUsuario = MEF.IdUsuario)
                    Inner Join Aduanas A on (A.CodigoAduana = MEF.CodigoAduana)
                    Left Join EspeciesFiscalesClientes EFC on (EFC.IdMovimiento = MEF.IdMovimiento and MEF.IdEspecieFiscal in ('DVA','F'))
                    Left Join Clientes CL on (CL.CodigoSAP = EFC.IdCliente)
                    Where MEF.CodEstado != 100 and MEF.CodigoAduana = '" + CodigoAduana + "'";
        if (Anuladas == false)
        {
            sql += "And MEF.CodEstado != '7'";
        }
        this.loadSQL(sql);
    }
    public void CargarInventarioXAduana(string CodigoAduana)
    {
        string sql = @"SELECT C.Descripcion,A.NombreAduana,RangoInicial,RangoFinal,Cantidad,MEF.Fecha,CodTipoMovimiento,MEF.CodEstado
                    ,U.Nombre+' '+U.Apellido as Nombre,CL.Nombre as Cliente,IdCompra,MEF.IdMovimiento,convert(date,GETDATE()) as FechaReporte
                    FROM MovimientosEspeciesFiscales MEF
                    Inner Join Codigos C on (C.Codigo = MEF.IdEspecieFiscal and C.Categoria ='ESPECIESFISCALES')
                    Inner Join Usuarios U on (U.IdUsuario = MEF.IdUsuario)
                    Inner Join Aduanas A on (A.CodigoAduana = MEF.CodigoAduana)
                    Left Join EspeciesFiscalesClientes EFC on (EFC.IdMovimiento = MEF.IdMovimiento and MEF.IdEspecieFiscal in ('DVA','F'))
                    Left Join Clientes CL on (CL.CodigoSAP = EFC.IdCliente)
                    Where MEF.CodEstado != 100 and MEF.CodigoAduana = '" + CodigoAduana + "'";
        this.loadSQL(sql);
    }

    #region campos
    public string ITEM
    {
        get
        {
            return (string)registro[Campos.ITEM].ToString();
        }
    }

    public string IDMOVIMIENTO
    {
        get
        {
            return (string)registro[Campos.IDMOVIMIENTO].ToString();
        }
        set
        {
            registro[Campos.IDMOVIMIENTO] = value;
        }
    }

    public string IDESPECIEFISCAL
    {
        get
        {
            return (string)registro[Campos.IDESPECIEFISCAL].ToString();
        }
        set
        {
            registro[Campos.IDESPECIEFISCAL] = value;
        }
    }

    public string CODPAIS
    {
        get
        {
            return (string)registro[Campos.CODPAIS].ToString();
        }
        set
        {
            registro[Campos.CODPAIS] = value;
        }
    }

    public string CODIGOADUANA
    {
        get
        {
            return (string)registro[Campos.CODIGOADUANA].ToString();
        }
        set
        {
            registro[Campos.CODIGOADUANA] = value;
        }
    }

    public string RANGOINICIAL
    {
        get
        {
            return (string)registro[Campos.RANGOINICIAL].ToString();
        }
        set
        {
            registro[Campos.RANGOINICIAL] = value;
        }
    }

    public string RANGOFINAL
    {
        get
        {
            return (string)registro[Campos.RANGOFINAL].ToString();
        }
        set
        {
            registro[Campos.RANGOFINAL] = value;
        }
    }

    public int CANTIDAD
    {
        get
        {
            return int.Parse(registro[Campos.CANTIDAD].ToString());
        }
        set
        {
            registro[Campos.CANTIDAD] = value;
        }
    }

    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }

    public string CODTIPOMOVIMIENTO
    {
        get
        {
            return (string)registro[Campos.CODTIPOMOVIMIENTO].ToString();
        }
        set
        {
            registro[Campos.CODTIPOMOVIMIENTO] = value;
        }
    }

    public string CODESTADO
    {
        get
        {
            return (string)registro[Campos.CODESTADO].ToString();
        }
        set
        {
            registro[Campos.CODESTADO] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }

    public string IDUSUARIORECIBO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIORECIBO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIORECIBO] = value;
        }
    }

    public string IDCOMPRA
    {
        get
        {
            return (string)registro[Campos.IDCOMPRA].ToString();
        }
        set
        {
            registro[Campos.IDCOMPRA] = value;
        }
    }
    #endregion
}
