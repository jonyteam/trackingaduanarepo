using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for EventosBO
/// </summary>
public class TasadeCambioUNOBO: CapaBase
{

    class Campos
    {
    public static string ID	="Id";
    public static string GASOLINASUPER = "GASOLINASUPER";
    public static string GASOLINAREGULAR = "GASOLINAREGULAR";
    public static string DIESEL = "DIESEL";
    public static string AVJET = "AVJET";
    public static string BUNKER = "BUNKER";
    public static string KEROSENO = "KEROSENO";
    public static string FECHAINICIO = "FechaInicio";
    public static string FECHAFINAL = "FechaFinal";
    public static string FECHA = "Fecha";
    public static string IDUSUARIO = "IdUsuario";
    public static string CODESTADO = "CodEstado";
    }

    public TasadeCambioUNOBO(GrupoLis.Login.Login log)
        : base(log, true)
	{
        this.coreSQL = "Select * from TasadeCambioUNO TCU";
        this.initializeSchema("TasadeCambioUNO");
    }

    public void loadtasacambio(string pais )
    {
        string sql = this.coreSQL;
        sql += " where Fecha = CONVERT (DATE, GETDATE()) and Pais = '" + pais + "'";
        this.loadSQL(sql);
    }
    public void loadtasa()
    {
        string sql = this.coreSQL;
        sql += " where FechaInicio = '2000-01-01'";
        this.loadSQL(sql);
    }
    public void VerificarFechaInicio(string fecha)
    {
        string sql = this.coreSQL;
        sql += " where '" + fecha + "' between FechaInicio and FechaFinal ";
        this.loadSQL(sql);
    }
    public void VerificarFechaFinal(string fecha)
    {
        string sql = this.coreSQL;
        sql += " where '" + fecha + "' between FechaInicio and FechaFinal ";
        this.loadSQL(sql);
    }
    public void CargarTasasDelYear(int year, int mes)
    {
        string sql = this.coreSQL;
        sql += " where FechaInicio >= '" + year +"-"+mes+"-01"+ "'";
        this.loadSQL(sql);
    }
    public void LoadFecha(string fechainicio, string fechafinal)
    {
        string sql = this.coreSQL;
        sql += " where Convert(varchar,FechaInicio,111) = '" + fechainicio + "' and Convert(varchar,FechaFinal,111) = '" + fechafinal + "' ";
        this.loadSQL(sql);
    }
    public void LoadFecha2(string fechainicio, string fechafinal)
    {
        string sql = "Select GASOLINASUPER,GASOLINAREGULAR,DIESEL,AVJET,BUNKER,KEROSENO,Convert(Varchar,FechaInicio,101) as FechaInicio,Convert(Varchar,FechaFinal,101) as FechaFinal from TasadeCambioUNO TCU";
        sql += " where FechaInicio >= '" + fechainicio + "' and FechaFinal <= '" + fechafinal + "' ";
        this.loadSQL(sql);
    }
     #region Campos
    public string ID
    {
        get
        {
            return (string)registro[Campos.ID].ToString();
        }
        set
        {
            registro[Campos.ID] = value;
        }
    }
    public Double GASOLINASUPER
    {
        get
        {
            return Convert.ToDouble(registro[Campos.GASOLINASUPER].ToString());
        }
        set
        {
            registro[Campos.GASOLINASUPER] = value;
        }
    }
    public Double GASOLINAREGULAR
    {
        get
        {
            return Convert.ToDouble(registro[Campos.GASOLINAREGULAR].ToString());
        }
        set
        {
            registro[Campos.GASOLINAREGULAR] = value;
        }
    }
    public Double DIESEL
    {
        get
        {
            return Convert.ToDouble(registro[Campos.DIESEL].ToString());
        }
        set
        {
            registro[Campos.DIESEL] = value;
        }
    }
    public Double AVJET
    {
        get
        {
            return Convert.ToDouble(registro[Campos.AVJET].ToString());
        }
        set
        {
            registro[Campos.AVJET] = value;
        }
    }
    public Double BUNKER
    {
        get
        {
            return Convert.ToDouble(registro[Campos.BUNKER].ToString());
        }
        set
        {
            registro[Campos.BUNKER] = value;
        }
    }
    public Double KEROSENO
    {
        get
        {
            return Convert.ToDouble(registro[Campos.KEROSENO].ToString());
        }
        set
        {
            registro[Campos.KEROSENO] = value;
        }
    }
    public string FECHAINICIO
    {
        get
        {
            return (string)registro[Campos.FECHAINICIO].ToString();
        }
        set
        {
            registro[Campos.FECHAINICIO] = value;
        }
    }
    public string FECHAFINAL
    {
        get
        {
            return (string)registro[Campos.FECHAFINAL].ToString();
        }
        set
        {
            registro[Campos.FECHAFINAL] = value;
        }
    }
    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }
    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }
    public string CODESTADO
    {
        get
        {
            return (string)registro[Campos.CODESTADO].ToString();
        }
        set
        {
            registro[Campos.CODESTADO] = value;
        }
    }
    #endregion
}
