using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;
/// <summary>
/// Summary description for RolesBO
/// </summary>
public class RolesBO: CapaBase
{
    class Campos
    {
        public static string IDROL = "IdRol";
        public static string DESCRIPCION = "Descripcion";
        public static string ELIMINADO = "Eliminado";
    }
    
    public RolesBO(GrupoLis.Login.Login log): base(log, true)
	{
        this.coreSQL = "Select * from Roles";
        this.initializeSchema("Roles");
        this.identityColumn = "IdRol";
    }

    public void loadRoles()
    {
        string sql = this.coreSQL;
        sql += " where Eliminado = '0' order by Descripcion";
        this.loadSQL(sql);
    }

    public void loadNombreRoles(string nombre)
    {
        string sql = this.coreSQL;
        sql += " where Descripcion = '" + nombre + "' and Eliminado = '0' order by Descripcion";
        this.loadSQL(sql);
    }

    public void loadRoles(string idRol)
    {
        string sql = this.coreSQL;
        sql += " where IdRol = " + idRol + " and Eliminado = '0'";
        this.loadSQL(sql);
    }

    public string IDROL
    {
        get
        {
            return (string)registro[Campos.IDROL].ToString();
        }
    }

    public string DESCRIPCION
    {
        get
        {
            return (string)registro[Campos.DESCRIPCION].ToString();
        }
        set
        {
            registro[Campos.DESCRIPCION] = value;
        }
    }

    public string ELIMINADO
    {
        get
        {
            return (string)registro[Campos.ELIMINADO].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO] = value;
        }
    }

    public int darBajaRole(string idRol)
    {
        return this.executeCustomSQL("update Roles set Estado = '1' where IdRol = " + idRol);
    }

}
