using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for AduanasBO
/// </summary>
public class HistorialImpresionesBO : CapaBase
{
    class Campos
    {
        public static string CODIGOIMPRESION = "CodigoImpresion";
        public static string DOCUMENTO = "Documento";
        public static string DESCRIPCION = "Descripcion";
        public static string FECHA = "Fecha";
        public static string IDUSUARIO = "IdUsuario";
        public static string ELIMINADO = "Eliminado";
    }

    public HistorialImpresionesBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from HistorialImpresiones ";
        this.initializeSchema("HistorialImpresiones");
    }

    public void loadHistorialImpresiones(string idUsuario)
    {
        string sql = coreSQL;
        sql += " WHERE Eliminado = '0' AND IdUsuario = '" + idUsuario + "' ORDER BY CodigoImpresion ASC ";
        this.loadSQL(sql);
    }

    public void loadHistorialImpresionesXDocumento(string documento)
    {
        string sql = coreSQL;
        sql += " WHERE Eliminado = '0' AND Documento = '" + documento + "' ";
        this.loadSQL(sql);
    }

    #region Campos
    public string CODIGOIMPRESION
    {
        get
        {
            return (string)registro[Campos.CODIGOIMPRESION].ToString();
        }
    }

    public string DOCUMENTO
    {
        get
        {
            return (string)registro[Campos.DOCUMENTO].ToString();
        }
        set
        {
            registro[Campos.DOCUMENTO] = value;
        }
    }

    public string DESCRIPCION
    {
        get
        {
            return (string)registro[Campos.DESCRIPCION].ToString();
        }
        set
        {
            registro[Campos.DESCRIPCION] = value;
        }
    }

    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }

    public string ELIMINADO
    {
        get
        {
            return (string)registro[Campos.ELIMINADO].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO] = value;
        }
    }
    #endregion
}
