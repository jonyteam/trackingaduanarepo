﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;

using GrupoLis.Ebase;
using GrupoLis.Login;

    public class UserSettingsBO : CapaBase
    {
        class Campos
        {
            public static string IDUSUARIO = "IdUsuario";
            public static string SKIN = "Skin";
        }

        public UserSettingsBO(GrupoLis.Login.Login conector)
            : base(conector, true)
        {
            coreSQL = "SELECT * FROM UserSettings";
            initializeSchema("UserSettings");
        }

        public void loadAllCampos()
        {
            loadSQL(coreSQL);
        }

        public void loadAllCampos(Int32 idUsuario)
        {
            loadSQL(coreSQL + " WHERE IdUsuario=@IdUsuario", new Parametro("@IdUsuario", idUsuario));
        }

        public void updateSettings(Int32 idUsuario, Hashtable nuevosValores)
        {
            if ((nuevosValores != null) && (nuevosValores.Count > 0) && (idUsuario > 0))
            {
                StringBuilder sb = new StringBuilder("UPDATE UserSettings SET ");
                List<Parametro> parametros = new List<Parametro>(nuevosValores.Count);

                bool cc = false;

                foreach (DictionaryEntry de in nuevosValores)
                {
                    if (cc) sb.Append(',');

                    string key = de.Key.ToString();

                    sb.AppendFormat("{0}=@{0}", key);
                    parametros.Add(new Parametro("@" + key, de.Value));

                    cc = true;
                }

                sb.Append(" WHERE IdUsuario=@IdUsuario");
                parametros.Add(new Parametro("@IdUsuario", idUsuario));

                executeCustomSQL(sb.ToString(), parametros.ToArray());
            }
        }

        public void insertSettings(Hashtable nuevosValores)
        {
            if ((nuevosValores != null) && (nuevosValores.Count > 0))
            {
                StringBuilder sbf = new StringBuilder();
                StringBuilder sbv = new StringBuilder();

                List<Parametro> parametros = new List<Parametro>(nuevosValores.Count);

                bool cc = false;

                foreach (DictionaryEntry de in nuevosValores)
                {
                    if (cc) { sbf.Append(','); sbv.Append(','); }

                    string key = de.Key.ToString();

                    sbf.Append(key);
                    sbv.Append("@" + key);
                    parametros.Add(new Parametro("@" + key, de.Value));

                    cc = true;
                }

                executeCustomSQL(String.Format("INSERT INTO UserSettings ({0}) VALUES ({1})", sbf.ToString(), sbv.ToString()), parametros.ToArray());
            }
        }

        #region Campos
        public Int32 IDUSUARIO
        {
            get
            {
                return Convert.ToInt32(registro[Campos.IDUSUARIO]);
            }
            set
            {
                registro[Campos.IDUSUARIO] = value;
            }
        }

        public String SKIN
        {
            get
            {
                return Convert.ToString(registro[Campos.SKIN]);
            }
            set
            {
                registro[Campos.SKIN] = value.Trim();
            }
        }
        #endregion
    }
