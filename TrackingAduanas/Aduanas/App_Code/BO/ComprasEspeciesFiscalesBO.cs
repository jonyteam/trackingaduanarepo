using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for ComprasEspeciesFiscalesBO
/// </summary>
public class ComprasEspeciesFiscalesBO : CapaBase
{
    class Campos
    {
        public static string IDCOMPRA = "IdCompra";
        public static string IDESPECIEFISCAL = "IdEspecieFiscal";
        public static string CODPAIS = "CodPais";
        public static string CODIGOADUANA = "CodigoAduana";
        public static string NUMEROFACTURA = "NumeroFactura";
        public static string RANGOINICIAL = "RangoInicial";
        public static string RANGOFINAL = "RangoFinal";
        public static string CANTIDAD = "Cantidad";
        public static string FECHA = "Fecha";
        public static string CODESTADO = "CodEstado";
        public static string IDUSUARIO = "IdUsuario";
        public static string PROVEEDOR = "Proveedor";
        public static string CARGA = "Carga";
        public static string FECHACARGA = "FechaCarga";
        public static string TOTALCOMPRA = "TotalCompra";
    }

    public ComprasEspeciesFiscalesBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from ComprasEspeciesFiscales CEF ";
        this.initializeSchema("ComprasEspeciesFiscales");
    }

    public void loadComprasEspeciesFiscalesXId(string idCompra)
    {
        this.loadSQL(this.coreSQL + " WHERE CodEstado = '0' AND IdCompra = '" + idCompra + "' ");
    }

    public void loadComprasEspeciesFiscales(string idEspecieFiscal)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdEspecieFiscal = '" + idEspecieFiscal + "' AND CodEstado = '0' ";
        this.loadSQL(sql);
    }

    public void ConseguirIdCompra(int RangoInicial)
    {
        string sql = this.coreSQL;
        sql += " WHERE " + RangoInicial + " between RangoInicial and RangoFinal and CodEstado = 0";
        this.loadSQL(sql);
    }

    public void loadComprasEspeciesFiscales(string idEspecieFiscal, string codigoAduana)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdEspecieFiscal = '" + idEspecieFiscal + "' AND CodigoAduana = '" + codigoAduana + "' AND CodEstado = '0' ";
        this.loadSQL(sql);
    }

    public void loadComprasEspeciesFiscalesVerificar(string rangoInicial, string rangoFinal, string codAduana, string codEspecie)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodEstado != '100' AND CodigoAduana = '" + codAduana + "' AND IdEspecieFiscal = '" + codEspecie + "' ";
        sql += " AND (RangoInicial BETWEEN " + rangoInicial + " AND " + rangoFinal + " OR RangoFinal BETWEEN " + rangoInicial + " AND " + rangoFinal + " ";
        sql += " OR (" + rangoInicial + " BETWEEN RangoInicial AND RangoFinal AND " + rangoFinal + " BETWEEN RangoInicial AND RangoFinal)) ";
        this.loadSQL(sql);
    }

    public void loadRangosEspeciesFiscalesAll()
    {
        string sql = " Select CEF.*, A.NombreAduana, C.Descripcion AS Pais, C1.Descripcion AS EspecieFiscal, U.Usuario from ComprasEspeciesFiscales CEF ";
        sql += " INNER JOIN Aduanas A ON (A.CodigoAduana = CEF.CodigoAduana) ";
        sql += " INNER JOIN Codigos C ON (C.Codigo = CEF.CodPais AND C.Categoria = 'PAISES') ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = CEF.IdEspecieFiscal AND C1.Categoria = 'ESPECIESFISCALES') ";
        sql += " INNER JOIN Usuarios U ON (U.IdUsuario = CEF.IdUsuario) ";
        sql += " WHERE CEF.CodEstado = '0' AND A.CodEstado = '0' ";
        this.loadSQL(sql);
    }

    public void loadRangosEspeciesFiscalesXPais(string codPais)
    {
        string sql = " Select CEF.*, A.NombreAduana, C.Descripcion AS Pais, C1.Descripcion AS EspecieFiscal, U.Usuario from ComprasEspeciesFiscales CEF ";
        sql += " INNER JOIN Aduanas A ON (A.CodigoAduana = CEF.CodigoAduana) ";
        sql += " INNER JOIN Codigos C ON (C.Codigo = CEF.CodPais AND C.Categoria = 'PAISES') ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = CEF.IdEspecieFiscal AND C1.Categoria = 'ESPECIESFISCALES') ";
        sql += " INNER JOIN Usuarios U ON (U.IdUsuario = CEF.IdUsuario) ";
        sql += " WHERE CEF.CodEstado = '0' AND A.CodEstado = '0' AND CEF.CodPais = '" + codPais + "' ";
        this.loadSQL(sql);
    }

    public void CargaUso()
    {
        string sql = " SELECT Convert(Varchar,A.Fecha,104) as 'Fec.docu.' ,'SA' as 'Clase Dto.','9095' as Sociedad,Convert(Varchar,GETDATE(),104) as 'Fec.Con.','10' as Periodo";
               sql += " ,'HNL' as Moneda,'1000010' as 'No.Dto.',[NumeroFactura] as Referencia,+'Compra de Especies Fiscales'+ Convert(Varchar,[Fecha],101) as 'TxtCabDto' ";
               sql += " ,'9095' as 'Divi.', '40' as CLAVE,C.Codigo as Cuenta,Round((TotalCompra/Convert(Money,Cantidad)),2) as 'ImporteMD','Compra '+ D.Descripcion as Texto";
               sql += " ,Round((TotalCompra/Convert(Money,Cantidad)),2) as 'ImporteML','Compra'+Convert(Varchar,A.Cantidad)+ D.Descripcion+E.Descripcion as Texto2 ,E.Codigo as Cuenta2";
               sql += " ,'31' as CLAVE2,Case IdEspecieFiscal When 'SSB' Then 'SS' Else IdEspecieFiscal End as Descripcion ,A.[CodPais],A.[CodigoAduana],[RangoInicial],[RangoFinal],[Cantidad],A.[CodEstado],[IdUsuario],[Proveedor], D.Descripcion";
               sql += " ,[Carga],[TotalCompra] FROM ComprasEspeciesFiscales A";
               sql += " Inner Join Aduanas B on ( A.CodigoAduana = B.CodigoAduana and B.CodEstado = 0 and A.CodEstado = 0)";
               sql += " Inner Join Codigos C on (C.Categoria = 'INVENTARIO' and C.Descripcion = B.NombreAduana and C.Eliminado = 0)";
               sql += " Inner Join Codigos D on (D.Categoria = 'ESPECIESFISCALES' and D.Codigo = A.IdEspecieFiscal and D.Eliminado = 0)";
               sql += " Inner Join Codigos E on (E.Categoria = 'PROVEEDORESESPECIES' and E.Codigo = A.Proveedor and E.Eliminado = 0 )";
               sql += " Where Carga = 0 and A.Fecha >= '2014-08-22'";
        this.loadSQL(sql);
    }
    public void CargaUsoCambiaEstado()
    {
        string sql = " Update ComprasEspeciesFiscales Set Carga = '1', FechaCarga = GetDate() FROM ComprasEspeciesFiscales A ";
               sql += " Inner Join Aduanas B on ( A.CodigoAduana = B.CodigoAduana and B.CodEstado = 0 and A.CodEstado = 0) ";
               sql += " Inner Join Codigos C on (C.Categoria = 'INVENTARIO' and C.Descripcion = B.NombreAduana and C.Eliminado = 0) ";
               sql += " Inner Join Codigos D on (D.Categoria = 'ESPECIESFISCALES' and D.Codigo = A.IdEspecieFiscal and D.Eliminado = 0) ";
               sql += " Inner Join Codigos E on (E.Categoria = 'PROVEEDORESESPECIES' and E.Codigo = A.Proveedor and E.Eliminado = 0 ) ";
               sql += " Where Carga = 0 and A.Fecha >= '2014-08-14' ";
            this.loadSQL(sql);
    }
    
    public void loadNuevoId(string codigo)
    {
        string sql = "declare @Id varchar(50)";
        sql += " select @Id = (SELECT NuevoId = MAX(CAST(LEFT(IdCompra,4)AS varchar(5))) +";
        sql += " RIGHT('' + CAST(MAX(CAST(RIGHT(IdCompra,LEN(IdCompra)- 4)+ 1 AS int))AS varchar(50)),50)";
        sql += " FROM ComprasEspeciesFiscales";
        sql += " Where IdCompra like '" + codigo + "%')";
        sql += " select @Id as NuevoId";
        this.loadSQL(sql);
    }

    public void buscarprecio(string rango, string CodEspecie)
    {
        string sql = " SELECT Top 1 Round((TotalCompra/Convert(Money,Cantidad)),2) as 'ImporteMD' FROM ComprasEspeciesFiscales Where " + rango + " between RangoInicial and RangoFinal";
        sql += " and IdEspecieFiscal = '" + CodEspecie + "' and CodEstado = 0 and CodigoAduana = 'PRINC' ";
        this.loadSQL(sql);
    }
    //Darwin 15/01/2016
    public void ComprasXFecha(string FechaInicio, string FechaFin, string CodEspecie)
    {
        string sql = @"SELECT C.Descripcion,RangoInicial,RangoFinal,IdCompra,Fecha, U.Nombre+' '+U.Apellido as Nombre  FROM ComprasEspeciesFiscales CEF
                       Inner Join Codigos C on (C.Codigo = CEF.IdEspecieFiscal and C.Categoria ='ESPECIESFISCALES')
                       Inner Join Usuarios U on (U.IdUsuario = CEF.IdUsuario)
                       Where CEF.IdEspecieFiscal = '" + CodEspecie + "' and Fecha between '" + FechaInicio + "' and '" + FechaFin + "'";
        this.loadSQL(sql);
    }



    #region campos
    public string IDCOMPRA
    {
        get
        {
            return (string)registro[Campos.IDCOMPRA].ToString();
        }
        set
        {
            registro[Campos.IDCOMPRA] = value;
        }
    }

    public string IDESPECIEFISCAL
    {
        get
        {
            return (string)registro[Campos.IDESPECIEFISCAL].ToString();
        }
        set
        {
            registro[Campos.IDESPECIEFISCAL] = value;
        }
    }

    public string CODPAIS
    {
        get
        {
            return (string)registro[Campos.CODPAIS].ToString();
        }
        set
        {
            registro[Campos.CODPAIS] = value;
        }
    }

    public string CODIGOADUANA
    {
        get
        {
            return (string)registro[Campos.CODIGOADUANA].ToString();
        }
        set
        {
            registro[Campos.CODIGOADUANA] = value;
        }
    }

    public string NUMEROFACTURA
    {
        get
        {
            return (string)registro[Campos.NUMEROFACTURA].ToString();
        }
        set
        {
            registro[Campos.NUMEROFACTURA] = value;
        }
    }

    public string RANGOINICIAL
    {
        get
        {
            return (string)registro[Campos.RANGOINICIAL].ToString();
        }
        set
        {
            registro[Campos.RANGOINICIAL] = value;
        }
    }

    public string RANGOFINAL
    {
        get
        {
            return (string)registro[Campos.RANGOFINAL].ToString();
        }
        set
        {
            registro[Campos.RANGOFINAL] = value;
        }
    }

    public int CANTIDAD
    {
        get
        {
            return int.Parse(registro[Campos.CANTIDAD].ToString());
        }
        set
        {
            registro[Campos.CANTIDAD] = value;
        }
    }

    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }

    public string CODESTADO
    {
        get
        {
            return (string)registro[Campos.CODESTADO].ToString();
        }
        set
        {
            registro[Campos.CODESTADO] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }

    public string PROVEEDOR
    {
        get
        {
            return (string)registro[Campos.PROVEEDOR].ToString();
        }
        set
        {
            registro[Campos.PROVEEDOR] = value;
        }
    }

    public string CARGA
    {
        get
        {
            return (string)registro[Campos.CARGA].ToString();
        }
        set
        {
            registro[Campos.CARGA] = value;
        }
    }
    public string FECHACARGA
    {
        get
        {
            return (string)registro[Campos.FECHACARGA].ToString();
        }
        set
        {
            registro[Campos.FECHACARGA] = value;
        }
    }
    public string TOTALCOMPRA
    {
        get
        {
            return (string)registro[Campos.TOTALCOMPRA].ToString();
        }
        set
        {
            registro[Campos.TOTALCOMPRA] = value;
        }
    }

    #endregion
}
