using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for EventosBO
/// </summary>
public class DetalleGastosEliminadosBO : CapaBase
{

    class Campos
    {

        public static string ID = "Id";
        public static string IDINSTRUCCION = "IdInstruccion";
        public static string IDGASTO = "IdGasto";
        public static string PROVEEDOR = "Proveedor";
        public static string MEDIOPAGO = "MedioPago";
        public static string MONTO = "Monto";
        public static string IVA = "IVA";
        public static string TOTAL = "Total";
        public static string TOTALVENTA = "TotalVenta";
        public static string ESTADO = "Estado";
        public static string IDUSUARIOCREO = "IdUsuarioCreo";
        public static string IDUSUARIOELIMINO = "IdUsuarioElimino";
    }

    public DetalleGastosEliminadosBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from DetalleGastosEliminados DG";
        this.initializeSchema("DetalleGastosEliminados");
    }

    public void loadAutorizaciones()
    {
        string sql = this.coreSQL;
        sql += " where Estado = '0'";
        this.loadSQL(sql);
    }
    #region QUERYS

    public void LlenarGatosXId(string Id)
    {
        string sql = "SELECT * FROM DetalleGastosEliminados Where Id = '" + Id + "'";
        this.loadSQL(sql);
    }

    #endregion

    #region Campos
    public string IDUSUARIOCREO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIOCREO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIOCREO] = value;
        }
    }

    public string IDUSUARIOELIMINO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIOELIMINO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIOELIMINO] = value;
        }
    }
    
    public Double TOTALVENTA
    {
        get
        {
            return Convert.ToDouble(registro[Campos.TOTALVENTA].ToString());
        }
        set
        {
            registro[Campos.TOTALVENTA] = value;
        }
    }
    public string IDINSTRUCCION
    {
        get
        {
            return (string)registro[Campos.IDINSTRUCCION].ToString();
        }
        set
        {
            registro[Campos.IDINSTRUCCION] = value;
        }
    }
    public string IDGASTO
    {
        get
        {
            return (string)registro[Campos.IDGASTO].ToString();
        }
        set
        {
            registro[Campos.IDGASTO] = value;
        }
    }
    public string MEDIOPAGO
    {
        get
        {
            return (string)registro[Campos.MEDIOPAGO].ToString();
        }
        set
        {
            registro[Campos.MEDIOPAGO] = value;
        }
    }
    public string PROVEEDOR
    {
        get
        {
            return (string)registro[Campos.PROVEEDOR].ToString();
        }
        set
        {
            registro[Campos.PROVEEDOR] = value;
        }
    }
    public Double MONTO
    {
        get
        {
            return Convert.ToDouble(registro[Campos.MONTO].ToString());
        }
        set
        {
            registro[Campos.MONTO] = value;
        }
    }
    public Double IVA
    {
        get
        {
            return Convert.ToDouble(registro[Campos.IVA].ToString());
        }
        set
        {
            registro[Campos.IVA] = value;
        }
    }
 
    public string ESTADO
    {
        get
        {
            return (string)registro[Campos.ESTADO].ToString();
        }
        set
        {
            registro[Campos.ESTADO] = value;
        }
    }
    #endregion
}
