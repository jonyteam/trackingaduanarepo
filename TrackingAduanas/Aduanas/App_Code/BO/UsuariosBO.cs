﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for UsuariosBO
/// </summary>
public class UsuariosBO : CapaBase
{
    class Campos
    {
        public static string IDUSUARIO = "IdUsuario";
        public static string NOMBRE = "Nombre";
        public static string APELLIDO = "Apellido";
        public static string EMAIL = "Email";
        public static string CELULAR = "Celular";
        public static string TELEFONO = "Telefono";
        public static string EXTENSION = "Extension";
        public static string USUARIO = "Usuario";
        public static string PASSWORD = "Password";
        public static string IDENTIDAD = "Identidad";
        public static string VENCIMIENTO = "Vencimiento";
        public static string CODIGOADUANA = "CodigoAduana";
        public static string ELIMINADO = "Eliminado";
        public static string IDUSUARIOINGRESO = "IdUsuarioIngreso";
        public static string CODPAIS = "CodPais";
        public static string IDUSUARIOADUANAS = "IdUsuarioAduanas";
    }

    public UsuariosBO(GrupoLis.Login.Login conector)
        : base(conector, true)
    {
        coreSQL = "SELECT * FROM Usuarios";
        initializeSchema("Usuarios");
    }

    public void loadUsuarios()
    {
        string sql = "Select * from Usuarios";
        sql += " where Eliminado = '0'";
        this.loadSQL(sql);
    }
    public void loadUsuarios_Esquema()
    {
        string sql = @"Select 
                      u.IdUsuario,
                      u.Nombre,
                      u.Apellido,
                      u.Email,
                      u.Celular,
                      u.Telefono,
                      u.Extension,
                      u.Usuario,
                      u.Password,
                      u.Identidad,
                      u.Vencimiento,
                      u.CodigoAduana,
                      u.Eliminado,
                      u.IdUsuarioIngreso,
                      u.CodPais,
	                  e.Usuario,
	                  e.Estado ,
	                  case when (ISNULL(e.Usuario,'0')='0' or e.Estado=1) then 0 else 1  end  as esquema 
	                  from Usuarios u left join 
	                  EsquemaTramites2.dbo.Empleados e on  u.Usuario  COLLATE Modern_Spanish_CI_AS =e.Usuario
                      where u.Eliminado = '0' and u.Usuario !=''";

        this.loadSQL(sql);
    }

    public void loadUsuariosNombreCompleto()
    {
        string sql = "Select *, RTRIM(RTRIM(LTRIM(Nombre)) + ' ' + RTRIM(LTRIM(Apellido))) as NombreCompleto from Usuarios";
        sql += " where Eliminado = '0'";
        this.loadSQL(sql);
    }

    public void loadUsuario(string idUsuario)
    {
        string sql = "Select * from Usuarios";
        sql += " where IdUsuario = " + idUsuario + " and Eliminado = '0'";
        this.loadSQL(sql);
    }

    public void loadUsuarioLogin(string usuario)
    {
        string sql = "Select * from Usuarios";
        sql += " where Usuario = @usuario and Eliminado = '0'";
        this.loadSQL(sql, new Parametro("@usuario", usuario));
    }

    public void DatosRemision(string idusuario)
    {
        string sql = "Select Nombre +' '+Apellido as Nombre , B.NombreAduana, C.Codigo+'-'+C.Numeracion as Numero From Usuarios A";
        sql += " Inner Join Aduanas B on (A.CodigoAduana = B.CodigoAduana) Inner Join Numeraciones C on (A.CodigoAduana = C.Codigo)";
        sql += " Where IdUsuario = '"+idusuario+"'";
        this.loadSQL(sql);
    }



    public void DatosRemisionReenvio(string idusuario)
    {
        string sql = "Select Nombre +' '+Apellido as Nombre , B.NombreAduana From Usuarios A  Inner Join Aduanas B on (A.CodigoAduana = B.CodigoAduana) ";
       sql += " Where IdUsuario = '" + idusuario + "'";
        this.loadSQL(sql);
    }

    public void loadUsuarioXUsuario(string usuario)
    {
        string sql = " Select * from Usuarios ";
        sql += " where Usuario = '" + usuario + "' and Eliminado = '0'";
        this.loadSQL(sql);
    }

    public void loadUsuariosPerfilXCodigo(string idRol)
    {
        string sql = "select NombreCompleto = Nombre + ' ' + Apellido, u.IdUsuario from Usuarios u";
        sql += " inner join UsuariosRoles ur on (u.IdUsuario = ur.IdUsuario) inner join Roles r on (ur.IdRol = r.IdRol)";
        sql += " where r.IdRol = @idRol and r.Eliminado = '0' and u.Eliminado = '0'";
        this.loadSQL(sql, new Parametro("@idRol", idRol));
    }

    public void loadUsuariosPerfilXNombre(string nombreRol)
    {
        string sql = "select NombreCompleto = Nombre + ' ' + Apellido, u.IdUsuario from Usuarios u";
        sql += " inner join UsuariosRoles ur on (u.IdUsuario = ur.IdUsuario) inner join Roles r on (ur.IdRol = r.IdRol)";
        sql += " where r.Descripcion = @nombreRol and r.Eliminado = '0' and u.Eliminado = '0'";
        this.loadSQL(sql, new Parametro("@nombreRol", nombreRol));
    }

    public void loadUsuariosNombreCompletoXAduana(string codAduana, string rol)
    {
        string sql = "Select *, RTRIM(RTRIM(LTRIM(Nombre)) + ' ' + RTRIM(LTRIM(Apellido))) as NombreCompleto from Usuarios u ";
        sql += " inner join UsuariosRoles ur on (u.IdUsuario = ur.IdUsuario) inner join Roles r on (ur.IdRol = r.IdRol)";
        sql += " where u.Eliminado = '0' AND r.Eliminado = '0' AND CodigoAduana = '" + codAduana + "' AND r.Descripcion = '" + rol + "' ";
        this.loadSQL(sql);
    }

    public void loadUsuariosNombreCompletoXAduanaGestores(string codAduana, string rol)
    {
        string sql = "Select *, RTRIM(RTRIM(LTRIM(Nombre)) + ' ' + RTRIM(LTRIM(Apellido))) as NombreCompleto from Usuarios u ";
        sql += " inner join UsuariosRoles ur on (u.IdUsuario = ur.IdUsuario) inner join Roles r on (ur.IdRol = r.IdRol)";
        sql += " where u.Eliminado = '0' AND r.Eliminado = '0' AND CodigoAduana = '" + codAduana + "' AND r.Descripcion = '" + rol + "' ";
        this.loadSQL(sql);
    }


    public void loadUsuariosNombreCompletoXPais(string codPais, string rol)
    {
        string sql = "Select *, RTRIM(RTRIM(LTRIM(Nombre)) + ' ' + RTRIM(LTRIM(Apellido))) as NombreCompleto from Usuarios u ";
        sql += " inner join UsuariosRoles ur on (u.IdUsuario = ur.IdUsuario) inner join Roles r on (ur.IdRol = r.IdRol)";
        sql += " where u.Eliminado = '0' AND r.Eliminado = '0' AND CodPais = '" + codPais + "' AND r.Descripcion = '" + rol + "' ";
        this.loadSQL(sql);
    }

    public void loadUsuariosXPais(string codPais)
    {
        string sql = "Select * from Usuarios";
        sql += " where Eliminado = '0' AND CodPais = '" + codPais + "' ";
        this.loadSQL(sql);
    }

    public string IDUSUARIO
    {
        get
        {
            return registro[Campos.IDUSUARIO].ToString();
        }
    }

    public string NOMBRE
    {
        get
        {
            return registro[Campos.NOMBRE].ToString();
        }
        set
        {
            registro[Campos.NOMBRE] = value;
        }
    }

    public string APELLIDO
    {
        get
        {
            return registro[Campos.APELLIDO].ToString();
        }
        set
        {
            registro[Campos.APELLIDO] = value;
        }
    }

    public string EMAIL
    {
        get
        {
            return registro[Campos.EMAIL].ToString();
        }
        set
        {
            registro[Campos.EMAIL] = value;
        }
    }

    public string TELEFONO
    {
        get
        {
            return registro[Campos.TELEFONO].ToString();
        }
        set
        {
            registro[Campos.TELEFONO] = value;
        }
    }

    public string EXTENSION
    {
        get
        {
            return registro[Campos.EXTENSION].ToString();
        }
        set
        {
            registro[Campos.EXTENSION] = value;
        }
    }

    public string CELULAR
    {
        get
        {
            return registro[Campos.CELULAR].ToString();
        }
        set
        {
            registro[Campos.CELULAR] = value;
        }
    }

    public string IDENTIDAD
    {
        get
        {
            return registro[Campos.IDENTIDAD].ToString();
        }
        set
        {
            registro[Campos.IDENTIDAD] = value;
        }
    }

    public string USUARIO
    {
        get
        {
            return registro[Campos.USUARIO].ToString();
        }
        set
        {
            registro[Campos.USUARIO] = value;
        }
    }

    public string PASSWORD
    {
        get
        {
            return registro[Campos.PASSWORD].ToString();
        }
        set
        {
            registro[Campos.PASSWORD] = value;
        }
    }

    public DateTime VENCIMIENTO
    {
        get
        {
            return Convert.ToDateTime(registro[Campos.VENCIMIENTO].ToString());
        }
        set
        {
            registro[Campos.VENCIMIENTO] = value;
        }
    }

    public string CODIGOADUANA
    {
        get
        {
            return registro[Campos.CODIGOADUANA].ToString();
        }
        set
        {
            registro[Campos.CODIGOADUANA] = value;
        }
    }

    public string ELIMINADO
    {
        get
        {
            return registro[Campos.ELIMINADO].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO] = value;
        }
    }

    public string IDUSUARIOINGRESO
    {
        get
        {
            return registro[Campos.IDUSUARIOINGRESO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIOINGRESO] = value;
        }
    }

    public string CODPAIS
    {
        get
        {
            return registro[Campos.CODPAIS].ToString();
        }
        set
        {
            registro[Campos.CODPAIS] = value;
        }
    }

    public string IDUSUARIOADUANAS
    {
        get
        {
            return registro[Campos.IDUSUARIOADUANAS].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIOADUANAS] = value;
        }
    }

}
