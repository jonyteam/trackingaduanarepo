using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for AduanasBO
/// </summary>
public class PlantillaGastosBO : CapaBase
{
    class Campos
    {
            public static string ITEM="Item";
            public static string IDINSTRUCCION="IdInstruccion";
            public static string FACTURAVESTA="FacturaVESTA";
            public static string FECHAENTREGACARGILL="FechaEntregaCargill";
            public static string ORDENCOMPRA="OrdenCompra";
            public static string ETAPLANTA="ETAPlanta";
            public static string TRACKINGCOURIER = "TrackingCourier";
            public static string FECHARECIBOCOPIAS="FechaReciboCopias";
            public static string FECHARECIBOORIGINALES="FechaReciboOriginales";
            public static string FECHAARRIBOPLANTAPROCESO="FechaArriboPlantaProceso";
            public static string TIEMPOLLEGADAHASTAENTREGA="TiempoLlegadaHastaEntrega";
            public static string DIASLIBRESALMACENAJE="DiasLibresAlmacenaje";
            public static string TIEMPOENERGIA="TiempoEnergia";
            public static string TERMINATIEMPOLIBREDCS="TerminatiempolibreDCS";
            public static string CARGOALMACENAJEDCS="CargoAlmacenajeDCS";
            public static string TERMINATIEMPOLIBREELECTRICIDAD="TerminaTiempoLibreElectricidad";
            public static string CARGODEMORAELECTRICIDAD="CargoDemoraElectricidad";
            public static string FECHARETORNOCONTENEDOR="FechaRetornoContenedor";
            public static string DIASDEMORA="DiasDemora";
            public static string DEMORA="Demora";
            public static string OBSERVACIONES="Observaciones";
            public static string DECLARACIONSEGURO="DeclaracionSeguro";
            public static string FECHADEFACTURA2 = "FechadeFactura2";

    }

    public PlantillaGastosBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from PlantillaGastos ";
        this.initializeSchema("PlantillaGastos");
    }



    #region Campos
    
    public string IDINSTRUCCION
    {
        get
        {
            return (string)registro[Campos.IDINSTRUCCION].ToString();
        }
        set
        {
            registro[Campos.IDINSTRUCCION] = value;
        }
    }

    public string FACTURAVESTA
    {
        get
        {
            return (string)registro[Campos.FACTURAVESTA].ToString();
        }
        set
        {
            registro[Campos.FACTURAVESTA] = value;
        }
    }

    public string FECHAENTREGACARGILL
    {
        get
        {
            return (string)registro[Campos.FECHAENTREGACARGILL].ToString();
        }
        set
        {
            registro[Campos.FECHAENTREGACARGILL] = value;
        }
    }

    public string ORDENCOMPRA
    {
        get
        {
            return (string)registro[Campos.ORDENCOMPRA].ToString();
        }
        set
        {
            registro[Campos.ORDENCOMPRA] = value;
        }
    }

    public string ETAPLANTA
    {
        get
        {
            return (string)registro[Campos.ETAPLANTA].ToString();
        }
        set
        {
            registro[Campos.ETAPLANTA] = value;
        }
    }

    public string FECHADEFACTURA2
    {
        get
        {
            return (string)registro[Campos.FECHADEFACTURA2].ToString();
        }
        set
        {
            registro[Campos.FECHADEFACTURA2] = value;
        }
    }
    public string TRACKINGCOURIER
    {
        get
        {
            return (string)registro[Campos.TRACKINGCOURIER].ToString();
        }
        set
        {
            registro[Campos.TRACKINGCOURIER] = value;
        }
    }
    public string FECHARECIBOCOPIAS
    {
        get
        {
            return (string)registro[Campos.FECHARECIBOCOPIAS].ToString();
        }
        set
        {
            registro[Campos.FECHARECIBOCOPIAS] = value;
        }
    }

    public string FECHARECIBOORIGINALES
    {
        get
        {
            return (string)registro[Campos.FECHARECIBOORIGINALES].ToString();
        }
        set
        {
            registro[Campos.FECHARECIBOORIGINALES] = value;
        }
    }

    public string FECHAARRIBOPLANTAPROCESO
    {
        get
        {
            return (string)registro[Campos.FECHAARRIBOPLANTAPROCESO].ToString();
        }
        set
        {
            registro[Campos.FECHAARRIBOPLANTAPROCESO] = value;
        }
    }

    public string TIEMPOLLEGADAHASTAENTREGA
    {
        get
        {
            return (string)registro[Campos.TIEMPOLLEGADAHASTAENTREGA].ToString();
        }
        set
        {
            registro[Campos.TIEMPOLLEGADAHASTAENTREGA] = value;
        }
    }

    public string DIASLIBRESALMACENAJE
    {
        get
        {
            return (string)registro[Campos.DIASLIBRESALMACENAJE].ToString();
        }
        set
        {
            registro[Campos.DIASLIBRESALMACENAJE] = value;
        }
    }

    public string TIEMPOENERGIA
    {
        get
        {
            return (string)registro[Campos.TIEMPOENERGIA].ToString();
        }
        set
        {
            registro[Campos.TIEMPOENERGIA] = value;
        }
    }

    public string TERMINATIEMPOLIBREDCS
    {
        get
        {
            return (string) registro[Campos.TERMINATIEMPOLIBREDCS].ToString();
        }
        set
        {
            registro[Campos.TERMINATIEMPOLIBREDCS] = value;
        }
    }

    public string CARGOALMACENAJEDCS
    {
        get
        {
            return (string)registro[Campos.CARGOALMACENAJEDCS].ToString();
        }
        set
        {
            registro[Campos.CARGOALMACENAJEDCS] = value;
        }
    }

    public string TERMINATIEMPOLIBREELECTRICIDAD
    {
        get
        {
            return (string) registro[Campos.TERMINATIEMPOLIBREELECTRICIDAD].ToString();
        }
        set
        {
            registro[Campos.TERMINATIEMPOLIBREELECTRICIDAD] = value;
        }
    }

    public string CARGODEMORAELECTRICIDAD
    {
        get
        {
            return (string) registro[Campos.CARGODEMORAELECTRICIDAD].ToString();
        }
        set
        {
            registro[Campos.CARGODEMORAELECTRICIDAD] = value;
        }
    }

    public string FECHARETORNOCONTENEDOR
    {
        get
        {
            return (string)registro[Campos.FECHARETORNOCONTENEDOR].ToString();
        }
        set
        {
            registro[Campos.FECHARETORNOCONTENEDOR] = value;
        }
    }

    public string DIASDEMORA
    {
        get
        {
            return (string)registro[Campos.DIASDEMORA].ToString();
        }
        set
        {
            registro[Campos.DIASDEMORA] = value;
        }
    }

    public string DEMORA
    {
        get
        {
            return (string)registro[Campos.DEMORA].ToString();
        }
        set
        {
            registro[Campos.DEMORA] = value;
        }
    }

    public string OBSERVACIONES
    {
        get
        {
            return (string)registro[Campos.OBSERVACIONES].ToString();
        }
        set
        {
            registro[Campos.OBSERVACIONES] = value;
        }
    }

    public string DECLARACIONSEGURO
    {
        get
        {
            return (string)registro[Campos.DECLARACIONSEGURO].ToString();
        }
        set
        {
            registro[Campos.DECLARACIONSEGURO] = value;
        }
    }

    #endregion

}
