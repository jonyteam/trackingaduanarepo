﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for TramitesBO
/// </summary>
public class ContenedoresBO : CapaBase
{
	class Campos
    {
        public static string ID = "Id";        
        public static string IDTRAMITE = "IdTramite";      
        public static string CONTENEDOR = "Contenedor";
        public static string PRECINTOS = "Precintos";
        public static string DIMENSIONES = "Dimensiones";
        public static string TIPOCARGA = "TipoCarga";
        public static string VACIO = "Vacio";
        public static string ESTADO = "Estado";
    }

    public ContenedoresBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from Contenedores";
        this.initializeSchema("Contenedores");
    }

    public void loadContenedores()
    {
        string sql = this.coreSQL;
        this.loadSQL(sql);
    }

    public void loadContenedorI(string Tramite)
    {
        string sql = "Select * from Contenedores";
        sql += " where IdTramite = '" + Tramite + "'";
        this.loadSQL(sql);
    }

    public void loadContenedorId(string id)
    {
        string sql = "Select * from Contenedores";
        sql += " where Id = '" + id + "'";
        this.loadSQL(sql);
    }

    public void loadContenedorEstado(string Tramite)
    {
        string sql = "Select * from Contenedores where IdTramite='"+Tramite+"' and estado=0";
        this.loadSQL(sql);
    }

    public void EstadoContenedores(string Estado,string Tramite)
    {
        string sql = "exec EstadoContenedores '"+Estado+"','"+Tramite+"'";
        this.loadSQL(sql);
    }
   
    public string ID
    {
        get
        {
            return (string)registro[Campos.ID].ToString();
        }
        set
        {
            registro[Campos.ID] = value;
        }
    }

    public string IDTRAMITE
    {
        get
        {
            return (string)registro[Campos.IDTRAMITE].ToString();
        }
        set
        {
            registro[Campos.IDTRAMITE] = value;
        }
    }

    public string CONTENEDOR
    {
        get
        {
            return (string)registro[Campos.CONTENEDOR].ToString();
        }
        set
        {
            registro[Campos.CONTENEDOR] = value;
        }
    }

    public string PRECINTOS
    {
        get
        {
            return (string)registro[Campos.PRECINTOS].ToString();
        }
        set
        {
            registro[Campos.PRECINTOS] = value;
        }
    }

    public string DIMENSIONES
    {
        get
        {
            return (string)registro[Campos.DIMENSIONES].ToString();
        }
        set
        {
            registro[Campos.DIMENSIONES] = value;
        }
    }

    public string TIPOCARGA
    {
        get
        {
            return (string)registro[Campos.TIPOCARGA].ToString();
        }
        set
        {
            registro[Campos.TIPOCARGA] = value;
        }
    }

    public string VACIO
    {
        get
        {
            return (string)registro[Campos.VACIO].ToString();
        }
        set
        {
            registro[Campos.VACIO] = value;
        }
    }

    public string ESTADO
    {
        get
        {
            return (string)registro[Campos.ESTADO].ToString();
        }
        set
        {
            registro[Campos.ESTADO] = value;
        }
    }
}