using System;
using System.Data;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for ProveedoresPaisBO
/// </summary>
public class ProveedoresPaisBO : CapaBase
{
    class Campos
    {
        public static string CODIGO = "Codigo";
        public static string DESCRIPCION = "Descripcion";
        public static string CODPAIS = "CodPais";
    }

    public ProveedoresPaisBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from ProveedoresPais PP";
        this.initializeSchema("ProveedoresPais");
    }

    public void loadProveedoresPais(string codPais)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodPais = '" + codPais + "' ORDER BY Descripcion ";
        this.loadSQL(sql);
    }

    public void loadProveedoresPais(string codPais, string proveedor)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodPais = '" + codPais + "' AND Descripcion = '" + proveedor + "' ";
        this.loadSQL(sql);
    }

    public void insertProveedoresPais(string proveedor, string codPais)
    {
        this.executeCustomSQL(String.Format("INSERT INTO ProveedoresPais (Descripcion, CodPais) VALUES ('{0}', '{1}')", proveedor.ToUpper(), codPais));
    }

    public void deleteProveedoresPais(string codigo, string codPais)
    {
        this.executeCustomSQL(String.Format("DELETE FROM ProveedoresPais WHERE Codigo={0} and CodPais='{1}'", codigo, codPais));
    }


    public string CODIGO
    {
        get
        {
            return Convert.ToString(registro[Campos.CODIGO]);
        }
        set
        {
            registro[Campos.CODIGO] = value;
        }
    }

    public string DESCRIPCION
    {
        get
        {
            return Convert.ToString(registro[Campos.DESCRIPCION]);
        }
        set
        {
            registro[Campos.DESCRIPCION] = value;
        }
    }

    public string CODPAIS
    {
        get
        {
            return Convert.ToString(registro[Campos.CODPAIS]);
        }
        set
        {
            registro[Campos.CODPAIS] = value;
        }
    }
}
