using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for DocumentosInstruccionesBO
/// </summary>
public class DocumentosInstruccionesBO : CapaBase
{
    class Campos
    {
        public static string IDINSTRUCCION = "IdInstruccion";
        public static string CODDOCUMENTO = "CodDocumento";
        public static string ORIGINAL = "Original";
        public static string COPIA = "Copia";
        public static string DOCUMENTONO = "DocumentoNo";
        public static string OBSERVACION = "Observacion";
        public static string FECHAORIGINAL = "FechaOriginal";
        public static string FECHACOPIA = "FechaCopia";
        public static string IDUSUARIOORIGINAL = "IdUsuarioOriginal";
        public static string IDUSUARIOCOPIA = "IdUsuarioCopia";
        public static string ELIMINADO = "Eliminado";
        public static string REQUERIDO = "Requerido";
    }

    public DocumentosInstruccionesBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from DocumentosInstrucciones DI ";
        this.initializeSchema("DocumentosInstrucciones");
    }

    public void loadDocumentosXInstrucciones(string idInstruccion, string codDocumento)
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' AND IdInstruccion = '" + idInstruccion + "' AND CodDocumento = '" + codDocumento + "' ";
        this.loadSQL(sql);
    }

    public void loadDocumentosXInstrucciones(string idInstruccion)
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' AND IdInstruccion = '" + idInstruccion + "' ";
        this.loadSQL(sql);
    }

    public void loadDocumentosMinimosXInstruccion(string idInstruccion)
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' AND IdInstruccion = '" + idInstruccion + "' AND Requerido = '1' ";
        this.loadSQL(sql);
    }

    public int inicializarDocumentoOriginal(string idInstruccion, string codDocumento)
    {
        string sql = " UPDATE DocumentosInstrucciones SET Original = NULL, FechaOriginal = NULL, IdUsuarioOriginal = NULL ";
        sql += " WHERE IdInstruccion = '" + idInstruccion + "' AND CodDocumento = '" + codDocumento + "' ";
        return this.executeCustomSQL(sql);
    }

    public int inicializarDocumentoCopia(string idInstruccion, string codDocumento)
    {
        string sql = " UPDATE DocumentosInstrucciones SET Copia = NULL, FechaCopia = NULL, IdUsuarioCopia = NULL ";
        sql += " WHERE IdInstruccion = '" + idInstruccion + "' AND CodDocumento = '" + codDocumento + "' ";
        return this.executeCustomSQL(sql);
    }

    public int eliminaDocumentosEmbarques(string idInstruccion, string codDocumento)
    {
        string sql = " UPDATE DocumentosInstrucciones SET Eliminado = '1' ";
        sql += " WHERE IdInstruccion = '" + idInstruccion + "' ";
        sql += " AND CodDocumento IN (SELECT Codigo FROM Codigos WHERE Categoria = 'DOCUMENTOEMBARQUE' AND Codigo != '" + codDocumento + "') ";
        return this.executeCustomSQL(sql);
    }

    #region Campos
    public string IDINSTRUCCION
    {
        get
        {
            return (string)registro[Campos.IDINSTRUCCION].ToString();
        }
        set
        {
            registro[Campos.IDINSTRUCCION] = value;
        }
    }

    public string CODDOCUMENTO
    {
        get
        {
            return (string)registro[Campos.CODDOCUMENTO].ToString();
        }
        set
        {
            registro[Campos.CODDOCUMENTO] = value;
        }
    }

    public string ORIGINAL
    {
        get
        {
            return (string)registro[Campos.ORIGINAL].ToString();
        }
        set
        {
            registro[Campos.ORIGINAL] = value;
        }
    }

    public string COPIA
    {
        get
        {
            return (string)registro[Campos.COPIA].ToString();
        }
        set
        {
            registro[Campos.COPIA] = value;
        }
    }

    public string DOCUMENTONO
    {
        get
        {
            return (string)registro[Campos.DOCUMENTONO].ToString();
        }
        set
        {
            registro[Campos.DOCUMENTONO] = value;
        }
    }

    public string OBSERVACION
    {
        get
        {
            return (string)registro[Campos.OBSERVACION].ToString();
        }
        set
        {
            registro[Campos.OBSERVACION] = value;
        }
    }

    public string FECHAORIGINAL
    {
        get
        {
            return (string)registro[Campos.FECHAORIGINAL].ToString();
        }
        set
        {
            registro[Campos.FECHAORIGINAL] = value;
        }
    }

    public string FECHACOPIA
    {
        get
        {
            return (string)registro[Campos.FECHACOPIA].ToString();
        }
        set
        {
            registro[Campos.FECHACOPIA] = value;
        }
    }

    public string IDUSUARIOORIGINAL
    {
        get
        {
            return (string)registro[Campos.IDUSUARIOORIGINAL].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIOORIGINAL] = value;
        }
    }

    public string IDUSUARIOCOPIA
    {
        get
        {
            return (string)registro[Campos.IDUSUARIOCOPIA].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIOCOPIA] = value;
        }
    }

    public string ELIMINADO
    {
        get
        {
            return (string)registro[Campos.ELIMINADO].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO] = value;
        }
    }

    public string REQUERIDO
    {
        get
        {
            return (string)registro[Campos.REQUERIDO].ToString();
        }
        set
        {
            registro[Campos.REQUERIDO] = value;
        }
    }
    #endregion
}
