using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for AduanasBO
/// </summary>
public class InstruccionesBO : CapaBase
{
    class Campos
    {
        public static string IDTRAMITE = "IdTramite";
        public static string IDINSTRUCCION = "IdInstruccion";
        public static string CODPAISHOJARUTA = "CodPaisHojaRuta";
        public static string CODIGOADUANA = "CodigoAduana";
        public static string FECHARECEPCION = "FechaRecepcion";
        public static string IDCLIENTE = "IdCliente";
        public static string OFICIALCUENTA = "OficialCuenta";
        public static string REFERENCIACLIENTE = "ReferenciaCliente";
        public static string PROVEEDOR = "Proveedor";
        public static string NOFACTURA = "NoFactura";
        public static string IDESPECIEFISCAL = "IdEspecieFiscal";
        public static string IDDOCUMENTOESPECIEFISCAL = "IdDocumentoEspecieFiscal";
        public static string VALORFOB = "ValorFOB";
        public static string FLETE = "Flete";
        public static string SEGURO = "Seguro";
        public static string OTROS = "Otros";
        public static string VALORCIF = "ValorCIF";
        public static string CODDOCUMENTOEMBARQUE = "CodDocumentoEmbarque";
        public static string NODOCUMENTOEMBARQUE = "NoDocumentoEmbarque";
        public static string CANTIDADBULTOS = "CantidadBultos";
        public static string PESOKGS = "PesoKgs";
        public static string CONTENEDORES = "Contenedores";
        public static string DESCRIPCIONCONTENEDORES = "DescripcionContenedores";
        public static string PRODUCTO = "Producto";
        public static string CODREGIMEN = "CodRegimen";
        public static string CODPAISPROCEDENCIA = "CodPaisProcedencia";
        public static string CODPAISORIGEN = "CodPaisOrigen";
        public static string CODPAISDESTINO = "CodPaisDestino";
        public static string CIUDADDESTINO = "CiudadDestino";
        public static string CODTIPOTRANSPORTE = "CodTipoTransporte";
        public static string CODTIPOCARGAMENTO = "CodTipoCargamento";
        public static string CODTIPOCARGA = "CodTipoCarga";
        public static string FECHAVENCTIEMPOLIBRENAVIERA = "FechaVencTiempoLibreNaviera";
        public static string FECHAVENCTIEMPOLIBREPUERTO = "FechaVencTiempoLibrePuerto";
        public static string ETA = "ETA";
        public static string TIEMPOESTIMADOENTREGA = "TiempoEstimadoEntrega";
        public static string INSTRUCCIONESENTREGA = "InstruccionesEntrega";
        public static string OBSERVACION = "Observacion";
        public static string FECHA = "Fecha";
        public static string CODESTADO = "CodEstado";
        public static string IDESTADOFLUJO = "IdEstadoFlujo";
        public static string IDUSUARIO = "IdUsuario";
        public static string IDUSUARIOADUANA = "IdUsuarioAduana";
        public static string ARRIBOCARGA = "ArriboCarga";
        public static string NOCORRELATIVO = "NoCorrelativo";
        public static string AFORADOR = "Aforador";
        public static string COLOR = "Color";
        public static string TRAMITADOR = "Tramitador";
        public static string IMPUESTO = "Impuesto";
        public static string CODIGOGUIA = "CodigoGuia";
        public static string DIVISION = "Division";
        public static string INCOTERM = "Incoterm";
        public static string NUMEROFACTURA = "NumeroFactura";
        public static string ISV = "ISV";
        public static string DAI = "DAI";
        public static string SELECTIVO = "Selectivo";
        public static string STD = "STD";
        public static string PRODUCCIONCONSUMO = "ProduccionConsumo";
        public static string OTROSIMPUESTOS = "OtrosImpuestos";
        public static string PRODUCTOSCARGILL = "ProductosCargill";
        public static string FECHAFINALFLUJO = "FechaFinalFlujo";
        public static string IDOFICIALCUENTA = "IdOficialCuenta";
        public static string RDUA = "RDua";


    }

    public InstruccionesBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select top 1 * from Instrucciones I ";
        this.initializeSchema("Instrucciones");
    }

    public void loadInstrucciones()
    {
        string sql = " Select * from Instrucciones I ";
        sql += " WHERE CodEstado in('0','1') AND IdEstadoFlujo != '99' ORDER BY CodigoAduana, Item ";
        this.loadSQL(sql);
    }

    public void CargarRemision(string CodAduana)
    {
        string sql = "Select A.IdInstruccion as HojaRuta,B.Nombre ,NumeroFactura,NoCorrelativo,Proveedor,C.Serie,C.CodEspecieFiscal From Instrucciones A";
        sql += " Inner Join Clientes B on (A.IdCliente = B.CodigoSAP and B.Eliminado = 0)";
        sql += " Left Join EspeciesFiscalesInstrucciones C on (A.IdInstruccion = C.IdInstruccion and C.Eliminado = '0' and C.CodEspecieFiscal in ('F','P') and C.Necesario = 'X')";
        sql += " Where Case When CodigoAduana = '016' then DATEADD(Week,3,FechaFinalFlujo) else DATEADD(Week,3,FechaFinalFlujo) end >= GETDATE()  and A.CodEstado != '100' and CodigoAduana = '" + CodAduana + "' and A.IdInstruccion not in(Select IdInstruccion From Remisiones) order by 1";
        this.loadSQL(sql);
    }

    public void CargarRemisionAdministrador()
    {
        string sql = "Select A.IdInstruccion as HojaRuta,B.Nombre ,NumeroFactura,NoCorrelativo,Proveedor,C.Serie,C.CodEspecieFiscal From Instrucciones A";
        sql += " Inner Join Clientes B on (A.IdCliente = B.CodigoSAP and B.Eliminado = 0)";
        sql += " Left Join EspeciesFiscalesInstrucciones C on (A.IdInstruccion = C.IdInstruccion and C.Eliminado = '0' and C.CodEspecieFiscal in ('F','P') and C.Necesario = 'X')";
        sql += " Where DATEADD(Week,12,FechaFinalFlujo) >= GETDATE() and A.CodEstado != '100' and A.CodPaisHojaRuta = 'H' and A.IdInstruccion not in(Select IdInstruccion From Remisiones) order by 1";
        this.loadSQL(sql);
    }
    // Reenvio de remisiones Arle aNDINO 28
    public void CargarRemisionAdministradorRenvio(String HojaR, String IdRemision)
    {

        string sql = "   Select A.IdInstruccion as HojaRuta,B.Nombre ,NumeroFactura,NoCorrelativo,Proveedor,C.Serie,C.CodEspecieFiscal, D.NumeroRemision";
        sql += " From Instrucciones A Inner Join Clientes B on (A.IdCliente = B.CodigoSAP and B.Eliminado = 0) ";
        sql += " Left Join EspeciesFiscalesInstrucciones C on (A.IdInstruccion = C.IdInstruccion and C.Eliminado = '0' and C.CodEspecieFiscal in ('F','P') and C.Necesario = 'X') ";
        sql += "left join Remisiones D on   A.IdInstruccion=D.IdInstruccion";
        sql += " Where ";
        sql += "A.CodEstado != '100' ";
        sql += " and A.CodPaisHojaRuta = 'H'  and ( D.NumeroRemision ='" + IdRemision + "' or  D.NumeroRemision in ( Select top 1  NumeroRemision From Remisiones where IdInstruccion ='" + HojaR + "'))";
        this.loadSQL(sql);

    }

    public void loadInstruccionesTemp()
    {
        string sql = " Select * from Instrucciones I ";
        sql += " WHERE CodEstado = '0' AND IdEstadoFlujo != '99' and ArriboCarga = 0 ORDER BY CodigoAduana, Item ";
        this.loadSQL(sql);
    }
    public void ValidarArribo(string idinstruccion)
    {
        string sql = " Select * from Instrucciones I ";
        sql += " WHERE CodEstado = '0' AND IdEstadoFlujo != '99' and ArriboCarga = 0 and IdInstruccion = '" + idinstruccion + "'ORDER BY CodigoAduana, Item ";
        this.loadSQL(sql);
    }


    public void loadInstruccionesCliente()
    {
        string sql = " SELECT DISTINCT I.IdInstruccion, I.*, C.Nombre, DI.DocumentoNo from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = 0) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1' AND DI.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0','1') AND I.IdEstadoFlujo != '99' ";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesClienteInstr()
    {
        string sql = " SELECT * from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        sql += " WHERE I.CodEstado IN ('0') AND I.IdEstadoFlujo != '99' ";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesSolicitudPago()
    {
        string sql = @" SELECT * from Instrucciones I  
                        INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente)  
                        WHERE I.CodEstado IN ('0','1') AND I.IdEstadoFlujo != '99' AND I.CodPaisHojaRuta != 'H' and ISNULL(I.FechaFinalFlujo,I.Fecha) >= '2014-09-01' 
                        ORDER BY I.CodigoAduana, I.Item ";



        this.loadSQL(sql);
    }

    public void loadInstruccionesSolicitudPagoConGuiaFinalizada()
    {
        string sql = @"  SELECT * from Instrucciones I  
                        INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente)  
                        WHERE I.CodEstado IN ('0','1') AND I.IdEstadoFlujo != '99' AND I.CodPaisHojaRuta not in ('H','S') and ISNULL(I.FechaFinalFlujo,I.Fecha) >= '2014-09-01' 
                    
							union
					    SELECT * from Instrucciones I  
                        INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente)  
                        WHERE I.CodEstado IN ('0','1') 
						
						AND I.CodPaisHojaRuta != 'H' and ISNULL(I.FechaFinalFlujo,I.Fecha) >= '2014-09-01'  and 
						
						 ISNULL(I.FechaFinalFlujo,I.Fecha) >  DATEADD( WEEK,-4 , GETDATE())
					     and 
						CodigoPais='S' 
		                ORDER BY I.CodigoAduana, I.Item ";



        this.loadSQL(sql);
    }



    public void loadInstruccionesSolicitudPagoSalvador(String Week)
    {
        string sql = @" SELECT * from Instrucciones I  
                        INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente)  
                        WHERE I.CodEstado IN ('0','1') 		
						AND I.CodPaisHojaRuta != 'H' and ISNULL(I.FechaFinalFlujo,I.Fecha) >= '2014-09-01'  and 			
						ISNULL(I.FechaFinalFlujo,I.Fecha) >  DATEADD( WEEK,-" + Week + " , GETDATE())" +
                        @" and 
						CodigoPais='S' 
                        ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }


    public void loadInstruccionesClienteSolicitudPago()
    {
        string sql = " SELECT A.IdInstruccion as HojaRuta,A.IdGasto , C.Gasto as Material ,D.Descripcion as MedioPago ";
        sql += " ,E.Descripcion as Proveedor,A.Monto as Monto,A.IVA as IVA,A.Total as Total ";
        sql += " ,F.Descripcion as Moneda, A.Id as Id from DetalleGastos A";
        sql += " INNER JOIN GatosTracking C on (A.IdGasto = C.IdGasto and C.CodPais = A.CodPais)";
        sql += " INNER JOIN Codigos D on (D.Categoria = 'MEDIOPAGO' and D.Codigo = A.MedioPago)";
        sql += " INNER JOIN Codigos E on (E.Categoria = 'PROVEEDOR' and E.Codigo = A.Proveedor)";
        sql += " INNER JOIN Codigos F on (F.Categoria = 'MONEDA' and F.Codigo = A.Moneda)";
        sql += " WHERE A.Estado = 0  and C.Estado = 0 Order By IdInstruccion";
        this.loadSQL(sql);
    }

    public void loadInstruccionesClienteSolicitudPagoXPais(string pais)
    {
        string sql = " SELECT A.IdInstruccion as HojaRuta, C.Gasto as Material ,D.Descripcion as MedioPago ";
        sql += " ,E.Descripcion as Proveedor,A.Monto as Monto,A.IVA as IVA,A.Total as Total ";
        sql += " ,F.Descripcion as Moneda from DetalleGastos A";
        sql += " INNER JOIN GatosTracking C on (A.IdGasto = C.IdGasto and C.CodPais = A.CodPais)";
        sql += " INNER JOIN Codigos D on (D.Categoria = 'MEDIOPAGO' and D.Codigo = A.MedioPago)";
        sql += " INNER JOIN Codigos E on (E.Categoria = 'PROVEEDOR' and E.Codigo = A.Proveedor)";
        sql += " INNER JOIN Codigos F on (F.Categoria = 'MONEDA' and F.Codigo = A.Moneda)";
        sql += " WHERE A.Estado = 0 and C.Estado = 0 and A.CodPais = '" + pais + "'Order By IdInstruccion";
        this.loadSQL(sql);
    }

    public void loadInstruccionesXPais(string codPais)
    {
        string sql = " SELECT DISTINCT I.IdInstruccion, I.*, C.Nombre, DI.DocumentoNo from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = 0) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1' AND DI.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0','1') AND I.IdEstadoFlujo != '99' AND I.CodPaisHojaRuta = '" + codPais + "' ";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesXPaisUsuario(string codPais, string idUsuario)
    {
        string sql = " SELECT DISTINCT I.IdInstruccion, I.*, C.Nombre, DI.DocumentoNo from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = 0) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1' AND DI.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0','1') AND I.IdEstadoFlujo != '99' AND I.CodPaisHojaRuta = '" + codPais + "' AND I.IdUsuario = '" + idUsuario + "'";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesXPaisyCliente(string codPais, string idUsuario)
    {
        string sql = " SELECT DISTINCT I.IdInstruccion, I.*, C.Nombre, DI.DocumentoNo from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = 0) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1' AND DI.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0','1') AND I.IdEstadoFlujo != '99' AND I.CodPaisHojaRuta = '" + codPais + "' and I.IdCliente in (8000825,8000837,8000882)";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesXPaisUsuarioNI(string codPais)
    {
        string sql = " SELECT DISTINCT I.IdInstruccion, I.*, C.Nombre, DI.DocumentoNo from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = 0) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1' AND DI.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0','1') AND I.IdEstadoFlujo != '99' AND I.CodPaisHojaRuta = '" + codPais + "'";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesXPaisIns(string codPais)
    {
        string sql = " SELECT DISTINCT I.IdInstruccion, I.*, C.Nombre, DI.DocumentoNo from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = 0) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1' AND DI.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0','1') AND I.IdEstadoFlujo != '99' AND I.CodPaisHojaRuta = '" + codPais + "' ";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesXPaisSolicitudPagos(string codPais)
    {
        string sql = " SELECT DISTINCT I.IdInstruccion, I.*, C.Nombre, DI.DocumentoNo from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = 0) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1' AND DI.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0','1') AND I.IdEstadoFlujo != '99' AND I.CodPaisHojaRuta = '" + codPais + "' AND I.CodPaisHojaRuta != 'H' and ISNULL(I.FechaFinalFlujo,I.Fecha) >= '2014-09-01'";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void LoadGastosPaisSinFactura(string codPais)
    {
        string sql = " SELECT DISTINCT TOP 1 I.IdInstruccion, I.*, C.Nombre, DI.DocumentoNo from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = 0) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1' AND DI.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0','1') AND I.IdEstadoFlujo != '99' AND I.CodPaisHojaRuta = '" + codPais + "' AND I.CodPaisHojaRuta != 'H' and ISNULL(I.FechaFinalFlujo,I.Fecha) >= '2014-09-01'";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesXPaisFinalizadas(string codPais)
    {
        string sql = " Select * from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = 1 AND DI.Eliminado = '0') ";
        sql += " WHERE CodEstado = '0' AND IdEstadoFlujo = '99' AND CodPaisHojaRuta = '" + codPais + "' AND I.IdInstruccion NOT IN (SELECT Documento FROM HistorialImpresiones WHERE Eliminado = '0') ";
        sql += "ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesXPaisFinalizadasRemision(string codPais)
    {
        string sql = @" Select  distinct  C.CodigoSAP,C.Nombre  , count(i.Idinstruccion) as Total from Instrucciones I  
                      INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente)  
                      LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = 1 AND DI.Eliminado = '0')  
					   WHERE I.CodEstado IN ('0') AND IdEstadoFlujo = '99' and C.PermitirRemisiones =1 and I.FechaFinalFlujo >=  dateadd(day,-2,GETDATE() ) 
					   and i.CodPaisHojaRuta='" + codPais + "'  group by C.CodigoSAP,C.Nombre  ORDER BY C.Nombre";
        this.loadSQL(sql);
    }

    public void loadInstruccionesFinalizadas()
    {
        string sql = " Select * from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = 1 AND DI.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0') AND IdEstadoFlujo = '99' AND I.IdInstruccion NOT IN (SELECT Documento FROM HistorialImpresiones WHERE Eliminado = '0') ";
        sql += "ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesFinalizadasRemision()
    {
        string sql = " Select * from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = 1 AND DI.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0') AND IdEstadoFlujo = '99' AND I.IdInstruccion NOT IN (SELECT Documento FROM HistorialImpresiones WHERE Eliminado = '0') ";
        sql += "ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesRemision()
    { //Correcta
        string sql = @" Select  distinct C.CodigoSAP,C.Nombre  , count(i.Idinstruccion) as Total from Instrucciones I  
                      INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente)  
                      LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = 1 AND DI.Eliminado = '0')  
					   WHERE I.CodEstado IN ('0') AND IdEstadoFlujo = '99' and C.PermitirRemisiones =1 and I.FechaFinalFlujo >=  dateadd(day,-10,GETDATE() ) 
					  group by C.Nombre,C.CodigoSAP"
                     ;
        this.loadSQL(sql);
    }

    public void loadInstruccionFinalizada(string idInstruccion)
    {
        string sql = " Select * from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = 1 AND DI.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0') AND IdEstadoFlujo = '99' ";
        sql += " AND I.IdInstruccion = '" + idInstruccion + "' ";
        sql += "ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesAll()
    {
        string sql = " Select FC.EstadoFlujo, CL.Nombre, CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'No.Correlativo:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Asignación Canal de Selectividad%' THEN 'Color:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Pago de Impuestos%' THEN 'Total:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Emisión de Pase de Salida%' THEN 'Tramitador:' END END END END Etiqueta1, ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'Aforador:' ELSE '' END Etiqueta2, ";
        sql += " I.*, C1.Descripcion AS Pais, C2.Codigo + ' | ' + C2.Descripcion AS Regimen, I.NumeroFactura from Instrucciones I ";
        sql += " INNER JOIN Clientes CL ON (CL.CodigoSAP = I.IdCliente) ";
        sql += " INNER JOIN FlujoCarga FC ON (FC.IdEstadoFlujo = I.IdEstadoFlujo) ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'Paises') ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) ";
        sql += " WHERE CodEstado = '0' ";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }
    public void loadInstruccionesUNO()
    {
        string sql = " SELECT Convert(varchar,A.Fecha,101) as Fecha ,A.IdInstruccion as Hoja,B.Nombre as Cliente,CodRegimen as CodigoRegimen,C.Serie as Serie,NoCorrelativo as Correlativo";
        sql += " ,Producto as Producto FROM Instrucciones A";
        sql += " Inner Join Clientes B on (A.IdCliente = B.CodigoSAP and B.Eliminado = 0)";
        sql += " Left Join EspeciesFiscalesInstrucciones C on (A.IdInstruccion = C.IdInstruccion and C.CodEspecieFiscal = 'P')";
        sql += " Where IdCliente = '8000006' and A.Fecha >= '2014-09-01' and A.CodEstado != 100 and A.IdEstadoFlujo != '99' and Producto like '%DIESEL%' or";
        sql += " IdCliente = '8000006' and A.Fecha >= '2014-09-01' and A.CodEstado != 100 and A.IdEstadoFlujo != '99' and Producto like '%AV JET%' or";
        sql += " IdCliente = '8000006' and A.Fecha >= '2014-09-01' and A.CodEstado != 100 and A.IdEstadoFlujo != '99' and Producto like '%GASOLINA REGULAR%' or";
        sql += " IdCliente = '8000006' and A.Fecha >= '2014-09-01' and A.CodEstado != 100 and A.IdEstadoFlujo != '99' and Producto like '%BUNKER%' or";
        sql += " IdCliente = '8000006' and A.Fecha >= '2014-09-01' and A.CodEstado != 100 and A.IdEstadoFlujo != '99' and Producto like '%KEROSENE%' order by 1 ";
        this.loadSQL(sql);
    }
    public void loadInstruccionesUNOXPais(string pais)
    {
        string sql = " SELECT Convert(varchar,A.Fecha,101) as Fecha ,A.IdInstruccion as Hoja,B.Nombre as Cliente,CodRegimen as CodigoRegimen,C.Serie as Serie,NoCorrelativo as Correlativo";
        sql += " ,Producto as Producto FROM Instrucciones A";
        sql += " Inner Join Clientes B on (A.IdCliente = B.CodigoSAP and B.Eliminado = 0)";
        sql += " Left Join EspeciesFiscalesInstrucciones C on (A.IdInstruccion = C.IdInstruccion and C.CodEspecieFiscal = 'P' )";
        sql += " Where IdCliente = '8000006' and A.Fecha >= '2014-09-01' and A.CodEstado != 100 and A.IdEstadoFlujo != '99' and Producto like '%DIESEL%' and A.CodPaisHojaRuta = '" + pais + "' or";
        sql += " IdCliente = '8000006' and A.Fecha >= '2014-09-01' and A.CodEstado != 100 and A.IdEstadoFlujo != '99' and Producto like '%AV JET%' and A.CodPaisHojaRuta = '" + pais + "' or";
        sql += " IdCliente = '8000006' and A.Fecha >= '2014-09-01' and A.CodEstado != 100 and A.IdEstadoFlujo != '99' and Producto like '%GASOLINA REGULAR%' and A.CodPaisHojaRuta = '" + pais + "' or";
        sql += " IdCliente = '8000006' and A.Fecha >= '2014-09-01' and A.CodEstado != 100 and A.IdEstadoFlujo != '99' and Producto like '%BUNKER%' and A.CodPaisHojaRuta = '" + pais + "' or";
        sql += " IdCliente = '8000006' and A.Fecha >= '2014-09-01' and A.CodEstado != 100 and A.IdEstadoFlujo != '99' and Producto like '%KEROSENE%' and A.CodPaisHojaRuta = '" + pais + "' order by 1 ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesAllXPaisSVGTNI(string codigoPais)
    {
        string sql = " Select FC.EstadoFlujo, CL.Nombre, CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'No.Correlativo:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Asignación Canal de Selectividad%' THEN 'Color:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Pago de Impuestos%' THEN 'Total:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Emisión de Pase de Salida%' THEN 'Tramitador:' END END END END Etiqueta1, ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'Aforador:' ELSE '' END Etiqueta2, ";
        sql += " I.*, C1.Descripcion AS Pais, C2.Codigo + ' | ' + C2.Descripcion AS Regimen,I.NumeroFactura from Instrucciones I ";
        sql += " INNER JOIN Clientes CL ON (CL.CodigoSAP = I.IdCliente) ";
        sql += " INNER JOIN FlujoCarga FC ON (FC.IdEstadoFlujo = I.IdEstadoFlujo) ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'Paises') ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) ";
        sql += " WHERE CodEstado = '0' AND I.CodPaisHojaRuta = '" + codigoPais + "' ";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesAllXInstruccion(string idInstruccion)
    {
        string sql = " Select CASE WHEN(I.IdEstadoFlujo != '99') THEN FC.EstadoFlujo ELSE 'Flujo Finalizado' END EstadoFlujo, CL.Nombre, ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'No.Correlativo:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Asignación Canal de Selectividad%' THEN 'Color:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Pago de Impuestos%' THEN 'Total:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Emisión de Pase de Salida%' THEN 'Tramitador:' END END END END Etiqueta1, ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'Aforador:' ELSE '' END Etiqueta2, ";
        sql += " I.*, C1.Descripcion AS Pais, C2.Codigo + ' | ' + C2.Descripcion AS Regimen from Instrucciones I ";
        sql += " INNER JOIN Clientes CL ON (CL.CodigoSAP = I.IdCliente) ";
        sql += " LEFT JOIN FlujoCarga FC ON (FC.IdEstadoFlujo = I.IdEstadoFlujo) ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'Paises') ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) ";
        sql += " WHERE CodEstado = '0' AND I.IdInstruccion = '" + idInstruccion + "' ";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesAllXPais(string codPais)
    {
        string sql = " Select FC.EstadoFlujo, CL.Nombre, CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'No.Correlativo:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Asignación Canal de Selectividad%' THEN 'Color:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Pago de Impuestos%' THEN 'Total:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Emisión de Pase de Salida%' THEN 'Tramitador:' END END END END Etiqueta1, ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'Aforador:' ELSE '' END Etiqueta2, ";
        sql += " I.*, C1.Descripcion AS Pais, C2.Codigo + ' | ' + C2.Descripcion AS Regimen from Instrucciones I ";
        sql += " INNER JOIN Clientes CL ON (CL.CodigoSAP = I.IdCliente) ";
        sql += " LEFT JOIN FlujoCarga FC ON (FC.IdEstadoFlujo = I.IdEstadoFlujo) ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'Paises') ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) ";
        sql += " WHERE CodEstado = '0' AND CodPaisHojaRuta = '" + codPais + "' ";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesAllXAduanaUsuario(string codAduana, string idUsuario)
    {
        string sql = " Select FC.EstadoFlujo, CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'No.Correlativo:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Asignación Canal de Selectividad%' THEN 'Color:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Pago de Impuestos%' THEN 'Total:' END END END Etiqueta1, ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'Aforador:' END Etiqueta2, ";
        sql += " I.*, C1.Descripcion AS Pais, C2.Codigo + ' | ' + C2.Descripcion AS Regimen,I.NumeroFactura from Instrucciones I ";
        sql += " INNER JOIN FlujoCarga FC ON (FC.IdEstadoFlujo = I.IdEstadoFlujo) ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'Paises') ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) ";
        sql += " WHERE I.CodEstado IN ('0') AND CodigoAduana = '" + codAduana + "' AND IdUsuarioAduana = '" + idUsuario + "' ";
        sql += " Order By 1 ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesAllXUsuario(string idUsuario)
    {
        string sql = " Select FC.EstadoFlujo, CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'No.Correlativo:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Asignación Canal de Selectividad%' THEN 'Color:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Pago de Impuestos%' THEN 'Total:' END END END Etiqueta1, ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'Aforador:' END Etiqueta2, ";
        sql += " I.*, C1.Descripcion AS Pais, C2.Codigo + ' | ' + C2.Descripcion AS Regimen from Instrucciones I ";
        sql += " INNER JOIN FlujoCarga FC ON (FC.IdEstadoFlujo = I.IdEstadoFlujo) ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'Paises') ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) ";
        sql += " WHERE I.CodEstado IN ('0') AND IdUsuarioAduana = '" + idUsuario + "' ";
        sql += " Order By 1 ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesAllXAduana(string codAduana)
    {
        string sql = " Select FC.EstadoFlujo, CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'No.Correlativo:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Asignación Canal de Selectividad%' THEN 'Color:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Pago de Impuestos%' THEN 'Total:' END END END Etiqueta1, ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'Aforador:' END Etiqueta2, ";
        sql += " I.*, C1.Descripcion AS Pais, C2.Codigo + ' | ' + C2.Descripcion AS Regimen,I.NumeroFactura from Instrucciones I ";
        sql += " INNER JOIN FlujoCarga FC ON (FC.IdEstadoFlujo = I.IdEstadoFlujo) ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'Paises') ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) ";
        sql += " WHERE I.CodEstado IN ('0') AND CodigoAduana = '" + codAduana + "' ";
        sql += " Order By 1 ";
        this.loadSQL(sql);
    }
    public void loadInstruccionesAllXPaisGestion(string codPais)
    {
        string sql = @" Select FC.EstadoFlujo, CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'No.Correlativo:' ELSE 
         CASE WHEN FC.EstadoFlujo LIKE '%Asignación Canal de Selectividad%' THEN 'Color:' ELSE 
         CASE WHEN FC.EstadoFlujo LIKE '%Pago de Impuestos%' THEN 'Total:' END END END Etiqueta1, 
         CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'Aforador:' END Etiqueta2, 
         I.*, C1.Descripcion AS Pais, C2.Codigo + ' | ' + C2.Descripcion AS Regimen,I.NumeroFactura from Instrucciones I 
         INNER JOIN FlujoCarga FC ON (FC.IdEstadoFlujo = I.IdEstadoFlujo) 
         INNER JOIN Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'Paises') 
         INNER JOIN Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) 
         WHERE I.CodEstado IN ('0') AND CodPaisHojaRuta = '" + codPais + "' ";
        sql += " Order By 1 ";
        this.loadSQL(sql);
    }

    public void loadInstruccion(string idInstruccion)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodEstado != '100' AND IdInstruccion = '" + idInstruccion + "' ";
        this.loadSQL(sql);
    }
    public void loadInstruccionXArribo(string idInstruccion)
    {
        string sql = "Select * from Instrucciones ";
        sql += " WHERE IdInstruccion = '" + idInstruccion + "' ";
        this.loadSQL(sql);
    }

    public void loadInstruccionGuia(string idInstruccion)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodEstado = '0' AND IdInstruccion = '" + idInstruccion + "' and CodigoGuia is not NULL";
        this.loadSQL(sql);
    }

    public void loadInstruccionGD(string idInstruccion)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodEstado IN ('0','1') AND IdInstruccion = '" + idInstruccion + "' ";
        this.loadSQL(sql);
    }

    public void loadInstruccionModificar(string idInstruccion)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodEstado IN ('0','1','2') AND IdInstruccion = '" + idInstruccion + "' ";
        this.loadSQL(sql);
    }

    public void loadInstruccionLlenar(string idInstruccion, string estado)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodEstado = '" + estado + "' AND IdInstruccion = '" + idInstruccion + "' ";
        this.loadSQL(sql);
    }

    public void LoadGuias(string idInstruccion)
    {
        string sql = "  Select distinct * from Instrucciones A  Inner Join GestionCarga C on (A.IdInstruccion = C.IdInstruccion)";
        sql += " Inner Join [FlujoCarga] B on (CodEstadoFlujoCarga = 6 and EstadoFLujo = 'Entrega de Servicio' and C.IdEstadoFlujo = B.IdEstadoFlujo) ";
        sql += " Where C.IdInstruccion = '" + idInstruccion + "' and CodigoGuia is not NULL and CodEstado != 100 and CodPaisHojaRuta != 'H'";
        this.loadSQL(sql);
    }

    public void LoadGuiasHN(string idInstruccion)
    {
        string sql = "  Select distinct * from Instrucciones A  Inner Join GestionCarga C on (A.IdInstruccion = C.IdInstruccion)";
        sql += " Inner Join [FlujoCarga] B on (CodEstadoFlujoCarga = 6 and EstadoFLujo = 'Entrega de Servicio' and C.IdEstadoFlujo = B.IdEstadoFlujo) ";
        sql += " Where A.CodigoGuia = '" + idInstruccion + "' and CodigoGuia is not NULL and CodEstado != 100 and CodPaisHojaRuta = 'H'";
        this.loadSQL(sql);
    }

    public void loadInstruccionXAduana(string codAduana)
    {
        string sql = " SELECT DISTINCT I.IdInstruccion, I.*, C.Nombre, DI.DocumentoNo from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = 0) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1' AND DI.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0','1') AND I.CodigoAduana = '" + codAduana + "' AND I.IdEstadoFlujo != '99' ";
        sql += " ORDER BY I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionXAduanaTemp(string codAduana)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodEstado = '0' AND CodigoAduana = '" + codAduana + "' AND IdEstadoFlujo != '99' AND CodEstado = '0' ";
        sql += " and ArriboCarga = 0 ";
        sql += " ORDER BY 3 ";
        this.loadSQL(sql);
    }

    public void loadInstruccionXAduanaFinalizadas(string codAduana)
    {
        string sql = " Select * from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = 1 AND DI.Eliminado = '0') ";
        sql += " WHERE CodEstado = '0' AND CodigoAduana = '" + codAduana + "' AND IdEstadoFlujo = '99' AND CodEstado = '0' AND I.IdInstruccion NOT IN (SELECT Documento FROM HistorialImpresiones WHERE Eliminado = '0') ";
        sql += "ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }
    public void loadInstruccionXAduanaFinalizadasRemision(string codAduana)
    {//Correcta
        string sql = @"	  Select  distinct C.CodigoSAP,C.Nombre  , count(i.Idinstruccion) as Total from Instrucciones I  
                      INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente)  
                      LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = 1 AND DI.Eliminado = '0')  
					   WHERE I.CodEstado IN ('0') AND IdEstadoFlujo = '99' and C.PermitirRemisiones =1 and I.FechaFinalFlujo >=  dateadd(day,-2,GETDATE() )   AND I.IdInstruccion NOT IN (SELECT Documento FROM HistorialImpresiones WHERE Eliminado = '0') 
					   and i.CodigoAduana = '" + codAduana + "' group by C.CodigoSAP,C.Nombre ORDER BY C.Nombre";
        this.loadSQL(sql);
    }

    public void loadInstruccionXAduanaUsuario(string codAduana, string idUsuario)
    {
        string sql = " SELECT DISTINCT I.IdInstruccion, I.*, C.Nombre, DI.DocumentoNo from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = 0) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1' AND DI.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0','1') AND I.CodigoAduana = '" + codAduana + "' AND I.IdUsuarioAduana = '" + idUsuario + "' AND I.IdEstadoFlujo != '99' ";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionXUsuario(string idUsuario)
    {
        string sql = " SELECT DISTINCT I.IdInstruccion, I.*, C.Nombre, DI.DocumentoNo from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = 0) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1' AND DI.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0','1') AND I.IdUsuarioAduana = '" + idUsuario + "' AND I.IdEstadoFlujo != '99' ";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionXAduanaUsuarioTemp(string codAduana, string idUsuario)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodEstado = '0' AND CodigoAduana = '" + codAduana + "' AND IdUsuarioAduana = '" + idUsuario + "' AND IdEstadoFlujo != '99' AND CodEstado = '0' ";
        sql += " and ArriboCarga = 0 ";
        sql += " ORDER BY CodigoAduana, Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionXAduanaUsuarioFinalizadas(string codAduana, string idUsuario)
    {
        string sql = " Select * from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = 1 AND DI.Eliminado = '0') ";
        sql += " WHERE CodEstado = '0' AND CodigoAduana = '" + codAduana + "' AND IdUsuarioAduana = '" + idUsuario + "' AND IdEstadoFlujo = '99' AND I.IdInstruccion NOT IN (SELECT Documento FROM HistorialImpresiones WHERE Eliminado = '0') ";
        sql += "ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }
    public void loadInstruccionXAduanaUsuarioFinalizadasRemision(string codAduana, string idUsuario)
    {
        string sql = @"  Select  distinct C.CodigoSAP,C.Nombre  , count(i.Idinstruccion) as Total from Instrucciones I  
                      INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente)  
                      LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = 1 AND DI.Eliminado = '0')  
					   WHERE I.CodEstado IN ('0') AND IdEstadoFlujo = '99' and C.PermitirRemisiones =1 and I.FechaFinalFlujo >=  dateadd(day,-2,GETDATE() ) 
					   and i.CodigoAduana = '" + codAduana + "' AND IdUsuarioAduana = '" + idUsuario + "' " +
                       "group by C.CodigoSAP,C.Nombre ORDER BY C.Nombre";
        this.loadSQL(sql);
    }


    public void loadIClienteXAduanaUsuarioDetalleRemision(string codAduana, String CodPais, String Cliente)
    {
        string sql = @"  Select  I.IdInstruccion, (AR.Rango + '-' +RI.Serie ) as Serie , I.NumeroFactura, I.Proveedor,I.IdCliente,
                            Case  When   Serie is NULL   then  'N/A'
                            else '1' end Estado  
                            from Instrucciones I  
                            INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente)  
                            LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = 1 AND DI.Eliminado = '0')  
                            Left JOIN RemisionesInstrucciones RI on (I.IdInstruccion =RI.IdInstruccion  and RI.CodEstado!=100)
                            LEFT JOIN AduanasRemision AR on (AR.Item=RI.IdRemision)
                            WHERE I.CodEstado IN ('0') AND IdEstadoFlujo = '99' and C.PermitirRemisiones =1 and I.FechaFinalFlujo >=  dateadd(day,-2,GETDATE() ) AND I.IdInstruccion NOT IN (SELECT Documento FROM HistorialImpresiones WHERE Eliminado = '0') 
                            and i.CodPaisHojaRuta='" + CodPais + "' and CodigoAduana = '" + codAduana + "' and  I.IdCliente='" + Cliente + "' ORDER BY C.Nombre ";
        this.loadSQL(sql);

    }

    public void loadClientePAisDetalleRemision(String CodPais, String Cliente)
    {
        string sql = @"  Select  I.IdInstruccion, (AR.Rango + '-' +RI.Serie ) as Serie , I.NumeroFactura, I.Proveedor,I.IdCliente,
                            Case  When   Serie = NULL   then  'N/A'
                            else '1' end Estado  
                            from Instrucciones I  
                            INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente)  
                            LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = 1 AND DI.Eliminado = '0')  
                            Left JOIN RemisionesInstrucciones RI on (I.IdInstruccion =RI.IdInstruccion  and RI.CodEstado!=100)
                            LEFT JOIN AduanasRemision AR on (AR.Item=RI.IdRemision)
                            WHERE I.CodEstado IN ('0') AND IdEstadoFlujo = '99' and C.PermitirRemisiones =1 and I.FechaFinalFlujo >=  dateadd(day,-2,GETDATE() ) 
                            and i.CodPaisHojaRuta='" + CodPais + "' I.IdCliente='" + Cliente + "' ORDER BY C.Nombre ";
        this.loadSQL(sql);

    }
    //rtorres 15 / Gestor traficco - jordonez-15 jefe de aduanas

    public void loadClienteDetalleRemision(String Cliente)
    {
        string sql = @"  Select  I.IdInstruccion, (AR.Rango + '-' +RI.Serie ) as Serie , I.NumeroFactura, I.Proveedor,I.IdCliente,
                            Case  When   Serie is NULL   then  'N/A'
                            else '1' end Estado  
                            from Instrucciones I  
                            INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente)  
                            LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = 1 AND DI.Eliminado = '0')  
                            Left JOIN RemisionesInstrucciones RI on (I.IdInstruccion =RI.IdInstruccion  and RI.CodEstado!=100)
                            LEFT JOIN AduanasRemision AR on (AR.Item=RI.IdRemision)
                            WHERE I.CodEstado IN ('0') AND IdEstadoFlujo = '99' and C.PermitirRemisiones =1 and I.FechaFinalFlujo >=  dateadd(day,-2,GETDATE() ) 
                            and I.IdCliente='" + Cliente + "' ORDER BY C.Nombre ";
        this.loadSQL(sql);

    }



    public void loadInstruccionXUsuarioFinalizadas(string idUsuario)
    {
        string sql = " Select * from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = 1 AND DI.Eliminado = '0') ";
        sql += " WHERE CodEstado = '0' AND IdUsuarioAduana = '" + idUsuario + "' AND IdEstadoFlujo = '99' AND I.IdInstruccion NOT IN (SELECT Documento FROM HistorialImpresiones WHERE Eliminado = '0') ";
        sql += "ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionXTramite(string idTramite)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodEstado = '0' AND IdTramite = '" + idTramite + "' ";
        this.loadSQL(sql);
    }

    public void loadMaxInstruccionXAduana(string codAduana)
    {
        string s = " select Top 1 MaxIdInstruccion=substring(IdInstruccion,charindex('-',IdInstruccion,charindex('-',IdInstruccion)+1)+1 , len(IdInstruccion)) + 1 from Instrucciones ";
        s = s + " where CodigoAduana = '" + codAduana + "' order by substring(IdInstruccion,charindex('-',IdInstruccion,charindex('-',IdInstruccion)+1)+1 , len(IdInstruccion)) + 1  desc";
        loadSQL(s);
    }

    public void loadMaxInstruccionXAduanaR(string codAduana)
    {
        string s = " select Top 1 MaxIdInstruccion=substring(IdInstruccion,charindex('-',IdInstruccion)+1 , len(IdInstruccion))  from Instrucciones ";
        s = s + " where CodigoAduana = '" + codAduana + "' order by substring(IdInstruccion,charindex('-',IdInstruccion)+1 , len(IdInstruccion))  desc";
        loadSQL(s);
    }

    public void loadMaxInstruccionXAduanaCompleta(string codAduana)
    {
        string s = @" 
                SELECT top 1
                   MaxIdInstruccion = 
                   CASE 
                      WHEN substring(IdInstruccion,1,2)='Hr' THEN 

                       substring(IdInstruccion,charindex('-',IdInstruccion)+1 , len(IdInstruccion))+1
    
	                 else
	                 substring(IdInstruccion,charindex('-',IdInstruccion,charindex('-',IdInstruccion)+1)+1 , 
		                 len(IdInstruccion)) + 1  
                   END 
                 FROM Instrucciones   Fechas  where CodigoAduana ='" + codAduana + @" ' order by    CASE 
                      WHEN substring(IdInstruccion,1,2)='Hr' THEN 

                       substring(IdInstruccion,charindex('-',IdInstruccion)+1 , len(IdInstruccion)+1)+1
    
	                else

	                 substring(IdInstruccion,charindex('-',IdInstruccion,charindex('-',IdInstruccion)+1)+1 , 
		                 len(IdInstruccion)) + 1  
                   END desc";

        loadSQL(s);

    }


    public void loadNuevoId(string codSF)
    {
        string sql = " declare @Id varchar(50) ";
        sql += " select @Id = (SELECT NuevoId = MAX(CAST(LEFT(IdInstruccion,4)AS varchar(5))) + ";
        sql += " RIGHT('' + CAST(MAX(CAST(RIGHT(IdInstruccion,LEN(IdInstruccion)-4)+ 1 AS int))AS varchar(50)),50) ";
        sql += " FROM Instrucciones ";
        sql += " Where IdInstruccion like '" + codSF + "%') ";
        sql += " select @Id as NuevoId ";
        this.loadSQL(sql);
    }

    public void ejecutarSPDatosInstruccion(string idInstruccion)
    {
        string sql = " exec spDatosInstruccion '" + idInstruccion + "' ";
        this.loadSQL(sql);
    }

    public void ejecutarSPDatosInstruccionRemision(string idInstruccion)
    {
        string sql = " exec spDatosInstruccionRemision '" + idInstruccion + "' ";
        this.loadSQL(sql);
    }

    public void loadInstruccionBusquedaAvanzada(string campos, string criterio, string opcion, string codPais, string codAduana)
    {
        string sql = " SELECT DISTINCT I.IdInstruccion, I.Impuesto,I.ISV,I.DAI,I.Selectivo,I.STD,I.ProduccionConsumo,I.OtrosImpuestos" + campos;
        sql += " FROM Instrucciones I ";
        sql += " INNER JOIN Clientes CL ON (CL.CodigoSAP = I.IdCliente AND CL.Eliminado = '0') ";
        sql += " INNER JOIN Aduanas A ON (A.CodigoAduana = I.CodigoAduana AND A.CodEstado = '0') ";
        sql += " LEFT JOIN DocumentosInstrucciones DI1 ON (DI1.IdInstruccion = I.IdInstruccion AND DI1.CodDocumento = '1' AND DI1.Eliminado ='0') ";
        sql += " LEFT JOIN DocumentosInstrucciones DI2 ON (DI2.IdInstruccion = I.IdInstruccion AND DI2.CodDocumento = I.CodDocumentoEmbarque AND DI2.Eliminado ='0') ";
        sql += " LEFT JOIN EspeciesFiscalesInstrucciones EFI ON (EFI.IdInstruccion = I.IdInstruccion AND (EFI.CodEspecieFiscal = 'F' OR EFI.CodEspecieFiscal = 'P') AND EFI.Eliminado ='0') ";
        sql += " INNER JOIN Codigos C ON (C.Codigo = I.CodPaisHojaRuta AND C.Categoria = 'PAISES') ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = I.CodRegimen AND C1.Categoria = 'REGIMEN' + C.Descripcion) ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = I.CodDocumentoEmbarque AND C2.Categoria = 'DOCUMENTOEMBARQUE') ";
        sql += " INNER JOIN Codigos C3 ON (C3.Codigo = I.CodPaisOrigen AND C3.Categoria = 'PAISES') ";
        sql += " INNER JOIN Codigos C4 ON (C4.Codigo = I.CodPaisDestino AND C4.Categoria = 'PAISES') ";
        //sql += " LEFT JOIN Codigos C6 ON (C6.Codigo = I.CodTipoCargamento AND C6.Categoria = 'TIPOCARGAMENTO') ";
        sql += " LEFT JOIN Codigos C7 ON (C7.Codigo = I.Division AND C7.Categoria = I.IdCliente)";
        sql += " LEFT JOIN Eventos E ON (E.IdInstruccion = I.IdInstruccion AND E.IdEvento = '1') ";
        //sql += " LEFT JOIN Eventos E2 ON (E2.IdInstruccion = I.IdInstruccion AND E2.IdEvento = '24') ";
        //sql += " LEFT JOIN Eventos E3 ON (E3.IdInstruccion = I.IdInstruccion AND E3.IdEvento = '25')  ";
        //sql += " LEFT JOIN Eventos E4 ON (E4.IdInstruccion = I.IdInstruccion AND E4.IdEvento = '26')  ";
        sql += " LEFT JOIN Eventos E5 ON (E5.IdInstruccion = I.IdInstruccion AND E5.IdEvento = '10')  ";
        //sql += " LEFT JOIN Eventos E6 ON (E6.IdInstruccion = I.IdInstruccion AND E6.IdEvento = '27')  ";
        sql += " LEFT JOIN Eventos E7 ON (E7.IdInstruccion = I.IdInstruccion AND E7.IdEvento = '41')  ";
        sql += " LEFT JOIN Eventos E8 ON (E8.IdInstruccion = I.IdInstruccion AND E8.IdEvento = '42')  ";
        sql += " LEFT JOIN Eventos E9 ON (E9.IdInstruccion = I.IdInstruccion AND E9.IdEvento = '33')  ";
        sql += " LEFT JOIN Eventos E10 ON (E10.IdInstruccion = I.IdInstruccion AND E10.IdEvento = '35')  ";
        sql += " LEFT JOIN Eventos E11 ON (E11.IdInstruccion = I.IdInstruccion AND E11.IdEvento = '37')  ";
        sql += " LEFT JOIN Eventos E12 ON (E12.IdInstruccion = I.IdInstruccion AND E12.IdEvento = '52') ";
        sql += " LEFT JOIN Eventos E13 ON (E13.IdInstruccion = I.IdInstruccion AND E13.IdEvento = '53') ";

        sql += " LEFT JOIN FlujoCarga FC2 ON (FC2.IdEstadoFlujo = I.IdEstadoFlujo AND FC2.Eliminado = '0') ";
        sql += " LEFT JOIN GestionCarga GC1 ON (GC1.IdInstruccion = I.IdInstruccion AND GC1.Estado = 0 and GC1.Eliminado = 0)";
        sql += " LEFT JOIN GestionCarga GC3 ON (GC3.IdInstruccion = I.IdInstruccion AND GC3.Estado = 1 and GC3.Eliminado = 0)";
        sql += " LEFT JOIN GestionCarga GC4 ON (GC4.IdInstruccion = I.IdInstruccion AND GC4.Estado = 2 and GC4.Eliminado = 0)";
        sql += " LEFT JOIN GestionCarga GC5 ON (GC5.IdInstruccion = I.IdInstruccion AND GC5.Estado = 3 and GC5.Eliminado = 0)";
        sql += " LEFT JOIN GestionCarga GC6 ON (GC6.IdInstruccion = I.IdInstruccion AND GC6.Estado = 4 and GC6.Eliminado = 0)";
        sql += " LEFT JOIN GestionCarga GC7 ON (GC7.IdInstruccion = I.IdInstruccion AND GC7.Estado = 5 and GC7.Eliminado = 0)";
        sql += " LEFT JOIN GestionCarga GC ON (GC.IdInstruccion = I.IdInstruccion AND GC.Estado = 6 and GC.Eliminado = 0)";

        /*Cargas Automaticas*/
        sql += @"  left JOIN [AduanasNueva].[dbo].[TiemposFlujoCarga] GC11 ON (GC11.IdInstruccion = I.IdInstruccion AND GC11.IDEstado = '0' and GC1.Eliminado = 0) /*Comienzo de Flujo*/
          LEFT JOIN [TiemposFlujoCarga] GC22 ON (GC22.IdInstruccion = I.IdInstruccion AND GC22.IDEstado = 12 and GC22.Eliminado = 0) /*Validacion Electronica*/
          LEFT JOIN [TiemposFlujoCarga] GC33 ON (GC33.IdInstruccion = I.IdInstruccion AND GC33.IDEstado = 1 and GC33.Eliminado = 0) /*Confirmación Pago de Impuestos*/
          LEFT JOIN [TiemposFlujoCarga] GC44 ON (GC44.IdInstruccion = I.IdInstruccion AND GC44.IDEstado = 2 and GC44.Eliminado = 0) /*Canal de Selectividad*/
          LEFT JOIN [TiemposFlujoCarga] GC55 ON (GC55.IdInstruccion = I.IdInstruccion AND GC55.IDEstado = 4 and GC55.Eliminado = 0) /*Revicion de Mercaderia*/
          LEFT JOIN [TiemposFlujoCarga] GC66 ON (GC66.IdInstruccion = I.IdInstruccion AND GC66.IDEstado = 7 and GC66.Eliminado = 0) /*Emision Pase de Salida*/
          LEFT JOIN [TiemposFlujoCarga] GC77 ON (GC77.IdInstruccion = I.IdInstruccion AND GC77.IDEstado = 9 and GC77.Eliminado = 0) /*Entrega de Servicio*/
          LEFT JOIN [TiemposFlujoCarga] GC88 ON (GC88.IdInstruccion = I.IdInstruccion AND GC88.IDEstado = 14 and GC88.Eliminado = 0) /*Arribo de Carga Aduanas*/
LEFT JOIN ProcesoFlujoCarga PF  on  PF.IdInstruccion=I.IdInstruccion
		   ";


        sql += " LEFT JOIN DatosGestionDocumentos DGC ON (DGC.IdInstruccion = I.IdInstruccion AND DGC.Eliminado = '0') ";
        sql += " INNER JOIN Usuarios U1 ON (U1.IdUsuario = I.IdUsuarioAduana) ";
        sql += " LEFT JOIN Usuarios U2 ON (U2.IdUsuario = I.Aforador) ";
        sql += " LEFT JOIN Usuarios U3 ON (U3.IdUsuario = I.Tramitador) ";
        sql += " LEFT JOIN Usuarios U4 ON (U4.IdUsuario = I.IdUsuario) ";
        sql += " LEFT JOIN PermisosInstrucciones PI ON (PI.IdInstruccion= I.IdInstruccion and PI.Eliminado = 0) ";
        sql += " LEFT JOIN EspeciesFiscalesInstrucciones EF ON (EF.IdInstruccion = I.IdInstruccion AND EF.CodEspecieFiscal in('HN-MI','HN-MN','SS')AND EF.Necesario = 'X')";
        sql += " LEFT JOIN Codigos C5 on (EF.CodEspecieFiscal = C5.Codigo and C5.Categoria = 'ESPECIESFISCALES')";
        sql += " LEFT JOIN[EsquemaTramites2].[dbo].[BoletinPago] bp ON(I.IdInstruccion = bp.IdInstruccion collate Modern_Spanish_CI_AI  AND bp.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0','1') AND " + criterio;
        if (opcion == "Por Pais")
            sql += " AND I.CodPaisHojaRuta = '" + codPais + "' ";
        else if (opcion == "Por Aduana")
            sql += " AND I.CodPaisHojaRuta = '" + codPais + "' AND I.CodigoAduana = '" + codAduana + "' ";
        this.loadSQL(sql);
    }

    public void loadDiarioAseguramiento(string codPais)
    {
        string sql = @" SELECT DISTINCT I.IdInstruccion,
                        A.NombreAduana AS Aduana,
                        CL.Nombre as Cliente,
                        I.Producto,
                        E.FechaInicio AS ArribodeCarga,
                        GC3.Fecha AS ValidacionElectonica,
                        GC4.Fecha AS PagodeImpuestos,
                        I.Color,GC.Fecha AS EntregadeServicio,
                        UPPER(C1.Descripcion) AS Regimen,
                        I.CiudadDestino
                        FROM Instrucciones I 
                        INNER JOIN Clientes CL ON (CL.CodigoSAP = I.IdCliente AND CL.Eliminado = '0') 
                        INNER JOIN Aduanas A ON (A.CodigoAduana = I.CodigoAduana AND A.CodEstado = '0')
                        LEFT JOIN DocumentosInstrucciones DI1 ON (DI1.IdInstruccion = I.IdInstruccion 
                        AND DI1.CodDocumento = '1' AND DI1.Eliminado ='0') 

                        LEFT JOIN EspeciesFiscalesInstrucciones EFI ON (EFI.IdInstruccion = I.IdInstruccion AND (EFI.CodEspecieFiscal = 'F' OR EFI.CodEspecieFiscal = 'P') 
                        AND EFI.Eliminado ='0')  INNER JOIN Codigos C ON (C.Codigo = I.CodPaisHojaRuta AND C.Categoria = 'PAISES') 
                        INNER JOIN Codigos C1 ON (C1.Codigo = I.CodRegimen AND C1.Categoria = 'REGIMEN' + C.Descripcion) 
	
                        LEFT JOIN Eventos E ON (E.IdInstruccion = I.IdInstruccion AND E.IdEvento = '1') 

                        LEFT JOIN FlujoCarga FC2 ON (FC2.IdEstadoFlujo = I.IdEstadoFlujo AND FC2.Eliminado = '0')
					  
                        LEFT JOIN GestionCarga GC3 ON (GC3.IdInstruccion = I.IdInstruccion AND GC3.Estado = 1 and GC3.Eliminado = 0) 
                        LEFT JOIN GestionCarga GC4 ON (GC4.IdInstruccion = I.IdInstruccion AND GC4.Estado = 2 and GC4.Eliminado = 0) 
						
                        LEFT JOIN GestionCarga GC ON (GC.IdInstruccion = I.IdInstruccion AND GC.Estado = 6 and GC.Eliminado = 0) 
                        LEFT JOIN DatosGestionDocumentos DGC ON (DGC.IdInstruccion = I.IdInstruccion AND DGC.Eliminado = '0')  
					 
                        LEFT JOIN EspeciesFiscalesInstrucciones EF ON (EF.IdInstruccion = I.IdInstruccion AND EF.CodEspecieFiscal in('HN-MI','HN-MN','SS')AND EF.Necesario = 'X')
                        LEFT JOIN Codigos C5 on (EF.CodEspecieFiscal = C5.Codigo and C5.Categoria = 'ESPECIESFISCALES')
							   
                        WHERE I.CodEstado IN ('0','1') AND I.CodPaisHojaRuta like '%" + codPais + @"%' and GC.Fecha is null and UPPER(C1.Descripcion)  not in  ('GASTOS ADMINISTRATIVOS','CROSSBORDER')
                        and  A.NombreAduana not in ('ALCONEX')
                        ";

        this.loadSQL(sql);
    }


    //reporte embarque con motorista
    public void loadInstruccionEmbarqueDiarioViejo(string fechaInicial, string fechaFinal, string codCliente, string codPais)
    {
        string sql = " SELECT I.CodigoAduana, I.IdInstruccion, I.ReferenciaCliente, A.NombreAduana, ";
        sql += " CASE WHEN I.CodPaisHojaRuta != 'S' THEN I.NoCorrelativo + '/' + EFI.Serie ELSE I.NoCorrelativo END Correlativo, ";
        sql += " I.Producto, C2.Descripcion AS Regimen, DI.DocumentoNo AS NumFactura, CONVERT(Varchar,E.FechaInicio,101) FechaEntrada, ";
        sql += " CONVERT(Varchar,E.FechaInicio,108) HoraEntrada, C3.Descripcion AS Procedencia, I.Proveedor, CiudadDestino, ";
        //sql += " DGD.Motorista,  ";
        sql += " (SELECT Motorista FROM DatosGestionDocumentos WHERE IdInstruccion = I.IdInstruccion) AS Motorista, ";
        //sql += " '' AS Motorista, ";
        sql += " I.Impuesto, CONVERT(Varchar,GC.Fecha,108) HoraSalida, ";
        sql += " CASE WHEN I.Impuesto > 0 THEN 'IMPUESTO' ELSE 'LIBRE DE IMPUESTO' END Observaciones, ";
        sql += " C.Nombre FROM Instrucciones I ";
        sql += " INNER JOIN Aduanas A ON (A.CodigoAduana = I.CodigoAduana) ";
        if (codPais == "H")
            sql += " INNER JOIN EspeciesFiscalesInstrucciones EFI ON (EFI.IdInstruccion = I.IdInstruccion AND CodEspecieFiscal IN ('P','F')) ";
        else
            sql += " LEFT JOIN EspeciesFiscalesInstrucciones EFI ON (EFI.IdInstruccion = I.IdInstruccion AND CodEspecieFiscal IN ('P','F')) ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'PAISES') ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) ";
        sql += " INNER JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1') ";
        sql += " INNER JOIN Eventos E ON (E.IdInstruccion = I.IdInstruccion AND E.IdEvento = '1') ";
        sql += " INNER JOIN Codigos C3 ON (C3.Codigo = I.CodPaisProcedencia AND C3.Categoria = 'PAISES') ";
        //sql += " LEFT JOIN DatosGestionDocumentos DGD ON (DGD.IdInstruccion = I.IdInstruccion) ";
        sql += " INNER JOIN FlujoCarga FC ON (FC.CodRegimen = I.CodRegimen AND FC.CodEstadoFlujoCarga = '6' AND FC.CodPais = I.CodPaisHojaRuta) ";
        sql += " INNER JOIN GestionCarga GC ON (GC.IdInstruccion = I.IdInstruccion AND GC.IdEstadoFlujo = FC.IdEstadoFlujo) ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        //sql += " WHERE I.IdCliente = '" + codCliente + "' AND I.CodEstado = '0' AND CONVERT(Varchar,GC.Fecha,111) BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + "' ";
        sql += " WHERE I.IdCliente = '" + codCliente + "' AND I.CodEstado = '0' AND CONVERT(Varchar,E.FechaInicio,111) BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + "' ";
        sql += " ORDER BY FechaEntrada, HoraEntrada ";
        this.loadSQL(sql);
    }

    public void loadInstruccionEmbarqueDiario(string fechaInicial, string fechaFinal, string codCliente, string codPais)
    {
        string sql = @"  SELECT I.CodigoAduana, 
 I.IdInstruccion,
  I.ReferenciaCliente,
   A.NombreAduana,
   CASE WHEN I.CodPaisHojaRuta != 'S' THEN I.NoCorrelativo + '/' + EFI.Serie ELSE I.NoCorrelativo END Correlativo,  I.Producto, C2.Descripcion AS Regimen, DI.DocumentoNo AS NumFactura,
    CONVERT(Varchar,E.FechaInicio,101) FechaEntrada, 
	 CONVERT(Varchar,E.FechaInicio,108) HoraEntrada, C3.Descripcion AS Procedencia, I.Proveedor, CiudadDestino,
	  (SELECT Motorista FROM DatosGestionDocumentos WHERE IdInstruccion = I.IdInstruccion) AS Motorista, 
	   I.Impuesto, ISNULL (CONVERT(Varchar,GC.Fecha,108) ,CONVERT(Varchar,GC1.Fecha,108) ) HoraSalida,  CASE WHEN I.Impuesto > 0 THEN 'IMPUESTO' ELSE 'LIBRE DE IMPUESTO' END Observaciones,  C.Nombre FROM Instrucciones I 
			INNER JOIN Aduanas A ON (A.CodigoAduana = I.CodigoAduana) 
			LEFT JOIN EspeciesFiscalesInstrucciones EFI ON (EFI.IdInstruccion = I.IdInstruccion AND CodEspecieFiscal IN ('P','F')) 
			INNER JOIN Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'PAISES') 
			INNER JOIN Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion)  
			INNER JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1')  
			INNER JOIN Eventos E ON (E.IdInstruccion = I.IdInstruccion AND E.IdEvento = '1')  
			INNER JOIN Codigos C3 ON (C3.Codigo = I.CodPaisProcedencia AND C3.Categoria = 'PAISES') 
			left JOIN FlujoCarga FC ON (FC.CodRegimen = I.CodRegimen AND FC.CodEstadoFlujoCarga = '6' AND FC.CodPais = I.CodPaisHojaRuta) 
			left JOIN GestionCarga GC ON (GC.IdInstruccion = I.IdInstruccion AND GC.IdEstadoFlujo = FC.IdEstadoFlujo)  
			INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente) 
			left JOIN ProcesoFlujoCarga FC1 ON (FC1.IdInstruccion = I.IdInstruccion and FC1.CodEstado='9' ) 
			left JOIN TiemposFlujoCarga GC1 ON (GC1.IdInstruccion = I.IdInstruccion  and GC1.IdEstado='9'  ) ";
        sql += " WHERE I.IdCliente = '" + codCliente + "' AND I.CodEstado = '0' AND CONVERT(Varchar,E.FechaInicio,111) BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + "' ";
        sql += " ORDER BY FechaEntrada, HoraEntrada ";
        this.loadSQL(sql);
    }

    //reporte embarque sin motorista
    public void loadInstruccionEmbarqueDiarioSM(string fechaInicial, string fechaFinal, string codCliente, string codPais)
    {
        string sql = " SELECT I.CodigoAduana, I.IdInstruccion, I.ReferenciaCliente, A.NombreAduana, ";
        sql += " CASE WHEN I.CodPaisHojaRuta != 'S' THEN I.NoCorrelativo + '/' + EFI.Serie ELSE I.NoCorrelativo END Correlativo, ";
        sql += " I.Producto, C2.Descripcion AS Regimen, DI.DocumentoNo AS NumFactura, CONVERT(Varchar,E.FechaInicio,101) FechaEntrada, ";
        sql += " CONVERT(Varchar,E.FechaInicio,108) HoraEntrada, C3.Descripcion AS Procedencia, I.Proveedor, CiudadDestino, ";
        //sql += " DGD.Motorista,  ";
        //sql += " (SELECT Motorista FROM DatosGestionDocumentos WHERE IdInstruccion = I.IdInstruccion) AS Motorista, ";
        sql += " '' AS Motorista, ";
        sql += " I.Impuesto, CONVERT(Varchar,GC.Fecha,108) HoraSalida, ";
        sql += " CASE WHEN I.Impuesto > 0 THEN 'IMPUESTO' ELSE 'LIBRE DE IMPUESTO' END Observaciones, ";
        sql += " C.Nombre FROM Instrucciones I ";
        sql += " INNER JOIN Aduanas A ON (A.CodigoAduana = I.CodigoAduana) ";
        if (codPais == "H")
            sql += " INNER JOIN EspeciesFiscalesInstrucciones EFI ON (EFI.IdInstruccion = I.IdInstruccion AND CodEspecieFiscal IN ('P','F')) ";
        else
            sql += " LEFT JOIN EspeciesFiscalesInstrucciones EFI ON (EFI.IdInstruccion = I.IdInstruccion AND CodEspecieFiscal IN ('P','F')) ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'PAISES') ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) ";
        sql += " INNER JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1') ";
        sql += " INNER JOIN Eventos E ON (E.IdInstruccion = I.IdInstruccion AND E.IdEvento = '1') ";
        sql += " INNER JOIN Codigos C3 ON (C3.Codigo = I.CodPaisProcedencia AND C3.Categoria = 'PAISES') ";
        //sql += " LEFT JOIN DatosGestionDocumentos DGD ON (DGD.IdInstruccion = I.IdInstruccion) ";
        sql += " INNER JOIN FlujoCarga FC ON (FC.CodRegimen = I.CodRegimen AND FC.CodEstadoFlujoCarga = '6' AND FC.CodPais = I.CodPaisHojaRuta) ";
        sql += " INNER JOIN GestionCarga GC ON (GC.IdInstruccion = I.IdInstruccion AND GC.IdEstadoFlujo = FC.IdEstadoFlujo) ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        //sql += " WHERE I.IdCliente = '" + codCliente + "' AND I.CodEstado = '0' AND CONVERT(Varchar,GC.Fecha,111) BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + "' ";
        sql += " WHERE I.IdCliente = '" + codCliente + "' AND I.CodEstado = '0' AND CONVERT(Varchar,E.FechaInicio,111) BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + "' ";
        sql += " ORDER BY FechaEntrada, HoraEntrada ";
        this.loadSQL(sql);
    }

    //reporte embarque de Vista com motorista
    public void loadInstruccionesEmbarqueVista(string fechaInicial, string fechaFinal, string codCliente, string pais)
    {
        string sql = " SELECT DISTINCT [IdInstruccion],[CodigoAduana],[ReferenciaCliente],CodRegimen,[NombreAduana],[Motorista],[IdCliente], ";
        sql += " [CodEstado],[FechaInicio],[Descripcion] AS Regimen,[Correlativo],[Producto],[DocumentoNo] AS NumFactura,FechaEntrada, ";
        sql += " HoraEntrada,Procedencia,Proveedor,CiudadDestino,Impuesto,HoraSalida,Observaciones,Nombre ";
        sql += " FROM [AduanasNueva].[dbo].[viewReporteEmbarque] ";
        sql += " WHERE IdCliente = '" + codCliente + "' AND CodEstado = '0' AND CONVERT(Varchar,FechaInicio,111) BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + "' ";
        sql += " AND Categoria like '%" + pais + "' ";
        sql += " ORDER BY FechaEntrada, HoraEntrada ";
        this.loadSQL(sql);
    }

    public void loadInstruccionGestioonCarga(string fechaInicial, string fechaFinal, string codCliente, string Pais)
    {
        #region Malo
        //string sql = " SELECT DISTINCT(I.IdInstruccion), CL.Nombre, I.Producto, DI.DocumentoNo, GC1.Fecha AS ComienzoFlujo, EV.FechaInicio AS ArriboCarga, GC2.Fecha AS ValidaciónElectrónica, ";
        //sql += " GC3.Fecha AS PagoImpuestos, GC4.Fecha AS AsignaciónCanalSelectividad, I.Color, GC5.Fecha AS RevisiónMercancía, ";
        //sql += " GC6.Fecha AS EmisiónPaseSalida, GC7.Fecha AS EntregaServicio,I.CodRegimen AS Regime, EFI.Serie AS Serie,DI2.DocumentoNo AS BL, I.ReferenciaCliente as ReferenciaCliente, I.CiudadDestino as CiudadDestino FROM [AduanasNueva].[dbo].[Instrucciones] I ";
        //sql += " INNER JOIN [AduanasNueva].[dbo].[Clientes] CL ON (CL.CodigoSAP = I.IdCliente) ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[DocumentosInstrucciones] DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1' AND DI.Eliminado = 0) ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[DocumentosInstrucciones] DI2 ON (DI2.IdInstruccion = I.IdInstruccion AND DI2.CodDocumento in('50','51','52') AND DI2.Eliminado = 0) ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[Eventos] EV ON (EV.IdInstruccion = I.IdInstruccion AND EV.IdEvento = '1') ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC1 ON (GC1.IdInstruccion = I.IdInstruccion AND GC1.IdEstadoFlujo = '0') ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[FlujoCarga] FC2 ON (FC2.CodRegimen = I.CodRegimen AND FC2.CodPais = I.CodPaisHojaRuta AND FC2.Eliminado = '0' AND FC2.CodEstadoFlujoCarga = '1') ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC2 ON (GC2.IdInstruccion = I.IdInstruccion AND GC2.IdEstadoFlujo = FC2.IdEstadoFlujo) ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[FlujoCarga] FC3 ON (FC3.CodRegimen = I.CodRegimen AND FC3.CodPais = I.CodPaisHojaRuta AND FC3.Eliminado = '0' AND FC3.CodEstadoFlujoCarga = '2') ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC3 ON (GC3.IdInstruccion = I.IdInstruccion AND GC3.IdEstadoFlujo = FC3.IdEstadoFlujo) ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[FlujoCarga] FC4 ON (FC4.CodRegimen = I.CodRegimen AND FC4.CodPais = I.CodPaisHojaRuta AND FC4.Eliminado = '0' AND FC4.CodEstadoFlujoCarga = '3') ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC4 ON (GC4.IdInstruccion = I.IdInstruccion AND GC4.IdEstadoFlujo = FC4.IdEstadoFlujo) ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[FlujoCarga] FC5 ON (FC5.CodRegimen = I.CodRegimen AND FC5.CodPais = I.CodPaisHojaRuta AND FC5.Eliminado = '0' AND FC5.CodEstadoFlujoCarga = '4') ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC5 ON (GC5.IdInstruccion = I.IdInstruccion AND GC5.IdEstadoFlujo = FC5.IdEstadoFlujo) ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[FlujoCarga] FC6 ON (FC6.CodRegimen = I.CodRegimen AND FC6.CodPais = I.CodPaisHojaRuta AND FC6.Eliminado = '0' AND FC6.CodEstadoFlujoCarga = '5') ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC6 ON (GC6.IdInstruccion = I.IdInstruccion AND GC6.IdEstadoFlujo = FC6.IdEstadoFlujo) ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[FlujoCarga] FC7 ON (FC7.CodRegimen = I.CodRegimen AND FC7.CodPais = I.CodPaisHojaRuta AND FC7.Eliminado = '0' AND FC7.CodEstadoFlujoCarga = '6') ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC7 ON (GC7.IdInstruccion = I.IdInstruccion AND GC7.IdEstadoFlujo = FC7.IdEstadoFlujo) ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[EspeciesFiscalesInstrucciones] EFI ON (EFI.IdInstruccion = I.IdInstruccion AND EFI.Eliminado = 0 AND EFI.CodEspecieFiscal = 'P')";
        //sql += " WHERE I.IdCliente = '" + codCliente + "' AND I.IdEstadoFlujo != '0' AND CONVERT(Varchar,GC1.Fecha,111) BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + "' ";
        //this.loadSQL(sql);
        #endregion
        string sql = " SELECT DISTINCT(I.IdInstruccion), CL.Nombre, I.Producto, DI.DocumentoNo, GC1.Fecha AS ComienzoFlujo, EV.FechaInicio AS ArriboCarga, GC2.Fecha AS ValidaciónElectrónica, ";
        sql += " GC3.Fecha AS PagoImpuestos, GC4.Fecha AS AsignaciónCanalSelectividad, I.Color, GC5.Fecha AS RevisiónMercancía, ";
        sql += " GC6.Fecha AS EmisiónPaseSalida, GC7.Fecha AS EntregaServicio,I.CodRegimen AS Regime, EFI.Serie AS Serie,DI2.DocumentoNo AS BL, I.ReferenciaCliente as ReferenciaCliente, I.CiudadDestino as CiudadDestino FROM [AduanasNueva].[dbo].[Instrucciones] I ";
        sql += " INNER JOIN [AduanasNueva].[dbo].[Clientes] CL ON (CL.CodigoSAP = I.IdCliente) ";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[DocumentosInstrucciones] DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1' AND DI.Eliminado = 0) ";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[DocumentosInstrucciones] DI2 ON (DI2.IdInstruccion = I.IdInstruccion AND DI2.CodDocumento in('50','51','52') AND DI2.Eliminado = 0) ";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[Eventos] EV ON (EV.IdInstruccion = I.IdInstruccion AND EV.IdEvento = '1') ";
        sql += " INNER JOIN [AduanasNueva].[dbo].[GestionCarga] GC1 ON (GC1.IdInstruccion = I.IdInstruccion AND GC1.Estado = '0' and GC1.Eliminado = 0)";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC2 ON (GC2.IdInstruccion = I.IdInstruccion AND GC2.Estado = 1 and GC2.Eliminado = 0)";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC3 ON (GC3.IdInstruccion = I.IdInstruccion AND GC3.Estado = 2 and GC3.Eliminado = 0)";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC4 ON (GC4.IdInstruccion = I.IdInstruccion AND GC4.Estado = 3 and GC4.Eliminado = 0)";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC5 ON (GC5.IdInstruccion = I.IdInstruccion AND GC5.Estado = 4 and GC5.Eliminado = 0)";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC6 ON (GC6.IdInstruccion = I.IdInstruccion AND GC6.Estado = 5 and GC6.Eliminado = 0)";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC7 ON (GC7.IdInstruccion = I.IdInstruccion AND GC7.Estado = 6 and GC7.Eliminado = 0)";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[EspeciesFiscalesInstrucciones] EFI ON (EFI.IdInstruccion = I.IdInstruccion AND EFI.Eliminado = 0 AND EFI.CodEspecieFiscal = 'P')";
        sql += " WHERE I.IdCliente = '" + codCliente + "' AND I.CodPaisHojaRuta = '" + Pais + "' AND I.IdEstadoFlujo != '0' AND CONVERT(Varchar,GC1.Fecha,111) BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + "' ";
        this.loadSQL(sql);
    }


    public void loadInstruccionGestioonCargaAutomaticas(string fechaInicial, string fechaFinal, string codCliente, string Pais)
    {

        string sql = @"  SELECT DISTINCT(I.IdInstruccion), CL.Nombre, I.Producto, DI.DocumentoNo, 
        ISNULL(GC1.Fecha,GC11.Fecha) AS ComienzoFlujo,/**/
	    ISNULL(EV.FechaInicio,GC8.Fecha) AS ArriboCarga, 
		ISNULL(GC2.Fecha,GC22.Fecha) AS ValidaciónElectrónica,  
        ISNULL(  GC3.Fecha,GC33.Fecha) AS PagoImpuestos,  
		isnull(GC4.Fecha,GC44.Fecha) AS AsignaciónCanalSelectividad, 
		IsNULL (I.Color,PFC.CanalSelectividad) as Color,
		 isNULL (GC5.Fecha,GC55.Fecha) AS RevisiónMercancía,  
         isnull( GC6.Fecha,  GC66.Fecha) AS EmisiónPaseSalida,
		 isnull( GC7.Fecha, GC77.Fecha) AS EntregaServicio,
		 I.CodRegimen AS Regime, EFI.Serie AS Serie,DI2.DocumentoNo AS BL, I.ReferenciaCliente as ReferenciaCliente, I.CiudadDestino as CiudadDestino
		  FROM [AduanasNueva].[dbo].[Instrucciones] I  
          INNER JOIN [AduanasNueva].[dbo].[Clientes] CL ON (CL.CodigoSAP = I.IdCliente)  
          LEFT JOIN [AduanasNueva].[dbo].[DocumentosInstrucciones] DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1' AND DI.Eliminado = 0)  
          LEFT JOIN [AduanasNueva].[dbo].[DocumentosInstrucciones] DI2 ON (DI2.IdInstruccion = I.IdInstruccion AND DI2.CodDocumento in('50','51','52') AND DI2.Eliminado = 0)  
          LEFT JOIN [AduanasNueva].[dbo].[Eventos] EV ON (EV.IdInstruccion = I.IdInstruccion AND EV.IdEvento = '1')  
          left JOIN [AduanasNueva].[dbo].[TiemposFlujoCarga] GC1 ON (GC1.IdInstruccion = I.IdInstruccion AND GC1.IDEstado = '0' and GC1.Eliminado = 0) /*Comienzo de Flujo*/
          LEFT JOIN [AduanasNueva].[dbo].[TiemposFlujoCarga] GC2 ON (GC2.IdInstruccion = I.IdInstruccion AND GC2.IDEstado = 12 and GC2.Eliminado = 0) /*Validacion Electronica*/
          LEFT JOIN [AduanasNueva].[dbo].[TiemposFlujoCarga] GC3 ON (GC3.IdInstruccion = I.IdInstruccion AND GC3.IDEstado = 1 and GC3.Eliminado = 0) /*Confirmación Pago de Impuestos*/
          LEFT JOIN [AduanasNueva].[dbo].[TiemposFlujoCarga] GC4 ON (GC4.IdInstruccion = I.IdInstruccion AND GC4.IDEstado = 2 and GC4.Eliminado = 0) /*Canal de Selectividad*/
          LEFT JOIN [AduanasNueva].[dbo].[TiemposFlujoCarga] GC5 ON (GC5.IdInstruccion = I.IdInstruccion AND GC5.IDEstado = 4 and GC5.Eliminado = 0) /*Revicion de Mercaderia*/
          LEFT JOIN [AduanasNueva].[dbo].[TiemposFlujoCarga] GC6 ON (GC6.IdInstruccion = I.IdInstruccion AND GC6.IDEstado = 7 and GC6.Eliminado = 0) /*Emision Pase de Salida*/
          LEFT JOIN [AduanasNueva].[dbo].[TiemposFlujoCarga] GC7 ON (GC7.IdInstruccion = I.IdInstruccion AND GC7.IDEstado = 9 and GC7.Eliminado = 0) /*Entrega de Servicio*/
          LEFT JOIN [AduanasNueva].[dbo].[TiemposFlujoCarga] GC8 ON (GC8.IdInstruccion = I.IdInstruccion AND GC8.IDEstado = 14 and GC8.Eliminado = 0) /*Arribo de Carga Aduanas*/
		  LEFT JOIN  [AduanasNueva].[dbo].ProcesoFlujoCarga PFC ON (I.IdInstruccion=PFC.IdInstruccion)	
	      INNER JOIN [AduanasNueva].[dbo].[GestionCarga] GC11 ON (GC11.IdInstruccion = I.IdInstruccion AND GC11.Estado = '0' and GC11.Eliminado = 0) 
          LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC22 ON (GC22.IdInstruccion = I.IdInstruccion AND GC22.Estado = 1 and GC22.Eliminado = 0) 
          LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC33 ON (GC33.IdInstruccion = I.IdInstruccion AND GC33.Estado = 2 and GC33.Eliminado = 0) 
          LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC44 ON (GC44.IdInstruccion = I.IdInstruccion AND GC44.Estado = 3 and GC44.Eliminado = 0) 
          LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC55 ON (GC55.IdInstruccion = I.IdInstruccion AND GC55.Estado = 4 and GC55.Eliminado = 0) 
          LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC66 ON (GC66.IdInstruccion = I.IdInstruccion AND GC66.Estado = 5 and GC66.Eliminado = 0) 
          LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC77 ON (GC77.IdInstruccion = I.IdInstruccion AND GC77.Estado = 6 and GC77.Eliminado = 0) 
          LEFT JOIN [AduanasNueva].[dbo].[EspeciesFiscalesInstrucciones] EFI ON (EFI.IdInstruccion = I.IdInstruccion AND EFI.Eliminado = 0 AND EFI.CodEspecieFiscal = 'P') 
          WHERE I.IdCliente = '" + codCliente + "' AND I.CodPaisHojaRuta = '" + Pais + "' AND I.IdEstadoFlujo != '0' AND  isnull( CONVERT(Varchar,GC1.Fecha,111), CONVERT(Varchar,GC11.Fecha,111)) BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + "' ";
        this.loadSQL(sql);
    }

    public void loadInstruccionSelloTiempos(string fechaInicial, string fechaFinal, string codCliente, string Pais)
    {
        #region Malo
        //string sql = " SELECT DISTINCT(I.IdInstruccion), CL.Nombre, I.Producto, DI.DocumentoNo, GC1.Fecha AS ComienzoFlujo, EV.FechaInicio AS ArriboCarga, GC2.Fecha AS ValidaciónElectrónica, ";
        //sql += " GC3.Fecha AS PagoImpuestos, GC4.Fecha AS AsignaciónCanalSelectividad, I.Color, GC5.Fecha AS RevisiónMercancía, ";
        //sql += " GC6.Fecha AS EmisiónPaseSalida, GC7.Fecha AS EntregaServicio,I.CodRegimen AS Regime, EFI.Serie AS Serie,DI2.DocumentoNo AS BL, I.ReferenciaCliente as ReferenciaCliente, I.CiudadDestino as CiudadDestino FROM [AduanasNueva].[dbo].[Instrucciones] I ";
        //sql += " INNER JOIN [AduanasNueva].[dbo].[Clientes] CL ON (CL.CodigoSAP = I.IdCliente) ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[DocumentosInstrucciones] DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1' AND DI.Eliminado = 0) ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[DocumentosInstrucciones] DI2 ON (DI2.IdInstruccion = I.IdInstruccion AND DI2.CodDocumento in('50','51','52') AND DI2.Eliminado = 0) ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[Eventos] EV ON (EV.IdInstruccion = I.IdInstruccion AND EV.IdEvento = '1') ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC1 ON (GC1.IdInstruccion = I.IdInstruccion AND GC1.IdEstadoFlujo = '0') ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[FlujoCarga] FC2 ON (FC2.CodRegimen = I.CodRegimen AND FC2.CodPais = I.CodPaisHojaRuta AND FC2.Eliminado = '0' AND FC2.CodEstadoFlujoCarga = '1') ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC2 ON (GC2.IdInstruccion = I.IdInstruccion AND GC2.IdEstadoFlujo = FC2.IdEstadoFlujo) ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[FlujoCarga] FC3 ON (FC3.CodRegimen = I.CodRegimen AND FC3.CodPais = I.CodPaisHojaRuta AND FC3.Eliminado = '0' AND FC3.CodEstadoFlujoCarga = '2') ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC3 ON (GC3.IdInstruccion = I.IdInstruccion AND GC3.IdEstadoFlujo = FC3.IdEstadoFlujo) ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[FlujoCarga] FC4 ON (FC4.CodRegimen = I.CodRegimen AND FC4.CodPais = I.CodPaisHojaRuta AND FC4.Eliminado = '0' AND FC4.CodEstadoFlujoCarga = '3') ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC4 ON (GC4.IdInstruccion = I.IdInstruccion AND GC4.IdEstadoFlujo = FC4.IdEstadoFlujo) ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[FlujoCarga] FC5 ON (FC5.CodRegimen = I.CodRegimen AND FC5.CodPais = I.CodPaisHojaRuta AND FC5.Eliminado = '0' AND FC5.CodEstadoFlujoCarga = '4') ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC5 ON (GC5.IdInstruccion = I.IdInstruccion AND GC5.IdEstadoFlujo = FC5.IdEstadoFlujo) ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[FlujoCarga] FC6 ON (FC6.CodRegimen = I.CodRegimen AND FC6.CodPais = I.CodPaisHojaRuta AND FC6.Eliminado = '0' AND FC6.CodEstadoFlujoCarga = '5') ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC6 ON (GC6.IdInstruccion = I.IdInstruccion AND GC6.IdEstadoFlujo = FC6.IdEstadoFlujo) ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[FlujoCarga] FC7 ON (FC7.CodRegimen = I.CodRegimen AND FC7.CodPais = I.CodPaisHojaRuta AND FC7.Eliminado = '0' AND FC7.CodEstadoFlujoCarga = '6') ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC7 ON (GC7.IdInstruccion = I.IdInstruccion AND GC7.IdEstadoFlujo = FC7.IdEstadoFlujo) ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].[EspeciesFiscalesInstrucciones] EFI ON (EFI.IdInstruccion = I.IdInstruccion AND EFI.Eliminado = 0 AND EFI.CodEspecieFiscal = 'P')";
        //sql += " WHERE I.IdCliente = '" + codCliente + "' AND I.IdEstadoFlujo != '0' AND CONVERT(Varchar,GC1.Fecha,111) BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + "' ";
        //this.loadSQL(sql);
        #endregion
        string sql = @"
	            SELECT DISTINCT(I.IdInstruccion), CL.Nombre, I.Producto, DI.DocumentoNo,
	             GC1.Fecha AS ComienzoFlujo,
	            I.CodRegimen AS Regime, EFI.Serie AS Serie,DI2.DocumentoNo AS BL, I.ReferenciaCliente as 
	            ReferenciaCliente, I.CiudadDestino as CiudadDestino FROM  Instrucciones I 
	            INNER JOIN  [Clientes] CL ON (CL.CodigoSAP = I.IdCliente)  
	            LEFT JOIN  [DocumentosInstrucciones] DI ON (DI.IdInstruccion = I.IdInstruccion
	            AND DI.CodDocumento = '1' AND DI.Eliminado = 0) 
               LEFT JOIN  [DocumentosInstrucciones] DI2 ON
	            (DI2.IdInstruccion = I.IdInstruccion AND DI2.CodDocumento in('50','51','52') AND DI2.Eliminado = 0)  
	            INNER JOIN  [GestionCarga] GC1 ON (GC1.IdInstruccion = I.IdInstruccion AND GC1.Estado = '0' 
	            and GC1.Eliminado = 0) 
	            LEFT JOIN  [EspeciesFiscalesInstrucciones] 
	            EFI ON (EFI.IdInstruccion = I.IdInstruccion AND EFI.Eliminado = 0 AND EFI.CodEspecieFiscal = 'P')
	            WHERE
	             I.IdCliente = '" + codCliente + @"' 
	             AND
	              I.CodPaisHojaRuta = '" + Pais + @"'  
	            AND 
	            I.Fecha  
	            BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + @"' 
	            and I.CodRegimen='1000'
	            and 
	             CodEspecieFiscal is null ";
        this.loadSQL(sql);
    }

    public void loadInstruccionReporteGerencial(string fechaInicial, string fechaFinal, string codPais, string campo, string agrupar, string opcion, string codigo1, string codigo2)
    {
        string sql = " SELECT C1.Descripcion AS País, " + campo + ", Count(*) AS Cantidad FROM Instrucciones I ";
        sql += " INNER JOIN Aduanas A ON (A.CodigoAduana = I.CodigoAduana AND A.CodEstado = '0') ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'PAISES') ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) ";
        sql += " INNER JOIN Codigos C3 ON (C3.Codigo = I.CodPaisOrigen AND C3.Categoria = 'PAISES') ";
        sql += " INNER JOIN GestionCarga GC ON (GC.IdInstruccion = I.IdInstruccion AND GC.IdEstadoFlujo = '0' AND GC.Eliminado = '0') ";
        sql += " WHERE CONVERT(Varchar,GC.Fecha,111) BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + "' ";
        sql += " AND I.CodEstado != '100' AND I.CodPaisHojaRuta = '" + codPais + "' ";
        if (opcion == "1")
            sql += " AND (A.CodigoAduana = '" + codigo1 + "' OR C.CodigoSAP = '" + codigo1 + "' OR C2.Codigo = '" + codigo1 + "') ";
        if (opcion == "2")
            sql += " AND (A.CodigoAduana = '" + codigo1 + "' OR C.CodigoSAP = '" + codigo1 + "' OR C2.Codigo = '" + codigo1 + "') AND (A.CodigoAduana = '" + codigo2 + "' OR C.CodigoSAP = '" + codigo2 + "' OR C2.Codigo = '" + codigo2 + "') ";
        sql += " GROUP BY C1.Descripcion, " + agrupar;
        sql += " ORDER BY 2 ";
        this.loadSQL(sql);
    }

    public void loadDescripcionEstados(string idInstruccion)
    {
        string sql = " SELECT GC.IDEstadoFlujo AS Codigo, CASE WHEN (GC.IdEstadoFlujo = '0') THEN 'Comienzo Flujo' ELSE FC.EstadoFlujo END Descripcion FROM Instrucciones I ";
        sql += " LEFT JOIN GestionCarga GC ON (GC.IdInstruccion = I.IdInstruccion AND GC.Eliminado = '0') ";
        sql += " LEFT JOIN FlujoCarga FC ON (FC.IdEstadoFlujo = GC.IdEstadoFlujo AND FC.Eliminado = '0') ";
        sql += " WHERE I.IdInstruccion = '" + idInstruccion + "' AND I.CodEstado != '100' ";
        sql += " UNION ";
        sql += " SELECT E.IdEvento + C1.Codigo AS Codigo,  ";
        sql += " CASE WHEN (C1.Codigo = 'A') THEN C.Descripcion + ' Inicio' ELSE C.Descripcion + ' Fin' END Descripcion ";
        sql += " FROM Instrucciones I ";
        sql += " LEFT JOIN Eventos E ON (E.IdInstruccion = I.IdInstruccion) ";
        sql += " LEFT JOIN Codigos C ON (C.Codigo = E.IdEvento AND C.Categoria = 'EVENTOS') ";
        sql += " LEFT JOIN Codigos C1 ON (C1.Codigo != E.IdEvento AND C1.Categoria = 'REPORTEEVENTOS') ";
        sql += " WHERE I.IdInstruccion = '" + idInstruccion + "' AND I.CodEstado != '100' ";
        this.loadSQL(sql);
    }

    public void ReporteOger(string FechaInicio, string FechaFin)
    {
        string sql = " SELECT DISTINCT A.IdInstruccion as HojaRuta,A.ReferenciaCliente as REFOperacion,B.Nombre as Empresa,C.Serie as [Fauca/Dua],Producto ";
        sql += " ,D.DocumentoNo as [HBL/HAWB],A.DescripcionContenedores,V.Descripcion as Origen,A.CodigoAduana as Aduana,A.CodRegimen as Regimen";
        sql += " ,Convert (Varchar,A.ETA) as ETA,Convert (Varchar,E.Fecha) as CopiasDOCSREC,ISNULL (Convert (varchar ,F.FechaInicio), 'NO HAY FECHA' ) as PermisoIngresadoInicio";
        sql += " ,ISNULL (Convert (varchar ,F.FechaFin), 'NO HAY FECHA' ) as PermisoIngresadoFin,ISNULL (Convert (varchar ,G.FechaInicio), 'NO HAY FECHA' ) as PermisoRecibido";
        sql += " ,ISNULL (Convert (varchar ,I.FechaInicio), 'NO HAY FECHA' ) as EnviodePrepoliza,ISNULL (Convert (varchar ,I.FechaFin), 'NO HAY FECHA' ) as AutorizacionPrepoliza";
        sql += " ,ISNULL (Convert (varchar ,H.FechaFin), 'NO HAY FECHA' ) as EnvioPoliza,ISNULL (Convert (varchar ,R.Fecha), 'NO HAY FECHA' ) as PolizaIngresada ,ISNULL (Convert (varchar ,K.Fecha), 'NO HAY FECHA' ) as PagoImpuesto";
        sql += " ,ISNULL (Convert (varchar ,M.Fecha), 'NO HAY FECHA' ) as ValidacionElectronicaVerde,ISNULL (Convert (varchar ,Z.Fecha), 'NO HAY FECHA' ) as ValidacionElectronicaRojo,ISNULL (Convert (varchar ,P.Fecha), 'NO HAY FECHA' ) as Liberacion";
        sql += " ,ISNULL (Convert (varchar ,S.FechaInicio), 'NO HAY FECHA' ) as SalidadePuerto,ISNULL (Convert (varchar ,U.Fecha), 'NO HAY FECHA' ) as EntregaBodega,ISNULL (Convert (varchar ,U.Observacion), 'NO HAY FECHA' ) as Observaciones FROM Instrucciones A";
        sql += " Inner Join Clientes B on (A.IdCliente = B.CodigoSAP and A.IdCliente in('1000055','1000067')) Inner Join EspeciesFiscalesInstrucciones C on (A.IdInstruccion = C.IdInstruccion and C.CodEspecieFiscal in ('F','P'))";
        sql += " Inner Join DocumentosInstrucciones D on ( D.CodDocumento <> 1 and A.IdInstruccion = D.IdInstruccion ) Inner Join GestionCarga E on (A.IdInstruccion = E.IdInstruccion and E.IdEstadoFlujo = '0') Left Join Eventos F on (A.IdInstruccion = F.IdInstruccion and F.IdEvento = 10)";
        sql += " Left Join Eventos G on (A.IdInstruccion = G.IdInstruccion and G.IdEvento = 11) Left Join Eventos H on (A.IdInstruccion = H.IdInstruccion and H.IdEvento = 12) Left Join Eventos I on (A.IdInstruccion = I.IdInstruccion and I.IdEvento = 7)";
        sql += " Left Join FlujoCarga J on ( A.CodRegimen = J.CodRegimen and J.CodEstadoFlujoCarga = 1) Left Join GestionCarga K on (A.IdInstruccion = K.IdInstruccion and J.IdEstadoFlujo = K.IdEstadoFlujo) Left Join FlujoCarga L on ( A.CodRegimen = L.CodRegimen and L.CodEstadoFlujoCarga = 3 and A.Color = 'Verde')";
        sql += " Left Join GestionCarga M on (A.IdInstruccion = M.IdInstruccion and L.IdEstadoFlujo = M.IdEstadoFlujo) Left Join FlujoCarga N on ( A.CodRegimen = N.CodRegimen and N.CodEstadoFlujoCarga = 3 and A.Color != 'Verde')";
        sql += " Left Join GestionCarga Z on (A.IdInstruccion = Z.IdInstruccion and N.IdEstadoFlujo = Z.IdEstadoFlujo) Left Join FlujoCarga O on ( A.CodRegimen = O.CodRegimen and O.CodEstadoFlujoCarga = 5)";
        sql += " Left Join GestionCarga P on (A.IdInstruccion = P.IdInstruccion and O.IdEstadoFlujo = P.IdEstadoFlujo) Left Join FlujoCarga Q on ( A.CodRegimen = Q.CodRegimen and Q.CodEstadoFlujoCarga = 2)";
        sql += " Left Join GestionCarga R on (A.IdInstruccion = R.IdInstruccion and Q.IdEstadoFlujo = R.IdEstadoFlujo) Left Join Eventos S on (A.IdInstruccion = S.IdInstruccion and S.IdEvento = 12)";
        sql += " Left Join FlujoCarga T on ( A.CodRegimen = T.CodRegimen and T.CodEstadoFlujoCarga = 6) Left Join GestionCarga U on (A.IdInstruccion = U.IdInstruccion and T.IdEstadoFlujo = U.IdEstadoFlujo)";
        sql += " Inner Join Codigos V on (Categoria = 'PAISES' and A.[CodPaisOrigen] = V.Codigo)";
        sql += "   Where convert(varchar,A.Fecha, 111) between '" + FechaInicio + "' and '" + FechaFin + "'";
        this.loadSQL(sql);
    }

    public void ReporteUNO(string FechaInicio, string FechaFin)
    {
        string sql = " Select Convert(Varchar,A.Fecha,101) as Fecha, A.IdInstruccion as HojaRuta, B.Nombre as Cliente, A.CodRegimen as Regimen, C.Serie as Serie,";
        sql += " A.NoCorrelativo as Correlativo,SUM(D.ItemAPV) as ItemAPV,COUNT(D.ItemAPV) as Cuenta,E.Factura as Factura,";
        sql += " E.ItemAPV as APV, E.Galones as Galones,A.PesoKgs as Kilos,E.Producto as Producto,F.DocumentoNo as BL,G.Motorista as Vapor";
        sql += " From Instrucciones A Inner Join Clientes B on (A.IdCliente = B.CodigoSAP and B.Eliminado = 0)";
        sql += " Inner Join UNO D on (A.IdInstruccion = D.IdInstruccion)";
        sql += " Inner Join UNO E on (A.IdInstruccion = E.IdInstruccion)";
        sql += " Left Join EspeciesFiscalesInstrucciones C on (A.IdInstruccion = C.IdInstruccion and C.CodEspecieFiscal = 'P')";
        sql += " Left Join DocumentosInstrucciones F on (A.IdInstruccion = F.IdInstruccion and F.CodDocumento in(50,51,52) and F.Eliminado = 0)";
        sql += " Left Join DatosGestionDocumentos G on (A.IdInstruccion = G.IdInstruccion and G.Eliminado = 0)";
        sql += " Where IdCliente = '8000006' and convert(varchar,A.Fecha, 111) between '" + FechaInicio + "' and '" + FechaFin + "' and A.Producto like '%DIESEL%' and A.CodEstado != 99 or";
        sql += " IdCliente = '8000006' and convert(varchar,A.Fecha, 111) between '" + FechaInicio + "' and '" + FechaFin + "' and A.Producto like '%AV JET%' and A.CodEstado != 99 or";
        sql += " IdCliente = '8000006' and convert(varchar,A.Fecha, 111) between '" + FechaInicio + "' and '" + FechaFin + "' and A.Producto like '%GASOLINA REGULAR%' and A.CodEstado != 99 or";
        sql += " IdCliente = '8000006' and convert(varchar,A.Fecha, 111) between '" + FechaInicio + "' and '" + FechaFin + "' and A.Producto like '%BUNKER%' and A.CodEstado != 99 or";
        sql += " IdCliente = '8000006' and convert(varchar,A.Fecha, 111) between '" + FechaInicio + "' and '" + FechaFin + "' and A.Producto like '%KEROSENE%' and A.CodEstado != 99";
        sql += " Group by A.Fecha,A.IdInstruccion,B.Nombre,A.CodRegimen,C.Serie,A.NoCorrelativo,E.ItemAPV,E.Galones,A.PesoKgs,E.Producto,F.DocumentoNo,G.Motorista,E.Factura";
        //sql += " Where convert(varchar,A.Fecha, 111) between '" + FechaInicio + "' and '" + FechaFin + "'";
        this.loadSQL(sql);
    }

    public void loadInstruccionesCargarArchivo()
    {
        string sql = " SELECT I.IdInstruccion, C.Nombre AS Cliente FROM Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0') AND IdCliente IN ('8000838','8000839','8000840') ";
        sql += " AND I.IdEstadoFlujo != '0' AND I.IdEstadoFlujo != '99' ";
        sql += " UNION ";
        sql += " SELECT I.IdInstruccion, C.Nombre AS Cliente FROM Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = '0') ";
        sql += " INNER JOIN FlujoCarga FC7 ON (FC7.CodRegimen = I.CodRegimen AND FC7.CodPais = I.CodPaisHojaRuta AND FC7.Eliminado = '0' AND FC7.CodEstadoFlujoCarga = '6') ";
        sql += " INNER JOIN GestionCarga GC7 ON (GC7.IdInstruccion = I.IdInstruccion AND GC7.IdEstadoFlujo = FC7.IdEstadoFlujo) ";
        sql += " WHERE I.CodEstado IN ('0') AND IdCliente IN ('8000838','8000839','8000840') ";
        sql += " AND I.IdEstadoFlujo = '99' AND GETDATE() <= DATEADD(WEEK, 1, GC7.Fecha) ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesCargarArchivoXPais(string codPais)
    {
        string sql = " SELECT I.IdInstruccion, C.Nombre AS Cliente FROM Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0') AND IdCliente IN ('8000838','8000839','8000840') ";
        sql += " AND I.IdEstadoFlujo != '0' AND I.IdEstadoFlujo != '99' ";
        sql += " AND I.CodPaisHojaRuta = '" + codPais + "' ";
        sql += " UNION ";
        sql += " SELECT I.IdInstruccion, C.Nombre AS Cliente FROM Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = '0') ";
        sql += " INNER JOIN FlujoCarga FC7 ON (FC7.CodRegimen = I.CodRegimen AND FC7.CodPais = I.CodPaisHojaRuta AND FC7.Eliminado = '0' AND FC7.CodEstadoFlujoCarga = '6') ";
        sql += " INNER JOIN GestionCarga GC7 ON (GC7.IdInstruccion = I.IdInstruccion AND GC7.IdEstadoFlujo = FC7.IdEstadoFlujo) ";
        sql += " WHERE I.CodEstado IN ('0') AND IdCliente IN ('8000838','8000839','8000840') ";
        sql += " AND I.IdEstadoFlujo = '99' AND GETDATE() <= DATEADD(WEEK, 1, GC7.Fecha) ";
        sql += " AND I.CodPaisHojaRuta = '" + codPais + "' ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesCargarArchivoXAduana(string codAduana)
    {
        string sql = " SELECT I.IdInstruccion, C.Nombre AS Cliente FROM Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0') AND IdCliente IN ('8000838','8000839','8000840') ";
        sql += " AND I.IdEstadoFlujo != '0' AND I.IdEstadoFlujo != '99' ";
        sql += " AND I.CodigoAduana = '" + codAduana + "' ";
        sql += " UNION ";
        sql += " SELECT I.IdInstruccion, C.Nombre AS Cliente FROM Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = '0') ";
        sql += " INNER JOIN FlujoCarga FC7 ON (FC7.CodRegimen = I.CodRegimen AND FC7.CodPais = I.CodPaisHojaRuta AND FC7.Eliminado = '0' AND FC7.CodEstadoFlujoCarga = '6') ";
        sql += " INNER JOIN GestionCarga GC7 ON (GC7.IdInstruccion = I.IdInstruccion AND GC7.IdEstadoFlujo = FC7.IdEstadoFlujo) ";
        sql += " WHERE I.CodEstado IN ('0') AND IdCliente IN ('8000838','8000839','8000840') ";
        sql += " AND I.IdEstadoFlujo = '99' AND GETDATE() <= DATEADD(WEEK, 1, GC7.Fecha) ";
        sql += " AND I.CodigoAduana = '" + codAduana + "' ";
        this.loadSQL(sql);
    }

    public void ReporteHagamodaPromoda(string cliente, string fechainicio, string fechafin)
    {
        string sql = " SELECT DISTINCT  I.IdInstruccion,I.Proveedor,CL.Nombre as Cliente,DI1.DocumentoNo AS Factura,DI2.DocumentoNo AS ValorDocumentoEmbarque,";
        sql += " I.CantidadBultos,I.PesoKgs,X.ObservacionFin as PesoMiami,W.ObservacionFin as IngresoSwissport,V.ObservacionFin as SalidaSwissport ,I.Producto,CONVERT(varchar,GC1.Fecha, 121)AS ComienzoFlujo,CONVERT(varchar,E.FechaInicio,121) AS ArribodeCarga, ";
        sql += " CONVERT(varchar,Z.FechaInicio,121) as ENVIODEPRELIQUIDACION,CONVERT(varchar,Z.FechaFin,121) as APROBACIONDEPRELIQUIDACION ,";
        sql += " CONVERT(varchar,Y.FechaFin,121) as ENVIODEBOLETIN ,CONVERT(varchar,GC4.Fecha,121) AS PagodeImpuestos,I.Color as Color,";
        sql += " CONVERT(varchar,GC6.Fecha,121) AS RevisionMercancia,CONVERT(varchar,GC7.Fecha,121) AS EmisionPaseSalida, CONVERT(varchar,GC8.Fecha,121) AS EntregadeServicios, U2.Nombre +' '+U2.Apellido  as Aforador,";
        sql += "CONVERT(varchar,T.FechaInicio,121) as IngresoDeposito,CONVERT(varchar,U.FechaInicio,121) as EntregaCliente,";
        sql += " 'Dias: ' + CAST(DATEDIFF(SECOND, GC1.Fecha, GC.Fecha) / 86400 AS varchar(50)) + ' Horas: ' + CAST((DATEDIFF(SECOND, GC1.Fecha, GC.Fecha) % 86400) / 3600 ";
        sql += " AS varchar(50)) + ' Minutos: ' + CAST((DATEDIFF(SECOND, GC1.Fecha, GC.Fecha) % 3600) / 60 AS varchar(50)) AS TiempoProceso,";
        sql += " 'Dias: ' +  CAST(DATEDIFF(SECOND, E.FechaInicio, GC.Fecha) / 86400 AS varchar(50)) + ' Horas: ' + CAST((DATEDIFF(SECOND, E.FechaInicio, GC.Fecha) % 86400) / 3600 ";
        sql += " AS varchar(50)) +' Minutos: ' + CAST((DATEDIFF(SECOND, E.FechaInicio, GC.Fecha) % 3600) / 60 AS varchar(50)) AS TiempoProceso2  FROM Instrucciones I  ";
        sql += " INNER JOIN Clientes CL ON (CL.CodigoSAP = I.IdCliente AND CL.Eliminado = '0') INNER JOIN Aduanas A ON (A.CodigoAduana = I.CodigoAduana AND A.CodEstado = '0')";
        sql += " LEFT JOIN DocumentosInstrucciones DI1 ON (DI1.IdInstruccion = I.IdInstruccion AND DI1.CodDocumento = '1' AND DI1.Eliminado ='0')  ";
        sql += " LEFT JOIN DocumentosInstrucciones DI2 ON (DI2.IdInstruccion = I.IdInstruccion AND  DI2.CodDocumento = I.CodDocumentoEmbarque AND DI2.Eliminado ='0') ";
        sql += " LEFT JOIN EspeciesFiscalesInstrucciones EFI ON (EFI.IdInstruccion = I.IdInstruccion  AND (EFI.CodEspecieFiscal = 'F' OR EFI.CodEspecieFiscal = 'P') ";
        sql += " AND EFI.Eliminado ='0') INNER JOIN Codigos C ON (C.Codigo = I.CodPaisHojaRuta AND C.Categoria = 'PAISES') ";
        sql += " LEFT JOIN Eventos E ON (E.IdInstruccion = I.IdInstruccion AND E.IdEvento = '1') ";
        sql += " LEFT JOIN Eventos Z ON (Z.IdInstruccion = I.IdInstruccion AND Z.IdEvento = '7') ";
        sql += " LEFT JOIN Eventos Y ON (Y.IdInstruccion = I.IdInstruccion AND Y.IdEvento = '18') ";
        sql += " LEFT JOIN Eventos X ON (X.IdInstruccion = I.IdInstruccion AND X.IdEvento = '19') ";
        sql += " LEFT JOIN Eventos W ON (W.IdInstruccion = I.IdInstruccion AND W.IdEvento = '20') ";
        sql += " LEFT JOIN Eventos V ON (V.IdInstruccion = I.IdInstruccion AND V.IdEvento = '21') ";
        sql += " LEFT JOIN Eventos T ON (T.IdInstruccion = I.IdInstruccion AND T.IdEvento = '22') ";
        sql += " LEFT JOIN Eventos U ON (U.IdInstruccion = I.IdInstruccion AND U.IdEvento = '23') ";
        sql += " LEFT JOIN FlujoCarga FC ON (FC.CodRegimen = I.CodRegimen AND FC.CodPais = I.CodPaisHojaRuta AND FC.CodEstadoFlujoCarga = '6' AND FC.Eliminado = '0') ";
        sql += " LEFT JOIN GestionCarga GC ON (GC.IdInstruccion = I.IdInstruccion AND GC.IdEstadoFlujo = FC.IdEstadoFlujo AND GC.Eliminado = '0')  ";
        sql += " LEFT JOIN GestionCarga GC1 ON (GC1.IdInstruccion = I.IdInstruccion AND GC1.IdEstadoFlujo = '0'  AND GC1.Eliminado = '0')  ";
        sql += " LEFT JOIN FlujoCarga FC2 ON (FC2.IdEstadoFlujo = I.IdEstadoFlujo AND FC2.Eliminado = '0') ";
        sql += " LEFT JOIN FlujoCarga FC3 ON (FC3.CodRegimen = I.CodRegimen AND FC3.CodPais = I.CodPaisHojaRuta AND FC3.CodEstadoFlujoCarga = '1' AND FC3.Eliminado = '0') ";
        sql += " LEFT JOIN GestionCarga GC3 ON (GC3.IdInstruccion = I.IdInstruccion AND GC3.IdEstadoFlujo = FC3.IdEstadoFlujo AND GC3.Eliminado = '0')  ";
        sql += " LEFT JOIN FlujoCarga FC4 ON (FC4.CodRegimen = I.CodRegimen AND FC4.CodPais = I.CodPaisHojaRuta AND FC4.CodEstadoFlujoCarga = '2' AND FC4.Eliminado = '0') ";
        sql += " LEFT JOIN GestionCarga GC4 ON (GC4.IdInstruccion = I.IdInstruccion AND GC4.IdEstadoFlujo = FC4.IdEstadoFlujo AND GC4.Eliminado = '0')  ";
        sql += " LEFT JOIN FlujoCarga FC5 ON (FC5.CodRegimen = I.CodRegimen AND FC5.CodPais = I.CodPaisHojaRuta AND FC5.CodEstadoFlujoCarga = '3' AND FC5.Eliminado = '0') ";
        sql += " LEFT JOIN GestionCarga GC5 ON (GC5.IdInstruccion = I.IdInstruccion AND GC5.IdEstadoFlujo = FC5.IdEstadoFlujo AND GC5.Eliminado = '0')  ";
        sql += " LEFT JOIN FlujoCarga FC6 ON (FC6.CodRegimen = I.CodRegimen AND FC6.CodPais = I.CodPaisHojaRuta AND FC6.CodEstadoFlujoCarga = '4' AND FC6.Eliminado = '0') ";
        sql += " LEFT JOIN GestionCarga GC6 ON (GC6.IdInstruccion = I.IdInstruccion AND GC6.IdEstadoFlujo = FC6.IdEstadoFlujo AND GC6.Eliminado = '0')  ";
        sql += " LEFT JOIN FlujoCarga FC7 ON (FC7.CodRegimen = I.CodRegimen AND FC7.CodPais = I.CodPaisHojaRuta AND FC7.CodEstadoFlujoCarga = '5' AND FC7.Eliminado = '0') ";
        sql += " LEFT JOIN FlujoCarga FC8 ON (FC8.CodRegimen = I.CodRegimen AND FC8.CodPais = I.CodPaisHojaRuta AND FC8.CodEstadoFlujoCarga = '6' AND FC7.Eliminado = '0')  ";
        sql += " LEFT JOIN GestionCarga GC8 ON (GC8.IdInstruccion = I.IdInstruccion AND GC8.IdEstadoFlujo = FC8.IdEstadoFlujo AND GC8.Eliminado = '0')  ";
        sql += " LEFT JOIN GestionCarga GC7 ON (GC7.IdInstruccion = I.IdInstruccion AND GC7.IdEstadoFlujo = FC7.IdEstadoFlujo AND GC7.Eliminado = '0')  ";
        sql += " INNER JOIN Usuarios U1 ON (U1.IdUsuario = I.IdUsuarioAduana) LEFT JOIN Usuarios U2 ON (U2.IdUsuario = I.Aforador)  ";
        sql += " LEFT JOIN Usuarios U3 ON (U3.IdUsuario = I.Tramitador)";
        sql += "  WHERE I.CodEstado IN ('0','1') AND CL.CodigoSAP =  '" + cliente + "' AND CONVERT(VARCHAR, I.Fecha, 111) >= '" + fechainicio + "' AND CONVERT(VARCHAR, I.Fecha, 111) <= '" + fechafin + "'";
        this.loadSQL(sql);
    }

    public void ReporteCargill(string fechainicio, string fechafin)
    {
        string sql = "SELECT B.RTN,A.CodPaisHojaRuta,C.Descripcion ,D.PosicionArancelaria, COUNT(D.PosicionArancelaria)as NumeroTransacciones, SUM(D.ValorCIF)as ValorCIF,Sum(D.DAITotal) as DAI, ";
        sql += " SUM(D.SELTotal)as Selectivo, SUM(D.ISVTotal) as ISV,'Vesta' as Nombre, 'HONDURAS' as Pais, F.Descripcion as Origen, ";
        sql += " case Month(A.Fecha) when 1 then '31.01' when 2 then '28.02' when 3 then '31.03' when 4 then '30.04' when 5 then '31.05' when 6 then '30.06' ";
        sql += " when 7 then '31.07' when 8 then '31.08' when 9 then '30.09' when 10 then '31.10' when 11 then '30.11'else '31.12' end as PeriodoFin ";
        sql += " , case D.Preferencia when '2' then 'Preferencia' else 'Sin Preferencia' end as Preferencia ";
        sql += " ,Case When A.CodRegimen in ('4600','4070','4000') then '01' When A.CodRegimen = '7000' then '02' when A.CodRegimen in ('8000','8100') then '05' ";
        sql += " When A.CodRegimen = '1000' then '07' When A.CodRegimen = '5100' then '03' else '08' end as CodigoTransaccion, B.Nombre as FDCNumber ";
        sql += " , case when A.Division in (3,4,5,6) then 'MEAT' else 'CFN' end as BUname,G.Descripcion as Import ,H.Descripcion as Export";
        sql += " FROM Instrucciones A Inner Join Clientes B on (B.CodigoSAP in (8000423,8000059) and A.IdCliente = B.CodigoSAP and B.Eliminado = 0) ";
        sql += " Right Join [EsquemaTramites2].[dbo].[HojaRutaPreliquidacionDet] D on (A.IdInstruccion = D.IdHojaRuta collate Modern_Spanish_CI_AI )";
        sql += " Inner Join Codigos C on (C.Categoria = 'REGIMENHONDURAS' and C.Codigo = A.CodRegimen and C.Eliminado = 0)";
        sql += " Left Join Codigos F on (F.Categoria = 'PAISES' and A.CodPaisOrigen = F.Codigo)";
        sql += " Left Join Codigos G on (G.Categoria = 'PAISES' and A.CodPaisHojaRuta = G.Codigo) ";
        sql += " Left Join Codigos H on (H.Categoria = 'PAISES' and A.CodPaisProcedencia = H.Codigo) ";
        sql += " Where CONVERT(VARCHAR, A.Fecha, 111) >= '" + fechainicio + "' AND CONVERT(VARCHAR, A.Fecha, 111) <= '" + fechafin + "' and IdCliente in(8000423,8000059) and A.CodEstado !=100";
        sql += " Group by A.CodPaisHojaRuta,D.PosicionArancelaria,B.RTN ,C.Descripcion,F.Descripcion,Month(A.fecha),Preferencia, A.CodRegimen,B.Nombre, A.Division,G.Descripcion,H.Descripcion";
        sql += " order by D.PosicionArancelaria, PeriodoFin";
        this.loadSQL(sql);
    }

    public void ReporteHCC(string fechainicio, string fechafin, string cliente)
    {
        string sql = @"SELECT B.Descripcion as Descripcion,Z.NombreAduana as NombreAduana,Case When A.Division IS NULL Then C.Nombre When A.Division ='' Then C.Nombre ELSE  Q.Descripcion END as Nombre,A.IdInstruccion,case when A.NumeroFactura IS null then I.DocumentoNo else A.NumeroFactura end as NumeroFactura 
        ,ISNULL(convert(varchar,FechaEntregaCargill),'N/A') as FechaEntregaCargill, ISNULL(OrdenCompra,'N/A') as #OrdenCompra,ISNULL(Proveedor,'N/A') as Proveedor
        ,ISNULL(Producto, 'N/A') as Producto,ISNULL(D.PosicionArancelaria,'N/A') as PosicionArancelaria
        ,ISNULL(Convert(Varchar,D.ISV) + '% ISV'+ Convert(Varchar,D.DAI) +' DAI','N/A') as '%Arancel'  
        ,ISNULL(CONVERT(VarChar(50),cast( (Select Top 1 ValorReal From [EsquemaTramites2].[dbo].[HojaRutaEsquemaTramites] Y Where A.IdInstruccion = Y.IdHojaRuta collate Modern_Spanish_CS_AS and Y.IdConceptoSAP = 82) as Money)),'N/A') as ValorImpuestoTotal
        ,ISNULL(CONVERT(VarChar ,ETAPlanta), 'N/A') as ETAPLANTA,'' as GestionDocumentos,ISNULL(Incoterm,'N/A') as Incoterm, ISNULL(ReferenciaCliente,'N/A') as ReferenciaCliente
        ,ISNULL(E.Serie,'N/A') as DUA, ISNULL(CONVERT(VarChar(50), cast( Flete as money ), 1),'N/A') as Flete,CONVERT(VarChar(50), cast( Seguro as money ), 1)as Seguro
        ,CONVERT(VarChar(50) , cast( A.ValorCIF as money ), 1) as ValorFactura ,F.Descripcion as PAISORIGEN,G.Descripcion as Transporte,A.CodRegimen as IdRegimen
        ,H.Empresa as Courier ,A.Contenedores as Contenedores ,ISNULL(A.DescripcionContenedores,'N/A') as DescripcionContenedores,ISNULL(M.DocumentoNo,'N/A')as DocumentoNo,Convert(BigInt, Round((PesoKgs*2.2046),0)) as PesoLibras ,A.CantidadBultos 
        ,J.Descripcion as Presentacion ,ISNULL(TrackingCourier,'N/A') as TackingCurrier,ISNULL(CONVERT(VARCHAR,FechaReciboCopias),'N/A') as Fechaderecibocopiasdocs
        ,ISNULL(CONVERT(VARCHAR,FechaReciboOriginales),'N/A') as Fecharecibooriginales ,ISNULL(CONVERT(VARCHAR,K.FechaInicio),'N/A') as MAGA
        ,ISNULL(CONVERT(VARCHAR,K.FechaFin),'N/A') as Fechaextendidopermiso, ISNULL(L.PermisoNo,'N/A') as PermisoNo ,ISNULL(Color,'N/A') as Color
        ,ISNULL(CONVERT(VARCHAR,N.FechaInicio),'N/A') as FechaArriboAduana,ISNULL(CONVERT(VARCHAR,FechaArriboPlantaProceso),'N/A') as FechaPlantaProceso
        ,ISNULL(Convert(Varchar,DATEDIFF(DAY,N.FechaInicio,FechaArriboPlantaProceso)+1),'N/A') as TiempoLlegadaHastaEntrega,ISNULL(DiasLibresAlmacenaje,'N/A') as DiasLibresAlmacenaje 
        ,ISNULL(TiempoEnergia,'N/A') as TiempoEnergia, ISNULL(CONVERT(VARCHAR,(DATEADD(Day,(DiasLibresAlmacenaje-1),N.FechaInicio))),'N/A') as TerminaTiempoLibre
        ,ISNULL(CargoAlmacenajeDCS,'N/A') as CargoAlmacenaje,ISNULL(CONVERT(VARCHAR,DATEADD(Hour,((convert(money,TiempoEnergia)*24)+0),N.FechaInicio)),'N/A') as TerminaTiempoLibreElectricidad
        ,ISNULL(CargoDemoraElectricidad,'N/A') as CargoDemoraElectricidad ,ISNULL(CONVERT(VARCHAR,FechaRetornoContenedor),'N/A') as FechaRetornoContenedor 
        ,ISNULL(DiasDemora,'N/A') as DiasDemora,ISNULL(Demora,'N/A') as Demora ,ISNULL(CONVERT(VARCHAR,A.Fecha),'N/A') as FechaRegistro
        , case when A.IdEstadoFlujo = '0' Then 'Comienzo Flujo' when A.IdEstadoFlujo = '99' Then 'FLujo Finalizado' Else O.EstadoFlujo end As EstadoFlujo
        ,Observaciones as Notes,ISNULL(DeclaracionSeguro,'N/A') as NumeroDeclaracionSeguro,'' as Fecha,ISNULL(FacturaVESTA,'N/A') as FacturaVESTA, A.ProductosCargill as ProductosCargill 
        ,case P.ColorTemp When 'F' Then 'Fusia' When 'M' Then 'Morado' Else case when A.CodEstado != 0 Then 'Verde' When A.ArriboCarga = 0 Then 'Azul' When A.IdEstadoFlujo = '99'
        and P.FechaArriboPlantaProceso IS NOT NULL Then 'Amarillo' Else 'Cafe' End End as ColorCargill ,A.CodEstado, A.ArriboCarga,A.IdEstadoFlujo
        , P.PuertoOrigen as PuertoOrigen, A.Color
        FROM [Instrucciones] A  
        Inner Join Codigos B on (A.CodTipoCargamento = B.Codigo and B.Categoria = 'TIPOCARGAMENTO' and CodEstado != 100)
        Inner Join Clientes C on (A.IdCliente = C.CodigoSAP and C.Eliminado = 0)
        Inner Join Aduanas Z on (A.CodigoAduana = Z.CodigoAduana and Z.CodEstado = 0)
        Left Join [EsquemaTramites2].[dbo].[HojaRutaPreliquidacionDet] D on (A.IdInstruccion = D.IdHojaRuta collate Modern_Spanish_CI_AI ) 
        Left Join EspeciesFiscalesInstrucciones E on (A.IdInstruccion = E.IdInstruccion and E.CodEspecieFiscal in ('P','F') and E.Eliminado = 0)
        Left Join Codigos F on (A.CodPaisOrigen = F.Codigo and F.Categoria = 'PAISES' and A.CodEstado != 100)
        Left Join Codigos G on (A.CodTipoTransporte = G.Codigo and G.Categoria = 'TIPOTRANSPORTE' and A.CodEstado != 100)
        Left Join DatosGestionDocumentos H on (A.IdInstruccion = H.IdInstruccion and H.Eliminado = 0)
        Left Join DocumentosInstrucciones I on (A.IdInstruccion = I.IdInstruccion and I.Eliminado = 0 and I.CodDocumento = 1)
        Left Join Codigos J on (A.CodTipoCarga= J.Codigo and J.Categoria = 'TIPOCARGA' and J.Eliminado != 100)
        Left Join Eventos K on (A.IdInstruccion = K.IdInstruccion and K.IdEvento = 10)
        Left Join PermisosInstrucciones L on (L.IdInstruccion = A.IdInstruccion and L.CodPermiso in ('GT-1','HN-1','GT-4') and L.Eliminado = 0)
        Left Join DocumentosInstrucciones M on (A.IdInstruccion = M.IdInstruccion and M.Eliminado = 0 and M.CodDocumento in (50,51,52)) 
        Left Join Eventos N on (A.IdInstruccion = N.IdInstruccion and N.IdEvento = 1) 
        Left Join FlujoCarga O on (A.IdEstadoFlujo = O.IdEstadoFlujo and A.CodPaisHojaRuta = O.CodPais and A.CodRegimen = O.CodRegimen and O.Eliminado = 0)
        Left Join Codigos Q on (A.IdCliente = Q.Categoria and A.Division = Q.Codigo and Q.Eliminado = 0)
        Left Join [PortalClientes].[dbo].[HCC] P on (A.IdInstruccion = P.IdInstruccion)"; 
        sql += " Where CONVERT(VARCHAR, A.Fecha, 111) >= '" + fechainicio + "' AND CONVERT(VARCHAR, A.Fecha, 111) <= '" + fechafin + "'";
        sql += " and A.IdCliente = " + cliente + "";
        sql += " OR A.IdCliente = " + cliente + "";
        sql += " and A.IdEstadoFlujo != '99' and dateadd(MONTH,4,ISNULL(A.[Fecha], GETDATE ())) > = '" + fechainicio + "'";
        sql += " OR A.IdCliente = " + cliente + "";
        sql += " and A.IdEstadoFlujo = '99' AND CONVERT(VARCHAR, A.FechaFinalFlujo, 111) >= '" + fechainicio + "' AND CONVERT(VARCHAR, A.FechaFinalFlujo, 111) <= '" + fechafin + "' Order by A.IdInstruccion";
        this.loadSQL(sql);
    }

    public void ReporteImpuesto(string fechainicio, string fechafin, string cliente)
    {
        string sql = "SELECT A.Item,B.NombreAduana,A.Fecha,A.Proveedor,A.NumeroFactura,A.Producto,A.ValorCIF,Convert(Varchar,H.Compra)+'Compra - '+Convert(Varchar,H.Venta)+'Venta' as Cambio";
        sql += " ,C.PosicionArancelaria,A.NoCorrelativo,D.Fecha as FechaAceptacion,G.Descripcion,Case When A.CodRegimen IN('5600','5100','5121') Then '0' Else A.[ISV] End as ISV";
        sql += " ,Case When A.CodRegimen IN('5600','5100','5121') Then '0' Else A.[DAI] End as DAI,Case When A.CodRegimen IN('5600','5100','5121') Then '0' Else A.[Selectivo] End as Selectivo";
        sql += " ,Case When A.CodRegimen IN('5600','5100','5121') Then '0' Else A.[STD] End as STD,Case When A.CodRegimen IN('5600','5100','5121') Then '0' Else A.[ProduccionConsumo]End as Produccion";
        sql += " ,Case When A.CodRegimen IN('5600','5100','5121') Then '0' Else A.[OtrosImpuestos]End as OtrosImpuestos,Case When A.CodRegimen IN('5600','5100','5121') Then '0' Else A.[Impuesto]End as Total";
        sql += " ,Case When A.CodRegimen NOT IN('5600','5100','5121') Then '0' Else A.[ISV] End as ISVEXONERADOS";
        sql += " ,Case When A.CodRegimen NOT IN('5600','5100','5121') Then '0' Else A.[DAI] End as DAIEXONERADOS ,Case When A.CodRegimen NOT  IN('5600','5100','5121') Then '0' Else A.[Selectivo] End as SelectivoEXONERADOS ";
        sql += " ,Case When A.CodRegimen NOT IN('5600','5100','5121') Then '0' Else A.[STD]End as STDEXONERADOS ,Case When A.CodRegimen NOT IN('5600','5100','5121') Then '0' Else A.[ProduccionConsumo]End as ProduccionEXONERADOS ";
        sql += " ,Case When A.CodRegimen NOT IN('5600','5100','5121') Then '0' Else A.[OtrosImpuestos]End as OtrosImpuestosEXONERADOS,Case When A.CodRegimen NOT IN('5600','5100','5121') Then '0' Else A.[Impuesto]End as TotalEXONERADOS";
        sql += " ,A.[IdInstruccion] FROM Instrucciones A";
        sql += " Inner Join Aduanas B on (A.CodigoAduana = B.CodigoAduana and B.CodEstado = 0)";
        sql += " Left Join [EsquemaTramites2].[dbo].[HojaRutaPreliquidacionDet] C on (A.IdInstruccion = C.IdHojaRuta collate Modern_Spanish_CI_AI)";
        sql += " Left Join FlujoCarga D on (D.CodEstadoFlujoCarga = '1' and D.Eliminado = 0 and A.CodRegimen = D.CodRegimen and A.CodPaisHojaRuta = D.CodPais)";
        sql += " Left Join GestionCarga E on (D.IdEstadoFlujo = E.IdEstadoFlujo and E.IdInstruccion = A.IdInstruccion and A.CodEstado != 100)";
        sql += " Inner Join Codigos F on (F.Codigo = A.CodPaisHojaRuta and F.Categoria = 'PAISES')";
        sql += " Inner Join Codigos G on (G.Codigo = A.CodRegimen and G.Categoria = 'REGIMEN'+F.Descripcion)";
        sql += " Left Join [EsquemaTramites2].[dbo].[FactorCambio] H on (convert(Varchar ,A.Fecha,103) = Convert(Varchar,H.Fecha,103))";
        sql += " Where CONVERT(VARCHAR, A.Fecha, 111) >= '" + fechainicio + "' AND CONVERT(VARCHAR, A.Fecha, 111) <= '" + fechafin + "'";
        sql += " and A.IdCliente = " + cliente + " Order By A.Item desc";
        this.loadSQL(sql);
    }
    public void ReporteHagamoda2Pagina1(string fechainicio, string fechafin, string cliente, string pais)
    {
        string sql = "SELECT A.IdInstruccion as HojaRuta,Proveedor as Proveedor,B.Nombre as Cliente,C.DocumentoNo as GuiaAerea,NumeroFactura as FacturaComercial";
        sql += " ,CantidadBultos as Bultos,PesoKgs as KGSOrigen,D.ObservacionFin as PesoMiami,E.ObservacionFin as PesoIngresoSwissport";
        sql += " ,F.ObservacionFin as PesoSalidaSwissport,A.Producto as Producto,A.Fecha as ETA,G.FechaInicio as ATA,H.FechaInicio as PreLiquidacion";
        sql += " ,H.FechaFin as AprovacionPreLiquidacion,I.FechaInicio as Liquidacion,GC1.Fecha as PagoLiquidacion,A.Color as Canal,";
        sql += " GC2.Fecha as FechaLiberacion,GC3.Fecha as FechaDespacho,GC4.Fecha as FechaEntrega,J.ObservacionGeneral as Comentarios";
        sql += " ,case When A.IdEstadoFlujo = '0' Then 'Comienzo Flujo' When A.IdEstadoFlujo = '99' Then 'Tramite Finalizado' When A.IdEstadoFlujo = '98'";
        sql += " Then 'Habilitada Temporalmente' Else K.EstadoFlujo end as EstadoActual, L.FechaInicio as IngresoDeposito ,U2.Nombre +' '+U2.Apellido  as Aforador";
        sql += " ,'Dias: ' + CAST(DATEDIFF(SECOND, GC.Fecha, GC4.Fecha) / 86400 AS varchar(50)) + ' Horas: ' + CAST((DATEDIFF(SECOND, GC.Fecha, GC4.Fecha) % 86400) / 3600";
        sql += " AS varchar(50)) + ' Minutos: ' + CAST((DATEDIFF(SECOND, GC.Fecha, GC4.Fecha) % 3600) / 60 AS varchar(50)) AS TiempoProceso";
        sql += " ,'Dias: ' +  CAST(DATEDIFF(SECOND, G.FechaInicio, GC4.Fecha) / 86400 AS varchar(50)) + ' Horas: ' + CAST((DATEDIFF(SECOND, G.FechaInicio, GC4.Fecha) % 86400) / 3600";
        sql += " AS varchar(50)) +' Minutos: ' + CAST((DATEDIFF(SECOND, G.FechaInicio, GC4.Fecha) % 3600) / 60 AS varchar(50)) AS TiempoProceso2";
        sql += " FROM [AduanasNueva].[dbo].[Instrucciones] A ";
        sql += " INNER JOIN Clientes B on (A.IdCliente = B.CodigoSAP)";
        sql += " LEFT JOIN DocumentosInstrucciones C on (A.IdInstruccion = C.IdInstruccion and C.CodDocumento in (51,52,53) AND C.Eliminado = 0 )";
        sql += " LEFT JOIN Eventos D on (A.IdInstruccion = D.IdInstruccion and D.IdEvento = 19)";
        sql += " LEFT JOIN Eventos E on (A.IdInstruccion = E.IdInstruccion and E.IdEvento = 20)";
        sql += " LEFT JOIN Eventos F on (A.IdInstruccion = F.IdInstruccion and F.IdEvento = 21)";
        sql += " LEFT JOIN Eventos G on (A.IdInstruccion = G.IdInstruccion and G.IdEvento = 1)";
        sql += " LEFT JOIN Eventos H on (A.IdInstruccion = H.IdInstruccion and H.IdEvento = 7)";
        sql += " LEFT JOIN Eventos I on (A.IdInstruccion = I.IdInstruccion and I.IdEvento = 18)";
        sql += " LEFT JOIN Eventos L on (A.IdInstruccion = L.IdInstruccion and L.IdEvento = 22)";
        sql += " LEFT JOIN FlujoCarga FC1 on (FC1.CodRegimen = A.CodRegimen AND FC1.CodPais = A.CodPaisHojaRuta AND FC1.CodEstadoFlujoCarga = '2' AND FC1.Eliminado = '0')";
        sql += " LEFT JOIN GestionCarga GC1 on (GC1.IdInstruccion = A.IdInstruccion AND GC1.IdEstadoFlujo = FC1.IdEstadoFlujo AND GC1.Eliminado = '0')";
        sql += " LEFT JOIN FlujoCarga FC2 on (FC2.CodRegimen = A.CodRegimen AND FC2.CodPais = A.CodPaisHojaRuta AND FC2.CodEstadoFlujoCarga = '4' AND FC2.Eliminado = '0')";
        sql += " LEFT JOIN GestionCarga GC2 on (GC2.IdInstruccion = A.IdInstruccion AND GC2.IdEstadoFlujo = FC2.IdEstadoFlujo AND GC2.Eliminado = '0')";
        sql += " LEFT JOIN FlujoCarga FC3 on (FC3.CodRegimen = A.CodRegimen AND FC3.CodPais = A.CodPaisHojaRuta AND FC3.CodEstadoFlujoCarga = '5' AND FC3.Eliminado = '0')";
        sql += " LEFT JOIN GestionCarga GC3 on (GC3.IdInstruccion = A.IdInstruccion AND GC3.IdEstadoFlujo = FC3.IdEstadoFlujo AND GC3.Eliminado = '0')";
        sql += " LEFT JOIN FlujoCarga FC4 on (FC4.CodRegimen = A.CodRegimen AND FC4.CodPais = A.CodPaisHojaRuta AND FC4.CodEstadoFlujoCarga = '6' AND FC4.Eliminado = '0')";
        sql += " LEFT JOIN GestionCarga GC4 on (GC4.IdInstruccion = A.IdInstruccion AND GC4.IdEstadoFlujo = FC4.IdEstadoFlujo AND GC4.Eliminado = '0')";
        sql += " LEFT JOIN GestionCarga GC ON (GC.IdInstruccion = A.IdInstruccion AND GC.IdEstadoFlujo = '0'  AND GC.Eliminado = '0')";
        sql += " LEFT JOIN DatosGestionDocumentos J on (A.IdInstruccion = J.IdInstruccion AND J.Eliminado = 0 )";
        sql += " LEFT JOIN FlujoCarga K on (K.CodRegimen = A.CodRegimen AND K.CodPais = A.CodPaisHojaRuta AND K.IdEstadoFlujo = A.IdEstadoFlujo AND FC4.Eliminado = '0')";
        sql += " LEFT JOIN Usuarios U2 ON (U2.IdUsuario = A.Aforador)";
        sql += " Where CONVERT(VARCHAR, A.Fecha, 111) >= '" + fechainicio + "' AND CONVERT(VARCHAR, A.Fecha, 111) <= '" + fechafin + "'";
        sql += " AND A.CodEstado !=100 AND B.Eliminado = 0 AND A.IdEstadoFlujo not in ('98','99') ";
        sql += " AND A.IdCliente = " + cliente + " AND A.CodPaisHojaRuta ='" + pais + "' Order By A.Item desc";
        this.loadSQL(sql);
    }
    public void ReporteHagamoda2Pagina2(string fechainicio, string fechafin, string cliente, string pais)
    {
        string sql = "SELECT A.IdInstruccion as HojaRuta,Proveedor as Proveedor,B.Nombre as Cliente,C.DocumentoNo as GuiaAerea,NumeroFactura as FacturaComercial";
        sql += " ,CantidadBultos as Bultos,PesoKgs as KGSOrigen,D.ObservacionFin as PesoMiami,E.ObservacionFin as PesoIngresoSwissport";
        sql += " ,F.ObservacionFin as PesoSalidaSwissport,A.Producto as Producto,A.Fecha as ETA,G.FechaInicio as ATA,H.FechaInicio as PreLiquidacion";
        sql += " ,H.FechaFin as AprovacionPreLiquidacion,I.FechaInicio as Liquidacion,GC1.Fecha as PagoLiquidacion,A.Color as Canal,";
        sql += " GC2.Fecha as FechaLiberacion,GC3.Fecha as FechaDespacho,GC4.Fecha as FechaEntrega,J.ObservacionGeneral as Comentarios";
        sql += " ,case When A.IdEstadoFlujo = '0' Then 'Comienzo Flujo' When A.IdEstadoFlujo = '99' Then 'Tramite Finalizado' When A.IdEstadoFlujo = '98'";
        sql += " Then 'Habilitada Temporalmente' Else K.EstadoFlujo end as EstadoActual, L.FechaInicio as IngresoDeposito ,U2.Nombre +' '+U2.Apellido  as Aforador";
        sql += " ,'Dias: ' + CAST(DATEDIFF(SECOND, GC.Fecha, GC4.Fecha) / 86400 AS varchar(50)) + ' Horas: ' + CAST((DATEDIFF(SECOND, GC.Fecha, GC4.Fecha) % 86400) / 3600";
        sql += " AS varchar(50)) + ' Minutos: ' + CAST((DATEDIFF(SECOND, GC.Fecha, GC4.Fecha) % 3600) / 60 AS varchar(50)) AS TiempoProceso";
        sql += " ,'Dias: ' +  CAST(DATEDIFF(SECOND, G.FechaInicio, GC4.Fecha) / 86400 AS varchar(50)) + ' Horas: ' + CAST((DATEDIFF(SECOND, G.FechaInicio, GC4.Fecha) % 86400) / 3600";
        sql += " AS varchar(50)) +' Minutos: ' + CAST((DATEDIFF(SECOND, G.FechaInicio, GC4.Fecha) % 3600) / 60 AS varchar(50)) AS TiempoProceso2";
        sql += " , Z.ETAPlanta as ETS,Z.FechaArriboPlantaProceso as EntregaEnTienda, Z.FacturaVESTA as FacturaVEsta";
        sql += " FROM [AduanasNueva].[dbo].[Instrucciones] A ";
        sql += " INNER JOIN Clientes B on (A.IdCliente = B.CodigoSAP)";
        sql += " LEFT JOIN DocumentosInstrucciones C on (A.IdInstruccion = C.IdInstruccion and C.CodDocumento in (51,52,53) AND C.Eliminado = 0 )";
        sql += " LEFT JOIN Eventos D on (A.IdInstruccion = D.IdInstruccion and D.IdEvento = 19)";
        sql += " LEFT JOIN Eventos E on (A.IdInstruccion = E.IdInstruccion and E.IdEvento = 20)";
        sql += " LEFT JOIN Eventos F on (A.IdInstruccion = F.IdInstruccion and F.IdEvento = 21)";
        sql += " LEFT JOIN Eventos G on (A.IdInstruccion = G.IdInstruccion and G.IdEvento = 1)";
        sql += " LEFT JOIN Eventos H on (A.IdInstruccion = H.IdInstruccion and H.IdEvento = 7)";
        sql += " LEFT JOIN Eventos I on (A.IdInstruccion = I.IdInstruccion and I.IdEvento = 18)";
        sql += " LEFT JOIN Eventos L on (A.IdInstruccion = L.IdInstruccion and L.IdEvento = 22)";
        sql += " LEFT JOIN FlujoCarga FC1 on (FC1.CodRegimen = A.CodRegimen AND FC1.CodPais = A.CodPaisHojaRuta AND FC1.CodEstadoFlujoCarga = '2' AND FC1.Eliminado = '0')";
        sql += " LEFT JOIN GestionCarga GC1 on (GC1.IdInstruccion = A.IdInstruccion AND GC1.IdEstadoFlujo = FC1.IdEstadoFlujo AND GC1.Eliminado = '0')";
        sql += " LEFT JOIN FlujoCarga FC2 on (FC2.CodRegimen = A.CodRegimen AND FC2.CodPais = A.CodPaisHojaRuta AND FC2.CodEstadoFlujoCarga = '4' AND FC2.Eliminado = '0')";
        sql += " LEFT JOIN GestionCarga GC2 on (GC2.IdInstruccion = A.IdInstruccion AND GC2.IdEstadoFlujo = FC2.IdEstadoFlujo AND GC2.Eliminado = '0')";
        sql += " LEFT JOIN FlujoCarga FC3 on (FC3.CodRegimen = A.CodRegimen AND FC3.CodPais = A.CodPaisHojaRuta AND FC3.CodEstadoFlujoCarga = '5' AND FC3.Eliminado = '0')";
        sql += " LEFT JOIN GestionCarga GC3 on (GC3.IdInstruccion = A.IdInstruccion AND GC3.IdEstadoFlujo = FC3.IdEstadoFlujo AND GC3.Eliminado = '0')";
        sql += " LEFT JOIN FlujoCarga FC4 on (FC4.CodRegimen = A.CodRegimen AND FC4.CodPais = A.CodPaisHojaRuta AND FC4.CodEstadoFlujoCarga = '6' AND FC4.Eliminado = '0')";
        sql += " LEFT JOIN GestionCarga GC4 on (GC4.IdInstruccion = A.IdInstruccion AND GC4.IdEstadoFlujo = FC4.IdEstadoFlujo AND GC4.Eliminado = '0')";
        sql += " LEFT JOIN GestionCarga GC ON (GC.IdInstruccion = A.IdInstruccion AND GC.IdEstadoFlujo = '0'  AND GC.Eliminado = '0')";
        sql += " LEFT JOIN DatosGestionDocumentos J on (A.IdInstruccion = J.IdInstruccion AND J.Eliminado = 0 )";
        sql += " LEFT JOIN FlujoCarga K on (K.CodRegimen = A.CodRegimen AND K.CodPais = A.CodPaisHojaRuta AND K.IdEstadoFlujo = A.IdEstadoFlujo AND FC4.Eliminado = '0')";
        sql += " LEFT JOIN Usuarios U2 ON (U2.IdUsuario = A.Aforador)";
        sql += " LEFT JOIN [PortalClientes].[dbo].[HCC] Z on (A.IdInstruccion = Z.IdInstruccion)";
        sql += " Where CONVERT(VARCHAR, A.Fecha, 111) >= '" + fechainicio + "' AND CONVERT(VARCHAR, A.Fecha, 111) <= '" + fechafin + "'";
        sql += " AND A.CodEstado !=100 AND B.Eliminado = 0 AND A.IdEstadoFlujo in ('98','99') ";
        sql += " AND A.IdCliente = " + cliente + " AND A.CodPaisHojaRuta ='" + pais + "' Order By A.Item desc";
        this.loadSQL(sql);
    }
    public void ReporteDinant(string fechainicio, string fechafin, string cliente, string pais)
    {
        string sql = @"Select B.NombreAduana as Aduana, A.ProductosCargill as TipoImportacion, G.Descripcion as Division, A.IdInstruccion as HojaRuta,A.ReferenciaCliente as Ordenes, A.Proveedor as Proveedor, A.Producto as Producto,
                 A.ETA as ETA,F.Fecha as ArriboCarga,  H.ETAPlanta as ETAPlanta, A.NumeroFactura as NumeroFactura, C.Descripcion as PaisOrigen,D.Empresa as 'Naviera/Courier',
         A.DescripcionContenedores as #Contenedor,E.DocumentoNo as 'BL/Guia/CartaPorte',A.CantidadBultos as CantidadBultos ,A.PesoKgs as PesoKilogramos,
         H.FechaReciboCopias as ReciboCopiasEmail, H.FechaReciboOriginales as ReciboOriginales, D.ObservacionGeneral as Comentario,H.Observaciones as Comentarios2,
         Case When A.IdEstadoFlujo = '99' Then 'Verde' When A.ArriboCarga = 0 Then 'Azul' Else 'Amarillo' End as Color
         ,Case When A.IdEstadoFlujo = '0' Then 'Comienzo Flujo' When A.IdEstadoFlujo = '99' Then 'Tramite Finalizado' When A.IdEstadoFlujo = '98' Then 'Habilitada Temporalmente' Else K.EstadoFlujo end as EstadoActual
         From Instrucciones A
         Inner Join Aduanas B on (A.CodigoAduana = B.CodigoAduana and A.CodEstado != 100 and B.CodEstado = 0)
         Inner Join Codigos C on (Categoria = 'PAISES' and C.Codigo = A.CodPaisOrigen and C.Eliminado = 0)
         Left Join DatosGestionDocumentos D on ( A.IdInstruccion = D.IdInstruccion and D.Eliminado = 0)
         Left Join DocumentosInstrucciones E on (A.IdInstruccion = E.IdInstruccion and E.Eliminado = 0 and E.CodDocumento in (50,51,52))
         Left Join GestionCarga F on (A.IdInstruccion = F.IdInstruccion and F.IdEstadoFlujo = '0' and F.Eliminado = 0 )
         Left Join Codigos G on (G.Categoria = A.IdCliente and C.Eliminado = 0 and A.Division = G.Codigo)
         Left Join [PortalClientes].[dbo].[HCC] H on (A.IdInstruccion = H.IdInstruccion)
         Left Join FlujoCarga K on (K.CodRegimen = A.CodRegimen AND K.CodPais = A.CodPaisHojaRuta AND K.IdEstadoFlujo = A.IdEstadoFlujo)
         Where CONVERT(VARCHAR, A.Fecha, 111) >= '" + fechainicio + "' AND CONVERT(VARCHAR, A.Fecha, 111) <= '" + fechafin + "'and  A.IdCliente = '" + cliente + "' and A.CodPaisHojaRuta = '" + pais + "'";
        sql +=  @"and A.CodRegimen in ('4000','4030','4050','4051','4053','4054','4057','4059','4070','4100','4200','4600','5000','5070','5100','5121','5170','5200','5270'
         ,'5300','5400','5470','5500','5600','5670','5700','5770','5800','5900','5970','6010','6020','6021','6022','6120','6121','7000','7054','7070','8000','8070'
         ,'8100','8170','8900','8970')
          OR A.IdCliente = '" + cliente + "' and A.CodPaisHojaRuta = '" + pais + "' and A.IdEstadoFlujo != '99' and dateadd(MONTH,4,ISNULL(A.[Fecha], GETDATE ())) > = '" + fechainicio + "' and A.CodRegimen in ('4000','4030','4050','4051','4053','4054','4057','4059','4070','4100','4200','4600','5000','5070','5100','5121','5170','5200','5270'";
        sql += @" ,'5300','5400','5470','5500','5600','5670','5700','5770','5800','5900','5970','6010','6020','6021','6022','6120','6121','7000','7054','7070','8000','8070','8100','8170','8900','8970')";
        sql += @"OR A.IdCliente = '" + cliente + "' and A.CodPaisHojaRuta = '" + pais +
               "' and A.IdEstadoFlujo = '99' AND CONVERT(VARCHAR,DATEADD(MONTH,1,A.FechaFinalFlujo), 111) >= '" +
               fechainicio + "' AND CONVERT(VARCHAR, DATEADD(MONTH,1,A.FechaFinalFlujo), 111) <= '" + fechafin + "'";
        sql += @" and A.CodRegimen in ('4000','4030','4050','4051','4053','4054','4057','4059','4070','4100','4200','4600','5000','5070','5100','5121','5170','5200','5270'
        ,'5300','5400','5470','5500','5600','5670','5700','5770','5800','5900','5970','6010','6020','6021','6022','6120','6121','7000','7054','7070','8000','8070'
         ,'8100s','8170','8900','8970') Order by A.IdInstruccion";
        this.loadSQL(sql);
    }
    public void ReporteEventosGenstion(string Pais, string Aduana, string FechaInicio, string FechaFin)
    {
        string sql = @"Select A.IdInstruccion,C.NombreAduana,E.Nombre as Cliente,D.Nombre+' '+D.Apellido as Nombre,B.Fecha,FechaIngreso
                    ,round((convert(money,DateDiff(Minute,B.Fecha,FechaIngreso)))/60,2) as Diferencia,'' as FechaFin, '' as FechaFinIngreso,'' as Diferencia2
                    ,case Estado When 0 Then 'Comienzo Flujo' When 1 Then 'Validación Electrónica' When 2 Then 'Pago de Impuestos' When 3 Then 'Asignación Canal de Selectividad (Colores)' When 4 Then
                    'Revisión de Mercancía (Inicio)' When 5 Then 'Emisión de Pase de Salida' Else 'Entrega de Servicio' end as 'Flujo/Evento','Gestion' as Informacion
                    From Instrucciones A
                    Inner Join GestionCarga B on (A.IdInstruccion = B.IdInstruccion and A.IdEstadoFlujo != '100')
                    Inner Join Aduanas C on (C.CodigoAduana = A.CodigoAduana)
                    Inner Join Usuarios D on (B.IdUsuario = D.IdUsuario)
                    Inner Join Clientes E on (A.IdCliente = E.CodigoSAP)
                    Where B.Eliminado = 0 and A.CodPaisHojaRuta = '" + Pais + "' and A.CodigoAduana = '" + Aduana + "' and B.FechaIngreso Between '" + FechaInicio + "' and '" + FechaFin + "'";
        sql += @"Union
                    Select A.IdInstruccion,D.NombreAduana,F.Nombre,E.Nombre+' '+E.Apellido as Nombre,B.FechaInicio, B.FechaInicioIngreso
                    ,round((convert(money,DateDiff(Minute,B.FechaInicio, B.FechaInicioIngreso)))/60,2) as Diferencia
                    ,B.FechaFin, B.FechaFinIngreso
                    ,round((convert(money,DateDiff(Minute,B.FechaFin, B.FechaFinIngreso)))/60,2) as Diferencia
                    ,C.Descripcion,'Evento'
                    From Instrucciones A
                    Inner Join Eventos B on (A.IdInstruccion = B.IdInstruccion and A.IdEstadoFlujo != '100')
                    Inner Join Codigos C on (C.Categoria = 'EVENTOS' and C.Codigo= B.IdEvento)
                    Inner Join Aduanas D on (D.CodigoAduana = A.CodigoAduana)
                    Inner Join Usuarios E on (B.IdUsuarioInicio = E.IdUsuario)
                    Inner Join Clientes F on (A.IdCliente = F.CodigoSAP)
                    Where A.CodPaisHojaRuta = '" + Pais + "' and A.CodigoAduana = '" + Aduana + "' and (B.FechaInicioIngreso Between '" + FechaInicio + "' and '" + FechaFin + "' or B.FechaFinIngreso Between '" + FechaInicio + "' and '" + FechaFin + "')";
        this.loadSQL(sql);
    }
    public void ReporteSeguimientoCalidad(string FechaInicio, string FechaFin, string Pais, string Aduana)
    {
        string sql = @"Select I.IdInstruccion,CL.Nombre as Cliente,I.Producto,C.Descripcion as Tipocargamento,I.CodRegimen,E1.FechaInicio as ArriboCarga
                    ,E2.FechaInicio as Manifiesto,GC.Fecha as DocumentosMinimos,GC1.Fecha as ValidacionElectronica,GC2.Fecha as PagoImpuesto,GC3.Fecha as AsignacionCanalSelectividad
                    ,I.Color as Color,E3.FechaInicio as CitaOPC,GC4.Fecha as RevisionMercancia,GC5.Fecha as EmisionPaseSalida,E4.FechaInicio as GatePass,GC6.Fecha as EntregaServicio
                    ,DGD.ObservacionGeneral,U.Nombre+' '+U.Apellido as Gestor,cast(Datediff(Hour,E1.FechaInicio,ISNULL(GC6.Fecha,GETDATE())) as Money)/24 as TiempoTotalProceso
                    ,E5.FechaInicio as EnvioDocAduana,E6.FechaInicio as RecepcionDocAduana,E7.FechaInicio as EnvioDocCentralizacion,E8.FechaInicio as RecepcionDocCentralizacion
                    From Instrucciones I
                    Inner Join Clientes CL on (I.IdCliente = CL.CodigoSAP and I.CodEstado != '100')
                    Inner Join Codigos C on (C.Categoria = 'TIPOCARGAMENTO' and C.Codigo = I.CodTipoCargamento)
                    Inner Join Usuarios U on (U.IdUsuario = I.IdUsuarioAduana)
                    Left Join Eventos E1 on (E1.IdEvento = 1 and E1.IdInstruccion = I.IdInstruccion)
                    Left Join Eventos E2 on (E2.IdEvento = 37 and E2.IdInstruccion = I.IdInstruccion)
                    Left Join GestionCarga GC on (GC.Estado = 0 and GC.IdInstruccion = I.IdInstruccion)
                    Left Join GestionCarga GC1 on (GC1.Estado = 1 and GC1.IdInstruccion = I.IdInstruccion)
                    Left Join GestionCarga GC2 on (GC2.Estado = 2 and GC2.IdInstruccion = I.IdInstruccion)
                    Left Join GestionCarga GC3 on (GC3.Estado = 3 and GC3.IdInstruccion = I.IdInstruccion)
                    Left Join Eventos E3 on (E3.IdEvento = 35 and E3.IdInstruccion = I.IdInstruccion)
                    Left Join GestionCarga GC4 on (GC4.Estado = 4 and GC4.IdInstruccion = I.IdInstruccion)
                    Left Join GestionCarga GC5 on (GC5.Estado = 5 and GC5.IdInstruccion = I.IdInstruccion)
                    Left Join Eventos E4 on (E4.IdEvento = 33 and E4.IdInstruccion = I.IdInstruccion)
                    Left Join GestionCarga GC6 on (GC6.Estado = 6 and GC6.IdInstruccion = I.IdInstruccion)
                    Left Join DatosGestionDocumentos DGD on (DGD.IdInstruccion = I.IdInstruccion and DGD.Eliminado = 0)
                    Left Join Eventos E5 on (E5.IdEvento = 43 and E5.IdInstruccion = I.IdInstruccion)
                    Left Join Eventos E6 on (E6.IdEvento = 44 and E6.IdInstruccion = I.IdInstruccion)
                    Left Join Eventos E7 on (E7.IdEvento = 45 and E7.IdInstruccion = I.IdInstruccion)
                    Left Join Eventos E8 on (E8.IdEvento = 46 and E8.IdInstruccion = I.IdInstruccion)
                    Where I.CodPaisHojaRuta = '" + Pais + "' and I.CodigoAduana = '" + Aduana + "' and (GC6.Fecha between '" + FechaInicio + "' and '" + FechaFin + "' or (DateAdd(Month,6,I.Fecha) >= GETDATE() and I.IdEstadoFlujo not in ('99','98')))";
        this.loadSQL(sql);
    }
    public void VerificarEventosCentralizacionAforo(string IdInstruccion)
    {
        string sql = @"Select A.IdInstruccion, A.IdEstadoFlujo, B.IdEvento,B.FechaInicio,A.CodigoAduana From Instrucciones A
                        Inner Join Eventos B on (A.IdInstruccion = B.IdInstruccion) 
                        Where A.IdInstruccion = '" + IdInstruccion + "' and CodEstado != 100 and B.IdEvento in (43,44,45,46) order by B.IdEvento";
        this.loadSQL(sql);
    }
    public void VerificarAduanaRevision(string IdInstruccion)
    {
        string sql = @"Select A.IdInstruccion
                        From Instrucciones A
                        Inner Join Aduanas B on (A.CodigoAduana = B.CodigoAduana and Revision = 1)
                        Where A.IdInstruccion = '" + IdInstruccion + "' and A.CodEstado != 100";
        this.loadSQL(sql);
    }



    public void Consulta_Gastos(string Fecha, string FechaF)
    {

        string sql = @" SELECT DISTINCT I.IdInstruccion,DI1.DocumentoNo AS Factura,CL.Nombre as Cliente,I.Producto,I.NoCorrelativo,GC.Fecha AS EntregadeServicio,DG.Observacion
                            FROM Instrucciones I  INNER JOIN Clientes CL ON (CL.CodigoSAP = I.IdCliente AND CL.Eliminado = '0' and  CL.CodigoSAP='700121')  
                            INNER JOIN Aduanas A ON (A.CodigoAduana = I.CodigoAduana AND A.CodEstado = '0') 
                            LEFT JOIN DocumentosInstrucciones DI1 ON (DI1.IdInstruccion = I.IdInstruccion AND DI1.CodDocumento = '1' AND DI1.Eliminado ='0')
                            INNER JOIN Codigos C ON (C.Codigo = I.CodPaisHojaRuta AND C.Categoria = 'PAISES') 
                            LEFT JOIN GestionCarga GC ON (GC.IdInstruccion = I.IdInstruccion AND GC.Estado = 6 and GC.Eliminado = 0)
                            LEFT JOIN EspeciesFiscalesInstrucciones EF ON (EF.IdInstruccion = I.IdInstruccion 
                            AND EF.CodEspecieFiscal in('HN-MI','HN-MN','SS')AND EF.Necesario = 'X') 
                            LEFT JOIN Codigos C5 on (EF.CodEspecieFiscal = C5.Codigo and C5.Categoria = 'ESPECIESFISCALES')
                            left Join DetalleGastos DG on ( DG.IdInstruccion=I.IdInstruccion   and DG.IdGasto='11')

                            WHERE I.CodEstado IN ('0','1') AND I.CodPaisHojaRuta like '%N%' AND  I.Fecha between '" + Fecha + "' and '" + FechaF + "'";

        this.loadSQL(sql);
    }

    public void ReporteCerveceriaHondureña(string fecha, string fechaFin, string codPais, string cliente)
    {

        //string sql = @"select Distinct i.IdInstruccion, a.NombreAduana, c.Nombre, i.Proveedor, i.Producto, r.Descripcion,  di.DocumentoNo,
        //                cd.Descripcion as Origen, i.Fecha, t.Fecha as [FechaLiquidacion],  i.NumeroFactura as Factura, i.NoCorrelativo,
        //                dbo.TraerPos(i.IdInstruccion) as PosArancelaria, i.Flete, i.PesoKgs,  i.Impuesto, i.ValorFOB, i.ValorCIF,  i.Seguro, i.Otros,
        //                ia.Factura as Factura1, ia.FechaFactura as FechaFactura, ia.NotasRembolso as NotasRembolso , ia.FechaEntregaFactura as FechaEntregaFactura, ia.PedidoCliente as PedidoCliente
        //                from Instrucciones i
        //                inner join Clientes c on i.IdCliente = c.CodigoSAP
        //                inner join Aduanas a on i.CodigoAduana = a.CodigoAduana
        //                inner join RegimenesHonduras r on i.CodRegimen = r.Codigo
        //                inner join Codigos cd on i.CodPaisOrigen = cd.Codigo and cd.Categoria = 'PAISES'
        //                left join TiemposFlujoCarga t on i.IdInstruccion = t.IdInstruccion and t.IdEstado = 12
        //                left join DocumentosInstrucciones di on i.IdInstruccion = di.IdInstruccion and di.CodDocumento in(50, 52, 51)
        //                left join InstruccionesAnexo ia on i.IdInstruccion = ia.Idinstruccion and ia.Eliminado = 0 
        //                where i.Fecha >=' " + fecha + " 'and i.Fecha <='" + fechaFin + "' and i.CodPaisHojaRuta = '" + codPais + " 'and i.IdCliente = '" + cliente + "'and i.CodEstado not in (100)";

        string sql = @"select Distinct   i.IdInstruccion, a.NombreAduana, c.Nombre, i.Proveedor, i.Producto, r.Descripcion,  di.DocumentoNo,
                        cd.Descripcion as Origen, i.Fecha, t.Fecha as [FechaLiquidacion],  i.NumeroFactura as Factura, i.NoCorrelativo,
                        --dbo.TraerPos(i.IdInstruccion) as PosArancelaria,
                        ZZ.PosArancelaria as PosArancelaria,
                        i.Flete, i.PesoKgs, isnull(cb1.TotalLps, i.Impuesto) as ISV, i.ValorFOB, i.ValorCIF,  i.Seguro, i.Otros,
                        ia.Factura as Factura1,/* ia.FechaFactura as FechaFactura,ia.NotasRembolso as NotasRembolso ,*/ ia.FechaEntregaFactura as FechaEntregaFactura, ia.PedidoCliente as PedidoCliente,
                        ISNULL(t1.DVA, 0) as [D.V.A], ISNULL(t2.Polizas, 0) as Polizas ,ISNULL(t3.DA, 0) as ExtensionesPoliza, ISNULL(t4.MN, 0) as MarchamosNacionales,
                        ISNULL(t5.MI, 0) as MarchamosInternacionales, ISNULL(t6.S, 0) as SellosDeSeguridad
                        from Instrucciones i
                        inner join Clientes c on i.IdCliente = c.CodigoSAP
                        inner join Aduanas a on i.CodigoAduana = a.CodigoAduana
                        inner join RegimenesHonduras r on i.CodRegimen = r.Codigo
                        inner join Codigos cd on i.CodPaisOrigen = cd.Codigo and cd.Categoria = 'PAISES'
                        LEFT join TiemposFlujoCarga t on i.IdInstruccion = t.IdInstruccion and t.IdEstado = '12' and t.Eliminado = '0'
                        LEFT join DocumentosInstrucciones di on i.IdInstruccion = di.IdInstruccion and di.CodDocumento in(50, 52, 51)
                        LEFT join InstruccionesAnexo ia on i.IdInstruccion = ia.Idinstruccion and ia.Eliminado = 0 
						left join [EsquemaTramites2].[dbo].[ConceptoBoletin] cb1 on (i.IdInstruccion = cb1.IdInstruccion collate Modern_Spanish_CS_AS and cb1.TipoImpuesto = 'ISV' and cb1.Eliminado = 0)
                          Left Join (Select distinct IdInstruccion, substring( (
                            Select ' | '+sub.PosicionArancelaria  AS [text()]
                            From DeclaracionItems sub
                            Where sub.IdInstruccion = zz.IdInstruccion
                            ORDER BY zz.IdInstruccion
                            For XML PATH ('')
                           ), 2, 1000) PosArancelaria
                            From dbo.DeclaracionItems zz) ZZ on ZZ.IdInstruccion = i.IdInstruccion
                         LEFT  JOIN (select  efi.IdInstruccion, ISNULL(((len(efi.Serie) - len(replace(efi.Serie, ',', '')) / len(',')) + 1), 0) as DVA
                           from [EspeciesFiscalesInstrucciones] efi
                           where   efi.CodEspecieFiscal = 'DVA' and efi.Eliminado = 0 and efi.Necesario = 'X'
                          ) t1 on i.IdInstruccion = t1.IdInstruccion
                          LEFT  JOIN
                          (
                           select efi.IdInstruccion, ((len(efi.Serie) - len(replace(efi.Serie, ',', '')) / len(',')) + 1) as 'Polizas'
                           from [EspeciesFiscalesInstrucciones] efi
                           where  efi.CodEspecieFiscal = 'P' and efi.Eliminado = 0 and efi.Necesario ='X'
                          ) t2 on i.IdInstruccion = t2.IdInstruccion
                          LEFT  JOIN 
                          ( select  efi.IdInstruccion, ((len(efi.Serie) - len(replace(efi.Serie, ',', '')) / len(',')) + 1) as 'DA'
                           from [EspeciesFiscalesInstrucciones] efi
                           where  efi.CodEspecieFiscal = 'DA' and efi.Eliminado = 0 and efi.Necesario ='X'
                          ) t3 on i.IdInstruccion = t3.IdInstruccion
                          LEFT  JOIN 
                          ( select efi.IdInstruccion,((len(efi.Serie) - len(replace(efi.Serie, ',', '')) / len(',')) + 1) as 'MN'
                           from [EspeciesFiscalesInstrucciones] efi
                           where  efi.CodEspecieFiscal = 'HN-MN' and efi.Eliminado = 0 and efi.Necesario ='X'
                          ) t4 
                           on i.IdInstruccion = t4.IdInstruccion
                          LEFT  JOIN 
                          ( select  efi.IdInstruccion, ((len(efi.Serie) - len(replace(efi.Serie, ',', '')) / len(',')) + 1) as 'MI'
                           from [EspeciesFiscalesInstrucciones] efi
                           where  efi.CodEspecieFiscal = 'HN-MI' and efi.Eliminado = 0 and efi.Necesario ='X'
                          ) t5
                           on i.IdInstruccion = t5.IdInstruccion 
                           LEFT  JOIN 
                          ( select  efi.IdInstruccion, ((len(efi.Serie) - len(replace(efi.Serie, ',', '')) / len(',')) + 1) as 'S'
                           from [EspeciesFiscalesInstrucciones] efi
                           where  efi.CodEspecieFiscal IN('SSB', 'SS') and efi.Eliminado = 0 and efi.Necesario ='X'
                          ) t6
                          on i.IdInstruccion = t6.IdInstruccion
                          where (i.Fecha between '" + fecha + "' and '" + fechaFin + "' ) and i.CodPaisHojaRuta = '" + codPais + " 'and i.IdCliente = '" + cliente + "'and i.CodEstado not in (100)";
        this.loadSQL(sql);
    }


    public void ReporteGeneral(string codAduana, string regimen, string fechaInicio, string fechaFin, int tipoAduana, int buscarPor, int buscarPor2, string codCliente, string tipoRegimen )
    {
        string sql = @"select distinct i.IdInstruccion as Instruccion, cl.Nombre as Cliente , i.ReferenciaCliente as [Referencia Cliente], a.NombreAduana as Aduana, i.Proveedor, i.Producto,i.NumeroFactura as Factura,c2.Descripcion as Regimen,
                        i.NoCorrelativo as Correlativo, c1.Descripcion as 'Pais Origen', i.ValorCIF, c3.Descripcion as [Documento Transporte] 
                        ";
        #region If

        if (tipoAduana == 2)
        { 
            {
                sql += @" ,CASE when pf1.CitaOpc = '1' then 'SI' else 'NO' end as [Decision Cita],
                    tc13.Fecha as [Ingreso Cita OPC],
                    tc14.Fecha as [Cita OPC],
                    tc15.Fecha as [Gate Pass] 
                    ";
            }
        }

        if (buscarPor == 1 || buscarPor2 == 1) //sellos de tiempo
        {
            if (tipoRegimen == "I") //Importaciones
            {
                sql += @",ISNULL(e.FechaInicio, tc0.Fecha) as [Arribo Carga],
                        ISNULL(tc1.Fecha, gc0.Fecha) as [Comienzo FLujo],
                        ISNULL(tc2.Fecha, gc1.Fecha) as [Validacion Electronica],
                        tc3.Fecha as [EscaneoBoletin],
                        ISNULL(tc4.Fecha, gc2.Fecha) as [Confirmacion Pago Impuestos],
                        ISNULL(tc5.Fecha, gc3.Fecha) as [Canal Selectividad],
                        i.Color as [Color],
                        tc6.Fecha as [Asignacion Tramitador],
                        ISNULL(tc7.Fecha, gc4.Fecha) [Inicio Revision],
                        tc8.Fecha as  [Fin Revision],
                        tc9.Fecha as [Fumigacion],
                        ISNULL(tc11.Fecha, gc5.Fecha) as [Pase Salida],
                        ISNULL(tc12.Fecha, gc6.Fecha) as [Entrega Documentos] 
                        ";
            }
            else if (tipoRegimen == "E") //Exportaciones
            {
                sql += @",ISNULL(e.FechaInicio, tc0.Fecha) as [Arribo Carga],
                        ISNULL(tc1.Fecha, gc0.Fecha) as [Comienzo FLujo],
                        ISNULL(tc2.Fecha, gc1.Fecha) as [Validacion Electronica],
                        --tc3.Fecha as [EscaneoBoletin],
                        --ISNULL(tc4.Fecha, gc2.Fecha) as [Confirmacion Pago Impuestos],
                        ISNULL(tc5.Fecha, gc3.Fecha) as [Canal Selectividad],
                        i.Color as [Color],
                        --tc6.Fecha as [Asignacion Tramitador],
                        ISNULL(tc7.Fecha, gc4.Fecha) [Inicio Revision],
                        tc8.Fecha as  [Fin Revision],
                        --tc9.Fecha as [Fumigacion],
                        ISNULL(tc11.Fecha, gc5.Fecha) as [Pase Salida],
                        ISNULL(tc12.Fecha, gc6.Fecha) as [Entrega Documentos] 
                        ";
            }
            else if (tipoRegimen == "T")//Transitos
            {
                sql += @",ISNULL(e.FechaInicio, tc0.Fecha) as [Arribo Carga],
                        ISNULL(tc1.Fecha, gc0.Fecha) as [Comienzo FLujo],
                        ISNULL(tc2.Fecha, gc1.Fecha) as [Validacion Electronica],
                        --tc3.Fecha as [EscaneoBoletin],
                        --ISNULL(tc4.Fecha, gc2.Fecha) as [Confirmacion Pago Impuestos],
                        --ISNULL(tc5.Fecha, gc3.Fecha) as [Canal Selectividad],
                        --pf.CanalSelectividad as [Color],
                        --tc6.Fecha as [Asignacion Tramitador],
                        --ISNULL(tc7.Fecha, gc4.Fecha) [Inicio Revision],
                        --tc8.Fecha as  [Fin Revision],
                        --tc9.Fecha as [Fumigacion],
                        ISNULL(tc11.Fecha, gc5.Fecha) as [Pase Salida],
                        ISNULL(tc12.Fecha, gc6.Fecha) as [Entrega Documentos] 
                        ";
            }
        }


        if (buscarPor2 == 2 || buscarPor == 2)//Impuestos
        {
            sql += @" ,ISNULL(cb.TotalLps,0) as [DAI], ISNULL(cb1.TotalLps,0) as [ISV], ISNULL(cb2.TotalLps,0)  as [SEL], ISNULL(cb3.TotalLps,0) as [VIA],
                    ISNULL(cb4.TotalLps,0) as [GAR],ISNULL(cb5.TotalLps,0) as [STD],ISNULL(cb6.TotalLps,0) as [PYC], ISNULL(cb7.TotalLps,0) as [IVG],
                    ISNULL(cb8.TotalLps,0) as [IVZ],
                    (ISNULL(cb.TotalLps,0) + ISNULL(cb1.TotalLps,0) + ISNULL(cb2.TotalLps,0) + ISNULL(cb3.TotalLps,0) + ISNULL(cb4.TotalLps,0) + ISNULL(cb5.TotalLps,0)
                    + ISNULL(cb6.TotalLps,0) + ISNULL(cb7.TotalLps,0) + ISNULL(cb8.TotalLps,0)) AS TOTAL 
                    ";
        }


        if (buscarPor == 3 || buscarPor2 == 3) //Eventos
        {
            sql += @",ISNULL(e01.FechaInicio, tf.Fecha) as [Inicio Arribo Carga], 
		             ISNULL(e01.FechaFin, tf.Fecha) as [Fin Arribo Carga],
                    e1.FechaInicio as [Incio Creacion Manifiesto], e1.FechaFin as [Fin Creacion Manifiesto],
                    e2.FechaInicio as [Inicio Ingreso Manifiesto], e2.FechaFin as [Fin Ingrreso Manifiesto],
                    e3.FechaInicio as [Inicio Envio Docs. Completos y Correctos Aduana], e3.FechaFin as [Fin Envio Docs. Completos y Correctos Aduana],
                    e4.FechaInicio as [Inicio Recepcion Docs. Completos y Correctos Aduana], e4.FechaFin as [Fin Recepcion Docs. Completos y Correctos Aduana],
                    e5.FechaInicio as [Inicio Envio Docs. Completos y Correctos a Aforro], e5.FechaFin as [Fin Envio Docs. Completos y Correctos a Aforro],
                    e6.FechaInicio as [Inicio Recepcion Docs. Completosy Correctos a Aforro], e6.FechaFin as [Fin Recepcion Docs. Completosy Correctos a Aforro] 
                    ";
        }


        if (buscarPor == 4 || buscarPor2 == 4) //Gastos
        {
            sql += @" ,ISNULL(ht.ValorReal, ISNULL(ht11.ValorReal, ISNULL(ht12.ValorReal, ISNULL(ht13.ValorReal, ht14.ValorReal)))) as [Almacenaje],
                    ISNULL(ht1.ValorReal, ht15.ValorReal) as [Manejo Documentos],
                    ht2.ValorReal as [Inspeccion SEPA],
                    ht3.ValorReal as [Complemento Inspeccion SEPA],
                    ht4.ValorReal as [Identificacion PlagaSEPA],
                    ht5.ValorReal as [Revision SEPA],
                    ht6.ValorReal as [Identificacion Plaga SENASA],
                    ht7.ValorReal as [Sobre Estadia],
                    ht8.ValorReal as [Servicio Portuario],
                    ht9.ValorReal as [Servicio Naviero],
                    (ISNULL(ht.ValorReal, ISNULL(ht11.ValorReal, ISNULL(ht12.ValorReal, ISNULL(ht13.ValorReal, ISNULL(ht14.ValorReal, 0))))) + ISNULL(ht1.ValorReal, ISNULL(ht15.ValorReal, 0)) + 
                    ISNULL(ht2.ValorReal, 0) + ISNULL(ht3.ValorReal, 0) + ISNULL(ht4.ValorReal,0) + ISNULL(ht5.ValorReal, 0) + ISNULL(ht6.ValorReal, 0) + ISNULL(ht7.ValorReal, 0) + ISNULL(ht8.ValorReal, 0) +
                    ISNULL(ht9.ValorReal, 0)) as Total 
                    ";
        }
        #endregion

        #region JoinsPrincipales
        //Inner Joins Obligatorios
        sql += @" from Instrucciones i
                  inner join (select cl.CodigoSAP, cl.Nombre, ROW_NUMBER() OVER(PARTITION BY CodigoSAP ORDER BY CodigoCliente DESC) as RN from Clientes cl
				    where cl.CodigoSAP in (" + codCliente +") and cl.Eliminado = '0') cl on i.IdCliente = cl.CodigoSAP and cl.RN = '1' ";
         sql += @"inner join Codigos c1 on (i.CodPaisOrigen = c1.Codigo and c1.Eliminado = '0' and c1.Categoria = 'PAISES')
                inner join (select a.CodigoAduana, a.NombreAduana, ROW_NUMBER() OVER(PARTITION BY CodigoAduana ORDER BY Item) as RN from Aduanas a
				    where a.CodigoAduana in ('" + codAduana +"') and a.CodPais = 'H' and CodEstado = '0') a on i.CodigoAduana = a.CodigoAduana and a.RN = '1' ";
        sql += @"inner join (select c.Codigo, c.Descripcion, ROW_NUMBER() OVER(PARTITION BY Codigo ORDER BY Codigo Desc) as RN 	from Codigos c 
				    where c.Categoria = 'REGIMENHONDURAS' and c.Eliminado = '0'	and (c.Codigo between " + regimen + ")) c2 on i.CodRegimen = c2.Codigo and c2.RN = '1'";
        sql += @"inner join Codigos c3 on (i.CodDocumentoEmbarque = c3.Codigo and c3.Eliminado = 0 and c3.Categoria = 'DOCUMENTOEMBARQUE') 
                 ";
        #endregion

        #region JoinsTiemposFlujo
        if (buscarPor == 1 || buscarPor2 == 1)
        {

            //--------------------------------------------------------------------JOINS Tiempos------------------------------------------------------------------------------------------------    
            sql += @" left join (select e.IdInstruccion, e.FechaInicio, ROW_NUMBER() OVER(PARTITION BY e.IdInstruccion Order By e.Id Desc) as RN from Eventos e
                        where e.IdEvento = '1' and e.Eliminado = '1') e on i.IdInstruccion = e.IdInstruccion and e.RN = '1' --Arribo de carga
                    left join (select tc.IdInstruccion, tc.Fecha, ROW_NUMBER() OVER(PARTITION BY tc.IdInstruccion ORDER BY Id Desc) as RN from TiemposFlujoCarga tc
                        where tc.IdEstado = 14 and tc.Eliminado = 0) tc0 on i.IdInstruccion = tc0.IdInstruccion and tc0.RN = '1' --Arribo Carga
                    left join (select tc.IdInstruccion, tc.Fecha, ROW_NUMBER() OVER(PARTITION BY tc.IdInstruccion ORDER BY tc.Id desc) as RN from TiemposFlujoCarga tc 
                     where tc.IdEstado = 0 and tc.Eliminado = 0) tc1 on i.IdInstruccion = tc1.IdInstruccion and tc1.RN = '1' --ComienzoFlujo
                    left join (select tc.IdInstruccion, tc.Fecha, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id DESC) as RN from TiemposFlujoCarga tc
                        where tc.IdEstado = 12 and tc.Eliminado = 0) tc2 on i.IdInstruccion = tc2.IdInstruccion and tc2.RN = '1' --Validacion Electronica
                    left join (select tc.IdInstruccion, tc.Fecha, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id DESC) as RN from TiemposFlujoCarga tc
                        where tc.IdEstado = 17 and tc.Eliminado = 0) tc3 on i.IdInstruccion = tc3.IdInstruccion and tc3.RN = '1' --Escaneo Boletin
                    left join (select tc.IdInstruccion, tc.Fecha, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id DESC) as RN from TiemposFlujoCarga tc 
                        where tc.IdEstado = 1 and tc.Eliminado = 0) tc4 on i.IdInstruccion = tc4.IdInstruccion and tc4.RN = '1' --Confirmacion pago de impuestos
                    left join (select tc.IdInstruccion, tc.Fecha, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id DESC) as RN from TiemposFlujoCarga tc
                        where tc.IdEstado = 2 and tc.Eliminado = 0) tc5 on i.IdInstruccion = tc5.IdInstruccion and tc5.RN = '1' --Canal de selectividad
                    left join (select tc.IdInstruccion, tc.Fecha, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id DESC) as RN from TiemposFlujoCarga tc
                        where tc.IdEstado = 11 and tc.Eliminado = 0) tc6 on i.IdInstruccion = tc6.IdInstruccion and tc6.RN = '1' --Asignacion de trammitador
                    left join (select tc.IdInstruccion, tc.Fecha, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id DESC) as RN from TiemposFlujoCarga tc
                        where tc.IdEstado = 4 and tc.Eliminado = 0) tc7 on i.IdInstruccion = tc7.IdInstruccion and tc7.RN = '1' --Inicio REvision
                    left join (select tc.IdInstruccion, tc.Fecha, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id DESC) as RN from TiemposFlujoCarga tc
                        where tc.IdEstado = 5 and tc.Eliminado = 0) tc8 on i.IdInstruccion = tc8.IdInstruccion and tc8.RN = '1' --Fin Revision
                    left join (select tc.IdInstruccion, tc.Fecha, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id DESC) as RN from TiemposFlujoCarga tc
                        where tc.IdEstado = 6 and tc.Eliminado = 0) tc9 on i.IdInstruccion = tc9.IdInstruccion and tc9.RN = '1' --Fumigacion
                    left join (select tc.IdInstruccion, tc.Fecha, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id DESC) as RN from TiemposFlujoCarga tc
                        where tc.IdEstado = 7 and tc.Eliminado = 0) tc11 on i.IdInstruccion = tc11.IdInstruccion and tc11.RN = '1' --Pase de Salida
                    left join (select tc.IdInstruccion, tc.Fecha, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id DESC) as RN from TiemposFlujoCarga tc
                        where tc.IdEstado = 9 and tc.Eliminado = 0) tc12 on i.IdInstruccion = tc12.IdInstruccion and tc12.RN = '1' --Entrega Documentos
                    -----------------------------------------------------Gestion Carga-------------------------------------------------------------
                    left join(select gc.IdInstruccion, gc.Fecha, ROW_NUMBER() OVER(PARTITION BY gc.IdInstruccion ORDER BY gc.IdGestionCarga DESC) as RN from GestionCarga gc
                        where gc.Estado = 0 and gc.Eliminado = 0) gc0 on i.IdInstruccion = gc0.IdInstruccion and gc0.RN = '1' --Comienzo Flujo
                    left join(select gc.IdInstruccion, gc.Fecha, ROW_NUMBER() OVER(PARTITION BY gc.IdInstruccion ORDER BY gc.IdGestionCarga DESC) as RN from GestionCarga gc
                        where gc.Estado = 1 and gc.Eliminado = 0) gc1 on i.IdInstruccion = gc1.IdInstruccion and gc1.RN = '1' --Validacion Electronica
                    left join GestionCarga gc2 on (i.IdInstruccion = gc2.IdInstruccion and gc2.Estado = 2 and gc2.Eliminado = 0)
                    left join GestionCarga gc3 on (i.IdInstruccion = gc3.IdInstruccion and gc3.Estado = 3 and gc3.Eliminado = 0)
                    left join GestionCarga gc4 on (i.IdInstruccion = gc4.IdInstruccion and gc4.Estado = 4 and gc4.Eliminado = 0)
                    left join GestionCarga gc5 on (i.IdInstruccion = gc5.IdInstruccion and gc5.Estado = 5 and gc5.Eliminado = 0)
                    left join GestionCarga gc6 on (i.IdInstruccion = gc6.IdInstruccion and gc6.Estado = 6 and gc6.Eliminado = 0)
                        ";
        }
        #endregion

        #region JoinImpuestos

        if (buscarPor2 == 2 || buscarPor == 2)
        {
            //--------------------------------------------------------------------JOINS IMPUESTOS------------------------------------------------------------------------------------------------
            sql += @" left join (select cb.IdInstruccion, cb.TotalLps, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Item DESC) as RN from [EsquemaTramites2].[dbo].[ConceptoBoletin] cb 
				            where cb.TipoImpuesto = 'DAI' and cb.Eliminado = 0) cb on (i.IdInstruccion  = cb.IdInstruccion collate Modern_Spanish_CI_AI) and cb.RN = '1' 
			            left join (select cb.IdInstruccion, cb.TotalLps, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Item DESC) as RN from [EsquemaTramites2].[dbo].[ConceptoBoletin] cb 
				            where cb.TipoImpuesto = 'ISV' and cb.Eliminado = 0) cb1 on (i.IdInstruccion = cb1.IdInstruccion collate Modern_Spanish_CI_AI) and cb1.RN = '1' 
			            left join (select cb.IdInstruccion, cb.TotalLps, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Item DESC) as RN from [EsquemaTramites2].[dbo].[ConceptoBoletin] cb 
				            where cb.TipoImpuesto = 'SEL' and cb.Eliminado = 0) cb2 on (i.IdInstruccion = cb2.IdInstruccion  collate Modern_Spanish_CI_AI ) and cb2.RN = '1' 
			            left join (select cb.IdInstruccion, cb.TotalLps, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Item DESC) as RN from [EsquemaTramites2].[dbo].[ConceptoBoletin] cb 
				            where cb.TipoImpuesto = 'VIA' and cb.Eliminado = 0) cb3 on (i.IdInstruccion = cb3.IdInstruccion collate Modern_Spanish_CI_AI ) and cb3.RN = '1' 
			            left join (select cb.IdInstruccion, cb.TotalLps, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Item DESC) as RN from [EsquemaTramites2].[dbo].[ConceptoBoletin] cb 
				            where cb.TipoImpuesto = 'GAR' and cb.Eliminado = 0) cb4 on (i.IdInstruccion = cb4.IdInstruccion collate Modern_Spanish_CI_AI ) and cb4.RN = '1' 
			            left join (select cb.IdInstruccion, cb.TotalLps, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Item DESC) as RN from [EsquemaTramites2].[dbo].[ConceptoBoletin] cb 
				            where cb.TipoImpuesto = 'STD' and cb.Eliminado = 0) cb5 on (i.IdInstruccion = cb5.IdInstruccion collate Modern_Spanish_CI_AI ) and cb5.RN = '1' 
			            left join (select cb.IdInstruccion, cb.TotalLps, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Item DESC) as RN from [EsquemaTramites2].[dbo].[ConceptoBoletin] cb 
				            where cb.TipoImpuesto = 'PYC' and cb.Eliminado = 0) cb6 on (i.IdInstruccion = cb6.IdInstruccion collate Modern_Spanish_CI_AI ) and cb6.RN = '1' 
			            left join (select cb.IdInstruccion, cb.TotalLps, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Item DESC) as RN from [EsquemaTramites2].[dbo].[ConceptoBoletin] cb 
				            where cb.TipoImpuesto = 'IVG' and cb.Eliminado = 0) cb7 on (i.IdInstruccion = cb7.IdInstruccion collate Modern_Spanish_CI_AI ) and cb7.RN = '1' 
			            left join (select cb.IdInstruccion, cb.TotalLps, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Item DESC) as RN from [EsquemaTramites2].[dbo].[ConceptoBoletin] cb 
				            where cb.TipoImpuesto = 'IVZ' and cb.Eliminado = 0) cb8 on (i.IdInstruccion = cb8.IdInstruccion collate Modern_Spanish_CI_AI ) and cb8.RN = '1' 
                        ";
        }
        #endregion

        #region JoinsAduanasMaritimas
        if (tipoAduana == 2)
        {

            if (codAduana == "016")
            {
                sql += @"left join (select tc.IdInstruccion, tc.Fecha, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id DESC) as RN from TiemposFlujoCarga tc
	                        where tc.IdEstado = 13 and tc.Eliminado = 0) tc13 on i.IdInstruccion = tc13.IdInstruccion and tc13.RN = '1' --Ingeso Cita OPC
                        left join (select tc.IdInstruccion, tc.Fecha, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id DESC) as RN from TiemposFlujoCarga tc
	                        where tc.IdEstado = 3 and tc.Eliminado = 0) tc14 on i.IdInstruccion = tc14.IdInstruccion and tc14.RN = '1' --Cita OPC
                        left join (select pf.IdInstruccion, pf.CitaOpc, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id) as RN from ProcesoFlujoCarga pf 
	                        where pf.Eliminado = 0) pf1 on i.IdInstruccion = pf1.IdInstruccion and pf1.RN = '1' --Decision CitaOPC
                        left join (select tc.IdInstruccion, tc.Fecha, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id) as RN from TiemposFlujoCarga tc 
	                    where tc.Eliminado = 0 and tc.IdEstado = 8) tc15 on i.IdInstruccion = tc15.IdInstruccion and tc15.RN = '1' --Gete Pass 
                        ";
            }
            
        }
        #endregion

        #region JoinsEventos

        if (buscarPor == 3 || buscarPor2 == 3)
        {
            sql += @" left join (select e.IdInstruccion, e.FechaInicio, e.FechaFin, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id Desc) as RN from Eventos e
	                    where e.IdEvento = 1 and e.Eliminado = '1') e01 on i.IdInstruccion = e01.IdInstruccion and e01.RN = '1' -- Arribo de Carga
                    left join (select tf.IdInstruccion, tf.Fecha, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id Desc) as RN from TiemposFlujoCarga tf
	                    where tf.IdEstado = 14 and tf.Eliminado = '1') tf on i.IdInstruccion = tf.IdInstruccion and tf.RN = '1' -- Arribo de Carga 
                    left join (select e.IdInstruccion, e.FechaInicio, e.FechaFin, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id Desc) as RN from Eventos e
	                    where e.IdEvento = 37 and e.Eliminado = '1') e1 on i.IdInstruccion = e1.IdInstruccion and e1.RN = '1' -- Creacion 
                    left join (select e.IdInstruccion, e.FechaInicio, e.FechaFin, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id Desc) as RN from Eventos e
	                    where e.IdEvento = 49 and e.Eliminado = '1') e2 on i.IdInstruccion = e2.IdInstruccion and e2.RN = '1' --Ingreso de 
                    left join (select e.IdInstruccion, e.FechaInicio, e.FechaFin, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id Desc) as RN from Eventos e
	                    where e.IdEvento = 43 and e.Eliminado = '1') e3 on i.IdInstruccion = e3.IdInstruccion and e3.RN = '1' --Envio Docs. completos y correctos a Aduana
                    left join (select e.IdInstruccion, e.FechaInicio, e.FechaFin, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id Desc) as RN from Eventos e
	                    where e.IdEvento = 44 and e.Eliminado = '1') e4 on i.IdInstruccion = e4.IdInstruccion and e4.RN = '1' --Recepcion Docs. correctos y completos a Aduana
                    left join (select e.IdInstruccion, e.FechaInicio, e.FechaFin, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id Desc) as RN from Eventos e
	                    where e.IdEvento = 45 and e.Eliminado = '1') e5 on i.IdInstruccion = e5.IdInstruccion and e5.RN = '1' --Envio Docs Completosy correctos a Aforro
                    left join (select e.IdInstruccion, e.FechaInicio, e.FechaFin, ROW_NUMBER() OVER(PARTITION BY IdInstruccion ORDER BY Id Desc) as RN from Eventos e
	                    where e.IdEvento = 16 and e.Eliminado = '1') e6 on i.IdInstruccion = e6.IdInstruccion and e6.RN = '1' --Recepcion Docs Completosy correctos a Aforro
                    ";
        }
        #endregion

        #region Join Gastos

        if (buscarPor == 4 || buscarPor2 == 4)
        {
            sql +=@" left join (select ht.IdHojaRuta, ht.ValorReal, ROW_NUMBER() OVER(PARTITION BY IdHojaRuta ORDER BY Id DESC) as RN from [EsquemaTramites2].dbo.HojaRutaEsquemaTramites ht
	                where ht.IdConceptoSAP  = 2) ht on i.IdInstruccion = ht.IdHojaRuta Collate Modern_Spanish_CI_AI and ht.RN = '1' --almacenaje
                left join (select ht.IdHojaRuta, ht.ValorReal, ROW_NUMBER() OVER(PARTITION BY IdHojaRuta ORDER BY Id DESC) as RN from [EsquemaTramites2].dbo.HojaRutaEsquemaTramites ht
	                where ht.IdConceptoSAP  = 71) ht11 on i.IdInstruccion = ht11.IdHojaRuta Collate Modern_Spanish_CI_AI and ht11.RN = '1' --almacenaje2
                left join (select ht.IdHojaRuta, ht.ValorReal, ROW_NUMBER() OVER(PARTITION BY IdHojaRuta ORDER BY Id DESC) as RN from [EsquemaTramites2].dbo.HojaRutaEsquemaTramites ht
	                where ht.IdConceptoSAP  = 124) ht12 on i.IdInstruccion = ht12.IdHojaRuta Collate Modern_Spanish_CI_AI and ht12.RN = '1' --almacenaje3
                left join (select ht.IdHojaRuta, ht.ValorReal, ROW_NUMBER() OVER(PARTITION BY IdHojaRuta ORDER BY Id DESC) as RN from [EsquemaTramites2].dbo.HojaRutaEsquemaTramites ht
	                where ht.IdConceptoSAP = 149) ht13 on i.IdInstruccion = ht13.IdHojaRuta Collate Modern_Spanish_CI_AI and ht13.RN = '1' --almacenaje4
                left join (select ht.IdHojaRuta, ht.ValorReal, ROW_NUMBER() OVER(PARTITION BY IdHojaRuta ORDER BY Id DESC) as RN from [EsquemaTramites2].dbo.HojaRutaEsquemaTramites ht
	                where ht.IdConceptoSAP = 176) ht14 on i.IdInstruccion = ht14.IdHojaRuta Collate Modern_Spanish_CI_AI and ht14.RN = '1' --almacenaje5
                left join (select ht.IdHojaRuta, ht.ValorReal, ROW_NUMBER() OVER(PARTITION BY IdHojaRuta ORDER BY Id DESC) as RN from [EsquemaTramites2].dbo.HojaRutaEsquemaTramites ht
	                where ht.IdConceptoSAP = 36) ht1 on i.IdInstruccion = ht1.IdHojaRuta Collate Modern_Spanish_CI_AI and ht1.RN = '1' --Manejo Documentos
                left join (select ht.IdHojaRuta, ht.ValorReal, ROW_NUMBER() OVER(PARTITION BY IdHojaRuta ORDER BY Id DESC) as RN from [EsquemaTramites2].dbo.HojaRutaEsquemaTramites ht
	                where ht.IdConceptoSAP = 170) ht15 on i.IdInstruccion = ht1.IdHojaRuta Collate Modern_Spanish_CI_AI and ht1.RN = '1' --Manejo Documentos2
                left join (select ht.IdHojaRuta, ht.ValorReal, ROW_NUMBER() OVER(PARTITION BY IdHojaRuta ORDER BY Id DESC) as RN from [EsquemaTramites2].dbo.HojaRutaEsquemaTramites ht
	                where ht.IdConceptoSAP = 34) ht2 on i.IdInstruccion = ht2.IdHojaRuta Collate Modern_Spanish_CI_AI and ht2.RN = '1' --Inspeccion Sepa
                left join (select ht.IdHojaRuta, ht.ValorReal, ROW_NUMBER() OVER(PARTITION BY IdHojaRuta ORDER BY Id DESC) as RN from [EsquemaTramites2].dbo.HojaRutaEsquemaTramites ht
	                where ht.IdConceptoSAP = 104) ht3 on i.IdInstruccion = ht3.IdHojaRuta Collate Modern_Spanish_CI_AI and ht3.RN = '1' --Complemento Inspeccion Sepa
                left join (select ht.IdHojaRuta, ht.ValorReal, ROW_NUMBER() OVER(PARTITION BY IdHojaRuta ORDER BY Id DESC) as RN from [EsquemaTramites2].dbo.HojaRutaEsquemaTramites ht
	                where ht.IdConceptoSAP = 85) ht4 on i.IdInstruccion = ht4.IdHojaRuta Collate Modern_Spanish_CI_AI and ht4.RN = '1' --Identificacion Plaga Sepa
                left join (select ht.IdHojaRuta, ht.ValorReal, ROW_NUMBER() OVER(PARTITION BY IdHojaRuta ORDER BY Id) as RN from [EsquemaTramites2].dbo.HojaRutaEsquemaTramites ht
	                where ht.IdConceptoSAP = 55) ht5 on i.IdInstruccion = ht5.IdHojaRuta Collate Modern_Spanish_CI_AI and ht5.RN = '1' --Revision Sepa
                left join (select ht.IdHojaRuta, ht.ValorReal, ROW_NUMBER() OVER(PARTITION BY IdHojaRuta ORDER BY Id DESC) as RN from [EsquemaTramites2].dbo.HojaRutaEsquemaTramites ht
	                where ht.IdConceptoSAP = 97) ht6 on i.IdInstruccion = ht6.IdHojaRuta Collate Modern_Spanish_CI_AI and ht6.RN = '1' --Identificacion Plaga Senasa
                left join (select ht.IdHojaRuta, ht.ValorReal, ROW_NUMBER() OVER(PARTITION BY IdHojaRuta ORDER BY Id DESC) as RN from [EsquemaTramites2].dbo.HojaRutaEsquemaTramites ht
	                where ht.IdConceptoSAP = 61) ht7 on i.IdInstruccion = ht7.IdHojaRuta Collate Modern_Spanish_CI_AI and ht7.RN = '1' --Sobre Estadia
                left join (select ht.IdHojaRuta, ht.ValorReal, ROW_NUMBER() OVER(PARTITION BY IdHojaRuta ORDER BY Id DESC) as RN from [EsquemaTramites2].dbo.HojaRutaEsquemaTramites ht
	                where ht.IdConceptoSAP = 175) ht8 on i.IdInstruccion = ht8.IdHojaRuta Collate Modern_Spanish_CI_AI and ht8.RN = '1' --Servicio Portuario
                left join (select ht.IdHojaRuta, ht.ValorReal, ROW_NUMBER() OVER(PARTITION BY IdHojaRuta ORDER BY Id DESC) as RN from [EsquemaTramites2].dbo.HojaRutaEsquemaTramites ht
	                where ht.IdConceptoSAP = 175) ht9 on i.IdInstruccion = ht9.IdHojaRuta Collate Modern_Spanish_CI_AI and ht9.RN = '1' --ServicioNaviero 
                ";
        }
                
        #endregion

                sql += @" where i.CodEstado in ('0', '1') and i.IdEstadoFlujo in ('99', 'H-6') and i.CodPaisHojaRuta = 'H'
                 and (i.FechaFinalFlujo between '" + fechaInicio + "' and '" + fechaFin + "') Order by i.IdInstruccion DESC";

        this.loadSQL(sql);
    }


    public void ReporteCargillPostFiling(string idInstruccion, int ejecucion, string idCliente)
    {
        string query = "";

        if (ejecucion == 1)
        {

            query = @"select i.IdInstruccion, ISNULL(da.Correlativo, i.NoCorrelativo) as RegistrationNumber, 
                    c.Descripcion as ExportingCountry, 
					da.RegimenAduanero as EntryType,
                    cl.RTN as ImporterOfRecordNumber,
                    bp.Liquidacion as ControlCarga,
                    i.ValorCIF as TotalValue, 
					'USD' as TotalValueCurrency,
                    tc.TasaCambio as ExchangeRate,                         
                    tf1.Fecha as  DateOfAcceptance,
                    da.FechaOficializacion as EntryDate,
                    i.Flete as Freight, 
					'USD' as FreightCurrency, 
                    i.Seguro as Insurance, 
					'USD' as InsuranceCurrency, 
                    i.Otros as Charges, 'USD' as ChargesCurrency,
					(dc.ImporteLps + dc1.ImporteLps) as Fees, 'LPS' as FeesCurrency,
                    ISNULL(dc3.ImporteLps, 0.00) as OtherTaxes, 'LPS' as OtherTaxesCurrency
                    from instrucciones i
                    left join EspeciesFiscalesInstrucciones ei on (i.IdInstruccion = ei.IdInstruccion and ei.CodEspecieFiscal in ('F','P') and ei.Eliminado = '0' and ei.Cliente = 'X')
                    left join Codigos c on (i.CodPaisProcedencia = c.Codigo and c.Categoria = 'PAISES' and c.Eliminado = '0')
                    left join Clientes cl on (i.IdCliente = cl.CodigoSAP and cl.Eliminado = '0' and cl.CodigoSAP = '8000423')
                    left join TiemposFlujoCarga tf2 on (i.IdInstruccion = tf2.IdInstruccion and tf2.IdEstado = 1 and tf2.Eliminado = 0)
                    left join TipoCambio tc on (CONVERT(varchar, tf2.Fecha, 103) = CONVERT(varchar, tc.Fecha, 103) and tc.Pais = 'H')
                    left join DeclaracionAduanera da on (i.IdInstruccion = da.IdInstruccion and da.Eliminado = '0')
                    left join TiemposFlujoCarga tf on (i.IdInstruccion = tf.IdInstruccion and tf.IdEstado = 12 and tf.Eliminado = 0) --fecha Validaion electronica
                    left join GestionCarga gc on (i.IdInstruccion = gc.IdInstruccion and gc.Estado = 2 and gc.Eliminado = 0)
                    left join TiemposFlujoCarga tf1 on (i.IdInstruccion = tf1.IdInstruccion and tf1.IdEstado in (18, 16, 1) and tf1.Eliminado = 0) --Pago impuestos Caja Chica/Dina
                    left join GestionCarga gc1 on (i.IdInstruccion = gc1.IdInstruccion and gc1.Estado = 1 and gc1.Eliminado = 0)
                    left join DeclaracionConceptos dc on (da.Correlativo = dc.Correlativo and dc.Concepto = 'ISV - IMPTO SOBRE VENTAS')
                    left join DeclaracionConceptos dc1 on (da.Correlativo = dc1.Correlativo and dc1.Concepto = 'DAI - DERECHOS ARANCELARIOS A LA IMPORTACION')
                    left join DeclaracionConceptos dc2 on (da.Correlativo = dc2.Correlativo and dc2.Concepto = 'STD - SERVICIO TRANSPORTE DE DATOS')
                    left join [EsquemaTramites2].[dbo].[BoletinPago] as bp on (i.IdInstruccion = bp.IdInstruccion collate Modern_Spanish_CI_AI and bp.Eliminado = '0')
                    left join DeclaracionConceptos dc3 on (da.Correlativo = dc2.Correlativo and dc2.Concepto = 'SEL - IMPUESTO SELECTIVO AL CONSUMO')
                    where i.CodEstado != '100' and i.CodPaisHojaRuta = 'H' and i.IdInstruccion = '" + idInstruccion + "' and i.IdCliente = '"+ idCliente +"'";

        }
        else if (ejecucion == 2)
        {
            query = @"select distinct i.IdInstruccion as IdInstruccion, 
                    i.PesoKgs as GrossWeight, --
                    di.PesoNeto as NetWeight, --
                    'KGS' as NetWeightUnitOfMeasure,--
                    di.CantidadComercial as NetQuantity, -- 
                    db.Embalaje as NetQuantityUnitOfMeasure,--
                    c.Descripcion as CountryOfOrigin,--
                    '' as [PartyRelationshipAffiliation],--
                    isnull(pf.CanalSelectividad, i.Color) as CustomsStatusPlacement,--
                    DI2.PosicionArancelaria as HTSNumber,--
                    di.ValorUsd as EnteredValue,--
                    'USD' as EnteredValueCurrency,--
                    dc1.ImporteLps as DutyAmount,--
                    'HNL' as DutyAmountCurrency,--
                    isnull(dc.ImporteLps, 0.00) as VatAmount, 'HNL' as VatAmountCurrency,--
                    dc.ImporteLps as TotalAmountInvoiced, 'USD' as CurrencyOfTotalAmountInvoiced, di.Id/**/
                    from Instrucciones i 
                    inner join DeclaracionAduanera da on (i.IdInstruccion = da.IdInstruccion and da.Eliminado = '0')
                    left join DeclaracionItems di on i.IdInstruccion = di.IdInstruccion
                    left join DeclaracionBultos db on i.NoCorrelativo = db.Correlativo
                    left join Codigos c on (i.CodPaisOrigen = c.Codigo and c.Categoria = 'PAISES' and c.Eliminado = 0)
                    left join ProcesoFlujoCarga pf on (i.IdInstruccion = pf.IdInstruccion and pf.Eliminado = 0)
                    left join DeclaracionItems di1 on i.IdInstruccion = di1.IdInstruccion
                    left join DeclaracionConceptos dc on (da.Correlativo = dc.Correlativo and dc.Concepto = 'ISV - IMPTO SOBRE VENTAS')
                    left join DeclaracionConceptos dc1 on (da.Correlativo = dc1.Correlativo and dc1.Concepto = 'DAI - DERECHOS ARANCELARIOS A LA IMPORTACION')
                    Inner Join DeclaracionItems DI2 on (DI2.Id = di.Id)
                    where i.CodEstado != '100'  and i.CodPaisHojaRuta = 'H' and i.IdInstruccion = '" + idInstruccion + "' and i.IdCliente = '" + idCliente + "' order by HTSNumber desc";
        }

        this.loadSQL(query);
    }

    #region Campos
    public string IDTRAMITE
    {
        get
        {
            return (string)registro[Campos.IDTRAMITE].ToString();
        }
        set
        {
            registro[Campos.IDTRAMITE] = value;
        }
    }
    //
    public string RDUA
    {
        get
        {
            return (string)registro[Campos.RDUA].ToString();
        }
        set
        {
            registro[Campos.RDUA] = value;
        }
    }
    public string IDINSTRUCCION
    {
        get
        {
            return (string)registro[Campos.IDINSTRUCCION].ToString();
        }
        set
        {
            registro[Campos.IDINSTRUCCION] = value;
        }
    }

    public string CODPAISHOJARUTA
    {
        get
        {
            return (string)registro[Campos.CODPAISHOJARUTA].ToString();
        }
        set
        {
            registro[Campos.CODPAISHOJARUTA] = value;
        }
    }

    public string CODIGOADUANA
    {
        get
        {
            return (string)registro[Campos.CODIGOADUANA].ToString();
        }
        set
        {
            registro[Campos.CODIGOADUANA] = value;
        }
    }

    public string FECHARECEPCION
    {
        get
        {
            return (string)registro[Campos.FECHARECEPCION].ToString();
        }
        set
        {
            registro[Campos.FECHARECEPCION] = value;
        }
    }

    public string FECHAFINALFLUJO
    {
        get
        {
            return (string)registro[Campos.FECHAFINALFLUJO].ToString();
        }
        set
        {
            registro[Campos.FECHAFINALFLUJO] = value;
        }
    }

    public string IDCLIENTE
    {
        get
        {
            return (string)registro[Campos.IDCLIENTE].ToString();
        }
        set
        {
            registro[Campos.IDCLIENTE] = value;
        }
    }

    public string OFICIALCUENTA
    {
        get
        {
            return (string)registro[Campos.OFICIALCUENTA].ToString();
        }
        set
        {
            registro[Campos.OFICIALCUENTA] = value;
        }
    }

    public string REFERENCIACLIENTE
    {
        get
        {
            return (string)registro[Campos.REFERENCIACLIENTE].ToString();
        }
        set
        {
            registro[Campos.REFERENCIACLIENTE] = value;
        }
    }

    public string PROVEEDOR
    {
        get
        {
            return (string)registro[Campos.PROVEEDOR].ToString();
        }
        set
        {
            registro[Campos.PROVEEDOR] = value;
        }
    }

    public string NOFACTURA
    {
        get
        {
            return (string)registro[Campos.NOFACTURA].ToString();
        }
        set
        {
            registro[Campos.NOFACTURA] = value;
        }
    }

    public string IDESPECIEFISCAL
    {
        get
        {
            return (string)registro[Campos.IDESPECIEFISCAL].ToString();
        }
        set
        {
            registro[Campos.IDESPECIEFISCAL] = value;
        }
    }

    public string IDDOCUMENTOESPECIEFISCAL
    {
        get
        {
            return (string)registro[Campos.IDDOCUMENTOESPECIEFISCAL].ToString();
        }
        set
        {
            registro[Campos.IDDOCUMENTOESPECIEFISCAL] = value;
        }
    }

    public double VALORFOB
    {
        get
        {
            return double.Parse(registro[Campos.VALORFOB].ToString());
        }
        set
        {
            registro[Campos.VALORFOB] = value;
        }
    }

    public double FLETE
    {
        get
        {
            return double.Parse(registro[Campos.FLETE].ToString());
        }
        set
        {
            registro[Campos.FLETE] = value;
        }
    }

    public double SEGURO
    {
        get
        {
            return double.Parse(registro[Campos.SEGURO].ToString());
        }
        set
        {
            registro[Campos.SEGURO] = value;
        }
    }

    public double OTROS
    {
        get
        {
            return double.Parse(registro[Campos.OTROS].ToString());
        }
        set
        {
            registro[Campos.OTROS] = value;
        }
    }

    public double VALORCIF
    {
        get
        {
            return double.Parse(registro[Campos.VALORCIF].ToString());
        }
        set
        {
            registro[Campos.VALORCIF] = value;
        }
    }

    public string CODDOCUMENTOEMBARQUE
    {
        get
        {
            return (string)registro[Campos.CODDOCUMENTOEMBARQUE].ToString();
        }
        set
        {
            registro[Campos.CODDOCUMENTOEMBARQUE] = value;
        }
    }

    public string NODOCUMENTOEMBARQUE
    {
        get
        {
            return (string)registro[Campos.NODOCUMENTOEMBARQUE].ToString();
        }
        set
        {
            registro[Campos.NODOCUMENTOEMBARQUE] = value;
        }
    }

    public Int32 CANTIDADBULTOS
    {
        get
        {
            return Int32.Parse(registro[Campos.CANTIDADBULTOS].ToString());
        }
        set
        {
            registro[Campos.CANTIDADBULTOS] = value;
        }
    }

    public double PESOKGS
    {
        get
        {
            return double.Parse(registro[Campos.PESOKGS].ToString());
        }
        set
        {
            registro[Campos.PESOKGS] = value;
        }
    }

    public string CONTENEDORES
    {
        get
        {
            return (string)registro[Campos.CONTENEDORES].ToString();
        }
        set
        {
            registro[Campos.CONTENEDORES] = value;
        }
    }

    public string DESCRIPCIONCONTENEDORES
    {
        get
        {
            return (string)registro[Campos.DESCRIPCIONCONTENEDORES].ToString();
        }
        set
        {
            registro[Campos.DESCRIPCIONCONTENEDORES] = value;
        }
    }

    public string PRODUCTO
    {
        get
        {
            return (string)registro[Campos.PRODUCTO].ToString();
        }
        set
        {
            registro[Campos.PRODUCTO] = value;
        }
    }

    public string CODREGIMEN
    {
        get
        {
            return (string)registro[Campos.CODREGIMEN].ToString();
        }
        set
        {
            registro[Campos.CODREGIMEN] = value;
        }
    }

    public string CODPAISPROCEDENCIA
    {
        get
        {
            return (string)registro[Campos.CODPAISPROCEDENCIA].ToString();
        }
        set
        {
            registro[Campos.CODPAISPROCEDENCIA] = value;
        }
    }

    public string CODPAISORIGEN
    {
        get
        {
            return (string)registro[Campos.CODPAISORIGEN].ToString();
        }
        set
        {
            registro[Campos.CODPAISORIGEN] = value;
        }
    }

    public string CODPAISDESTINO
    {
        get
        {
            return (string)registro[Campos.CODPAISDESTINO].ToString();
        }
        set
        {
            registro[Campos.CODPAISDESTINO] = value;
        }
    }

    public string CIUDADDESTINO
    {
        get
        {
            return (string)registro[Campos.CIUDADDESTINO].ToString();
        }
        set
        {
            registro[Campos.CIUDADDESTINO] = value;
        }
    }

    public string CODTIPOTRANSPORTE
    {
        get
        {
            return (string)registro[Campos.CODTIPOTRANSPORTE].ToString();
        }
        set
        {
            registro[Campos.CODTIPOTRANSPORTE] = value;
        }
    }

    public string CODTIPOCARGAMENTO
    {
        get
        {
            return (string)registro[Campos.CODTIPOCARGAMENTO].ToString();
        }
        set
        {
            registro[Campos.CODTIPOCARGAMENTO] = value;
        }
    }

    public string CODTIPOCARGA
    {
        get
        {
            return (string)registro[Campos.CODTIPOCARGA].ToString();
        }
        set
        {
            registro[Campos.CODTIPOCARGA] = value;
        }
    }

    public string FECHAVENCTIEMPOLIBRENAVIERA
    {
        get
        {
            return (string)registro[Campos.FECHAVENCTIEMPOLIBRENAVIERA].ToString();
        }
        set
        {
            registro[Campos.FECHAVENCTIEMPOLIBRENAVIERA] = value;
        }
    }

    public string FECHAVENCTIEMPOLIBREPUERTO
    {
        get
        {
            return (string)registro[Campos.FECHAVENCTIEMPOLIBREPUERTO].ToString();
        }
        set
        {
            registro[Campos.FECHAVENCTIEMPOLIBREPUERTO] = value;
        }
    }

    public string ETA
    {
        get
        {
            return (string)registro[Campos.ETA].ToString();
        }
        set
        {
            registro[Campos.ETA] = value;
        }
    }

    public string TIEMPOESTIMADOENTREGA
    {
        get
        {
            return (string)registro[Campos.TIEMPOESTIMADOENTREGA].ToString();
        }
        set
        {
            registro[Campos.TIEMPOESTIMADOENTREGA] = value;
        }
    }

    public string INSTRUCCIONESENTREGA
    {
        get
        {
            return (string)registro[Campos.INSTRUCCIONESENTREGA].ToString();
        }
        set
        {
            registro[Campos.INSTRUCCIONESENTREGA] = value;
        }
    }

    public string OBSERVACION
    {
        get
        {
            return (string)registro[Campos.OBSERVACION].ToString();
        }
        set
        {
            registro[Campos.OBSERVACION] = value;
        }
    }

    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }

    public string CODESTADO
    {
        get
        {
            return (string)registro[Campos.CODESTADO].ToString();
        }
        set
        {
            registro[Campos.CODESTADO] = value;
        }
    }

    public string IDESTADOFLUJO
    {
        get
        {
            return (string)registro[Campos.IDESTADOFLUJO].ToString();
        }
        set
        {
            registro[Campos.IDESTADOFLUJO] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }

    public string IDUSUARIOADUANA
    {
        get
        {
            return (string)registro[Campos.IDUSUARIOADUANA].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIOADUANA] = value;
        }
    }

    public string ARRIBOCARGA
    {
        get
        {
            return (string)registro[Campos.ARRIBOCARGA].ToString();
        }
        set
        {
            registro[Campos.ARRIBOCARGA] = value;
        }
    }

    public string NOCORRELATIVO
    {
        get
        {
            return (string)registro[Campos.NOCORRELATIVO].ToString();
        }
        set
        {
            registro[Campos.NOCORRELATIVO] = value;
        }
    }

    public string AFORADOR
    {
        get
        {
            return (string)registro[Campos.AFORADOR].ToString();
        }
        set
        {
            registro[Campos.AFORADOR] = value;
        }
    }

    public string COLOR
    {
        get
        {
            return (string)registro[Campos.COLOR].ToString();
        }
        set
        {
            registro[Campos.COLOR] = value;
        }
    }

    public string TRAMITADOR
    {
        get
        {
            return (string)registro[Campos.TRAMITADOR].ToString();
        }
        set
        {
            registro[Campos.TRAMITADOR] = value;
        }
    }

    public double IMPUESTO
    {
        get
        {
            return double.Parse(registro[Campos.IMPUESTO].ToString());
        }
        set
        {
            registro[Campos.IMPUESTO] = value;
        }
    }

    public string CODIGOGUIA
    {
        get
        {
            return (string)registro[Campos.CODIGOGUIA].ToString();
        }
        set
        {
            registro[Campos.CODIGOGUIA] = value;
        }
    }

    public string DIVISION
    {
        get
        {
            return (string)registro[Campos.DIVISION].ToString();
        }
        set
        {
            registro[Campos.DIVISION] = value;
        }
    }
    public string INCOTERM
    {
        get
        {
            return (string)registro[Campos.INCOTERM].ToString();
        }
        set
        {
            registro[Campos.INCOTERM] = value;
        }
    }
    public string NUMEROFACTURA
    {
        get
        {
            return (string)registro[Campos.NUMEROFACTURA].ToString();
        }
        set
        {
            registro[Campos.NUMEROFACTURA] = value;
        }
    }
    public double ISV
    {
        get
        {
            return double.Parse(registro[Campos.ISV].ToString());
        }
        set
        {
            registro[Campos.ISV] = value;
        }
    }
    public double DAI
    {
        get
        {
            return double.Parse(registro[Campos.DAI].ToString());
        }
        set
        {
            registro[Campos.DAI] = value;
        }
    }
    public double SELECTIVO
    {
        get
        {
            return double.Parse(registro[Campos.SELECTIVO].ToString());
        }
        set
        {
            registro[Campos.SELECTIVO] = value;
        }
    }
    public double STD
    {
        get
        {
            return double.Parse(registro[Campos.STD].ToString());
        }
        set
        {
            registro[Campos.STD] = value;
        }
    }
    public double PRODUCCIONCONSUMO
    {
        get
        {
            return double.Parse(registro[Campos.PRODUCCIONCONSUMO].ToString());
        }
        set
        {
            registro[Campos.PRODUCCIONCONSUMO] = value;
        }
    }
    public double OTROSIMPUESTOS
    {
        get
        {
            return double.Parse(registro[Campos.OTROSIMPUESTOS].ToString());
        }
        set
        {
            registro[Campos.OTROSIMPUESTOS] = value;
        }
    }

    public int IDOFICIALCUENTA
    {
        get
        {
            return int.Parse(registro[Campos.IDOFICIALCUENTA].ToString());
        }
        set
        {
            registro[Campos.IDOFICIALCUENTA] = value;
        }
    }

    // IDOFICIALCUENTA
    public string PRODUCTOSCARGILL
    {
        get
        {
            return (string)registro[Campos.PRODUCTOSCARGILL].ToString();
        }
        set
        {
            registro[Campos.PRODUCTOSCARGILL] = value;
        }
    }

    #endregion


}
