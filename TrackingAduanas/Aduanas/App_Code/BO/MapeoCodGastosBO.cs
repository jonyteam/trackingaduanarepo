﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;
using GrupoLis.Login;
using GrupoLis.Ebase;

/// <summary>
/// Summary description for MapeoCodGastosBO
/// </summary>
public class MapeoCodGastosBO : CapaBase
{
    class campos
    {
        public static string CodEsquema = "CodEsquema";
        public static string CodCajaChica = "CodCajaChica";
    }

    public MapeoCodGastosBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "select * from MapeoCodGastos";
        this.initializeSchema("MapeoCodGastos");
    }

    public void loadMapeoCodGastos()
    {
        string sql = this.coreSQL;
        this.loadSQL(sql);
    }

    public void loadCodigosEsquemaCC(String CodESquemaCC)
    {
        string sql = " select codCajaChica from MapeoCodGastos where codEsquema='" + CodESquemaCC + "'";
        this.loadSQL(sql);
    }
    public void BuscarGastos(String CodAduanas, String CodCajaChica)
    {
        string sql = " select * from MapeoCodGastos where CodEsquema ='" + CodAduanas + "' and CodCajaChica ='" + CodCajaChica + "'";
        this.loadSQL(sql);
    }

    //**Campos
    public Int32 CodEsquema
    {
        get
        {
            return Convert.ToInt32(registro[campos.CodEsquema].ToString());
        }
        set
        {
            registro[campos.CodEsquema] = value;
        }
    }

    public Int32 CodCajaChica
    {
        get
        {
            return Convert.ToInt32(registro[campos.CodCajaChica].ToString());
        }
        set
        {
            registro[campos.CodCajaChica] = value;
        }
    }
}
