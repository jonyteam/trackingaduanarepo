﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for CiudadesBO
/// </summary>
public class CiudadesBO: CapaBase
{
    class Campos
    {
        public static string CODIGOCIUDAD = "CodigoCiudad";
        public static string DESCRIPCION = "Descripcion";
        public static string CODIGOPAIS = "CodigoPais";
        public static string ESTADO = "Estado";
    }

    public CiudadesBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from Ciudades ";
        this.initializeSchema("Ciudades");
    }

    public void loadAllCampoCiuades()
    {
        string sql = this.coreSQL;
        sql += " where Estado = '0' order by Descripcion";
        this.loadSQL(sql);
    }

    public void loadAllCampoCiudades(string codCiudad)
    {
        string sql = this.coreSQL;
        sql += " where Estado = '0' and CodigoCiudad=" + codCiudad + " order by Descripcion";
        this.loadSQL(sql);
    }

    public void loadAllCampoCiudadesPais(string codpais)
    {
        string sql = this.coreSQL;
        sql += " where Estado = '0' and CodigoPais = @codPais";
        this.loadSQL(sql, new Parametro("@codPais", codpais));
    }

    public void loadCiudades()
    {
        string sql = "select CodigoPais, c.Descripcion as Pais, ci.Descripcion, CodigoCiudad";
        sql += " from Ciudades ci inner join Codigos c on (ci.CodigoPais = c.Codigo and c.Categoria = 'PAISES')";
        sql += " where Estado = '0'";
        this.loadSQL(sql);
    }

    public string CODIGOCIUDAD
    {
        get
        {
            return (string)registro[Campos.CODIGOCIUDAD].ToString();
        }
        set
        {
            registro[Campos.CODIGOCIUDAD] = value;
        }
    }

    public string DESCRIPCION
    {
        get
        {
            return (string)registro[Campos.DESCRIPCION].ToString();
        }
        set
        {
            registro[Campos.DESCRIPCION] = value;
        }
    }


    public string CODIGOPAIS
    {
        get
        {
            return (string)registro[Campos.CODIGOPAIS].ToString();
        }
        set
        {
            registro[Campos.CODIGOPAIS] = value;
        }
    }

    public string ESTADO
    {
        get
        {
            return (string)registro[Campos.ESTADO].ToString();
        }
        set
        {
            registro[Campos.ESTADO] = value;
        }
    }
}