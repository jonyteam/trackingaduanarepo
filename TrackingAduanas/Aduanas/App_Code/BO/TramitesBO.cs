﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for TramitesBO
/// </summary>
public class TramitesBO : CapaBase
{
    class Campos
    {
        public static string ID = "Id";
        public static string IDARCHIVO = "IdArchivo";
        public static string TRAMITE = "Tramite";
        public static string NOMBREARCHIVO = "NombreArchivo";
        public static string BARCO = "Barco";
        public static string VIAJE = "Viaje";
        public static string REGIMEN = "Regimen";
        public static string SARA = "SARA";
        public static string PREIMPRESO = "Preimpreso";
        public static string BL = "BL";
        public static string CONSOLIDADO = "Consolidado";
        public static string PROVEEDOR = "Proveedor";
        public static string CONSIGNATARIO = "Consignatario";
        public static string NOTIFICAR = "Notificar";
        public static string TRANSBORDO = "Transbordo";
        public static string IMPORTEFLETE = "ImporteFlete";
        public static string FRACCIONADO = "Fraccionado";
        public static string MARCAS = "Marcas";
        public static string DESCRIPCION = "Descripcion";
        public static string CLASEBULTO = "ClaseBulto";
        public static string CANTIDADBULTO = "CantidadBulto";
        public static string MERCADERIA = "Mercaderia";
        public static string UNIDADPESO = "UnidadPeso";
        public static string PESOBRUTO = "PesoBruto";
        public static string CONTENEDOR = "Contenedor";
        public static string PRECINTOS = "Precintos";
        public static string DIMENSIONES = "Dimensiones";
        public static string TIPOCARGA = "TipoCarga";
        public static string VACIO = "Vacio";
        public static string FECHA = "Fecha";
        public static string IDUSUARIO = "IdUsuario";
        public static string ESTADO = "Estado";
        public static string DECLARACIONES = "Declaraciones";
        public static string MANIFIESTO = "Manifiesto";
        public static string FACTURA = "Factura";

    }

    public TramitesBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from Tramites";
        this.initializeSchema("Tramites");
    }

    public void loadTramites()
    {
        string sql = this.coreSQL;
        this.loadSQL(sql);
    }

    public void loadNuevoId(string cod)
    {
        string sql = "declare @Id varchar(50)";
        sql += " select @Id = (SELECT NuevoId = MAX(CAST(LEFT(Tramite,2)AS varchar(5))) +";
        sql += " RIGHT('' + CAST(MAX(CAST(RIGHT(Tramite,LEN(Tramite)- 2)+ 1 AS int))AS varchar(50)),50)";
        sql += " FROM Tramites";
        sql += " Where Tramite like '" + cod + "%')";
        sql += " select @Id as NuevoId";
        this.loadSQL(sql);
    }

    public void loadmax()
    {
        //string sql = "Select MAX(Id)as Id, Tramite from Tramites Group by Tramite order by Tramite desc";
        string sql = "Select MAX(Id),Tramite from Tramites group by Id,Tramite order by Id DESC";
        loadSQL(sql);
    }

    public void loadTramites(string estado)
    {
        string sql = "Select * from Tramites";
        sql += " where Estado = '" + estado + "'";
        this.loadSQL(sql);
    }

    public void loadTramitesI(string Tramite)
    {
        string sql = "Select * from Tramites";
        sql += " where Tramite = '" + Tramite + "'";
        this.loadSQL(sql);
    }

    public void loadTramitesId(string id)
    {
        string sql = "Select * from Tramites";
        sql += " where Id = '" + id + "'";
        this.loadSQL(sql);
    }

    public void loadTramitesNombre(string nombre)
    {
        string sql = "Select * from Tramites";
        sql += " where NombreArchivo = '" + nombre + "'";
        this.loadSQL(sql);
    }

    public void loadTramitesViaje(string viaje, string barco)
    {
        string sql = "Select * from Tramites";
        sql += " where Barco = '" + barco + "' and Viaje = '" + viaje + "'";
        this.loadSQL(sql);
    }

    public void loadTramitesNombreEstado(string nombre, string estado)
    {
        string sql = "Select * from Tramites";
        sql += " where NombreArchivo = '" + nombre + "' and Estado = '" + estado + "'";
        this.loadSQL(sql);
    }

    public void loadTramitesNombrePreimpreso(string nombre, string preimpreso, string estado)
    {
        string sql = "Select * from Tramites";
        sql += " where NombreArchivo = '" + nombre + "' and Preimpreso = '" + preimpreso + "' and Estado = '" + estado + "'";
        this.loadSQL(sql);
    }

    public void loadTramitesSARAEstado(string estado)
    {
        string sql = "Select * from Tramites";
        sql += " where SARA is not null and Estado = '" + estado + "'";
        this.loadSQL(sql);
    }

    public void loadTramitesSaraTransitos(string estado)//Carga todos los tramites de Sara y Transitos con estado 2
    {
        string sql = "Select * from Tramites where estado='" + estado + "'";
        this.loadSQL(sql);
    }

    public void loadTramitesEstadoReg(string estado, string regimen)
    {
        string sql = "";
        //if (regimen == "102")//Contenedores Adicionales *Aun en prueba*
        //{
        //    sql+="Select T.Id,T.Barco,T.Consignatario,T.Tramite,T.Preimpreso,T.SARA ";
        //    sql+=" from Tramites T ";
        //    sql+=" inner join Contenedores C on (C.IdTramite=T.Tramite) ";
        //    sql += " where C.regimen=" + regimen + " and T.Estado in (3,4) and C.estado=0";
        //    sql+=" group by T.Id,T.Consignatario,T.Tramite,T.Preimpreso,T.SARA,T.Barco ";
        //    sql += " having COUNT(C.IdTramite)>1 ";
        //}
        //else
        //{
        //    sql += "Select * from Tramites";
        //    sql += " where Estado = '" + estado + "' and Regimen = '" + regimen + "'";
        //}
        sql += "Select * from Tramites";
        sql += " where Estado = '" + estado + "' and Regimen = '" + regimen + "'";
        this.loadSQL(sql);
    }

    public void loadTramitesDist(string estado)
    {
        string sql = "Select distinct(NombreArchivo), IdArchivo from Tramites";
        sql += " where Estado = '" + estado + "'";
        this.loadSQL(sql);
    }

    public void loadArchivoTramites(string estado)
    {
        string sql = "Select 'Seleccione..' as NombreArchivo, '0' as IdArchivo ";
        sql += " UNION ";
        sql += " Select distinct(NombreArchivo),IdArchivo from Tramites ";
        sql += " where Estado= '" + estado + "'";
        sql += " order by IdArchivo";
        this.loadSQL(sql);
    }

    public void loadTramitesArchivo(string mani)
    {
        string sql = "Select 'Seleccione..' as NombreArchivo, '0' as IdArchivo ";
        sql += " UNION ";
        sql += "Select distinct(NombreArchivo), IdArchivo from Tramites";
        sql += " where Manifiesto = '" + mani + "' and Estado not in (8,9) order by IdArchivo";
        this.loadSQL(sql);
    }

    public void CargarTramitesxArchivo(string nombreArchivo, string estadoMANI)
    {
        string sql = "select * from Tramites ";
        sql += " where NombreArchivo='" + nombreArchivo + "' and Manifiesto='" + estadoMANI + "' and Estado not in (8,9)";
        this.loadSQL(sql);
    }

    public void loadTramitesDeclara(string decla)
    {
        string sql = "Select distinct(NombreArchivo), IdArchivo from Tramites";
        sql += " where Declaraciones = '" + decla + "'";
        this.loadSQL(sql);
    }

    public void loadTramitesManifi(string mani)
    {
        string sql = "Select distinct(NombreArchivo), IdArchivo from Tramites";
        sql += " where Manifiesto = '" + mani + "'";
        this.loadSQL(sql);
    }


    public void loadTramitesinDeclaracion(string nombre)//Carga todos los tramites que no se le han ingresado preimpreso
    {
        string sql = "select * from Tramites ";
        sql += " where NombreArchivo= '" + nombre + " ' and Declaraciones =0";
        this.loadSQL(sql);
    }

    public void AgregarRegimenTransitos(string regimen)//Ingresar el regimen para los transitos internacionales 
    {
        string sql = "exec RegimenTransitos '" + regimen + "'";
        this.loadSQL(sql);
    }

    public void AgregarRegimenTraslados(string regimen)
    {
        string sql = "exec RegimenTraslados '" + regimen + "'";
        this.loadSQL(sql);
    }

    public void CargarIdArchivo(string Archivo)
    {
        string sql = "Select distinct IdArchivo ";
        sql += " from Tramites where NombreArchivo='" + Archivo + "' and Estado not in (8,9)";
        this.loadSQL(sql);
    }

    public void CargarGridTramites(string barco, string viaje)
    {
        string sql = "Select distinct T.tramite as Tramite,T.Consignatario as Consignatario,T.BL as BL,C.Contenedor as Contenedor, ";
        //sql += " Registro as Registro, ";
        sql += " case when (T.Regimen in('100','101') and T.Estado<>'0') ";
        sql += " then T.Preimpreso ";
        sql += " when (T.Regimen is not null) ";
        sql += " then T.SARA else	'-' end as 'NumRegistro', ";
        sql += " Co.Descripcion as Descripcion,TM.Fecha as ETA, ";
        sql += " isnull(convert(varchar(25),TM1.Fecha,21),'-') as InicioTramite, ";
        sql += " isnull(CONVERT(varchar(25),TM2.fecha,21),'-')as FinTramite, ";
        sql += " (TM.Observacion+'. '+TM1.Observacion+'. '+TM2.Observacion) as Observaciones ";
        sql += " from Tramites T ";
        sql += " inner join Contenedores C on (C.IdTramite=T.Tramite) ";
        //sql+=" inner join Manifiesto M on (M.Tramite=T.NombreArchivo and M.Estado=1) ";
        sql += " inner join Codigos Co on (Co.Codigo=T.Regimen and Co.Categoria='Regimen' and Co.Eliminado=0) ";
        sql += " inner join TiemposMaersk TM on (TM.IdTramite=T.Tramite and TM.IdFlujo=0 and TM.Eliminado=0) ";
        sql += " left join TiemposMaersk TM1 on (TM1.IdTramite=T.Tramite and TM1.IdFlujo=1 and TM1.Eliminado=0) ";
        sql += " left join TiemposMaersk TM2 on (TM2.IdTramite=T.Tramite and TM2.IdFlujo=2 and TM2.Eliminado=0) ";
        sql += " where T.Barco='" + barco + "' and T.Viaje='" + viaje.ToString() + "' and T.Estado not in (8,9) ";
        sql += " order by T.Tramite ";
        this.loadSQL(sql);
    }

    public void CargarTramitesXFecha(string FechaInicio, string FechaFinal)
    {
        string sql = "Select T.Tramite as Tramite,T.Consignatario as Consignatario,B.Nombre as Barco,T.Viaje as Viaje,T.BL as BL,C.Contenedor as Contenedor,M.Registro as Manifiesto, ";
        sql += " case when (T.Regimen in('100','101') and T.Estado<>'0') ";
        sql += " then T.Preimpreso ";
        sql += " when (T.Regimen is not null) ";
        sql += " then T.SARA else	'-' end as 'NumRegistro', ";
        sql += " Co.Descripcion as TipoTramite, ";
        //sql+=" TM.Fecha as ETA,isnull(convert(varchar(25),TM1.Fecha,21),'-') as InicioTramite,isnull(CONVERT(varchar(25),TM2.fecha,21),'-')as FinTramite ";
        sql += " TM.Fecha as ETA,TM1.Fecha as InicioTramite,TM2.fecha as FinTramite, ";
        sql += " (TM.Observacion+'. '+TM1.Observacion+'. '+TM2.Observacion) as Observaciones,T.SARA,T.Factura,T.Proveedor ";
        sql += " from Tramites T ";
        sql += " inner join Barcos B on (B.Codigo=T.Barco and B.Eliminado=0) ";
        sql += " inner join Contenedores C on (C.IdTramite=T.Tramite) ";
        sql += " inner join Manifiesto M on (M.Tramite=T.NombreArchivo and M.Estado=1) ";
        sql += " inner join Codigos Co on (Co.Codigo=T.Regimen and Co.Categoria='Regimen' and Co.Eliminado=0) ";
        sql += " inner join TiemposMaersk TM on (TM.IdTramite=T.Tramite and TM.IdFlujo=0 and TM.Eliminado=0) ";
        sql += " left join TiemposMaersk TM1 on (TM1.IdTramite=T.Tramite and TM1.IdFlujo=1 and TM1.Eliminado=0) ";
        sql += " left join TiemposMaersk TM2 on (TM2.IdTramite=T.Tramite and TM2.IdFlujo=2 and TM2.Eliminado=0) ";
        sql += " where T.Estado not in (8,9) and CAST(T.Fecha as DATE) between '" + FechaInicio + "' AND '" + FechaFinal + "'";
        sql += " order by T.Viaje DESC ";
        this.loadSQL(sql);
    }

    public void CargarTramitesxBarcoyViaje(string Barco, string Numviaje)
    {
        string sql = "Select T.Tramite as Tramite,t.Consignatario as Consignatario,T.Factura as NumFactura,C.Contenedor as Contenedor,T.BL as BL,T.Preimpreso as Poliza, ";
        sql += " T.SARA as NumRegistro,B.Nombre as Vapor ";
        sql += " from Tramites T ";
        sql += " inner join Barcos B on (B.Codigo=T.Barco and B.Eliminado=0) ";
        sql += " inner join Contenedores C on (C.IdTramite=T.Tramite) ";
        sql += " where T.Barco='" + Barco + "' and T.Viaje='" + Numviaje + "' and T.Estado=4 and T.Manifiesto=1";
        this.loadSQL(sql);
    }

    public string ID
    {
        get
        {
            return (string)registro[Campos.ID].ToString();
        }
        set
        {
            registro[Campos.ID] = value;
        }
    }

    public string IDARCHIVO
    {
        get
        {
            return (string)registro[Campos.IDARCHIVO].ToString();
        }
        set
        {
            registro[Campos.IDARCHIVO] = value;
        }
    }

    public string TRAMITE
    {
        get
        {
            return (string)registro[Campos.TRAMITE].ToString();
        }
        set
        {
            registro[Campos.TRAMITE] = value;
        }
    }

    public string NOMBREARCHIVO
    {
        get
        {
            return (string)registro[Campos.NOMBREARCHIVO].ToString();
        }
        set
        {
            registro[Campos.NOMBREARCHIVO] = value;
        }
    }

    public string BARCO
    {
        get
        {
            return (string)registro[Campos.BARCO].ToString();
        }
        set
        {
            registro[Campos.BARCO] = value;
        }
    }

    public string VIAJE
    {
        get
        {
            return (string)registro[Campos.VIAJE].ToString();
        }
        set
        {
            registro[Campos.VIAJE] = value;
        }
    }

    public string REGIMEN
    {
        get
        {
            return (string)registro[Campos.REGIMEN].ToString();
        }
        set
        {
            registro[Campos.REGIMEN] = value;
        }
    }

    public string SARA
    {
        get
        {
            return (string)registro[Campos.SARA].ToString();
        }
        set
        {
            registro[Campos.SARA] = value;
        }
    }

    public string PREIMPRESO
    {
        get
        {
            return (string)registro[Campos.PREIMPRESO].ToString();
        }
        set
        {
            registro[Campos.PREIMPRESO] = value;
        }
    }

    public string BL
    {
        get
        {
            return (string)registro[Campos.BL].ToString();
        }
        set
        {
            registro[Campos.BL] = value;
        }
    }

    public string CONSOLIDADO
    {
        get
        {
            return (string)registro[Campos.CONSOLIDADO].ToString();
        }
        set
        {
            registro[Campos.CONSOLIDADO] = value;
        }
    }

    public string PROVEEDOR
    {
        get
        {
            return (string)registro[Campos.PROVEEDOR].ToString();
        }
        set
        {
            registro[Campos.PROVEEDOR] = value;
        }
    }

    public string CONSIGNATARIO
    {
        get
        {
            return (string)registro[Campos.CONSIGNATARIO].ToString();
        }
        set
        {
            registro[Campos.CONSIGNATARIO] = value;
        }
    }

    public string NOTIFICAR
    {
        get
        {
            return (string)registro[Campos.NOTIFICAR].ToString();
        }
        set
        {
            registro[Campos.NOTIFICAR] = value;
        }
    }

    public string TRANSBORDO
    {
        get
        {
            return (string)registro[Campos.TRANSBORDO].ToString();
        }
        set
        {
            registro[Campos.TRANSBORDO] = value;
        }
    }

    public string IMPORTEFLETE
    {
        get
        {
            return (string)registro[Campos.IMPORTEFLETE].ToString();
        }
        set
        {
            registro[Campos.IMPORTEFLETE] = value;
        }
    }

    public string FRACCIONADO
    {
        get
        {
            return (string)registro[Campos.FRACCIONADO].ToString();
        }
        set
        {
            registro[Campos.FRACCIONADO] = value;
        }
    }

    public string MARCAS
    {
        get
        {
            return (string)registro[Campos.MARCAS].ToString();
        }
        set
        {
            registro[Campos.MARCAS] = value;
        }
    }

    public string DESCRIPCION
    {
        get
        {
            return (string)registro[Campos.DESCRIPCION].ToString();
        }
        set
        {
            registro[Campos.DESCRIPCION] = value;
        }
    }

    public string CLASEBULTO
    {
        get
        {
            return (string)registro[Campos.CLASEBULTO].ToString();
        }
        set
        {
            registro[Campos.CLASEBULTO] = value;
        }
    }

    public string CANTIDADBULTO
    {
        get
        {
            return (string)registro[Campos.CANTIDADBULTO].ToString();
        }
        set
        {
            registro[Campos.CANTIDADBULTO] = value;
        }
    }

    public string MERCADERIA
    {
        get
        {
            return (string)registro[Campos.MERCADERIA].ToString();
        }
        set
        {
            registro[Campos.MERCADERIA] = value;
        }
    }

    public string UNIDADPESO
    {
        get
        {
            return (string)registro[Campos.UNIDADPESO].ToString();
        }
        set
        {
            registro[Campos.UNIDADPESO] = value;
        }
    }

    public string PESOBRUTO
    {
        get
        {
            return (string)registro[Campos.PESOBRUTO].ToString();
        }
        set
        {
            registro[Campos.PESOBRUTO] = value;
        }
    }

    public string CONTENEDOR
    {
        get
        {
            return (string)registro[Campos.CONTENEDOR].ToString();
        }
        set
        {
            registro[Campos.CONTENEDOR] = value;
        }
    }

    public string PRECINTOS
    {
        get
        {
            return (string)registro[Campos.PRECINTOS].ToString();
        }
        set
        {
            registro[Campos.PRECINTOS] = value;
        }
    }

    public string DIMENSIONES
    {
        get
        {
            return (string)registro[Campos.DIMENSIONES].ToString();
        }
        set
        {
            registro[Campos.DIMENSIONES] = value;
        }
    }

    public string TIPOCARGA
    {
        get
        {
            return (string)registro[Campos.TIPOCARGA].ToString();
        }
        set
        {
            registro[Campos.TIPOCARGA] = value;
        }
    }

    public string VACIO
    {
        get
        {
            return (string)registro[Campos.VACIO].ToString();
        }
        set
        {
            registro[Campos.VACIO] = value;
        }
    }

    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }

    public string ESTADO
    {
        get
        {
            return (string)registro[Campos.ESTADO].ToString();
        }
        set
        {
            registro[Campos.ESTADO] = value;
        }
    }

    public string DECLARACIONES
    {
        get
        {
            return (string)registro[Campos.DECLARACIONES].ToString();
        }
        set
        {
            registro[Campos.DECLARACIONES] = value;
        }
    }

    public string MANIFIESTO
    {
        get
        {
            return (string)registro[Campos.MANIFIESTO].ToString();
        }
        set
        {
            registro[Campos.MANIFIESTO] = value;
        }
    }

    public string FACTURA
    {
        get
        {
            return (string)registro[Campos.FACTURA].ToString();
        }
        set
        {
            registro[Campos.FACTURA] = value;
        }
    }
}