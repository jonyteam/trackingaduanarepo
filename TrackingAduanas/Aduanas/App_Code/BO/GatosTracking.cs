using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for EventosBO
/// </summary>
public class GatosTracking : CapaBase
{

    class Campos
    {

        public static string IDGASTO = "IdGasto";
        public static string GASTO = "Gasto";
        public static string CUENTA = "Cuenta";
        public static string BALANCE = "Balance";
        public static string CODPAIS = "CodPais";
        public static string ESTADO = "Estado";
        public static string IMPUESTO = "Impuesto";
        public static string BALANCE2 = "Balance2";
    }

    public GatosTracking(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from GatosTracking";
        this.initializeSchema("Eventos");
    }

    public void loadAutorizaciones()
    {
        string sql = this.coreSQL;
        sql += " where Estado = '0'";
        this.loadSQL(sql);
    }
    #region QUERYS

    public void LoadGatos(string IdGasto)
    {
        string sql = "SELECT * FROM GatosTracking Where IdGasto = '" + IdGasto + "' and Estado = 0";
        this.loadSQL(sql);
    }


    public void LoadGatosXNombre(string Material)
    {
        string sql = "SELECT * FROM GatosTracking Where Gasto = '" + Material + "' and Estado = 0";
        this.loadSQL(sql);
    }
    public void LoadTodosGatos(string IdGasto)
    {
        string sql = "SELECT * FROM GatosTracking Where IdGasto = '" + IdGasto + "'";
        this.loadSQL(sql);
    }
    public void LoadGatosXNombre(string Material, string CodPais)
    {
        string sql = "SELECT * FROM GatosTracking Where Gasto = '" + Material + "' and Estado = 0 and CodPais = '" + CodPais + "'";
        this.loadSQL(sql);
    }
    public void VerificarXGuardar(string Pais, string Cuenta, string Balance, string Gasto)
    {
        string sql = "SELECT * FROM GatosTracking Where CodPais = '" + Pais + "' and Cuenta = '" + Cuenta + "' and Balance = '" + Balance + "' and Gasto like '%" + Gasto + "%' and Estado = 0";
        this.loadSQL(sql);
    }
    public void LlenarGatos(string Pais)
    {
        string sql = "SELECT Gasto,IdGasto FROM GatosTracking Where CodPais = '" + Pais + "' and Estado = 0 order by 1";
        this.loadSQL(sql);
    }
    public void LlenarGatosMapeo(string Pais)
    {
        string sql = "SELECT IdGasto,Gasto,Balance FROM GatosTracking Where CodPais = '" + Pais + "' and Estado = 0";
        this.loadSQL(sql);
    }
    public void LlenarGatosXAdministrador()
    {
        string sql = "SELECT Gasto,Cuenta as Material,Balance ";
        sql += ",Case CodPais When 'N' Then 'Nicaragua' When 'G' Then 'Guatemala' Else 'El Salvador' End as Pais ";
        sql += ",CodPais ,Case Estado When 0 Then 'Habilitado' else 'Deshabilitado' End as Estado ";
        sql += ",Impuesto as CodImpuesto,Case Impuesto When 0 Then 'NO' Else 'SI' End as Impuesto ,Balance2,IdGasto ";
        sql += "FROM GatosTracking";
        this.loadSQL(sql);
    }
    public void LlenarHojasConGastosMant()
    {
        string sql = @"Select Distinct A.IdInstruccion as HojaRuta, A.Id, G.Nombre as Clientes, C.Gasto as Material,C.IdGasto 
        ,E.Nombre as Proveedor, E.Cuenta,A.Monto as Monto,A.IVA as IVA,A.TotalVenta as ValorNegociado 
        , D.Descripcion as MediodePago, A.MedioPago From DetalleGastos A 
        INNER JOIN GatosTracking C on (A.IdGasto = C.IdGasto and C.CodPais = A.CodPais ) 
        INNER JOIN Codigos D on (D.Categoria = 'MEDIOPAGO' and D.Codigo = A.MedioPago) 
        INNER JOIN ProveedoresSolicitud E on (E.Cuenta= A.Proveedor) 
        LEFT JOIN Instrucciones B on (A.IdInstruccion = B.IdInstruccion and B.CodEstado != 100) 
        LEFT JOIN Clientes G on (B.IdCliente = G.CodigoSAP and G.Eliminado = 0) 
        Where  A.Estado not in (99,10) and C.Estado = 0 and A.CodPais !='H' and DATEADD(MONTH,4,A.FechaCreacion) >= GETDATE() order by A.MedioPago";
        this.loadSQL(sql);
    }
    public void LlenarHojasConGastosMantXId(string Id)
    {
        string sql = @"Select Distinct A.IdInstruccion as HojaRuta, A.Id, G.Nombre as Clientes, C.Gasto as Material,C.IdGasto 
        ,E.Nombre as Proveedor, E.Cuenta,A.Monto as Monto,A.IVA as IVA,A.TotalVenta as ValorNegociado 
        , D.Descripcion as MediodePago, A.MedioPago From DetalleGastos A 
        INNER JOIN GatosTracking C on (A.IdGasto = C.IdGasto and C.CodPais = A.CodPais ) 
        INNER JOIN Codigos D on (D.Categoria = 'MEDIOPAGO' and D.Codigo = A.MedioPago) 
        INNER JOIN ProveedoresSolicitud E on (E.Cuenta= A.Proveedor) 
        LEFT JOIN Instrucciones B on (A.IdInstruccion = B.IdInstruccion and B.CodEstado != 100) 
        LEFT JOIN Clientes G on (B.IdCliente = G.CodigoSAP and G.Eliminado = 0) 
        Where  A.Estado not in (99,10) and C.Estado = 0 and A.CodPais !='H' and DATEADD(MONTH,1,A.FechaCreacion) >= GETDATE() and Id = '" + Id + "'order by A.MedioPago";
        this.loadSQL(sql);
    }
    public void loadAutorizacionesEstado()
    {
        this.loadSQL(this.coreSQL);
    }

    public void mostrardatos(string hojaruta)
    {
        string sql = "  Select C.Descripcion ,FechaInicio,FechaFin,ObservacionFin,ObservacionInicio,CL.Nombre,I.NumeroFactura,I.ReferenciaCliente from Codigos C";
        sql += " Inner Join Eventos E on (IdInstruccion ='" + hojaruta + "' and C.Codigo = IdEvento and C.Categoria = 'EVENTOS' and E.Eliminado = 1)";
        sql += " Inner Join Instrucciones I on (E.IdInstruccion = I.IdInstruccion and I.CodEstado != 100)";
        sql += " Inner Join Clientes CL on (I.IdCliente = CL.CodigoSAP)";
        this.loadSQL(sql);
    }
    public void mostrardatos2(string hojaruta)
    {
        string sql = "SELECT ISNULL(FC.EstadoFlujo, 'Comienzo de Flujo/Doc Minimos') as GestionCarga, GC.Fecha as FechaGestor, I.Color,GC.Observacion as Observacion from GestionCarga GC";
        sql += " left join FlujoCarga FC on (GC.IdEstadoFlujo = FC.IdEstadoFlujo) inner join Instrucciones I on ('" + hojaruta + "' = I.IdInstruccion) where GC.IdInstruccion = '" + hojaruta + "' order by FC.Orden";
        this.loadSQL(sql);
    }
    public void loadAutorizacionesRoles(string idRol)
    {
        string sql = this.coreSQL;
        sql += " where IdRol = " + idRol + " and Estado = '0'";
        this.loadSQL(sql);
    }
    public void loadAutorizacionesModulo(string idModulo)
    {
        string sql = this.coreSQL;
        sql += " where IdModulo = " + idModulo + " and Estado = '0'";
        this.loadSQL(sql);
    }
    public void loadAutorizacionesModulosRoles(string idRol, string idModulo)
    {
        string sql = this.coreSQL;
        sql += " where IdRol = " + idRol + " and IdModulo = " + idModulo + " and Estado = '0'";
        this.loadSQL(sql);
    }

    public void loadAutorizacionesModulosRolesEstado(string idRol, string idModulo)
    {
        string sql = this.coreSQL;
        sql += " where IdRol = " + idRol + " and IdModulo = " + idModulo;
        this.loadSQL(sql);
    }

    public void deleAutorizacionesModulos(string idRol, string idModulo)
    {
        string sql = this.coreSQL;
        sql += " where IdRole = " + idRol + " and IdModulo = " + idModulo;
        this.executeCustomSQL(String.Format("delete from AutorizacionesModulo where IdRol={0} and IdModulo={1}", idRol, idModulo));
    }

    public int eliminarAutorizacionesModulo(string codigo)
    {
        return this.executeCustomSQL("delete from AutorizacionesModulo where IdRol = " + codigo);
    }

    public int darBajaAutorizacionesModulo(string codigo)
    {
        return this.executeCustomSQL("update AutorizacionesModulo set Estado = '1' where IdRol = " + codigo);
    }
    #endregion




    #region Campos

    public string IMPUESTO
    {
        get
        {
            return (string)registro[Campos.IMPUESTO].ToString();
        }
        set
        {
            registro[Campos.IMPUESTO] = value;
        }
    }
    public string BALANCE
    {
        get
        {
            return (string)registro[Campos.BALANCE].ToString();
        }
        set
        {
            registro[Campos.BALANCE] = value;
        }
    }
    public string IDGASTO
    {
        get
        {
            return (string)registro[Campos.IDGASTO].ToString();
        }
        set
        {
            registro[Campos.IDGASTO] = value;
        }
    }
    public string GASTO
    {
        get
        {
            return (string)registro[Campos.GASTO].ToString();
        }
        set
        {
            registro[Campos.GASTO] = value;
        }
    }
    public string CODPAIS
    {
        get
        {
            return (string)registro[Campos.CODPAIS].ToString();
        }
        set
        {
            registro[Campos.CODPAIS] = value;
        }
    }
    public string ESTADO
    {
        get
        {
            return (string)registro[Campos.ESTADO].ToString();
        }
        set
        {
            registro[Campos.ESTADO] = value;
        }
    }
    public string CUENTA
    {
        get
        {
            return (string)registro[Campos.CUENTA].ToString();
        }
        set
        {
            registro[Campos.CUENTA] = value;
        }
    }
    public string BALANCE2
    {
        get
        {
            return (string)registro[Campos.BALANCE2].ToString();
        }
        set
        {
            registro[Campos.BALANCE2] = value;
        }
    }
    #endregion


}
