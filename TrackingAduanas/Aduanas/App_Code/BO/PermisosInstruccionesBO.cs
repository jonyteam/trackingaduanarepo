using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for PermisosInstruccionesBO
/// </summary>
public class PermisosInstruccionesBO : CapaBase
{
    class Campos
    {
        public static string IDINSTRUCCION = "IdInstruccion";
        public static string CODPERMISO = "CodPermiso";
        public static string NECESARIO = "Necesario";
        public static string PERMISONO = "PermisoNo";
        public static string OBSERVACION = "Observacion";
        public static string FECHA = "Fecha";
        public static string ELIMINADO = "Eliminado";
        public static string IDUSUARIO = "IdUsuario";
    }

    public PermisosInstruccionesBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from PermisosInstrucciones PI ";
        this.initializeSchema("PermisosInstrucciones");
    }

    public void loadPermisosInstrucciones()
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' ";
        this.loadSQL(sql);
    }

    public void loadPermisosInstrucciones(string idInstruccion, string codPermiso)
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' AND IdInstruccion = '" + idInstruccion + "' AND CodPermiso = '" + codPermiso + "' ";
        this.loadSQL(sql);
    }

    public void loadPermisosInstruccionesXInstruccion(string idInstruccion)
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' AND IdInstruccion = '" + idInstruccion + "' ";
        this.loadSQL(sql);
    }

    public int inicializarPermiso(string idInstruccion, string codPermiso)
    {
        string sql = " UPDATE PermisosInstrucciones SET Necesario = NULL, PermisoNo = NULL, Observacion = NULL, Fecha = NULL, IdUsuario = NULL ";
        sql += " WHERE IdInstruccion = '" + idInstruccion + "' AND CodPermiso = '" + codPermiso + "' ";
        return this.executeCustomSQL(sql);
    }

    #region Campos
    public string IDINSTRUCCION
    {
        get
        {
            return (string)registro[Campos.IDINSTRUCCION].ToString();
        }
        set
        {
            registro[Campos.IDINSTRUCCION] = value;
        }
    }

    public string CODPERMISO
    {
        get
        {
            return (string)registro[Campos.CODPERMISO].ToString();
        }
        set
        {
            registro[Campos.CODPERMISO] = value;
        }
    }

    public string NECESARIO
    {
        get
        {
            return (string)registro[Campos.NECESARIO].ToString();
        }
        set
        {
            registro[Campos.NECESARIO] = value;
        }
    }

    public string PERMISONO
    {
        get
        {
            return (string)registro[Campos.PERMISONO].ToString();
        }
        set
        {
            registro[Campos.PERMISONO] = value;
        }
    }

    public string OBSERVACION
    {
        get
        {
            return (string)registro[Campos.OBSERVACION].ToString();
        }
        set
        {
            registro[Campos.OBSERVACION] = value;
        }
    }

    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }

    public string ELIMINADO
    {
        get
        {
            return (string)registro[Campos.ELIMINADO].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }
    #endregion
}
