using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for HojaRutaDatosBO
/// </summary>
public class HojaRutaDatosBO1 : CapaBase
{
    class Campos
    {
        public static string IDHOJARUTA = "IdHojaRuta";
        public static string CODIGO = "Codigo";
        public static string DATOS = "Datos";
    }

    public HojaRutaDatosBO1(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from HojaRutaDatos ";
        this.initializeSchema("HojaRutaDatos");
    }

    public void loadAllAduanas()
    {
        string sql = coreSQL;
        sql += " WHERE CodEstado = '0' ORDER BY CodPais DESC, CodigoAduana ASC ";
        this.loadSQL(sql);
    }

    #region campos
    public string IDHOJARUTA
    {
        get
        {
            return (string)registro[Campos.IDHOJARUTA].ToString();
        }
        set
        {
            registro[Campos.IDHOJARUTA] = value;
        }
    }

    public string CODIGO
    {
        get
        {
            return (string)registro[Campos.CODIGO].ToString();
        }
        set
        {
            registro[Campos.CODIGO] = value;
        }
    }

    public string DATOS
    {
        get
        {
            return (string)registro[Campos.DATOS].ToString();
        }
        set
        {
            registro[Campos.DATOS] = value;
        }
    }
    #endregion

}
