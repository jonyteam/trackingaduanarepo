using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for EspeciesFiscalesBO
/// </summary>
public class EspeciesFiscalesBO : CapaBase
{
    class Campos
    {
        public static string IDESPECIEFISCAL = "IdEspecieFiscal";
        public static string ESPECIESFISCALES = "EspecieFiscal";
        public static string FECHA = "Fecha";
        public static string CODESTADO = "CodEstado";
        public static string IDUSUARIO = "IdUsuario";
    }

    public EspeciesFiscalesBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from EspeciesFiscales ";
        this.initializeSchema("EspeciesFiscales");
    }

    public void loadEspeciesFiscales()
    {
        this.loadSQL(this.coreSQL + " WHERE CodEstado = '0' ");
    }

    public void loadEspeciesFiscales(string especieFiscal)
    {
        string sql = this.coreSQL;
        sql += " WHERE EspecieFiscal = '" + especieFiscal + "' AND CodEstado = '0' ";
        this.loadSQL(sql);
    }

    public void loadNuevoId(string codigo)
    {
        string sql = "declare @Id varchar(50)";
        sql += " select @Id = (SELECT NuevoId = MAX(CAST(LEFT(IdEspecieFiscal,3)AS varchar(5))) +";
        sql += " RIGHT('' + CAST(MAX(CAST(RIGHT(IdEspecieFiscal,LEN(IdEspecieFiscal)- 3)+ 1 AS int))AS varchar(50)),50)";
        sql += " FROM EspeciesFiscales";
        sql += " Where IdEspecieFiscal like '" + codigo + "%')";
        sql += " select @Id as NuevoId";
        this.loadSQL(sql);
    }


    #region campos
    public string IDESPECIEFISCAL
    {
        get
        {
            return (string)registro[Campos.IDESPECIEFISCAL].ToString();
        }
        set
        {
            registro[Campos.IDESPECIEFISCAL] = value;
        }
    }

    public string ESPECIESFISCALES
    {
        get
        {
            return (string)registro[Campos.ESPECIESFISCALES].ToString();
        }
        set
        {
            registro[Campos.ESPECIESFISCALES] = value;
        }
    }

    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }

    public string CODESTADO
    {
        get
        {
            return (string)registro[Campos.CODESTADO].ToString();
        }
        set
        {
            registro[Campos.CODESTADO] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }
    #endregion
}
