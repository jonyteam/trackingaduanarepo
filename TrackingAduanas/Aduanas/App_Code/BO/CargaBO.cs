using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for EventosBO
/// </summary>
public class CargaBO: CapaBase
{

    class Campos
    {

    public static string IDCARGA = "IdCarga";
    public static string FECHA = "Fecha";
    public static string USUARIO = "Usuario";

    }

    public CargaBO(GrupoLis.Login.Login log)
        : base(log, true)
	{
        this.coreSQL = "Select * from Carga C";
        this.initializeSchema("DetalleGastos");
    }

    public void loadAutorizaciones()
    {
        string sql = this.coreSQL;
        sql += " where Estado = '0'";
        this.loadSQL(sql);
    }
     #region QUERYS

    public void UltimoResgitros()
    {
        string sql = "SELECT TOP 1 * FROM Carga order by IdCarga DESC";
        this.loadSQL(sql);
    }

    public void UltimoResgitros( string Id )
    {
        string sql = "SELECT * FROM Carga where IdCarga = '"+ Id +"'";
        this.loadSQL(sql);
    }
    public void BuscarHoja(string Hoja)
    {
        string sql = "SELECT * FROM DetalleGastos Where IdInstruccion = '" + Hoja + "' and Estado = 0";
        this.loadSQL(sql);
    }
    public void LlenarGatosXHojaYGasto(string IdInstruccion, string IdGasto)
    {
        string sql = "SELECT * FROM DetalleGastos Where IdInstruccion = '" + IdInstruccion + "' and IdGasto = '" + IdGasto + "' and Estado = 0";
        this.loadSQL(sql);
    }
    public void LlenarGatosXId(string Id)
    {
        string sql = "SELECT * FROM DetalleGastos Where Id = '" + Id + "' and Estado = 1";
        this.loadSQL(sql);
    }
    public void loadAutorizacionesEstado()
    {
        this.loadSQL(this.coreSQL);
    }

    public void mostrardatos(string hojaruta)
    {
        string sql = "  Select C.Descripcion ,FechaInicio,FechaFin,ObservacionFin,ObservacionInicio,CL.Nombre,I.NumeroFactura,I.ReferenciaCliente from Codigos C";
               sql += " Inner Join Eventos E on (IdInstruccion ='" + hojaruta + "' and C.Codigo = IdEvento and C.Categoria = 'EVENTOS' and E.Eliminado = 1)";
               sql +=" Inner Join Instrucciones I on (E.IdInstruccion = I.IdInstruccion and I.CodEstado != 100)";
               sql +=" Inner Join Clientes CL on (I.IdCliente = CL.CodigoSAP)";
        this.loadSQL(sql);
    }

    public void mostrardatos2(string hojaruta)
    {
        string sql = "SELECT ISNULL(FC.EstadoFlujo, 'Comienzo de Flujo/Doc Minimos') as GestionCarga, GC.Fecha as FechaGestor, I.Color,GC.Observacion as Observacion from GestionCarga GC";
        sql += " left join FlujoCarga FC on (GC.IdEstadoFlujo = FC.IdEstadoFlujo) inner join Instrucciones I on ('" + hojaruta + "' = I.IdInstruccion) where GC.IdInstruccion = '" + hojaruta + "' order by FC.Orden";
        this.loadSQL(sql);
    }

    public void loadAutorizacionesRoles(string idRol)
    {
        string sql = this.coreSQL;
        sql += " where IdRol = " + idRol + " and Estado = '0'";
        this.loadSQL(sql);
    }


    public void loadAutorizacionesModulo(string idModulo)
    {
        string sql = this.coreSQL;
        sql += " where IdModulo = " + idModulo + " and Estado = '0'";
        this.loadSQL(sql);
    }

    public void loadAutorizacionesModulosRoles(string idRol, string idModulo)
    {
        string sql = this.coreSQL;
        sql += " where IdRol = " + idRol + " and IdModulo = " + idModulo + " and Estado = '0'";
        this.loadSQL(sql);
    }

    public void loadAutorizacionesModulosRolesEstado(string idRol, string idModulo)
    {
        string sql = this.coreSQL;
        sql += " where IdRol = " + idRol + " and IdModulo = " + idModulo;
        this.loadSQL(sql);
    }

    public void deleAutorizacionesModulos(string idRol, string idModulo)
    {
        string sql = this.coreSQL;
        sql += " where IdRole = " + idRol + " and IdModulo = " + idModulo;
        this.executeCustomSQL(String.Format("delete from AutorizacionesModulo where IdRol={0} and IdModulo={1}", idRol, idModulo));
    }


    public void loadInstruccionesSolicitudPagoCXP()
    {
        string sql = " Select A.IdInstruccion as HojaRuta, G.Nombre as Clientes , A.Id, C.Gasto as Material ,C.Cuenta as CuentaSAP";
               sql += " ,E.Descripcion as Proveedor,A.Monto as Monto,A.IVA as IVA,A.Total as Total ";
               sql += " ,F.Descripcion as Moneda, D.Descripcion as MedioPago From DetalleGastos A";
               sql += " INNER JOIN Instrucciones B on (A.IdInstruccion = B.IdInstruccion)";
               sql += " INNER JOIN Clientes G on (B.IdCliente = G.CodigoSAP and G.Eliminado = 0)";
               sql += " INNER JOIN GatosTracking C on (A.IdGasto = C.IdGasto and C.CodPais = A.CodPais)";
               sql += " INNER JOIN Codigos D on (D.Categoria = 'MEDIOPAGO' and D.Codigo = A.MedioPago)";
               sql += " INNER JOIN Codigos E on (E.Categoria = 'PROVEEDOR' and E.Codigo = A.Proveedor)";
               sql += " INNER JOIN Codigos F on (F.Categoria = 'MONEDA' and F.Codigo = A.Moneda)";
               sql += " Where B.CodEstado != 100 and A.Estado = 1 and C.Estado = 0 and A.MedioPago in ('TR','CH') order by A.MedioPago";
        this.loadSQL(sql);
    }

    public int eliminarAutorizacionesModulo(string codigo)
    {
        return this.executeCustomSQL("delete from AutorizacionesModulo where IdRol = " + codigo);
    }

    public int darBajaAutorizacionesModulo(string codigo)
    {
        return this.executeCustomSQL("update AutorizacionesModulo set Estado = '1' where IdRol = " + codigo);
    }
     #endregion

     #region Campos
    public Int32 IDCARGA
    {
        get
        {
            return Convert.ToInt32(registro[Campos.IDCARGA].ToString());
        }
        set
        {
            registro[Campos.IDCARGA] = value;
        }
    }
    public string USUARIO
    {
        get
        {
            return (string)registro[Campos.USUARIO].ToString();
        }
        set
        {
            registro[Campos.USUARIO] = value;
        }
    }
    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }
    #endregion
}
