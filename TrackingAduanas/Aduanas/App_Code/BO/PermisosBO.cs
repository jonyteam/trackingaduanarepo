using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for PermisosBO
/// </summary>
public class PermisosBO : CapaBase
{
    class Campos
    {
        public static string IDPERMISO = "IdPermiso";
        public static string DESCRIPCION = "Descripcion";
        public static string ELIMINADO = "Eliminado";
    }

    public PermisosBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from Permisos";
        this.initializeSchema("Permisos");
    }

    public void loadPermisos()
    {
        string sql = this.coreSQL;
        sql += " where Eliminado = '0'";
        sql += " order by Descripcion";
        this.loadSQL(sql);
    }

    public void loadPermisos(string idPermiso)
    {
        string sql = this.coreSQL;
        sql += " where IdPermiso = " + idPermiso + " and Eliminado = '0'";
        this.loadSQL(sql);
    }

    public string IDPERMISO
    {
        get
        {
            return (string)registro[Campos.IDPERMISO].ToString();
        }
    }

    public string DESCRIPCION
    {
        get
        {
            return (string)registro[Campos.DESCRIPCION].ToString();
        }
        set
        {
            registro[Campos.DESCRIPCION] = value;
        }
    }

    public string ELIMINADO
    {
        get
        {
            return (string)registro[Campos.ELIMINADO].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO] = value;
        }
    }
}
