﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for RemisionesBO
/// </summary>
public class TiempoFlujosBO : CapaBase
{
    class Campos
    {
        public static string CORRELATIVO = "Correlativo";
        public static string CODIGOGUIA = "CodigoGuia";
        public static string CODFLUJO = "CodFlujo";
        public static string CODIGOADUANA = "CodigoAduana";
        public static string FECHA = "Fecha";
        public static string OBSERVACION = "Observacion";
        public static string CORRELATIVOGUIA = "CorrelativoGuia";
        public static string ELIMINADO = "Eliminado";
        public static string IDUSUARIO = "IdUsuario";
        public static string FECHASISTEMA = "FechaSistema";

    }

    public TiempoFlujosBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from HojaRuta";
        this.initializeSchema("HojaRuta");
    }

    public void AgregarTiempo()
    {
        string sql = "Select * From [TrackingTerrestreNueva].[dbo].[TiempoFlujos]Where CodigoGuia = '1'";
        this.loadSQL(sql);
    }


    #region Campos
    public string CORRELATIVO
    {
        get
        {
            return (string)registro[Campos.CORRELATIVO].ToString();
        }
    }

    public string CODIGOGUIA
    {
        get
        {
            return (string)registro[Campos.CODIGOGUIA].ToString();
        }
        set
        {
            registro[Campos.CODIGOGUIA] = value;
        }
    }

    public string CODFLUJO
    {
        get
        {
            return (string)registro[Campos.CODFLUJO].ToString();
        }
        set
        {
            registro[Campos.CODFLUJO] = value;
        }
    }

    public string CODIGOADUANA
    {
        get
        {
            return (string)registro[Campos.CODIGOADUANA].ToString();
        }
        set
        {
            registro[Campos.CODIGOADUANA] = value;
        }
    }

    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }

    public string OBSERVACION
    {
        get
        {
            return (string)registro[Campos.OBSERVACION].ToString();
        }
        set
        {
            registro[Campos.OBSERVACION] = value;
        }
    }

    public string CORRELATIVOGUIA
    {
        get
        {
            return (string)registro[Campos.CORRELATIVOGUIA].ToString();
        }
        set
        {
            registro[Campos.CORRELATIVOGUIA] = value;
        }
    }

    public string ELIMINADO
    {
        get
        {
            return (string)registro[Campos.ELIMINADO].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }

    public string FECHASISTEMA
    {
        get
        {
            return (string)registro[Campos.FECHASISTEMA].ToString();
        }
        set
        {
            registro[Campos.FECHASISTEMA] = value;
        }
    }
    #endregion
} // class HojaRutaBO

