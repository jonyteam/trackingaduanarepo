using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for EventosBO
/// </summary>
public class InstruccionesCargillCargaBO : CapaBase
{

    class Campos
    {

        public static string ID = "Id";
        public static string IDINSTRUCCION = "IdInstruccion";
        public static string TIPOCARGACARGILL = "TipoCargaCargill";
        public static string ESTADO = "Estado";
        public static string IDUSUARIO = "IdUsuario";
        public static string FECHACREACION = "FechaCreacion";

    }

    public InstruccionesCargillCargaBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from InstruccionesCargillCarga";
        this.initializeSchema("InstruccionesCargillCarga");
    }

    public void loadAutorizaciones()
    {
        string sql = this.coreSQL;
        sql += " where Estado = '0'";
        this.loadSQL(sql);
    }
    #region QUERYS

    public void VerificarRegistro(string IdInstruccion)
    {
        string sql = "SELECT * FROM InstruccionesCargillCarga Where IdInstruccion ='" + IdInstruccion + "'";
        this.loadSQL(sql);
    }
    #endregion

    #region Campos
    public Int32 ID
    {
        get
        {
            return Convert.ToInt32(registro[Campos.ID].ToString());
        }
        set
        {
            registro[Campos.ID] = value;
        }
    }
    public string IDINSTRUCCION
    {
        get
        {
            return (string)registro[Campos.IDINSTRUCCION].ToString();
        }
        set
        {
            registro[Campos.IDINSTRUCCION] = value;
        }
    }
    public string TIPOCARGACARGILL
    {
        get
        {
            return (string)registro[Campos.TIPOCARGACARGILL].ToString();
        }
        set
        {
            registro[Campos.TIPOCARGACARGILL] = value;
        }
    }
    public Int32 IDUSUARIO
    {
        get
        {
            return Convert.ToInt32(registro[Campos.IDUSUARIO].ToString());
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }
    public string ESTADO
    {
        get
        {
            return (string)registro[Campos.ESTADO].ToString();
        }
        set
        {
            registro[Campos.ESTADO] = value;
        }
    }
    public string FECHACREACION
    {
        get
        {
            return (string)registro[Campos.FECHACREACION].ToString();
        }
        set
        {
            registro[Campos.FECHACREACION] = value;
        }
    }
    #endregion
}
