using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for FlujoCargaBO
/// </summary>
public class FlujoCargaBO : CapaBase
{
    class Campos
    {
        public static string IDESTADOFLUJO = "IdEstadoFlujo";
        public static string CODESTADOFLUJOCARGA = "CodEstadoFlujoCarga";
        public static string ESTADOFLUJO = "EstadoFlujo";
        public static string CODPAIS = "CodPais";
        public static string CODREGIMEN = "CodRegimen";
        public static string OBLIGATORIO = "Obligatorio";
        public static string ORDEN = "Orden";
        public static string FECHA = "Fecha";
        public static string ELIMINADO = "Eliminado";
        public static string IDUSUARIO = "IdUsuario";
    }

    public FlujoCargaBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from FlujoCarga FC ";
        this.initializeSchema("FlujoCarga");
    }

    public void loadFlujoCarga()
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' ORDER BY CodPais DESC, Id ASC ";
        this.loadSQL(sql);
    }

    public void loadFlujoCargaUnico(string flujo)
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' and IdEstadoFlujo = '" + flujo + "'ORDER BY CodPais DESC, Id ASC ";
        this.loadSQL(sql);
    }

    public void loadFlujoCarga(string idEstado)
    {
        string sql = coreSQL;
        sql += " WHERE IdEstadoFlujo = '" + idEstado + "' AND Eliminado = '0' ";
        this.loadSQL(sql);
    }

    public void loadFlujoCargaXPais(string codPais)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodPais = '" + codPais + "' AND Eliminado = '0' ORDER BY Id ASC ";
        this.loadSQL(sql);
    }

    public void loadEstadoFlujoCargaXPais(string codPais)
    {
        string sql = " Select IdEstadoFlujo from FlujoCarga FC ";
        sql += " WHERE CodPais = '" + codPais + "' AND Eliminado = '0' ORDER BY Orden ASC ";
        this.loadSQL(sql);
    }

    public void loadEstadoFlujoCargaXPaisRegimen(string codPais, string codRegimen)
    {
        string sql = " Select IdEstadoFlujo from FlujoCarga FC ";
        sql += " WHERE CodPais = '" + codPais + "' AND CodRegimen = '" + codRegimen + "' AND Eliminado = '0' ORDER BY Orden ASC ";
        this.loadSQL(sql);
    }

    public void loadCodEstadoFlujoCarga(string codRegimen, string CodEstadoFlujoCarga, string pais)
    {
        string sql = " Select IdEstadoFlujo from FlujoCarga FC ";
        sql += " WHERE CodRegimen = '" + codRegimen + "' AND Eliminado = '0' And CodEstadoFlujoCarga = '" + CodEstadoFlujoCarga + "' and FC.CodPais ='" + pais + "' ORDER BY Orden ASC ";
        this.loadSQL(sql);
    }

    public void loadCodEstadoFlujoCargaXHoja(string codRegimen, string CodEstadoFlujoCarga, string pais)
    {
        string sql = " Select IdEstadoFlujo from FlujoCarga FC ";
        sql += " WHERE CodRegimen = '" + codRegimen + "' AND Eliminado = '0' And CodEstadoFlujoCarga = '" + CodEstadoFlujoCarga + "' and CodPais = '" + pais + "' ORDER BY Orden ASC ";
        this.loadSQL(sql);
    }

    public void loadFlujoCargaXEstadoFlujoyPais(string estadoFlujo, string codPais)
    {
        string sql = this.coreSQL;
        sql += " WHERE EstadoFlujo = '" + estadoFlujo + "' AND CodPais = '" + codPais + "' AND Eliminado = '0' ";
        this.loadSQL(sql);
    }

    public void loadFlujoCargaAll()
    {
        string sql = " Select FC.*, C1.Descripcion AS Pais, C2.Descripcion AS Regimen, C3.Descripcion AS EstadoFlujoCarga from FlujoCarga FC ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = FC.CodPais AND C1.Categoria = 'PAISES') ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = FC.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) ";
        sql += " INNER JOIN Codigos C3 ON (C3.Codigo = FC.CodEstadoFlujoCarga AND C3.Categoria = 'ESTADOSFLUJOCARGA') ";
        sql += " WHERE FC.Eliminado = '0' ORDER BY FC.CodPais DESC, FC.Id ASC ";
        this.loadSQL(sql);
    }

    public void loadNuevoId(string codigo)
    {
        string sql = "declare @Id varchar(50)";
        sql += " select @Id = (SELECT NuevoId = MAX(CAST(LEFT(IdEstadoFlujo,2)AS varchar(5))) +";
        sql += " RIGHT('' + CAST(MAX(CAST(RIGHT(IdEstadoFlujo,LEN(IdEstadoFlujo)-2)+ 1 AS int))AS varchar(50)),50)";
        sql += " FROM FlujoCarga";
        sql += " Where IdEstadoFlujo like '" + codigo + "%')";
        sql += " select @Id as NuevoId";
        this.loadSQL(sql);
    }
    public void restricionNicaragua(string codigo, string idInstruccion)
    {
        string sql = " SELECT *  FROM FlujoCarga A  Inner Join Instrucciones B on (A.IdEstadoFlujo = B.IdEstadoFlujo and B.IdInstruccion = '" + idInstruccion + "')";
        sql += " Where A.CodPais = 'N' and A.CodRegimen = '" + codigo + "' and CodEstadoFlujoCarga in (5)";
        this.loadSQL(sql);
    }
    public void LoadCanalSelectividad(string CodPais, string Regimen)
    {
        string sql = @" SELECT *  FROM FlujoCarga Where CodPais = '" + CodPais + "' and CodRegimen = '" + Regimen + "' and Eliminado = 0 and CodEstadoFlujoCarga = 3";
        this.loadSQL(sql);
    }

    #region campos
    public string IDESTADOFLUJO
    {
        get
        {
            return (string)registro[Campos.IDESTADOFLUJO].ToString();
        }
        set
        {
            registro[Campos.IDESTADOFLUJO] = value;
        }
    }

    public string CODESTADOFLUJOCARGA
    {
        get
        {
            return (string)registro[Campos.CODESTADOFLUJOCARGA].ToString();
        }
        set
        {
            registro[Campos.CODESTADOFLUJOCARGA] = value;
        }
    }

    public string ESTADOFLUJO
    {
        get
        {
            return (string)registro[Campos.ESTADOFLUJO].ToString();
        }
        set
        {
            registro[Campos.ESTADOFLUJO] = value;
        }
    }

    public string CODPAIS
    {
        get
        {
            return (string)registro[Campos.CODPAIS].ToString();
        }
        set
        {
            registro[Campos.CODPAIS] = value;
        }
    }

    public string CODREGIMEN
    {
        get
        {
            return (string)registro[Campos.CODREGIMEN].ToString();
        }
        set
        {
            registro[Campos.CODREGIMEN] = value;
        }
    }

    public string OBLIGATORIO
    {
        get
        {
            return (string)registro[Campos.OBLIGATORIO].ToString();
        }
        set
        {
            registro[Campos.OBLIGATORIO] = value;
        }
    }

    public Int32 ORDEN
    {
        get
        {
            return Convert.ToInt32(registro[Campos.ORDEN].ToString());
        }
        set
        {
            registro[Campos.ORDEN] = value;
        }
    }

    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }

    public string ELIMINADO
    {
        get
        {
            return (string)registro[Campos.ELIMINADO].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }
    #endregion

}
