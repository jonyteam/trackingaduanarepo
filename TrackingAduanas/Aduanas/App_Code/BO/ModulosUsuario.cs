using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public class ModulosUsuario
{
    private string[] modulos;
    public ModulosUsuario(string[] modulos)
	{
        this.modulos = modulos;
	}

    public bool isInModule(string nombreModulo)
    {
        for (int i = 0; i < modulos.Length; i++)
        {
            if (nombreModulo == modulos[i])
                return true;
        }
        return false;
    }

    public bool isContains(string palabra)
    {
        for (int i = 0; i < modulos.Length; i++)
        {
            if (modulos[i].Contains(palabra))
                return true;
        }
        return false;
    }

}
