using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for ClientesBO
/// </summary>
public class ClientesBO : CapaBase
{
    class Campos
    {
        public static string CODIGOCLIENTE = "CodigoCliente";
        public static string CODIGOSAP = "CodigoSAP";
        public static string NOMBRE = "Nombre";
        public static string RTN = "RTN";
        public static string DIRECCION = "Direccion";
        public static string OFICIALCUENTA = "OficialCuenta";
        public static string CONTACTO = "Contacto";
        public static string TELEFONO = "Telefono";
        public static string EMAIL = "eMail";
        public static string CODIGOPAIS = "CodigoPais";
        public static string FECHA = "Fecha";
        public static string ELIMINADO = "Eliminado";
        public static string IDUSUARIO = "IdUsuario";
        public static string CODIGOCLIENTEADUANAS = "CodigoClienteAduanas";
        public static string MENSAJE = "Mensaje";
        public static string PERMITIRREMISIONES = "PermitirRemisiones";
        public static string IDOFICIALCUENTA = "IdOficialCuenta";
    }

    public ClientesBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from Clientes ";
        this.initializeSchema("Clientes");
    }

    public void loadClientes()
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' order by Nombre";
        this.loadSQL(sql);
    }

    public void loadAllCamposClientes()
    {
        this.loadSQL(coreSQL + " WHERE Eliminado = '0' ORDER BY NombreEmpresa ");
    }

    public void loadAllCamposClientesXPais(string codPais)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodigoPais = '" + codPais + "' AND Eliminado = '0' ORDER BY Nombre ";
        this.loadSQL(sql);
    }

    public void loadCliente(string codCliente)
    {
        string sql = coreSQL;
        sql += " WHERE CodigoSAP = '" + codCliente + "' AND Eliminado = '0' order by CodigoCliente";
        this.loadSQL(sql);
    }
    public void loadClienteMensaje(string codCliente)
    {
        string sql = coreSQL;
        sql += " WHERE CodigoSAP = '" + codCliente + "' AND Eliminado = '0' and Mensaje IS NOT NULL order by CodigoCliente";
        this.loadSQL(sql);
    }
    public void loadNuevoId(string codigo)
    {
        string sql = "declare @Id varchar(50)";
        sql += " select @Id = (SELECT NuevoId = MAX(CAST(LEFT(CodigoCliente,3)AS varchar(5))) +";
        sql += " RIGHT('' + CAST(MAX(CAST(RIGHT(CodigoCliente,LEN(CodigoCliente)- 3)+ 1 AS int))AS varchar(50)),50)";
        sql += " FROM Clientes";
        sql += " Where CodigoCliente like '" + codigo + "%')";
        sql += " select @Id as NuevoId";
        this.loadSQL(sql);
    }

    public void loadClienteItem(string codCliente)
    {
        string sql = coreSQL;
        sql += " WHERE CodigoCliente = '" + codCliente + "' AND Eliminado = '0' order by CodigoCliente";
        this.loadSQL(sql);
    }


    #region Campos

    public int IDOFICIALCUENTA
    {
        get
        {
            return int.Parse(registro[Campos.IDOFICIALCUENTA].ToString());
        }
        set
        {
            registro[Campos.IDOFICIALCUENTA] = value;
        }
    }


    public string MENSAJE
    {
        get
        {
            return (string)registro[Campos.MENSAJE].ToString();
        }
        set
        {
            registro[Campos.MENSAJE] = value;
        }
    }
    //PERMITIRREMISIONES

    public string PERMITIRREMISIONES
    {
        get
        {
            return (string)registro[Campos.PERMITIRREMISIONES].ToString();
        }
        set
        {
            registro[Campos.PERMITIRREMISIONES] = value;
        }
    }
    public string CODIGOCLIENTE
    {
        get
        {
            return (string)registro[Campos.CODIGOCLIENTE].ToString();
        }
        set
        {
            registro[Campos.CODIGOCLIENTE] = value;
        }
    }

    public string CODIGOSAP
    {
        get
        {
            return (string)registro[Campos.CODIGOSAP].ToString();
        }
        set
        {
            registro[Campos.CODIGOSAP] = value;
        }
    }

    public string NOMBRE
    {
        get
        {
            return (string)registro[Campos.NOMBRE].ToString();
        }
        set
        {
            registro[Campos.NOMBRE] = value;
        }
    }

    public string RTN
    {
        get
        {
            return registro[Campos.RTN].ToString();
        }
        set
        {
            registro[Campos.RTN] = value;
        }
    }

    public string DIRECCION
    {
        get
        {
            return (string)registro[Campos.DIRECCION].ToString();
        }
        set
        {
            registro[Campos.DIRECCION] = value;
        }
    }

    public string OFICIALCUENTA
    {
        get
        {
            return (string)registro[Campos.OFICIALCUENTA].ToString();
        }
        set
        {
            registro[Campos.OFICIALCUENTA] = value;
        }
    }

    public string CONTACTO
    {
        get
        {
            return registro[Campos.CONTACTO].ToString();
        }
        set
        {
            registro[Campos.CONTACTO] = value;
        }
    }

    public string TELEFONO
    {
        get
        {
            return registro[Campos.TELEFONO].ToString();
        }
        set
        {
            registro[Campos.TELEFONO] = value;
        }
    }

    public string EMAIL
    {
        get
        {
            return (string)registro[Campos.EMAIL].ToString();
        }
        set
        {
            registro[Campos.EMAIL] = value;
        }
    }

    public string CODIGOPAIS
    {
        get
        {
            return (string)registro[Campos.CODIGOPAIS].ToString();
        }
        set
        {
            registro[Campos.CODIGOPAIS] = value;
        }
    }

    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }

    public string ELIMINADO
    {
        get
        {
            return (string)registro[Campos.ELIMINADO].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }

    public string CODIGOCLIENTEADUANAS
    {
        get
        {
            return (string)registro[Campos.CODIGOCLIENTEADUANAS].ToString();
        }
        set
        {
            registro[Campos.CODIGOCLIENTEADUANAS] = value;
        }
    }
    #endregion

}
