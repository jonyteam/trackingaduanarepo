﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;
using GrupoLis.Ebase;
using GrupoLis.Login;
/// <summary>
/// Descripción breve de PlantillaGastosBO
/// </summary>
public class PlantillaGastosMBO: CapaBase
{


    class Campos
    {
        public static string ID = "Id";
        public static string CODPAIS = "CodPais";
        public static string CODIGOADUANA = "CodigoAduana";
        public static string IDCLIENTE = "IdCliente";
        public static string IDPROVEEDOR = "IdProveedor";
        public static string IDGASTO = "IdGasto";
        public static string COSTO = "Costo";
        public static string IVA = "IVA";
        public static string VENTA = "Venta";
        public static string MEDIOPAGO = "MedioPago";
        public static string ESTADO = "Estado";
        public static string IDUSUARIO = "IdUsuario";
        public static string FECHA = "Fecha";
        public static string OBSERVACION = "Observacion";
        public static string CANALSELECTIVIDAD = "CanalSelectividad";

    }

	public PlantillaGastosMBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from PlantillaGastos";
        this.initializeSchema("PlantillaGastosM");
    }

        public void loadGastos_masivos(string idcliente)
    {
        string sql = " SELECT  Id, CodPais, CodigoAduana, IdCliente, IdProveedor, IdGasto, Costo, Venta, MedioPago, Estado, IdUsuario, Fecha, CanalSelectividad, IVA,Observacion";
              sql+=" FROM PlantillaGastos";
              sql+=" WHERE (IdCliente = '"+idcliente+"' and Estado=0 )";
              this.loadSQL(sql);
    }

        public void loadGastos_masivos_Salvador(string idcliente, string codAduana)
        {
            string sql = @" SELECT  Id, CodPais, CodigoAduana, IdCliente, IdProveedor, IdGasto, Costo, Venta, MedioPago, Estado, IdUsuario, Fecha, CanalSelectividad, IVA,Observacion 
                  FROM PlantillaGastos  WHERE ( (IdCliente = '" + idcliente + "' or IdCliente='0000000' ) and CodigoAduana='"+codAduana+"' and Estado=0  and CodPais='S' )";
            this.loadSQL(sql);
        }

    #region campos

        public string ID
        {
            get
            {
                return (string)registro[Campos.ID].ToString();
            }
            set
            {
                registro[Campos.ID] = value;
            }
        }
        public string CODPAIS 
        {
            get
            {
                return (string)registro[Campos.CODPAIS ].ToString();
            }
            set
            {
                registro[Campos.CODPAIS ] = value;
            }
        }
        public string CODIGOADUANA
        {
            get
            {
                return (string)registro[Campos.CODIGOADUANA].ToString();
            }
            set
            {
                registro[Campos.CODIGOADUANA] = value;
            }
        }
        public string IDCLIENTE
        {
            get
            {
                return (string)registro[Campos.IDCLIENTE].ToString();
            }
            set
            {
                registro[Campos.IDCLIENTE] = value;
            }
        }
        public string IDPROVEEDOR
        {
            get
            {
                return (string)registro[Campos.IDPROVEEDOR].ToString();
            }
            set
            {
                registro[Campos.IDPROVEEDOR] = value;
            }
        }
        public string IDGASTO
        {
            get
            {
                return (string)registro[Campos.IDGASTO].ToString();
            }
            set
            {
                registro[Campos.IDGASTO] = value;
            }
        }
        public double COSTO
        {
            get
            {
                return double.Parse(registro[Campos.COSTO].ToString());
            }
            set
            {
                registro[Campos.COSTO] = value;
            }
        }
        public double IVA
        {
            get
            {
                return double.Parse(registro[Campos.IVA].ToString());
            }
            set
            {
                registro[Campos.IVA] = value;
            }
        }
        public double VENTA
        {
            get
            {
                return double.Parse(registro[Campos.VENTA].ToString());
            }
            set
            {
                registro[Campos.VENTA] = value;
            }
        }
        public string MEDIOPAGO
        {
            get
            {
                return (string)registro[Campos.MEDIOPAGO].ToString();
            }
            set
            {
                registro[Campos.MEDIOPAGO] = value;
            }
        }
        public string ESTADO
        {
            get
            {
                return (string)registro[Campos.ESTADO].ToString();
            }
            set
            {
                registro[Campos.ESTADO] = value;
            }
        }
        public string IDUSUARIO
        {
            get
            {
                return (string)registro[Campos.IDUSUARIO].ToString();
            }
            set
            {
                registro[Campos.IDUSUARIO] = value;
            }
        }

        public string FECHA
        {
            get
            {
                return (string)registro[Campos.FECHA].ToString();
            }
            set
            {
                registro[Campos.FECHA] = value;
            }
        }

        public string OBSERVACION
        {
            get
            {
                return (string)registro[Campos.OBSERVACION].ToString();
            }
            set
            {
                registro[Campos.OBSERVACION] = value;
            }
        }
    
       
    #endregion 
}