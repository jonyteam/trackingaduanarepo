﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for FacturaBO
/// </summary>
public class LogErroresBO : CapaBase
{
    class Campos
    {
        public static string IDERROR = "IdError";
        public static string ERROR = "Error";
        public static string MODULO = "Modulo";
        public static string IDUSUARIO = "IdUsuario";
        public static string FECHA = "Fecha";
    }

    public LogErroresBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from LogErrores";
        this.initializeSchema("LogErrores");
    }

    public void loadError()
    {
        string sql = this.coreSQL;
        this.loadSQL(sql);
    }

    public void loadError(string idError)
    {
        string sql = this.coreSQL + " WHERE IdError = '" + idError + "' ";
        this.loadSQL(sql);
    }

    #region campos
    public string IDERROR
    {
        get
        {
            return (string)registro[Campos.IDERROR].ToString();
        }
    }

    public string ERROR
    {
        get
        {
            return (string)registro[Campos.ERROR].ToString();
        }
        set
        {
            registro[Campos.ERROR] = value;
        }
    }

    public string MODULO
    {
        get
        {
            return (string)registro[Campos.MODULO].ToString();
        }
        set
        {
            registro[Campos.MODULO] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }

    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }
    #endregion

}
