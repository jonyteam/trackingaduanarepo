using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for EspeciesFiscalesBO
/// </summary>
public class EventosBO : CapaBase
{
    class Campos
    {
        public static string ID = "Id";
        public static string IDINSTRUCCION = "IdInstruccion";
        public static string IDEVENTO = "IdEvento";
        public static string FECHAINICIO = "FechaInicio";
        public static string FECHAFIN = "FechaFin";
        public static string FECHAINICIOINGRESO = "FechaInicioIngreso";
        public static string FECHAFININGRESO = "FechaFinIngreso";
        public static string OBSERVACIONINICIO = "ObservacionInicio";
        public static string OBSERVACIONFIN = "ObservacionFin";
        public static string ELIMINADO = "Eliminado";
        public static string IDUSUARIOINICIO = "IdUsuarioInicio";
        public static string IDUSUARIOFIN = "IdUsuarioFin";
    }

    public EventosBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from Eventos E ";
        this.initializeSchema("Eventos");
    }

    public void loadEventos()
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' ";
        this.loadSQL(sql);
    }

    public void loadEventosAll(string codPais, string fechaInicial, string fechaFinal)
    {
        string sql = " SELECT distinct E.IdInstruccion, C.Nombre, I.* FROM Eventos E ";
        sql += " INNER JOIN Instrucciones I ON (I.IdInstruccion = E.IdInstruccion AND I.CodEstado = '0') ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = '0') ";
        sql += " WHERE I.CodPaisHojaRuta = '" + codPais + "' AND CONVERT(Varchar,I.Fecha,111) BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + "' AND I.CodEstado = '0' ";
        sql += " order by I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadEventosXPais(string codPais)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdInstruccion like '" + codPais + "-%' AND Eliminado = '0' ";
        this.loadSQL(sql);
    }
    public void loadEventosXPaisUsuario(string codPais, string Usuario)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdInstruccion like '" + codPais + "-%' AND Eliminado = '0' and IdUsuarioInicio = '" + Usuario + "' and IdUsuarioFin is Null";
        this.loadSQL(sql);
    }
    public void loadEventosXPaisUsuarioNI(string codPais)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdInstruccion like '" + codPais + "-%' AND Eliminado = '0' and IdUsuarioFin is Null";
        this.loadSQL(sql);
    }

    public void loadEventosXPaisUsuarioH(string codPais)
    {
        string sql = this.coreSQL;
        sql += "Inner Join Instrucciones  I on (I.IdInstruccion = E.IdInstruccion)";
        sql += " WHERE I.IdInstruccion like '" + codPais + "-%' AND Eliminado = '0' and IdUsuarioFin is Null and I.IdCliente in (8000825,8000837,8000882)";
        this.loadSQL(sql);
    }
    public void loadEventosXInstruccion(string idInstruccion)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdInstruccion = '" + idInstruccion + "' AND Eliminado = '0' ";
        this.loadSQL(sql);
    }

    public void loadEventosXInstruccionEvento(string idInstruccion, string idEvento)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdInstruccion = '" + idInstruccion + "' AND IdEvento = '" + idEvento + "' AND Eliminado = '0' ";
        this.loadSQL(sql);
    }

    public void loadEventosXInstruccionYEvento(string idInstruccion, string idEvento)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdInstruccion = '" + idInstruccion + "' AND IdEvento = '" + idEvento + "' ";
        this.loadSQL(sql);
    }

    public void InicioEventoGuia(string hojaruta, string evento)
    {
        string sql = " SELECT * From Eventos A";
        sql += " Inner Join Instrucciones B on ( B.CodigoGuia is not NULL and A.IdInstruccion = B.IdInstruccion and B.CodEstado = 0 and B.ArriboCarga = 1 )";
        sql += " Inner Join Codigos C on (C.Categoria = 'EVENTOS' AND C.Descripcion = 'Arribo de Carga' AND C.Codigo = '" + evento + "')";
        sql += " Where B.IdInstruccion = '" + hojaruta + "' and B.CodPaisHojaRuta ='H' and IdEvento = 1 and FechaFin Is NULL and FechaFinIngreso is NUll";
        this.loadSQL(sql);
    }

    public void loadEventosXInstruccionEventoArriboFin(string idInstruccion, string idEvento)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdInstruccion = '" + idInstruccion + "' AND IdEvento = '" + idEvento + "' AND Eliminado = '1' ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesXAduanaIdU(string codigoAduana, string idUsuarioA)
    {
        string sql = " SELECT * FROM Instrucciones I ";
        sql += " INNER JOIN Eventos E ON (E.IdInstruccion = I.IdInstruccion) ";
        sql += " WHERE I.CodEstado = '0' AND I.CodigoAduana = '" + codigoAduana + "' AND I.IdUsuarioAduana = '" + idUsuarioA + "' AND E.Eliminado = '0' ";
        sql += " ORDER BY 1 ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesXIdU(string idUsuarioA)
    {
        string sql = " SELECT * FROM Instrucciones I ";
        sql += " INNER JOIN Eventos E ON (E.IdInstruccion = I.IdInstruccion) ";
        sql += " WHERE I.CodEstado = '0' AND I.IdUsuarioAduana = '" + idUsuarioA + "' AND E.Eliminado = '0' ";
        sql += " ORDER BY 1 ";
        this.loadSQL(sql);
    }

    public void loadEventosAllXInstruccion(string idInstruccion)
    {
        string sql = " SELECT E.*, C.Descripcion AS Evento FROM [AduanasNueva].[dbo].[Eventos] E ";
        sql += " INNER JOIN Codigos C ON (C.Codigo = E.IdEvento AND C.Categoria = 'EVENTOS' AND C.Eliminado = '0') ";
        sql += " WHERE E.IdInstruccion = '" + idInstruccion + "' ";
        this.loadSQL(sql);
    }

    #region campos
    public string IDINSTRUCCION
    {
        get
        {
            return (string)registro[Campos.IDINSTRUCCION].ToString();
        }
        set
        {
            registro[Campos.IDINSTRUCCION] = value;
        }
    }

    public string IDEVENTO
    {
        get
        {
            return (string)registro[Campos.IDEVENTO].ToString();
        }
        set
        {
            registro[Campos.IDEVENTO] = value;
        }
    }

    public string FECHAINICIO
    {
        get
        {
            return (string)registro[Campos.FECHAINICIO].ToString();
        }
        set
        {
            registro[Campos.FECHAINICIO] = value;
        }
    }

    public string FECHAFIN
    {
        get
        {
            return (string)registro[Campos.FECHAFIN].ToString();
        }
        set
        {
            registro[Campos.FECHAFIN] = value;
        }
    }

    public string FECHAINICIOINGRESO
    {
        get
        {
            return (string)registro[Campos.FECHAINICIOINGRESO].ToString();
        }
        set
        {
            registro[Campos.FECHAINICIOINGRESO] = value;
        }
    }

    public string FECHAFININGRESO
    {
        get
        {
            return (string)registro[Campos.FECHAFININGRESO].ToString();
        }
        set
        {
            registro[Campos.FECHAFININGRESO] = value;
        }
    }

    public string OBSERVACIONINICIO
    {
        get
        {
            return (string)registro[Campos.OBSERVACIONINICIO].ToString();
        }
        set
        {
            registro[Campos.OBSERVACIONINICIO] = value;
        }
    }

    public string OBSERVACIONFIN
    {
        get
        {
            return (string)registro[Campos.OBSERVACIONFIN].ToString();
        }
        set
        {
            registro[Campos.OBSERVACIONFIN] = value;
        }
    }

    public string ELIMINADO
    {
        get
        {
            return (string)registro[Campos.ELIMINADO].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO] = value;
        }
    }

    public string IDUSUARIOINICIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIOINICIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIOINICIO] = value;
        }
    }

    public string IDUSUARIOFIN
    {
        get
        {
            return (string)registro[Campos.IDUSUARIOFIN].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIOFIN] = value;
        }
    }
    #endregion
}
