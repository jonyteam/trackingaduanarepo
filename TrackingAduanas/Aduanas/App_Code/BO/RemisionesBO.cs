using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for EventosBO
/// </summary>
public class RemisionesBO : CapaBase
{

    class Campos
    {

        public static string NUMEROREMISION = "NumeroRemision";
        public static string IDINSTRUCCION = "IdInstruccion";
        public static string FECHAENVIO = "FechaEnvio";
        public static string FECHAENVIOSISTEMA = "FechaEnvioSistema";
        public static string USUARIOENVIO = "UsuarioEnvio";
        public static string FECHARECEPCION = "FechaRecepcion";
        public static string FECHARECEPCIONSISTEMA = "FechaRecepcionSistema";
        public static string USUARIORECEPCION = "UsuarioRecepcion";
        public static string ESTADO = "Estado";
    }

    public RemisionesBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from Remisiones R";
        this.initializeSchema("DetalleGastos");
    }



    public void loadAutorizaciones()
    {
        string sql = this.coreSQL;
        sql += " where Estado = '0'";
        this.loadSQL(sql);
    }

    public void LoadRemision(string Codigo)
    {
        string sql = this.coreSQL;
        sql += " WHERE NumeroRemision = '" + Codigo + "' ";
        this.loadSQL(sql);
    }
    public void LoadNumeraciones2(string Codigo)
    {
        string sql = "SELECT Codigo+'-'+Numeracion as Numeraciones2 FROM Numeraciones";
        sql += " WHERE Codigo = '" + Codigo + "' ";
        this.loadSQL(sql);
    }
    public void LoadRemisionXPais(string Inicio, string Fin, string CodPais)
    {
        string sql = @"SELECT NumeroRemision,IdInstruccion,FechaEnvioSistema,B.Nombre+' '+B.Apellido as Nombre,A.Estado
                    FROM Remisiones A
                    Inner Join Usuarios B on (A.UsuarioEnvio = B.IdUsuario)
                    Where FechaEnvioSistema between '" + Inicio + "' and '" + Fin + "' and IdInstruccion like '%"+CodPais+"%'order by 3";
        this.loadSQL(sql);
    }




       public void LoadRemisionXAduanas(string top ,string CodAduana)
    {
       
            string sql ="  SELECT Distinct Top "+top+"";
            sql += "[NumeroRemision], cast (SUBSTRING([NumeroRemision], CHARINDEX ('-', [NumeroRemision]) + 1 ,10) as int ) as orden    	  ";
            sql += "FROM [Remisiones] ";
            sql += "where SUBSTRING([NumeroRemision],1,( cast ( CHARINDEX ('-', [NumeroRemision]) as int) -1 ))='" + CodAduana + "'";
            sql += "order by orden desc ";
            this.loadSQL(sql);

    }

       public void LoadAduanas( )
       {

          
             string  sql ="SELECT distinct";
                     sql +="   NombreAduana";
                     sql +="   ,SUBSTRING(NumeroRemision,1, CHARINDEX ('-', NumeroRemision) - 1 ) as cod";
                     sql +=" FROM Remisiones a ";
                     sql += "inner join AduanasNueva.dbo.Aduanas b on b.CodigoAduana= SUBSTRING(NumeroRemision,1, CHARINDEX ('-', NumeroRemision) - 1 )";


           this.loadSQL(sql);

       }


    #region Campos
    public String NUMEROREMISION
    {
        get
        {
            return Convert.ToString(registro[Campos.NUMEROREMISION]);
        }
        set
        {
            registro[Campos.NUMEROREMISION] = value;
        }
    }
    public String IDINSTRUCCION
    {
        get
        {
            return Convert.ToString(registro[Campos.IDINSTRUCCION]);
        }
        set
        {
            registro[Campos.IDINSTRUCCION] = value;
        }
    }
    public String FECHAENVIO
    {
        get
        {
            return Convert.ToString(registro[Campos.FECHAENVIO]);
        }
        set
        {
            registro[Campos.FECHAENVIO] = value;
        }
    }
    public String FECHARECEPCION
    {
        get
        {
            return Convert.ToString(registro[Campos.FECHARECEPCION]);
        }
        set
        {
            registro[Campos.FECHARECEPCION] = value;
        }
    }
    public String FECHARECEPCIONSISTEMA
    {
        get
        {
            return Convert.ToString(registro[Campos.FECHARECEPCIONSISTEMA]);
        }
        set
        {
            registro[Campos.FECHARECEPCIONSISTEMA] = value;
        }
    }
    public String USUARIORECEPCION
    {
        get
        {
            return Convert.ToString(registro[Campos.USUARIORECEPCION].ToString());
        }
        set
        {
            registro[Campos.USUARIORECEPCION] = value;
        }
    }
    public String USUARIOENVIO
    {
        get
        {
            return Convert.ToString(registro[Campos.USUARIOENVIO].ToString());
        }
        set
        {
            registro[Campos.USUARIOENVIO] = value;
        }
    }
    public double ESTADO
    {
        get
        {
            return double.Parse(registro[Campos.ESTADO].ToString());
        }
        set
        {
            registro[Campos.ESTADO] = value;
        }
    }
    #endregion
}
