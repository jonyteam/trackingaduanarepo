﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using GrupoLis.Ebase;
using GrupoLis.Login;

    public class TiemposMaerskBO : CapaBase
    {
        class Campos
        {
            public static string IDTRAMITE = "IdTramite";
            public static string IDFLUJO = "IdFlujo";
            public static string FECHA = "Fecha";
            public static string ELIMINADO = "Eliminado";
            public static string IDUSUARIO = "IdUsuario";
            public static string FECHASISTEMA = "FechaSistema"; 
            public static string OBSERVACION = "Observacion";
            public static string NOMBREARCHIVO = "NombreArchivo";
        }

        public TiemposMaerskBO(GrupoLis.Login.Login conector): base(conector, true)
        {
            coreSQL = "SELECT * FROM TiemposMaersk";
            initializeSchema("TiemposMaersk");
        }

        public void loadTiempos(string id){
            string sql = "Select * from TiemposMaersk where id='" + id + "'";
            this.loadSQL(sql);
        }

        public void CargarFlujoporArchivo(string archivo)
        {
            string sql = "Select * from TiemposMaersk where NombreArchivo='" + archivo + "' and Eliminado =0 and IdFlujo=0";
            this.loadSQL(sql);
        }

        #region Campos
        public String IDTRAMITE
        {
            get
            {
                return Convert.ToString(registro[Campos.IDTRAMITE]);
            }
            set
            {
                registro[Campos.IDTRAMITE] = value;
            }
        }

        public String IDFLUJO
        {
            get
            {
                return Convert.ToString(registro[Campos.IDFLUJO]);
            }
            set
            {
                registro[Campos.IDFLUJO] = value.Trim();
            }
        }


        public String FECHA
        {
            get
            {
                return Convert.ToString(registro[Campos.FECHA]);
            }
            set
            {
                registro[Campos.FECHA] = value.Trim();
            }
        }

        public String ELIMINADO
        {
            get
            {
                return Convert.ToString(registro[Campos.ELIMINADO]);
            }
            set
            {
                registro[Campos.ELIMINADO] = value.Trim();
            }
        }
        
        public String IDUSUARIO
        {
            get
            {
                return Convert.ToString(registro[Campos.IDUSUARIO]);
            }
            set
            {
                registro[Campos.IDUSUARIO] = value.Trim();
            }
        }

        public String FECHASISTEMA
        {
            get
            {
                return Convert.ToString(registro[Campos.FECHASISTEMA]);
            }
            set
            {
                registro[Campos.FECHASISTEMA] = value.Trim();
            }
        }

        public String OBSERVACION
        {
            get
            {
                return Convert.ToString(registro[Campos.OBSERVACION]);
            }
            set
            {
                registro[Campos.OBSERVACION] = value.Trim();
            }
        }

        public String NOMBREARCHIVO
        {
            get
            {
                return Convert.ToString(registro[Campos.NOMBREARCHIVO]);
            }
            set
            {
                registro[Campos.NOMBREARCHIVO] = value.Trim();
            }
        }
       #endregion
    }
