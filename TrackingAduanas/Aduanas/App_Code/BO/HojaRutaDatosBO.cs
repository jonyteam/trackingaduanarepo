using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;
using System.Text;

/// <summary>
/// Summary description for HojaRutaDatosBO
/// </summary>
public class HojaRutaDatosBO: CapaBase
{
    class Campos
    {
        public static string ID = "Id";
        public static string IDHOJARUTA = "IdHojaRuta";
        public static string CODIGO = "Codigo";
        public static string DATOS = "Datos";
    }

    public HojaRutaDatosBO(GrupoLis.Login.Login log)
        : base(log, true)
	{
        this.coreSQL = "Select * from HojaRutaDatos";
        this.initializeSchema("HojaRutaDatos");
    }

    public void loadAllCamposHojaRutaDatos()
    {
        this.loadSQL(this.coreSQL);
    }

    public void loadAllCamposHojaRutaDatos(string idHojaRuta)
    {
        string sql = this.coreSQL;
        sql += " where IdHojaRuta = '" + idHojaRuta + "' and Estado = '0'";
        this.loadSQL(sql);
    }

    public void loadHojaRutaDatos(string NumContenedor)
    {
        string sql = this.coreSQL;
        sql += " where Datos like '%" + NumContenedor + "%' and Codigo = '1'";
        this.loadSQL(sql);
    }

    public void loadBusquedaHojaRuta(string idHojaRuta, string codigo)
    {
        this.loadSQL(String.Format("SELECT * FROM HojaRutaDatos WHERE Codigo='{0}' and IdHojaRuta = '{1}'", codigo, idHojaRuta));
    }

    public void deleteHojaRutaDatos(Int64 id)
    {
        this.executeCustomSQL(String.Format("delete from HojaRutaDatos where Id={0}", id.ToString()));
    }

    public void loadHojaRutasDatosSinSello(String idHojaRuta, String codigo, String codigoSello)
    {
        StringBuilder sql = new StringBuilder();

        sql.Append("select A.Id as Id, A.IdHojaRuta as IdHojaRuta, A.Codigo as Codigo, A.Datos as Datos from HojaRutaDatos as A");
        sql.Append(" where (A.Id not in ");
        sql.Append("(");
        sql.Append("	select B.Id from HojaRutaDatosSellos as B where (B.IdHojaRuta='{0}') and (B.Codigo='{2}') and (B.Estado='0')");
        sql.Append("))");
        sql.Append(" and (A.IdHojaRuta = '{0}')");
        sql.Append(" and (A.Codigo='{1}')");

        this.loadSQL(String.Format(sql.ToString(), idHojaRuta, codigo, codigoSello));
    }

    public void sellarFecha(Int64 id, String idHojaRuta, String codigoSello, DateTime fecha)
    {
        StringBuilder sql = new StringBuilder();

        sql.Append(" insert into HojaRutaDatosSellos (");
        sql.Append(" Id, IdHojaRuta, Codigo, Fecha)");
        sql.Append(" VALUES (");
        sql.Append(" '{0}','{1}','{2}',CONVERT(DATETIME, '{3}', 120))");

        this.executeCustomSQL(String.Format(sql.ToString(), id.ToString(), idHojaRuta, codigoSello, fecha.ToString("yyyy-MM-dd HH:mm:ss")));
    }

    public void loadBusquedaCriterioTramites(string criterio)
    {
        string sql = " select a.CodigoPais, r.IdHojaRuta, FechaRecepcion as [Fecha-R], NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor, cod.descripcion as Flujo";
        sql += " from HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        sql += " inner join ComplementoRecepcion cr on (r.IdHojaRuta = cr.IdHojaRuta)";
        sql += " inner join TiempoFlujo tf on (tf.IdHojaRuta = r.IdHojaRuta and tf.IdFlujo = 1)";
        sql += " inner join HojaRutaDatos hr on (hr.IdHojaRuta = r.IdHojaRuta and hr.Codigo = '1')";
        sql += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        sql += " inner join Codigos as cod on (cod.codigo = r.IdFlujo and cod.clasificacion = 'FLUJO')";
        sql += " where " + criterio + " and r.Estado = '0'";
        sql += " order by a.CodigoPais, NombreAduana";
        this.loadSQL(sql);
    }

    public Int64 ID
    {
        get
        {
            return Convert.ToInt64(registro[Campos.ID]);
        }
    }

    public String IDHOJARUTA
    {
        get
        {
            return registro[Campos.IDHOJARUTA].ToString();
        }
        set
        {
            registro[Campos.IDHOJARUTA] = value;
        }
    }

    public String CODIGO
    {
        get
        {
            return registro[Campos.CODIGO].ToString();
        }
        set
        {
            registro[Campos.CODIGO] = value;
        }
    }

    public String DATOS
    {
        get
        {
            return registro[Campos.DATOS].ToString();
        }
        set
        {
            registro[Campos.DATOS] = value;
        }
    }
}
