﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;

using GrupoLis.Ebase;
using GrupoLis.Login;

public class Numeraciones2BO : CapaBase
{
    class Campos
    {
        public static string CODIGO = "Codigo";
        public static string NUMERACION = "Numeracion";
    }

    public Numeraciones2BO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        coreSQL = " SELECT * FROM Numeraciones ";
        initializeSchema("Numeraciones");
    }

    public void LoadNumeraciones(string Pais)
    {
        string sql = this.coreSQL;
        sql += " WHERE Codigo = '"+ Pais+"' ";
        this.loadSQL(sql);
    }

    #region Campos
    public String CODIGO
    {
        get
        {
            return Convert.ToString(registro[Campos.CODIGO]);
        }
        set
        {
            registro[Campos.CODIGO] = value;
        }
    }
    //public String NUMERACION
    //{
    //    get
    //    {
    //        return Convert.ToString(registro[Campos.NUMERACION]);
    //    }
    //    set
    //    {
    //        registro[Campos.NUMERACION] = value;
    //    }
    //}
    public double NUMERACION
    {
        get
        {
            return double.Parse(registro[Campos.NUMERACION].ToString());
        }
        set
        {
            registro[Campos.NUMERACION] = value;
        }
    }
    #endregion
}
