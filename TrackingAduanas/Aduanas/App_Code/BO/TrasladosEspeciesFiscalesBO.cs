using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for TrasladosEspeciesFiscalesBO
/// </summary>
public class TrasladosEspeciesFiscalesBO : CapaBase
{
    class Campos
    {
        public static string IDENVIO = "IdEnvio";
        public static string IDESPECIEFISCAL = "IdEspecieFiscal";
        public static string CODPAIS = "CodPais";
        public static string CODIGOADUANAORIGEN = "CodigoAduanaOrigen";
        public static string CODIGOADUANADESTINO = "CodigoAduanaDestino";
        public static string RANGOINICIAL = "RangoInicial";
        public static string RANGOFINAL = "RangoFinal";
        public static string CANTIDAD = "Cantidad";
        public static string FECHA = "Fecha";
        public static string IDUSUARIO = "IdUsuario";
        public static string IDMOVIMIENTO = "IdMovimiento";
        public static string ELIMINADO = "Eliminado";
        public static string FECHARECIBIDO = "FechaRecibido";
        public static string OBSERVACION = "Observacion";
        public static string IDUSUARIORECIBO = "IdUsuarioRecibo";
        public static string CARGA = "Carga";
        public static string FECHACARGA = "FechaCarga";
    }

    public TrasladosEspeciesFiscalesBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from TrasladosEspeciesFiscales TEF ";
        this.initializeSchema("TrasladosEspeciesFiscales");
    }

    public void loadTrasladosEspeciesFiscales(string idEspecieFiscal)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdEspecieFiscal = '" + idEspecieFiscal + "' ";
        this.loadSQL(sql);
    }

    public void loadTrasladosEspeciesFiscalesXIdMov(string idMovimiento)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdEnvio = '" + idMovimiento + "' ";
        this.loadSQL(sql);
    }

    public void CargaUso()
    {
        string sql = " SELECT Convert(Varchar,A.Fecha,104) as 'Fec.docu.' ,'SA' as 'Clase Dto.','9095' as Sociedad,Convert(Varchar,GETDATE(),104) as 'Fec.Con.','10' as Periodo";
               sql += " ,'HNL' as Moneda,'1000010' as 'No.Dto.',[IdEnvio] as Referencia, 'Envio Especies Fiscales de ' +B.NombreAduana +' a '+ C.NombreAduana as TxtCabDto";
               sql += " ,'9095' as 'Divi.', '40' as CLAVE,'50' as CLAVE2,D.Codigo as Cuenta, E.Codigo as Cuenta2, 'Envio ' + F.Descripcion +' ' + B.NombreAduana as Texto ";
               sql += " ,[IdEnvio],Case A.IdEspecieFiscal When 'SSB' Then 'SS' Else IdEspecieFiscal End  as CodigoEspecie,A.[CodPais],B.NombreAduana,[CodigoAduanaDestino],A.[RangoInicial],A.[RangoFinal],A.[Cantidad],A.[Fecha]";
               sql += " ,A.[IdUsuario],[IdMovimiento],A.[Eliminado],[FechaRecibido],[Observacion],[IdUsuarioRecibo], 'Recepcion ' + F.Descripcion +' ' + C.NombreAduana as Texto2";
               sql += " FROM [AduanasNueva].[dbo].[TrasladosEspeciesFiscales] A ";
               sql += " Inner Join Aduanas B on ( A.[CodigoAduanaOrigen] = B.CodigoAduana and B.CodEstado = 0 and A.Eliminado = 0)";
               sql += " Inner Join Aduanas C on ( A.[CodigoAduanaDestino] = C.CodigoAduana and B.CodEstado = 0 and A.Eliminado = 0)";
               sql += " Inner Join Codigos D on (D.Categoria = 'INVENTARIO' and D.Descripcion = B.NombreAduana and D.Eliminado = 0)";
               sql += " Inner Join Codigos E on (E.Categoria = 'INVENTARIO' and E.Descripcion = C.NombreAduana and E.Eliminado = 0)";
               sql += " Inner Join Codigos F on (F.Categoria = 'ESPECIESFISCALES' and F.Codigo = A.IdEspecieFiscal and F.Eliminado = 0)";
               sql += " Where A.Carga = 0 and FechaRecibido is not null And A.Fecha >= '2016-01-15' Order by A.RangoInicial";
        this.loadSQL(sql);
    }
    public void CargaUsoCambiaEstado()
    {
        string sql = " Update TrasladosEspeciesFiscales Set Carga = '1', FechaCarga = GetDate() ";
               sql += " FROM TrasladosEspeciesFiscales A ";
               sql += " Inner Join Aduanas B on ( A.[CodigoAduanaOrigen] = B.CodigoAduana and B.CodEstado = 0 and A.Eliminado = 0) ";
               sql += " Inner Join Aduanas C on ( A.[CodigoAduanaDestino] = C.CodigoAduana and B.CodEstado = 0 and A.Eliminado = 0) ";
               sql += " Inner Join Codigos D on (D.Categoria = 'INVENTARIO' and D.Descripcion = B.NombreAduana and D.Eliminado = 0) ";
               sql += " Inner Join Codigos E on (E.Categoria = 'INVENTARIO' and E.Descripcion = C.NombreAduana and E.Eliminado = 0) ";
               sql += " Inner Join Codigos F on (F.Categoria = 'ESPECIESFISCALES' and F.Codigo = A.IdEspecieFiscal and F.Eliminado = 0) ";
               sql += " Where A.Carga = 0 and FechaRecibido is not null And A.Fecha >= '2014-08-14' ";
        this.loadSQL(sql);
    }
    // DARWIN 15-015-2016
    public void TrasladosXAduanaYFecha(string FechaInicio, string FechaFin, string CodigoAduana)
    {
        string sql = @"SELECT C.Descripcion,A.NombreAduana as Origen,A1.NombreAduana as Destino,RangoInicial,RangoFinal,convert(date,Fecha) as FechaEnvio,U.Nombre+' '+U.Apellido as Nombre
                        ,convert(date,FechaRecibido) as FechaRecibido,Observacion,U1.Nombre+' '+U1.Apellido as NombreRecibio
                        FROM TrasladosEspeciesFiscales TEF
                        Inner Join Codigos C on (C.Codigo = TEF.IdEspecieFiscal and C.Categoria ='ESPECIESFISCALES')
                        Inner Join Usuarios U on (U.IdUsuario = TEF.IdUsuario)
                        Inner Join Aduanas A on (A.CodigoAduana = TEF.CodigoAduanaOrigen)
                        Inner Join Aduanas A1 on (A1.CodigoAduana = TEF.CodigoAduanaDestino)
                        LEft Join Usuarios U1 on (U1.IdUsuario = TEF.IdUsuarioRecibo)
                        Where TEF.Eliminado = 0 and Fecha between '" + FechaInicio + "' and '" + FechaFin + "' and TEF.CodigoAduanaDestino ='" + CodigoAduana + "'";
        this.loadSQL(sql);
    }


    #region campos
    public string FECHACARGA
    {
        get
        {
            return (string)registro[Campos.FECHACARGA].ToString();
        }
        set
        {
            registro[Campos.FECHACARGA] = value;
        }
    }
    public string IDENVIO
    {
        get
        {
            return (string)registro[Campos.IDENVIO].ToString();
        }
        set
        {
            registro[Campos.IDENVIO] = value;
        }
    }

    public string IDESPECIEFISCAL
    {
        get
        {
            return (string)registro[Campos.IDESPECIEFISCAL].ToString();
        }
        set
        {
            registro[Campos.IDESPECIEFISCAL] = value;
        }
    }

    public string CODPAIS
    {
        get
        {
            return (string)registro[Campos.CODPAIS].ToString();
        }
        set
        {
            registro[Campos.CODPAIS] = value;
        }
    }

    public string CODIGOADUANAORIGEN
    {
        get
        {
            return (string)registro[Campos.CODIGOADUANAORIGEN].ToString();
        }
        set
        {
            registro[Campos.CODIGOADUANAORIGEN] = value;
        }
    }

    public string CODIGOADUANADESTINO
    {
        get
        {
            return (string)registro[Campos.CODIGOADUANADESTINO].ToString();
        }
        set
        {
            registro[Campos.CODIGOADUANADESTINO] = value;
        }
    }

    public string RANGOINICIAL
    {
        get
        {
            return (string)registro[Campos.RANGOINICIAL].ToString();
        }
        set
        {
            registro[Campos.RANGOINICIAL] = value;
        }
    }

    public string RANGOFINAL
    {
        get
        {
            return (string)registro[Campos.RANGOFINAL].ToString();
        }
        set
        {
            registro[Campos.RANGOFINAL] = value;
        }
    }

    public int CANTIDAD
    {
        get
        {
            return int.Parse(registro[Campos.CANTIDAD].ToString());
        }
        set
        {
            registro[Campos.CANTIDAD] = value;
        }
    }

    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }

    public string IDMOVIMIENTO
    {
        get
        {
            return (string)registro[Campos.IDMOVIMIENTO].ToString();
        }
        set
        {
            registro[Campos.IDMOVIMIENTO] = value;
        }
    }

    public string ELIMINADO
    {
        get
        {
            return (string)registro[Campos.ELIMINADO].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO] = value;
        }
    }

    public string FECHARECIBIDO
    {
        get
        {
            return (string)registro[Campos.FECHARECIBIDO].ToString();
        }
        set
        {
            registro[Campos.FECHARECIBIDO] = value;
        }
    }

    public string OBSERVACION
    {
        get
        {
            return (string)registro[Campos.OBSERVACION].ToString();
        }
        set
        {
            registro[Campos.OBSERVACION] = value;
        }
    }

    public string IDUSUARIORECIBO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIORECIBO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIORECIBO] = value;
        }
    }

    public string CARGA
    {
        get
        {
            return (string)registro[Campos.CARGA].ToString();
        }
        set
        {
            registro[Campos.CARGA] = value;
        }
    }
    #endregion
}
