using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for AduanasBO
/// </summary>
public class AduanasRemisionBO : CapaBase
{



    class Campos
    {
        public static string ITEM = "Item";
        public static string CODADUANA = "CodAduana";
        public static string FECHALIMITE = "FechaLimite";
        public static string RANGOINICIAL = "RangoInicial";
        public static string RANGOFINAL = "RangoFinal";
        public static string SERIEACTUAL = "SerieActual";
        public static string CODESTADO = "CodEstado";
        public static string OBSERVACION = "Observacion";
        public static string CANTIDAD = "Cantidad";
        public static string DISPONIBLES = "Disponibles";


    }

    public AduanasRemisionBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from AduanasRemision";
        this.initializeSchema("AduanasRemision");
    }



    public void loadRemision( string Item)
    {
        string sql = "Select * from AduanasRemision";
        sql += " WHERE Item ="+Item;
        this.loadSQL(sql);
    }



    public void ActualizarRemision(string Item, string serie, string Disponibles)
    {

        string sql = "update AduanasRemision ";
        sql+=" set SerieActual='"+serie+"'";
        sql+=" ,Disponibles="+Disponibles;
        sql +=" WHERE Item =" + Item;
        this.loadSQL(sql);
    }



   
    #region campos


 







    public string CODESTADO
    {
        get
        {
            return (string)registro[Campos.CODESTADO].ToString();
        }
        set
        {
            registro[Campos.CODESTADO] = value;
        }
    }


    public string SERIEACTUAL
    {
        get
        {
            return (string)registro[Campos.SERIEACTUAL].ToString();
        }
        set
        {
            registro[Campos.SERIEACTUAL] = value;
        }
    }


    public int DISPONIBLES
    {
        get
        {
            return int.Parse(registro[Campos.DISPONIBLES].ToString());
        }
        set
        {
            registro[Campos.DISPONIBLES] = value;
        }
    }

    #endregion

}
