﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

public class HojaRutaEsquemaTramitesBO : CapaBase
{
    class Campos
    {
        public static string IdHojaRuta = "IdHojaRuta";
        public static string IdConceptoSAP = "IdConceptoSAP";
        public static string MedioPago = "MedioPago";
        public static string Monto = "Monto";
        public static string ValorReal = "ValorReal";
        public static string Solicitar = "Solicitar";
        public static string Aprobar = "Aprobar";
        public static string NumeroSAP = "NumeroSAP";
        public static string NotaDebito = "NotaDebito";
        public static string FechaPago = "FechaPago";
        public static string NumRecibo = "NumRecibo";
        public static string NumFactSAP = "NumFactSAP";
        public static string VFReal = "VFReal";
        public static string NumContraRecibo = "NumContraRecibo";
        public static string VCReal = "VCReal";
        public static string FchFactura = "FchFactura";
        public static string CodigoClienteSAP = "CodigoClienteSAP";
        public static string IdEncargado = "IdEncargado";
        public static string FlagCorte = "FlagCorte";
        public static string NumCheque = "NumCheque";
        public static string ProveedorPago = "ProveedorPago";
        public static string INGRESOCHTR = "IngresoCHTR";
    }

    public HojaRutaEsquemaTramitesBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from HojaRutaEsquemaTramites";
        this.initializeSchema("HojaRutaEsquemaTramites");
    }

    public void loadAllCamposHojaRuta(string idHojaRuta)
    {
        string sql = this.coreSQL;
        sql += " where IdHojaRuta = '" + idHojaRuta + "'";
        this.loadSQL(sql);
    }

    public void loadAllCamposHojaRutaSAP(string idHojaRuta, string IdConceptoSAP)
    {
        string sql = this.coreSQL;
        sql += " where IdHojaRuta = '" + idHojaRuta + "' and IdConceptoSAP = '" + IdConceptoSAP + "'";
        this.loadSQL(sql);
    }
    public void loadAllCamposHojaRuta2(string idHojaRuta, int numero)
    {
        string sql = this.coreSQL;
        sql += " where IdHojaRuta = '" + idHojaRuta + "' and IdConceptoSAP = '"+numero+"'";
        this.loadSQL(sql);
    }

    public void loadHojaRutaMonto(string idHojaRuta)
    {
        string sql = this.coreSQL;
        sql += " where Monto = 0 and IdHojaRuta = '" + idHojaRuta + "' and IdConceptoSAP not in (Select ID from ConceptoSAP where Categoria = 'PRELIQUIDACION')";
        this.loadSQL(sql);
    }

    public void loadAllCamposHojaRutaFacturaSAP(string NumFactSAP)
    {
        string sql = this.coreSQL;
        sql += " where NumFactSAP = '" + NumFactSAP + "'";
        this.loadSQL(sql);
    }

    public void loadAllCamposHojaRuta(string idHojaRuta, int IdConcepto)
    {
        string sql = this.coreSQL;
        sql += " where IdHojaRuta = '" + idHojaRuta + "' and IdConceptoSAP = " + IdConcepto;
        this.loadSQL(sql);
    }

    public void loadDeleteCamposHojaRuta(string idHojaRuta, int IdConcepto)
    {
        string sql = "Delete HojaRutaEsquemaTramites where IdHojaRuta = '" + idHojaRuta + "' and IdConceptoSAP = " + IdConcepto;
        this.loadSQL(sql);
    }

    public void loadEsquemaTramiteHojaRuta(string idHojaRuta)
    {
        //string sql = "select a.IdConceptoSAP ID, c.Descripcion Categoria, b.Concepto Concepto, a.Monto Monto from HojaRutaEsquemaTramites a, ConceptoSAP b, CategoriaSAP c where a.IdHojaRuta = '" + idHojaRuta + "' and a.IdConceptoSAP = b.ID and b.Categoria = c.Categoria";
        string sql = "select a.IdConceptoSAP ID, c.Descripcion Categoria, b.Concepto Concepto, a.MedioPago, d.descripcion, a.Monto Monto ";
        sql += "from ConceptoSAP b, CategoriaSAP c, HojaRutaEsquemaTramites a ";
        sql += "left join Codigos d on (d.clasificacion = 'MEDIOPAGO' and a.MedioPago = d.codigo) ";
        sql += "where a.IdHojaRuta = '" + idHojaRuta + "' and a.IdConceptoSAP = b.ID and b.Categoria = c.Categoria";
        this.loadSQL(sql);
    }

    public void loadForzarEsquemaTramiteHojaRuta(string idHojaRuta)
    {
        string sql = "select a.IdHojaRuta, a.IdConceptoSAP ID, c.ID CategoriaID, c.Descripcion Categoria, b.Concepto Concepto, a.MedioPago, d.descripcion, a.ValorReal, a.Solicitar, a.ProveedorPago, e.Obs  ";
        sql += "from ConceptoSAP b, CategoriaSAP c, HojaRutaEsquemaTramites a ";
        sql += "left join Codigos d on (d.clasificacion = 'MEDIOPAGO' and a.MedioPago = d.codigo) ";
        sql += "left join HojaRutaEsquemaForzado e on (a.IdHojaRuta = e.IdHojaRuta and a.IdConceptoSAP = e.IdConceptoSAP) ";
        sql += "where a.IdHojaRuta = '" + idHojaRuta + "' and a.IdConceptoSAP = b.ID and b.Categoria = c.Categoria and a.ValorReal > a.Monto and a.Monto = 0 and a.Solicitar = 0";
        this.loadSQL(sql);

    }

    public void comprobargasto(string idHojaRuta, string concepto)
    {
        string sql = "SELECT *  FROM [EsquemaTramites2].[dbo].[HojaRutaEsquemaTramites]";
        sql += " Where IdHojaRuta = '"+idHojaRuta+"' and IdConceptoSAP = '"+concepto+"'";
        this.loadSQL(sql);

    }

    public void loadSolicitudGastoHojaRuta(string idHojaRuta, Boolean Flag)
    {
        //string sql = "select a.IdHojaRuta, b.Categoria, a.IdConceptoSAP, b.Concepto, c.descripcion, a.Monto, a.ValorReal, a.Solicitar from HojaRutaEsquemaTramites a, ConceptoSAP b, Codigos c where a.IdConceptoSAP = b.ID and c.clasificacion = 'MEDIOPAGO' and c.codigo = b.MedioPago and a.IdHojaRuta = '" + idHojaRuta + "' order by b.Categoria, b.Concepto";
        string sql = "";
        sql = "select a.IdHojaRuta, b.Categoria, a.IdConceptoSAP, b.Concepto, a.MedioPago, c.descripcion, a.Monto, a.ValorReal, a.Solicitar, a.ProveedorPago ";
        sql += "from HojaRutaEsquemaTramites a, ConceptoSAP b, Codigos c ";
        sql += "where a.IdConceptoSAP = b.ID and c.clasificacion = 'MEDIOPAGO' ";
        sql += "and c.codigo = a.MedioPago and a.IdHojaRuta = '" + idHojaRuta + "' and b.Categoria <> 'PRELIQUIDACION' and a.Solicitar = '0' ";
        if (Flag == true)
        {
            sql += "union ";
            sql += "select a.IdHojaRuta, 'PRELIQUIDACION' Categoria, '0' IdConceptoSAP, 'Pre-Liquidación' Concepto, 'ND' MedioPago, 'Transferencia' descripcion, ";
            sql += "(sum(DAITotal + SELTotal + ISVTotal + PyC) * Max(Compra)) Monto, b.GastoReal ValorReal, b.Solicitar Solicitar, (select ProveedorPago from HojaRutaEsquemaTramites where IdConceptoSAP = 81 and IdHojaRuta = '" + idHojaRuta + "') ProveedorPago ";
            sql += "from HojaRutaPreliquidacionDet a, HojaRutaPreliquidacionEnc b, FactorCambio ";
            sql += "where a.IdHojaRuta = b.IdHojaRuta and a.IdHojaRuta = '" + idHojaRuta + "' and b.Solicitar = '0' ";
            sql += "group by a.IdHojaRuta, b.FechaPago, b.GastoReal, b.Solicitar ";
            sql += "order by b.Categoria, b.Concepto";
        }
        this.loadSQL(sql);
    }

    public void DeteleAllCamposHojaRuta(string idHojaRuta)
    {
        string sql = "Delete HojaRutaEsquemaTramites where IdHojaRuta = '" + idHojaRuta + "'";
        this.executeCustomSQL(sql);
    }

    public void loadGastosHojaRuta(string IdHojaRuta)
    {
        string s = "select IdHojaRuta, Categoria, IdConceptoSAP, Concepto, h.MedioPago, Monto, ValorReal, NumeroSAP, NotaDebito, NumCheque, ProveedorPago from ";
        s += "HojaRutaEsquemaTramites h inner join  ConceptoSAP c on (h.IdConceptoSAP = c.ID)  where IdHojaRuta = '" + IdHojaRuta + "'";
        loadSQL(s);
    }

    public void DeteleAllCamposHojaRuta(string idHojaRuta, string idConcepto)
    {
        string sql = "Delete from HojaRutaEsquemaTramites where IdHojaRuta = '" + idHojaRuta + "' and IdConceptoSAP = " + idConcepto;
        this.executeCustomSQL(sql);
    }

    public void loadEncabezadoHojaRuta(string idHojaRuta)
    {
        string sql = "SELECT a.IdHojaRuta, a.CodigoCliente, b.NombreCliente, b.Email, b.Contacto, isnull(c.ValorCIF,0) ValorCIF, isnull((c.Peso/1000),0) Peso, a.DescripcionProducto, ";
        sql += " isnull((SELECT c.TipoCarga + ' - ' + descripcion from Codigos where clasificacion = 'TIPOCARGAMENTO' and codigo = c.TipoCarga),'') TipoCargaDsc, ";
        sql += " isnull((SELECT Datos from HojaRutaDatos where IdHojaRuta = a.IdHojaRuta and Codigo = 5),'') Naviera, ";
        //sql += " (SELECT Count(Datos) from HojaRutaDatos where IdHojaRuta = a.IdHojaRuta and Codigo = 1) Contenedores, isnull(c.TipoCarga,'') TipoCarga, isnull(c.FechaVencimiento,'01/01/1999') FechaVencimiento ";
        sql += " I.Contenedores, isnull(c.TipoCarga,'') TipoCarga, isnull(c.FechaVencimiento,'01/01/1999') FechaVencimiento ";
        sql += " FROM HojaRuta a, Clientes b ";
        sql += " INNER JOIN [AduanasNueva].[dbo].[Instrucciones] I ON (I.IdInstruccion = '" + idHojaRuta + "') ";
        sql += " left join ComplementoRecepcion c on (c.IdHojaRuta = '" + idHojaRuta + "') ";
        sql += " where a.IdHojaRuta = '" + idHojaRuta + "' ";
        sql += " and a.CodigoCliente = b.CodigoCliente ";
        this.loadSQL(sql);
    }

    public void loadEsquemaCreditosEncabezado(int IdFlujo)
    {
        //string sql = "select a.IdHojaRuta, c.NombreCliente, (case d.Status when 1 then 'Pendiente' when 2 then 'Aprobada' when -1 then 'Rechazada' end) Estatus, ";
        //sql += "isnull(e.Obs,'') Obs, (sum(b.Monto) + sum(isnull(r.ValorTotal,0))) Total ";
        //sql += "from Clientes c, EncabezadoEsquema d, HojaRutaFlujoPortuario e, HojaRutaEsquemaTramites b, HojaRuta a ";
        //sql += "left join HojaRutaPagosPortuariosEncabezado r on (a.IdHojaRuta = r.IdHojaRuta) ";
        //sql += "where a.IdHojaRuta = b.IdHojaRuta and a.CodigoCliente = c.CodigoCliente and a.IdHojaRuta = d.IdHojaRuta and ";
        //sql += "a.IdHojaRuta = d.IdHojaRuta and ";
        //sql += "a.IdHojaRuta = e.IdHojaRuta and d.Flujo = " + IdFlujo + " and d.Flujo = e.IdFlujo ";
        //if (IdFlujo == 5)
        //    sql += "and d.Status = '1' ";
        //else
        //    sql += "and d.Status = '2' ";
        //sql += "group by a.IdHojaRuta, c.NombreCliente, d.Status, e.Obs ";
        //sql += "order by a.IdHojaRuta, c.NombreCliente, d.Status, e.Obs";

        string sql = "select IdHojaRuta, NombreCliente, Estatus, Impuestos, Obs, sum(Total) as Total";
        sql += " from (";
        sql += " select a.IdHojaRuta, c.NombreCliente, (case d.Status when 1 then 'Pendiente' when 2 then 'Aprobada' when -1 then 'Rechazada' end) Estatus, (case p.FlagISV when '1' then 'Pagados por Vesta' when '0' then 'Pagados por Cliente' end) Impuestos, isnull(e.Obs,'') Obs,";
        sql += " (case when (b.IdConceptoSAP = 81 or b.IdConceptoSAP = 82 or b.IdConceptoSAP = 83) then ((b.Monto) * Max(Compra)) else ((b.Monto)) End) + sum(isnull(r.ValorTotal,0)) Total";
        sql += " from Clientes c, EncabezadoEsquema d, HojaRutaFlujoPortuario e, HojaRutaEsquemaTramites b, FactorCambio, HojaRuta a ";
        sql += "left join HojaRutaPagosPortuariosEncabezado r on (a.IdHojaRuta = r.IdHojaRuta) ";
        sql += "left join HojaRutaPreliquidacionEnc p on (a.IdHojaRuta = p.IdHojaRuta)  ";
        sql += "where a.IdHojaRuta = b.IdHojaRuta and a.CodigoCliente = c.CodigoCliente and a.IdHojaRuta = d.IdHojaRuta and ";
        sql += "a.IdHojaRuta = d.IdHojaRuta and ";
        sql += "a.IdHojaRuta = e.IdHojaRuta and d.Flujo = " + IdFlujo + " and d.Flujo = e.IdFlujo ";
        if (IdFlujo == 3)
            sql += "and d.Status = '1' ";
        else
            sql += "and d.Status = '2' ";
        sql += "group by a.IdHojaRuta, c.NombreCliente, d.Status, p.FlagISV, e.Obs, b.IdConceptoSAP, b.Monto ";
        sql += ") as t ";
        sql += "group by t.IdHojaRuta, t.NombreCliente, t.Estatus, t.Impuestos, t.Obs ";
        sql += "order by t.IdHojaRuta, t.NombreCliente, t.Estatus, t.Impuestos, t.Obs";

        this.loadSQL(sql);
    }

    public void loadEsquemaFlujo()
    {
        string sql = "select a.IdHojaRuta, c.NombreCliente,  isnull((sum(b.Monto) + (select sum(DAITotal + SELTotal + ISVTotal) from HojaRutaPreliquidacionDet where IdHojaRuta = a.IdHojaRuta group by IdHojaRuta) /*+ (select sum(ValorTotal) from HojaRutaPagosPortuariosEncabezado where IdHojaRuta = a.IdHojaRuta)*/),'0.00') Total ";
        sql += "from HojaRuta a, HojaRutaEsquemaTramites b, Clientes c where a.IdHojaRuta = b.IdHojaRuta ";
        sql += "and a.CodigoCliente = c.CodigoCliente ";
        sql += "group by a.IdHojaRuta, c.NombreCliente ";
        sql += "order by a.IdHojaRuta, c.NombreCliente";
        this.loadSQL(sql);
    }

    public void loadEsquemaCreditosEncabezado(string IdHojaRuta, int IdFlujo)
    {
        string sql = "select a.IdHojaRuta, c.NombreCliente, (case d.Status when 1 then 'Pendiente' when 2 then 'Aprobada' when -1 then 'Rechazada' end) Estatus, ";
        sql += " isnull(e.Obs,'') Obs, ";
        sql += "(sum(b.Monto) + (select sum(DAITotal + SELTotal + ISVTotal) Total from HojaRutaPreliquidacionDet where IdHojaRuta = '" + IdHojaRuta + "' group by IdHojaRuta)) Total ";
        sql += "from HojaRuta a, HojaRutaEsquemaTramites b, Clientes c, EncabezadoEsquema d, HojaRutaFlujoPortuario e ";
        sql += "where a.IdHojaRuta = b.IdHojaRuta and ";
        sql += "a.CodigoCliente = c.CodigoCliente and ";
        sql += "a.IdHojaRuta = d.IdHojaRuta and ";
        sql += "a.IdHojaRuta = e.IdHojaRuta and e.IdFlujo = " + IdFlujo + " and ";
        sql += "a.IdHojaRuta = '" + IdHojaRuta + "' and d.Status = '0' ";
        sql += "group by a.IdHojaRuta, c.NombreCliente, d.Status, e.Obs ";
        sql += "order by a.IdHojaRuta, c.NombreCliente, d.Status, e.Obs";
        this.loadSQL(sql);
    }

    public void loadEsquemaFlujoDetalle(string IdHojaRuta)
    {
        string sql = "select a.IdFlujo, b.descripcion, a.FchIngreso, a.Obs, a.IdUsuario, c.NombreEmpleado ";
        sql += "from Codigos b, HojaRutaFlujoPortuario a ";
        sql += "left join Empleados c on (a.IdUsuario = c.IdEmpleado) ";
        sql += "where a.IdHojaRuta = '" + IdHojaRuta + "' ";
        sql += "and b.clasificacion = 'FLUJOPUERTO' ";
        sql += "and a.IdFlujo = b.codigo ";
        sql += "order by a.FchIngreso desc";
        this.loadSQL(sql);
    }

    public void loadEsquemaCreditosDetalle(string idHojaRuta)
    {
        //string sql = "select a.IdConceptoSAP ID, c.Descripcion Categoria, b.Concepto Concepto, a.Monto Monto, '' FechaPago ";
        //sql += "from HojaRutaEsquemaTramites a, ConceptoSAP b, CategoriaSAP c ";
        //sql += "where a.IdHojaRuta = '" + idHojaRuta + "' and a.IdConceptoSAP = b.ID and  b.Categoria = c.Categoria and a.Monto > 0 ";
        //sql += "union ";
        //sql += "select '0' ID, 'PRELIQUIDACION' Categoria, 'Pre-Liquidación' Concepto, sum(DAITotal + SELTotal + ISVTotal) Monto, Convert(varchar,b.FechaPago,106) FechaPago ";
        //sql += "from HojaRutaPreliquidacionDet a, HojaRutaPreliquidacionEnc b ";
        //sql += "where a.IdHojaRuta = b.IdHojaRuta and a.IdHojaRuta = '" + idHojaRuta + "' ";
        //sql += "group by b.FechaPago ";
        //sql += "union ";
        //sql += "select '0' ID, 'PROFORMA PORTUARIA' Categoria, 'Proforma Portuaria' Concepto, ValorTotal Monto, '' FechaPago ";
        //sql += "from HojaRutaPagosPortuariosEncabezado ";
        //sql += "where IdHojaRuta = '" + idHojaRuta + "' ";
        //sql += "order by c.Descripcion, b.Concepto";

        string sql = "select a.IdConceptoSAP ID, b.Categoria Categoria, b.Concepto Concepto, a.Monto Monto, '' FechaPago ";
        sql += "from HojaRutaEsquemaTramites a, ConceptoSAP b ";
        sql += "where a.IdHojaRuta = '" + idHojaRuta + "' and a.IdConceptoSAP = b.ID and a.Monto > 0 and b.Categoria <> 'PRELIQUIDACION' ";
        sql += "union ";
        sql += "select '0' ID, 'PRELIQUIDACION' Categoria, 'Pre-Liquidación' Concepto, (sum(DAITotal + SELTotal + ISVTotal + PyC) * Max(Compra)) Monto, Convert(varchar,b.FechaPago,106) FechaPago ";
        sql += "from HojaRutaPreliquidacionDet a, HojaRutaPreliquidacionEnc b, FactorCambio  ";
        sql += "where a.IdHojaRuta = b.IdHojaRuta and a.IdHojaRuta = '" + idHojaRuta + "' ";
        sql += "group by b.FechaPago ";
        sql += "union ";
        sql += "select '0' ID, 'PROFORMA PORTUARIA' Categoria, 'Proforma Portuaria' Concepto, ValorTotal Monto, '' FechaPago ";
        sql += "from HojaRutaPagosPortuariosEncabezado ";
        sql += "where IdHojaRuta = '" + idHojaRuta + "' ";
        sql += "order by  b.Categoria, b.Concepto";
        this.loadSQL(sql);
    }

    public void loadEsquemaIngresoRecibos()
    {
        //string sql = "select a.IdHojaRuta, e.ValorSerie, b.Categoria, a.IdConceptoSAP, b.Concepto, c.descripcion, a.Monto, a.ValorReal, a.NumeroSAP, a.NotaDebito, a.NumRecibo ";
        //sql += "from HojaRutaEsquemaTramites a, ConceptoSAP b, Codigos c, HojaRuta d, ComplementoRecepcion e  ";
        //sql += "where a.IdConceptoSAP = b.ID and a.IdHojaRuta = d.IdHojaRuta and a.IdHojaRuta = e.IdHojaRuta and ";
        //sql += "c.clasificacion = 'MEDIOPAGO' and c.codigo = b.MedioPago and a.NumeroSAP <> '' and a.NotaDebito <> '' and b.Categoria <> 'PRELIQUIDACION' and (NumRecibo is null or NumRecibo = '') ";
        string sql = "select a.IdHojaRuta, e.ValorSerie, b.Categoria, a.IdConceptoSAP, b.Concepto, cod.descripcion, a.NumCheque, a.ProveedorPago, ";
        sql += "a.Monto, a.ValorReal, a.NumeroSAP, a.NotaDebito, a.NumRecibo ";
        sql += "from HojaRutaEsquemaTramites a ";
        sql += "inner join ConceptoSAP b on (a.IdConceptoSAP = b.ID) ";
        sql += "inner join ComplementoRecepcion e on (e.IdHojaRuta = a.IdHojaRuta) ";
        //cambio 11/05/2011 quite 'CC'                                                         //cambio 11/05/2011 quite 'CC'
        sql += "inner join Codigos cod on (cod.clasificacion = 'MEDIOPAGO' AND ((cod.codigo in ('CH') and a.MedioPago = cod.codigo and a.NumCheque is not null) or (cod.codigo not in ('CH') and cod.codigo = a.MedioPago))) ";
        sql += "where a.NumeroSAP <> '' and (a.NotaDebito <> '' or a.NotaDebito is null) and b.Categoria <> 'PRELIQUIDACION' and (a.NumRecibo is null or a.NumRecibo = '') ";
        sql += " and cod.descripcion != 'Transferencia' and cod.descripcion != 'Cheque' or IngresoCHTR is not null ";//linea de codigo agregado 06/07/2012 por Mario Meza
        sql += "union ";
        sql += "select a.IdHojaRuta, e.ValorSerie, b.Categoria, '0' IdConceptoSAP, 'Preliquidacion' Concepto, c.descripcion, a.NumCheque, a.ProveedorPago, sum(a.Monto) Monto, a.ValorReal, a.NumeroSAP, a.NotaDebito, a.NumRecibo ";
        sql += "from HojaRutaEsquemaTramites a, ConceptoSAP b, Codigos c, HojaRuta d, ComplementoRecepcion e  ";
        sql += "where a.IdConceptoSAP = b.ID and a.IdHojaRuta = d.IdHojaRuta and a.IdHojaRuta = e.IdHojaRuta and ";
        sql += "c.clasificacion = 'MEDIOPAGO' and c.codigo = a.MedioPago and a.NumeroSAP <> '' and a.NotaDebito <> '' and b.Categoria = 'PRELIQUIDACION' and (NumRecibo is null or NumRecibo = '') ";
        sql += "group by a.IdHojaRuta, e.ValorSerie, b.Categoria, c.descripcion, a.ValorReal, a.NumeroSAP, a.NotaDebito, a.NumRecibo, a.NumCheque, a.ProveedorPago ";
        sql += "order by a.IdHojaRuta, b.Categoria, b.Concepto";
        this.loadSQL(sql);
    }

    public void loadEsquemaAuxiliarTesoreria(string MedioPago)
    {
        string sql = "select a.IdHojaRuta, f.NombreCliente, e.ValorSerie, e.Correlativo, b.Categoria, a.IdConceptoSAP, b.Concepto, c.descripcion, a.Monto, a.ValorReal, a.NumeroSAP, a.NotaDebito, a.ProveedorPago, b.CuentaContable ";
        sql += "from HojaRutaEsquemaTramites a, ConceptoSAP b, Codigos c, HojaRuta d, ComplementoRecepcion e, Clientes f ";
        sql += "where a.IdConceptoSAP = b.ID and a.IdHojaRuta = d.IdHojaRuta and a.IdHojaRuta = e.IdHojaRuta and ";
        sql += "c.clasificacion = 'MEDIOPAGO' and c.codigo = a.MedioPago and (a.ValorReal <= a.Monto or a.Aprobar = '1') and a.ValorReal > 0 and a.Solicitar = '1' ";

        if (MedioPago != "ALL")
            sql += "and a.MedioPago = '" + MedioPago + "' ";

        sql += "and (a.NumeroSAP is null /*or a.NotaDebito is null*/) ";
        sql += "and b.Categoria <> 'PRELIQUIDACION' and d.CodigoCliente = f.CodigoCliente ";
        sql += "union ";
        sql += "select a.IdHojaRuta, f.NombreCliente, e.ValorSerie, e.Correlativo, b.Categoria, '0' IdConceptoSAP, 'Preliquidación' Concepto, c.descripcion, ";
        sql += "(sum(a.Monto) * Max(Compra)) Monto, a.ValorReal, a.NumeroSAP, a.NotaDebito, a.ProveedorPago, b.CuentaContable ";
        sql += "from HojaRutaEsquemaTramites a, ConceptoSAP b, Codigos c, HojaRuta d, ComplementoRecepcion e, Clientes f, FactorCambio  ";
        sql += "where a.IdConceptoSAP = b.ID and a.IdHojaRuta = d.IdHojaRuta and a.IdHojaRuta = e.IdHojaRuta and c.clasificacion = 'MEDIOPAGO' and ";
        sql += "c.codigo = a.MedioPago and a.ValorReal > a.Monto and a.ValorReal > 0 and a.Solicitar = '1' and b.Categoria = 'PRELIQUIDACION' and (a.NumeroSAP is null /*or a.NotaDebito is null*/) and d.CodigoCliente = f.CodigoCliente ";

        if (MedioPago != "ALL")
            sql += "and a.MedioPago = '" + MedioPago + "'";

        sql += "group by a.IdHojaRuta, f.NombreCliente, e.ValorSerie, b.Categoria, c.descripcion, a.ValorReal, a.NumeroSAP, a.NotaDebito, a.Aprobar, e.Correlativo, a.ProveedorPago, b.CuentaContable ";
        sql += "having (a.ValorReal < (sum(a.Monto) * Max(Compra)) or a.Aprobar = '1') ";
        sql += "order by a.IdHojaRuta, b.Categoria, b.Concepto";

        //string sql = "select a.IdHojaRuta, f.NombreCliente, e.ValorSerie, e.Correlativo, b.Categoria, a.IdConceptoSAP, b.Concepto, c.descripcion, a.Monto, a.ValorReal, a.NumeroSAP, a.NotaDebito, a.ProveedorPago, b.CuentaContable ";
        //sql += "from HojaRutaEsquemaTramites a, ConceptoSAP b, Codigos c, HojaRuta d, ComplementoRecepcion e, Clientes f ";
        //sql += "where a.IdConceptoSAP = b.ID and a.IdHojaRuta = d.IdHojaRuta and a.IdHojaRuta = e.IdHojaRuta and ";
        //sql += "c.clasificacion = 'MEDIOPAGO' and c.codigo = a.MedioPago and (a.ValorReal <= a.Monto or a.Aprobar = '1') and a.ValorReal > 0 and a.Solicitar = '1' ";

        //if (MedioPago != "ALL")
        //    sql += "and a.MedioPago = '" + MedioPago + "' ";

        //sql += "and (a.NumeroSAP is null /*or a.NotaDebito is null*/) ";
        //sql += "and b.Categoria <> 'PRELIQUIDACION' and d.CodigoCliente = f.CodigoCliente ";
        //sql += "union ";
        //sql += "select a.IdHojaRuta, f.NombreCliente, e.ValorSerie, e.Correlativo, b.Categoria, '0' IdConceptoSAP, Concepto, c.descripcion, ";
        //sql += "((a.Monto) * Compra) Monto, a.ValorReal, a.NumeroSAP, a.NotaDebito, a.ProveedorPago, b.CuentaContable ";
        //sql += "from HojaRutaEsquemaTramites a, ConceptoSAP b, Codigos c, HojaRuta d, ComplementoRecepcion e, Clientes f, FactorCambio  ";
        //sql += "where a.IdConceptoSAP = b.ID and a.IdHojaRuta = d.IdHojaRuta and a.IdHojaRuta = e.IdHojaRuta and c.clasificacion = 'MEDIOPAGO' and ";
        //sql += "c.codigo = a.MedioPago and a.ValorReal > a.Monto and a.ValorReal > 0 and a.Solicitar = '1' and b.Categoria = 'PRELIQUIDACION' and (a.NumeroSAP is null /*or a.NotaDebito is null*/) and d.CodigoCliente = f.CodigoCliente ";

        //if (MedioPago != "ALL")
        //    sql += "and a.MedioPago = '" + MedioPago + "'";
        //sql += "and (a.ValorReal < (a.Monto * Compra) or a.Aprobar = '1') ";
        //sql += "order by a.IdHojaRuta, b.Categoria, b.Concepto";

        this.loadSQL(sql);
    }

    public void loadEsquemaAuxiliarTesoreria(string IdEmpleado, string IdRole)
    {
        string sql = "select a.IdHojaRuta, f.NombreCliente, e.ValorSerie, e.Correlativo, b.Categoria, a.IdConceptoSAP, b.Concepto, c.descripcion, a.Monto, a.ValorReal, a.NumeroSAP, a.NotaDebito, a.ProveedorPago, b.CuentaContable ";
        sql += "from HojaRutaEsquemaTramites a, ConceptoSAP b, Codigos c, HojaRuta d, ComplementoRecepcion e, Clientes f ";
        sql += "where a.IdConceptoSAP = b.ID and a.IdHojaRuta = d.IdHojaRuta and a.IdHojaRuta = e.IdHojaRuta and ";
        sql += "c.clasificacion = 'MEDIOPAGO'";
        //Si es Dina solo muestra las notas si es alguien mas muestra lo que tenga que mostrar
        if (IdEmpleado == "78")
        {
            sql += " and c.codigo = a.MedioPago and a.MedioPago = 'ND'";
        }
        else
        {
        sql +=" and c.codigo = a.MedioPago";
        }
        //////////////////Agregado por Darwin Bonilla 10-01-2013////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////
        sql +=" and (a.ValorReal <= a.Monto or a.Aprobar = '1') and a.ValorReal > 0 and a.Solicitar = '1' and ";

        if (IdRole != "1")
            sql += "a.MedioPago in (select codigo from EmpleadoMedioPago where IdEmpleado = " + IdEmpleado + ") and  ";

        sql += " (a.NumeroSAP is null /*or a.NotaDebito is null*/) ";
        //cambio 11/05/2011 agregue and a.MedioPago <> 'CC'
        sql += "and b.Categoria <> 'PRELIQUIDACION' and d.CodigoCliente = f.CodigoCliente and a.MedioPago <> 'CC' ";
        sql += "union ";
        sql += "select a.IdHojaRuta, f.NombreCliente, e.ValorSerie, e.Correlativo, b.Categoria, '0' IdConceptoSAP, 'Preliquidación' Concepto, c.descripcion, ";
        sql += "(sum(a.Monto) * Max(Compra)) Monto, a.ValorReal, a.NumeroSAP, a.NotaDebito, a.ProveedorPago, b.CuentaContable ";
        sql += "from HojaRutaEsquemaTramites a, ConceptoSAP b, Codigos c, HojaRuta d, ComplementoRecepcion e, Clientes f, FactorCambio  ";
        sql += "where a.IdConceptoSAP = b.ID and a.IdHojaRuta = d.IdHojaRuta and a.IdHojaRuta = e.IdHojaRuta and c.clasificacion = 'MEDIOPAGO' and ";
        //cambio 11/05/2011 agregue and a.MedioPago <> 'CC'
        sql += "c.codigo = a.MedioPago and a.ValorReal > a.Monto and a.ValorReal > 0 and a.Solicitar = '1' and b.Categoria = 'PRELIQUIDACION' and (a.NumeroSAP is null /*or a.NotaDebito is null*/) and d.CodigoCliente = f.CodigoCliente and a.MedioPago <> 'CC' ";

        if (IdRole != "1")
            //Si es Dina solo muestra las notas si es alguien mas muestra lo que tenga que mostrar
            if (IdEmpleado == "78")
            {
                sql += " and a.MedioPago in ('ND') ";
            }
            else
            {
                sql += " and a.MedioPago in (select codigo from EmpleadoMedioPago where IdEmpleado = " + IdEmpleado + ") ";
            }
        //////////////////Agregado por Darwin Bonilla 10-01-2013////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////
        sql += "group by a.IdHojaRuta, f.NombreCliente, e.ValorSerie, b.Categoria, c.descripcion, a.ValorReal, a.NumeroSAP, a.NotaDebito, a.Aprobar, e.Correlativo, a.ProveedorPago, b.CuentaContable ";
        sql += "having (a.ValorReal < (sum(a.Monto) * Max(Compra)) or a.Aprobar = '1') ";
        sql += "order by a.IdHojaRuta, b.Categoria, b.Concepto";
    
        this.loadSQL(sql);
    }

    public void loadEsquemaDeptoTesoreria()
    {
        string sql = "select a.IdHojaRuta, NombreCliente, e.ValorSerie, b.Categoria, a.IdConceptoSAP, b.Concepto, c.descripcion, a.Monto, a.ValorReal, a.NumeroSAP, a.NotaDebito  ";
        sql += "from HojaRutaEsquemaTramites a, ConceptoSAP b, Codigos c, HojaRuta d inner join Clientes cl on (d.CodigoCliente = cl.CodigoCliente), ComplementoRecepcion e ";
        sql += "where a.IdConceptoSAP = b.ID and a.IdHojaRuta = d.IdHojaRuta and a.IdHojaRuta = e.IdHojaRuta and ";
        sql += "c.clasificacion = 'MEDIOPAGO' and c.codigo = a.MedioPago and a.ValorReal > a.Monto and a.ValorReal > 0 and a.Solicitar = '1' and a.Aprobar = '0' and b.Categoria <> 'PRELIQUIDACION' ";
        sql += "union ";
        sql += "select a.IdHojaRuta, NombreCliente, e.ValorSerie, b.Categoria, '0' IdConceptoSAP, 'Preliquidación' Concepto, c.descripcion, ";
        sql += "(sum(a.Monto) * Max(Compra)) Monto, a.ValorReal, a.NumeroSAP, a.NotaDebito ";
        sql += "from HojaRutaEsquemaTramites a, ConceptoSAP b, Codigos c, HojaRuta d inner join Clientes cl on (d.CodigoCliente = cl.CodigoCliente), ComplementoRecepcion e, FactorCambio ";
        sql += "where a.IdConceptoSAP = b.ID and a.IdHojaRuta = d.IdHojaRuta and a.IdHojaRuta = e.IdHojaRuta and c.clasificacion = 'MEDIOPAGO' and ";
        sql += "c.codigo = a.MedioPago and a.ValorReal > a.Monto and a.ValorReal > 0 and a.Solicitar = '1' and a.Aprobar = '0' and b.Categoria = 'PRELIQUIDACION' ";
        sql += "group by a.IdHojaRuta, NombreCliente, e.ValorSerie, b.Categoria, c.descripcion, a.ValorReal, a.NumeroSAP, a.NotaDebito ";
        sql += "having a.ValorReal > (sum(a.Monto) * Max(Compra)) ";
        sql += "order by a.IdHojaRuta, b.Categoria, b.Concepto ";
        this.loadSQL(sql);
    }

    public void loadEsquemaContraRecibo(int IdEncargado)
    {
        string sql = "select distinct(a.NumFactSAP), a.NumContraRecibo ";
        sql += "from HojaRutaEsquemaTramites a, ConceptoSAP b, Codigos c, HojaRuta d, ComplementoRecepcion e  ";
        sql += "where a.IdConceptoSAP = b.ID and a.IdHojaRuta = d.IdHojaRuta and a.IdHojaRuta = e.IdHojaRuta and ";
        sql += "c.clasificacion = 'MEDIOPAGO' and c.codigo = b.MedioPago and a.ValorReal > 0 and a.NumFactSAP <> '' and  ";
        //sql += "a.VFReal > 0 and NumContraRecibo is null and a.IdEncargado = " + IdEncargado + " ";
        sql += "a.VFReal > 0 and a.IdEncargado = " + IdEncargado + " and a.FlagCorte = 0 ";
        sql += "group by a.NumFactSAP, a.NumContraRecibo ";
        sql += "order by a.NumFactSAP, a.NumContraRecibo";
        this.loadSQL(sql);
    }

    public void loadEsquemaIngresoCheques(string CodModPago)
    {
        string sql = "select distinct(a.NumeroSAP), a.IdHojaRuta, a.ProveedorPago, sum(a.ValorReal) ValorReal, a.NumCheque, b.Concepto, (case a.MedioPago when 'CC' then 'Caja Chica' else 'Cheque' end) MedioPago ";
        sql += "from HojaRutaEsquemaTramites a, ConceptoSAP b, Codigos c, HojaRuta d, ComplementoRecepcion e  ";
        sql += "where a.IdConceptoSAP = b.ID and a.IdHojaRuta = d.IdHojaRuta and a.IdHojaRuta = e.IdHojaRuta and ";
        sql += "c.clasificacion = 'MEDIOPAGO' and c.codigo = a.MedioPago and a.ValorReal > 0 and (a.NumeroSAP <> '' or a.NumeroSAP is not null) and ";
        //sql += "b.MedioPago = '" + CodModPago + "' and a.FlagCorte = 0 and (a.NumCheque is null or a.NumCheque = '') ";
        sql += "a.MedioPago in ('CH','CC') and a.FlagCorte = 0 and (a.NumCheque is null or a.NumCheque = '') ";
        sql += "group by a.IdHojaRuta, a.NumeroSAP, a.ProveedorPago, a.NumCheque, a.MedioPago, b.Concepto ";
        sql += "having sum(a.ValorReal) > 0 ";
        sql += "order by a.IdHojaRuta, a.NumeroSAP, a.ProveedorPago, a.NumCheque";
        this.loadSQL(sql);
    }

    public void loadEsquemaIngresoChequesxEmpleado(string IdEmpleado, string IdRole)
    {
        string sql = "select distinct(a.NumeroSAP), a.IdHojaRuta, a.ProveedorPago, sum(a.ValorReal) ValorReal, a.NumCheque, b.Concepto, c.descripcion MedioPago ";
        sql += "from HojaRutaEsquemaTramites a, ConceptoSAP b, Codigos c, HojaRuta d, ComplementoRecepcion e ";
        sql += "where a.IdConceptoSAP = b.ID and a.IdHojaRuta = d.IdHojaRuta and a.IdHojaRuta = e.IdHojaRuta and c.clasificacion = 'MEDIOPAGO' and ";
        //cambio 11/05/2011 agregue 'CC'
        sql += "c.codigo not in ('EF', 'ND', 'CC') and c.codigo = a.MedioPago and a.ValorReal > 0 and (a.NumeroSAP <> '' or a.NumeroSAP is not null) and ";

        if (IdRole != "1")
            sql += "a.MedioPago in (select codigo from EmpleadoMedioPago where IdEmpleado = " + IdEmpleado + ") and  ";

        sql += "a.FlagCorte = 0 and (a.NumCheque is null or a.NumCheque = '') ";
        if (IdEmpleado == "78")
        {
            sql += "and Concepto in ('Almacenaje Cargill','Digitación electronica STD','Almacenaje (No a la ENP)')";
        }
        sql += "group by a.IdHojaRuta, a.NumeroSAP, a.ProveedorPago, a.NumCheque, a.MedioPago, b.Concepto, c.descripcion ";
        sql += "having sum(a.ValorReal) > 0 ";
        sql += "order by a.IdHojaRuta, a.NumeroSAP, a.ProveedorPago, a.NumCheque";
        this.loadSQL(sql);
    }

    public void loadEsquemaTramiteHojaRutaDocs(string idHojaRuta)
    {
        string sql = "select a.IdConceptoSAP, c.Descripcion, b.Concepto, d.FchEnvio, a.ValorReal ";
        sql += "from CategoriaSAP c, ConceptoSAP b, HojaRutaEsquemaTramites a  ";
        sql += "left join EsquemaEnvioDoctos d on (d.IdHojaRuta = a.IdHojaRuta and d.IdConceptoSAP = a.IdConceptoSAP) ";
        sql += "where a.IdHojaRuta = '" + idHojaRuta + "' and a.IdConceptoSAP = b.ID and a.ValorReal > 0 and  b.Categoria = c.Categoria and (NumRecibo is not null or NumRecibo <> '') ";
        sql += "union ";
        sql += "select '0' IdConceptoSAP, b.Categoria Descripcion, 'Preliquidación' Concepto, c.FchEnvio, a.ValorReal ";
        sql += "from HojaRutaEsquemaTramites a, ConceptoSAP b, HojaRutaPreliquidacionEnc c ";
        sql += "where a.IdConceptoSAP = b.ID and b.Categoria = 'PRELIQUIDACION' and a.IdHojaRuta = '" + idHojaRuta + "' and a.IdHojaRuta = c.IdHojaRuta and (NumRecibo is not null or NumRecibo <> '') ";
        sql += "group by b.Categoria, a.ValorReal, c.FchEnvio ";
        sql += "order by c.Descripcion, b.Concepto";
        this.loadSQL(sql);
    }

    public void loadEsquemaTramiteHojaRutaSinAsignar()
    {
        string sql = "select distinct(a.NumFactSAP), a.IdEncargado ";
        sql += "from HojaRutaEsquemaTramites a, ConceptoSAP b, Codigos c, HojaRuta d, ComplementoRecepcion e ";
        sql += "where a.IdConceptoSAP = b.ID and a.IdHojaRuta = d.IdHojaRuta and a.IdHojaRuta = e.IdHojaRuta and  ";
        sql += "c.clasificacion = 'MEDIOPAGO' and c.codigo = b.MedioPago and a.ValorReal > 0 and a.NumFactSAP <> '' and ";
        sql += "a.VFReal > 0 and NumContraRecibo is null and a.IdEncargado is null ";
        sql += "group by a.NumFactSAP, a.IdEncargado order by a.NumFactSAP, a.IdEncargado";
        this.loadSQL(sql);
    }

    public void loadRptFlujoEsquemaTramite(string IdHojaRuta)
    {
        string sql = "Exec [dbo].[RptFlujoEsquemaTramite] '" + IdHojaRuta + "'";
        this.loadSQL(sql);
    }

    public void loadRptFlujoContraRecibos(int IdEmpleado)
    {
        string sql = "Exec [dbo].[RptContraRecibos] " + IdEmpleado;
        this.loadSQL(sql);
    }

    public void UpdateNumeroContraRecibos(string NumFactura, string NumContraRecibo, int IdEncargado)
    {
        string sql = "Update HojaRutaEsquemaTramites set NumContraRecibo = '" + NumContraRecibo + "', FchIngresoContraRecibo = GetDate() where NumFactSAP = '" + NumFactura + "' and IdEncargado = " + IdEncargado;
        this.executeCustomSQL(sql);
    }

    public void UpdateNumeroCheque(string NumeroSAP, string NumCheque, int IdEncargado, string idHojaRuta) //NumFactSAP
    {
        string sql = "Update HojaRutaEsquemaTramites set NumCheque = '" + NumCheque + "', FchIngresoContraRecibo = GetDate(), IdEncargado = " + IdEncargado + ", IngresoCHTR = 1 where NumeroSAP = '" + NumeroSAP + "' and IdHojaRuta='" + idHojaRuta + "'";
        this.executeCustomSQL(sql);
    }

    public void UpdateAsigancionRecibos(string NumFactura, int IdEncargado)
    {
        string sql = "Update HojaRutaEsquemaTramites set IdEncargado = " + IdEncargado + ", FchAsignacion = GetDate() where NumFactSAP = '" + NumFactura + "'";
        this.executeCustomSQL(sql);
    }

    public void UpdateAsigancionRecibos(int IdEncargado)
    {
        string sql = "Update HojaRutaEsquemaTramites set FlagCorte = '1' where IdEncargado = " + IdEncargado + " and NumFactSAP in (select distinct(NumFactSAP) from HojaRutaEsquemaTramites where IdEncargado = " + IdEncargado + " and NumContraRecibo is not null)";
        this.executeCustomSQL(sql);
    }

    public void loadNumeroSAP()
    {
        string sql = "Select * from HojaRutaEsquemaTramites where NumCheque like 'p-%' or NumCheque like 'P-%' and MedioPago = 'TR'";
        this.loadSQL(sql);
    }

    public void loadNumeroSAPHojaRuta(string NumCheque, string IdConceptoSAP, string IdHojaRuta)
    {
        string sql = " select * from HojaRutaEsquemaTramites where NumCheque = '" + NumCheque + "' and IdConceptoSAP = '" + IdConceptoSAP + "' and IdHojaRuta = '" + IdHojaRuta + "'";
        this.loadSQL(sql);
    }

    public void CargarValores(string IdHojaRuta)
    {
        string sql = "Select IdHojaRuta, B.ID, B.Categoria, B.Concepto, C.descripcion, Monto, ValorReal, NumeroSAP,NotaDebito From HojaRutaEsquemaTramites A";
               sql += " Inner Join ConceptoSAP B on (A.IdConceptoSAP = B.ID) Inner Join Codigos C on (C.clasificacion = 'MEDIOPAGO' and A.MedioPago = C.codigo)";
               sql += "Where IdHojaRuta = '" + IdHojaRuta + "' and NumeroSAP IS NOT NULL and NotaDebito IS NOT NULL";
        this.loadSQL(sql);
    }

    #region campos
    public String IdHojaRuta
    {
        get
        {
            return registro[Campos.IdHojaRuta].ToString();
        }
        set
        {
            registro[Campos.IdHojaRuta] = value;
        }
    }

    public Int16 IdConceptoSAP
    {
        get
        {
            return Convert.ToInt16(registro[Campos.IdConceptoSAP].ToString());
        }
        set
        {
            registro[Campos.IdConceptoSAP] = value;
        }
    }

    public String MedioPago
    {
        get
        {
            return registro[Campos.MedioPago].ToString();
        }
        set
        {
            registro[Campos.MedioPago] = value;
        }
    }

    public Double Monto
    {
        get
        {
            return Convert.ToDouble(registro[Campos.Monto].ToString());
        }
        set
        {
            registro[Campos.Monto] = value;
        }
    }

    public Double ValorReal
    {
        get
        {
            return Convert.ToDouble(registro[Campos.ValorReal].ToString());
        }
        set
        {
            registro[Campos.ValorReal] = value;
        }
    }

    public String Solicitar
    {
        get
        {
            return registro[Campos.Solicitar].ToString();
        }
        set
        {
            registro[Campos.Solicitar] = value;
        }
    }

    public String Aprobar
    {
        get
        {
            return registro[Campos.Aprobar].ToString();
        }
        set
        {
            registro[Campos.Aprobar] = value;
        }
    }

    public String NumeroSAP
    {
        get
        {
            return registro[Campos.NumeroSAP].ToString();
        }
        set
        {
            registro[Campos.NumeroSAP] = value;
        }
    }

    public String NotaDebito
    {
        get
        {
            return registro[Campos.NotaDebito].ToString();
        }
        set
        {
            registro[Campos.NotaDebito] = value;
        }
    }

    public DateTime FechaPago
    {
        get
        {
            return Convert.ToDateTime(registro[Campos.FechaPago].ToString());
        }
        set
        {
            registro[Campos.FechaPago] = value;
        }
    }

    public String NumRecibo
    {
        get
        {
            return registro[Campos.NumRecibo].ToString();
        }
        set
        {
            registro[Campos.NumRecibo] = value;
        }
    }

    public String NumFactSAP
    {
        get
        {
            return registro[Campos.NumFactSAP].ToString();
        }
        set
        {
            registro[Campos.NumFactSAP] = value;
        }
    }

    public Decimal VFReal
    {
        get
        {
            return Convert.ToDecimal(registro[Campos.VFReal].ToString());
        }
        set
        {
            registro[Campos.VFReal] = value;
        }
    }

    public String NumContraRecibo
    {
        get
        {
            return registro[Campos.NumContraRecibo].ToString();
        }
        set
        {
            registro[Campos.NumContraRecibo] = value;
        }
    }

    public Decimal VCReal
    {
        get
        {
            return Convert.ToDecimal(registro[Campos.VCReal].ToString());
        }
        set
        {
            registro[Campos.VCReal] = value;
        }
    }

    public DateTime FchFactura
    {
        get
        {
            return Convert.ToDateTime(registro[Campos.FchFactura].ToString());
        }
        set
        {
            registro[Campos.FchFactura] = value;
        }
    }

    public String CodigoClienteSAP
    {
        get
        {
            return registro[Campos.CodigoClienteSAP].ToString();
        }
        set
        {
            registro[Campos.CodigoClienteSAP] = value;
        }
    }

    public Int16 IdEncargado
    {
        get
        {
            return Convert.ToInt16(registro[Campos.IdEncargado].ToString());
        }
        set
        {
            registro[Campos.IdEncargado] = value;
        }
    }

    public String FlagCorte
    {
        get
        {
            return registro[Campos.FlagCorte].ToString();
        }
        set
        {
            registro[Campos.FlagCorte] = value;
        }
    }

    public String NumCheque
    {
        get
        {
            return registro[Campos.NumCheque].ToString();
        }
        set
        {
            registro[Campos.NumCheque] = value;
        }
    }

    public String ProveedorPago
    {
        get
        {
            return registro[Campos.ProveedorPago].ToString();
        }
        set
        {
            registro[Campos.ProveedorPago] = value;
        }
    }

    public String INGRESOCHTR
    {
        get
        {
            return registro[Campos.INGRESOCHTR].ToString();
        }
        set
        {
            registro[Campos.INGRESOCHTR] = value;
        }
    }
    #endregion

}