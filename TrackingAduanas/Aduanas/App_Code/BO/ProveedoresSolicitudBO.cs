﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;

using GrupoLis.Ebase;
using GrupoLis.Login;

public class ProveedoresSolicitudBO : CapaBase
{
    class Campos
    {
        public static string ID = "Id";
        public static string CUENTA = "Cuenta";
        public static string NOMBRE = "Nombre";
        public static string CODPAIS = "CodPais";
        public static string ESTADO = "Estado";
    }

    public ProveedoresSolicitudBO(GrupoLis.Login.Login conector)
        : base(conector, true)
    {
        coreSQL = " SELECT * FROM ProveedoresSolicitud ";
        initializeSchema("ProveedoresSolicitud");
    }

    public void loadProveedoresPais(string codPais)
    {
        string sql = "Select * From ProveedoresSolicitud ";
        sql += " WHERE CodPais = '" + codPais + "' AND Estado  = '0' ";
        this.loadSQL(sql);
    }

    public void loadProveedoresTodos()
    {
        string sql = "SELECT IdProveedor,Cuenta,Nombre ";
        sql += " ,Case CodPais When 'N' Then 'Nicaragua' When 'G' Then 'Guatemala' Else 'El Salvador' End Pais ";
        sql += " ,Case Estado When 0 Then 'Habilitado' Else 'Deshabilitado' End as Estado ";
        sql += " ,CodPais FROM ProveedoresSolicitud";
        this.loadSQL(sql);
    }
    public void VerificarProveedor(string Cuenta, string Pais)
    {
        string sql = " Select * From ProveedoresSolicitud Where Cuenta = '" + Cuenta + "' and CodPais = '" + Pais + "' ";
        this.loadSQL(sql);
    }
    public void VerificarProveedorXcuenta(string Cuenta)
    {
        string sql = " Select * From ProveedoresSolicitud Where Cuenta = '" + Cuenta + "' and Estado = 0";
        this.loadSQL(sql);
    }
    public void VerificarProveedorXId(string Id)
    {
        string sql = " Select * From ProveedoresSolicitud Where IdProveedor = '" + Id + "' ";
        this.loadSQL(sql);
    }
    #region Campos

    public String ID
    {
        get
        {
            return Convert.ToString(registro[Campos.ID]);
        }
        set
        {
            registro[Campos.ID] = value;
        }
    }
    public String CUENTA
    {
        get
        {
            return Convert.ToString(registro[Campos.CUENTA]);
        }
        set
        {
            registro[Campos.CUENTA] = value.Trim();
        }
    }
    public String NOMBRE
    {
        get
        {
            return Convert.ToString(registro[Campos.NOMBRE]);
        }
        set
        {
            registro[Campos.NOMBRE] = value.Trim();
        }
    }
    public String CODPAIS
    {
        get
        {
            return Convert.ToString(registro[Campos.CODPAIS]);
        }
        set
        {
            registro[Campos.CODPAIS] = value.Trim();
        }
    }
    public String ESTADO
    {
        get
        {
            return Convert.ToString(registro[Campos.ESTADO]);
        }
        set
        {
            registro[Campos.ESTADO] = value.Trim();
        }
    }
    #endregion
}
