﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for EncabezadoEsquemaBO
/// </summary>
public class EncabezadoEsquemaBO: CapaBase
{
    class Campos
    {
        public static string IdHojaRuta = "IdHojaRuta";
        public static string CodigoCliente = "CodigoCliente";
        public static string ValorCIF = "ValorCIF";
        public static string Peso = "Peso";
        public static string DescripcionProducto = "DescripcionProducto";
        public static string TipoCarga = "TipoCarga";
        public static string Contenedores = "Contenedores";
        public static string TipoCargamento = "TipoCargamento";
        public static string Importador = "Importador";
        public static string Tramitador = "Tramitador";
        public static string VecimientoNaviera = "VecimientoNaviera";
        public static string Status = "Status";
        public static string Flujo = "Flujo";
        public static string IMPUESTODEI = "ImpuestoDEI";
    }

    public EncabezadoEsquemaBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from EncabezadoEsquema";
        this.initializeSchema("EncabezadoEsquema");
    }

    public void loadEncabezadoEsquema()
    {
        string sql = this.coreSQL;
        this.loadSQL(sql);
    }

    public void loadEncabezadoEsquema(string IdHojaRuta)
    {
        string sql = this.coreSQL;
        sql += " where IdHojaRuta = '" + IdHojaRuta + "'";
        this.loadSQL(sql);
    }

    public string IdHojaRuta
    {
        get
        {
            return (string)registro[Campos.IdHojaRuta].ToString();
        }
        set
        {
            registro[Campos.IdHojaRuta] = value;
        }
    }

    public string CodigoCliente
    {
        get
        {
            return (string)registro[Campos.CodigoCliente].ToString();
        }
        set
        {
            registro[Campos.CodigoCliente] = value;
        }
    }

    public double ValorCIF
    {
        get
        {
            return double.Parse(registro[Campos.ValorCIF].ToString());
        }
        set
        {
            registro[Campos.ValorCIF] = value;
        }
    }

    public double Peso
    {
        get
        {
            return double.Parse(registro[Campos.Peso].ToString());
        }
        set
        {
            registro[Campos.Peso] = value;
        }
    }

    public string DescripcionProducto
    {
        get
        {
            return registro[Campos.DescripcionProducto].ToString();
        }
        set
        {
            registro[Campos.DescripcionProducto] = value;
        }
    }

    public string TipoCarga
    {
        get
        {
            return (string)registro[Campos.TipoCarga].ToString();
        }
        set
        {
            registro[Campos.TipoCarga] = value;
        }
    }

    public Int16 Contenedores
    {
        get
        {
            return Convert.ToInt16(registro[Campos.Contenedores].ToString());
        }
        set
        {
            registro[Campos.Contenedores] = value;
        }
    }

    public string TipoCargamento
    {
        get
        {
            return (string)registro[Campos.TipoCargamento].ToString();
        }
        set
        {
            registro[Campos.TipoCargamento] = value;
        }
    }

    public string Importador
    {
        get
        {
            return (string)registro[Campos.Importador].ToString();
        }
        set
        {
            registro[Campos.Importador] = value;
        }
    }

    public string Tramitador
    {
        get
        {
            return (string)registro[Campos.Tramitador].ToString();
        }
        set
        {
            registro[Campos.Tramitador] = value;
        }
    }

    public DateTime VecimientoNaviera
    {
        get
        {
            return Convert.ToDateTime(registro[Campos.VecimientoNaviera].ToString());
        }
        set
        {
            registro[Campos.VecimientoNaviera] = value;
        }
    }

    public string Status
    {
        get
        {
            return (string)registro[Campos.Status].ToString();
        }
        set
        {
            registro[Campos.Status] = value;
        }
    }

    public Int16 Flujo
    {
        get
        {
            return Convert.ToInt16(registro[Campos.Flujo].ToString());
        }
        set
        {
            registro[Campos.Flujo] = value;
        }
    }


    public string IMPUESTODEI
    {
        get
        {
            return (string)registro[Campos.IMPUESTODEI].ToString();
        }
        set
        {
            registro[Campos.IMPUESTODEI] = value;
        }
    }

}
