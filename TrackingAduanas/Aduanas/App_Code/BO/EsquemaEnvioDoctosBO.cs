﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for EsquemaEnvioDoctosBO
/// </summary>
public class EsquemaEnvioDoctosBO : CapaBase
{
    class Campos
    {
        public static string IdHojaRuta = "IdHojaRuta";
        public static string IdConceptoSAP = "IdConceptoSAP";
        public static string IdEmpleado = "IdEmpleado";
    }

    public EsquemaEnvioDoctosBO(GrupoLis.Login.Login log): base(log, true)
	{
        this.coreSQL = "Select * from EsquemaEnvioDoctos";
        this.initializeSchema("EsquemaEnvioDoctos");
    }

    public void loadAllDoctos(string IdHojaRuta)
    {
        string sql = this.coreSQL;
        sql += " where IdHojaRuta = '" + IdHojaRuta + "'";
        this.loadSQL(sql);
    }

    public void loadAllDoctos(string IdHojaRuta, int IdConceptoSAP)
    {
        string sql = this.coreSQL;
        sql += " where IdHojaRuta = '" + IdHojaRuta + "' and IdConceptoSAP = " + IdConceptoSAP;
        this.loadSQL(sql);
    }

    public void loadAllDoctosDsc(string idHojaRuta)
    {
        string sql = "select a.IdConceptoSAP ID, c.Descripcion Categoria, b.Concepto Concepto ";
        sql += "from HojaRutaEsquemaTramites a, ConceptoSAP b, CategoriaSAP c  ";
        sql += "where a.IdHojaRuta = '" + idHojaRuta + "' and ";
        sql += "a.IdConceptoSAP = b.ID and  ";
        sql += "b.Categoria = c.Categoria ";
        sql += "order by c.Descripcion, b.Concepto";
        this.loadSQL(sql);
    }

    public String IdHojaRuta
    {
        get
        {
            return registro[Campos.IdHojaRuta].ToString();
        }
        set
        {
            registro[Campos.IdHojaRuta] = value;
        }
    }

    public Int16 IdConceptoSAP
    {
        get
        {
            return Convert.ToInt16(registro[Campos.IdConceptoSAP].ToString());
        }
        set
        {
            registro[Campos.IdConceptoSAP] = value;
        }
    }

    public String IdEmpleado
    {
        get
        {
            return registro[Campos.IdEmpleado].ToString();
        }
        set
        {
            registro[Campos.IdEmpleado] = value;
        }
    }
}