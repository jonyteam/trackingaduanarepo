using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for ComplementoRecepcionBO
/// </summary>
public class ComplementoRecepcionBO: CapaBase
{

    class Campos
    {
        public static string IDHOJARUTA = "IdHojaRuta";
        public static string CORRELATIVO = "Correlativo";
        public static string CODIGOSERIE = "CodigoSerie";
        public static string VALORSERIE = "ValorSerie";
        public static string VALORFOB = "ValorFOB";
        public static string FLETE = "Flete";
        public static string SEGURO = "Seguro";
        public static string OTROS = "Otros";
        public static string VALORCIF = "ValorCIF";
        public static string VALOREMBARQUE = "ValorEmbarque";
        public static string CANTIDAD = "Cantidad";
        public static string PESO = "Peso";
        public static string CODIGOREGIMEN = "CodigoRegimen";
        public static string CODIGOTRANSPORTE = "CodigoTransporte";
        public static string ETA = "Eta";
        public static string TIEMPOESTIMADOENTREGA = "TiempoEstimadoEntrega";
        public static string CODIGOPAIS = "CodigoPais";
        public static string DESTINO = "Destino";
        public static string IMPUESTO = "Impuesto";
        public static string FECHAVENCIMIENTO = "FechaVencimiento";
        public static string ANTICIPO = "Anticipo";
        public static string VALORMULTA = "ValorMulta";
        public static string VALORDEMORA = "ValorDemora";
        public static string IDRESPONSABLE = "IdResponsable";
        public static string NOMBRERESPONSABLE = "NombreResponsable";
        public static string CODIGODOCEMBARQUE = "CodigoDocEmbarque";
        public static string TIPOCARGA = "TipoCarga";
        public static string FECHAVENCIMIENTOENP = "FechaVencimientoENP";
        public static string VALORLIQUIDACION = "ValorLiquidacion";
        public static string OTROSFONDOS = "OtrosFondos";
        public static string ESTADO = "Estado";
        public static string VALORPROVISIONAMIENTOEST = "ValorProvisionamientoEst";
    }
    
    public ComplementoRecepcionBO(GrupoLis.Login.Login log): base(log, true)
	{
        this.coreSQL = "Select * from ComplementoRecepcion";
        this.initializeSchema("ComplementoRecepcion");
    }

    public void loadComplementoRecepcion()
    {
        string sql = this.coreSQL;
        sql += " where Estado = '0'";
        this.loadSQL(sql);
    }

    public void loadComplementoRecepcion(string idHoja)
    {
        string sql = this.coreSQL;
        sql += " where IdHojaRuta = '" + idHoja + "' and Estado = '0'";
        this.loadSQL(sql);
    }

    public void loadComplementoRecepcion(string valorSerie, string tipoSerie)
    {
        string sql = this.coreSQL;
        sql += " where CodigoSerie = '" + tipoSerie + "' and ValorSerie = '" + valorSerie + "' and Estado = '0'";
        this.loadSQL(sql);
    }

    public void loadNumerosSeriesUtilizadosAduanas(string codigoSerie, string codigoAduana, string inicio, string fin)
    {
/*        string sql = "Select ValorSerie from ComplementoRecepcion as c inner join HojaRuta as r on (c.IdHojaRuta = r.IdHojaRuta)";
        sql += " where r.CodigoAduana = '" + codigoAduana + "' and c.CodigoSerie = '" + codigoSerie + "' and cast(c.ValorSerie as numeric(18,0)) between cast('" + inicio + "' as numeric(18,0)) and cast('" + fin + "' as numeric(18,0)) and c.Estado = '0' order by ValorSerie";*/
        string sql = "Select ValorSerie from";
        sql += " ComplementoRecepcion as c";
        sql += " inner join HojaRuta as r on (c.IdHojaRuta = r.IdHojaRuta)";
        sql += " where r.CodigoAduana = '" + codigoAduana + "' and c.CodigoSerie = '" + codigoSerie + "'";
        sql += " and c.ValorSerie in (";
        sql += " Select c.ValorSerie from";
        sql += " ComplementoRecepcion as c inner join HojaRuta as r on (c.IdHojaRuta = r.IdHojaRuta)";
        sql += " where r.CodigoAduana = '" + codigoAduana + "' and c.CodigoSerie = '" + codigoSerie + "'";
        sql += " and (isnumeric(c.ValorSerie)=1 and (Convert(numeric(18,0),c.ValorSerie) >= " + inicio + " and Convert(numeric(18,0),c.ValorSerie) <= " + fin + ")) and c.Estado = '0')";
        sql += " and r.Estado = '0'";
        sql += " union ";
        sql += " Select sa.ValorSerie from";
        sql += " SeriesAnuladas sa where (isnumeric(sa.ValorSerie)=1 and (Convert(numeric(18,0),sa.ValorSerie) >= " + inicio + " and Convert(numeric(18,0),sa.ValorSerie) <= " + fin + "))";
        sql += " order by ValorSerie";
        this.loadSQL(sql);
    }

    public string IDHOJARUTA
    {
        get
        {
            return (string)registro[Campos.IDHOJARUTA].ToString();
        }
        set
        {
            registro[Campos.IDHOJARUTA] = value;
        }
    }

    public string VALORSERIE
    {
        get
        {
            return (string)registro[Campos.VALORSERIE].ToString();
        }
        set
        {
            registro[Campos.VALORSERIE] = value;
        }
    }

    public string CORRELATIVO
    {
        get
        {
            return (string)registro[Campos.CORRELATIVO].ToString();
        }
        set
        {
            registro[Campos.CORRELATIVO] = value;
        }
    }

    public string CODIGOSERIE
    {
        get
        {
            return (string)registro[Campos.CODIGOSERIE].ToString();
        }
        set
        {
            registro[Campos.CODIGOSERIE] = value;
        }
    }

    public double VALORFOB
    {
        get
        {
            return double.Parse(registro[Campos.VALORFOB].ToString());
        }
        set
        {
            registro[Campos.VALORFOB] = value;
        }
    }

    public double FLETE
    {
        get
        {
            return double.Parse(registro[Campos.FLETE].ToString());
        }
        set
        {
            registro[Campos.FLETE] = value;
        }
    }

    public double SEGURO
    {
        get
        {
            return double.Parse(registro[Campos.SEGURO].ToString());
        }
        set
        {
            registro[Campos.SEGURO] = value;
        }
    }

    public double OTROS
    {
        get
        {
            return double.Parse(registro[Campos.OTROS].ToString());
        }
        set
        {
            registro[Campos.OTROS] = value;
        }
    }

    public double VALORCIF
    {
        get
        {
            return double.Parse(registro[Campos.VALORCIF].ToString());
        }
        set
        {
            registro[Campos.VALORCIF] = value;
        }
    }

    public double IMPUESTO
    {
        get
        {
            return double.Parse(registro[Campos.IMPUESTO].ToString());
        }
        set
        {
            registro[Campos.IMPUESTO] = value;
        }
    }

    public double VALORLIQUIDACION
    {
        get
        {
            return double.Parse(registro[Campos.VALORLIQUIDACION].ToString());
        }
        set
        {
            registro[Campos.VALORLIQUIDACION] = value;
        }
    }

    public double VALORPROVISIONAMIENTOEST
    {
        get
        {
            return double.Parse(registro[Campos.VALORPROVISIONAMIENTOEST].ToString());
        }
        set
        {
            registro[Campos.VALORPROVISIONAMIENTOEST] = value;
        }
    }

    public double OTROSFONDOS
    {
        get
        {
            return double.Parse(registro[Campos.OTROSFONDOS].ToString());
        }
        set
        {
            registro[Campos.OTROSFONDOS] = value;
        }
    }

    public double ANTICIPO
    {
        get
        {
            return double.Parse(registro[Campos.ANTICIPO].ToString());
        }
        set
        {
            registro[Campos.ANTICIPO] = value;
        }
    }

    public string FECHAVENCIMIENTO
    {
        get
        {
            return (string)registro[Campos.FECHAVENCIMIENTO].ToString();
        }
        set
        {
            registro[Campos.FECHAVENCIMIENTO] = value;
        }
    }

    public string CODIGOPAIS
    {
        get
        {
            return (string)registro[Campos.CODIGOPAIS].ToString();
        }
        set
        {
            registro[Campos.CODIGOPAIS] = value;
        }
    }

    public string DESTINO
    {
        get
        {
            return (string)registro[Campos.DESTINO].ToString();
        }
        set
        {
            registro[Campos.DESTINO] = value;
        }
    }

    public string VALOREMBARQUE
    {
        get
        {
            return (string)registro[Campos.VALOREMBARQUE].ToString();
        }
        set
        {
            registro[Campos.VALOREMBARQUE] = value;
        }
    }

    public string CODIGODOCEMBARQUE
    {
        get
        {
            return (string)registro[Campos.CODIGODOCEMBARQUE].ToString();
        }
        set
        {
            registro[Campos.CODIGODOCEMBARQUE] = value;
        }
    }

    public string CANTIDAD
    {
        get
        {
            return (string)registro[Campos.CANTIDAD].ToString();
        }
        set
        {
            registro[Campos.CANTIDAD] = value;
        }
    }

    public double PESO
    {
        get
        {
            return double.Parse(registro[Campos.PESO].ToString());
        }
        set
        {
            registro[Campos.PESO] = value;
        }
    }

    public string CODIGOREGIMEN
    {
        get
        {
            return (string)registro[Campos.CODIGOREGIMEN].ToString();
        }
        set
        {
            registro[Campos.CODIGOREGIMEN] = value;
        }
    }

    public string CODIGOTRANSPORTE
    {
        get
        {
            return (string)registro[Campos.CODIGOTRANSPORTE].ToString();
        }
        set
        {
            registro[Campos.CODIGOTRANSPORTE] = value;
        }
    }

    public string TIEMPOESTIMADOENTREGA
    {
        get
        {
            return (string)registro[Campos.TIEMPOESTIMADOENTREGA].ToString();
        }
        set
        {
            registro[Campos.TIEMPOESTIMADOENTREGA] = value;
        }
    }

    public string ETA
    {
        get
        {
            return (string)registro[Campos.ETA].ToString();
        }
        set
        {
            registro[Campos.ETA] = value;
        }
    }

    public double VALORMULTA
    {
        get
        {
            return double.Parse(registro[Campos.VALORMULTA].ToString());
        }
        set
        {
            registro[Campos.VALORMULTA] = value;
        }
    }

    public double VALORDEMORA
    {
        get
        {
            return double.Parse(registro[Campos.VALORDEMORA].ToString());
        }
        set
        {
            registro[Campos.VALORDEMORA] = value;
        }
    }

    public string IDRESPONSABLE
    {
        get
        {
            return (string)registro[Campos.IDRESPONSABLE].ToString();
        }
        set
        {
            registro[Campos.IDRESPONSABLE] = value;
        }
    }

    public string NOMBRERESPONSABLE
    {
        get
        {
            return (string)registro[Campos.NOMBRERESPONSABLE].ToString();
        }
        set
        {
            registro[Campos.NOMBRERESPONSABLE] = value;
        }
    }

    public string FECHAVENCIMIENTOENP
    {
        get
        {
            return (string)registro[Campos.FECHAVENCIMIENTOENP].ToString();
        }
        set
        {
            registro[Campos.FECHAVENCIMIENTOENP] = value;
        }
    }

    public string TIPOCARGA
    {
        get
        {
            return (string)registro[Campos.TIPOCARGA].ToString();
        }
        set
        {
            registro[Campos.TIPOCARGA] = value;
        }
    }

    public string ESTADO
    {
        get
        {
            return (string)registro[Campos.ESTADO].ToString();
        }
        set
        {
            registro[Campos.ESTADO] = value;
        }
    }

    public int darBajaComplementoRecepcion(string codigo)
    {
        return this.executeCustomSQL("update ComplementoRecepcion set Estado = '1' where IdHojaRuta = '" + codigo + "'");
    }

}
