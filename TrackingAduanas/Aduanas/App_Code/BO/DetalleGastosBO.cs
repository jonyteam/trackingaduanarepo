using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for EventosBO
/// </summary>
public class DetalleGastosBO : CapaBase
{

    class Campos
    {

        public static string ID = "Id";
        public static string IDINSTRUCCION = "IdInstruccion";
        public static string IDGASTO = "IdGasto";
        public static string FACTURADINANT = "FacturaDinant";
        public static string MEDIOPAGO = "MedioPago";
        public static string PROVEEDOR = "Proveedor";
        public static string MONTO = "Monto";
        public static string IVA = "IVA";
        public static string TOTAL = "Total";
        public static string TOTALVENTA = "TotalVenta";
        public static string TASACAMBIOFIJA = "TasaCambioFija";
        public static string TASACAMBIOVARIABLE = "TasaCambioVariable";
        public static string MONEDA = "Moneda";
        public static string NUMERORECIBO = "NumeroRecibo";
        public static string ESTADO = "Estado";
        public static string FECHAPAGO = "FechaPago";
        public static string FECHACREACION = "FechaCreacion";
        public static string USUARIO = "Usuario";
        public static string FECHAPAGOSISTEMA = "FechaPagoSistema";
        public static string USUARIOPAGO = "UsuarioPago";
        public static string NUMEROCARGA = "NumeroCarga";
        public static string NUMEROSAP = "NumeroSAP";
        public static string FECHANUMEROSAP = "FechaNumeroSAP";
        public static string CLIENTE = "Cliente";
        public static string NUMERORECIBODIGITALIZADO = "NumeroReciboDigitalizado";
        public static string IDDIGITALIZACION = "IdDigitalizacion";
        public static string FECHADIGITALIZACION = "FechaDigitalizacion";
        public static string USUARIODIGITALIZACION = "UsuarioDigitalizacion";
        public static string FECHAENVIOFACTURA = "FechaEnvioFactura";
        public static string USUARIOENVIOFACTURA = "UsuarioEnvioFactura";
        public static string REFERENCIAFACTURACION = "ReferenciaFacturacion";
        public static string CODESTADOFACTURACION = "CodEstadoFacturacion";
        public static string OBSERVACION = "Observacion";
        public static string REVERSARPARTIDA = "ReversarPartida";


    }

    public DetalleGastosBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from DetalleGastos DG";
        this.initializeSchema("DetalleGastos");
    }

    public void loadAutorizaciones()
    {
        string sql = this.coreSQL;
        sql += " where Estado = '0'";
        this.loadSQL(sql);
    }
    //Facturacion 01-02-2016
    public void loadRemisiones()
    {
        string sql = @"  SELECT   distinct a.ReferenciaFacturacion ,a.FechaEnvioFactura, ( u.Nombre +' '+ u.Apellido) as Nombre    from   DetalleGastos a
                         inner join Usuarios  U on UsuarioEnvioFactura=u.IdUsuario 
                         where   CodEstadoFacturacion=1
                         order by FechaEnvioFactura desc ";
     
        this.loadSQL(sql);
    }

    public void RevertirFacturacion( string referencia)
    {
        string sql = @" update DetalleGastos set CodEstadoFacturacion=0  WHERE ReferenciaFacturacion='"+referencia+"'";

        this.loadSQL(sql);
    }


    public void LoadGastosCargados(string CodPais, string FechaInicio, string FechaFin)
    {
        string sql = @"Select DG.IdInstruccion, I.NumeroFactura, C.Nombre, GT.Balance, GT.Cuenta,DG.Monto, DG.IVA , DG.Total From DetalleGastos DG
                        Inner Join Instrucciones I on (DG.IdInstruccion = I.IdInstruccion)
                        Inner Join Clientes C on (I.IdCliente = C.CodigoSAP)
                        Inner Join GatosTracking GT on (GT.IdGasto = DG.IdGasto)
                        Where DG.CodPais = '" + CodPais + "' and FechaCreacion between '" + FechaInicio + "' and '" + FechaFin + "' and DG.Estado not in(0,1,7,99)";
        this.loadSQL(sql);
    }

    #region QUERYS

    public void LlenarGatos(string Pais)
    {
        string sql = "SELECT * FROM GatosTracking Where CodPais = '" + Pais + "' and Estado = 0";
        this.loadSQL(sql);
    }

    public void BuscarHoja(string Hoja)
    {
        string sql = "SELECT * FROM DetalleGastos Where IdInstruccion = '" + Hoja + "' and Estado = 0";
        this.loadSQL(sql);
    }
    public void BuscarHojaParaAcutlizar(string Hoja, string IdUsuario, string referenciafacturacion)
    {
        string sql = "Update DetalleGastos Set CodEstadoFacturacion = 1, FechaEnvioFactura = GETDATE(),UsuarioEnvioFactura= '" + IdUsuario + "', ReferenciaFacturacion = '" + referenciafacturacion + "' Where IdInstruccion = '" + Hoja + "' and Estado != 99";
        this.loadSQL(sql);
    }

    public void BuscarReferencia(string Referencia)
    {
        string sql = "Select * from DetalleGastos  Where  IdDigitalizacion='" + Referencia + "'";
        this.loadSQL(sql);
    }
    public void LlenarGatosXHojaYGasto(string IdInstruccion, string IdGasto)
    {
        string sql = "SELECT * FROM DetalleGastos Where IdInstruccion = '" + IdInstruccion + "' and IdGasto = '" + IdGasto + "' and Estado = 0";
        this.loadSQL(sql);
    }

    public void LlenarGatosXHojaYGastoNueva(string IdInstruccion, string IdGasto)
    {
        string sql = "SELECT * FROM DetalleGastos Where IdInstruccion = '" + IdInstruccion + "' and IdGasto = '" + IdGasto+ "'" ;
        this.loadSQL(sql);
    }
    public void ComprobarGatosXHojaYGasto(string IdInstruccion, string IdGasto)
    {
        string sql = "SELECT * FROM DetalleGastos Where IdInstruccion = '" + IdInstruccion + "' and IdGasto = '" + IdGasto + "'";
        this.loadSQL(sql);
    }
    public void LlenarGatosXHojaYGastoTesoreria(string IdInstruccion, string IdGasto)
    {
        string sql = "SELECT * FROM DetalleGastos Where IdInstruccion = '" + IdInstruccion + "' and Id = '" + IdGasto + "' and Estado = 3";
        this.loadSQL(sql);
    }

    public void LlenarGatosXId(string Id)
    {
        string sql = "SELECT * FROM DetalleGastos Where Id = '" + Id + "' and Estado = 1";
        this.loadSQL(sql);
    }
    public void DescargaSharePoint(string Id)
    {
        string sql = "SELECT IdInstruccion,NumeroReciboDigitalizado,IdDigitalizacion";
        sql += ",Case MONTH(FechaDigitalizacion) when '1' Then 'ENERO'+CONVERT(VARCHAR,YEAR(FechaDigitalizacion)) When '2' Then 'FEBRERO'+CONVERT(VARCHAR,YEAR(FechaDigitalizacion))";
        sql += " when '3' Then 'MARZO'+CONVERT(VARCHAR,YEAR(FechaDigitalizacion)) When '4' Then 'ABRIL'+CONVERT(VARCHAR,YEAR(FechaDigitalizacion))";
        sql += " when '5' Then 'MAYO'+CONVERT(VARCHAR,YEAR(FechaDigitalizacion)) When '6' Then 'JUNIO'+CONVERT(VARCHAR,YEAR(FechaDigitalizacion))";
        sql += " when '7' Then 'JULIO'+CONVERT(VARCHAR,YEAR(FechaDigitalizacion)) When '8' Then 'AGOSTO'+CONVERT(VARCHAR,YEAR(FechaDigitalizacion))";
        sql += " when '9' Then 'SEPTIEMBRE'+CONVERT(VARCHAR,YEAR(FechaDigitalizacion)) When '10' Then 'OCTUBRE'+CONVERT(VARCHAR,YEAR(FechaDigitalizacion))";
        sql += " when '11' Then 'NOVIEMBRE'+CONVERT(VARCHAR,YEAR(FechaDigitalizacion)) Else 'DICIEMBRE'+CONVERT(VARCHAR,YEAR(FechaDigitalizacion))";
        sql += " END AS FechaDigitalizacion ,UsuarioDigitalizacion FROM DetalleGastos Where Id = '" + Id + "' and Estado = 3";
        this.loadSQL(sql);
    }
    public void loadAutorizacionesEstado()
    {
        this.loadSQL(this.coreSQL);
    }

    public void mostrardatos(string hojaruta)
    {
        string sql = "  Select C.Descripcion ,FechaInicio,FechaFin,ObservacionFin,ObservacionInicio,CL.Nombre,I.NumeroFactura,I.ReferenciaCliente from Codigos C";
        sql += " Inner Join Eventos E on (IdInstruccion ='" + hojaruta + "' and C.Codigo = IdEvento and C.Categoria = 'EVENTOS' and E.Eliminado = 1)";
        sql += " Inner Join Instrucciones I on (E.IdInstruccion = I.IdInstruccion and I.CodEstado != 100)";
        sql += " Inner Join Clientes CL on (I.IdCliente = CL.CodigoSAP)";
        this.loadSQL(sql);
    }

    public void mostrardatos2(string hojaruta)
    {
        string sql = "SELECT ISNULL(FC.EstadoFlujo, 'Comienzo de Flujo/Doc Minimos') as GestionCarga, GC.Fecha as FechaGestor, I.Color,GC.Observacion as Observacion from GestionCarga GC";
        sql += " left join FlujoCarga FC on (GC.IdEstadoFlujo = FC.IdEstadoFlujo) inner join Instrucciones I on ('" + hojaruta + "' = I.IdInstruccion) where GC.IdInstruccion = '" + hojaruta + "' order by FC.Orden";
        this.loadSQL(sql);
    }

    public void loadAutorizacionesRoles(string idRol)
    {
        string sql = this.coreSQL;
        sql += " where IdRol = " + idRol + " and Estado = '0'";
        this.loadSQL(sql);
    }


    public void loadAutorizacionesModulo(string idModulo)
    {
        string sql = this.coreSQL;
        sql += " where IdModulo = " + idModulo + " and Estado = '0'";
        this.loadSQL(sql);
    }

    public void loadAutorizacionesModulosRoles(string idRol, string idModulo)
    {
        string sql = this.coreSQL;
        sql += " where IdRol = " + idRol + " and IdModulo = " + idModulo + " and Estado = '0'";
        this.loadSQL(sql);
    }

    public void loadAutorizacionesModulosRolesEstado(string idRol, string idModulo)
    {
        string sql = this.coreSQL;
        sql += " where IdRol = " + idRol + " and IdModulo = " + idModulo;
        this.loadSQL(sql);
    }

    public void deleAutorizacionesModulos(string idRol, string idModulo)
    {
        string sql = this.coreSQL;
        sql += " where IdRole = " + idRol + " and IdModulo = " + idModulo;
        this.executeCustomSQL(String.Format("delete from AutorizacionesModulo where IdRol={0} and IdModulo={1}", idRol, idModulo));
    }
    public void loadInstruccionesSolicitudPagoCXP()
    {
        string sql = " Select A.IdInstruccion as HojaRuta, G.Nombre as Clientes , A.Id, C.Gasto as Material ,C.Cuenta as CuentaSAP";
        sql += " ,E.Nombre as Proveedor,A.Monto as Monto,A.IVA as IVA,A.Total as Total ";
        sql += " ,F.Descripcion as Moneda, D.Descripcion as MedioPago, A.IdGasto as IdGasto, I.Serie as Serie,A.Proveedor as CodigoProveedor From DetalleGastos A";
        sql += " LEFT JOIN Instrucciones B on (A.IdInstruccion = B.IdInstruccion and B.CodEstado !=100 and B.CodEstado != 100) ";
        sql += " LEFT JOIN Clientes G on (B.IdCliente = G.CodigoSAP and G.Eliminado = 0) ";
        sql += " INNER JOIN GatosTracking C on (A.IdGasto = C.IdGasto and C.CodPais = A.CodPais and C.Impuesto != 1 )";
        sql += " INNER JOIN Codigos D on (D.Categoria = 'MEDIOPAGO' and D.Codigo = A.MedioPago)";
        sql += " INNER JOIN ProveedoresSolicitud E on (E.Cuenta= A.Proveedor)";
        sql += " INNER JOIN Codigos F on (F.Categoria like '%MONEDA%' and F.Codigo = A.Moneda)";
        sql += " LEFT JOIN Aduanas H on ( B.CodigoAduana = H.CodigoAduana and H.CodEstado = 0) ";
        sql += " LEFT JOIN EspeciesFiscalesInstrucciones I on ( A.IdInstruccion = I.IdInstruccion and I.CodEspecieFiscal = 'P' and I.Cliente ='X' and I.Eliminado = 0)";
        sql += " Where A.Estado = 1 and C.Estado = 0 and A.MedioPago in ('TR','CH','ND','EF')  and A.Monto > 0 order by A.MedioPago";
        this.loadSQL(sql);
    }
    public void loadInstruccionesSolicitudPagoCXPXPAIS(string pais)
    {
        string sql = " Select DISTINCT A.IdInstruccion as HojaRuta, G.Nombre as Clientes , A.Id, C.Gasto as Material ,C.Balance as CuentaSAP,A.Proveedor as CodigoProveedor";
        sql += " ,E.Nombre as Proveedor,A.Monto as Monto,A.IVA as IVA,A.Total as Total ";
        sql += " ,F.Descripcion as Moneda, D.Descripcion as MedioPago, A.IdGasto as IdGasto,I.Serie as Serie From DetalleGastos A";
        sql += " LEFT JOIN Instrucciones B on (A.IdInstruccion = B.IdInstruccion and B.CodEstado !=100 ) ";
        sql += " LEFT JOIN Clientes G on (B.IdCliente = G.CodigoSAP and G.Eliminado = 0) ";
        sql += " INNER JOIN GatosTracking C on (A.IdGasto = C.IdGasto and C.CodPais = A.CodPais and C.Impuesto != 1 )";
        sql += " INNER JOIN Codigos D on (D.Categoria = 'MEDIOPAGO' and D.Codigo = A.MedioPago)";
        sql += " INNER JOIN ProveedoresSolicitud E on (E.Cuenta= A.Proveedor)";
        sql += " INNER JOIN Codigos F on (F.Categoria like '%MONEDA%' and F.Codigo = A.Moneda)";
        sql += " LEFT JOIN Aduanas H on ( B.CodigoAduana = H.CodigoAduana and H.CodEstado = 0) ";
        sql += " LEFT JOIN EspeciesFiscalesInstrucciones I on ( A.IdInstruccion = I.IdInstruccion and I.CodEspecieFiscal = 'P' and I.Cliente ='X' and I.Eliminado = 0)";
        sql += " Where A.Estado = 1 and C.Estado = 0 and A.MedioPago in ('TR','CH','ND','EF') and A.CodPais = '" + pais + "' and A.Monto > 0 ";
        sql += " or ReversarPartida = 1 and C.Estado = 0 and A.MedioPago in ('TR','CH','ND','EF') and A.CodPais = '" + pais + "' and A.Monto > 0";
        this.loadSQL(sql);
    }
    public void loadInstruccionesSolicitudConfirmar()
    {
        string sql = " Select Distinct A.IdInstruccion as HojaRuta, G.Nombre as Clientes , A.Id, C.Gasto as Material ,C.Cuenta as CuentaSAP";
        sql += " ,E.Nombre as Proveedor,A.Monto as Monto,A.IVA as IVA,A.TotalVenta as Total ";
        sql += " ,F.Descripcion as Moneda, D.Descripcion as MedioPago, A.IdGasto as IdGasto, A.MedioPago From DetalleGastos A";
        sql += " INNER JOIN GatosTracking C on (A.IdGasto = C.IdGasto and C.CodPais = A.CodPais )";
        sql += " INNER JOIN Codigos D on (D.Categoria = 'MEDIOPAGO' and D.Codigo = A.MedioPago)";
        sql += " INNER JOIN ProveedoresSolicitud E on (E.Cuenta= A.Proveedor) ";
        sql += " INNER JOIN Codigos F on (F.Categoria like '%MONEDA%' and F.Codigo = A.Moneda)";
        sql += " LEFT JOIN Instrucciones B on (A.IdInstruccion = B.IdInstruccion and B.CodEstado != 100)";
        sql += " LEFT JOIN Clientes G on (B.IdCliente = G.CodigoSAP and G.Eliminado = 0)";
        sql += " Where  A.Estado = 0 and C.Estado = 0 and A.CodPais !='H' order by A.MedioPago";
        this.loadSQL(sql);
    }

    public void loadIdGasto(string IdGasto)
    {
        string sql = " Select * from DetalleGastos  ";
        sql += " Where Id in(" + IdGasto + ")";
        this.loadSQL(sql);
    }
    public void loadInstruccionesSolicitudConfirmarXPais(string pais)
    {
        string sql = " Select Distinct A.IdInstruccion as HojaRuta, G.Nombre as Clientes , A.Id, C.Gasto as Material ,C.Cuenta as CuentaSAP,A.MedioPago as Codigo";
        sql += " ,E.Nombre as Proveedor,A.Monto as Monto,A.IVA as IVA,A.TotalVenta as Total,A.Proveedor as ProveedorCodigo ";
        sql += " ,F.Descripcion as Moneda, D.Descripcion as MedioPago, A.IdGasto as IdGasto From DetalleGastos A";
        sql += " INNER JOIN GatosTracking C on (A.IdGasto = C.IdGasto and C.CodPais = A.CodPais )";
        sql += " INNER JOIN Codigos D on (D.Categoria = 'MEDIOPAGO' and D.Codigo = A.MedioPago)";
        sql += " INNER JOIN ProveedoresSolicitud E on (E.Cuenta= A.Proveedor)";
        sql += " INNER JOIN Codigos F on (F.Categoria like '%MONEDA%' and F.Codigo = A.Moneda)";
        sql += " LEFT JOIN Instrucciones B on (A.IdInstruccion = B.IdInstruccion and B.CodEstado != 100)";
        sql += " LEFT JOIN Clientes G on (B.IdCliente = G.CodigoSAP and G.Eliminado = 0)";
        sql += " Where A.Estado = 0 and C.Estado = 0 and A.CodPais !='H' and A.CodPais = '" + pais + "'order by A.MedioPago";
        this.loadSQL(sql);
    }

    public void loadInstruccionesSolicitudConfirmarXPaisRecuperar(string pais)
    {
        string sql = " Select Distinct A.IdInstruccion as HojaRuta, G.Nombre as Clientes , A.Id, C.Gasto as Material ,C.Cuenta as CuentaSAP,A.MedioPago as Codigo";
        sql += " ,E.Nombre as Proveedor,A.Monto as Monto,A.IVA as IVA,A.TotalVenta as Total,A.Proveedor as ProveedorCodigo ";
        sql += " ,F.Descripcion as Moneda, D.Descripcion as MedioPago, A.IdGasto as IdGasto From DetalleGastos A";
        sql += " INNER JOIN GatosTracking C on (A.IdGasto = C.IdGasto and C.CodPais = A.CodPais )";
        sql += " INNER JOIN Codigos D on (D.Categoria = 'MEDIOPAGO' and D.Codigo = A.MedioPago)";
        sql += " INNER JOIN ProveedoresSolicitud E on (E.Cuenta= A.Proveedor)";
        sql += " INNER JOIN Codigos F on (F.Categoria like '%MONEDA%' and F.Codigo = A.Moneda)";
        sql += " LEFT JOIN Instrucciones B on (A.IdInstruccion = B.IdInstruccion and B.CodEstado != 100)";
        sql += " LEFT JOIN Clientes G on (B.IdCliente = G.CodigoSAP and G.Eliminado = 0)";
        sql += " Where A.Estado = 0 and C.Estado = 0 and A.CodPais !='H' and A.CodPais = '" + pais + "'order by A.MedioPago,C.Gasto,A.FechaCreacion";
        this.loadSQL(sql);
    }
    public void loadInstruccionesSolicitudSubirArchivo()
    {
        string sql = @" Select A.IdInstruccion as HojaRuta,B.IdTramite,G.Nombre as Clientes , A.Id, C.Gasto as Material ,C.Cuenta as CuentaSAP 
                         ,E.Nombre as Proveedor,A.Monto as Monto,A.IVA as IVA,A.Total as Total,H.NombreAduana as Aduana 
                         ,F.Descripcion as Moneda, D.Descripcion as MedioPago, A.IdGasto as IdGasto From DetalleGastos A
                         LEFT JOIN Instrucciones B on (A.IdInstruccion = B.IdInstruccion and B.CodEstado != 100)
                         LEFT JOIN Clientes G on (B.IdCliente = G.CodigoSAP and G.Eliminado = 0)
                         INNER JOIN GatosTracking C on (A.IdGasto = C.IdGasto and C.CodPais = A.CodPais )
                         INNER JOIN Codigos D on (D.Categoria = 'MEDIOPAGO' and D.Codigo = A.MedioPago)
                         INNER JOIN ProveedoresSolicitud E on (E.Cuenta= A.Proveedor)
                         INNER JOIN Codigos F on (F.Categoria like '%MONEDA%' and F.Codigo = A.Moneda)
                         LEFT JOIN Aduanas H on (B.CodigoAduana = H.CodigoAduana and H.CodEstado = 0)
                         Where A.Estado = 2 and C.Estado = 0  and A.Monto > 0  and D.Descripcion  not in  ('Caja Chica') order by E.Nombre,A.MedioPago,C.Gasto,A.FechaCreacion";
        this.loadSQL(sql);
    }
    public void loadInstruccionesSolicitudSubirArchivo_Fileshare()
    {     
        string sql = " Select distinct A.IdInstruccion as HojaRuta,";
                 sql += " G.Nombre as Clientes ,";
                 sql += " H.NombreAduana as Aduana";
                 sql += " From DetalleGastos A LEFT JOIN Instrucciones B on (A.IdInstruccion = B.IdInstruccion and B.CodEstado != 100) ";
                 sql += "LEFT JOIN Clientes G on (B.IdCliente = G.CodigoSAP and G.Eliminado = 0)";
                 sql += "LEFT JOIN Aduanas H on (B.CodigoAduana = H.CodigoAduana and H.CodEstado = 0) ";
                 sql += "Where A.Estado = 2";
                 sql += "and A.Monto > 0 ";
        this.loadSQL(sql);
    }
    public void loadInstruccionesSolicitudSubirArchivoXPais(string pais)
    {
        string sql = " Select A.IdInstruccion as HojaRuta, G.Nombre as Clientes , A.Id, C.Gasto as Material ,C.Cuenta as CuentaSAP";
        sql += " ,E.Nombre as Proveedor,A.Monto as Monto,A.IVA as IVA,A.Total as Total,H.NombreAduana as Aduana ";
        sql += " ,F.Descripcion as Moneda, D.Descripcion as MedioPago, A.IdGasto as IdGasto From DetalleGastos A";
        sql += " LEFT JOIN Instrucciones B on (A.IdInstruccion = B.IdInstruccion and B.CodEstado != 100)";
        sql += " LEFT JOIN Clientes G on (B.IdCliente = G.CodigoSAP and G.Eliminado = 0)";
        sql += " INNER JOIN GatosTracking C on (A.IdGasto = C.IdGasto and C.CodPais = A.CodPais )";
        sql += " INNER JOIN Codigos D on (D.Categoria = 'MEDIOPAGO' and D.Codigo = A.MedioPago)";
        sql += " INNER JOIN ProveedoresSolicitud E on (E.Cuenta= A.Proveedor)";
        sql += " INNER JOIN Codigos F on (F.Categoria like '%MONEDA%' and F.Codigo = A.Moneda)";
        sql += " LEFT JOIN Aduanas H on (B.CodigoAduana = H.CodigoAduana and H.CodEstado = 0)";
        sql += " Where A.Estado = 2 and C.Estado = 0 and A.CodPais !='H' and A.CodPais = '" + pais + "' and A.Monto > 0 order by E.Nombre,A.MedioPago,C.Gasto,A.FechaCreacion";
        this.loadSQL(sql);
    }
    public void loadInstruccionesSolicitudPagoIMP()
    {
        string sql = " Select A.IdInstruccion as HojaRuta, G.Nombre as Clientes , A.Id, C.Gasto as Material ,C.Cuenta as CuentaSAP,  A.NumeroReciboDigitalizado, A.IdDigitalizacion";
        sql += " ,E.Nombre as Proveedor,A.Monto as Monto,A.IVA as IVA,A.Total as Total, H.NombreAduana as Aduana ";
        sql += " ,F.Descripcion as Moneda, D.Descripcion as MedioPago, A.FechaPago as FechaPago, A.Observacion as Observacion From DetalleGastos A";
        sql += " INNER JOIN Instrucciones B on (A.IdInstruccion = B.IdInstruccion)";
        sql += " INNER JOIN Clientes G on (B.IdCliente = G.CodigoSAP and G.Eliminado = 0)";
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        sql += " INNER JOIN GatosTracking C on (A.IdGasto = C.IdGasto and C.CodPais = A.CodPais )";////////mirar si van los impuestos
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        sql += " INNER JOIN Codigos D on (D.Categoria = 'MEDIOPAGO' and D.Codigo = A.MedioPago)";
        sql += " INNER JOIN ProveedoresSolicitud E on (E.Cuenta= A.Proveedor)";
        sql += " INNER JOIN Codigos F on (F.Categoria like '%MONEDA%' and F.Codigo = A.Moneda)";
        sql += " INNER JOIN Aduanas H on (B.CodigoAduana = H.CodigoAduana and H.CodEstado = 0)";
        sql += " Where B.CodEstado != 100 and A.Estado = 3 and C.Estado = 0 and A.MedioPago in ('TR','CH','EF','ND') and A.Monto > 0 order by A.FechaPago";
        this.loadSQL(sql);
    }


    public void loadInstruccionesSolicitudPagoIMPAgrupado()
    {
        string sql = @"Select  distinct    A.IdDigitalizacion 
          ,E.Nombre as Proveedor , sum (A.Total) as Total  , A.FechaPagoTesoreria
           From DetalleGastos A 
          INNER JOIN Instrucciones B on (A.IdInstruccion = B.IdInstruccion) 
          INNER JOIN Clientes G on (B.IdCliente = G.CodigoSAP and G.Eliminado = 0)      
          INNER JOIN GatosTracking C on (A.IdGasto = C.IdGasto and C.CodPais = A.CodPais )
          INNER JOIN Codigos D on (D.Categoria = 'MEDIOPAGO' and D.Codigo = A.MedioPago) 
          INNER JOIN ProveedoresSolicitud E on (E.Cuenta= A.Proveedor) 
          INNER JOIN Codigos F on (F.Categoria like '%MONEDA%' and F.Codigo = A.Moneda) 
          INNER JOIN Aduanas H on (B.CodigoAduana = H.CodigoAduana and H.CodEstado = 0) 
		  Where B.CodEstado != 100 and A.Estado = 3 and C.Estado = 0 and A.MedioPago in ('TR','CH','EF','ND') and A.Monto > 0 
		  and  A.IdDigitalizacion is not null  group by  A.IdDigitalizacion,E.Nombre, A.FechaPagoTesoreria";
        this.loadSQL(sql);
    }
    //Arle 05/02/2016
    public void loadInstruccionesSolicitudPagoIMPREFERENCIA(string REF)
    {
        string sql = " Select A.IdInstruccion as HojaRuta, G.Nombre as Clientes , A.Id, C.Gasto as Material ,C.Cuenta as CuentaSAP,  A.NumeroReciboDigitalizado, A.IdDigitalizacion ,A.IdGasto,A.IdInstruccion";
        sql += " ,E.Nombre as Proveedor,A.Monto as Monto,A.IVA as IVA,A.Total as Total, H.NombreAduana as Aduana ";
        sql += " ,F.Descripcion as Moneda, D.Descripcion as MedioPago, A.FechaPago as FechaPago, A.Observacion as Observacion From DetalleGastos A";
        sql += " INNER JOIN Instrucciones B on (A.IdInstruccion = B.IdInstruccion)";
        sql += " INNER JOIN Clientes G on (B.IdCliente = G.CodigoSAP and G.Eliminado = 0)";
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        sql += " INNER JOIN GatosTracking C on (A.IdGasto = C.IdGasto and C.CodPais = A.CodPais )";////////mirar si van los impuestos
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        sql += " INNER JOIN Codigos D on (D.Categoria = 'MEDIOPAGO' and D.Codigo = A.MedioPago)";
        sql += " INNER JOIN ProveedoresSolicitud E on (E.Cuenta= A.Proveedor)";
        sql += " INNER JOIN Codigos F on (F.Categoria like '%MONEDA%' and F.Codigo = A.Moneda)";
        sql += " INNER JOIN Aduanas H on (B.CodigoAduana = H.CodigoAduana and H.CodEstado = 0)";
        sql += " Where B.CodEstado != 100 and A.Estado = 3 and C.Estado = 0 and A.MedioPago in ('TR','CH','EF','ND') and A.Monto > 0  AND  A.IdDigitalizacion='" + REF + "' order by A.FechaPago";
        this.loadSQL(sql);
    }
    public void loadHojaxFactura(string Factura, string IdPais)
    {
        string sql = " Select IdInstruccion,Id From DetalleGastos";
        sql += " Where IdInstruccion = '" + IdPais + "-" + Factura + "' and FacturaDinant = '" + Factura + "' and CodPais = '" + IdPais + "'";
        this.loadSQL(sql);
    }

    public void ActualizaRSapXReferencia(string Referencia , string NSAP)
    {
        string sql = " Update DetalleGastos Set FechaNumeroSAP=getdate(),Estado=10, NumeroSAP='" + NSAP + "'";     
        sql += " Where  IdDigitalizacion='" + Referencia + "'";
        this.loadSQL(sql);
    }
    public void ActualizarHojaxFactura(string Factura, string IdPais, string Hoja)
    {
        string sql = " Update DetalleGastos Set IdInstruccion = '" + Hoja + "'";
        sql += " Where IdInstruccion = '" + IdPais + "-" + Factura + "' and FacturaDinant = '" + Factura + "' and CodPais = '" + IdPais + "'";
        this.loadSQL(sql);
    }
    public void ComprobarHoja(string Factura, string IdPais, string Hoja)
    {
        string sql = "Select TOP 1 IdInstruccion,Id From DetalleGastos";
        sql += " Where IdInstruccion != '" + IdPais + "-" + Factura + "' and FacturaDinant = '" + Factura + "' and CodPais = '" + IdPais + "'";
        this.loadSQL(sql);
    }
    public void loadInstruccionesSolicitudPagoIMPXPais(string pais)
    {
        string sql = @" Select A.IdInstruccion as HojaRuta, G.Nombre as Clientes , A.Id, C.Gasto as Material ,C.Cuenta as CuentaSAP ,A.NumeroReciboDigitalizado, A.IdDigitalizacion 
         ,E.Nombre as Proveedor,A.Monto as Monto,A.IVA as IVA,A.Total as Total, H.NombreAduana as Aduana  
         ,F.Descripcion as Moneda, D.Descripcion as MedioPago, A.FechaPago as FechaPago, A.Observacion as Observacion From DetalleGastos A 
         INNER JOIN Instrucciones B on (A.IdInstruccion = B.IdInstruccion) 
         INNER JOIN Clientes G on (B.IdCliente = G.CodigoSAP and G.Eliminado = 0) 
       
         INNER JOIN GatosTracking C on (A.IdGasto = C.IdGasto and C.CodPais = A.CodPais ) 
       
         INNER JOIN Codigos D on (D.Categoria = 'MEDIOPAGO' and D.Codigo = A.MedioPago) 
         INNER JOIN ProveedoresSolicitud E on (E.Cuenta= A.Proveedor) 
         INNER JOIN Codigos F on (F.Categoria like '%MONEDA%' and F.Codigo = A.Moneda) 
         INNER JOIN Aduanas H on (B.CodigoAduana = H.CodigoAduana and H.CodEstado = 0) 
          Where B.CodEstado != 100 and A.Estado = 3 and C.Estado = 0 and A.MedioPago in ('TR','CH','EF','ND') and A.Monto > 0 and ";
        sql += "B.CodPaisHojaRuta = '" + pais + "' order by A.FechaPago";
        this.loadSQL(sql);
    }
    public void loadInstruccionesSolicitudPagoIMPXPaisAgrupado(string pais)
    {
        string sql = @" Select distinct    A.IdDigitalizacion 
          ,E.Nombre as Proveedor , sum (A.Total) as Total 
          , D.Descripcion as MedioPago, A.FechaPagoTesoreria From DetalleGastos A
         INNER JOIN Instrucciones B on (A.IdInstruccion = B.IdInstruccion) 
         INNER JOIN Clientes G on (B.IdCliente = G.CodigoSAP and G.Eliminado = 0) 
       
         INNER JOIN GatosTracking C on (A.IdGasto = C.IdGasto and C.CodPais = A.CodPais ) 
       
         INNER JOIN Codigos D on (D.Categoria = 'MEDIOPAGO' and D.Codigo = A.MedioPago) 
         INNER JOIN ProveedoresSolicitud E on (E.Cuenta= A.Proveedor) 
         INNER JOIN Codigos F on (F.Categoria like '%MONEDA%' and F.Codigo = A.Moneda) 
         INNER JOIN Aduanas H on (B.CodigoAduana = H.CodigoAduana and H.CodEstado = 0) 
        Where B.CodEstado != 100 and A.Estado = 3 and C.Estado = 0 and A.MedioPago in ('TR','CH','EF','ND') and A.Monto > 0 and";
        sql += " B.CodPaisHojaRuta ='" + pais+"' ";
        sql += " group by  A.IdDigitalizacion,E.Nombre, D.Descripcion, A.FechaPagoTesoreria";
        this.loadSQL(sql);
    }

    public void loadInstruccionesSolicitudPagoIMPXPaisREFERENCIA(string pais , string REF)
    {
        string sql = @" Select A.IdInstruccion as HojaRuta, G.Nombre as Clientes , A.Id, C.Gasto as Material ,C.Cuenta as CuentaSAP ,A.NumeroReciboDigitalizado, A.IdDigitalizacion 
         ,E.Nombre as Proveedor,A.Monto as Monto,A.IVA as IVA,A.Total as Total, H.NombreAduana as Aduana  
         ,F.Descripcion as Moneda, D.Descripcion as MedioPago, A.FechaPago as FechaPago, A.Observacion as Observacion From DetalleGastos A 
         INNER JOIN Instrucciones B on (A.IdInstruccion = B.IdInstruccion) 
         INNER JOIN Clientes G on (B.IdCliente = G.CodigoSAP and G.Eliminado = 0) 
       
         INNER JOIN GatosTracking C on (A.IdGasto = C.IdGasto and C.CodPais = A.CodPais ) 
       
         INNER JOIN Codigos D on (D.Categoria = 'MEDIOPAGO' and D.Codigo = A.MedioPago) 
         INNER JOIN ProveedoresSolicitud E on (E.Cuenta= A.Proveedor) 
         INNER JOIN Codigos F on (F.Categoria like '%MONEDA%' and F.Codigo = A.Moneda) 
         INNER JOIN Aduanas H on (B.CodigoAduana = H.CodigoAduana and H.CodEstado = 0) 
          Where B.CodEstado != 100 and A.Estado = 3 and C.Estado = 0 and A.MedioPago in ('TR','CH','EF','ND') and A.Monto > 0 and ";
        sql += "B.CodPaisHojaRuta = '" + pais + "' and  A.IdDigitalizacion='"+REF+"' order by A.FechaPago";
        this.loadSQL(sql);
    }

    //public void loadInstruccionesSolicitudPagoIMPXPais(string pais)
    //{
    //    string sql = " Select A.IdInstruccion as HojaRuta, G.Nombre as Clientes , A.Id, C.Gasto as Material ,C.Cuenta as CuentaSAP ,A.NumeroReciboDigitalizado, A.IdDigitalizacion";
    //    sql += " ,E.Nombre as Proveedor,A.Monto as Monto,A.IVA as IVA,A.Total as Total, H.NombreAduana as Aduana ";
    //    sql += " ,F.Descripcion as Moneda, D.Descripcion as MedioPago, A.FechaPago as FechaPago, A.Observacion as Observacion From DetalleGastos A";
    //    sql += " INNER JOIN Instrucciones B on (A.IdInstruccion = B.IdInstruccion)";
    //    sql += " INNER JOIN Clientes G on (B.IdCliente = G.CodigoSAP and G.Eliminado = 0)";
    //    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //    sql += " INNER JOIN GatosTracking C on (A.IdGasto = C.IdGasto and C.CodPais = A.CodPais )";////////mirar si van los impuestos
    //    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //    sql += " INNER JOIN Codigos D on (D.Categoria = 'MEDIOPAGO' and D.Codigo = A.MedioPago)";
    //    sql += " INNER JOIN ProveedoresSolicitud E on (E.Cuenta= A.Proveedor)";
    //    sql += " INNER JOIN Codigos F on (F.Categoria like '%MONEDA%' and F.Codigo = A.Moneda)";
    //    sql += " INNER JOIN Aduanas H on (B.CodigoAduana = H.CodigoAduana and H.CodEstado = 0)";
    //    sql += " Where B.CodEstado != 100 and A.Estado = 3 and C.Estado = 0 and A.MedioPago in ('TR','CH','EF','ND') and A.Monto > 0 and B.CodPaisHojaRuta = '" + pais + "' order by A.FechaPago";
    //    this.loadSQL(sql);
    //}
    public void CargarGastosExellDeUnaHoja(string IdInstruccion)
    {
        string sql = @" SELECT C.CodigoSAP+'-'+C.Nombre as Cliente ,A.IdInstruccion,B.Producto as Producto,B.NumeroFactura as Factura,B.ValorCIF as CIF,D.Cuenta
         ,D.Gasto,ValorVentaDolares, F.Codigo +'-'+F.Descripcion as Regimen, G.NombreAduana as Aduana, B.NoCorrelativo as Correlativo,
         B.Proveedor as Proveedor, H.Descripcion as PaisOrigen, I.Descripcion as Transporte, ISNULL(J.Serie,K.Serie) as 'Fauca/DUA', A.IdGasto, C.CodigoPaisCliente
         ,A.ValorCostoDolares as Costo FROM DetalleGastos A 
         Inner Join Instrucciones B on (A.IdInstruccion = B.IdInstruccion and B.CodEstado != 100 and A.Estado not in (0,99))
         Inner Join Clientes C on (B.IdCliente = C.CodigoSAP and C.Eliminado = 0)
         Inner Join GatosTracking D on (A.IdGasto = D.IdGasto and D.Estado = 0 )
         Inner Join Codigos E on ( E.Categoria = 'Paises' and A.CodPais = E.Codigo and E.Eliminado = 0)
         Inner Join Codigos F on ( F.Categoria = 'REGIMEN'+E.Descripcion and B.CodRegimen = F.Codigo and F.Eliminado = 0)
         Inner Join Aduanas G on ( B.CodigoAduana = G.CodigoAduana and G.CodEstado = 0)
         Inner Join Codigos H on ( H.Categoria = 'Paises' and B.CodPaisOrigen = H.Codigo and H.Eliminado = 0)
         Inner Join Codigos I on ( I.Categoria = 'TIPOTRANSPORTE' and B.CodTipoTransporte = I.Codigo and I.Eliminado = 0)
         Left Join EspeciesFiscalesInstrucciones J on (A.IdInstruccion = J.IdInstruccion and J.Cliente = 'X' and J.CodEspecieFiscal = 'F' and J.Eliminado = 0)
         Left Join EspeciesFiscalesInstrucciones K on (A.IdInstruccion = K.IdInstruccion and K.Cliente = 'X' and K.CodEspecieFiscal = 'P' and K.Eliminado = 0)
         Where A.IdInstruccion = '" + IdInstruccion + "'";
        this.loadSQL(sql);
    }
    public void RevisarGastosXPias(string pais)
    {
        string sql = "Select A.IdInstruccion as HojaRuta, G.Nombre as Clientes, H.NombreAduana as Aduana From DetalleGastos A";
        sql += " INNER JOIN Instrucciones B on (A.IdInstruccion = B.IdInstruccion)";
        sql += " INNER JOIN Clientes G on (B.IdCliente = G.CodigoSAP and G.Eliminado = 0)";
        sql += " INNER JOIN Aduanas H on (B.CodigoAduana = H.CodigoAduana and H.CodEstado = 0)";
        sql += " Where B.CodEstado != 100 and B.CodPaisHojaRuta !='H' and B.CodPaisHojaRuta = '" + pais + "'";
        sql += " and ISNULL(B.FechaFinalFlujo,GETDATE()) >= (DATEADD(DAY,-60,GETDATE()))";
        sql += " Group by A.IdInstruccion, G.Nombre, H.NombreAduana ";
        this.loadSQL(sql);
    }
    public void RevisarGastos()
    {
        string sql = "Select A.IdInstruccion as HojaRuta, G.Nombre as Clientes, H.NombreAduana as Aduana From DetalleGastos A";
        sql += " INNER JOIN Instrucciones B on (A.IdInstruccion = B.IdInstruccion)";
        sql += " INNER JOIN Clientes G on (B.IdCliente = G.CodigoSAP and G.Eliminado = 0)";
        sql += " INNER JOIN Aduanas H on (B.CodigoAduana = H.CodigoAduana and H.CodEstado = 0)";
        sql += " Where B.CodEstado != 100 and B.CodPaisHojaRuta !='H' ";
        sql += " and ISNULL(B.FechaFinalFlujo,GETDATE()) >= (DATEADD(DAY,-60,GETDATE()))";
        sql += " Group by A.IdInstruccion, G.Nombre, H.NombreAduana ";
        this.loadSQL(sql);
    }
    public void RevisarGastos(string IdHojaRuta)
    {
        string sql = "Select A.IdInstruccion as HojaRuta, G.Nombre as Clientes, H.NombreAduana as Aduana From DetalleGastos A";
        sql += " INNER JOIN Instrucciones B on (A.IdInstruccion = B.IdInstruccion)";
        sql += " INNER JOIN Clientes G on (B.IdCliente = G.CodigoSAP and G.Eliminado = 0)";
        sql += " INNER JOIN Aduanas H on (B.CodigoAduana = H.CodigoAduana and H.CodEstado = 0)";
        sql += " Where B.CodEstado != 100 and B.CodPaisHojaRuta !='H' ";
        sql += " and A.IdInstruccion = '" + IdHojaRuta + "'";
        sql += " Group by A.IdInstruccion, G.Nombre, H.NombreAduana ";
        this.loadSQL(sql);
    }
    public void CargarGastosExellXHojas(string IdInstrucciones)
    {
        string sql = " SELECT C.CodigoSAP+'-'+C.Nombre as Clientes ,(Select SUM(ValorCIF) From Instrucciones Where IdInstruccion in (" + IdInstrucciones + ")) as CIF,D.Cuenta";
        sql += " ,D.Gasto,SUM(ValorVentaDolares) as ValorDolar, G.NombreAduana as Aduana, A.IdGasto, C.CodigoPaisCliente";
        sql += " ,SUM(A.ValorCostoDolares) as Costo, COUNT(A.IdGasto) as Cantidad,C.CodigoPais as PAIS FROM DetalleGastos A ";
        sql += " Inner Join Instrucciones B on (A.IdInstruccion = B.IdInstruccion and B.CodEstado != 100 and A.Estado not in (0,99))";
        sql += " Inner Join Clientes C on (B.IdCliente = C.CodigoSAP and C.Eliminado = 0)";
        sql += " Inner Join GatosTracking D on (A.IdGasto = D.IdGasto and D.Estado = 0 and D.Impuesto = 0)";
        sql += " Inner Join Aduanas G on ( B.CodigoAduana = G.CodigoAduana and G.CodEstado = 0)";
        sql += " Left Join EspeciesFiscalesInstrucciones J on (A.IdInstruccion = J.IdInstruccion and J.Cliente = 'X' and J.CodEspecieFiscal = 'F' and J.Eliminado = 0)";
        sql += " Left Join EspeciesFiscalesInstrucciones K on (A.IdInstruccion = K.IdInstruccion and K.Cliente = 'X' and K.CodEspecieFiscal = 'P' and K.Eliminado = 0)";
        sql += " Where A.IdInstruccion in(" + IdInstrucciones + ")";
        sql += " Group by C.CodigoSAP,C.Nombre,D.Gasto,D.Cuenta,G.NombreAduana,A.IdGasto,C.CodigoPaisCliente,C.CodigoPais";
        this.loadSQL(sql);
    }
    public void CargarGastosExellAgrupados(string IdInstrucciones)
    {
        string sql = " SELECT C.CodigoSAP+'-'+C.Nombre as Clientes ,(Select SUM(ValorCIF) From Instrucciones Where IdInstruccion in (" + IdInstrucciones + ")) as CIF,D.Cuenta";
        sql += " ,D.Gasto,SUM(ValorVentaDolares) as ValorDolar, G.NombreAduana as Aduana, A.IdGasto, C.CodigoPaisCliente";
        sql += " ,SUM(A.ValorCostoDolares) as Costo, COUNT(A.IdGasto) as Cantidad,C.CodigoPais as PAIS FROM DetalleGastos A ";
        sql += " Inner Join Instrucciones B on (A.IdInstruccion = B.IdInstruccion and B.CodEstado != 100 and A.Estado not in (0,99))";
        sql += " Inner Join Clientes C on (B.IdCliente = C.CodigoSAP and C.Eliminado = 0)";
        sql += " Inner Join GatosTracking D on (A.IdGasto = D.IdGasto and D.Estado = 0 and D.Impuesto = 0)";
        sql += " Inner Join Aduanas G on ( B.CodigoAduana = G.CodigoAduana and G.CodEstado = 0)";
        sql += " Where A.IdInstruccion in(" + IdInstrucciones + ")";
        sql += " Group by C.CodigoSAP,C.Nombre,D.Gasto,D.Cuenta,G.NombreAduana,A.IdGasto,C.CodigoPaisCliente,C.CodigoPais";
        this.loadSQL(sql);
    }
    public void RevisionPreFacturacion()
    {
        string sql = "SELECT A.IdInstruccion as HojaRuta,D.Nombre as Cliente, E.NombreAduana as Aduana,Sum(A.[Total]) as TotalGastos ,SUM(ISNULL(B.Total,0)) as TotalComprobado";
        sql += " ,(Sum(A.[Total])- SUM(ISNULL(B.Total,0)))as Diferencia ,Sum(A.[ValorCostoDolares]) as TotalGastosDolares";
        sql += " ,SUM(ISNULL(B.ValorCostoDolares,0)) as TotalDolaresComprobadoDolares";
        sql += " ,(Sum(A.[ValorCostoDolares])-SUM(ISNULL(B.ValorCostoDolares,0))) DiferenciaDolares";
        sql += " ,(COUNT(A.IdInstruccion)-COUNT(B.IdInstruccion)) as Pendiente";
        sql += " FROM DetalleGastos A";
        sql += " Inner Join Instrucciones C on (A.IdInstruccion = C.IdInstruccion)";
        sql += " Inner Join Clientes D on (C.IdCliente = D.CodigoSAP and D.Eliminado = 0)";
        sql += " Inner Join Aduanas E on (C.CodigoAduana = E.CodigoAduana and E.CodEstado = 0)";
        sql += " Left Join DetalleGastos B on (A.IdInstruccion = B.IdInstruccion and A.IdGasto = B.IdGasto and B.Estado not in (0,1))";
        sql += " Where A.Estado in (0,1,2,3,4,10) and A.CodEstadoFacturacion = 0 Group by A.IdInstruccion,D.Nombre, E.NombreAduana";
        sql += " Order by 2,3";
        this.loadSQL(sql);
    }
    public void RevisionPreFacturacionXPais(string codpais)
    {
        string sql = "SELECT A.IdInstruccion as HojaRuta,D.Nombre as Cliente, E.NombreAduana as Aduana,Sum(A.[Total]) as TotalGastos ,SUM(ISNULL(B.Total,0)) as TotalComprobado";
        sql += " ,(Sum(A.[Total])- SUM(ISNULL(B.Total,0)))as Diferencia ,Sum(A.[ValorCostoDolares]) as TotalGastosDolares";
        sql += " ,SUM(ISNULL(B.ValorCostoDolares,0)) as TotalDolaresComprobadoDolares";
        sql += " ,(Sum(A.[ValorCostoDolares])-SUM(ISNULL(B.ValorCostoDolares,0))) DiferenciaDolares";
        sql += " ,(COUNT(A.IdInstruccion)-COUNT(B.IdInstruccion)) as Pendiente";
        sql += " FROM DetalleGastos A";
        sql += " Inner Join Instrucciones C on (A.IdInstruccion = C.IdInstruccion)";
        sql += " Inner Join Clientes D on (C.IdCliente = D.CodigoSAP and D.Eliminado = 0)";
        sql += " Inner Join Aduanas E on (C.CodigoAduana = E.CodigoAduana and E.CodEstado = 0)";
        sql += " Left Join DetalleGastos B on (A.IdInstruccion = B.IdInstruccion and A.IdGasto = B.IdGasto and B.Estado not in (0,1))";
        sql += " Where A.Estado in (0,1,2,3,4,10) and A.CodEstadoFacturacion = 0  and A.CodPais = '" + codpais + "' Group by A.IdInstruccion,D.Nombre, E.NombreAduana";
        sql += " Order by 2,3";
        this.loadSQL(sql);
    }
    public void CargarDetalleFacturacion(string hoja)
    {
        string sql = "SELECT A.IdInstruccion as HojadeRuta,B.Cuenta+'-'+B.Gasto as Gasto,A.ValorCostoDolares as Costo, A.ValorVentaDolares as VentaDolares ";
        sql += " , D.NombreAduana as Aduana, E.Codigo +'-'+E.Numeracion as Numeraciones2, F.Nombre as Cliente";
        sql += " From DetalleGastos A";
        sql += " Inner Join GatosTracking B on (B.IdGasto = A.IdGasto)";
        sql += " Inner Join Instrucciones C on (A.IdInstruccion = C.IdInstruccion and C.CodEstado != 100)";
        sql += " Inner Join Aduanas D on (C.CodigoAduana = D.CodigoAduana and D.CodEstado = 0)";
        sql += " Inner Join Numeraciones E on (E.Codigo = A.CodPais)";
        sql += " Inner Join Clientes F on (C.IdCliente = F.CodigoSAP and F.Eliminado = 0)";
        sql += " Where A.IdInstruccion in (" + hoja + ") and A.Estado != 99";
        this.loadSQL(sql);
    }
    public void DetalleGastosXHoja(string hoja)
    {
        string sql = @" Select B.Gasto as Gasto ,C.Descripcion as MedioPago,A.Monto as Monto,A.IVA as IVA ,A.TotalVenta as ValorNegociado,D.Nombre as Proveedor,E.Descripcion as EstadoActual, 
         Case  A.Estado When 0 Then A.FechaCreacion When 1 Then A.FechaPagoSistema When 2 Then ISNULL(F.Fecha,A.FechaPago) 
         When 3 Then A.FechaDigitalizacion When 4 Then A.FechaNumeroSAP When 7 Then A.FechaCreacion 
         When 10 Then ISNULL(A.FechaEnvioFactura,A.FechaCreacion) Else A.FechaCreacion End as UltimaFecha, 
         Case  A.CodEstadoFacturacion When  0 Then 'Pendiente Facturacion' Else 'Facturado' End as Facturado 
         From DetalleGastos A 
         Inner Join GatosTracking B on (A.IdGasto = B.IdGasto) 
         Inner Join Codigos C on (C.Categoria = 'MEDIOPAGO' and C.Codigo = A.MedioPago) 
         Inner Join ProveedoresSolicitud D on (D.Cuenta = A.Proveedor and D.CodPais = A.CodPais) 
         Inner Join Codigos E on (E.Categoria = 'SOLICITUDPAGO' and E.Codigo = A.Estado) 
         Left Join Carga F on (A.NumeroCarga = F.IdCarga) 
         Where IdInstruccion = '" + hoja + "'";
        this.loadSQL(sql);
    }
    public void CargarDatosReporteGastos(string IdCliente, string IdGasto, string FechaInicio, string FechaFinal, string Aduana)
    {
        string sql = @"SELECT
                       A.IdInstruccion,C.Nombre,F.Gasto,Total,B.Producto,B.Fecha as FechaCreacionHoja,A.FechaCreacion as FechaCreacionGasto,B.FechaFinalFlujo as FechaEntregaServicio
                       ,E.Fecha as ArriboCarga,B.CodigoGuia,B.NumeroFactura,D.Cabezal,D.Furgon
                       FROM DetalleGastos A
                       Inner Join Instrucciones B on (A.IdInstruccion = B.IdInstruccion and B.IdCliente = '" + IdCliente + "')";
        sql += @"Inner Join Clientes C on (B.IdCliente = C.CodigoSAP)
                       Inner Join DatosGestionDocumentos D on (A.IdInstruccion = D.IdInstruccion)
                       Inner Join GestionCarga E on (A.IdInstruccion = E.IdInstruccion and E.Estado = 0)
                       Inner Join GatosTracking F on (F.IdGasto = A.IdGasto)";
        sql += "Where A.IdGasto in ('" + IdGasto + "') and FechaCreacion >='" + FechaInicio + "' and FechaCreacion <= '" + FechaFinal + "'and B.CodigoAduana = '" + Aduana + "' and A.Estado not in (7,99)";
        sql += "Group by A.IdInstruccion,C.Nombre,F.Gasto,Total,B.Producto,B.Fecha,A.FechaCreacion,B.FechaFinalFlujo,B.CodigoGuia,B.NumeroFactura,D.Cabezal,D.Furgon,E.Fecha ";
        this.loadSQL(sql);
    }
    public void BuscarHojasyActualizarlas(string Hoja, string IdUsuario, string referenciafacturacion)
    {
        string sql = "Update DetalleGastos Set  CodEstadoFacturacion = 1, FechaEnvioFactura = GETDATE(),UsuarioEnvioFactura= '" + IdUsuario + "',ReferenciaFacturacion = '" + referenciafacturacion + "'  Where IdInstruccion in (" + Hoja + ") and Estado != 99";
        this.loadSQL(sql);
    }
    public void CargarGastoXID(string IdGasto)
    {
        string sql = " SELECT * FROM DetalleGastos A ";
        sql += " Where A.Id = '" + IdGasto + "'";
        this.loadSQL(sql);
    }
    public void CargarGastoXIDDocumento(string IdGasto)
    {
        string sql = " SELECT * FROM DetalleGastos A ";
        sql += " Where A.Id in(" + IdGasto + ")";
        this.loadSQL(sql);
    }

    public void EliminarGastoXIDDocumento(string IdGasto)
    {
        string sql = " Delete DetalleGastos  ";
        sql += " Where Id in(" + IdGasto + ")";
        this.loadSQL(sql);
    }
    public void ActualizarGrupo(string IdGasto, string digitalizacion, string UsuarioDigitalizacion, string referenciaGasto, string ReferenciaMadre,string fecha)
    {
        string sql = " Update DetalleGastos Set  Estado = 3, FechaDigitalizacion = GETDATE(), UsuarioDigitalizacion = '" + UsuarioDigitalizacion + "'";
        sql += " ,NumeroReciboDigitalizado='" + referenciaGasto + "' ,IdDigitalizacion='" + ReferenciaMadre + "', FechaPagoTesoreria='"+fecha + "'  Where Id in(" + IdGasto + ") and MedioPago != 'CC'";
        this.loadSQL(sql);
    }
    public void ActualizarGrupoCajaChica(string IdGasto, string digitalizacion, string UsuarioDigitalizacion)
    {
        string sql = " Update DetalleGastos Set  Estado = 10, IdDigitalizacion = '" + digitalizacion + "', FechaDigitalizacion = GETDATE(), UsuarioDigitalizacion = '" + UsuarioDigitalizacion + "'";
        sql += " Where Id in(" + IdGasto + ") and MedioPago = 'CC'";
        this.loadSQL(sql);
    }
    public void RevisarGastoCargado(string IdGasto)
    {
        string sql = " SELECT * FROM DetalleGastos A ";
        sql += " Where A.Id = '" + IdGasto + "' and Estado = 3";
        this.loadSQL(sql);
    }
    public int eliminarAutorizacionesModulo(string codigo)
    {
        return this.executeCustomSQL("delete from AutorizacionesModulo where IdRol = " + codigo);
    }
    public int darBajaAutorizacionesModulo(string codigo)
    {
        return this.executeCustomSQL("update AutorizacionesModulo set Estado = '1' where IdRol = " + codigo);
    }
    #endregion

    #region Campos
    public string REVERSARPARTIDA
    {
        get
        {
            return (string)registro[Campos.REVERSARPARTIDA].ToString();
        }
        set
        {
            registro[Campos.REVERSARPARTIDA] = value;
        }
    }
    public string CODESTADOFACTURACION
    {
        get
        {
            return (string)registro[Campos.CODESTADOFACTURACION].ToString();
        }
        set
        {
            registro[Campos.CODESTADOFACTURACION] = value;
        }
    }
    public string FACTURADINANT
    {
        get
        {
            return (string)registro[Campos.FACTURADINANT].ToString();
        }
        set
        {
            registro[Campos.FACTURADINANT] = value;
        }
    }
    public string REFERENCIAFACTURACION
    {
        get
        {
            return (string)registro[Campos.REFERENCIAFACTURACION].ToString();
        }
        set
        {
            registro[Campos.REFERENCIAFACTURACION] = value;
        }
    }
    public string FECHAENVIOFACTURA
    {
        get
        {
            return (string)registro[Campos.FECHAENVIOFACTURA].ToString();
        }
        set
        {
            registro[Campos.FECHAENVIOFACTURA] = value;
        }
    }
    public string USUARIOENVIOFACTURA
    {
        get
        {
            return (string)registro[Campos.USUARIOENVIOFACTURA].ToString();
        }
        set
        {
            registro[Campos.USUARIOENVIOFACTURA] = value;
        }
    }
    public string FECHADIGITALIZACION
    {
        get
        {
            return (string)registro[Campos.FECHADIGITALIZACION].ToString();
        }
        set
        {
            registro[Campos.FECHADIGITALIZACION] = value;
        }
    }
    public string USUARIODIGITALIZACION
    {
        get
        {
            return (string)registro[Campos.USUARIODIGITALIZACION].ToString();
        }
        set
        {
            registro[Campos.USUARIODIGITALIZACION] = value;
        }
    }
    public string NUMERORECIBODIGITALIZADO
    {
        get
        {
            return (string)registro[Campos.NUMERORECIBODIGITALIZADO].ToString();
        }
        set
        {
            registro[Campos.NUMERORECIBODIGITALIZADO] = value;
        }
    }
    public string IDDIGITALIZACION
    {
        get
        {
            return (string)registro[Campos.IDDIGITALIZACION].ToString();
        }
        set
        {
            registro[Campos.IDDIGITALIZACION] = value;
        }
    }
    public Double TASACAMBIOFIJA
    {
        get
        {
            return Convert.ToDouble(registro[Campos.TASACAMBIOFIJA].ToString());
        }
        set
        {
            registro[Campos.TASACAMBIOFIJA] = value;
        }
    }
    public Double TASACAMBIOVARIABLE
    {
        get
        {
            return Convert.ToDouble(registro[Campos.TASACAMBIOVARIABLE].ToString());
        }
        set
        {
            registro[Campos.TASACAMBIOVARIABLE] = value;
        }
    }
    public Double TOTALVENTA
    {
        get
        {
            return Convert.ToDouble(registro[Campos.TOTALVENTA].ToString());
        }
        set
        {
            registro[Campos.TOTALVENTA] = value;
        }
    }
    public string IDINSTRUCCION
    {
        get
        {
            return (string)registro[Campos.IDINSTRUCCION].ToString();
        }
        set
        {
            registro[Campos.IDINSTRUCCION] = value;
        }
    }
    public string IDGASTO
    {
        get
        {
            return (string)registro[Campos.IDGASTO].ToString();
        }
        set
        {
            registro[Campos.IDGASTO] = value;
        }
    }
    public string MEDIOPAGO
    {
        get
        {
            return (string)registro[Campos.MEDIOPAGO].ToString();
        }
        set
        {
            registro[Campos.MEDIOPAGO] = value;
        }
    }
    public string PROVEEDOR
    {
        get
        {
            return (string)registro[Campos.PROVEEDOR].ToString();
        }
        set
        {
            registro[Campos.PROVEEDOR] = value;
        }
    }
    public Double MONTO
    {
        get
        {
            return Convert.ToDouble(registro[Campos.MONTO].ToString());
        }
        set
        {
            registro[Campos.MONTO] = value;
        }
    }
    public Double IVA
    {
        get
        {
            return Convert.ToDouble(registro[Campos.IVA].ToString());
        }
        set
        {
            registro[Campos.IVA] = value;
        }
    }
    public string MONEDA
    {
        get
        {
            return (string)registro[Campos.MONEDA].ToString();
        }
        set
        {
            registro[Campos.MONEDA] = value;
        }
    }
    public string NUMERORECIBO
    {
        get
        {
            return (string)registro[Campos.NUMERORECIBO].ToString();
        }
        set
        {
            registro[Campos.NUMERORECIBO] = value;
        }
    }
    public string OBSERVACION
    {
        get
        {
            return (string)registro[Campos.OBSERVACION].ToString();
        }
        set
        {
            registro[Campos.OBSERVACION] = value;
        }
    }
    public string FECHAPAGO
    {
        get
        {
            return (string)registro[Campos.FECHAPAGO].ToString();
        }
        set
        {
            registro[Campos.FECHAPAGO] = value;
        }
    }
    public string FECHAPAGOSISTEMA
    {
        get
        {
            return (string)registro[Campos.FECHAPAGOSISTEMA].ToString();
        }
        set
        {
            registro[Campos.FECHAPAGOSISTEMA] = value;
        }
    }
    public string ESTADO
    {
        get
        {
            return (string)registro[Campos.ESTADO].ToString();
        }
        set
        {
            registro[Campos.ESTADO] = value;
        }
    }
    public string FECHACREACION
    {
        get
        {
            return (string)registro[Campos.FECHACREACION].ToString();
        }
        set
        {
            registro[Campos.FECHACREACION] = value;
        }
    }
    public string NUMEROSAP
    {
        get
        {
            return (string)registro[Campos.NUMEROSAP].ToString();
        }
        set
        {
            registro[Campos.NUMEROSAP] = value;
        }
    }
    public string FECHANUMEROSAP
    {
        get
        {
            return (string)registro[Campos.FECHANUMEROSAP].ToString();
        }
        set
        {
            registro[Campos.FECHANUMEROSAP] = value;
        }
    }
    public string NUMEROCARGA
    {
        get
        {
            return (string)registro[Campos.NUMEROCARGA].ToString();
        }
        set
        {
            registro[Campos.NUMEROCARGA] = value;
        }
    }
    public string USUARIO
    {
        get
        {
            return (string)registro[Campos.USUARIO].ToString();
        }
        set
        {
            registro[Campos.USUARIO] = value;
        }
    }
    public string USUARIOPAGO
    {
        get
        {
            return (string)registro[Campos.USUARIOPAGO].ToString();
        }
        set
        {
            registro[Campos.USUARIOPAGO] = value;
        }
    }
    public string CLIENTE
    {
        get
        {
            return (string)registro[Campos.CLIENTE].ToString();
        }
        set
        {
            registro[Campos.CLIENTE] = value;
        }
    }
    #endregion
}
