using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for HojaRutaDatosBO
/// </summary>
public class GuiasBO : CapaBase
{
    class Campos
    {
public static string	CODIGOGUIA	=	"CodigoGuia";
public static string	CORRELATIVOGUIA	= "CorrelativoGuia";
public static string	CODIGOCLIENTE	= "CodigoCliente";
public static string	CODIGOPROVEEDOR	= "CodigoProveedor";
public static string	CODIGOSOLICITUDFLETE = "CodigoSolicitudFlete";
public static string	CODIGONEGOCIACION = "CodigoNegociacion";
public static string	CODIGORUTA = "CodigoRuta";
public static string	PRODUCTO = "Producto";
public static string	FLETEFALSO = "FleteFalso";
public static string	COBROMONITOREO = "CobroMonitoreo";
public static string	POSICIONAMIENTO	= "Posicionamiento";
public static string	CONDICIONESCREDITODIAS = "CondicionesCreditoDias";
public static string	TIEMPOLIBRECARGAHRS = "TiempoLibreCargaHrs";
public static string	TIEMPOLIBREDESCARGAHRS = "TiempoLibreDescargaHrs";
public static string	DIASNOHABILES = "DiasNoHabiles";
public static string	COBROSOBRESTADIADIARIO = "CobroSobrestadiaDiario";
public static string	PORCENTAJECARGAPELIGROSA = "PorcentajeCargaPeligrosa";
public static string	CUSTODIO = "Custodio";
public static string	SEGURO = "Seguro";
public static string	ADUANA = "Aduana";
public static string	ALMACENAMIENTO = "Almacenamiento";
public static string	CUADRILLA = "Cuadrilla";
public static string	CODIGOMOTORISTA	= "CodigoMotorista";
public static string	CODIGOUNIDAD = "CodigoUnidad";
public static string	REFERENCIA1	= "Referencia1";
public static string	REFERENCIA2	= "Referencia2";
public static string	REFERENCIA3	= "Referencia3";
public static string	REFERENCIA4	= "Referencia4";
public static string	FECHA = "Fecha";
public static string	OBSERVACION	= "Observacion";
public static string	CODESTADO	= "CodEstado";
public static string	LLAMADA	= "Llamada";
public static string	IDTXNLLAMADA = "IdTxnLlamada";
public static string	LIQUIDADO = "Liquidado";
public static string	MONITOREOREMOTOINICIO = "MonitoreoRemotoInicio";
public static string	MONITOREOREMOTOFIN = "MonitoreoRemotoFin";
public static string	IDUSUARIO = "IdUsuario";
public static string	RELACIONADUANAS	= "RelacionAduanas";
public static string	CODLISTADINANT = "CodListaDinant";
public static string	CODREQUERIMIENTO = "CodRequerimiento";


    }

    public GuiasBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from HojaRutaDatos ";
        this.initializeSchema("HojaRutaDatos");
    }

    public void loadAllAduanas()
    {
        string sql = coreSQL;
        sql += " WHERE CodEstado = '0' ORDER BY CodPais DESC, CodigoAduana ASC ";
        this.loadSQL(sql);
    }


    public void CargarGuia(string guia)
    {
        string sql = "Select * From TrackingTerrestreNueva.dbo.Guias Where CorrelativoGuia = '" + guia + "'";
        this.loadSQL(sql);
    }
    public void LoadReporteJulio(string FechaInicio, string FechaFinal, string Cliente)
    {
        string sql = @"Select a.CorrelativoGuia,NombreEmpresa,B.NumeroFactura +' / '+ C.NumeroFactura as NumeroFactura,Producto,Empresa,Motorista,PlacaCamion,PlacaFurgon,TipoEquipo
        ,PaisOrigen,CiudadDestino,EstadoFlete,LLegadaOrigen,InicioCarga,FinCarga,EntregaDocumentos,SalidaOrigen,PermisoInicio,PermisoFin
        ,HojaUno,ArriboAduana1,ComienzoFlujo1,ValidacionElectronica1,PagoImpuestos1,AsignacionCanalSelectividad1,RevisionMercancia1,EmisionPaseSalida1,EntregaServicio1,B.Color as Color1, PermisoEnteGubernamentalInicio1, PermisoEnteGubernamentalFin1,B.ObservacionGeneral as ObservacionGeneral1
        ,HojaDos,ArriboAduana2,ComienzoFlujo2,ValidacionElectronica2,PagoImpuestos2,AsignacionCanalSelectividad2,RevisionMercancia2,EmisionPaseSalida2,EntregaServicio2,C.Color as Color2, PermisoEnteGubernamentalInicio2, PermisoEnteGubernamentalFin2,C.ObservacionGeneral as ObservacionGeneral2
        ,ArriboAduana3,SalidaAduana3,LlegadaDestino,InicioDescarga,FinDescarga
        From  [TrackingTerrestreNueva].[dbo].[VistaFlujoTerrestre_Darwin] A
        Inner Join [TrackingTerrestreNueva].[dbo].[VistaFlujoAduanaPrimeraHoja_Darwin] B on (A.CorrelativoGuia = B.CorrelativoGuia)
        Inner Join [TrackingTerrestreNueva].[dbo].[VistaFlujoAduanaSegundaHoja_Darwin] C on (A.CorrelativoGuia = C.CorrelativoGuia)
        Where A.CodEstado != '100' AND CONVERT(Varchar,A.Fecha,111) BETWEEN '" + FechaInicio + "' AND '" + FechaFinal + "' AND SUBSTRING(A.CorrelativoGuia,4,1) = '1' AND A.CodigoCliente = '" + Cliente + "'";
        loadSQL(sql);
    }

    #region campos
    public string CODIGOGUIA
    {
        get
        {
            return (string)registro[Campos.CODIGOGUIA].ToString();
        }
        set
        {
            registro[Campos.CODIGOGUIA] = value;
        }
    }

    public string CORRELATIVOGUIA	
    {
        get
        {
            return (string)registro[Campos.CORRELATIVOGUIA].ToString();
        }
        set
        {
            registro[Campos.CORRELATIVOGUIA] = value;
        }
    }

    public string CODIGOCLIENTE	
    {
        get
        {
            return (string)registro[Campos.CODIGOCLIENTE].ToString();
        }
        set
        {
            registro[Campos.CODIGOCLIENTE] = value;
        }
    }
    public string CODIGOPROVEEDOR	
    {
        get
        {
            return (string)registro[Campos.CODIGOPROVEEDOR].ToString();
        }
        set
        {
            registro[Campos.CODIGOPROVEEDOR] = value;
        }
    }
    public string CODIGOSOLICITUDFLETE
    {
        get
        {
            return (string)registro[Campos.CODIGOSOLICITUDFLETE].ToString();
        }
        set
        {
            registro[Campos.CODIGOSOLICITUDFLETE] = value;
        }
    }
    public string CODIGONEGOCIACION
    {
        get
        {
            return (string)registro[Campos.CODIGONEGOCIACION].ToString();
        }
        set
        {
            registro[Campos.CODIGONEGOCIACION] = value;
        }
    }
    public string CODIGORUTA
    {
        get
        {
            return (string)registro[Campos.CODIGORUTA].ToString();
        }
        set
        {
            registro[Campos.CODIGORUTA] = value;
        }
    }
    public string PRODUCTO
    {
        get
        {
            return (string)registro[Campos.PRODUCTO].ToString();
        }
        set
        {
            registro[Campos.PRODUCTO] = value;
        }
    }
    public string FLETEFALSO
    {
        get
        {
            return (string)registro[Campos.FLETEFALSO].ToString();
        }
        set
        {
            registro[Campos.FLETEFALSO] = value;
        }
    }
    public string COBROMONITOREO
    {
        get
        {
            return (string)registro[Campos.COBROMONITOREO].ToString();
        }
        set
        {
            registro[Campos.COBROMONITOREO] = value;
        }
    }
    public string POSICIONAMIENTO
    {
        get
        {
            return (string)registro[Campos.POSICIONAMIENTO].ToString();
        }
        set
        {
            registro[Campos.POSICIONAMIENTO] = value;
        }
    }
    public string CONDICIONESCREDITODIAS
    {
        get
        {
            return (string)registro[Campos.CONDICIONESCREDITODIAS].ToString();
        }
        set
        {
            registro[Campos.CONDICIONESCREDITODIAS] = value;
        }
    }
    public string TIEMPOLIBRECARGAHRS
    {
        get
        {
            return (string)registro[Campos.TIEMPOLIBRECARGAHRS].ToString();
        }
        set
        {
            registro[Campos.TIEMPOLIBRECARGAHRS] = value;
        }
    }
    public string TIEMPOLIBREDESCARGAHRS
    {
        get
        {
            return (string)registro[Campos.TIEMPOLIBREDESCARGAHRS].ToString();
        }
        set
        {
            registro[Campos.TIEMPOLIBREDESCARGAHRS] = value;
        }
    }
    public string DIASNOHABILES
    {
        get
        {
            return (string)registro[Campos.DIASNOHABILES].ToString();
        }
        set
        {
            registro[Campos.DIASNOHABILES] = value;
        }
    }
    public string COBROSOBRESTADIADIARIO
    {
        get
        {
            return (string)registro[Campos.COBROSOBRESTADIADIARIO].ToString();
        }
        set
        {
            registro[Campos.COBROSOBRESTADIADIARIO] = value;
        }
    }
    public string PORCENTAJECARGAPELIGROSA
    {
        get
        {
            return (string)registro[Campos.PORCENTAJECARGAPELIGROSA].ToString();
        }
        set
        {
            registro[Campos.PORCENTAJECARGAPELIGROSA] = value;
        }
    }
    public string CUSTODIO
    {
        get
        {
            return (string)registro[Campos.CUSTODIO].ToString();
        }
        set
        {
            registro[Campos.CUSTODIO] = value;
        }
    }
    public string SEGURO
    {
        get
        {
            return (string)registro[Campos.SEGURO].ToString();
        }
        set
        {
            registro[Campos.SEGURO] = value;
        }
    }
    public string ADUANA
    {
        get
        {
            return (string)registro[Campos.ADUANA].ToString();
        }
        set
        {
            registro[Campos.ADUANA] = value;
        }
    }
    public string ALMACENAMIENTO
    {
        get
        {
            return (string)registro[Campos.ALMACENAMIENTO].ToString();
        }
        set
        {
            registro[Campos.ALMACENAMIENTO] = value;
        }
    }
    public string CUADRILLA
    {
        get
        {
            return (string)registro[Campos.CUADRILLA].ToString();
        }
        set
        {
            registro[Campos.CUADRILLA] = value;
        }
    }
    public string CODIGOMOTORISTA
    {
        get
        {
            return (string)registro[Campos.CODIGOMOTORISTA].ToString();
        }
        set
        {
            registro[Campos.CODIGOMOTORISTA] = value;
        }
    }
    public string CODIGOUNIDAD
    {
        get
        {
            return (string)registro[Campos.CODIGOUNIDAD].ToString();
        }
        set
        {
            registro[Campos.CODIGOUNIDAD] = value;
        }
    }
    public string REFERENCIA1
    {
        get
        {
            return (string)registro[Campos.REFERENCIA1].ToString();
        }
        set
        {
            registro[Campos.REFERENCIA1] = value;
        }
    }
    public string REFERENCIA2
    {
        get
        {
            return (string)registro[Campos.REFERENCIA2].ToString();
        }
        set
        {
            registro[Campos.REFERENCIA2] = value;
        }
    }
    public string REFERENCIA3
    {
        get
        {
            return (string)registro[Campos.REFERENCIA3].ToString();
        }
        set
        {
            registro[Campos.REFERENCIA3] = value;
        }
    }
    public string REFERENCIA4
    {
        get
        {
            return (string)registro[Campos.REFERENCIA4].ToString();
        }
        set
        {
            registro[Campos.REFERENCIA4] = value;
        }
    }
    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }
    public string OBSERVACION
    {
        get
        {
            return (string)registro[Campos.OBSERVACION].ToString();
        }
        set
        {
            registro[Campos.OBSERVACION] = value;
        }
    }
    public string CODESTADO
    {
        get
        {
            return (string)registro[Campos.CODESTADO].ToString();
        }
        set
        {
            registro[Campos.CODESTADO] = value;
        }
    }
    public string LLAMADA
    {
        get
        {
            return (string)registro[Campos.LLAMADA].ToString();
        }
        set
        {
            registro[Campos.LLAMADA] = value;
        }
    }
    public string IDTXNLLAMADA
    {
        get
        {
            return (string)registro[Campos.IDTXNLLAMADA].ToString();
        }
        set
        {
            registro[Campos.IDTXNLLAMADA] = value;
        }
    }
    public string LIQUIDADO
    {
        get
        {
            return (string)registro[Campos.LIQUIDADO].ToString();
        }
        set
        {
            registro[Campos.LIQUIDADO] = value;
        }
    }
    public string MONITOREOREMOTOINICIO
    {
        get
        {
            return (string)registro[Campos.MONITOREOREMOTOINICIO].ToString();
        }
        set
        {
            registro[Campos.MONITOREOREMOTOINICIO] = value;
        }
    }
    public string MONITOREOREMOTOFIN
    {
        get
        {
            return (string)registro[Campos.MONITOREOREMOTOFIN].ToString();
        }
        set
        {
            registro[Campos.MONITOREOREMOTOFIN] = value;
        }
    }
    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }
    public string RELACIONADUANAS
    {
        get
        {
            return (string)registro[Campos.RELACIONADUANAS].ToString();
        }
        set
        {
            registro[Campos.RELACIONADUANAS] = value;
        }
    }
    public string CODLISTADINANT
    {
        get
        {
            return (string)registro[Campos.CODLISTADINANT].ToString();
        }
        set
        {
            registro[Campos.CODLISTADINANT] = value;
        }
    }
    public string CODREQUERIMIENTO
    {
        get
        {
            return (string)registro[Campos.CODREQUERIMIENTO].ToString();
        }
        set
        {
            registro[Campos.CODREQUERIMIENTO] = value;
        }
    }
    #endregion

}
