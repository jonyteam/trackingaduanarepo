﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for HojaRutaFlujoPortuarioBO
/// </summary>
public class HojaRutaFlujoPortuarioBO : CapaBase
{

    class Campos
    {
        public static string IDHOJARUTA = "IdHojaRuta";
        public static string IDFLUJO = "IdFlujo";
        public static string IDUSUARIO = "IdUsuario";
        public static string FCHINGRESO = "IdUsuario";
        public static string OBS = "Obs";
    }

    public HojaRutaFlujoPortuarioBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from HojaRutaFlujoPortuario";
        this.initializeSchema("HojaRutaFlujoPortuario");
    }

    public void loadHojaRutaFlujo(int IdFlujo)
    {
        string sql = this.coreSQL;
        sql += " where IdFlujo = " + IdFlujo;
        this.loadSQL(sql);
    }

    public void loadHojaRutalastFlujo(string IdHojaRuta)
    {
        string sql = this.coreSQL;
        sql += " where convert(varchar,FchIngreso,112) = (select Convert(varchar,max(FchIngreso),112) from HojaRutaFlujoPortuario where IdHojaRuta = '" + IdHojaRuta + "') and IdHojaRuta = " + IdHojaRuta;
        this.loadSQL(sql);
    }

    public void loadHojaRutaFlujo(string IdHojaRuta, int IdFlujo)
    {
        string sql = this.coreSQL;
        sql += " where IdHojaRuta = '" + IdHojaRuta + "' and IdFlujo = " + IdFlujo;
        this.loadSQL(sql);
    }

    public int eliminarRegistro(string IdHojaRuta, int IdFlujo)
    {
        return this.executeCustomSQL(" DELETE HojaRutaFlujoPortuario WHERE IdHojaRuta = '" + IdHojaRuta + "' AND IdFlujo = " + IdFlujo);
    }

    public string IDHOJARUTA
    {
        get
        {
            return registro[HojaRutaFlujoPortuarioBO.Campos.IDHOJARUTA].ToString();
        }
        set
        {
            registro[HojaRutaFlujoPortuarioBO.Campos.IDHOJARUTA] = value;
        }
    }

    public Int16 IDFLUJO
    {
        get
        {
            return Convert.ToInt16(registro[HojaRutaFlujoPortuarioBO.Campos.IDFLUJO].ToString());
        }
        set
        {
            registro[HojaRutaFlujoPortuarioBO.Campos.IDFLUJO] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return registro[HojaRutaFlujoPortuarioBO.Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[HojaRutaFlujoPortuarioBO.Campos.IDUSUARIO] = value;
        }
    }

    public string OBS
    {
        get
        {
            return registro[HojaRutaFlujoPortuarioBO.Campos.OBS].ToString();
        }
        set
        {
            registro[HojaRutaFlujoPortuarioBO.Campos.OBS] = value;
        }
    }

}
