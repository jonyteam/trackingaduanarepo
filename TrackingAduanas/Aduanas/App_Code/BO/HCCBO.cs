using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for AduanasBO
/// </summary>
public class HCCBO : CapaBase
{
    class Campos
    {
            public static string ITEM="Item";
            public static string IDINSTRUCCION="IdInstruccion";
            public static string FACTURAVESTA="FacturaVESTA";
            public static string FECHAENTREGACARGILL="FechaEntregaCargill";
            public static string ORDENCOMPRA="OrdenCompra";
            public static string ETAPLANTA="ETAPlanta";
            public static string TRACKINGCOURIER = "TrackingCourier";
            public static string FECHARECIBOCOPIAS="FechaReciboCopias";
            public static string FECHARECIBOORIGINALES="FechaReciboOriginales";
            public static string FECHAARRIBOPLANTAPROCESO="FechaArriboPlantaProceso";
            public static string TIEMPOLLEGADAHASTAENTREGA="TiempoLlegadaHastaEntrega";
            public static string DIASLIBRESALMACENAJE="DiasLibresAlmacenaje";
            public static string TIEMPOENERGIA="TiempoEnergia";
            public static string TERMINATIEMPOLIBREDCS="TerminatiempolibreDCS";
            public static string CARGOALMACENAJEDCS="CargoAlmacenajeDCS";
            public static string TERMINATIEMPOLIBREELECTRICIDAD="TerminaTiempoLibreElectricidad";
            public static string CARGODEMORAELECTRICIDAD="CargoDemoraElectricidad";
            public static string FECHARETORNOCONTENEDOR="FechaRetornoContenedor";
            public static string DIASDEMORA="DiasDemora";
            public static string DEMORA="Demora";
            public static string OBSERVACIONES="Observaciones";
            public static string DECLARACIONSEGURO="DeclaracionSeguro";
            public static string FECHADEFACTURA2 = "FechadeFactura2";

    }

    public HCCBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from HCC ";
        this.initializeSchema("HCC");
    }

    public void loadInstrucciones()
    {
        string sql = " Select * from Instrucciones I ";
        sql += " WHERE CodEstado = '0' AND IdEstadoFlujo != '99' ORDER BY CodigoAduana, Item ";
        this.loadSQL(sql);
    }
    public void loadInstruccionesTemp()
    {
        string sql = " Select * from Instrucciones I ";
        sql += " WHERE CodEstado = '0' AND IdEstadoFlujo != '99' and ArriboCarga = 0 ORDER BY CodigoAduana, Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesCliente()
    {
        string sql = " SELECT DISTINCT I.IdInstruccion, I.*, C.Nombre, DI.DocumentoNo from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = 0) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1' AND DI.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0','1') AND I.IdEstadoFlujo != '99' ";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesClienteInstr()
    {
        string sql = " SELECT * from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        sql += " WHERE I.CodEstado IN ('0') AND I.IdEstadoFlujo != '99' ";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesXPais(string codPais)
    {
        string sql = " SELECT DISTINCT I.IdInstruccion, I.*, C.Nombre, DI.DocumentoNo from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = 0) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1' AND DI.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0','1') AND I.IdEstadoFlujo != '99' AND I.CodPaisHojaRuta = '" + codPais + "' ";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesXPaisFinalizadas(string codPais)
    {
        string sql = " Select * from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = 1 AND DI.Eliminado = '0') ";
        sql += " WHERE CodEstado = '0' AND IdEstadoFlujo = '99' AND CodPaisHojaRuta = '" + codPais + "' AND I.IdInstruccion NOT IN (SELECT Documento FROM HistorialImpresiones WHERE Eliminado = '0') ";
        sql += "ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesFinalizadas()
    {
        string sql = " Select * from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = 1 AND DI.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0') AND IdEstadoFlujo = '99' AND I.IdInstruccion NOT IN (SELECT Documento FROM HistorialImpresiones WHERE Eliminado = '0') ";
        sql += "ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionFinalizada(string idInstruccion)
    {
        string sql = " Select * from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = 1 AND DI.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0') AND IdEstadoFlujo = '99' ";
        sql += " AND I.IdInstruccion = '" + idInstruccion + "' ";
        sql += "ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesAll()
    {
        string sql = " Select FC.EstadoFlujo, CL.Nombre, CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'No.Correlativo:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Asignación Canal de Selectividad%' THEN 'Color:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Pago de Impuestos%' THEN 'Total:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Emisión de Pase de Salida%' THEN 'Tramitador:' END END END END Etiqueta1, ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'Aforador:' ELSE '' END Etiqueta2, ";
        sql += " I.*, C1.Descripcion AS Pais, C2.Codigo + ' | ' + C2.Descripcion AS Regimen from Instrucciones I ";
        sql += " INNER JOIN Clientes CL ON (CL.CodigoSAP = I.IdCliente) ";
        sql += " INNER JOIN FlujoCarga FC ON (FC.IdEstadoFlujo = I.IdEstadoFlujo) ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'Paises') ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) ";
        sql += " WHERE CodEstado = '0' ";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesAllXInstruccion(string idInstruccion)
    {
        string sql = " Select CASE WHEN(I.IdEstadoFlujo != '99') THEN FC.EstadoFlujo ELSE 'Flujo Finalizado' END EstadoFlujo, CL.Nombre, ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'No.Correlativo:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Asignación Canal de Selectividad%' THEN 'Color:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Pago de Impuestos%' THEN 'Total:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Emisión de Pase de Salida%' THEN 'Tramitador:' END END END END Etiqueta1, ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'Aforador:' ELSE '' END Etiqueta2, ";
        sql += " I.*, C1.Descripcion AS Pais, C2.Codigo + ' | ' + C2.Descripcion AS Regimen from Instrucciones I ";
        sql += " INNER JOIN Clientes CL ON (CL.CodigoSAP = I.IdCliente) ";
        sql += " LEFT JOIN FlujoCarga FC ON (FC.IdEstadoFlujo = I.IdEstadoFlujo) ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'Paises') ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) ";
        sql += " WHERE CodEstado = '0' AND I.IdInstruccion = '" + idInstruccion + "' ";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void CargarCargill(string Pais)
    {
        string sql= " SELECT IdInstruccion,D.NombreAduana,B.Nombre,ReferenciaCliente";
               sql+= " ,Case A.IdEstadoFlujo When '99' Then 'Finalizado' When '0' Then 'Comienzo Flujo' Else C.EstadoFlujo End as Estado";
               sql+= " ,FechaRecepcion,A.NumeroFactura,A.CodRegimen,A.* FROM Instrucciones A Inner Join Clientes B on (A.IdCliente = B.CodigoSAP and B.Eliminado = 0)";
               sql+= " Inner Join Aduanas D on (A.CodigoAduana = D.CodigoAduana and D.CodEstado = 0) ";
               sql+= " Left Join FlujoCarga C on (A.IdEstadoFlujo = C.IdEstadoFlujo and C.Eliminado = 0) ";
               sql+= " Where IdCliente in (8000423,1000102,8400911)and DATEADD(MONTH,3,A.[Fecha]) > = GETDATE() and CodPaisHojaRuta = convert(char(1),'"+ Pais +"')";
               sql+= " And A.CodEstado !=100 And A.CodRegimen in ('ID','DI','ED','FI','4000','5100','5600') Order By A.Fecha Desc";
        this.loadSQL(sql);
    }


    public void loadInstruccionesAllXPais(string codPais)
    {
        string sql = " Select FC.EstadoFlujo, CL.Nombre, CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'No.Correlativo:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Asignación Canal de Selectividad%' THEN 'Color:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Pago de Impuestos%' THEN 'Total:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Emisión de Pase de Salida%' THEN 'Tramitador:' END END END END Etiqueta1, ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'Aforador:' ELSE '' END Etiqueta2, ";
        sql += " I.*, C1.Descripcion AS Pais, C2.Codigo + ' | ' + C2.Descripcion AS Regimen from Instrucciones I ";
        sql += " INNER JOIN Clientes CL ON (CL.CodigoSAP = I.IdCliente) ";
        sql += " LEFT JOIN FlujoCarga FC ON (FC.IdEstadoFlujo = I.IdEstadoFlujo) ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'Paises') ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) ";
        sql += " WHERE CodEstado = '0' AND CodPaisHojaRuta = '" + codPais + "' ";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesAllXAduanaUsuario(string codAduana, string idUsuario)
    {
        string sql = " Select FC.EstadoFlujo, CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'No.Correlativo:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Asignación Canal de Selectividad%' THEN 'Color:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Pago de Impuestos%' THEN 'Total:' END END END Etiqueta1, ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'Aforador:' END Etiqueta2, ";
        sql += " I.*, C1.Descripcion AS Pais, C2.Codigo + ' | ' + C2.Descripcion AS Regimen from Instrucciones I ";
        sql += " INNER JOIN FlujoCarga FC ON (FC.IdEstadoFlujo = I.IdEstadoFlujo) ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'Paises') ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) ";
        sql += " WHERE I.CodEstado IN ('0') AND CodigoAduana = '" + codAduana + "' AND IdUsuarioAduana = '" + idUsuario + "' ";
        sql += " Order By 1 ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesAllXUsuario(string idUsuario)
    {
        string sql = " Select FC.EstadoFlujo, CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'No.Correlativo:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Asignación Canal de Selectividad%' THEN 'Color:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Pago de Impuestos%' THEN 'Total:' END END END Etiqueta1, ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'Aforador:' END Etiqueta2, ";
        sql += " I.*, C1.Descripcion AS Pais, C2.Codigo + ' | ' + C2.Descripcion AS Regimen from Instrucciones I ";
        sql += " INNER JOIN FlujoCarga FC ON (FC.IdEstadoFlujo = I.IdEstadoFlujo) ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'Paises') ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) ";
        sql += " WHERE I.CodEstado IN ('0') AND IdUsuarioAduana = '" + idUsuario + "' ";
        sql += " Order By 1 ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesAllXAduana(string codAduana)
    {
        string sql = " Select FC.EstadoFlujo, CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'No.Correlativo:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Asignación Canal de Selectividad%' THEN 'Color:' ELSE ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Pago de Impuestos%' THEN 'Total:' END END END Etiqueta1, ";
        sql += " CASE WHEN FC.EstadoFlujo LIKE '%Validación Electrónica%' THEN 'Aforador:' END Etiqueta2, ";
        sql += " I.*, C1.Descripcion AS Pais, C2.Codigo + ' | ' + C2.Descripcion AS Regimen from Instrucciones I ";
        sql += " INNER JOIN FlujoCarga FC ON (FC.IdEstadoFlujo = I.IdEstadoFlujo) ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'Paises') ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) ";
        sql += " WHERE I.CodEstado IN ('0') AND CodigoAduana = '" + codAduana + "' ";
        sql += " Order By 1 ";
        this.loadSQL(sql);
    }

    public void loadInstruccion(string idInstruccion)
    {
        string sql = "Select * from HCC WHERE IdInstruccion = '" + idInstruccion + "' ";
        this.loadSQL(sql);
    }

    public void loadInstruccionGD(string idInstruccion)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodEstado IN ('0','1') AND IdInstruccion = '" + idInstruccion + "' ";
        this.loadSQL(sql);
    }

    public void loadInstruccionModificar(string idInstruccion)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodEstado IN ('0','1') AND IdInstruccion = '" + idInstruccion + "' ";
        this.loadSQL(sql);
    }

    public void loadInstruccionesXGuia(string codigoGuia)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodEstado != '100' AND CodigoGuia = '" + codigoGuia + "' ORDER BY Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionLlenar(string idInstruccion, string estado)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodEstado = '" + estado + "' AND IdInstruccion = '" + idInstruccion + "' ";
        this.loadSQL(sql);
    }

    public void loadInstruccionXAduana(string codAduana)
    {
        string sql = " SELECT DISTINCT I.IdInstruccion, I.*, C.Nombre, DI.DocumentoNo from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = 0) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1' AND DI.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0','1') AND I.CodigoAduana = '" + codAduana + "' AND I.IdEstadoFlujo != '99' ";
        sql += " ORDER BY I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionXAduanaTemp(string codAduana)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodEstado = '0' AND CodigoAduana = '" + codAduana + "' AND IdEstadoFlujo != '99' AND CodEstado = '0' ";
        sql += " and ArriboCarga = 0 ";
        sql += " ORDER BY 3 ";
        this.loadSQL(sql);
    }

    public void loadInstruccionXAduanaFinalizadas(string codAduana)
    {
        string sql = " Select * from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = 1 AND DI.Eliminado = '0') ";
        sql += " WHERE CodEstado = '0' AND CodigoAduana = '" + codAduana + "' AND IdEstadoFlujo = '99' AND CodEstado = '0' AND I.IdInstruccion NOT IN (SELECT Documento FROM HistorialImpresiones WHERE Eliminado = '0') ";
        sql += "ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionXAduanaUsuario(string codAduana, string idUsuario)
    {
        string sql = " SELECT DISTINCT I.IdInstruccion, I.*, C.Nombre, DI.DocumentoNo from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = 0) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1' AND DI.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0','1') AND I.CodigoAduana = '" + codAduana + "' AND I.IdUsuarioAduana = '" + idUsuario + "' AND I.IdEstadoFlujo != '99' ";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionXUsuario(string idUsuario)
    {
        string sql = " SELECT DISTINCT I.IdInstruccion, I.*, C.Nombre, DI.DocumentoNo from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente AND C.Eliminado = 0) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1' AND DI.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0','1') AND I.IdUsuarioAduana = '" + idUsuario + "' AND I.IdEstadoFlujo != '99' ";
        sql += " ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionXAduanaUsuarioTemp(string codAduana, string idUsuario)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodEstado = '0' AND CodigoAduana = '" + codAduana + "' AND IdUsuarioAduana = '" + idUsuario + "' AND IdEstadoFlujo != '99' AND CodEstado = '0' ";
        sql += " and ArriboCarga = 0 ";
        sql += " ORDER BY CodigoAduana, Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionXAduanaUsuarioFinalizadas(string codAduana, string idUsuario)
    {
        string sql = " Select * from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = 1 AND DI.Eliminado = '0') ";
        sql += " WHERE CodEstado = '0' AND CodigoAduana = '" + codAduana + "' AND IdUsuarioAduana = '" + idUsuario + "' AND IdEstadoFlujo = '99' AND I.IdInstruccion NOT IN (SELECT Documento FROM HistorialImpresiones WHERE Eliminado = '0') ";
        sql += "ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionXUsuarioFinalizadas(string idUsuario)
    {
        string sql = " Select * from Instrucciones I ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        sql += " LEFT JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = 1 AND DI.Eliminado = '0') ";
        sql += " WHERE CodEstado = '0' AND IdUsuarioAduana = '" + idUsuario + "' AND IdEstadoFlujo = '99' AND I.IdInstruccion NOT IN (SELECT Documento FROM HistorialImpresiones WHERE Eliminado = '0') ";
        sql += "ORDER BY I.CodigoAduana, I.Item ";
        this.loadSQL(sql);
    }

    public void loadInstruccionXTramite(string idTramite)
    {
        string sql = this.coreSQL;
        sql += " WHERE CodEstado = '0' AND IdTramite = '" + idTramite + "' ";
        this.loadSQL(sql);
    }

    public void loadMaxInstruccionXAduana(string codAduana)
    {
        string s = " select Top 1 MaxIdInstruccion=substring(IdInstruccion,charindex('-',IdInstruccion,charindex('-',IdInstruccion)+1)+1 , len(IdInstruccion)) + 1 from Instrucciones ";
        s = s + " where CodigoAduana = '" + codAduana + "' order by substring(IdInstruccion,charindex('-',IdInstruccion,charindex('-',IdInstruccion)+1)+1 , len(IdInstruccion)) + 1  desc";
        loadSQL(s);
    }

    public void loadNuevoId(string codSF)
    {
        string sql = " declare @Id varchar(50) ";
        sql += " select @Id = (SELECT NuevoId = MAX(CAST(LEFT(IdInstruccion,4)AS varchar(5))) + ";
        sql += " RIGHT('' + CAST(MAX(CAST(RIGHT(IdInstruccion,LEN(IdInstruccion)-4)+ 1 AS int))AS varchar(50)),50) ";
        sql += " FROM Instrucciones ";
        sql += " Where IdInstruccion like '" + codSF + "%') ";
        sql += " select @Id as NuevoId ";
        this.loadSQL(sql);
    }

    public void ejecutarSPDatosInstruccion(string idInstruccion)
    {
        string sql = " exec spDatosInstruccion '" + idInstruccion + "' ";
        this.loadSQL(sql);
    }

    public void loadInstruccionBusquedaAvanzada(string campos, string criterio, string opcion, string codPais, string codAduana)
    {
        string sql = " SELECT DISTINCT I.IdInstruccion" + campos;
        sql += " FROM Instrucciones I ";
        sql += " INNER JOIN Clientes CL ON (CL.CodigoSAP = I.IdCliente AND CL.Eliminado = '0') ";
        sql += " INNER JOIN Aduanas A ON (A.CodigoAduana = I.CodigoAduana AND A.CodEstado = '0') ";
        sql += " LEFT JOIN DocumentosInstrucciones DI1 ON (DI1.IdInstruccion = I.IdInstruccion AND DI1.CodDocumento = '1' AND DI1.Eliminado ='0') ";
        sql += " LEFT JOIN DocumentosInstrucciones DI2 ON (DI2.IdInstruccion = I.IdInstruccion AND DI2.CodDocumento = I.CodDocumentoEmbarque AND DI2.Eliminado ='0') ";
        sql += " LEFT JOIN EspeciesFiscalesInstrucciones EFI ON (EFI.IdInstruccion = I.IdInstruccion AND (EFI.CodEspecieFiscal = 'F' OR EFI.CodEspecieFiscal = 'P') AND EFI.Eliminado ='0') ";
        sql += " INNER JOIN Codigos C ON (C.Codigo = I.CodPaisHojaRuta AND C.Categoria = 'PAISES') ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = I.CodRegimen AND C1.Categoria = 'REGIMEN' + C.Descripcion) ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = I.CodDocumentoEmbarque AND C2.Categoria = 'DOCUMENTOEMBARQUE') ";
        sql += " INNER JOIN Codigos C3 ON (C3.Codigo = I.CodPaisOrigen AND C3.Categoria = 'PAISES') ";
        sql += " LEFT JOIN Eventos E ON (E.IdInstruccion = I.IdInstruccion AND E.IdEvento = '1') ";
        sql += " LEFT JOIN FlujoCarga FC ON (FC.CodRegimen = I.CodRegimen AND FC.CodPais = I.CodPaisHojaRuta AND FC.CodEstadoFlujoCarga = '6' AND FC.Eliminado = '0') ";
        sql += " LEFT JOIN GestionCarga GC ON (GC.IdInstruccion = I.IdInstruccion AND GC.IdEstadoFlujo = FC.IdEstadoFlujo AND GC.Eliminado = '0') ";
        sql += " LEFT JOIN GestionCarga GC1 ON (GC1.IdInstruccion = I.IdInstruccion AND GC1.IdEstadoFlujo = '0' AND GC1.Eliminado = '0') ";
        sql += " LEFT JOIN FlujoCarga FC2 ON (FC2.IdEstadoFlujo = I.IdEstadoFlujo AND FC2.Eliminado = '0') ";
        sql += " LEFT JOIN FlujoCarga FC3 ON (FC3.CodRegimen = I.CodRegimen AND FC3.CodPais = I.CodPaisHojaRuta AND FC3.CodEstadoFlujoCarga = '1' AND FC3.Eliminado = '0') ";
        sql += " LEFT JOIN GestionCarga GC3 ON (GC3.IdInstruccion = I.IdInstruccion AND GC3.IdEstadoFlujo = FC3.IdEstadoFlujo AND GC3.Eliminado = '0') ";
        sql += " LEFT JOIN FlujoCarga FC4 ON (FC4.CodRegimen = I.CodRegimen AND FC4.CodPais = I.CodPaisHojaRuta AND FC4.CodEstadoFlujoCarga = '2' AND FC4.Eliminado = '0') ";
        sql += " LEFT JOIN GestionCarga GC4 ON (GC4.IdInstruccion = I.IdInstruccion AND GC4.IdEstadoFlujo = FC4.IdEstadoFlujo AND GC4.Eliminado = '0') ";
        sql += " LEFT JOIN FlujoCarga FC5 ON (FC5.CodRegimen = I.CodRegimen AND FC5.CodPais = I.CodPaisHojaRuta AND FC5.CodEstadoFlujoCarga = '3' AND FC5.Eliminado = '0') ";
        sql += " LEFT JOIN GestionCarga GC5 ON (GC5.IdInstruccion = I.IdInstruccion AND GC5.IdEstadoFlujo = FC5.IdEstadoFlujo AND GC5.Eliminado = '0') ";
        sql += " LEFT JOIN FlujoCarga FC6 ON (FC6.CodRegimen = I.CodRegimen AND FC6.CodPais = I.CodPaisHojaRuta AND FC6.CodEstadoFlujoCarga = '4' AND FC6.Eliminado = '0') ";
        sql += " LEFT JOIN GestionCarga GC6 ON (GC6.IdInstruccion = I.IdInstruccion AND GC6.IdEstadoFlujo = FC6.IdEstadoFlujo AND GC6.Eliminado = '0') ";
        sql += " LEFT JOIN FlujoCarga FC7 ON (FC7.CodRegimen = I.CodRegimen AND FC7.CodPais = I.CodPaisHojaRuta AND FC7.CodEstadoFlujoCarga = '5' AND FC7.Eliminado = '0') ";
        sql += " LEFT JOIN GestionCarga GC7 ON (GC7.IdInstruccion = I.IdInstruccion AND GC7.IdEstadoFlujo = FC7.IdEstadoFlujo AND GC7.Eliminado = '0') ";
        sql += " WHERE I.CodEstado IN ('0','1') AND " + criterio;
        if (opcion == "Por Pais")
            sql += " AND I.CodPaisHojaRuta = '" + codPais + "' ";
        else if (opcion == "Por Aduana")
            sql += " AND I.CodPaisHojaRuta = '" + codPais + "' AND I.CodigoAduana = '" + codAduana + "' ";
        this.loadSQL(sql);
    }

    //public void loadInstruccionEmbarqueDiario(string fechaInicial, string fechaFinal, string codCliente, string codPais)
    //{
    //    string sql = " SELECT I.CodigoAduana, A.NombreAduana, I.IdInstruccion, I.ReferenciaCliente, ";
    //    sql += " CASE WHEN I.CodPaisHojaRuta != 'S' THEN I.NoCorrelativo + '/' + EFI.Serie ELSE I.NoCorrelativo END Correlativo, ";
    //    sql += " I.Producto, C2.Descripcion AS Regimen, DI.DocumentoNo AS NumFactura, CONVERT(Varchar,E.FechaInicio,101) FechaEntrada, ";
    //    sql += " CONVERT(Varchar,E.FechaInicio,108) HoraEntrada, C3.Descripcion AS Procedencia, CiudadDestino, ";
    //    sql += " DGD.Motorista, I.Impuesto, CONVERT(Varchar,GC.Fecha,108) HoraSalida, ";
    //    sql += " CASE WHEN I.Impuesto > 0 THEN 'IMPUESTO' ELSE 'LIBRE DE IMPUESTO' END Observaciones, ";
    //    sql += " C.Nombre, I.* FROM Instrucciones I ";
    //    sql += " INNER JOIN Aduanas A ON (A.CodigoAduana = I.CodigoAduana) ";
    //    if (codPais == "H")
    //        sql += " INNER JOIN EspeciesFiscalesInstrucciones EFI ON (EFI.IdInstruccion = I.IdInstruccion AND CodEspecieFiscal IN ('P','F')) ";
    //    else
    //        sql += " LEFT JOIN EspeciesFiscalesInstrucciones EFI ON (EFI.IdInstruccion = I.IdInstruccion AND CodEspecieFiscal IN ('P','F')) ";
    //    sql += " INNER JOIN Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'PAISES') ";
    //    sql += " INNER JOIN Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) ";
    //    sql += " INNER JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1') ";
    //    sql += " INNER JOIN Eventos E ON (E.IdInstruccion = I.IdInstruccion AND E.IdEvento = '1') ";
    //    sql += " INNER JOIN Codigos C3 ON (C3.Codigo = I.CodPaisProcedencia AND C3.Categoria = 'PAISES') ";
    //    sql += " LEFT JOIN DatosGestionDocumentos DGD ON (DGD.IdInstruccion = I.IdInstruccion) ";
    //    sql += " INNER JOIN FlujoCarga FC ON (FC.CodRegimen = I.CodRegimen AND FC.CodEstadoFlujoCarga = '6' AND FC.CodPais = I.CodPaisHojaRuta) ";
    //    sql += " INNER JOIN GestionCarga GC ON (GC.IdInstruccion = I.IdInstruccion AND GC.IdEstadoFlujo = FC.IdEstadoFlujo) ";
    //    sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente) ";
    //    //sql += " WHERE I.IdCliente = '" + codCliente + "' AND I.CodEstado = '0' AND CONVERT(Varchar,GC.Fecha,111) BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + "' ";
    //    sql += " WHERE I.IdCliente = '" + codCliente + "' AND I.CodEstado = '0' AND CONVERT(Varchar,E.FechaInicio,111) BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + "' ";
    //    sql += " ORDER BY FechaEntrada, HoraEntrada ";
    //    this.loadSQL(sql);
    //}

    //reporte embarque con motorista
    public void loadInstruccionEmbarqueDiario(string fechaInicial, string fechaFinal, string codCliente, string codPais)
    {
        string sql = " SELECT I.CodigoAduana, I.IdInstruccion, I.ReferenciaCliente, A.NombreAduana, ";
        sql += " CASE WHEN I.CodPaisHojaRuta != 'S' THEN I.NoCorrelativo + '/' + EFI.Serie ELSE I.NoCorrelativo END Correlativo, ";
        sql += " I.Producto, C2.Descripcion AS Regimen, DI.DocumentoNo AS NumFactura, CONVERT(Varchar,E.FechaInicio,101) FechaEntrada, ";
        sql += " CONVERT(Varchar,E.FechaInicio,108) HoraEntrada, C3.Descripcion AS Procedencia, I.Proveedor, CiudadDestino, ";
        //sql += " DGD.Motorista,  ";
        sql += " (SELECT Motorista FROM [AduanasNueva].[dbo].DatosGestionDocumentos WHERE IdInstruccion = I.IdInstruccion) AS Motorista, ";
        //sql += " '' AS Motorista, ";
        sql += " I.Impuesto, CONVERT(Varchar,GC.Fecha,108) HoraSalida, ";
        sql += " CASE WHEN I.Impuesto > 0 THEN 'IMPUESTO' ELSE 'LIBRE DE IMPUESTO' END Observaciones, ";
        sql += " C.Nombre, I.ValorFOB FROM [AduanasNueva].[dbo].Instrucciones I ";
        sql += " INNER JOIN [AduanasNueva].[dbo].Aduanas A ON (A.CodigoAduana = I.CodigoAduana) ";
        if (codPais == "H")
            sql += " INNER JOIN [AduanasNueva].[dbo].EspeciesFiscalesInstrucciones EFI ON (EFI.IdInstruccion = I.IdInstruccion AND CodEspecieFiscal IN ('P','F')) ";
        else
            sql += " LEFT JOIN [AduanasNueva].[dbo].EspeciesFiscalesInstrucciones EFI ON (EFI.IdInstruccion = I.IdInstruccion AND CodEspecieFiscal IN ('P','F')) ";
        sql += " INNER JOIN [AduanasNueva].[dbo].Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'PAISES') ";
        sql += " INNER JOIN [AduanasNueva].[dbo].Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) ";
        sql += " INNER JOIN [AduanasNueva].[dbo].DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1') ";
        sql += " INNER JOIN [AduanasNueva].[dbo].Eventos E ON (E.IdInstruccion = I.IdInstruccion AND E.IdEvento = '1') ";
        sql += " INNER JOIN [AduanasNueva].[dbo].Codigos C3 ON (C3.Codigo = I.CodPaisProcedencia AND C3.Categoria = 'PAISES') ";
        //sql += " LEFT JOIN [AduanasNueva].[dbo].DatosGestionDocumentos DGD ON (DGD.IdInstruccion = I.IdInstruccion) ";
        sql += " INNER JOIN [AduanasNueva].[dbo].FlujoCarga FC ON (FC.CodRegimen = I.CodRegimen AND FC.CodEstadoFlujoCarga = '6' AND FC.CodPais = I.CodPaisHojaRuta) ";
        sql += " INNER JOIN [AduanasNueva].[dbo].GestionCarga GC ON (GC.IdInstruccion = I.IdInstruccion AND GC.IdEstadoFlujo = FC.IdEstadoFlujo) ";
        sql += " INNER JOIN [AduanasNueva].[dbo].Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        //sql += " WHERE I.IdCliente = '" + codCliente + "' AND I.CodEstado = '0' AND CONVERT(Varchar,GC.Fecha,111) BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + "' ";
        sql += " WHERE I.IdCliente = '" + codCliente + "' AND I.CodEstado = '0' AND CONVERT(Varchar,E.FechaInicio,111) BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + "' ";
        sql += " ORDER BY FechaEntrada, HoraEntrada ";
        this.loadSQL(sql);
    }

    //reporte embarque sin motorista
    public void loadInstruccionEmbarqueDiarioSM(string fechaInicial, string fechaFinal, string codCliente, string codPais)
    {
        string sql = " SELECT I.CodigoAduana, I.IdInstruccion, I.ReferenciaCliente, A.NombreAduana, ";
        sql += " CASE WHEN I.CodPaisHojaRuta != 'S' THEN I.NoCorrelativo + '/' + EFI.Serie ELSE I.NoCorrelativo END Correlativo, ";
        sql += " I.Producto, C2.Descripcion AS Regimen, DI.DocumentoNo AS NumFactura, CONVERT(Varchar,E.FechaInicio,101) FechaEntrada, ";
        sql += " CONVERT(Varchar,E.FechaInicio,108) HoraEntrada, C3.Descripcion AS Procedencia, I.Proveedor, CiudadDestino, ";
        //sql += " DGD.Motorista,  ";
        //sql += " (SELECT Motorista FROM DatosGestionDocumentos WHERE IdInstruccion = I.IdInstruccion) AS Motorista, ";
        sql += " '' AS Motorista, ";
        sql += " I.Impuesto, CONVERT(Varchar,GC.Fecha,108) HoraSalida, ";
        sql += " CASE WHEN I.Impuesto > 0 THEN 'IMPUESTO' ELSE 'LIBRE DE IMPUESTO' END Observaciones, ";
        sql += " C.Nombre FROM Instrucciones I ";
        sql += " INNER JOIN Aduanas A ON (A.CodigoAduana = I.CodigoAduana) ";
        if (codPais == "H")
            sql += " INNER JOIN EspeciesFiscalesInstrucciones EFI ON (EFI.IdInstruccion = I.IdInstruccion AND CodEspecieFiscal IN ('P','F')) ";
        else
            sql += " LEFT JOIN EspeciesFiscalesInstrucciones EFI ON (EFI.IdInstruccion = I.IdInstruccion AND CodEspecieFiscal IN ('P','F')) ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'PAISES') ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) ";
        sql += " INNER JOIN DocumentosInstrucciones DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1') ";
        sql += " INNER JOIN Eventos E ON (E.IdInstruccion = I.IdInstruccion AND E.IdEvento = '1') ";
        sql += " INNER JOIN Codigos C3 ON (C3.Codigo = I.CodPaisProcedencia AND C3.Categoria = 'PAISES') ";
        //sql += " LEFT JOIN DatosGestionDocumentos DGD ON (DGD.IdInstruccion = I.IdInstruccion) ";
        sql += " INNER JOIN FlujoCarga FC ON (FC.CodRegimen = I.CodRegimen AND FC.CodEstadoFlujoCarga = '6' AND FC.CodPais = I.CodPaisHojaRuta) ";
        sql += " INNER JOIN GestionCarga GC ON (GC.IdInstruccion = I.IdInstruccion AND GC.IdEstadoFlujo = FC.IdEstadoFlujo) ";
        sql += " INNER JOIN Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        //sql += " WHERE I.IdCliente = '" + codCliente + "' AND I.CodEstado = '0' AND CONVERT(Varchar,GC.Fecha,111) BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + "' ";
        sql += " WHERE I.IdCliente = '" + codCliente + "' AND I.CodEstado = '0' AND CONVERT(Varchar,E.FechaInicio,111) BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + "' ";
        sql += " ORDER BY FechaEntrada, HoraEntrada ";
        this.loadSQL(sql);
    }

    //reporte embarque de Vista com motorista
    public void loadInstruccionesEmbarqueVista(string fechaInicial, string fechaFinal, string codCliente, string pais)
    {
        string sql = " SELECT DISTINCT [IdInstruccion],[CodigoAduana],[ReferenciaCliente],CodRegimen,[NombreAduana],[Motorista],[IdCliente], ";
        sql += " [CodEstado],[FechaInicio],[Descripcion] AS Regimen,[Correlativo],[Producto],[DocumentoNo] AS NumFactura,FechaEntrada, ";
        sql += " HoraEntrada,Procedencia,Proveedor,CiudadDestino,Impuesto,HoraSalida,Observaciones,Nombre ";
        sql += " FROM [AduanasNueva].[dbo].[viewReporteEmbarque] ";
        sql += " WHERE IdCliente = '" + codCliente + "' AND CodEstado = '0' AND CONVERT(Varchar,FechaInicio,111) BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + "' ";
        sql += " AND Categoria like '%" + pais + "' ";
        sql += " ORDER BY FechaEntrada, HoraEntrada ";
        this.loadSQL(sql);
    }

    public void loadInstruccionGestioonCarga(string fechaInicial, string fechaFinal, string codCliente)
    {
        string sql = " SELECT DISTINCT(I.IdInstruccion), CL.Nombre, I.Producto, DI.DocumentoNo, GC1.Fecha AS ComienzoFlujo, EV.FechaInicio AS ArriboCarga, GC2.Fecha AS ValidaciónElectrónica, ";
        sql += " GC3.Fecha AS PagoImpuestos, GC4.Fecha AS AsignaciónCanalSelectividad, I.Color, GC5.Fecha AS RevisiónMercancía, ";
        sql += " GC6.Fecha AS EmisiónPaseSalida, GC7.Fecha AS EntregaServicio,E.FechaInicio AS Plaga FROM [AduanasNueva].[dbo].[Instrucciones] I ";
        sql += " INNER JOIN [AduanasNueva].[dbo].[Clientes] CL ON (CL.CodigoSAP = I.IdCliente) ";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[DocumentosInstrucciones] DI ON (DI.IdInstruccion = I.IdInstruccion AND DI.CodDocumento = '1') ";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[Eventos] EV ON (EV.IdInstruccion = I.IdInstruccion AND EV.IdEvento = '1') ";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC1 ON (GC1.IdInstruccion = I.IdInstruccion AND GC1.IdEstadoFlujo = '0') ";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[FlujoCarga] FC2 ON (FC2.CodRegimen = I.CodRegimen AND FC2.CodPais = I.CodPaisHojaRuta AND FC2.Eliminado = '0' AND FC2.CodEstadoFlujoCarga = '1') ";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC2 ON (GC2.IdInstruccion = I.IdInstruccion AND GC2.IdEstadoFlujo = FC2.IdEstadoFlujo) ";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[FlujoCarga] FC3 ON (FC3.CodRegimen = I.CodRegimen AND FC3.CodPais = I.CodPaisHojaRuta AND FC3.Eliminado = '0' AND FC3.CodEstadoFlujoCarga = '2') ";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC3 ON (GC3.IdInstruccion = I.IdInstruccion AND GC3.IdEstadoFlujo = FC3.IdEstadoFlujo) ";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[FlujoCarga] FC4 ON (FC4.CodRegimen = I.CodRegimen AND FC4.CodPais = I.CodPaisHojaRuta AND FC4.Eliminado = '0' AND FC4.CodEstadoFlujoCarga = '3') ";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC4 ON (GC4.IdInstruccion = I.IdInstruccion AND GC4.IdEstadoFlujo = FC4.IdEstadoFlujo) ";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[FlujoCarga] FC5 ON (FC5.CodRegimen = I.CodRegimen AND FC5.CodPais = I.CodPaisHojaRuta AND FC5.Eliminado = '0' AND FC5.CodEstadoFlujoCarga = '4') ";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC5 ON (GC5.IdInstruccion = I.IdInstruccion AND GC5.IdEstadoFlujo = FC5.IdEstadoFlujo) ";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[FlujoCarga] FC6 ON (FC6.CodRegimen = I.CodRegimen AND FC6.CodPais = I.CodPaisHojaRuta AND FC6.Eliminado = '0' AND FC6.CodEstadoFlujoCarga = '5') ";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC6 ON (GC6.IdInstruccion = I.IdInstruccion AND GC6.IdEstadoFlujo = FC6.IdEstadoFlujo) ";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[FlujoCarga] FC7 ON (FC7.CodRegimen = I.CodRegimen AND FC7.CodPais = I.CodPaisHojaRuta AND FC7.Eliminado = '0' AND FC7.CodEstadoFlujoCarga = '6') ";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[GestionCarga] GC7 ON (GC7.IdInstruccion = I.IdInstruccion AND GC7.IdEstadoFlujo = FC7.IdEstadoFlujo) ";
        sql += " LEFT JOIN [AduanasNueva].[dbo].[Eventos] E ON (E.IdInstruccion = I.IdInstruccion AND E.Eliminado = '1' AND E.IdEvento= '2')  ";
        sql += " WHERE I.IdCliente = '" + codCliente + "' AND I.IdEstadoFlujo != '0' AND CONVERT(Varchar,GC1.Fecha,111) BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + "' ";
        this.loadSQL(sql);
    }

    public void loadInstruccionReporteGerencial(string fechaInicial, string fechaFinal, string codPais, string campo, string agrupar, string opcion, string codigo1, string codigo2)//
    {
        string sql = " SELECT C1.Descripcion AS País, " + campo + ", Count(*) AS Cantidad FROM [AduanasNueva].[dbo].Instrucciones I ";
        sql += " INNER JOIN [AduanasNueva].[dbo].Aduanas A ON (A.CodigoAduana = I.CodigoAduana AND A.CodEstado = '0') ";
        sql += " INNER JOIN [AduanasNueva].[dbo].Clientes C ON (C.CodigoSAP = I.IdCliente) ";
        sql += " INNER JOIN [AduanasNueva].[dbo].Codigos C1 ON (C1.Codigo = I.CodPaisHojaRuta AND C1.Categoria = 'PAISES') ";
        sql += " INNER JOIN [AduanasNueva].[dbo].Codigos C2 ON (C2.Codigo = I.CodRegimen AND C2.Categoria = 'REGIMEN' + C1.Descripcion) ";
        sql += " INNER JOIN [AduanasNueva].[dbo].Codigos C3 ON (C3.Codigo = I.CodPaisOrigen AND C3.Categoria = 'PAISES') ";
        sql += " INNER JOIN [AduanasNueva].[dbo].GestionCarga GC ON (GC.IdInstruccion = I.IdInstruccion AND GC.IdEstadoFlujo = '0' AND GC.Eliminado = '0') ";
        sql += " WHERE CONVERT(Varchar,GC.Fecha,111) BETWEEN '" + fechaInicial + "' AND '" + fechaFinal + "' ";
        sql += " AND I.CodEstado != '100' AND I.CodPaisHojaRuta = '" + codPais + "' ";
        if (opcion == "1")
            sql += " AND (A.CodigoAduana = '" + codigo1 + "' OR C.CodigoSAP = '" + codigo1 + "' OR C2.Codigo = '" + codigo1 + "') ";
        if (opcion == "2")
            sql += " AND (A.CodigoAduana = '" + codigo1 + "' OR C.CodigoSAP = '" + codigo1 + "' OR C2.Codigo = '" + codigo1 + "') AND (A.CodigoAduana = '" + codigo2 + "' OR C.CodigoSAP = '" + codigo2 + "' OR C2.Codigo = '" + codigo2 + "') ";
        sql += " GROUP BY C1.Descripcion, " + agrupar;
        sql += " ORDER BY 2 ";
        this.loadSQL(sql);
    }

    public void ReporteHagamodaPromoda(string cliente, string fechainicio, string fechafin)
    {

        string sql = " SELECT DISTINCT  I.IdInstruccion,I.Proveedor,CL.Nombre as Cliente,DI1.DocumentoNo AS Factura,DI2.DocumentoNo AS ValorDocumentoEmbarque,";
        sql += " I.CantidadBultos,I.PesoKgs,X.ObservacionFin as PesoMiami,W.ObservacionFin as IngresoSwissport,V.ObservacionFin as SalidaSwissport ,I.Producto,CONVERT(varchar,GC1.Fecha, 121)AS ComienzoFlujo,CONVERT(varchar,E.FechaInicio,121) AS ArribodeCarga, ";
        sql += " CONVERT(varchar,Z.FechaInicio,121) as ENVIODEPRELIQUIDACION,CONVERT(varchar,Z.FechaFin,121) as APROBACIONDEPRELIQUIDACION ,";
        sql += " CONVERT(varchar,Y.FechaFin,121) as ENVIODEBOLETIN ,CONVERT(varchar,GC4.Fecha,121) AS PagodeImpuestos,I.Color as Color,";
        sql += " CONVERT(varchar,GC6.Fecha,121) AS RevisionMercancia,CONVERT(varchar,GC7.Fecha,121) AS EmisionPaseSalida, CONVERT(varchar,GC8.Fecha,121) AS EntregadeServicios, U2.Nombre +' '+U2.Apellido  as Aforador,";
        sql += "CONVERT(varchar,T.FechaInicio,121) as IngresoDeposito,CONVERT(varchar,U.FechaInicio,121) as EntregaCliente,";
        sql += " 'Dias: ' + CAST(DATEDIFF(SECOND, GC1.Fecha, GC.Fecha) / 86400 AS varchar(50)) + ' Horas: ' + CAST((DATEDIFF(SECOND, GC1.Fecha, GC.Fecha) % 86400) / 3600 ";
        sql += " AS varchar(50)) + ' Minutos: ' + CAST((DATEDIFF(SECOND, GC1.Fecha, GC.Fecha) % 3600) / 60 AS varchar(50)) AS TiempoProceso,";
        sql += " 'Dias: ' +  CAST(DATEDIFF(SECOND, E.FechaInicio, GC.Fecha) / 86400 AS varchar(50)) + ' Horas: ' + CAST((DATEDIFF(SECOND, E.FechaInicio, GC.Fecha) % 86400) / 3600 ";
        sql += " AS varchar(50)) +' Minutos: ' + CAST((DATEDIFF(SECOND, E.FechaInicio, GC.Fecha) % 3600) / 60 AS varchar(50)) AS TiempoProceso2  FROM Instrucciones I  ";
        sql += " INNER JOIN Clientes CL ON (CL.CodigoSAP = I.IdCliente AND CL.Eliminado = '0') INNER JOIN Aduanas A ON (A.CodigoAduana = I.CodigoAduana AND A.CodEstado = '0')";
        sql += " LEFT JOIN DocumentosInstrucciones DI1 ON (DI1.IdInstruccion = I.IdInstruccion AND DI1.CodDocumento = '1' AND DI1.Eliminado ='0')  ";
        sql += " LEFT JOIN DocumentosInstrucciones DI2 ON (DI2.IdInstruccion = I.IdInstruccion AND  DI2.CodDocumento = I.CodDocumentoEmbarque AND DI2.Eliminado ='0') ";
        sql += " LEFT JOIN EspeciesFiscalesInstrucciones EFI ON (EFI.IdInstruccion = I.IdInstruccion  AND (EFI.CodEspecieFiscal = 'F' OR EFI.CodEspecieFiscal = 'P') ";
        sql += " AND EFI.Eliminado ='0') INNER JOIN Codigos C ON (C.Codigo = I.CodPaisHojaRuta AND C.Categoria = 'PAISES') ";
        sql += " LEFT JOIN Eventos E ON (E.IdInstruccion = I.IdInstruccion AND E.IdEvento = '1') ";
        sql += " LEFT JOIN Eventos Z ON (Z.IdInstruccion = I.IdInstruccion AND Z.IdEvento = '7') ";
        sql += " LEFT JOIN Eventos Y ON (Y.IdInstruccion = I.IdInstruccion AND Y.IdEvento = '18') ";
        sql += " LEFT JOIN Eventos X ON (X.IdInstruccion = I.IdInstruccion AND X.IdEvento = '19') ";
        sql += " LEFT JOIN Eventos W ON (W.IdInstruccion = I.IdInstruccion AND W.IdEvento = '20') ";
        sql += " LEFT JOIN Eventos V ON (V.IdInstruccion = I.IdInstruccion AND V.IdEvento = '21') ";
        sql += " LEFT JOIN Eventos T ON (T.IdInstruccion = I.IdInstruccion AND T.IdEvento = '22') ";
        sql += " LEFT JOIN Eventos U ON (U.IdInstruccion = I.IdInstruccion AND U.IdEvento = '23') ";
        sql += " LEFT JOIN FlujoCarga FC ON (FC.CodRegimen = I.CodRegimen AND FC.CodPais = I.CodPaisHojaRuta AND FC.CodEstadoFlujoCarga = '6' AND FC.Eliminado = '0') ";
        sql += " LEFT JOIN GestionCarga GC ON (GC.IdInstruccion = I.IdInstruccion AND GC.IdEstadoFlujo = FC.IdEstadoFlujo AND GC.Eliminado = '0')  ";
        sql += " LEFT JOIN GestionCarga GC1 ON (GC1.IdInstruccion = I.IdInstruccion AND GC1.IdEstadoFlujo = '0'  AND GC1.Eliminado = '0')  ";
        sql += " LEFT JOIN FlujoCarga FC2 ON (FC2.IdEstadoFlujo = I.IdEstadoFlujo AND FC2.Eliminado = '0') ";
        sql += " LEFT JOIN FlujoCarga FC3 ON (FC3.CodRegimen = I.CodRegimen AND FC3.CodPais = I.CodPaisHojaRuta AND FC3.CodEstadoFlujoCarga = '1' AND FC3.Eliminado = '0') ";
        sql += " LEFT JOIN GestionCarga GC3 ON (GC3.IdInstruccion = I.IdInstruccion AND GC3.IdEstadoFlujo = FC3.IdEstadoFlujo AND GC3.Eliminado = '0')  ";
        sql += " LEFT JOIN FlujoCarga FC4 ON (FC4.CodRegimen = I.CodRegimen AND FC4.CodPais = I.CodPaisHojaRuta AND FC4.CodEstadoFlujoCarga = '2' AND FC4.Eliminado = '0') ";
        sql += " LEFT JOIN GestionCarga GC4 ON (GC4.IdInstruccion = I.IdInstruccion AND GC4.IdEstadoFlujo = FC4.IdEstadoFlujo AND GC4.Eliminado = '0')  ";
        sql += " LEFT JOIN FlujoCarga FC5 ON (FC5.CodRegimen = I.CodRegimen AND FC5.CodPais = I.CodPaisHojaRuta AND FC5.CodEstadoFlujoCarga = '3' AND FC5.Eliminado = '0') ";
        sql += " LEFT JOIN GestionCarga GC5 ON (GC5.IdInstruccion = I.IdInstruccion AND GC5.IdEstadoFlujo = FC5.IdEstadoFlujo AND GC5.Eliminado = '0')  ";
        sql += " LEFT JOIN FlujoCarga FC6 ON (FC6.CodRegimen = I.CodRegimen AND FC6.CodPais = I.CodPaisHojaRuta AND FC6.CodEstadoFlujoCarga = '4' AND FC6.Eliminado = '0') ";
        sql += " LEFT JOIN GestionCarga GC6 ON (GC6.IdInstruccion = I.IdInstruccion AND GC6.IdEstadoFlujo = FC6.IdEstadoFlujo AND GC6.Eliminado = '0')  ";
        sql += " LEFT JOIN FlujoCarga FC7 ON (FC7.CodRegimen = I.CodRegimen AND FC7.CodPais = I.CodPaisHojaRuta AND FC7.CodEstadoFlujoCarga = '5' AND FC7.Eliminado = '0') ";
        sql += " LEFT JOIN FlujoCarga FC8 ON (FC8.CodRegimen = I.CodRegimen AND FC8.CodPais = I.CodPaisHojaRuta AND FC8.CodEstadoFlujoCarga = '6' AND FC7.Eliminado = '0')  ";
        sql += " LEFT JOIN GestionCarga GC8 ON (GC8.IdInstruccion = I.IdInstruccion AND GC8.IdEstadoFlujo = FC8.IdEstadoFlujo AND GC8.Eliminado = '0')  ";
        sql += " LEFT JOIN GestionCarga GC7 ON (GC7.IdInstruccion = I.IdInstruccion AND GC7.IdEstadoFlujo = FC7.IdEstadoFlujo AND GC7.Eliminado = '0')  ";
        sql += " INNER JOIN Usuarios U1 ON (U1.IdUsuario = I.IdUsuarioAduana) LEFT JOIN Usuarios U2 ON (U2.IdUsuario = I.Aforador)  ";
        sql += " LEFT JOIN Usuarios U3 ON (U3.IdUsuario = I.Tramitador)";
        sql += "  WHERE I.CodEstado IN ('0','1') AND CL.CodigoSAP =  '" + cliente + "' AND CONVERT(VARCHAR, I.Fecha, 111) >= '" + fechainicio + "' AND CONVERT(VARCHAR, I.Fecha, 111) <= '" + fechafin + "'";
        this.loadSQL(sql);

    }

    public void ReporteCargill(string cliente, string fechainicio, string fechafin)
    {
        string sql = " select distinct A.IdInstruccion,D.PosicionArancelaria,E.Serie, I.[Fecha],";
        sql += " case  when A.CodRegimen in('1000','1050','1051','1052','1100','2000','2100','2200','3040','3051','3052','3053','3054','3059','3070','3154','3155') then 2 else 1 end as 'Import/Export'";
        sql += " , B.Nombre as Importador ,A.Proveedor,F.Descripcion as PaisOrigen, G.Descripcion as PaisProcedencia, SUM(D.ValorCIF)as ValorenUSD,";
        sql += " case A.CodRegimen When '5100' then 0 else SUM(D.DAITotal) end as DAI,D.DAI as '%',case A.CodRegimen When '5100' then D.DAITotal else 0 end as ImpuestoExonerado ";
        sql += " ,SUM(D.ISVTotal) as ISV,Case D.Preferencia when 2 Then 'SI' else 'NO' end as Preferencia, A.CodPaisHojaRuta ";
        sql +=" ,Case When A.CodRegimen in ('4600','4070','4000') then '01' When A.CodRegimen = '7000' then '02' when A.CodRegimen in ('8000','8100') then '05' ";
        sql += " When A.CodRegimen = '1000' then '07' When A.CodRegimen = '5100' then '03' else '08' end as CodigoTransaccion, case when A.Division in (3,4,5,6) then '5515' else '5513' end as FDCNumber";
        sql +=" , '' as BUname";
        sql += " From Instrucciones A";
        sql += " Inner Join [EsquemaTramites2].[dbo].[HojaRutaPreliquidacionDet] D on (A.IdInstruccion = D.IdHojaRuta collate Modern_Spanish_CI_AI )";
        sql += " Inner Join Clientes B on (B.CodigoSAP ='" + cliente + "' and A.IdCliente = B.CodigoSAP and B.Eliminado = 0)";
        sql += " Inner Join EspeciesFiscalesInstrucciones E on (E.IdInstruccion = A.IdInstruccion and E.Eliminado = 0 and E.CodEspecieFiscal = 'P')";
        sql += " Inner Join Codigos C on (C.Categoria = 'REGIMENHONDURAS' and C.Codigo = A.CodRegimen and C.Eliminado = 0)";
        sql += " Inner Join Codigos F on (F.Categoria = 'PAISES' and A.CodPaisOrigen = F.Codigo)";
        sql += " Inner Join Codigos G on (G.Categoria = 'PAISES' and A.CodPaisProcedencia = G.Codigo)";
        sql += " inner join AduanasNueva.dbo.FlujoCarga H on ( A.codRegimen = H.[CodRegimen] and A.[CodPaisHojaRuta] = H.[CodPais] and  H.Eliminado = 0 and H.[CodEstadoFlujoCarga] = '6')";
        sql += " Inner Join [AduanasNueva].[dbo].[GestionCarga] I on ( A.IdInstruccion = I.IdInstruccion and I.IdEstadoFlujo = H.IdEstadoFlujo)";
        sql += " Where A.CodEstado != 100 and A.IdEstadoFlujo ='99' and D.PosicionArancelaria is not null AND CONVERT(VARCHAR, I.Fecha, 111) >= '" + fechainicio + "' AND CONVERT(VARCHAR, I.Fecha, 111) <= '" + fechafin + "'";
        sql += " Group by A.IdInstruccion, D.PosicionArancelaria, A.CodRegimen, E.Serie, I.Fecha, B.Nombre, A.Proveedor, F.Descripcion, G.Descripcion, D.DAITotal, D.DAI, A.CodPaisHojaRuta ,D.Preferencia";
        sql += " ,CodEspecieFiscal,A.CodRegimen,A.Division order by A.IdInstruccion ";
        this.loadSQL(sql);
    }

    public void loadDescripcionEstados(string idInstruccion)
    {
        string sql = " SELECT GC.IDEstadoFlujo AS Codigo, CASE WHEN (GC.IdEstadoFlujo = '0') THEN 'Comienzo Flujo' ELSE FC.EstadoFlujo END Descripcion FROM Instrucciones I ";
        sql += " LEFT JOIN GestionCarga GC ON (GC.IdInstruccion = I.IdInstruccion AND GC.Eliminado = '0') ";
        sql += " LEFT JOIN FlujoCarga FC ON (FC.IdEstadoFlujo = GC.IdEstadoFlujo AND FC.Eliminado = '0') ";
        sql += " WHERE I.IdInstruccion = '" + idInstruccion + "' AND I.CodEstado != '100' ";
        sql += " UNION ";
        sql += " SELECT E.IdEvento + C1.Codigo AS Codigo,  ";
        sql += " CASE WHEN (C1.Codigo = 'A') THEN C.Descripcion + ' Inicio' ELSE C.Descripcion + ' Fin' END Descripcion ";
        sql += " FROM Instrucciones I ";
        sql += " LEFT JOIN Eventos E ON (E.IdInstruccion = I.IdInstruccion) ";
        sql += " LEFT JOIN Codigos C ON (C.Codigo = E.IdEvento AND C.Categoria = 'EVENTOS') ";
        sql += " LEFT JOIN Codigos C1 ON (C1.Codigo != E.IdEvento AND C1.Categoria = 'REPORTEEVENTOS') ";
        sql += " WHERE I.IdInstruccion = '" + idInstruccion + "' AND I.CodEstado != '100' ";
        this.loadSQL(sql);
    }

    public void loadInstruccionXGuia(string codigoGuia, string codigoPais)
    {
        string sql = " SELECT * FROM Instrucciones ";
        sql += " WHERE CodigoGuia = '" + codigoGuia + "' AND CodPaisHojaRuta = '" + codigoPais + "' AND CodEstado != '100' ";
        this.loadSQL(sql);
    }

    #region Campos
    
    public string IDINSTRUCCION
    {
        get
        {
            return (string)registro[Campos.IDINSTRUCCION].ToString();
        }
        set
        {
            registro[Campos.IDINSTRUCCION] = value;
        }
    }

    public string FACTURAVESTA
    {
        get
        {
            return (string)registro[Campos.FACTURAVESTA].ToString();
        }
        set
        {
            registro[Campos.FACTURAVESTA] = value;
        }
    }

    public string FECHAENTREGACARGILL
    {
        get
        {
            return (string)registro[Campos.FECHAENTREGACARGILL].ToString();
        }
        set
        {
            registro[Campos.FECHAENTREGACARGILL] = value;
        }
    }

    public string ORDENCOMPRA
    {
        get
        {
            return (string)registro[Campos.ORDENCOMPRA].ToString();
        }
        set
        {
            registro[Campos.ORDENCOMPRA] = value;
        }
    }

    public string ETAPLANTA
    {
        get
        {
            return (string)registro[Campos.ETAPLANTA].ToString();
        }
        set
        {
            registro[Campos.ETAPLANTA] = value;
        }
    }

    public string FECHADEFACTURA2
    {
        get
        {
            return (string)registro[Campos.FECHADEFACTURA2].ToString();
        }
        set
        {
            registro[Campos.FECHADEFACTURA2] = value;
        }
    }
    public string TRACKINGCOURIER
    {
        get
        {
            return (string)registro[Campos.TRACKINGCOURIER].ToString();
        }
        set
        {
            registro[Campos.TRACKINGCOURIER] = value;
        }
    }
    public string FECHARECIBOCOPIAS
    {
        get
        {
            return (string)registro[Campos.FECHARECIBOCOPIAS].ToString();
        }
        set
        {
            registro[Campos.FECHARECIBOCOPIAS] = value;
        }
    }

    public string FECHARECIBOORIGINALES
    {
        get
        {
            return (string)registro[Campos.FECHARECIBOORIGINALES].ToString();
        }
        set
        {
            registro[Campos.FECHARECIBOORIGINALES] = value;
        }
    }

    public string FECHAARRIBOPLANTAPROCESO
    {
        get
        {
            return (string)registro[Campos.FECHAARRIBOPLANTAPROCESO].ToString();
        }
        set
        {
            registro[Campos.FECHAARRIBOPLANTAPROCESO] = value;
        }
    }

    public string TIEMPOLLEGADAHASTAENTREGA
    {
        get
        {
            return (string)registro[Campos.TIEMPOLLEGADAHASTAENTREGA].ToString();
        }
        set
        {
            registro[Campos.TIEMPOLLEGADAHASTAENTREGA] = value;
        }
    }

    public string DIASLIBRESALMACENAJE
    {
        get
        {
            return (string)registro[Campos.DIASLIBRESALMACENAJE].ToString();
        }
        set
        {
            registro[Campos.DIASLIBRESALMACENAJE] = value;
        }
    }

    public string TIEMPOENERGIA
    {
        get
        {
            return (string)registro[Campos.TIEMPOENERGIA].ToString();
        }
        set
        {
            registro[Campos.TIEMPOENERGIA] = value;
        }
    }

    public string TERMINATIEMPOLIBREDCS
    {
        get
        {
            return (string) registro[Campos.TERMINATIEMPOLIBREDCS].ToString();
        }
        set
        {
            registro[Campos.TERMINATIEMPOLIBREDCS] = value;
        }
    }

    public string CARGOALMACENAJEDCS
    {
        get
        {
            return (string)registro[Campos.CARGOALMACENAJEDCS].ToString();
        }
        set
        {
            registro[Campos.CARGOALMACENAJEDCS] = value;
        }
    }

    public string TERMINATIEMPOLIBREELECTRICIDAD
    {
        get
        {
            return (string) registro[Campos.TERMINATIEMPOLIBREELECTRICIDAD].ToString();
        }
        set
        {
            registro[Campos.TERMINATIEMPOLIBREELECTRICIDAD] = value;
        }
    }

    public string CARGODEMORAELECTRICIDAD
    {
        get
        {
            return (string) registro[Campos.CARGODEMORAELECTRICIDAD].ToString();
        }
        set
        {
            registro[Campos.CARGODEMORAELECTRICIDAD] = value;
        }
    }

    public string FECHARETORNOCONTENEDOR
    {
        get
        {
            return (string)registro[Campos.FECHARETORNOCONTENEDOR].ToString();
        }
        set
        {
            registro[Campos.FECHARETORNOCONTENEDOR] = value;
        }
    }

    public string DIASDEMORA
    {
        get
        {
            return (string)registro[Campos.DIASDEMORA].ToString();
        }
        set
        {
            registro[Campos.DIASDEMORA] = value;
        }
    }

    public string DEMORA
    {
        get
        {
            return (string)registro[Campos.DEMORA].ToString();
        }
        set
        {
            registro[Campos.DEMORA] = value;
        }
    }

    public string OBSERVACIONES
    {
        get
        {
            return (string)registro[Campos.OBSERVACIONES].ToString();
        }
        set
        {
            registro[Campos.OBSERVACIONES] = value;
        }
    }

    public string DECLARACIONSEGURO
    {
        get
        {
            return (string)registro[Campos.DECLARACIONSEGURO].ToString();
        }
        set
        {
            registro[Campos.DECLARACIONSEGURO] = value;
        }
    }

    #endregion

}
