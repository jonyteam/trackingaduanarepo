using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for DetalleRangosEspeciesFiscalesBO
/// </summary>
public class DetalleRangosEspeciesFiscalesBO : CapaBase
{
    class Campos
    {
        public static string IDDETALLERANGOESPECIEFISCAL = "IdDetalleRangoEspecieFiscal";
        public static string IDRANGOESPECIEFISCAL = "IdRangoEspecieFiscal";
        public static string DOCUMENTONO = "DocumentoNo";
        public static string COSTOUNITARIO = "CostoUnitario";
        public static string FECHAUSO = "FechaUso";
        public static string OBSERVACION = "Observacion";
        public static string CODESTADO = "CodEstado";
        public static string IDUSUARIO = "IdUsuario";
    }

    public DetalleRangosEspeciesFiscalesBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from DetalleRangoEspeciesFiscales DREF ";
        this.initializeSchema("DetalleRangoEspeciesFiscales");
    }

    public void loadDetalleRangoEspeciesFiscales()
    {
        this.loadSQL(this.coreSQL + " WHERE CodEstado = '0' ");
    }

    public void loadDetalleRangoEspeciesFiscales(string noDocumento)
    {
        string sql = this.coreSQL;
        sql += " WHERE DocumentoNo = '" + noDocumento + "' AND CodEstado = '0' ";
        this.loadSQL(sql);
    }

    public void loadDocumentoESUsado(string noDocumento)
    {
        string sql = this.coreSQL;
        sql += " WHERE DocumentoNo = '" + noDocumento + "' AND CodEstado = '1' ";
        this.loadSQL(sql);
    }

    public int cambiarEstadoDoc(string noDocumento)
    {
        string sql = " UPDATE DetalleRangoEspeciesFiscales ";
        sql += " SET FechaUso = NULL, CostoUnitario = NULL ";
        sql += " WHERE DocumentoNo = '" + noDocumento + "' ";
        return this.executeCustomSQL(sql);
    }

    public void loadDocumentoXDocAduanaLike(string noDocumento, string codAduana)
    {
        string sql = " SELECT * FROM RangosEspeciesFiscales REF ";
        sql += " INNER JOIN DetalleRangoEspeciesFiscales DREF ON (DREF.IdRangoEspecieFiscal = REF.IdRangoEspecieFiscal) ";
        sql += " WHERE DREF.DocumentoNo LIKE '" + noDocumento + "%' AND REF.CodigoAduana = '" + codAduana + "' AND DREF.CodEstado = '0' ";
        this.loadSQL(sql);
    }

    public void loadDetalleRangoEspeciesFiscalesXIdRango(string idRangoEF)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdRangoEspecieFiscal = '" + idRangoEF + "' ";
        this.loadSQL(sql);
    }

    public void loadDetalleRangoEspeciesFiscalesXIdRangoAll(string idRangoEF)
    {
        string sql = this.coreSQL;
        sql += " INNER JOIN Codigos C ON (C.Codigo = DREF.CodEstado AND C.Categoria = 'DETALLERANGOESPECIESFISCALES') ";
        sql += " INNER JOIN Usuarios U ON (U.IdUsuario = DREF.IdUsuario) ";
        sql += " WHERE IdRangoEspecieFiscal = '" + idRangoEF + "' ";
        this.loadSQL(sql);
    }

    public void loadDetRgoEspFisXIdEspFis(string idEF)
    {
        string sql = " SELECT * FROM RangosEspeciesFiscales REF ";
        sql += " INNER JOIN DetalleRangoEspeciesFiscales DREF ON (DREF.IdRangoEspecieFiscal = REF.IdRangoEspecieFiscal) ";
        sql += " WHERE REF.IdEspecieFiscal = '" + idEF + "' AND DREF.CodEstado = '0' ";
        this.loadSQL(sql);
    }

    public void loadDetalleRangoEspeciesFiscalesXIdRangoUS(string idRangoEF)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdRangoEspecieFiscal = '" + idRangoEF + "' AND CodEstado = '1' ";
        this.loadSQL(sql);
    }

    public void loadDocumentoXEFAduana(string idEspecieFiscal, string codAduana)
    {
        string sql = " SELECT * FROM RangosEspeciesFiscales REF ";
        sql += " INNER JOIN DetalleRangoEspeciesFiscales DREF ON (DREF.IdRangoEspecieFiscal = REF.IdRangoEspecieFiscal) ";
        sql += " WHERE REF.IdEspecieFiscal = '" + idEspecieFiscal + "' AND REF.CodigoAduana = '" + codAduana + "' AND DREF.CodEstado = '0' ";
        this.loadSQL(sql);
    }

    #region campos
    public string IDDETALLERANGOESPECIEFISCAL
    {
        get
        {
            return (string)registro[Campos.IDDETALLERANGOESPECIEFISCAL].ToString();
        }
    }

    public string IDRANGOESPECIEFISCAL
    {
        get
        {
            return (string)registro[Campos.IDRANGOESPECIEFISCAL].ToString();
        }
        set
        {
            registro[Campos.IDRANGOESPECIEFISCAL] = value;
        }
    }

    public string DOCUMENTONO
    {
        get
        {
            return (string)registro[Campos.DOCUMENTONO].ToString();
        }
        set
        {
            registro[Campos.DOCUMENTONO] = value;
        }
    }

    public double COSTOUNITARIO
    {
        get
        {
            return double.Parse(registro[Campos.COSTOUNITARIO].ToString());
        }
        set
        {
            registro[Campos.COSTOUNITARIO] = value;
        }
    }

    public string FECHAUSO
    {
        get
        {
            return (string)registro[Campos.FECHAUSO].ToString();
        }
        set
        {
            registro[Campos.FECHAUSO] = value;
        }
    }

    public string OBSERVACION
    {
        get
        {
            return (string)registro[Campos.OBSERVACION].ToString();
        }
        set
        {
            registro[Campos.OBSERVACION] = value;
        }
    }

    public string CODESTADO
    {
        get
        {
            return (string)registro[Campos.CODESTADO].ToString();
        }
        set
        {
            registro[Campos.CODESTADO] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }
    #endregion
}
