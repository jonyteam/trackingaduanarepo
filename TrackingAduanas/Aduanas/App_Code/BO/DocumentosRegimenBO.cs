using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for DocumentosRegimenBO
/// </summary>
public class DocumentosRegimenBO : CapaBase
{
    class Campos
    {
        public static string CODREGIMEN = "CodRegimen";
        public static string CODDOCUMENTO = "CodDocumento";
        public static string CODPAIS = "CodPais";
        public static string FECHA = "Fecha";
        public static string ELIMINADO = "Eliminado";
        public static string IDUSUARIO = "IdUsuario";
    }

    public DocumentosRegimenBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from DocumentosRegimen DR ";
        this.initializeSchema("DocumentosRegimen");
    }

    public void loadDocumentosRegimen()
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' ";
        this.loadSQL(sql);
    }

    public void loadDocumentosRegimenALL()
    {
        string sql = " Select DR.*, C.Descripcion AS Pais, C1.Descripcion AS Regimen, C2.Descripcion AS Documento from DocumentosRegimen DR ";
        sql += " INNER JOIN Codigos C ON (C.Codigo = DR.CodPais AND C.Categoria = 'PAISES') ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = DR.CodRegimen AND C1.Categoria = 'REGIMEN' + C.Descripcion) ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = DR.CodDocumento AND C2.Categoria = 'TIPODOCUMENTO') ";
        sql += " WHERE DR.Eliminado = '0' ";
        this.loadSQL(sql);
    }

    public void loadDocumentosRegimenALLXCodRegimen(string codPais, string codRegimen)
    {
        string sql = " Select DR.*, C.Descripcion AS Pais, C1.Descripcion AS Regimen, C2.Descripcion AS Documento, C2.Codigo AS CodDocumento from DocumentosRegimen DR ";
        sql += " INNER JOIN Codigos C ON (C.Codigo = DR.CodPais AND C.Categoria = 'PAISES') ";
        sql += " INNER JOIN Codigos C1 ON (C1.Codigo = DR.CodRegimen AND C1.Categoria = 'REGIMEN' + C.Descripcion) ";
        sql += " INNER JOIN Codigos C2 ON (C2.Codigo = DR.CodDocumento AND C2.Categoria = 'TIPODOCUMENTO') ";
        sql += " WHERE DR.Eliminado = '0' AND DR.CodRegimen = '" + codRegimen + "' AND DR.CodPais = '" + codPais + "' ";
        this.loadSQL(sql);
    }

    public void loadDocumentosXRegimen(string codRegimen)
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' AND CodRegimen = '" + codRegimen + "' ";
        this.loadSQL(sql);
    }

    public void loadDocumentosXRegimenDoc(string codRegimen, string codDocumento)
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' AND CodRegimen = '" + codRegimen + "' AND CodDocumento = '" + codDocumento + "' ";
        this.loadSQL(sql);
    }

    public void loadDocumentosXRegimenPais(string codRegimen, string codPais)
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' AND CodRegimen = '" + codRegimen + "' AND CodPais = '" + codPais + "' ";
        this.loadSQL(sql);
    }

    #region Campos
    public string CODREGIMEN
    {
        get
        {
            return (string)registro[Campos.CODREGIMEN].ToString();
        }
        set
        {
            registro[Campos.CODREGIMEN] = value;
        }
    }

    public string CODDOCUMENTO
    {
        get
        {
            return (string)registro[Campos.CODDOCUMENTO].ToString();
        }
        set
        {
            registro[Campos.CODDOCUMENTO] = value;
        }
    }

    public string CODPAIS
    {
        get
        {
            return (string)registro[Campos.CODPAIS].ToString();
        }
        set
        {
            registro[Campos.CODPAIS] = value;
        }
    }

    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }

    public string ELIMINADO
    {
        get
        {
            return (string)registro[Campos.ELIMINADO].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }
    #endregion
}
