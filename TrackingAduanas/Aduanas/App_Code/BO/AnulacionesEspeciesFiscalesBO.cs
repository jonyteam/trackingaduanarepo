using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for AnulacionesEspeciesFiscalesBO
/// </summary>
public class AnulacionesEspeciesFiscalesBO : CapaBase
{
    class Campos
    {
        public static string IDESPECIEFISCAL = "IdEspecieFiscal";
        public static string CODPAIS = "CodPais";
        public static string CODIGOADUANA = "CodigoAduana";
        public static string SERIE = "Serie";
        public static string FECHA = "Fecha";
        public static string CODRAZONANULACION = "CodRazonAnulacion";
        public static string OBSERVACION = "Observacion";
        public static string IDUSUARIO = "IdUsuario";
        public static string IDMOVIMIENTO = "IdMovimiento";
        public static string CARGA = "Carga";
        public static string FECHACARGA = "FechaCarga";
    }

    public AnulacionesEspeciesFiscalesBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from AnulacionesEspeciesFiscales AEF ";
        this.initializeSchema("AnulacionesEspeciesFiscales");
    }

    public void loadAnulacionesEspeciesFiscales(string idEspecieFiscal)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdEspecieFiscal = '" + idEspecieFiscal + "' ";
        this.loadSQL(sql);
    }

    public void CargaUso()
    {
        string sql = @"SELECT Distinct Convert(Varchar,A.Fecha,104) as 'Fec.docu.' ,'SA' as 'Clase Dto.','9095' as Sociedad,Convert(Varchar,GETDATE(),104) as 'Fec.Con.','10' as Periodo 
         ,'HNL' as Moneda,'1000010' as 'No.Dto.','Uso de Especie' as Referencia,'Uso Especie Fiscal '+B.NombreAduana as TxtCabDto ,'9095' as 'Divi.', '40' as CLAVE,'50' as CLAVE2, '6230036' as Cuenta, C.Codigo as Cuenta2 
         ,ISNULL(Round((TotalCompra/Convert(Money,Cantidad)),2),0) as ImporteMD, E.Codigo as CeCo, F.Descripcion +' '+G.Nombre+' '+G.Apellido as Texto, Case A.IdEspecieFiscal When 'SSB' Then 'SS' Else A.IdEspecieFiscal End  as  IdEspecieFiscal 
         ,A.[CodPais],A.[CodigoAduana],[Serie],A.[Fecha],[CodRazonAnulacion],[Observacion],A.[IdUsuario],[IdMovimiento],A.[Carga],A.[FechaCarga] 
         From [AduanasNueva].[dbo].[AnulacionesEspeciesFiscales] A 
         Inner Join Aduanas B on ( A.CodigoAduana = B.CodigoAduana and B.CodEstado = 0 And  A.CodPais = B.CodPais) 
         Inner Join Codigos C on (C.Categoria = 'INVENTARIO' and C.Descripcion = B.NombreAduana and C.Eliminado = 0) 
         Inner Join ComprasEspeciesFiscales D on ((Convert(Int,A.Serie )>= Convert(Int,D.RangoInicial) and Convert(Int,A.Serie) <= Convert(Int,D.RangoFinal)) 
         and D.CodEstado = 0 and A.CodPais = D.CodPais and A.IdEspecieFiscal = D.IdEspecieFiscal) 
         Inner Join Codigos E on (E.Categoria = 'COSTO POR SERVICIOS' and E.Descripcion = A.CodigoAduana and E.Eliminado = 0) 
         Inner Join Codigos F on (F.Categoria = 'RAZONANULACION' and F.Codigo = A.CodRazonAnulacion and F.Eliminado = 0) 
         Inner Join Usuarios G on ( G.IdUsuario = A.IdUsuario and G.Eliminado = 0) 
         Where A.Carga = 0 and A.Fecha >= '2014-08-23'";
        this.loadSQL(sql);
    }
    public void CargaUsoCambiaEstado()
    {
        string sql = "Update AnulacionesEspeciesFiscales Set Carga = '1', FechaCarga = GetDate()";
        sql += " From AnulacionesEspeciesFiscales A ";
        sql += " Inner Join Aduanas B on ( A.CodigoAduana = B.CodigoAduana and B.CodEstado = 0 And  A.CodPais = B.CodPais) ";
        sql += " Inner Join Codigos C on (C.Categoria = 'INVENTARIO' and C.Descripcion = B.NombreAduana and C.Eliminado = 0) ";
        sql += " Inner Join ComprasEspeciesFiscales D on ((Convert(Int,A.Serie )>= Convert(Int,D.RangoInicial) and Convert(Int,A.Serie) <= Convert(Int,D.RangoFinal)) ";
        sql += " and D.CodEstado = 0 and A.CodPais = D.CodPais and A.IdEspecieFiscal = D.IdEspecieFiscal) ";
        sql += " Inner Join Codigos E on (E.Categoria = 'COSTO POR SERVICIOS' and E.Descripcion = A.CodigoAduana and E.Eliminado = 0) ";
        sql += " Inner Join Codigos F on (F.Categoria = 'RAZONANULACION' and F.Codigo = A.CodRazonAnulacion and F.Eliminado = 0) ";
        sql += " Inner Join Usuarios G on ( G.IdUsuario = A.IdUsuario and G.Eliminado = 0) ";
        sql += " Where A.Carga = 0 and A.Fecha >= '2014-08-14' ";
        this.loadSQL(sql);
    }
    public void LoadAnulacionesXFecha(string FechaInicio, string FechaFin)
    {
        string sql = @"Select Distinct AEF.Serie,A.NombreAduana,AEF.IdEspecieFiscal,C.Descripcion as RazonAnulacion,U.Nombre+' '+U.Apellido as Nombre,Fecha,Observacion
                       From AnulacionesEspeciesFiscales AEF
                       Inner Join Aduanas A on (A.CodigoAduana =AEF.CodigoAduana)
                       Inner Join Codigos C on (C.Categoria = 'RAZONANULACION' and C.Codigo = AEF.CodRazonAnulacion)
                       Inner Join Usuarios U on (AEF.IdUsuario = U.IdUsuario)
                       Where AEF.Fecha Between '" + FechaInicio + "' and '" + FechaFin + "'";
        this.loadSQL(sql);
    }
    #region campos

    public string IDESPECIEFISCAL
    {
        get
        {
            return (string)registro[Campos.IDESPECIEFISCAL].ToString();
        }
        set
        {
            registro[Campos.IDESPECIEFISCAL] = value;
        }
    }

    public string FECHACARGA
    {
        get
        {
            return (string)registro[Campos.FECHACARGA].ToString();
        }
        set
        {
            registro[Campos.FECHACARGA] = value;
        }
    }
    public string CARGA
    {
        get
        {
            return (string)registro[Campos.CARGA].ToString();
        }
        set
        {
            registro[Campos.CARGA] = value;
        }
    }

    public string CODPAIS
    {
        get
        {
            return (string)registro[Campos.CODPAIS].ToString();
        }
        set
        {
            registro[Campos.CODPAIS] = value;
        }
    }

    public string CODIGOADUANA
    {
        get
        {
            return (string)registro[Campos.CODIGOADUANA].ToString();
        }
        set
        {
            registro[Campos.CODIGOADUANA] = value;
        }
    }

    public string SERIE
    {
        get
        {
            return (string)registro[Campos.SERIE].ToString();
        }
        set
        {
            registro[Campos.SERIE] = value;
        }
    }

    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }

    public string CODRAZONANULACION
    {
        get
        {
            return (string)registro[Campos.CODRAZONANULACION].ToString();
        }
        set
        {
            registro[Campos.CODRAZONANULACION] = value;
        }
    }

    public string OBSERVACION
    {
        get
        {
            return (string)registro[Campos.OBSERVACION].ToString();
        }
        set
        {
            registro[Campos.OBSERVACION] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }

    public string IDMOVIMIENTO
    {
        get
        {
            return (string)registro[Campos.IDMOVIMIENTO].ToString();
        }
        set
        {
            registro[Campos.IDMOVIMIENTO] = value;
        }
    }
    #endregion
}
