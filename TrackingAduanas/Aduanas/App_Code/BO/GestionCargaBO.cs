using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for GestionCargaBO
/// </summary>
public class GestionCargaBO : CapaBase
{
    class Campos
    {
        public static string IDGESTIONCARGA = "IdGestionCarga";
        public static string IDINSTRUCCION = "IdInstruccion";
        public static string IDESTADOFLUJO = "IdEstadoFlujo";
        public static string FECHA = "Fecha";
        public static string FECHAINGRESO = "FechaIngreso";
        public static string OBSERVACION = "Observacion";
        public static string ELIMINADO = "Eliminado";
        public static string IDUSUARIO = "IdUsuario";
        public static string ESTADO = "Estado";
    }

    public GestionCargaBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from GestionCarga GC ";
        this.initializeSchema("GestionCarga");
    }

    public void loadGestionCarga()
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' ";
        this.loadSQL(sql);
    }

    public void loadGestionCargaXInstruccion(string idInstruccion)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdInstruccion = '" + idInstruccion + "' AND Eliminado = '0' ";
        this.loadSQL(sql);
    }

    public void loadGestionCargaXIdGestionCarga(string IdGestionCarga)
    {
        string sql = "Select * from GestionCarga GC ";
        sql += " WHERE IdGestionCarga = '" + IdGestionCarga + "' AND Eliminado = '0' ";
        this.loadSQL(sql);
    }

    public void loadGestionCargaXInstruccionEvento(string idInstruccion, string idEstadoFlujo)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdInstruccion = '" + idInstruccion + "' AND IdEstadoFlujo = '" + idEstadoFlujo + "' AND Eliminado = '0' ";
        this.loadSQL(sql);
    }

    public void loadGestionIntrucionFinalizada(string idInstruccion)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdInstruccion = '" + idInstruccion + "' AND (IdEstadoFlujo = 'H-15' or IdEstadoFlujo = 'H-6') AND Eliminado = '0' ";
        this.loadSQL(sql);
    }

    public void getMaximaFechaXInstruccion(string idIntruccion)
    {
        string sql = " SELECT MAX(Fecha) AS Fecha FROM [AduanasNueva].[dbo].[GestionCarga] ";
        sql += " WHERE IdInstruccion = '" + idIntruccion + "' ";
        this.loadSQL(sql);
    }
    public void CargaTiempoRegimen(string idIntruccion)
    {
        string sql = " SELECT [IdGestionCarga],A.[IdInstruccion],A.[IdEstadoFlujo],ISNULL(B.EstadoFlujo, 'Comienzo Flujo') as EstadoFlujo";
        sql += " ,[FechaIngreso],A.[Observacion],D.CodRegimen ,C.Nombre +' '+C.Apellido as Nombre FROM GestionCarga A";
        sql += " Inner Join Instrucciones D on (A.IdInstruccion =  D.IdInstruccion)";
        sql += " Left Join FlujoCarga B on (A.IdEstadoFlujo = B.IdEstadoFlujo) Inner Join Usuarios C on (A.IdUsuario = C.IdUsuario)";
        sql += " Where A.IdInstruccion ='" + idIntruccion + "' and A.Eliminado = 0 ";
        this.loadSQL(sql);
    }
    //////////////////////////////////Dariwn Bonilla 2015-11-26//////////////////////////////////
    public void BuscarPagoImpuesto(string IdInstruccion)
    {
        string sql = @" Select * From GestionCarga Where IdInstruccion = '" + IdInstruccion + "' and Estado = 2";
        this.loadSQL(sql);
    }
    public void getMaximaFechaXInstruccionSINIMPUESTO(string idIntruccion)
    {
        string sql = " SELECT MAX(Fecha) AS Fecha FROM [AduanasNueva].[dbo].[GestionCarga] ";
        sql += " WHERE IdInstruccion = '" + idIntruccion + "' and Estado != 2";
        this.loadSQL(sql);
    }
    /////////////////////////////////////////////////////////////////////////////////////////////
    #region campos
    public string IDINSTRUCCION
    {
        get
        {
            return (string)registro[Campos.IDINSTRUCCION].ToString();
        }
        set
        {
            registro[Campos.IDINSTRUCCION] = value;
        }
    }
    public string IDESTADOFLUJO
    {
        get
        {
            return (string)registro[Campos.IDESTADOFLUJO].ToString();
        }
        set
        {
            registro[Campos.IDESTADOFLUJO] = value;
        }
    }
    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }
    public string FECHAINGRESO
    {
        get
        {
            return (string)registro[Campos.FECHAINGRESO].ToString();
        }
        set
        {
            registro[Campos.FECHAINGRESO] = value;
        }
    }
    public string OBSERVACION
    {
        get
        {
            return (string)registro[Campos.OBSERVACION].ToString();
        }
        set
        {
            registro[Campos.OBSERVACION] = value;
        }
    }
    public string ELIMINADO
    {
        get
        {
            return (string)registro[Campos.ELIMINADO].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO] = value;
        }
    }
    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }
    public string ESTADO
    {
        get
        {
            return (string)registro[Campos.ESTADO].ToString();
        }
        set
        {
            registro[Campos.ESTADO] = value;
        }
    }
    #endregion
}
