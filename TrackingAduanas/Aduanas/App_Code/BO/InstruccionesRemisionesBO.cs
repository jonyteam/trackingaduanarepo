using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for AduanasBO
/// </summary>
public class RemisionesInstruccionesBO : CapaBase
{



    class Campos
    {

        public static string ITEM = "Item";
        public static string IDREMISION = "IdRemision";
        public static string SERIE = "Serie";
        public static string IDINSTRUCCION = "IdInstruccion";
        public static string CODADUANA = "CodAduana";
        public static string CODESTADO = "CodEstado";
        public static string RTNREMITENTE = "RtnRemitente";
        public static string RTNEMPRESA = "RtnEmpresa";
        public static string RTNMOTORISTA = "RtnMotorista";
        public static string SELLOFISCAL = "SelloFiscal";
        public static string FECHA = "Fecha";
        public static string FECHAFINTRASLADO = "FechaFinTraslado";
        public static string IDUSUARIO = "IdUsuario";
        public static string OBSERVACION = "Observacion";





    }

    public RemisionesInstruccionesBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from RemisionesInstrucciones";
        this.initializeSchema("RemisionesInstrucciones");
    }



    public void loadBuscar_Remision( string hoja)
    {
        string sql = "Select * from RemisionesInstrucciones";
        sql += " WHERE IdInstruccion='"+ hoja + "'";
        this.loadSQL(sql);
    }



    public void ActualizarRemision(string Item, string serie, string Disponibles)
    {

        string sql = "update AduanasRemision ";
        sql+=" set SerieActual='"+serie+"'";
        sql+=" ,Disponibles="+Disponibles;
        sql +=" WHERE Item =" + Item;
        this.loadSQL(sql);
    }


    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }

   
    #region campos


 







    public string CODESTADO
    {
        get
        {
            return (string)registro[Campos.CODESTADO].ToString();
        }
        set
        {
            registro[Campos.CODESTADO] = value;
        }
    }


   


    #endregion

}
