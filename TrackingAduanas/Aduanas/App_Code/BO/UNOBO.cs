using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for EventosBO
/// </summary>
public class UNOBO: CapaBase
{

    class Campos
    {
    public static string ID	="Id";
    public static string IDINSTRUCCION = "IdInstruccion";
    public static string PRODUCTO = "Producto";
    public static string ITEMAPV = "ItemAPV";
    public static string GALONES = "Galones";
    public static string FACTURA = "Factura";
    public static string FECHA = "Fecha";
    public static string IDUSUARIO = "IdUsuario";
    public static string CODESTADO = "CodEstado";
    }

    public UNOBO(GrupoLis.Login.Login log)
        : base(log, true)
	{
        this.coreSQL = "Select * from UNO U";
        this.initializeSchema("UNO");
    }

    public void VerificarProducto(string HojaRuta, string Producto)
    {
        string sql = "Select * from UNO";
        sql += " Where IdInstruccion = '" + HojaRuta + "' and Producto = '"+Producto+"' and CodEstado = 0 ";
        this.loadSQL(sql);
    }

    #region Querys
    public void loadtasacambio(string pais )
    {
        string sql = this.coreSQL;
        sql += " where Fecha = CONVERT (DATE, GETDATE()) and Pais = '" + pais + "'";
        this.loadSQL(sql);
    }
    public void loadtasa()
    {
        string sql = this.coreSQL;
        sql += " where FechaInicio = '2000-01-01'";
        this.loadSQL(sql);
    }
    public void VerificarFechaInicio(string fecha)
    {
        string sql = this.coreSQL;
        sql += " where '" + fecha + "' between FechaInicio and FechaFinal ";
        this.loadSQL(sql);
    }
    public void VerificarFechaFinal(string fecha)
    {
        string sql = this.coreSQL;
        sql += " where '" + fecha + "' between FechaInicio and FechaFinal ";
        this.loadSQL(sql);
    }
    public void CargarTasasDelYear(int year, int mes)
    {
        string sql = this.coreSQL;
        sql += " where FechaInicio >= '" + year +"-"+mes+"-01"+ "'";
        this.loadSQL(sql);
    }
    public void LoadFecha(string fechainicio, string fechafinal)
    {
        string sql = this.coreSQL;
        sql += " where Convert(varchar,FechaInicio,111) = '" + fechainicio + "' and Convert(varchar,FechaFinal,111) = '" + fechafinal + "' ";
        this.loadSQL(sql);
    }
    #endregion

    #region Campos
    public string ID
    {
        get
        {
            return (string)registro[Campos.ID].ToString();
        }
        set
        {
            registro[Campos.ID] = value;
        }
    }
    public string IDINSTRUCCION
    {
        get
        {
            return (string)registro[Campos.IDINSTRUCCION].ToString();
        }
        set
        {
            registro[Campos.IDINSTRUCCION] = value;
        }
    }
    public string PRODUCTO
    {
        get
        {
            return (string)registro[Campos.PRODUCTO].ToString();
        }
        set
        {
            registro[Campos.PRODUCTO] = value;
        }
    }
    public Double ITEMAPV
    {
        get
        {
            return Convert.ToDouble(registro[Campos.ITEMAPV].ToString());
        }
        set
        {
            registro[Campos.ITEMAPV] = value;
        }
    }
    public Double GALONES
    {
        get
        {
            return Convert.ToDouble(registro[Campos.GALONES].ToString());
        }
        set
        {
            registro[Campos.GALONES] = value;
        }
    }
    public string FACTURA
    {
        get
        {
            return (string)registro[Campos.FACTURA].ToString();
        }
        set
        {
            registro[Campos.FACTURA] = value;
        }
    }
    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }
    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }
    public string CODESTADO
    {
        get
        {
            return (string)registro[Campos.CODESTADO].ToString();
        }
        set
        {
            registro[Campos.CODESTADO] = value;
        }
    }
    #endregion
}
