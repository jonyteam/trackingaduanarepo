using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for EventosBO
/// </summary>
public class NumeracionesBO: CapaBase
{

    class Campos
    {
        public static string CODIGO = "Codigo";
        public static string NUMERACION = "Numeracion";
    }

    public NumeracionesBO(GrupoLis.Login.Login log)
        : base(log, true)
	{
        this.coreSQL = "Select * from Numeraciones N";
        this.initializeSchema("DetalleGastos");
    }

    public void loadAutorizaciones()
    {
        string sql = this.coreSQL;
        sql += " where Estado = '0'";
        this.loadSQL(sql);
    }

    public void LoadNumeraciones(string Codigo)
    {
        string sql = this.coreSQL;
        sql += " WHERE Codigo = '" + Codigo + "' ";
        this.loadSQL(sql);
    }
    public void LoadNumeraciones2(string Codigo)
    {
        string sql = "SELECT Codigo+'-'+Numeracion as Numeraciones2 FROM Numeraciones";
        sql += " WHERE Codigo = '" + Codigo + "' ";
        this.loadSQL(sql);
    }
    public void DatosReferencia(string Pais)
    {
        string sql = @"Select Codigo,Numeracion From Numeraciones 
                        where Codigo  = 'RF" + Pais + "'";
        this.loadSQL(sql);
    }

    public void DatosReferencia2(string Pais)
    {
        string sql = @"Select Codigo,Numeracion From Numeraciones 
                        where Codigo  = 'RF" + Pais + "'";
        this.loadSQL(sql);
    }
     #region Campos

    public String CODIGO
    {
        get
        {
            return Convert.ToString(registro[Campos.CODIGO]);
        }
        set
        {
            registro[Campos.CODIGO] = value;
        }
    }
    //public String NUMERACION
    //{
    //    get
    //    {
    //        return Convert.ToString(registro[Campos.NUMERACION]);
    //    }
    //    set
    //    {
    //        registro[Campos.NUMERACION] = value;
    //    }
    //}
    public double NUMERACION
    {
        get
        {
            return double.Parse(registro[Campos.NUMERACION].ToString());
        }
        set
        {
            registro[Campos.NUMERACION] = value;
        }
    }
    #endregion
}
