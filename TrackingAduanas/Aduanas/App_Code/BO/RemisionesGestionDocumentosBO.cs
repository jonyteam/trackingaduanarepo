using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for RemisionesGestionDocumentosBO
/// </summary>
public class RemisionesGestionDocumentosBO : CapaBase
{
    class Campos
    {
        public static string IDINSTRUCCION = "IdInstruccion";
        public static string IDREMISION = "IdRemision";
        public static string NOMANIFIESTO = "NoManifiesto";
        public static string TRANSPORTEREF1 = "TransporteRef1";
        public static string TRANSPORTEREF2 = "TransporteRef2";
        public static string TRANSPORTEREF3 = "TransporteRef3";
        public static string TRANSPORTEREF4 = "TransporteRef4";
        public static string SERIEPOLIZA = "SeriePoliza";
        public static string FECHA = "Fecha";
        public static string ELIMINADO = "Eliminado";
        public static string IDUSUARIO = "IdUsuario";
    }

    public RemisionesGestionDocumentosBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from RemisionesGestionDocumentos RGD ";
        this.initializeSchema("RemisionesGestionDocumentos");
    }

    public void loadRemisionesGestionDocumentos(string idInstruccion, string idRemision)
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' AND IdInstruccion = '" + idInstruccion + "' AND IdRemision = '" + idRemision + "' ";
        this.loadSQL(sql);
    }

    public void loadRemisionesGestionDocumentos(string idInstruccion)
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' AND IdInstruccion = '" + idInstruccion + "' ";
        this.loadSQL(sql);
    }

    #region Campos
    public string IDINSTRUCCION
    {
        get
        {
            return (string)registro[Campos.IDINSTRUCCION].ToString();
        }
        set
        {
            registro[Campos.IDINSTRUCCION] = value;
        }
    }

    public string IDREMISION
    {
        get
        {
            return (string)registro[Campos.IDREMISION].ToString();
        }
        set
        {
            registro[Campos.IDREMISION] = value;
        }
    }

    public string NOMANIFIESTO
    {
        get
        {
            return (string)registro[Campos.NOMANIFIESTO].ToString();
        }
        set
        {
            registro[Campos.NOMANIFIESTO] = value;
        }
    }

    public string TRANSPORTEREF1
    {
        get
        {
            return (string)registro[Campos.TRANSPORTEREF1].ToString();
        }
        set
        {
            registro[Campos.TRANSPORTEREF1] = value;
        }
    }

    public string TRANSPORTEREF2
    {
        get
        {
            return (string)registro[Campos.TRANSPORTEREF2].ToString();
        }
        set
        {
            registro[Campos.TRANSPORTEREF2] = value;
        }
    }

    public string TRANSPORTEREF3
    {
        get
        {
            return (string)registro[Campos.TRANSPORTEREF3].ToString();
        }
        set
        {
            registro[Campos.TRANSPORTEREF3] = value;
        }
    }

    public string TRANSPORTEREF4
    {
        get
        {
            return (string)registro[Campos.TRANSPORTEREF4].ToString();
        }
        set
        {
            registro[Campos.TRANSPORTEREF4] = value;
        }
    }

    public string SERIEPOLIZA
    {
        get
        {
            return (string)registro[Campos.SERIEPOLIZA].ToString();
        }
        set
        {
            registro[Campos.SERIEPOLIZA] = value;
        }
    }

    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }

    public string ELIMINADO
    {
        get
        {
            return (string)registro[Campos.ELIMINADO].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }
    #endregion
}
