using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for DatosGestionDocumentosBO
/// </summary>
public class DatosGestionDocumentosBO : CapaBase
{
    class Campos
    {
        public static string IDINSTRUCCION = "IdInstruccion";
        public static string NOMANIFIESTO = "NoManifiesto";
        public static string EMPRESA = "Empresa";
        public static string MOTORISTA = "Motorista";
        public static string PLACA = "Placa";
        public static string FURGON = "Furgon";
        public static string CABEZAL = "Cabezal";
        public static string FECHA = "Fecha";
        public static string ELIMINADO = "Eliminado";
        public static string IDUSUARIO = "IdUsuario";
        public static string CELULAR = "celular";
        public static string OBSERVACIONGENERAL = "ObservacionGeneral";
    }

    public DatosGestionDocumentosBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from DatosGestionDocumentos DGD ";
        this.initializeSchema("DatosGestionDocumentos");
    }

    public void loadDatosGestionDocumentos(string idInstruccion)
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' AND IdInstruccion = '" + idInstruccion + "' ";
        this.loadSQL(sql);
    }

    #region Campos
    public string IDINSTRUCCION
    {
        get
        {
            return (string)registro[Campos.IDINSTRUCCION].ToString();
        }
        set
        {
            registro[Campos.IDINSTRUCCION] = value;
        }
    }
      
    public string NOMANIFIESTO
    {
        get
        {
            return (string)registro[Campos.NOMANIFIESTO].ToString();
        }
        set
        {
            registro[Campos.NOMANIFIESTO] = value;
        }
    }

    public string EMPRESA
    {
        get
        {
            return (string)registro[Campos.EMPRESA].ToString();
        }
        set
        {
            registro[Campos.EMPRESA] = value;
        }
    }

    public string MOTORISTA
    {
        get
        {
            return (string)registro[Campos.MOTORISTA].ToString();
        }
        set
        {
            registro[Campos.MOTORISTA] = value;
        }
    }

    public string PLACA
    {
        get
        {
            return (string)registro[Campos.PLACA].ToString();
        }
        set
        {
            registro[Campos.PLACA] = value;
        }
    }

    public string FURGON
    {
        get
        {
            return (string)registro[Campos.FURGON].ToString();
        }
        set
        {
            registro[Campos.FURGON] = value;
        }
    }

    public string CABEZAL
    {
        get
        {
            return (string)registro[Campos.CABEZAL].ToString();
        }
        set
        {
            registro[Campos.CABEZAL] = value;
        }
    }

    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }

    public string ELIMINADO
    {
        get
        {
            return (string)registro[Campos.ELIMINADO].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }

    public string CELULAR
    {
        get
        {
            return (string)registro[Campos.CELULAR].ToString();
        }
        set
        {
            registro[Campos.CELULAR] = value;
        }
    }
    public string OBSERVACIONGENERAL
    {
        get
        {
            return (string)registro[Campos.OBSERVACIONGENERAL].ToString();
        }
        set
        {
            registro[Campos.OBSERVACIONGENERAL] = value;
        }
    }
    #endregion
}
