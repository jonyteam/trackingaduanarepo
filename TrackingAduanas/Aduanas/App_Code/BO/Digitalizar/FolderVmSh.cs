﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileShare.Vm.FolderFile
{
    public class FolderVmSh
    {
        public string codPais { get; set; }
        public string codOficina { get; set; }
        public string Codsistema { get; set; }
        public string sistema { get; set; }
        public string userId { get; set; }
        public string userName { get; set; }
        public string oficina { get; set; }
        public string pais { get; set; }

        public IEnumerable<FolderFileVmSh> FolderFile { get; set; }
    }
}
