﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FileShare.Vm.FolderFile;

namespace FileShare.WebApp.ClientSide.Clases
{
    public class Datos
    {
        public FolderVmSh Folder { get; set; }
        public IEnumerable<FileTypeVm> Documentos { get; set; }
    }
}