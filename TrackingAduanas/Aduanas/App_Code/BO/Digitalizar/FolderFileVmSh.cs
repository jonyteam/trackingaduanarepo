﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileShare.Vm.FolderFile
{
    public class FolderFileVmSh
    {
        public string IdSistema { get; set; }
        public string Identificador { get; set; }
        public string NombreEmpresa { get; set; }
        public string Datos { get; set; }
        public string CodPaisOperacion { get; set; }
        public string Observacion { get; set; }

	//IdSistema obligatorio
	//los demas son opcionales se debe poner al menos uno
    }
}
