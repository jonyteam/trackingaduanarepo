﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileShare.Vm.FolderFile
{
    public class FileTypeVm
    {
        public long IdFileType { get; set; }
        public string FileType { get; set; }
    }
}
