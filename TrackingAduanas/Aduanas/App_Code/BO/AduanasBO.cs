using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for AduanasBO
/// </summary>
public class AduanasBO : CapaBase
{

    class Campos
    {
        public static string CODIGOADUANA = "CodigoAduana";
        public static string NOMBREADUANA = "NombreAduana";
        public static string CODPAIS = "CodPais";
        public static string CODESTADO = "CodEstado";
        public static string CREARINSTRUCCIONES = "CrearInstrucciones";
    }

    public AduanasBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from Aduanas A ";
        this.initializeSchema("Aduanas");
    }

    public void loadAllAduanas()
    {
        string sql = coreSQL;
        sql += " WHERE CodEstado = '0' ORDER BY CodPais DESC, CodigoAduana ASC ";
        this.loadSQL(sql);
    }

    public void loadAllAduanasPais()
    {
        string sql = " SELECT LTRIM(RTRIM(RTRIM(NombreAduana) + ' - ' + LTRIM(CodPais))) AS NombreAdunaPais, * FROM Aduanas ";
        sql += " WHERE CodEstado = '0' ORDER BY CodPais DESC, CodigoAduana ASC ";
        this.loadSQL(sql);
    }

    public void loadAllCampoAduanas()
    {
        string sql = this.coreSQL;
        sql += " where CodEstado = '0' order by NombreAduana";
        this.loadSQL(sql);
    }

    public void loadAllCampoAduanasReportes(string codpais)
    {
        string sql = this.coreSQL;
        sql += " where CodEstado = '0' and CodPais = @codPais";
        this.loadSQL(sql, new Parametro("@codPais", codpais));
    }

    public void loadAduanas()
    {
        string sql = "select CodPais, c.Descripcion as Pais, NombreAduana as Aduana, CodigoAduana";
        sql += " from Aduanas a inner join Codigos c on (a.CodPais = c.Codigo and c.Categoria = 'PAISES')";
        sql += " where CodEstado = '0'";
        this.loadSQL(sql);
    }

    public void loadAduanas(string codigo)
    {
        string sql = this.coreSQL;
        sql += " where CodigoAduana = '" + codigo + "' and CodEstado = '0' order by CodigoAduana";
        this.loadSQL(sql);
    }

    public void loadNombreAduanas(string nombre)
    {
        string sql = this.coreSQL;
        sql += " where NombreAduana like '%" + nombre + "%' and CodEstado = '0' order by CodigoAduana";
        this.loadSQL(sql);
    }

    public void loadNombreAduanas(string nombre, string codPais)
    {
        string sql = this.coreSQL;
        sql += " where NombreAduana like '%" + nombre + "%' and CodPais = '" + codPais + "' and CodEstado = '0' order by CodigoAduana";
        this.loadSQL(sql);
    }

    public void loadPaisAduanas(string pais)
    {
        string sql = this.coreSQL;
        sql += " where CodPais = '" + pais + "' and CodEstado = '0' order by CodigoAduana";
        this.loadSQL(sql);
    }

    public void loadAduanasXPaisSinOrigen(string pais, string codAduana)
    {
        string sql = this.coreSQL;
        sql += " where CodPais = '" + pais + "' and CodEstado = '0' and CodigoAduana != '" + codAduana + "' order by CodigoAduana";
        this.loadSQL(sql);
    }

    public void loadAduanasParaInstrucciones(string pais)
    {
        string sql = this.coreSQL;
        sql += " where CodPais = '" + pais + "' and CodEstado = '0' and CrearInstrucciones = 0 order by CodigoAduana";
        this.loadSQL(sql);
    }

    #region campos


    public string CREARINSTRUCCIONES
    {
        get
        {
            return (string)registro[Campos.CREARINSTRUCCIONES].ToString();
        }
        set
        {
            registro[Campos.CREARINSTRUCCIONES] = value;
        }
    }

    public string CODIGOADUANA
    {
        get
        {
            return (string)registro[Campos.CODIGOADUANA].ToString();
        }
        set
        {
            registro[Campos.CODIGOADUANA] = value;
        }
    }

    public string NOMBREADUANA
    {
        get
        {
            return (string)registro[Campos.NOMBREADUANA].ToString();
        }
        set
        {
            registro[Campos.NOMBREADUANA] = value;
        }
    }

    public string CODPAIS
    {
        get
        {
            return (string)registro[Campos.CODPAIS].ToString();
        }
        set
        {
            registro[Campos.CODPAIS] = value;
        }
    }

    public string CODESTADO
    {
        get
        {
            return (string)registro[Campos.CODESTADO].ToString();
        }
        set
        {
            registro[Campos.CODESTADO] = value;
        }
    }

    public int darBajaAduana(string codigo)
    {
        return this.executeCustomSQL("update Aduanas set Eliminado = '1' where CodigoAduana = '" + codigo + "'");
    }

    #endregion

}
