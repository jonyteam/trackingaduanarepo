using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for ModulosBO
/// </summary>
public class ModulosBO: CapaBase
{
    class Campos
    {
        public static string IDMODULO = "IdModulo";
        public static string DESCRIPCION = "Descripcion";
        public static string ELIMINADO = "Eliminado";
    }

    public ModulosBO(GrupoLis.Login.Login log): base(log, true)
	{
        this.coreSQL = "Select * from Modulos";
        this.initializeSchema("Modulos");
    }

    public void loadModulos()
    {
        string sql = this.coreSQL;
        sql += " where Eliminado = '0' ";
        sql += " order by Descripcion";
        this.loadSQL(sql);
    }

    public void loadModulos(string idModulo)
    {
        string sql = this.coreSQL;
        sql += " where IdModulo = " + idModulo + " and Eliminado = '0'";
        this.loadSQL(sql);
    }

    public void loadModulosRol(string idRol)
    {
        string sql = "select m.IdModulo, m.Descripcion from AutorizacionesModulo as am inner join Modulos as m";
        sql += " on (am.IdModulo = m.IdModulo)";
        sql += " where am.IdRol = " + idRol + " and m.Eliminado = '0' and am.Eliminado = '0'";
        sql += " order by m.Descripcion";
        this.loadSQL(sql);
    }

    public void loadRolesModulo(string nombreModulo)
    {
        string sql = "select r.IdRol, r.Descripcion from AutorizacionesModulo as am inner join Modulos as m";
        sql += " on (am.IdModulo = m.IdModulo) inner join Roles as r on (am.IdRol = r.IdRol)";
        sql += " where m.Descripcion like '%" + nombreModulo + "%' and am.Eliminado = '0'";
        sql += " order by r.Descripcion";
        this.loadSQL(sql);
    }

    public void loadPermisosModuloUsuario(string modulo, string idUsuario)
    {
        string sql = "select p.IdPermiso, p.Descripcion from Modulos m inner join AutorizacionesModulo am on (m.IdModulo = am.IdModulo)";
        sql += " inner join ModulosPermisos mp on (m.IdModulo = mp.IdModulo and mp.IdRol = am.IdRol)";
        sql += " inner join Permisos p on (mp.IdPermiso = p.IdPermiso) inner join UsuariosRoles ur on (ur.IdRol = am.IdRol)";
        sql += " where ur.IdUsuario = @idUsuario and m.Descripcion = @modulo and p.Eliminado = '0' and mp.Eliminado ='0'";
        this.loadSQL(sql, new Parametro("@idUsuario", idUsuario), new Parametro("@modulo", modulo));
    }

    public string IDMODULO
    {
        get
        {
            return (string)registro[Campos.IDMODULO].ToString();
        }
    }

    public string DESCRIPCION
    {
        get
        {
            return (string)registro[Campos.DESCRIPCION].ToString();
        }
        set
        {
            registro[Campos.DESCRIPCION] = value;
        }
    }

    public string ELIMINADO
    {
        get
        {
            return (string)registro[Campos.ELIMINADO].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO] = value;
        }
    }

}
