﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for DistribuidorBO
/// </summary>
public class BarcosBO: CapaBase
{
    class Campos
    {
        public static string ID = "Id";
        public static string NOMBRE = "Nombre";
        public static string CODIGO = "Codigo";      
        public static string ELIMINADO = "Eliminado";      
        public static string IDUSUARIO = "IdUsuario";
        public static string FECHACREACION = "FechaCreacion";      
    }

    public BarcosBO(GrupoLis.Login.Login log)
            : base(log, true)
	{
        coreSQL = "Select * from Barcos";
        initializeSchema("Barcos");
    }

    public void loadBarcos()
    {
        string sql = this.coreSQL;
        sql += " where Eliminado = '0' order by Nombre";
        this.loadSQL(sql);
    }

    public void loadTodosBarcos()
    {
        string sql = "SELECT 0 as Id, 'Seleccione...' as Codigo, '0' as Eliminado";
        sql += " UNION";
        sql += " Select Id, Codigo, Eliminado from Barcos g";
        sql += " where g.Eliminado = '0'";
        loadSQL(sql);
    }

    public void loadBarcos(string id)
    {
        string sql = this.coreSQL;
        sql += " where Id = '" + id + "' and Eliminado = '0'";
        loadSQL(sql);
    }

    public void ZarpesBarcos()
    {
        string sql = "Select '0' as Id,'Seleccione...' as Barco ";
        sql += " UNION ";
        sql += " Select distinct B.Id,Z.Barco ";
        sql += " from Barcos B ";
        sql += " inner join Zarpes Z on (Z.Barco=B.Codigo and Z.Estado=0) ";
        sql += " where B.Eliminado=0 ";
        this.loadSQL(sql);
    }

    public string ID
    {
        get
        {
            return (string)registro[Campos.ID].ToString();
        }
        set
        {
            registro[Campos.ID] = value;
        }
    }

    public string NOMBRE
    {
        get
        {
            return (string)registro[Campos.NOMBRE];
        }
        set
        {
            registro[Campos.NOMBRE] = value;
        }
    }

    public string ELIMINADO
    {
        get
        {
            return (string)registro[Campos.ELIMINADO].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
       }
    }
   
    public DateTime FECHACREACION
    {
        get
        {
            return Convert.ToDateTime(registro[Campos.FECHACREACION]);
        }
        set
        {
            registro[Campos.FECHACREACION] = value;
        }
    }

    public string CODIGO
    {
        get
        {
            return (string)registro[Campos.CODIGO].ToString();
        }
        set
        {
            registro[Campos.CODIGO] = value;
        }
    }


}
