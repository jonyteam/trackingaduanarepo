﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;
using GrupoLis.Login;
using GrupoLis.Ebase;

/// <summary>
/// Summary description for DetalleRecibosBO
/// </summary>
public class DetalleRecibosBO: CapaBase
{
    class campos
    {
        public static string correlativo = "correlativo";
        public static string nro_rembolso = "nro_rembolso";
        public static string Fecha_Hora = "Fecha_Hora";
        public static string nombre_cliente = "nombre_cliente";
        public static string CodigoCliente = "CodigoCliente";
        public static string cod_descripcion = "cod_descripcion";
        public static string descripcion = "descripcion";
        public static string nro_hoja_ruta = "nro_hoja_ruta";
        public static string nro_recibo = "nro_recibo";
        public static string serie_poliza = "serie_poliza";
        public static string valor = "valor";
        public static string ingresado_por = "ingresado_por";
        public static string estado = "estado";
        public static string observaciones = "observaciones";
        public static string fecha_modificacion = "fecha_modificacion";
        public static string relacion = "relacion";
        public static string control_conta = "control_conta";
        public static string seleccion = "seleccion";
        public static string centro_costo = "centro_costo";
        public static string cuenta = "cuenta";
        public static string fecha_contabilidad = "fecha_contabilidad";
        public static string responsable_caja = "responsable_caja";
        public static string valor_letras = "valor_letras";
        public static string num_carga = "num_carga";
        public static string id_usuario = "id_usuario";
        public static string doc_sap = "doc_sap";
        public static string estatus = "estatus";
        public static string IVA = "IVA";

    }    

	public DetalleRecibosBO(GrupoLis.Login.Login log)
        : base (log,true)
	{
        this.coreSQL = ("Select * from Detalle_Recibos");
        this.initializeSchema("Detalle_Recibos");

	}

    public void loadtabladetalle()
    {
        string sql = this.coreSQL;
        this.loadSQL(sql);
    }
   
    public void loadCargados(Int32 num_carga)
    {        
        string sql = "Update Detalle_Recibos set control_conta='1', seleccion = 'False', num_carga=" + num_carga + " where seleccion = 'True'";
        this.loadSQL(sql);
    }

    public void loadCargarRembolso(String correlativoX)
    {
        string sql = this.coreSQL;
        sql += " where correlativo like('" + correlativoX + "%') and seleccion='False' and estado <> 'Anulado'";
        this.loadSQL(sql);
    }

    public void loadCargarRecibos()
    {
        string sql = this.coreSQL;
        //sql += " where control_conta = 0 and estado <> 'Anulado'";
        sql += " where control_conta = 0 and estatus = 'False'";
        this.loadSQL(sql);
    }

    public void loadCargarModificados()
    {
        string sql = this.coreSQL;
        //sql += " where control_conta = 2 and estado <> 'Anulado'";
        sql += " where control_conta = 2 and estatus = 'False'";
        this.loadSQL(sql);
    }

    public void loadCargar(String correlativoY)
    {
        string sql = this.coreSQL;
        sql += " where correlativo like('" + correlativoY + "%') and seleccion='True'";
        this.loadSQL(sql);
    }

    public void DetalleRecibo(String correlativoY)
    {
        string sql = this.coreSQL;
        sql += " where id_detalle = '" + correlativoY + "' and estatus= 0 ";
        this.loadSQL(sql);
    }
   
    public void loadGenerarArchivo()
    {
        string sql = this.coreSQL;
        sql += " where seleccion='True' and control_conta = 0";
        this.loadSQL(sql);
    }
    public void loadTotalCargaVieja()
    {
        string sql = "Select Convert(Varchar,GETDATE(),104) as 'fec.docu.','SA' as 'Clase Dto.','GT01' as Sociedad,Convert(Varchar,GETDATE(),104) as 'fec.Con.'";
        sql += " ,MONTH(GETDATE()) as Periodo, 'GTQ' as Moneda, '1000010' as 'No.Dto.',";
        sql += " 'RembolsoN.' + a.nro_rembolso as Referencia, a.responsable_caja, SPACE(15) AS Divi, SPACE(30) AS ClvCt, b.cuenta_acreedora as Cuenta,IVA, SPACE(15) AS space, SUM(a.valor) as Total, SPACE(15) as col1,SPACE(15) as col2, SPACE(15) as col3,SPACE(15) as col4,SPACE(15) as col5, a.nro_rembolso as Asignacion, a.nro_rembolso + ' ' + b.nombre_aduana as Aduana From Detalle_Recibos a, Aduanas b where a.seleccion= 'True' and a.centro_costo = b.centro_costo Group By a.responsable_caja, b.cuenta_acreedora,a.nro_rembolso, b.nombre_aduana, IVA";
        this.loadSQL(sql);
    }
    public void loadGenerarArchivo2()
    {
        string sql = this.coreSQL;
        sql += " where seleccion='True' and control_conta = 2";
        this.loadSQL(sql);
    }

    public void loadSelCorrelativo(String SelCorrelativo)
    {
        string sql = this.coreSQL;
     //   sql += " where correlativo = '" + SelCorrelativo + "'";
        sql += " where id_detalle = '" + SelCorrelativo + "'";
        this.loadSQL(sql);
    }

    public void loadtabla(String numero_rembolso)
    {
        string sql = "Update Detalle_Recibos set estado='Impreso' where nro_rembolso = '" + numero_rembolso + "' and estado='Pendiente'";
        this.loadSQL(sql);
    }

    public void loadFirmado(String numero_rembolso)
    {
        string sql = "Update Detalle_Recibos set estado='Firmado' where nro_rembolso = '" + numero_rembolso + "' and estado='Impreso'";
        this.loadSQL(sql);
    }

    public void loadcorrelativo()
    {
        string sql = "SELECT correlativo FROM Detalle_Recibos WHERE id_detalle =(SELECT MAX(id_detalle) FROM Detalle_Recibos AS Detalle_Recibos_1)";
        this.loadSQL(sql);
    }

    public void loadModRecibo(String CorrelativoRecibo)
    {
        //string sql = "select nro_recibo, control_conta, nombre_cliente, descripcion, nro_hoja_ruta, serie_poliza, valor from Detalle_Recibos where correlativo='" + CorrelativoRecibo + "' and estado <> 'Anulado'";
        string sql = "select nro_recibo, control_conta, nombre_cliente, descripcion, nro_hoja_ruta, serie_poliza, valor from Detalle_Recibos where correlativo='" + CorrelativoRecibo + "' and estatus = 'False'";
        this.loadSQL(sql);
    }
    
    public void loadModRecibo2(String CorrelativoRecibo2)
    {
        //string sql = "select nro_recibo, CodigoCliente, control_conta, nombre_cliente, descripcion, cod_descripcion, nro_hoja_ruta, serie_poliza, valor from Detalle_Recibos where correlativo='" + CorrelativoRecibo2 + "' and estado <> 'Anulado'";
        string sql = "select nro_recibo, CodigoCliente, control_conta, nombre_cliente, descripcion, cod_descripcion, nro_hoja_ruta, serie_poliza, valor from Detalle_Recibos where correlativo='" + CorrelativoRecibo2 + "' and estatus = 'False'";
        this.loadSQL(sql);
    }

    public void loadnro_reciboexiste(String nro_recibo, String correlativo0)
    {
        string sql = this.coreSQL;
        sql += " where nro_recibo = '" + nro_recibo + "' and correlativo like'" + correlativo0 + "%'";
        this.loadSQL(sql);
    }

    public void loadgridrecibos(String nro_recibo, String correlativo1)
    {
        string sql = this.coreSQL;
        sql += " where nro_recibo = '" + nro_recibo + "' and estado='Pendiente' and correlativo like'" + correlativo1 + "%' and estatus='False'";
        this.loadSQL(sql);
    }

    public void loadgriddetalle(String codigo)
    {
        string sql = "select correlativo as CORRELATIVO, nro_rembolso as REMBOLSO, fecha_hora as FECHA_INGRESO, nombre_cliente as CLIENTE, descripcion as DESCRIPCION, nro_hoja_ruta as HOJA_RUTA, nro_recibo as NRO_RECIBO, serie_poliza as SERIE_POLIZA,valor as VALOR, ingresado_por as INGRESADO_POR, estado as ESTADO, observaciones as OBSERVACIONES, fecha_modificacion as FECHA_MODIFICACION,relacion as RELACION, doc_sap as Doc_SAP from Detalle_Recibos where correlativo like('" + codigo + "%') and estatus='False'";
      this.loadSQL(sql);
    }

    public void loadmodificarrecibo(String correlativo, String fechamod, String relacion)
    {
        string sql = "Update Detalle_Recibos set fecha_modificacion='" + fechamod + "', relacion ='" + relacion + "', estado='Anulado', estatus='True' where correlativo='" + correlativo + "'";
        this.loadSQL(sql);
    }

    public void loadagregarrecibo(String correlativo, String rembolso, String cliente, String descripcion, String hojaruta, String nro_recibo, String poliza, String valor, String ingresado, String Obs, String relacion, String centro_costo, String cuenta, String responsable)
    {
        string sql = "Insert into Detalle_Recibos (correlativo, nro_rembolso, nombre_cliente, descripcion, nro_hoja_ruta, nro_recibo, serie_poliza, valor, ingresado_por, estado, observaciones, relacion, control_conta, seleccion, centro_costo, cuenta, responsable_caja) values ('" + correlativo + "', '" + rembolso + "', '" + cliente + "', '" + descripcion + "', '" + hojaruta + "', '" + nro_recibo + "', '" + poliza + "', '" + valor + "', '" + ingresado + "', 'Pendiente','" + Obs + "', '" + relacion + "', '0', 'False', '" + centro_costo + "', '" + cuenta + "', '" + responsable + "')";
        this.loadSQL(sql);
    }

    public void loadcontrolrembolso(String num_rembolso)
    {
        string sql = "SELECT correlativo as CORRELATIVO, nombre_cliente as NOMBRE_CLIENTE, descripcion as DESCRIPCION, nro_hoja_ruta as HOJA_RUTA, nro_recibo as NRO_RECIBO, serie_poliza as SERIE_POLIZA, valor as VALOR from Detalle_Recibos where nro_rembolso = '" + num_rembolso + "' and estado='Pendiente' and estatus='False'";
         this.loadSQL(sql);
    }

    //Impresos
    public void loadcontrolimpresos(String numero_rembolso)
    {
        string sql = "SELECT correlativo as CORRELATIVO, nombre_cliente as NOMBRE_CLIENTE, descripcion as DESCRIPCION, nro_hoja_ruta as HOJA_RUTA, nro_recibo as NRO_RECIBO, serie_poliza as SERIE_POLIZA, valor as VALOR, estado as ESTADO from Detalle_Recibos where nro_rembolso = '" + numero_rembolso + "' and estado <>'Firmado' and estado <> 'Anulado' and estado <> 'Modificado' and estatus='False'";
        this.loadSQL(sql);
    }
    //Firmados
    public void loadcontrolfirmados(String NumRembolsoX)
    {
        string sql = "SELECT correlativo as CORRELATIVO, nombre_cliente as NOMBRE_CLIENTE, descripcion as DESCRIPCION, nro_hoja_ruta as HOJA_RUTA, nro_recibo as NRO_RECIBO, serie_poliza as SERIE_POLIZA, valor as VALOR, estado as ESTADO from Detalle_Recibos where nro_rembolso = '" + NumRembolsoX + "' and estado = 'Firmado' and estado <> 'Anulado' and estado <> 'Modificado' and estatus='False'";
        this.loadSQL(sql);
    }
               
    public void loadconsultarembolsos(String nro_rembolso2, String cod_aduana2)
    {
        string sql = "SELECT correlativo as CORRELATIVO, nombre_cliente as NOMBRE_CLIENTE, descripcion as DESCRIPCION, nro_hoja_ruta as HOJA_RUTA, nro_recibo as NRO_RECIBO, serie_poliza as SERIE_POLIZA, valor as VALOR, estado as ESTADO from Detalle_Recibos where nro_rembolso='" + nro_rembolso2 + "' and correlativo like'" + cod_aduana2 + "%' and estatus='False'";
        this.loadSQL(sql);
    }

    public void loadestadoimpreso(String estadoimpreso, String cod_aduana3)
    {
         string sql = "Update Detalle_Recibos set estado='Impreso' where nro_rembolso='" + estadoimpreso + "' and estado='Pendiente' and correlativo like'" + cod_aduana3 + "%'";
         this.loadSQL(sql);
    }

    public void loadfirmado(String estadoimpreso2)
    {
        string sql = "Update Detalle_Recibos set estado='Firmado' where nro_rembolso='" + estadoimpreso2 + "' and estado='Impreso'";
        this.loadSQL(sql);
    }
 
    public void loadTotalCarga()
    {
        string sql = "select 'RembolsoN.' + a.nro_rembolso, a.responsable_caja, SPACE(15) AS Divi, SPACE(30) AS ClvCt, b.cuenta_acreedora,IVA, SPACE(15) AS space, SUM(a.valor) as Total, SPACE(15) as col1,SPACE(15) as col2, SPACE(15) as col3,SPACE(15) as col4,SPACE(15) as col5, a.nro_rembolso, a.nro_rembolso + ' ' + b.nombre_aduana as Aduana From Detalle_Recibos a, Aduanas b where a.seleccion= 'True' and a.centro_costo = b.centro_costo Group By a.responsable_caja, b.cuenta_acreedora,a.nro_rembolso, b.nombre_aduana, IVA";        
        this.loadSQL(sql);
    }

    public void LoadReporte(string NumeroRecibo)  // 
    {
        string sql = "EXEC IMPRIMIR_RECIBO '"+ NumeroRecibo + "'";
        this.loadSQL(sql);
    }

    public void LoadReporteLiquidacion(string NumeroRembolso, string CodidoAduanaR)
    {
        string sql = "EXEC LIQUIDACION '" + NumeroRembolso + "', " + "'" + CodidoAduanaR + "'";
        this.loadSQL(sql);
    }

    public void LoadReporteCerrar(string NumeroRembolsoX, string CodidoAduanaX)
    {
        string sql = "EXEC CERRAR '" + NumeroRembolsoX + "', " + "'" + CodidoAduanaX + "'";
        this.loadSQL(sql);
    }
    //**

    public void LoadReporteLiquidacionA(string NumeroRembolsoA, string CodidoAduanaRA)
    {
        string sql = "EXEC LIQUIDACION1 '" + NumeroRembolsoA + "', " + "'" + CodidoAduanaRA + "'";
        this.loadSQL(sql);
    }

    public void LoadReporteCerrarA(string NumeroRembolsoXA, string CodidoAduanaXA)
    {
        string sql = "EXEC CERRARL '" + NumeroRembolsoXA + "', " + "'" + CodidoAduanaXA + "'";
        this.loadSQL(sql);
    }

    //**
    public void LoadReporteCarga(String fecha_carga)
    {
        string sql = "EXEC CARGAS '" + fecha_carga + "'";
        this.loadSQL(sql);       
    }

    //Doc. SAP
    public void loadDocSAP(String DocSAP, String CorrelativoNum, String ObsDoc)
    {
        string sql = "Update Detalle_Recibos set estado= 'Modificado', control_conta='3', doc_sap= '" + DocSAP + "', observaciones = '" + ObsDoc + "'  where  correlativo = '" + CorrelativoNum + "'";
        this.loadSQL(sql);
    }

    public void loadMantDetalleRecibos()
    {
        string sql = "select id_detalle, correlativo, nro_rembolso, fecha_hora, nombre_cliente, descripcion, nro_hoja_ruta, nro_recibo, serie_poliza, valor, ingresado_por, estado, control_conta from Detalle_Recibos where estatus='False'";
        this.loadSQL(sql);
    }
    //**Mantenimiento para ingresos dentro de un rago de fechas
    public void loadManteEntreFechas(String Fecha1, String Fecha2)
    {
        string sql = "select id_detalle AS ID, correlativo as Correlativo, nro_rembolso as NroRembolso, fecha_hora as Fecha, nombre_cliente as Cliente, descripcion as Descripcion, nro_hoja_ruta as HojaRuta, nro_recibo as NroRecibo, serie_poliza as Poliza, valor as Valor, ingresado_por as Ingresado, estado As Estado, control_conta as Cargado from Detalle_Recibos where estatus='False' and Convert(varchar,(Fecha_Hora),1) between '" + Fecha1 + "' and '" + Fecha2 + "' order by Correlativo";
        this.loadSQL(sql);
    }

    public void loadEliminarItemDR(String ECorrelativo, String ERecibo)
    {
        //string sql = "Delete from Detalle_Recibos where correlativo='" + ECorrelativo + "' and nro_recibo='" + ERecibo + "'";
        string sql = "Update Detalle_Recibos set estatus='True' where correlativo='" + ECorrelativo + "' and nro_recibo='" + ERecibo + "'"; 
        this.loadSQL(sql);
    }

    public void loadModificarTotalRem(String NumRembolsoCor)
    {
        string sql = "Update Rembolsos_Realizados set valor = (select sum(valor) as valor from Detalle_Recibos where nro_rembolso='" + NumRembolsoCor + "' and estatus='False') where nro_rembolso = '" + NumRembolsoCor + "'";
        this.loadSQL(sql);
    }
        

    public void loadModTablaDetalle(String CorrelativoTD, String HojaTD, String ReciboTD, String SerieTD, Double ValorTD, String estadoTD, String ContaTD, Int32 Id_TD, String NumRembolsoTD)
    {
        string sql = "Update Detalle_Recibos set correlativo= '" + CorrelativoTD + "', nro_hoja_ruta='" + HojaTD + "', nro_recibo = '" + ReciboTD + "', serie_poliza='" + SerieTD + "', valor = " + ValorTD + ", estado= '" + estadoTD + "', control_conta='" + ContaTD + "' where id_detalle=" + Id_TD;
        sql += " ";      
        sql += " Update Rembolsos_Realizados set valor = (select sum(valor) as total from Detalle_Recibos where nro_rembolso= '" + NumRembolsoTD + "' and estado<>'Anulado') where nro_rembolso= '" + NumRembolsoTD + "'";
        this.loadSQL(sql);
    }

    //campos 

    public string correlativo
    {
        get
        {
            return registro[campos.correlativo].ToString();
        }
        set
        {
            registro[campos.correlativo] = value;
        }
    }
    public string nro_rembolso
    {
        get
        {
            return registro[campos.nro_rembolso].ToString();
        }
        set
        {
            registro[campos.nro_rembolso] = value;
        }

    }
    public DateTime Fecha_Hora
    {
        get
        {
            return Convert.ToDateTime(registro[campos.Fecha_Hora].ToString());
        }
        set
        {
            registro[campos.Fecha_Hora] = value;
        }

    }
    public int CodigoCliente
    {
        get
        {
            return Convert.ToInt32(registro[campos.CodigoCliente].ToString());
        }
        set
        {
            registro[campos.CodigoCliente] = value;
        }
    }

    public string nombre_cliente
    {
        get
        {
            return (registro[campos.nombre_cliente].ToString());
        }
        set
        {
            registro[campos.nombre_cliente] = value;
        }

    }

    public int cod_descripcion
    {
        get
        {
            return Convert.ToInt32(registro[campos.cod_descripcion].ToString());
        }
        set
        {
            registro[campos.cod_descripcion] = value;
        }

    }
    public string descripcion
    {
        get
        {
            return registro[campos.descripcion].ToString();
        }
        set
        {
            registro[campos.descripcion] = value;
        }

    }

    public string nro_hoja_ruta
    {
        get
        {
            return registro[campos.nro_hoja_ruta].ToString();
        }
        set
        {
            registro[campos.nro_hoja_ruta] = value;
        }

    }
    public string nro_recibo
    {
        get
        {
            return registro[campos.nro_recibo].ToString();
        }
        set
        {
            registro[campos.nro_recibo] = value;
        }

    }
    public string serie_poliza
    {
        get
        {
            return registro[campos.serie_poliza].ToString();
        }
        set
        {
            registro[campos.serie_poliza] = value;
        }

    }
    public double valor
    {
        get
        {
            return Convert.ToDouble(registro[campos.valor].ToString());
        }
        set
        {
            registro[campos.valor] = value;
        }

    }
    public string ingresado_por
    {
        get
        {
            return registro[campos.ingresado_por].ToString();
        }
        set
        {
            registro[campos.ingresado_por] = value;
        }

    }
    public string estado
    {
        get
        {
            return registro[campos.estado].ToString();
        }
        set
        {
            registro[campos.estado] = value;
        }

    }
    public string observaciones
    {
        get
        {
            return registro[campos.observaciones].ToString();
        }
        set
        {
            registro[campos.observaciones] = value;
        }

    }
    public String fecha_modificacion
    {
        get
        {
            return registro[campos.fecha_modificacion].ToString();
        }
        set
        {
            registro[campos.fecha_modificacion] = value;
        }

    }
    public string relacion
    {
        get
        {
            return registro[campos.relacion].ToString();
        }
        set
        {
            registro[campos.relacion] = value;
        }

    }
    public string control_conta
    {
        get
        {
            return registro[campos.control_conta].ToString();
        }
        set
        {
            registro[campos.control_conta] = value;
        }

    }

    public string seleccion
    {
        get
        {
            return registro[campos.seleccion].ToString();
        }
        set
        {
            registro[campos.seleccion] = value;
        }

    }

    public string centro_costo
    {
        get
        {
            return registro[campos.centro_costo].ToString();
        }
        set
        {
            registro[campos.centro_costo] = value;
        }

    }

    public string cuenta
    {
        get
        {
            return registro[campos.cuenta].ToString();
        }
        set
        {
            registro[campos.cuenta] = value;
        }

    }
    public string fecha_contabilidad
    {
        get
        {
            return registro[campos.fecha_contabilidad].ToString();
        }
        set
        {
            registro[campos.fecha_contabilidad] = value;
        }

    }
    public string responsable_caja
    {
        get
        {
            return registro[campos.responsable_caja].ToString();
        }
        set
        {
            registro[campos.responsable_caja] = value;
        }

    }
    public string valor_letras
    {
        get
        {
            return registro[campos.valor_letras].ToString();
        }
        set
        {
            registro[campos.valor_letras] = value;
        }
    }


    public int num_carga
    {
        get
        {
            return Convert.ToInt32(registro[campos.num_carga].ToString());
        }
        set
        {
            registro[campos.num_carga] = value;
        }
    }

    public int id_usuario
    {
        get
        {
            return Convert.ToInt16(registro[campos.id_usuario].ToString());
        }
        set
        {
            registro[campos.id_usuario] = value;
        }

    }

    public string doc_sap
    {
        get
        {
            return registro[campos.doc_sap].ToString();
        }
        set
        {
            registro[campos.doc_sap] = value;
        }
    }

    public string estatus
    {
        get
        {
            return registro[campos.estatus].ToString();
        }
        set
        {
            registro[campos.estatus] = value;
        }
    }

    public string IVA
    {
        get
        {
            return registro[campos.IVA].ToString();
        }
        set
        {
            registro[campos.IVA] = value;
        }
    }
}
