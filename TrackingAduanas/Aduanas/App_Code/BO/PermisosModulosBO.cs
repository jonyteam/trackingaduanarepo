using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for PermisosBO
/// </summary>
public class PermisosModulosBO: CapaBase
{
    class Campos
    {
        public static string IDPERMISO = "IdPermiso";
        public static string IDMODULO = "IdModulo";
        public static string IDROL = "IdRol";
        public static string PERMISODESCRIPCION = "PermisoDescripcion";
        public static string MODULODESCRIPCION = "ModuloDescripcion";
        public static string ROLDESCRIPCION = "RolDescripcion";
        public static string ELIMINADO = "Eliminado";
    }

    public PermisosModulosBO(GrupoLis.Login.Login log)
        : base(log, true)
	{
        this.coreSQL = "Select * from ModulosPermisos";

        this.initializeSchema("ModulosPermisos");
    }

    public void loadPermisosModulos()
    {
        this.loadSQL(this.coreSQL);
    }

    public void loadPermisosModulos(string idPermiso, string idModulo, string idRol)
    {
        this.loadSQL(String.Format("{0} where IdPermiso={1} and IdModulo={2} and IdRol={3}", this.coreSQL, idPermiso, idModulo, idRol));
    }

    public void loadPermisosModulosDescs()
    {
        string sql = "Select mp.IdPermiso, mp.IdModulo, mp.IdRol, p.Descripcion as PermisoDescripcion, m.Descripcion as ModuloDescripcion, r.Descripcion as RolDescripcion, mp.Eliminado from ModulosPermisos as mp";
        sql += " inner join Permisos as p on (p.IdPermiso=mp.IdPermiso and p.Eliminado='0')";
        sql += " inner join Modulos as m on (m.IdModulo=mp.IdModulo and m.Eliminado='0')";
        sql += " inner join Roles as r on (r.IdRol=mp.IdRol and r.Eliminado='0')";
        sql += " order by m.Descripcion, r.Descripcion, p.Descripcion";

        this.loadSQL(sql);
    }

    public void deletePermisosModulos(string idPermiso, string idModulo, string idRol)
    {
        this.executeCustomSQL(String.Format("delete from ModulosPermisos where IdPermiso={0} and IdModulo={1} and IdRol={2}", idPermiso, idModulo, idRol));
    }

    public string IDPERMISO
    {
        get
        {
            return (string)registro[Campos.IDPERMISO].ToString();
        }
        set
        {
            registro[Campos.IDPERMISO] = value;
        }
    }

    public string IDMODULO
    {
        get
        {
            return (string)registro[Campos.IDMODULO].ToString();
        }
        set
        {
            registro[Campos.IDMODULO] = value;
        }
    }

    public string IDROL
    {
        get
        {
            return (string)registro[Campos.IDROL].ToString();
        }
        set
        {
            registro[Campos.IDROL] = value;
        }
    }

    public string PERMISODESCRIPCION
    {
        get
        {
            return (string)registro[Campos.PERMISODESCRIPCION].ToString();
        }
    }

    public string MODULODESCRIPCION
    {
        get
        {
            return (string)registro[Campos.MODULODESCRIPCION].ToString();
        }
    }

    public string ROLDESCRIPCION
    {
        get
        {
            return (string)registro[Campos.ROLDESCRIPCION].ToString();
        }
    }

    public string ELIMINADO
    {
        get
        {
            return (string)registro[Campos.ELIMINADO].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO] = value;
        }
    }
}
