﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for UsuariosRolesBO
/// </summary>
public class UsuariosRolesBO : CapaBase
{
    class Campos
    {
        public static string IDUSUARIO = "IdUsuario";
        public static string IDROL = "IdRol";
        public static string ELIMINADO = "Eliminado";
    }

    public UsuariosRolesBO(GrupoLis.Login.Login conector)
        : base(conector, true)
    {
        coreSQL = "SELECT * FROM UsuariosRoles";
        initializeSchema("UsuariosRoles");
    }

    public void loadUsuariosRoles()
    {
        string sql = "Select * from UsuariosRoles";
        this.loadSQL(sql);
    }

    public void loadUsuariosRoles(string idUsuario)
    {
        string sql = " Select * from UsuariosRoles where IdUsuario = '" + idUsuario + "' ";
        this.loadSQL(sql);
    }

    public void loadRolesUsuarios()
    {
        string sql = "Select ur.IdRol, u.IdUsuario, u.Usuario, RTRIM(RTRIM(LTRIM(u.Nombre)) + ' ' + RTRIM(LTRIM(u.Apellido))) as NombreCompleto, r.Descripcion as RolDescripcion, ur.Eliminado from UsuariosRoles ur";
        sql += " inner join Roles r on (ur.IdRol = r.IdRol and r.Eliminado = '0')";
        sql += " inner join Usuarios u on (ur.IdUsuario = u.IdUsuario and u.Eliminado = '0')";
        this.loadSQL(sql);
    }

    public void loadRolesUsuario(string idUsuario)
    {
        string sql = "Select ur.IdRol, IdUsuario, r.Descripcion from UsuariosRoles ur inner join Roles r on (ur.IdRol = r.IdRol)";
        sql += " where IdUsuario = @idUsuario and ur.Eliminado = '0'";
        this.loadSQL(sql, new Parametro("@idUsuario", idUsuario));
    }

    public void loadRolUsuario(string idUsuario, string idRol)
    {
        string sql = "Select IdUsuario, IdRol, Eliminado from UsuariosRoles";
        sql += " where IdUsuario = {0} and IdRol = {1}";
        this.loadSQL(String.Format(sql, idUsuario, idRol));
    }

    public void deleteRolUsuario(string idUsuario, string idRol)
    {
        string sql = "delete from UsuariosRoles";
        sql += " where IdUsuario = {0} and IdRol = {1}";
        this.executeCustomSQL(String.Format(sql, idUsuario, idRol));
    }

    public string IDROL
    {
        get
        {
            return (string)registro[Campos.IDROL].ToString();
        }
        set
        {
            registro[Campos.IDROL] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }

    public string ELIMINADO
    {
        get
        {
            return (string)registro[Campos.ELIMINADO].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO] = value;
        }
    }
}
