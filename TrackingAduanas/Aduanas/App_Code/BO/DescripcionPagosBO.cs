﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;
using GrupoLis.Login;
using GrupoLis.Ebase;


/// <summary>
/// Summary description for DescripcionPagosBO
/// </summary>
public class Descripcion_PagosBO : CapaBase
{
    class campos
    {
        public static string COD_DESCRIPCION = "cod_descripcion";
        public static string DESCRIPCION = "descripcion";
        public static string CUENTA = "cuenta";
        public static string ESTATUS = "estatus";
    }

    public Descripcion_PagosBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = ("Select * from Descripcion_Pagos");
        this.initializeSchema("Descripcion_Pagos");
    }

    #region Querys
    public void loadtabladescripcion()
    {
        string sql = this.coreSQL;
        this.loadSQL(sql);
    }

    public void loadTablaDescripcionOrden()
    {
        string sql = this.coreSQL;
        sql += " order by descripcion";
        this.loadSQL(sql);
    }
    public void LoadGastosDisponibles()
    {
        string sql = @"Select * from Descripcion_Pagos where estatus = 0";
        this.loadSQL(sql);
    }


    public void loadTablaDescripcion2()
    {
        string sql = this.coreSQL;
        sql += " where estatus= 'False'";
        this.loadSQL(sql);
    }

    //
    public void loadeliminardes(String cod_descripcion)
    {
        //string sql = "Delete from Descripcion_Pagos where cod_descripcion=" + cod_descripcion;
        string sql = "Update Descripcion_Pagos set estatus='True' where cod_descripcion=" + cod_descripcion;
        this.loadSQL(sql);

    }
    public void LoadGasto(String cod_descripcion)
    {
        string sql = "Select * from Descripcion_Pagos where cod_descripcion = " + cod_descripcion;
        this.loadSQL(sql);
    }
    public void LoadGasto2(String cod_descripcion, String cuenta)
    {
        string sql = "Select * from Descripcion_Pagos where cod_descripcion = '" + cod_descripcion + "' and cuenta = '" + cuenta + "'";
        this.loadSQL(sql);
    }
    public void loadcuenta(String cod_descripcion)
    {
        string sql = "Select cuenta from Descripcion_Pagos where cod_descripcion = " + cod_descripcion;
        this.loadSQL(sql);
    }
    #endregion

    #region Campos
    public int COD_DESCRIPCION
    {
        get
        {
            return Convert.ToInt32(registro[campos.COD_DESCRIPCION].ToString());
        }
        set
        {
            registro[campos.COD_DESCRIPCION] = value;
        }

    }
    public string DESCRIPCION
    {
        get
        {
            return registro[campos.DESCRIPCION].ToString();
        }
        set
        {
            registro[campos.DESCRIPCION] = value;
        }

    }
    public string CUENTA
    {
        get
        {
            return registro[campos.CUENTA].ToString();
        }
        set
        {
            registro[campos.CUENTA] = value;
        }
    }
    public string ESTATUS
    {
        get
        {
            return registro[campos.ESTATUS].ToString();
        }
        set
        {
            registro[campos.ESTATUS] = value;
        }
    }
    #endregion

}
