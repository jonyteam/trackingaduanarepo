using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for AduanasBO
/// </summary>
public class TiemposFlujoCargaBO : CapaBase
{

    class Campos
    {
        public static string ID = "Id";
        public static string IDINSTRUCCION = "IdInstruccion";
        public static string CODIGOADUANA = "CodigoAduana";
        public static string CLIENTE = "Cliente";
        public static string PROVEEDOR = "Proveedor";
        public static string CODREGIMEN = "CodRegimen";
        public static string CORRELATIVO = "Correlativo";
        public static string NOFACTURA = "NoFactura";
        public static string CANALSELECTIVIDAD = "CanalSelectividad";
        public static string PAGA = "Paga";
        public static string PAGADO = "Pagado";
        public static string PERMISO = "Permiso";
        public static string IDTRAMITADOR = "IdTramitador";
        public static string REVISION = "Revision";
        public static string IDESTADO = "IdEstado";
        public static string FECHA = "Fecha";
        public static string FECHAINGRESO = "FechaIngreso";
        public static string ELIMINADO = "Eliminado";
        public static string IDUSUARIO = "IdUsuario";


    }

    public TiemposFlujoCargaBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from TiemposFlujoCarga";
        this.initializeSchema("TiemposFlujoCarga");
    }

    public void loadflujo(string IdHojaRuta)
    {
        string sql = this.coreSQL;
        sql += " where IdInstruccion = '" + IdHojaRuta + "'";
        this.loadSQL(sql);
    }

    public string IDINSTRUCCION
    {
        get
        {
            return (string)registro[Campos.IDINSTRUCCION].ToString();
        }
        set
        {
            registro[Campos.IDINSTRUCCION] = value;
        }
    }
    public string PROVEEDOR
    {
        get
        {
            return (string)registro[Campos.PROVEEDOR].ToString();
        }
        set
        {
            registro[Campos.PROVEEDOR] = value;
        }
    }
    public string CLIENTE
    {
        get
        {
            return (string)registro[Campos.CLIENTE].ToString();
        }
        set
        {
            registro[Campos.CLIENTE] = value;
        }
    }

    public string PERMISO
    {
        get
        {
            return (string)registro[Campos.PERMISO].ToString();
        }
        set
        {
            registro[Campos.PERMISO] = value;
        }
    }

    public string ELIMINADO 
    {
        get
        {
            return (string)registro[Campos.ELIMINADO ].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO ] = value;
        }
    }

    public string CODREGIMEN
    {
        get
        {
            return (string)registro[Campos.CODREGIMEN].ToString();
        }
        set
        {
            registro[Campos.CODREGIMEN] = value;
        }
    }

    public DateTime FECHA
    {
        get
        {
            return Convert.ToDateTime(registro[Campos.FECHA]);
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }

    public DateTime FECHAINGRESO
    {
        get
        {
            return Convert.ToDateTime(registro[Campos.FECHAINGRESO]);
        }
        set
        {
            registro[Campos.FECHAINGRESO] = value;
        }
    }
    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }

    public string CODIGOADUANA
    {
        get
        {
            return (string)registro[Campos.CODIGOADUANA].ToString();
        }
        set
        {
            registro[Campos.CODIGOADUANA] = value;
        }
    }

    public string IDTRAMITADOR
    {
        get
        {
            return (string)registro[Campos.IDTRAMITADOR].ToString();
        }
        set
        {
            registro[Campos.IDTRAMITADOR] = value;
        }
    }
    public string IDESTADO
    {
        get
        {
            return (string)registro[Campos.IDESTADO].ToString();
        }
        set
        {
            registro[Campos.IDESTADO] = value;
        }
    }

    public string PAGA
    {
        get
        {
            return (string)registro[Campos.PAGA].ToString();
        }
        set
        {
            registro[Campos.PAGA] = value;
        }
    }

}
