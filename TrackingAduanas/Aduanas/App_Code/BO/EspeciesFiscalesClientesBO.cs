using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for EspeciesFiscalesClientesBO
/// </summary>
public class EspeciesFiscalesClientesBO : CapaBase
{
    class Campos
    {
        public static string IDMOVIMIENTO = "IdMovimiento";
        public static string IDCLIENTE = "IdCliente";
        public static string OBSERVACION = "Observacion";
    }

    public EspeciesFiscalesClientesBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from EspeciesFiscalesClientes EFC ";
        this.initializeSchema("EspeciesFiscalesClientes");
    }

    public void loadEspeciesFiscalesClientes(string idCliente)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdCliente = '" + idCliente + "' ";
        this.loadSQL(sql);
    }

    public void loadEspeciesFiscalesClientesXIdMov(string idMovimiento)
    {
        string sql = this.coreSQL;
        sql += " WHERE IdMovimiento = '" + idMovimiento + "' ";
        this.loadSQL(sql);
    }

    #region campos
    public string IDMOVIMIENTO
    {
        get
        {
            return (string)registro[Campos.IDMOVIMIENTO].ToString();
        }
        set
        {
            registro[Campos.IDMOVIMIENTO] = value;
        }
    }

    public string IDCLIENTE
    {
        get
        {
            return (string)registro[Campos.IDCLIENTE].ToString();
        }
        set
        {
            registro[Campos.IDCLIENTE] = value;
        }
    }

    public string OBSERVACION
    {
        get
        {
            return (string)registro[Campos.OBSERVACION].ToString();
        }
        set
        {
            registro[Campos.OBSERVACION] = value;
        }
    }
    #endregion
}
