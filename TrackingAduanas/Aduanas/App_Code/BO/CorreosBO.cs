﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;

using GrupoLis.Ebase;
using GrupoLis.Login;

public class CorreosBO : CapaBase
{
    class Campos
    {
        public static string ID = "Id";
        public static string CORREOS = "Correos";
        public static string COPIAS = "Copias";
        public static string PAIS = "Pais";
        public static string ESTADO = "Estado";
        public static string DEPARTAMENTO = "Departamento";
    }

    public CorreosBO(GrupoLis.Login.Login conector)
        : base(conector, true)
    {
        coreSQL = " SELECT * FROM Correos ";
        initializeSchema("Codigos");
    }

    public void LoadCorreosXPais(string codPais, string departamento)
    {
        string sql = this.coreSQL;
        sql += " WHERE Pais = '" + codPais + "' AND Estado = '0' and Departamento = '"+ departamento+"' ";
        this.loadSQL(sql);
    }

    #region Campos
    public String ID
    {
        get
        {
            return Convert.ToString(registro[Campos.ID]);
        }
        set
        {
            registro[Campos.ID] = value;
        }
    }
    public String DEPARTAMENTO
    {
        get
        {
            return Convert.ToString(registro[Campos.DEPARTAMENTO]);
        }
        set
        {
            registro[Campos.DEPARTAMENTO] = value.Trim();
        }
    }
    public String CORREOS
    {
        get
        {
            return Convert.ToString(registro[Campos.CORREOS]);
        }
        set
        {
            registro[Campos.CORREOS] = value.Trim();
        }
    }
    public String COPIAS
    {
        get
        {
            return Convert.ToString(registro[Campos.COPIAS]);
        }
        set
        {
            registro[Campos.COPIAS] = value.Trim();
        }
    }
    public String PAIS
    {
        get
        {
            return Convert.ToString(registro[Campos.PAIS]);
        }
        set
        {
            registro[Campos.PAIS] = value.Trim();
        }
    }
    public String ESTADO
    {
        get
        {
            return Convert.ToString(registro[Campos.ESTADO]);
        }
        set
        {
            registro[Campos.ESTADO] = value.Trim();
        }
    }
    #endregion
}
