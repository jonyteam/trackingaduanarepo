using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for EventosBO
/// </summary>
public class TipoCambioBO : CapaBase
{

    class Campos
    {

        public static string ID = "Id";
        public static string TASACAMBIO = "TasaCambio";
        public static string PAIS = "Pais";
        public static string FECHA = "Fecha";
        public static string IDUSUARIO = "IdUsuario";
    }

    public TipoCambioBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from TipoCambio TC";
        this.initializeSchema("TipoCambio");
    }

    public void ValidarTasa(string fecha, string pais)
    {
        string sql = this.coreSQL;
        sql += " where Fecha = '" + fecha + "' and Pais = '" + pais + "'";
        this.loadSQL(sql);
    }
    public void loadtasacambio(string pais)
    {
        string sql = this.coreSQL;
        sql += " where Fecha = CONVERT (DATE, GETDATE()) and Pais = '" + pais + "'";
        this.loadSQL(sql);
    }
    public void loadtasa()
    {
        string sql = this.coreSQL;
        sql += " where Fecha = '1900' and Pais = 'S'";
        this.loadSQL(sql);
    }
    #region QUERYS

    public void LoadTasasAdministrador()
    {
        string sql = "SELECT TasaCambio,B.Descripcion as Pais,Fecha,C.Nombre +' '+Apellido as Nombre";
        sql += " FROM TipoCambio A Inner Join Codigos B on (B.Categoria = 'PAISES' and B.Codigo = A.Pais and B.Eliminado =  0)";
        sql += " Inner Join Usuarios C on (A.IdUsuario = C.IdUsuario and C.Eliminado = 0)";
        sql += " Where DATEADD(Year,1,Fecha) >= GETDATE()";
        this.loadSQL(sql);
    }
    public void LoadTasasAdministradorXPais(string Pais)
    {
        string sql = "SELECT TasaCambio,B.Descripcion as Pais,Fecha,C.Nombre +' '+Apellido as Nombre";
        sql += " FROM TipoCambio A Inner Join Codigos B on (B.Categoria = 'PAISES' and B.Codigo = A.Pais and B.Eliminado =  0)";
        sql += " Inner Join Usuarios C on (A.IdUsuario = C.IdUsuario and C.Eliminado = 0)";
        sql += " Where DATEADD(Year,1,Fecha) >= GETDATE() and A.Pais = '" + Pais + "'";
        this.loadSQL(sql);
    }
    public int eliminarAutorizacionesModulo(string codigo)
    {
        return this.executeCustomSQL("delete from AutorizacionesModulo where IdRol = " + codigo);
    }
    public int darBajaAutorizacionesModulo(string codigo)
    {
        return this.executeCustomSQL("update AutorizacionesModulo set Estado = '1' where IdRol = " + codigo);
    }
    #endregion

    #region Campos

    public string ID
    {
        get
        {
            return (string)registro[Campos.ID].ToString();
        }
        set
        {
            registro[Campos.ID] = value;
        }
    }
    public Double TASACAMBIO
    {
        get
        {
            return Convert.ToDouble(registro[Campos.TASACAMBIO].ToString());
        }
        set
        {
            registro[Campos.TASACAMBIO] = value;
        }
    }
    public string PAIS
    {
        get
        {
            return (string)registro[Campos.PAIS].ToString();
        }
        set
        {
            registro[Campos.PAIS] = value;
        }
    }
    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }
    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }

    #endregion
}
