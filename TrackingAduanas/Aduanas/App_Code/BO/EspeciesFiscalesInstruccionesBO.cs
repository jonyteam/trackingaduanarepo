using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for EspeciesFiscalesInstruccionesBO
/// </summary>
public class EspeciesFiscalesInstruccionesBO : CapaBase
{
    class Campos
    {
        public static string ITEM = "Item";
        public static string IDINSTRUCCION = "IdInstruccion";
        public static string CODESPECIEFISCAL = "CodEspecieFiscal";
        public static string NECESARIO = "Necesario";
        public static string CLIENTE = "Cliente";
        public static string SERIE = "Serie";
        public static string OBSERVACION = "Observacion";
        public static string FECHA = "Fecha";
        public static string ELIMINADO = "Eliminado";
        public static string IDUSUARIO = "IdUsuario";
        public static string IDMOVIMIENTO = "IdMovimiento";
        public static string CARGA = "Carga";
        public static string FECHACARGA = "FechaCarga";
    }

    public EspeciesFiscalesInstruccionesBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = " Select * from EspeciesFiscalesInstrucciones EFI ";
        this.initializeSchema("EspeciesFiscalesInstrucciones");
    }

    public void loadEspeciesFiscalesInstrucciones()
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' ";
        this.loadSQL(sql);
    }

    public void loadEspeciesFiscalesInstruccionesXItem(string item)
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' AND Item = " + item;
        this.loadSQL(sql);
    }

    public void loadEspeciesFiscalesInstruccionesInstrucciones(string idInstruccion, string codEspecieFiscal)
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' AND IdInstruccion = '" + idInstruccion + "' AND CodEspecieFiscal = '" + codEspecieFiscal + "' ";
        this.loadSQL(sql);
    }

    public void loadEspeciesFiscalesInstruccionesInstrucciones(string idInstruccion, string codEspecieFiscal, string serie)
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' AND IdInstruccion = '" + idInstruccion + "' AND CodEspecieFiscal = '" + codEspecieFiscal + "' ";
        sql += " AND Serie = '" + serie + "' ";
        this.loadSQL(sql);
    }

    public void loadEspeciesFiscalesInstruccionesXInstruccion(string idInstruccion)
    {
        string sql = this.coreSQL;
        sql += " WHERE Eliminado = '0' AND IdInstruccion = '" + idInstruccion + "' ORDER BY 1 ";
        this.loadSQL(sql);
    }

    public void loadSerieEFIEnUso(string serie, string idEspecieFiscal)
    {
        string sql = " SELECT * FROM [AduanasNueva].[dbo].[EspeciesFiscalesInstrucciones] EFI ";
        sql += " INNER JOIN [AduanasNueva].[dbo].[Instrucciones] I ON (I.IdInstruccion = EFI.IdInstruccion) ";
        sql += " WHERE I.IdEstadoFlujo != '99' AND EFI.Serie = '" + serie + "' AND EFI.CodEspecieFiscal = '" + idEspecieFiscal + "' ";
        sql += " AND EFI.Necesario = 'X' AND EFI.Eliminado = '0' ";
        this.loadSQL(sql);
    }

    //public int inicializarPermiso(string idInstruccion, string codPermiso)
    //{
    //    string sql = " UPDATE PermisosInstrucciones SET Necesario = NULL, PermisoNo = NULL, Observacion = NULL, Fecha = NULL, IdUsuario = NULL ";
    //    sql += " WHERE IdInstruccion = '" + idInstruccion + "' AND CodPermiso = '" + codPermiso + "' ";
    //    return this.executeCustomSQL(sql);
    //}

    public void ConsultaVaria(string serie, string codaduana, string Especie)
    {
        string sql = @" SELECT A.[IdInstruccion] as HojaRuta,C.Nombre as Cliente,D.Descripcion as Especie 
          ,Case [Necesario] when  'X' then 'Especie de Vesta' else 'No hay Especie de Vesta' end as Vesta 
          ,Case [Cliente]  when  'X' then 'Especie del Cliente' else 'No hay Especie del Cliente' end as ClienteB 
          ,[Serie],A.[Observacion],convert(varchar,A.[Fecha],103) as Fecha,E.Nombre +' '+Apellido as Asignado FROM [AduanasNueva].[dbo].[EspeciesFiscalesInstrucciones] A 
          Inner Join Instrucciones B on (A.IdInstruccion = B.IdInstruccion) Inner Join Clientes C on (B.IdCliente = C.CodigoSAP) 
          Inner Join Codigos D on (D.Categoria = 'ESPECIESFISCALES' and D.Codigo = A.CodEspecieFiscal) Inner Join Usuarios E on (A.IdUsuario = E.IdUsuario) 
          Where Serie like '%" + serie + "%' and B.CodigoAduana like '%" + codaduana + "%' and A.[CodEspecieFiscal] = '" + Especie + "' and A.Eliminado != '1' order by A.IdInstruccion desc";
        this.loadSQL(sql);
    }

    public void ConsultaAnulaciones(string serie, string codaduana, string Especie)
    {
        string sql = @" SELECT A.[IdInstruccion] as HojaRuta,C.Nombre as Cliente,D.Descripcion as Especie 
          ,Case [Necesario] when  'X' then 'Especie de Vesta' else 'No hay Especie de Vesta' end as Vesta 
          ,Case [Cliente]  when  'X' then 'Especie del Cliente' else 'No hay Especie del Cliente' end as ClienteB 
          ,[Serie],A.[Observacion],convert(varchar,A.[Fecha],103) as Fecha,E.Nombre +' '+Apellido as Asignado FROM [AduanasNueva].[dbo].[EspeciesFiscalesInstrucciones] A 
          Inner Join Instrucciones B on (A.IdInstruccion = B.IdInstruccion) Inner Join Clientes C on (B.IdCliente = C.CodigoSAP) 
          Inner Join Codigos D on (D.Categoria = 'ESPECIESFISCALES' and D.Codigo = A.CodEspecieFiscal) Inner Join Usuarios E on (A.IdUsuario = E.IdUsuario) 
          Where Serie like '%" + serie + "%' and B.CodigoAduana like '%" + codaduana + "%' and A.[CodEspecieFiscal] = '" + Especie + "' and A.Eliminado != '1' order by A.IdInstruccion desc";
        this.loadSQL(sql);
    }

    public void ConsultaCompleta(string serie, string codaduana, string Especie)
    {
        string sql = @" SELECT A.[IdInstruccion] as HojaRuta,C.Nombre as Cliente,D.Descripcion as Especie 
          ,Case [Necesario] when  'X' then 'Especie de Vesta' else 'No hay Especie de Vesta' end as Vesta 
          ,Case [Cliente]  when  'X' then 'Especie del Cliente' else 'No hay Especie del Cliente' end as ClienteB 
          ,[Serie],A.[Observacion],convert(varchar,A.[Fecha],103) as Fecha,E.Nombre +' '+Apellido as Asignado FROM [AduanasNueva].[dbo].[EspeciesFiscalesInstrucciones] A 
          Inner Join Instrucciones B on (A.IdInstruccion = B.IdInstruccion) Inner Join Clientes C on (B.IdCliente = C.CodigoSAP) 
          Inner Join Codigos D on (D.Categoria = 'ESPECIESFISCALES' and D.Codigo = A.CodEspecieFiscal) Inner Join Usuarios E on (A.IdUsuario = E.IdUsuario) 
          Where Serie like '%" + serie + "%' and B.CodigoAduana like '%" + codaduana + "%' and A.[CodEspecieFiscal] = '" + Especie + "' and A.Eliminado != '1' order by A.IdInstruccion desc";
        this.loadSQL(sql);
    }


    public void CargaUso()
    {
        string sql = @" SELECT Convert(Varchar,A.Fecha,104) as 'Fec.docu.' ,'SA' as 'Clase Dto.','9095' as Sociedad,Convert(Varchar,GETDATE(),104) as 'Fec.Con.','10' as Periodo
        ,'HNL' as Moneda,'1000010' as 'No.Dto.','USO ESPECIE' as Referencia,'USO ESPECIE FISCALES' +' '+ B.NombreAduana 'TxtCabDto','9095' as 'Divi.'
        , '50' as CLAVE,Case When E.IdUsuario = '62' Then '11411010' When E.IdUsuario = '402' Then '11411027' Else C.Codigo End as Codigo
        ,[Serie] as Asignacion, 'Uso '+D.Descripcion +'El Cliente'+ F.Nombre as Texto
        ,A.IdInstruccion as 'Referencia 3',[CodEspecieFiscal], case When A.CodEspecieFiscal in ('HN-MN','SS','HN-MI') Then '11312065'  else '11312030' end as Cuenta2
        ,[Necesario],[Cliente],A.[Observacion],A.[Fecha],A.[Eliminado],A.[IdUsuario],[IdMovimiento],A.[Carga], Serie, '40' as CLAVE2, A.CodEspecieFiscal as CodigoEspecieFiscal
        FROM EspeciesFiscalesInstrucciones  A Inner Join Aduanas B on ( Convert(Varchar(5) ,A.IdInstruccion) = ('H-'+B.CodigoAduana) and B.CodEstado = 0 and A.Serie !='')
        Inner Join Codigos C on (C.Categoria = 'INVENTARIO' and C.Descripcion = B.NombreAduana and B.CodEstado = 0 and C.Eliminado = 0)
        Inner Join Codigos D on (D.Categoria = 'ESPECIESFISCALES' and D.Codigo = A.CodEspecieFiscal and A.Eliminado = 0 and D.Eliminado = 0)  
        Inner Join Instrucciones E on (A.IdInstruccion = E.IdInstruccion and E.CodEstado in (0,1) and E.CodPaisHojaRuta = 'H')
        Inner Join Clientes F on (E.IdCliente = F.CodigoSAP and F.Eliminado = 0) Where A.Carga = 0 and Necesario ='X' and Cliente is NULL And A.Fecha >=  '2014-08-27'
        Union
         SELECT Convert(Varchar,A.FechaResolvio,104) as 'Fec.docu.' ,'SA' as 'Clase Dto.','9095' as Sociedad,Convert(Varchar,GETDATE(),104) as 'Fec.Con.','10' as Periodo
        ,'HNL' as Moneda,'1000010' as 'No.Dto.','USO ESPECIE' as Referencia,'USO ESPECIE FISCALES' +' '+ B.NombreAduana 'TxtCabDto','9095' as 'Divi.'
        , '50' as CLAVE,Case When E.IdUsuario = '62' Then '11411010' When E.IdUsuario = '402' Then '11411027' Else C.Codigo End as Codigo
        ,Series as Asignacion, 'Uso '+D.Descripcion +'El Cliente'+ F.Nombre as Texto
        ,A.IdInstruccion as 'Referencia 3',CodEspecie, case When A.CodEspecie in ('HN-MN','SS','HN-MI') Then '11312065'  else '11312030' end as Cuenta2
        ,'X','','',A.FechaResolvio,A.Estado,A.[UsuarioAsignoEspecie],'',A.Estado, Series, '40' as CLAVE2, A.CodEspecie as CodigoEspecieFiscal
        FROM Correcciones  A Inner Join Aduanas B on ( Convert(Varchar(5) ,A.IdInstruccion) = ('H-'+B.CodigoAduana) and B.CodEstado = 0 and A.Series !='')
        Inner Join Codigos C on (C.Categoria = 'INVENTARIO' and C.Descripcion = B.NombreAduana and B.CodEstado = 0 and C.Eliminado = 0)
        Inner Join Codigos D on (D.Categoria = 'ESPECIESFISCALES' and D.Codigo = A.CodEspecie and  A.Estado = 0 and D.Eliminado = 0 and A.Partida = 3)  
        Inner Join Instrucciones E on (A.IdInstruccion = E.IdInstruccion and E.CodEstado in (0,1) and E.CodPaisHojaRuta = 'H')
        Inner Join Clientes F on (E.IdCliente = F.CodigoSAP and F.Eliminado = 0)";



        //sql += " Inner Join Clientes F on (E.IdCliente = F.CodigoSAP and F.Eliminado = 0) Where A.Carga = 0 and Necesario ='X' and Cliente is NULL And A.Fecha between '2014-01-01' and '2014-01-07' order by A.IdInstruccion";
        this.loadSQL(sql);
        /*string sql = " SELECT Convert(Varchar,A.Fecha,104) as 'Fec.docu.' ,'SA' as 'Clase Dto.','9095' as Sociedad,Convert(Varchar,GETDATE(),104) as 'Fec.Con.','10' as Periodo";
        sql += " ,'HNL' as Moneda,'1000010' as 'No.Dto.','USO ESPECIE' as Referencia,'USO ESPECIE FISCALES' +' '+ B.NombreAduana 'TxtCabDto','9095' as 'Divi.'";
        sql += " , '50' as CLAVE,Case When E.IdCliente = '8000079' Then '11411020' When E.IdCliente = '8000423' Then '1111213' When E.IdCliente = '8000338' Then '1111215' When E.IdCliente = '8000006'";
        sql += " Then '11411027' When E.IdUsuario = '62' Then '11411010' Else C.Codigo End as Codigo";
        sql += " ,[Serie] as Asignacion, 'Uso '+D.Descripcion +'El Cliente'+ F.Nombre as Texto";
        sql += " ,A.IdInstruccion as 'Referencia 3',[CodEspecieFiscal], case When A.CodEspecieFiscal in ('HN-MN','SS','HN-MI') Then '11312065'  else '11312030' end as Cuenta2";
        sql += " ,[Necesario],[Cliente],A.[Observacion],A.[Fecha],A.[Eliminado],A.[IdUsuario],[IdMovimiento],A.[Carga], Serie, '40' as CLAVE2, A.CodEspecieFiscal as CodigoEspecieFiscal";
        sql += " FROM EspeciesFiscalesInstrucciones  A Inner Join Aduanas B on ( Convert(Varchar(5) ,A.IdInstruccion) = ('H-'+B.CodigoAduana) and B.CodEstado = 0 and A.Serie !='')";
        sql += " Inner Join Codigos C on (C.Categoria = 'INVENTARIO' and C.Descripcion = B.NombreAduana and B.CodEstado = 0 and C.Eliminado = 0)";
        sql += " Inner Join Codigos D on (D.Categoria = 'ESPECIESFISCALES' and D.Codigo = A.CodEspecieFiscal and A.Eliminado = 0 and D.Eliminado = 0)  ";
        sql += " Inner Join Instrucciones E on (A.IdInstruccion = E.IdInstruccion and E.CodEstado in (0,1) and E.CodPaisHojaRuta = 'H')";
        sql += " Inner Join Clientes F on (E.IdCliente = F.CodigoSAP and F.Eliminado = 0) Where A.Carga = 0 and Necesario ='X' and Cliente is NULL And A.Fecha >=  '2014-08-27' order by A.IdInstruccion";
        this.loadSQL(sql);*/
    }
    public void CargaUsoCambiaEstado()
    {
        string sql = " Update EspeciesFiscalesInstrucciones Set Carga = '1', FechaCarga = GetDate()";
        sql += " From EspeciesFiscalesInstrucciones A Inner Join Aduanas B on ( Convert(Varchar(5) ,A.IdInstruccion) = ('H-'+B.CodigoAduana) and B.CodEstado = 0 and A.Serie !='') ";
        sql += " Inner Join Codigos C on (C.Categoria = 'INVENTARIO' and C.Descripcion = B.NombreAduana and B.CodEstado = 0 and C.Eliminado = 0) ";
        sql += " Inner Join Codigos D on (D.Categoria = 'ESPECIESFISCALES' and D.Codigo = A.CodEspecieFiscal and A.Eliminado = 0 and D.Eliminado = 0) ";
        sql += " Left Join Instrucciones E on (A.IdInstruccion = E.IdInstruccion and E.CodEstado in (0,1) and E.CodPaisHojaRuta = 'H') ";
        sql += " Left Join Clientes F on (E.IdCliente = F.CodigoSAP and F.Eliminado = 0) ";
        sql += " Where A.Carga = 0 and Necesario ='X' and Cliente is NULL And A.Fecha >=  '2014-08-27'";
        this.loadSQL(sql);
    }
    public void CargaUsoCambios()
    {
        string sql = " SELECT Convert(Varchar,A.Fecha,104) as 'Fec.docu.' ,'SA' as 'Clase Dto.','9095' as Sociedad,Convert(Varchar,GETDATE(),104) as 'Fec.Con.','10' as Periodo";
        sql += " ,'HNL' as Moneda,'1000010' as 'No.Dto.','USO ESPECIE' as Referencia,'USO ESPECIE FISCALES' +' '+ B.NombreAduana 'TxtCabDto','9095' as 'Divi.'";
        sql += " , '50' as CLAVE, '40' as CLAVE2, case When A.CodEspecieFiscal in ('HN-MN','SS','HN-MI') Then '11312065'  else '11312030' end as Cuenta2,";
        sql += " A.IdInstruccionCambio as Asignacion2, A.IdInstruccion as Asignacion1,";
        sql += " C.Codigo,[Serie] as 'Referencia 3', 'Uso '+D.Descripcion +'El Cliente '+ F.Nombre as Texto1,'Uso '+D.Descripcion +'El Cliente '+ H.Nombre as Texto2";
        sql += " ,[CodEspecieFiscal],[Necesario],[Cliente],A.[Observacion],A.[Fecha],A.[Eliminado],A.[IdUsuario],[IdMovimiento],A.[Carga], Serie, A.CodEspecieFiscal as CodigoEspecieFiscal";
        sql += " FROM EspeciesFiscalesInstrucciones  A Inner Join Aduanas B on ( Convert(Varchar(5) ,A.IdInstruccion) = ('H-'+B.CodigoAduana) and B.CodEstado = 0 and A.Serie !='')";
        sql += " Inner Join Codigos C on (C.Categoria = 'INVENTARIO' and C.Descripcion = B.NombreAduana and B.CodEstado = 0 and C.Eliminado = 0)";
        sql += " Inner Join Codigos D on (D.Categoria = 'ESPECIESFISCALES' and D.Codigo = A.CodEspecieFiscal and A.Eliminado = 0 and D.Eliminado = 0)";
        sql += " Inner Join Instrucciones E on (A.IdInstruccion = E.IdInstruccion and E.CodEstado in (0,1) and E.CodPaisHojaRuta = 'H')";
        sql += " Inner Join Clientes F on (E.IdCliente = F.CodigoSAP and F.Eliminado = 0)";
        sql += " Inner Join Instrucciones G on (A.IdInstruccionCambio = G.IdInstruccion and G.CodEstado in (0,1) and G.CodPaisHojaRuta = 'H')";
        sql += " Inner Join Clientes H on (G.IdCliente = H.CodigoSAP and F.Eliminado = 0)";
        sql += " Where A.Carga = 2 and Necesario ='X' and Cliente is NULL And A.Fecha >=  '2014-08-27'";
        this.loadSQL(sql);
    }
    public void CargaUsoCambiosCambiaEstado()
    {
        string sql = " Update EspeciesFiscalesInstrucciones Set Carga = '1', FechaCarga = GetDate()";
        sql += " FROM EspeciesFiscalesInstrucciones  A Inner Join Aduanas B on ( Convert(Varchar(5) ,A.IdInstruccion) = ('H-'+B.CodigoAduana) and B.CodEstado = 0 and A.Serie !='')";
        sql += " Inner Join Codigos C on (C.Categoria = 'INVENTARIO' and C.Descripcion = B.NombreAduana and B.CodEstado = 0 and C.Eliminado = 0)";
        sql += " Inner Join Codigos D on (D.Categoria = 'ESPECIESFISCALES' and D.Codigo = A.CodEspecieFiscal and A.Eliminado = 0 and D.Eliminado = 0)";
        sql += " Inner Join Instrucciones E on (A.IdInstruccion = E.IdInstruccion and E.CodEstado in (0,1) and E.CodPaisHojaRuta = 'H')";
        sql += " Inner Join Clientes F on (E.IdCliente = F.CodigoSAP and F.Eliminado = 0)";
        sql += " Inner Join Instrucciones G on (A.IdInstruccionCambio = G.IdInstruccion and G.CodEstado in (0,1) and G.CodPaisHojaRuta = 'H')";
        sql += " Inner Join Clientes H on (G.IdCliente = H.CodigoSAP and F.Eliminado = 0)";
        sql += " Where A.Carga = 2 and Necesario ='X' and Cliente is NULL And A.Fecha >=  '2014-08-27'";
        this.loadSQL(sql);
    }
    public void cargarespeciesxinstruccion(string especie, string hoja)
    {
        string sql = @"SELECT IdInstruccion  collate DATABASE_DEFAULT as IdInstruccion,CodEspecieFiscal ,Serie collate DATABASE_DEFAULT as Serie,IdUsuario,Eliminado FROM EspeciesFiscalesInstrucciones
                      Where IdInstruccion = '" + hoja + "' and CodEspecieFiscal = '" + especie + "' and Eliminado = 0 and Necesario = 'X' and Cliente is NULL";
//        if (especie == "P" || especie == "ZL")
//        {
//            sql += @" Union 
//                   Select NTramite  collate DATABASE_DEFAULT,'P',Preimpreso collate DATABASE_DEFAULT,IdUsuario,Estado collate DATABASE_DEFAULT  FROM [Maersk].[dbo].[Tramite]
//                   Where NTramite = '" + hoja + "' and Preimpreso IS NOT NULL and Estado in (0,1,2,3)";
//        }
//        else if (especie == "DA")
//        {
//            sql += @" Union 
//                   Select NTramite  collate DATABASE_DEFAULT,'P',Extension collate DATABASE_DEFAULT,IdUsuario,Estado collate DATABASE_DEFAULT  FROM [Maersk].[dbo].[Tramite]
//                   Where NTramite = '" + hoja + "' and Estado in (0,1,2,3)";
//        }
        this.loadSQL(sql);
    }

    public void cargarespeciesxinstruccionSellos(string hoja)
    {
        string sql = @"SELECT IdInstruccion  collate DATABASE_DEFAULT as IdInstruccion,CodEspecieFiscal ,Serie collate DATABASE_DEFAULT as Serie,IdUsuario,Eliminado FROM EspeciesFiscalesInstrucciones
                      Where IdInstruccion = '" + hoja + "' and ( CodEspecieFiscal = 'SS' or CodEspecieFiscal = 'SSB' ) and Eliminado = 0";

        this.loadSQL(sql);
    }

    public void cargarespeciesxinstruccionNaviero(string especie, string hoja)
    {

        string sql = @"SELECT IdInstruccion  collate DATABASE_DEFAULT as IdInstruccion,CodEspecieFiscal ,Serie collate DATABASE_DEFAULT as Serie,IdUsuario,Eliminado FROM [Maersk].[dbo].EspeciesFiscalesInstrucciones
                       Where IdInstruccion = '" + hoja + "' and CodEspecieFiscal = '" + especie + "' and Eliminado = 0 and Necesario = 'X' and Cliente is NULL";
       if (especie == "P" || especie == "ZL")
        {
            sql += @" union Select NTramite  collate DATABASE_DEFAULT,'P',Preimpreso collate DATABASE_DEFAULT,IdUsuario,Estado collate DATABASE_DEFAULT  FROM [Maersk].[dbo].[Tramite]
                   Where NTramite = '" + hoja + "' and Preimpreso IS NOT NULL and Estado in (0,1,2,3)";
        }
        else if (especie == "DA")
        {
            sql += @" union Select NTramite  collate DATABASE_DEFAULT,'P',Extension collate DATABASE_DEFAULT,IdUsuario,Estado collate DATABASE_DEFAULT  FROM [Maersk].[dbo].[Tramite]
                   Where NTramite = '" + hoja + "' and Estado in (0,1,2,3)";
        }
        this.loadSQL(sql);
    }

    public void CargarEspeciesxInstruccionParaEliminar(string especie, string hoja)
    {
        string sql = @"SELECT * FROM EspeciesFiscalesInstrucciones
                      Where IdInstruccion = '" + hoja + "' and CodEspecieFiscal = '" + especie + "' and Eliminado = 0 and Necesario = 'X' and Cliente is NULL";
        this.loadSQL(sql);
    }

    public void CargarUsoNohelia(string FechaInicio, string FechaFinal, string CodEspecieFiscal)
    {
        string sql = @"Select A.IdInstruccion as Hoja,C.Nombre as Cliente,D.NombreAduana as Aduana,Serie as Serie, E.Descripcion as Especie,A.Fecha as Fecha
                       From EspeciesFiscalesInstrucciones A
                       Inner Join Instrucciones B on (A.IdInstruccion = B.IdInstruccion)
                       Inner Join Aduanas D on (B.CodigoAduana = D.CodigoAduana)
                       Inner Join Clientes C on (B.IdCliente = C.CodigoSAP)
                       Inner Join Codigos E on (E.Categoria = 'ESPECIESFISCALES' and E.Codigo= A.CodEspecieFiscal)
                       Where A.Fecha >= '" + FechaInicio + "' and A.Fecha <='" + FechaFinal + "' and A.CodEspecieFiscal = '" + CodEspecieFiscal + "' and Necesario ='X' and A.Eliminado = 0";
        this.loadSQL(sql);
    }
    public void CargarUsoNohelia(string FechaInicio, string FechaFinal)
    {
        string sql = @"Select A.IdInstruccion as Hoja,C.Nombre as Cliente,D.NombreAduana as Aduana,Serie as Serie, E.Descripcion as Especie,A.Fecha as Fecha
                       From EspeciesFiscalesInstrucciones A
                       Inner Join Instrucciones B on (A.IdInstruccion = B.IdInstruccion)
                       Inner Join Aduanas D on (B.CodigoAduana = D.CodigoAduana)
                       Inner Join Clientes C on (B.IdCliente = C.CodigoSAP)
                       Inner Join Codigos E on (E.Categoria = 'ESPECIESFISCALES' and E.Codigo= A.CodEspecieFiscal)
                       Where A.Fecha >= '" + FechaInicio + "' and A.Fecha <='" + FechaFinal + "' and Necesario ='X' and A.Eliminado = 0";
        this.loadSQL(sql);
    }

    #region Campos
    public string ITEM
    {
        get
        {
            return (string)registro[Campos.ITEM].ToString();
        }
    }

    public string IDINSTRUCCION
    {
        get
        {
            return (string)registro[Campos.IDINSTRUCCION].ToString();
        }
        set
        {
            registro[Campos.IDINSTRUCCION] = value;
        }
    }

    public string CODESPECIEFISCAL
    {
        get
        {
            return (string)registro[Campos.CODESPECIEFISCAL].ToString();
        }
        set
        {
            registro[Campos.CODESPECIEFISCAL] = value;
        }
    }

    public string NECESARIO
    {
        get
        {
            return (string)registro[Campos.NECESARIO].ToString();
        }
        set
        {
            registro[Campos.NECESARIO] = value;
        }
    }

    public string CLIENTE
    {
        get
        {
            return (string)registro[Campos.CLIENTE].ToString();
        }
        set
        {
            registro[Campos.CLIENTE] = value;
        }
    }

    public string SERIE
    {
        get
        {
            return (string)registro[Campos.SERIE].ToString();
        }
        set
        {
            registro[Campos.SERIE] = value;
        }
    }

    public string OBSERVACION
    {
        get
        {
            return (string)registro[Campos.OBSERVACION].ToString();
        }
        set
        {
            registro[Campos.OBSERVACION] = value;
        }
    }

    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }

    public string ELIMINADO
    {
        get
        {
            return (string)registro[Campos.ELIMINADO].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO] = value;
        }
    }

    public string IDUSUARIO
    {
        get
        {
            return (string)registro[Campos.IDUSUARIO].ToString();
        }
        set
        {
            registro[Campos.IDUSUARIO] = value;
        }
    }

    public string IDMOVIMIENTO
    {
        get
        {
            return (string)registro[Campos.IDMOVIMIENTO].ToString();
        }
        set
        {
            registro[Campos.IDMOVIMIENTO] = value;
        }
    }
    public string CARGA
    {
        get
        {
            return (string)registro[Campos.CARGA].ToString();
        }
        set
        {
            registro[Campos.CARGA] = value;
        }
    }
    public string FECHACARGA
    {
        get
        {
            return (string)registro[Campos.FECHACARGA].ToString();
        }
        set
        {
            registro[Campos.FECHACARGA] = value;
        }
    }
    #endregion
}
