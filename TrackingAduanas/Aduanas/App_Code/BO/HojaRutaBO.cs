﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for RemisionesBO
/// </summary>
public class HojaRutaBO : CapaBase
{
    class Campos
    {
        public static string IDHOJARUTA = "IdHojaRuta";
        public static string FECHARECEPCION = "FechaRecepcion";
        public static string CODIGOADUANA = "CodigoAduana";
        public static string CODIGOCLIENTE = "CodigoCliente";
        public static string REFERENCIACLIENTE = "ReferenciaCliente";
        public static string PROVEEDOR = "Proveedor";
        public static string NUMFACTURA = "NumFactura";
        public static string DESCRIPCIONPRODUCTO = "DescripcionProducto";
        public static string IDREMITENTE = "IdRemitente";
        public static string IDRECEPTOR = "IdReceptor";
        public static string IDAFORADOR = "IdAforador";
        public static string IDTRAMITADOR = "IdTramitador";
        public static string NOMBREMOTORISTA = "Motorista";
        public static string EMPRESATRANSPORTE = "EmpresaTransporte";
        public static string CONTENEDOR = "Contenedor";
        public static string PLACACABEZAL = "PlacaCabezal";
        public static string PLACAFURGON = "PlacaFurgon";
        public static string CONTACTOCLIENTE = "ContactoCliente";
        public static string INSTRUCCIONESENTREGA = "InstruccionesEntrega";
        public static string OBSERVACIONES = "Observaciones";
        public static string IDFLUJO = "IdFlujo";
        public static string ESTADO = "Estado";
    }

    public HojaRutaBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from HojaRuta";
        this.initializeSchema("HojaRuta");
    }

    public string CODIGOADUANA
    {
        get
        {
            return registro[HojaRutaBO.Campos.CODIGOADUANA].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.CODIGOADUANA] = value;
        }
    }

    public string CODIGOCLIENTE
    {
        get
        {
            return registro[HojaRutaBO.Campos.CODIGOCLIENTE].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.CODIGOCLIENTE] = value;
        }
    }

    public string CONTACTOCLIENTE
    {
        get
        {
            return registro[HojaRutaBO.Campos.CONTACTOCLIENTE].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.CONTACTOCLIENTE] = value;
        }
    }

    public string CONTENEDOR
    {
        get
        {
            return registro[HojaRutaBO.Campos.CONTENEDOR].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.CONTENEDOR] = value;
        }
    }

    public string DESCRIPCIONPRODUCTO
    {
        get
        {
            return registro[HojaRutaBO.Campos.DESCRIPCIONPRODUCTO].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.DESCRIPCIONPRODUCTO] = value;
        }
    }

    public string EMPRESATRANSPORTE
    {
        get
        {
            return registro[HojaRutaBO.Campos.EMPRESATRANSPORTE].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.EMPRESATRANSPORTE] = value;
        }
    }

    public string ESTADO
    {
        get
        {
            return registro[HojaRutaBO.Campos.ESTADO].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.ESTADO] = value;
        }
    }

    public string FECHARECEPCION
    {
        get
        {
            return registro[HojaRutaBO.Campos.FECHARECEPCION].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.FECHARECEPCION] = value;
        }
    }

    public string IDAFORADOR
    {
        get
        {
            return registro[HojaRutaBO.Campos.IDAFORADOR].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.IDAFORADOR] = value;
        }
    }

    public string IDTRAMITADOR
    {
        get
        {
            return registro[HojaRutaBO.Campos.IDTRAMITADOR].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.IDTRAMITADOR] = value;
        }
    }

    public string IDFLUJO
    {
        get
        {
            return registro[HojaRutaBO.Campos.IDFLUJO].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.IDFLUJO] = value;
        }
    }

    public string IDHOJARUTA
    {
        get
        {
            return registro[HojaRutaBO.Campos.IDHOJARUTA].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.IDHOJARUTA] = value;
        }
    }

    public string IDRECEPTOR
    {
        get
        {
            return registro[HojaRutaBO.Campos.IDRECEPTOR].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.IDRECEPTOR] = value;
        }
    }

    public string IDREMITENTE
    {
        get
        {
            return registro[HojaRutaBO.Campos.IDREMITENTE].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.IDREMITENTE] = value;
        }
    }

    public string INSTRUCCIONESENTREGA
    {
        get
        {
            return registro[HojaRutaBO.Campos.INSTRUCCIONESENTREGA].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.INSTRUCCIONESENTREGA] = value;
        }
    }

    public string NOMBREMOTORISTA
    {
        get
        {
            return registro[HojaRutaBO.Campos.NOMBREMOTORISTA].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.NOMBREMOTORISTA] = value;
        }
    }

    public string NUMFACTURA
    {
        get
        {
            return registro[HojaRutaBO.Campos.NUMFACTURA].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.NUMFACTURA] = value;
        }
    }

    public string OBSERVACIONES
    {
        get
        {
            return registro[HojaRutaBO.Campos.OBSERVACIONES].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.OBSERVACIONES] = value;
        }
    }

    public string PLACACABEZAL
    {
        get
        {
            return registro[HojaRutaBO.Campos.PLACACABEZAL].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.PLACACABEZAL] = value;
        }
    }

    public string PLACAFURGON
    {
        get
        {
            return registro[HojaRutaBO.Campos.PLACAFURGON].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.PLACAFURGON] = value;
        }
    }

    public string PROVEEDOR
    {
        get
        {
            return registro[HojaRutaBO.Campos.PROVEEDOR].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.PROVEEDOR] = value;
        }
    }

    public string REFERENCIACLIENTE
    {
        get
        {
            return registro[HojaRutaBO.Campos.REFERENCIACLIENTE].ToString();
        }
        set
        {
            registro[HojaRutaBO.Campos.REFERENCIACLIENTE] = value;
        }
    }


    public int darBajaRemision(string codigo)
    {
        return executeCustomSQL("update HojaRuta set Estado = '1' where IdHojaRuta = '" + codigo + "'");
    }

    public void detalleControlDiario(string codigoAduana, string fecha, string idFlujo)
    {
        string s = "declare @Duration as DateTime";
        s += " set @Duration = GETDATE()";
        s += " select NombreAduana as Aduana, NombreCliente as Cliente, r.IdHojaRuta, NumFactura as Factura, Proveedor, DescripcionProducto as Producto, CodigoRegimen as Regimen,cod.descripcion as FlujoActual,";
        s += " ('Dias: ' + cast((DateDiff(second, t.FechaProceso, @Duration)) / 86400 as varchar(50)) + ' Horas: ' + cast(((DateDiff(second, t.FechaProceso, @Duration)) % 86400) / 3600 as varchar(50)) + ' Mins: ' + cast(((DateDiff(second, t.FechaProceso, @Duration)) % 3600) / 60 as varchar(50))) as TiempoFlujo";
        s += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s = s + " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta and t.IdFlujo = " + idFlujo + ")";
        s += " inner join Clientes c on (r.CodigoCliente = c.CodigoCliente)";
        s += " left join ComplementoRecepcion as com on (r.IdHojaRuta = com.IdHojaRuta)";
        s += " inner join Codigos as cod on (cod.codigo = r.IdFlujo and cod.clasificacion = 'FLUJO')";
        s = s + " where CONVERT(VARCHAR, FechaProceso, 111) = '" + Convert.ToDateTime(fecha).ToString("yyyy/MM/dd") + "'  and r.Estado = '0' ";
        s = s + " and r.CodigoAduana = '" + codigoAduana + "'";
        s += " order by NombreCliente, r.IdHojaRuta";
        loadSQL(s);
    }

    public void loadBusquedaHojaRuta(string campos, string criterio)
    {
        string s = "declare @Duration as DateTime";
        s += " set @Duration = GETDATE()";
        if (campos.Length > 0)
            s = s + " select a.CodigoPais as Pais, r.IdHojaRuta as Codigo" + campos;
        else
            s += " select a.CodigoPais as Pais, r.IdHojaRuta as Codigo, FechaRecepcion as [Fecha-R], NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, e.NombreEmpleado as Gestor, cod.descripcion as Flujo,";
        s += " (case when tf.IdFlujo != '6' then ('Dias: ' + cast((DateDiff(second, tf.FechaProceso, @Duration)) / 86400 as varchar(50)) + ' Horas: ' + cast(((DateDiff(second, tf.FechaProceso, @Duration)) % 86400) / 3600 as varchar(50)) + ' Mins: ' + cast(((DateDiff(second, tf.FechaProceso, @Duration)) % 3600) / 60 as varchar(50))) else ('Dias: 0 Horas: 0 Mins: 0') end) as TiempoFlujo";
        s += " from HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        s += " left join Empleados as af on (r.IdAforador = af.IdEmpleado)";
        s += " left join Empleados as tr on (r.IdTramitador = tr.IdEmpleado)";
        s += " left join ComplementoRecepcion as com on (com.IdHojaRuta = r.IdHojaRuta)";
        s += " inner join TiempoFlujo as tf on (tf.IdHojaRuta = r.IdHojaRuta and tf.IdFlujo = r.IdFlujo)";
        s += " inner join TiempoFlujo as t on (t.IdHojaRuta = r.IdHojaRuta and t.IdFlujo = 1)";
        s += " inner join Codigos as cod on (cod.codigo = r.IdFlujo and cod.clasificacion = 'FLUJO')";
        s += " left join Codigos as paises on (paises.codigo = isnull(com.CodigoPais, '9999') and paises.clasificacion = 'PAISES')";
        s += " left join Codigos as doc on (doc.codigo = isnull(com.CodigoDocEmbarque, '9999') and doc.clasificacion = 'DOCUMENTOEMBARQUE')";
        s += " left join (select r.IdHojaRuta, descripcion, Observacion, Fecha, ('Dias: ' + cast((DateDiff(second, Fecha, @Duration)) / 86400 as varchar(50)) + ' Horas: ' + cast(((DateDiff(second, Fecha, @Duration)) % 86400) / 3600 as varchar(50)) + ' Mins: ' + cast(((DateDiff(second, Fecha, @Duration)) % 3600) / 60 as varchar(50))) as TiempoEtapa, ta.Cod";
        s += " from HojaRuta r";
        s += " left join (select IdHojaRuta, max(Codigo) as Cod from TramiteAduanero where Codigo < 14 group by IdHojaRuta) as ta on (r.IdHojaRuta = ta.IdHojaRuta)";
        s += " inner join Codigos c on (isnull((case when ta.Cod < 14 then ta.Cod + 1 Else ta.Cod end),'1') = c.codigo and (clasificacion = 'PROCESOADUANERO' or clasificacion = 'PREPROCESO'))";
        s += " left join TramiteAduanero ta2 on (r.IdHojaRuta = ta2.IdHojaRuta and ta.Cod = ta2.Codigo) where (r.IdFlujo = 3 or r.IdFlujo = 2) and r.Estado = '0') as pa";
        s += " on (isnull(pa.IdHojaRuta,'9999') = r.IdHojaRuta)";
        s += " where " + criterio + " and r.Estado = '0'";
        loadSQL(s);
    }

    public void loadBusquedaHojaRuta(string campos, string criterio, string codigoAduana, string idEmpleado)
    {
        string s1 = "declare @Duration as DateTime";
        s1 += " set @Duration = GETDATE()";
        if (campos.Length > 0)
            s1 += " select a.CodigoPais as Pais, r.IdHojaRuta as Codigo" + campos;
        else
            s1 += " select a.CodigoPais as Pais, r.IdHojaRuta as Codigo, FechaRecepcion as [Fecha-R], NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor, cod.descripcion as Flujo,";
        s1 += " (case when tf.IdFlujo != '6' then ('Dias: ' + cast((DateDiff(second, tf.FechaProceso, @Duration)) / 86400 as varchar(50)) + ' Horas: ' + cast(((DateDiff(second, tf.FechaProceso, @Duration)) % 86400) / 3600 as varchar(50)) + ' Mins: ' + cast(((DateDiff(second, tf.FechaProceso, @Duration)) % 3600) / 60 as varchar(50))) else ('Dias: 0 Horas: 0 Mins: 0') end) as TiempoFlujo";
        s1 += " from HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        s1 += " inner join TiempoFlujo as tf on (tf.IdHojaRuta = r.IdHojaRuta and tf.IdFlujo = r.IdFlujo)";
        s1 += " inner join TiempoFlujo as t on (t.IdHojaRuta = r.IdHojaRuta and t.IdFlujo = 1)";
        s1 += " inner join Codigos as cod on (cod.codigo = r.IdFlujo and cod.clasificacion = 'FLUJO')";
        s1 += " where " + criterio + " and r.CodigoAduana = '" + codigoAduana + "' and r.IdReceptor = '" + idEmpleado + "' and r.Estado = '0'";
        loadSQL(s1);
    }

    public void loadBusquedaHojaRuta(string campos, string criterio, string codigoAduana)
    {
        string s1 = "declare @Duration as DateTime";
        s1 += " set @Duration = GETDATE()";
        if (campos.Length > 0)
            s1 += " select a.CodigoPais as Pais, r.IdHojaRuta as Codigo" + campos;
        else
            s1 += " select a.CodigoPais as Pais, r.IdHojaRuta as Codigo, FechaRecepcion as [Fecha-R], NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, e.NombreEmpleado as Gestor, cod.descripcion as Flujo,";
        s1 += " (case when tf.IdFlujo != '6' then ('Dias: ' + cast((DateDiff(second, tf.FechaProceso, @Duration)) / 86400 as varchar(50)) + ' Horas: ' + cast(((DateDiff(second, tf.FechaProceso, @Duration)) % 86400) / 3600 as varchar(50)) + ' Mins: ' + cast(((DateDiff(second, tf.FechaProceso, @Duration)) % 3600) / 60 as varchar(50))) else ('Dias: 0 Horas: 0 Mins: 0') end) as TiempoFlujo";
        s1 += " from HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        s1 += " left join Empleados as af on (r.IdAforador = af.IdEmpleado)";
        s1 += " left join Empleados as tr on (r.IdTramitador = tr.IdEmpleado)";
        s1 += " left join ComplementoRecepcion as com on (com.IdHojaRuta = r.IdHojaRuta)";
        s1 += " inner join TiempoFlujo as tf on (tf.IdHojaRuta = r.IdHojaRuta and tf.IdFlujo = r.IdFlujo)";
        s1 += " inner join TiempoFlujo as t on (t.IdHojaRuta = r.IdHojaRuta and t.IdFlujo = 1)";
        s1 += " inner join Codigos as cod on (cod.codigo = r.IdFlujo and cod.clasificacion = 'FLUJO')";
        s1 += " left join Codigos as paises on (paises.codigo = isnull(com.CodigoPais, '9999') and paises.clasificacion = 'PAISES')";
        s1 += " left join Codigos as doc on (doc.codigo = isnull(com.CodigoDocEmbarque, '9999') and doc.clasificacion = 'DOCUMENTOEMBARQUE')";
        s1 += " left join (select r.IdHojaRuta, descripcion, Observacion, Fecha, ('Dias: ' + cast((DateDiff(second, Fecha, @Duration)) / 86400 as varchar(50)) + ' Horas: ' + cast(((DateDiff(second, Fecha, @Duration)) % 86400) / 3600 as varchar(50)) + ' Mins: ' + cast(((DateDiff(second, Fecha, @Duration)) % 3600) / 60 as varchar(50))) as TiempoEtapa, ta.Cod";
        s1 += " from HojaRuta r";
        s1 += " left join (select IdHojaRuta, max(Codigo) as Cod from TramiteAduanero where Codigo < 14 group by IdHojaRuta) as ta on (r.IdHojaRuta = ta.IdHojaRuta)";
        s1 += " inner join Codigos c on (isnull((case when ta.Cod < 14 then ta.Cod + 1 Else ta.Cod end),'1') = c.codigo and (clasificacion = 'PROCESOADUANERO' or clasificacion = 'PREPROCESO'))";
        s1 += " left join TramiteAduanero ta2 on (r.IdHojaRuta = ta2.IdHojaRuta and ta.Cod = ta2.Codigo) where (r.IdFlujo = 3 or r.IdFlujo = 2) and r.Estado = '0') as pa";
        s1 += " on (isnull(pa.IdHojaRuta,'9999') = r.IdHojaRuta)";
        s1 += " where " + criterio + " and r.CodigoAduana = '" + codigoAduana + "' and r.Estado = '0'";
        loadSQL(s1);
    }

    public void loadBusquedaHojaRutaFacturacion(string criterio, string codigoRegion)
    {
        string s1 = "declare @Duration as DateTime";
        s1 += " set @Duration = GETDATE()";
        s1 += " select a.CodigoPais as Pais, r.IdHojaRuta as Codigo, FechaRecepcion as [Fecha-R], NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor, cod.descripcion as Flujo,";
        s1 += " (case when tf.IdFlujo != '6' then ('Dias: ' + cast((DateDiff(second, tf.FechaProceso, @Duration)) / 86400 as varchar(50)) + ' Horas: ' + cast(((DateDiff(second, tf.FechaProceso, @Duration)) % 86400) / 3600 as varchar(50)) + ' Mins: ' + cast(((DateDiff(second, tf.FechaProceso, @Duration)) % 3600) / 60 as varchar(50))) else ('Dias: 0 Horas: 0 Mins: 0') end) as Tiempo";
        s1 += " from HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        s1 += " inner join TiempoFlujo as tf on (tf.IdHojaRuta = r.IdHojaRuta and tf.IdFlujo = r.IdFlujo)";
        s1 += " inner join TiempoFlujo as t on (t.IdHojaRuta = r.IdHojaRuta and t.IdFlujo = 1)";
        s1 += " inner join Codigos as cod on (cod.codigo = r.IdFlujo and cod.clasificacion = 'FLUJO')";
        s1 += " where " + criterio + " and a.CodigoRegion = '" + codigoRegion + "' and r.Estado = '0'";
        loadSQL(s1);
    }

    public void loadBusquedaHojaRutaJefeFacturacion(string criterio, string codigoPais)
    {
        string s1 = "declare @Duration as DateTime";
        s1 += " set @Duration = GETDATE()";
        s1 += " select a.CodigoPais as Pais, r.IdHojaRuta as Codigo, FechaRecepcion as [Fecha-R], NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor, cod.descripcion as Flujo,";
        s1 += " (case when tf.IdFlujo != '6' then ('Dias: ' + cast((DateDiff(second, tf.FechaProceso, @Duration)) / 86400 as varchar(50)) + ' Horas: ' + cast(((DateDiff(second, tf.FechaProceso, @Duration)) % 86400) / 3600 as varchar(50)) + ' Mins: ' + cast(((DateDiff(second, tf.FechaProceso, @Duration)) % 3600) / 60 as varchar(50))) else ('Dias: 0 Horas: 0 Mins: 0') end) as Tiempo";
        s1 += " from HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        s1 += " inner join TiempoFlujo as tf on (tf.IdHojaRuta = r.IdHojaRuta and tf.IdFlujo = r.IdFlujo)";
        s1 += " inner join TiempoFlujo as t on (t.IdHojaRuta = r.IdHojaRuta and t.IdFlujo = 1)";
        s1 += " inner join Codigos as cod on (cod.codigo = r.IdFlujo and cod.clasificacion = 'FLUJO')";
        s1 += " where " + criterio + " and a.CodigoPais = '" + codigoPais + "' and r.Estado = '0'";
        loadSQL(s1);
    }

    public void loadBusquedaHojaRutaRemitente(string criterio, string idEmpleado)
    {
        string s1 = "declare @Duration as DateTime";
        s1 += " set @Duration = GETDATE()";
        s1 += " select r.IdHojaRuta as Codigo, FechaRecepcion as [Fecha-R], NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor, cod.descripcion as Flujo,";
        s1 += " (case when tf.IdFlujo != '6' then ('Dias: ' + cast((DateDiff(second, tf.FechaProceso, @Duration)) / 86400 as varchar(50)) + ' Horas: ' + cast(((DateDiff(second, tf.FechaProceso, @Duration)) % 86400) / 3600 as varchar(50)) + ' Mins: ' + cast(((DateDiff(second, tf.FechaProceso, @Duration)) % 3600) / 60 as varchar(50))) else ('Dias: 0 Horas: 0 Mins: 0') end) as Tiempo";
        s1 += " from HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        s1 += " inner join TiempoFlujo as tf on (tf.IdHojaRuta = r.IdHojaRuta and tf.IdFlujo = r.IdFlujo)";
        s1 += " inner join TiempoFlujo as t on (t.IdHojaRuta = r.IdHojaRuta and t.IdFlujo = 1)";
        s1 += " inner join Codigos as cod on (cod.codigo = r.IdFlujo and cod.clasificacion = 'FLUJO')";
        s1 += " where " + criterio + " and r.IdRemitente = '" + idEmpleado + "' and r.Estado = '0'";
        loadSQL(s1);
    }

    public void loadClientesTiempoTramiteAduanero(string fechaInicio, string fechaFinal, string criterio)
    {
        string s1 = "select a.CodigoPais, NombreAduana as Aduana, NombreCliente as Cliente, r.IdHojaRuta, NumFactura as Factura, Proveedor, DescripcionProducto as Producto, CodigoRegimen as Regimen, tp.Fecha as FechaEntrada, tf.Fecha as FechaSalida,";
        s1 += " ('Dias: ' + cast((DateDiff(second, tp.Fecha, tf.Fecha)) / 86400 as varchar(50)) + ' Horas: ' + cast(((DateDiff(second, tp.Fecha, tf.Fecha)) % 86400) / 3600 as varchar(50)) + ' Mins: ' + cast(((DateDiff(second, tp.Fecha, tf.Fecha)) % 3600) / 60 as varchar(50))) as [Tiempo Tramite]";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join Clientes c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join ComplementoRecepcion cm on (r.IdHojaRuta = cm.IdHojaRuta)";
        s1 += " inner join TramiteAduanero tp on (tp.IdHojaRuta = r.IdHojaRuta and tp.Codigo = '1')";
        s1 += " inner join TramiteAduanero tf on (tf.IdHojaRuta = r.IdHojaRuta and tf.Codigo = '13')";
        s1 += " where CONVERT(VARCHAR, tp.Fecha, 111) >= '" + Convert.ToDateTime(fechaInicio).ToString("yyyy/MM/dd") + "' and CONVERT(VARCHAR, tf.Fecha, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "'";
        s1 += " and r.Estado = '0' and " + criterio + " order by a.CodigoPais, NombreAduana, NombreCliente";
        loadSQL(s1);
    }

    public void loadControlDiarioAduanas(string fecha, string codigoPais)
    {
        string s1 = "select CodigoPais, r.CodigoAduana, NombreAduana, Tipo = '1', count(*) as Cantidad from";
        s1 += " HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta and t.IdFlujo = 1)";
        s1 += " where CONVERT(VARCHAR, FechaProceso, 111) = '" + Convert.ToDateTime(fecha).ToString("yyyy/MM/dd") + "' and r.Estado = '0' and CodigoPais = '" + codigoPais + "'";
        s1 += " group by CodigoPais, r.CodigoAduana,NombreAduana union";
        s1 += " select CodigoPais, r.CodigoAduana,NombreAduana, Tipo = '2', count(*) as Cantidad from";
        s1 += " HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta and t.IdFlujo = 2)";
        s1 += " where CONVERT(VARCHAR, FechaProceso, 111) = '" + Convert.ToDateTime(fecha).ToString("yyyy/MM/dd") + "'  and r.Estado = '0' and CodigoPais = '" + codigoPais + "'";
        s1 += " group by CodigoPais, r.CodigoAduana,NombreAduana union";
        s1 += " select CodigoPais, r.CodigoAduana,NombreAduana, Tipo = '3', count(*) as Cantidad from";
        s1 += " HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta and t.IdFlujo = 3)";
        s1 += " where CONVERT(VARCHAR, FechaProceso, 111) = '" + Convert.ToDateTime(fecha).ToString("yyyy/MM/dd") + "'  and r.Estado = '0' and CodigoPais = '" + codigoPais + "'";
        s1 += " group by CodigoPais, r.CodigoAduana,NombreAduana union";
        s1 += " select CodigoPais, r.CodigoAduana, NombreAduana, Tipo = '4', count(*) as Cantidad from";
        s1 += " HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta and t.IdFlujo = 4)";
        s1 += " where CONVERT(VARCHAR, FechaProceso, 111) = '" + Convert.ToDateTime(fecha).ToString("yyyy/MM/dd") + "'  and r.Estado = '0' and CodigoPais = '" + codigoPais + "'";
        s1 += " group by CodigoPais, r.CodigoAduana, NombreAduana union";
        s1 += " select CodigoPais, r.CodigoAduana, NombreAduana, Tipo = '5', count(*) as Cantidad from";
        s1 += " HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta and t.IdFlujo = 5)";
        s1 += " where CONVERT(VARCHAR, FechaProceso, 111) = '" + Convert.ToDateTime(fecha).ToString("yyyy/MM/dd") + "'  and r.Estado = '0' and CodigoPais = '" + codigoPais + "'";
        s1 += " group by CodigoPais, r.CodigoAduana, NombreAduana union";
        s1 += " select CodigoPais, r.CodigoAduana, NombreAduana, Tipo = '6', count(*) as Cantidad from";
        s1 += " HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta and t.IdFlujo = 6)";
//        s1 += " where CONVERT(VARCHAR, FechaProceso, 111) = '" + Convert.ToDateTime(fecha).ToString("yyyy/MM/dd") + "'  and r.Estado = '0' and CodigoPais = '" + codigoPais + "'";
        //s1 += " group by CodigoPais, r.CodigoAduana, NombreAduana union";
        //s1 += " select CodigoPais, r.CodigoAduana, NombreAduana, Tipo = '7', count(*) as Cantidad from";
        //s1 += " HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        //s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta and t.IdFlujo = 7)";
        s1 += " where CONVERT(VARCHAR, FechaProceso, 111) = '" + Convert.ToDateTime(fecha).ToString("yyyy/MM/dd") + "' and r.Estado = '0' and CodigoPais = '" + codigoPais + "'";
        s1 += " group by CodigoPais, r.CodigoAduana, NombreAduana order by NombreAduana, Tipo";
        loadSQL(s1);
    }

    public void loadFlujoHojaRuta(string IdFlujo)
    {
        string s = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor from";
        s += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        s += " where r.Estado = '0' and cast(IdFlujo as numeric(18,0)) = " + IdFlujo;
        loadSQL(s);
    }

    public void loadFlujoHojaRutaPais(string idHojaRuta, string idFlujo, string codPais)
    {
        string s1 = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana from";
        s1 += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " where a.CodigoPais = '" + codPais + "' and IdHojaRuta like '%" + idHojaRuta + "%' and r.Estado = '0' and cast(IdFlujo as numeric(18,0)) = " + idFlujo;
        loadSQL(s1);
    }

    public void loadFlujoHojaRuta(string idHojaRuta, string idFlujo)
    {
        string s1 = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana from";
        s1 += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " where IdHojaRuta like '%" + idHojaRuta + "%' and r.Estado = '0' and cast(IdFlujo as numeric(18,0)) = " + idFlujo;
        loadSQL(s1);
    }

    public void loadFlujoHojaRutaAduana(string codigoAduana, string idHojaRuta, string idFlujo)
    {
        string s1 = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor from";
        s1 += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        s1 += " where IdHojaRuta like '%" + idHojaRuta + "%' and r.CodigoAduana = '" + codigoAduana + "' and r.Estado = '0' and IdFlujo = '" + idFlujo + "'";
        loadSQL(s1);
    }

    public void loadFlujoHojaRutaAduana(string codigoAduana, string IdFlujo)
    {
        string s1 = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor from";
        s1 += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        s1 += " where r.Estado = '0' and r.CodigoAduana = '" + codigoAduana + "' and IdFlujo = '" + IdFlujo + "'";
        loadSQL(s1);
    }

    public void loadFlujoHojaRutaAforador(string idEmpleado, string idHojaRuta, string idFlujo)
    {
        string s1 = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana from";
        s1 += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " where IdHojaRuta like '%" + idHojaRuta + "%' and IdAforador = " + idEmpleado + " and r.Estado = '0' and IdFlujo = '" + idFlujo + "'";
        loadSQL(s1);
    }

    public void loadFlujoHojaRutaAforador(string idEmpleado, string IdFlujo)
    {
        string s1 = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor from";
        s1 += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        s1 += " where r.Estado = '0' and IdAforador = " + idEmpleado + " and IdFlujo = '" + IdFlujo + "'";
        loadSQL(s1);
    }

    public void loadFlujoHojaRutaEmpleado(string idEmpleado, string IdFlujo)
    {
        string s1 = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor from";
        s1 += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        s1 += " where r.Estado = '0' and IdReceptor = " + idEmpleado + " and IdFlujo = '" + IdFlujo + "'";
        loadSQL(s1);
    }

    public void loadFlujoHojaRutaEmpleado(string idEmpleado, string idHojaRuta, string idFlujo)
    {
        string s1 = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana from";
        s1 += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " where IdHojaRuta like '%" + idHojaRuta + "%' and IdReceptor = " + idEmpleado + " and r.Estado = '0' and IdFlujo = '" + idFlujo + "'";
        loadSQL(s1);
    }

    public void loadFlujoHojaRutaRemitente(string idEmpleado, string IdFlujo)
    {
        string s1 = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor from";
        s1 += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        s1 += " where r.Estado = '0' and IdRemitente = " + idEmpleado + " and IdFlujo = '" + IdFlujo + "'";
        loadSQL(s1);
    }

    public void loadFlujoHojaRutaInplantDinant(string IdFlujo)
    {
        string s1 = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor from";
        s1 += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        s1 += " where r.Estado = '0' and c.NombreCliente like '%DINANT%' and IdFlujo = '" + IdFlujo + "'";
        loadSQL(s1);
    }

    public void loadFlujoHojaRutaInplantDinant(string IdFlujo, string idHojaRuta)
    {
        string s1 = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor from";
        s1 += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        s1 += " where r.Estado = '0' and c.NombreCliente like '%DINANT%' and IdFlujo = '" + IdFlujo + "' and r.IdHojaRuta like '%" + idHojaRuta + "%'";
        loadSQL(s1);
    }

    public void loadFlujoHojaRutaRemitente(string idEmpleado, string idHojaRuta, string idFlujo)
    {
        string s1 = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana from";
        s1 += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " where IdHojaRuta like '%" + idHojaRuta + "%' and IdRemitente = " + idEmpleado + " and r.Estado = '0' and IdFlujo = '" + idFlujo + "'";
        loadSQL(s1);
    }

    public void loadFlujoHojaRutaPais(string IdFlujo, string codigoPais)
    {
        string s1 = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente,Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor from";
        s1 += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        s1 += " where r.Estado = '0' and cast(IdFlujo as numeric(18,0)) = " + IdFlujo + " and a.CodigoPais = '" + codigoPais + "'";
        loadSQL(s1);
    }

    public void loadFlujoHojaRutaRegion(string IdFlujo, string region, string codigoPais)
    {
        string s1 = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente,Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor from";
        s1 += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        s1 += " where r.Estado = '0' and cast(IdFlujo as numeric(18,0)) = " + IdFlujo + " and a.CodigoRegion = '" + region + "' and a.CodigoPais = '" + codigoPais + "'";
        loadSQL(s1);
    }

    public void loadFlujoHojaRutaRegion(string idHojaRuta, string idFlujo, string region, string codigoPais)
    {
        string s1 = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana from";
        s1 += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " where IdHojaRuta like '%" + idHojaRuta + "%' and r.Estado = '0' and cast(IdFlujo as numeric(18,0)) = " + idFlujo + " and a.CodigoRegion = '" + region + "' and a.CodigoPais = '" + codigoPais + "'";
        loadSQL(s1);
    }

    public void loadFlujoHojaRutasTodasPais(string IdFlujo, string codPais)
    {
        string s = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor from";
        s += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        s = s + " where a.CodigoPais = '" + codPais + "' and r.Estado = '0' and cast(IdFlujo as numeric(18,0)) >= " + IdFlujo + " and cast(IdFlujo as numeric(18,0)) < 6";
        loadSQL(s);
    }

    public void loadFlujoHojaRutasTodas(string idHojaRuta, string idFlujo)
    {
        string s1 = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana from";
        s1 += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " where IdHojaRuta like '%" + idHojaRuta + "%' and r.Estado = '0' and cast(IdFlujo as numeric(18,0)) >= " + idFlujo + " and cast(IdFlujo as numeric(18,0)) < 6";
        loadSQL(s1);
    }
    
    public void loadFlujoHojaRutasTodas(string IdFlujo)
    {
        string s = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor from";
        s += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        s = s + " where r.Estado = '0' and cast(IdFlujo as numeric(18,0)) >= " + IdFlujo + " and cast(IdFlujo as numeric(18,0)) < 6";
        loadSQL(s);
    }

    public void loadFlujoHojaRutasTodasPais(string idHojaRuta, string idFlujo, string codPais)
    {
        string s1 = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana from";
        s1 += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " where a.CodigoPais = '" + codPais + "' and IdHojaRuta like '%" + idHojaRuta + "%' and r.Estado = '0' and cast(IdFlujo as numeric(18,0)) >= " + idFlujo + " and cast(IdFlujo as numeric(18,0)) < 6";
        loadSQL(s1);
    }

    public void loadHojaRuta(string idHoja)
    {
        string s = coreSQL;
        s = s + " where IdHojaRuta = '" + idHoja + "' and Estado = '0'";
        loadSQL(s);
    }

    public void loadHojaRuta()
    {
        string s = coreSQL;
        s += " where Estado = '0'";
        loadSQL(s);
    }

    public void loadHojaRutaAduana(string codAduana)
    {
        string s = coreSQL;
        s = s + " where CodigoAduana = '" + codAduana + "'";
        loadSQL(s);
    }

    public void loadHojaRutasAduanaFecha(string fechaFinal, string codigoPais)
    {
        string s = "select a.CodigoPais, a.CodigoAduana, a.NombreAduana, Cantidad = count(*)";
        s += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s += " inner join TiempoFlujo ti on (r.IdHojaRuta = ti.IdHojaRuta and ti.IdFlujo = 1 and CONVERT(VARCHAR, ti.FechaProceso, 101) <= '" + fechaFinal + "')";
        s += " inner join (";
        s += " select t.IdHojaRuta from";
        s += " TiempoFlujo t inner join HojaRuta r2 on (r2.IdHojaRuta = t.IdHojaRuta and r2.IdFlujo <> 6)";
        s += " where CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and r2.Estado = '0' and t.IdFlujo = 1";
        s += " union";
        s += " select IdHojaRuta from";
        s += " TiempoFlujo tf where tf.IdFlujo = 6 and CONVERT(VARCHAR, tf.FechaProceso, 111) > '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and tf.Estado = '0'";
        s += " ) as tp";
        s += " on (r.IdHojaRuta = tp.IdHojaRuta)";
        s += " where r.Estado = '0' and a.CodigoPais = '" + codigoPais + "'";
        s += " Group by a.CodigoPais, a.CodigoAduana, a.NombreAduana";
        loadSQL(s);
    }

    public void loadHojaRutasAduanaFecha(string fechaFinal, string codigoAduana, string codigoPais)
    {
        string s1 = "select r.IdHojaRuta, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor, descripcion as Flujo";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join Codigos c on (r.IdFlujo = c.codigo and c.clasificacion = 'FLUJO')";
        s1 += " inner join Empleados e on (r.IdReceptor = e.IdEmpleado) inner join Clientes cl";
        s1 += " on (cl.CodigoCliente = r.CodigoCliente)";
        s1 += " inner join TiempoFlujo ti on (r.IdHojaRuta = ti.IdHojaRuta and ti.IdFlujo = 1 and CONVERT(VARCHAR, ti.FechaProceso, 101) <= '" + fechaFinal + "')";
        s1 += " inner join (";
        s1 += " select t.IdHojaRuta from";
        s1 += " TiempoFlujo t inner join HojaRuta r2 on (r2.IdHojaRuta = t.IdHojaRuta and r2.IdFlujo <> 6)";
        s1 += " where CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and r2.CodigoAduana = '" + codigoAduana + "' and r2.Estado = '0' and t.IdFlujo = 1";
        s1 += " union";
        s1 += " select tf.IdHojaRuta from";
        s1 += " HojaRuta r2 inner join TiempoFlujo tf on (r2.IdHojaRuta = tf.IdHojaRuta) where tf.IdFlujo = 6 and r2.CodigoAduana = '" + codigoAduana + "' and CONVERT(VARCHAR, tf.FechaProceso, 111) > '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and tf.Estado = '0'";
        s1 += " ) as tp";
        s1 += " on (r.IdHojaRuta = tp.IdHojaRuta)";
        s1 += " where r.Estado = '0' and r.CodigoAduana = '" + codigoAduana + "' and a.CodigoPais = '" + codigoPais + "'";
        s1 += " order by r.IdFlujo, NombreCliente";
        loadSQL(s1);
    }

    public void loadHojaRutasAduanaFechaFinalizadas(string fechaInicio, string fechaFinal, string codigoAduana)
    {
        string s1 = "select r.IdHojaRuta, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor, descripcion as Flujo";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join Codigos c on (r.IdFlujo = c.codigo and c.clasificacion = 'FLUJO')";
        s1 += " inner join Empleados e on (r.IdReceptor = e.IdEmpleado) inner join Clientes cl";
        s1 += " on (cl.CodigoCliente = r.CodigoCliente)";
        s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta)";
        s1 += " where CONVERT(VARCHAR, FechaProceso, 111) >= '" + Convert.ToDateTime(fechaInicio).ToString("yyyy/MM/dd") + "' and CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and t.IdFlujo = 6";
        s1 += " and r.CodigoAduana = '" + codigoAduana + "' and r.Estado = '0' order by r.IdFlujo, NombreCliente";
        loadSQL(s1);
    }

    public void loadHojaRutasAduanaFechaGrafica(string fechaFinal, string codigoAduana)
    {
        string s1 = "select r.IdFlujo, descripcion as Flujo, count(*) as Cantidad";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join Codigos c on (r.IdFlujo = c.codigo and c.clasificacion = 'FLUJO')";
        s1 += " inner join Empleados e on (r.IdReceptor = e.IdEmpleado) inner join Clientes cl";
        s1 += " on (cl.CodigoCliente = r.CodigoCliente)";
        s1 += " inner join TiempoFlujo ti on (r.IdHojaRuta = ti.IdHojaRuta and ti.IdFlujo = 1 and CONVERT(VARCHAR, ti.FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "')";
        s1 += " inner join (";
        s1 += " select t.IdHojaRuta from";
        s1 += " TiempoFlujo t inner join HojaRuta r2 on (r2.IdHojaRuta = t.IdHojaRuta and r2.IdFlujo <> 6)";
        s1 += " where CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and r2.CodigoAduana = '" + codigoAduana + "' and r2.Estado = '0' and t.IdFlujo = 1";
        s1 += " union";
        s1 += " select tf.IdHojaRuta from";
        s1 += " HojaRuta r2 inner join TiempoFlujo tf on (r2.IdHojaRuta = tf.IdHojaRuta) where tf.IdFlujo = 6 and r2.CodigoAduana = '" + codigoAduana + "' and CONVERT(VARCHAR, tf.FechaProceso, 111) > '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and tf.Estado = '0'";
        s1 += " ) as tp";
        s1 += " on (r.IdHojaRuta = tp.IdHojaRuta)";
        s1 += " where r.CodigoAduana = '" + codigoAduana + "' and r.Estado = '0'";
        s1 += " group by r.IdFlujo, descripcion order by r.IdFlujo";
        loadSQL(s1);
    }

    public void loadHojaRutasAduanaFechaGraficaFinalizadas(string fechaInicio, string fechaFinal, string codigoAduana)
    {
        string s1 = "select r.IdFlujo, descripcion as Flujo, count(*) as Cantidad";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join Codigos c on (r.IdFlujo = c.codigo and c.clasificacion = 'FLUJO')";
        s1 += " inner join Empleados e on (r.IdReceptor = e.IdEmpleado) inner join Clientes cl";
        s1 += " on (cl.CodigoCliente = r.CodigoCliente)";
        s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta)";
        s1 += " where CONVERT(VARCHAR, FechaProceso, 111) >= '" + Convert.ToDateTime(fechaInicio).ToString("yyyy/MM/dd") + "' and CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and t.IdFlujo = 6";
        s1 += " and r.CodigoAduana = '" + codigoAduana + "' and r.Estado = '0'";
        s1 += " group by r.IdFlujo, descripcion order by r.IdFlujo";
        loadSQL(s1);
    }

    public void loadHojaRutasAduanaFechaGraficaTodas(string codigoAduana, string fechaInicial, string fechaFinal)
    {
        string s1 = "select r.IdFlujo, descripcion as Flujo, count(*) as Cantidad";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join Codigos c on (r.IdFlujo = c.codigo and c.clasificacion = 'FLUJO')";
        s1 += " inner join Empleados e on (r.IdReceptor = e.IdEmpleado) inner join Clientes cl";
        s1 += " on (cl.CodigoCliente = r.CodigoCliente)";
        s1 += " inner join TiempoFlujo ti on (r.IdHojaRuta = ti.IdHojaRuta and ti.IdFlujo = 1 and CONVERT(VARCHAR, ti.FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "')";
        s1 += " inner join (";
        s1 += " select t.IdHojaRuta from";
        s1 += " TiempoFlujo t inner join HojaRuta r2 on (r2.IdHojaRuta = t.IdHojaRuta and r2.IdFlujo <> 6)";
        s1 += " where CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and r2.CodigoAduana = '" + codigoAduana + "' and r2.Estado = '0' and t.IdFlujo = 1";
        s1 += " union";
        s1 += " select tf.IdHojaRuta from";
        s1 += " HojaRuta r2 inner join TiempoFlujo tf on (r2.IdHojaRuta = tf.IdHojaRuta) where tf.IdFlujo = 6 and r2.CodigoAduana = '" + codigoAduana + "' and CONVERT(VARCHAR, tf.FechaProceso, 111) > '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and tf.Estado = '0'";
        s1 += " union";
        s1 += " select tf.IdHojaRuta from";
        s1 += " HojaRuta r2 inner join TiempoFlujo tf on (r2.IdHojaRuta = tf.IdHojaRuta) where tf.IdFlujo = 6 and r2.CodigoAduana = '" + codigoAduana + "' and CONVERT(VARCHAR, tf.FechaProceso, 111) >= '" + Convert.ToDateTime(fechaInicial).ToString("yyyy/MM/dd") + "' and CONVERT(VARCHAR, tf.FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and tf.Estado = '0'";
        s1 += " ) as tp";
        s1 += " on (r.IdHojaRuta = tp.IdHojaRuta)";
        s1 += " where r.CodigoAduana = '" + codigoAduana + "' and r.Estado = '0'";
        s1 += " group by r.IdFlujo, descripcion order by r.IdFlujo";
        loadSQL(s1);
    }

    public void loadHojaRutasAduanaFechaPDFFinalizadas(string fechaInicio, string fechaFinal, string codigoAduana)
    {
        string s1 = "select r.IdHojaRuta as [Codigo Hoja], CONVERT(VARCHAR, FechaRecepcion, 106) as Fecha, NombreAduana as Aduana, NombreCliente as Cliente, Proveedor, DescripcionProducto as Producto, NombreEmpleado as Gestor, descripcion as Flujo";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join Codigos c on (r.IdFlujo = c.codigo and c.clasificacion = 'FLUJO')";
        s1 += " inner join Empleados e on (r.IdReceptor = e.IdEmpleado) inner join Clientes cl";
        s1 += " on (cl.CodigoCliente = r.CodigoCliente)";
        s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta)";
        s1 += " where CONVERT(VARCHAR, FechaProceso, 111) >= '" + Convert.ToDateTime(fechaInicio).ToString("yyyy/MM/dd") + "' and CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and t.IdFlujo = 6";
        s1 += " and r.CodigoAduana = '" + codigoAduana + "' and r.Estado = '0'";
        s1 += " order by NombreAduana, r.IdFlujo, FechaRecepcion, NombreCliente";
        loadSQL(s1);
    }

    public void loadHojaRutasAduanaFechaPDFProceso(string fechaFinal, string codigoAduana)
    {
        string s1 = "select r.IdHojaRuta as [Codigo Hoja], CONVERT(VARCHAR, FechaRecepcion, 106) as Fecha, NombreAduana as Aduana, NombreCliente as Cliente, Proveedor, DescripcionProducto as Producto, NombreEmpleado as Gestor, descripcion as Flujo";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join Codigos c on (r.IdFlujo = c.codigo and c.clasificacion = 'FLUJO')";
        s1 += " inner join Empleados e on (r.IdReceptor = e.IdEmpleado) inner join Clientes cl";
        s1 += " on (cl.CodigoCliente = r.CodigoCliente)";
        s1 += " inner join TiempoFlujo ti on (r.IdHojaRuta = ti.IdHojaRuta and ti.IdFlujo = 1 and CONVERT(VARCHAR, ti.FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "')";
        s1 += " inner join (";
        s1 += " select t.IdHojaRuta from";
        s1 += " TiempoFlujo t inner join HojaRuta r2 on (r2.IdHojaRuta = t.IdHojaRuta and r2.IdFlujo <> 6)";
        s1 += " where CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and r2.CodigoAduana = '" + codigoAduana + "' and r2.Estado = '0' and t.IdFlujo = 1";
        s1 += " union";
        s1 += " select IdHojaRuta from";
        s1 += " TiempoFlujo tf where tf.IdFlujo = 6 and CONVERT(VARCHAR, tf.FechaProceso, 111) > '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and tf.Estado = '0'";
        s1 += " ) as tp";
        s1 += " on (r.IdHojaRuta = tp.IdHojaRuta)";
        s1 += " where r.CodigoAduana = '" + codigoAduana + "' and r.Estado = '0'";
        s1 += " order by NombreAduana, r.IdFlujo, FechaRecepcion, NombreCliente";
        loadSQL(s1);
    }

    public void loadHojaRutasAduanaFechaPDFTodas(string codigoAduana, string fechaInicial, string fechaFinal)
    {
        string s1 = "select r.IdHojaRuta as [Codigo Hoja], CONVERT(VARCHAR, FechaRecepcion, 106) as Fecha, NombreAduana as Aduana, NombreCliente as Cliente, Proveedor, DescripcionProducto as Producto, NombreEmpleado as Gestor, descripcion as Flujo";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join Codigos c on (r.IdFlujo = c.codigo and c.clasificacion = 'FLUJO')";
        s1 += " inner join Empleados e on (r.IdReceptor = e.IdEmpleado) inner join Clientes cl";
        s1 += " on (cl.CodigoCliente = r.CodigoCliente)";
        s1 += " inner join TiempoFlujo ti on (r.IdHojaRuta = ti.IdHojaRuta and ti.IdFlujo = 1 and CONVERT(VARCHAR, ti.FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "')";
        s1 += " inner join (";
        s1 += " select t.IdHojaRuta from";
        s1 += " TiempoFlujo t inner join HojaRuta r2 on (r2.IdHojaRuta = t.IdHojaRuta and r2.IdFlujo <> 6)";
        s1 += " where CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and r2.CodigoAduana = '" + codigoAduana + "' and r2.Estado = '0' and t.IdFlujo = 1";
        s1 += " union";
        s1 += " select IdHojaRuta from";
        s1 += " TiempoFlujo tf where tf.IdFlujo = 6 and CONVERT(VARCHAR, tf.FechaProceso, 111) > '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and tf.Estado = '0'";
        s1 += " ) as tp";
        s1 += " on (r.IdHojaRuta = tp.IdHojaRuta)";
        s1 += " where r.CodigoAduana = '" + codigoAduana + "' and r.Estado = '0'";
        s1 += " order by NombreAduana, r.IdFlujo, FechaRecepcion, NombreCliente";
        loadSQL(s1);
    }

    public void loadHojaRutasAduanaFechaTodas(string codigoAduana, string fechaInicial, string fechaFinal)
    {
        string s1 = "select r.IdHojaRuta, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor, descripcion as Flujo";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join Codigos c on (r.IdFlujo = c.codigo and c.clasificacion = 'FLUJO')";
        s1 += " inner join Empleados e on (r.IdReceptor = e.IdEmpleado) inner join Clientes cl";
        s1 += " on (cl.CodigoCliente = r.CodigoCliente)";
        s1 += " inner join TiempoFlujo ti on (r.IdHojaRuta = ti.IdHojaRuta and ti.IdFlujo = 1 and CONVERT(VARCHAR, ti.FechaProceso, 101) <= '" + fechaFinal + "')";
        s1 += " inner join (";
        s1 += " select t.IdHojaRuta from";
        s1 += " TiempoFlujo t inner join HojaRuta r2 on (r2.IdHojaRuta = t.IdHojaRuta and r2.IdFlujo <> 6)";
        s1 += " where CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and r2.CodigoAduana = '" + codigoAduana + "' and r2.Estado = '0' and t.IdFlujo = 1";
        s1 += " union";
        s1 += " select tf.IdHojaRuta from";
        s1 += " HojaRuta r2 inner join TiempoFlujo tf on (r2.IdHojaRuta = tf.IdHojaRuta) where tf.IdFlujo = 6 and r2.CodigoAduana = '" + codigoAduana + "' and CONVERT(VARCHAR, tf.FechaProceso, 111) > '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and tf.Estado = '0'";
        s1 += " union";
        s1 += " select tf.IdHojaRuta from";
        s1 += " HojaRuta r2 inner join TiempoFlujo tf on (r2.IdHojaRuta = tf.IdHojaRuta) where tf.IdFlujo = 6 and r2.CodigoAduana = '" + codigoAduana + "' and CONVERT(VARCHAR, tf.FechaProceso, 111) >= '" + Convert.ToDateTime(fechaInicial).ToString("yyyy/MM/dd") + "' and CONVERT(VARCHAR, tf.FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and tf.Estado = '0'";
        s1 += " ) as tp";
        s1 += " on (r.IdHojaRuta = tp.IdHojaRuta)";
        s1 += " where r.Estado = '0' and r.CodigoAduana = '" + codigoAduana + "'";
        s1 += " order by r.IdFlujo, NombreCliente";
        loadSQL(s1);
    }

    public void loadHojaRutasAduanasFinalizadas(string fechaInicio, string fechaFinal, string codigoPais)
    {
        string s1 = "select a.CodigoPais, a.CodigoAduana, a.NombreAduana, Cantidad = count(*)";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta)";
        s1 += " where CONVERT(VARCHAR, FechaProceso, 111) >= '" + Convert.ToDateTime(fechaInicio).ToString("yyyy/MM/dd") + "' and CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and t.IdFlujo = 6 and r.Estado = '0'";
        s1 += " and a.CodigoPais = '" + codigoPais + "'";
        s1 += " Group by a.CodigoPais, a.CodigoAduana, a.NombreAduana";
        loadSQL(s1);
    }

    public void loadHojaRutasControlAduanaEmbarqueFecha(string fechaInicio, string fechaFinal, string codigoAduana, string codigoPais)
    {
        string s1 = "select r.IdHojaRuta, NombreAduana, NombreCliente, FechaRecepcion, DescripcionProducto, Proveedor, ValorEmbarque, CodigoDocEmbarque, Cantidad, ValorCIF, Peso, cod.descripcion as NombrePais, NumFactura, Flete, Observaciones, ValorSerie, CodigoSerie, Correlativo, cod2.descripcion as NombreRegimen";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join Clientes c on (c.CodigoCliente = r.CodigoCliente)";
        s1 += " inner join ComplementoRecepcion com on (com.IdHojaRuta = r.IdHojaRuta)";
        s1 += " inner join Codigos cod on (cod.codigo = com.CodigoPais and cod.clasificacion = 'PAISES')";
        if (codigoPais == "HN")
            s1 += " inner join Codigos cod2 on (cod2.codigo = com.CodigoRegimen and cod2.clasificacion = 'REGIMEN')";
        else
            s1 += " inner join Codigos cod2 on (cod2.codigo = com.CodigoRegimen and cod2.clasificacion = 'REGIMENGUATEMALA')";
        s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta)";
        s1 += " where CONVERT(VARCHAR, FechaProceso, 111) >= '" + Convert.ToDateTime(fechaInicio).ToString("yyyy/MM/dd") + "' and CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and t.IdFlujo = 1 and r.Estado = '0' and r.CodigoAduana = '" + codigoAduana + "'";
        s1 += " order by NombreAduana, NombreCliente";
        loadSQL(s1);
    }

    public void loadHojaRutasControlEmbarqueFecha(string fechaInicio, string fechaFinal, string codigoPais)
    {
        string s1 = "select r.IdHojaRuta, NombreAduana, NombreCliente, FechaRecepcion, DescripcionProducto, Proveedor, ValorEmbarque, CodigoDocEmbarque, Cantidad, ValorCIF, Peso, cod.descripcion as NombrePais, NumFactura, Flete, Observaciones, ValorSerie, CodigoSerie, Correlativo, cod2.descripcion as NombreRegimen";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join Clientes c on (c.CodigoCliente = r.CodigoCliente)";
        s1 += " inner join ComplementoRecepcion com on (com.IdHojaRuta = r.IdHojaRuta)";
        s1 += " inner join Codigos cod on (cod.codigo = com.CodigoPais and cod.clasificacion = 'PAISES')";
        if (codigoPais == "HN")
            s1 += " inner join Codigos cod2 on (cod2.codigo = com.CodigoRegimen and cod2.clasificacion = 'REGIMEN')";
        else
            s1 += " inner join Codigos cod2 on (cod2.codigo = com.CodigoRegimen and cod2.clasificacion = 'REGIMENGUATEMALA')";
        s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta)";
        s1 += " where CONVERT(VARCHAR, FechaProceso, 111) >= '" + Convert.ToDateTime(fechaInicio).ToString("yyyy/MM/dd") + "' and CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and t.IdFlujo = 1 and r.Estado = '0' and a.CodigoPais = '" + codigoPais + "'";
        s1 += " order by NombreAduana, NombreCliente";
        loadSQL(s1);
    }

    public void loadHojaRutasFechaPDFFinalizadas(string fechaInicio, string fechaFinal)
    {
        string s1 = "select a.CodigoPais, r.IdHojaRuta as [Codigo Hoja], CONVERT(VARCHAR, FechaRecepcion, 106) as Fecha, NombreAduana as Aduana, NombreCliente as Cliente, Proveedor, DescripcionProducto as Producto, NombreEmpleado as Gestor, descripcion as Flujo";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join Codigos c on (r.IdFlujo = c.codigo and c.clasificacion = 'FLUJO')";
        s1 += " inner join Empleados e on (r.IdReceptor = e.IdEmpleado) inner join Clientes cl";
        s1 += " on (cl.CodigoCliente = r.CodigoCliente)";
        s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta)";
        s1 += " where CONVERT(VARCHAR, FechaProceso, 111) >= '" + Convert.ToDateTime(fechaInicio).ToString("yyyy/MM/dd") + "' and CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and t.IdFlujo = 6 and r.Estado = '0'";
        s1 += " order by a.CodigoPais, NombreAduana, r.IdFlujo, FechaRecepcion, NombreCliente";
        loadSQL(s1);
    }

    public void loadHojaRutasFechaPDFFinalizadas(string fechaInicio, string fechaFinal, string codigoPais)
    {
        string s1 = "select a.CodigoPais, r.IdHojaRuta as [Codigo Hoja], CONVERT(VARCHAR, FechaRecepcion, 106) as Fecha, NombreAduana as Aduana, NombreCliente as Cliente, Proveedor, DescripcionProducto as Producto, NombreEmpleado as Gestor, descripcion as Flujo";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join Codigos c on (r.IdFlujo = c.codigo and c.clasificacion = 'FLUJO')";
        s1 += " inner join Empleados e on (r.IdReceptor = e.IdEmpleado) inner join Clientes cl";
        s1 += " on (cl.CodigoCliente = r.CodigoCliente)";
        s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta)";
        s1 += " where CONVERT(VARCHAR, FechaProceso, 111) >= '" + Convert.ToDateTime(fechaInicio).ToString("yyyy/MM/dd") + "' and CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and t.IdFlujo = 6 and r.Estado = '0'";
        s1 += " and a.CodigoPais = '" + codigoPais + "'";
        s1 += " order by NombreAduana, r.IdFlujo, FechaRecepcion, NombreCliente";
        loadSQL(s1);
    }

    public void loadHojaRutasFechaPDFProceso(string fechaFinal)
    {
        string s = "select a.CodigoPais, r.IdHojaRuta as [Codigo Hoja], CONVERT(VARCHAR, FechaRecepcion, 106) as Fecha, NombreAduana as Aduana, NombreCliente as Cliente, Proveedor, DescripcionProducto as Producto, NombreEmpleado as Gestor, descripcion as Flujo";
        s += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s += " inner join Codigos c on (r.IdFlujo = c.codigo and c.clasificacion = 'FLUJO')";
        s += " inner join Empleados e on (r.IdReceptor = e.IdEmpleado) inner join Clientes cl";
        s += " on (cl.CodigoCliente = r.CodigoCliente)";
        s += " inner join TiempoFlujo ti on (r.IdHojaRuta = ti.IdHojaRuta and ti.IdFlujo = 1 and CONVERT(VARCHAR, ti.FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "')";
        s += " inner join (";
        s += " select t.IdHojaRuta from";
        s += " TiempoFlujo t inner join HojaRuta r2 on (r2.IdHojaRuta = t.IdHojaRuta and r2.IdFlujo <> 6)";
        s += " where CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and r2.Estado = '0' and t.IdFlujo = 1";
        s += " union";
        s += " select IdHojaRuta from";
        s += " TiempoFlujo tf where tf.IdFlujo = 6 and CONVERT(VARCHAR, tf.FechaProceso, 111) > '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and tf.Estado = '0'";
        s += " ) as tp";
        s += " on (r.IdHojaRuta = tp.IdHojaRuta)";
        s += " where r.Estado = '0'";
        s += " order by a.CodigoPais, NombreAduana, r.IdFlujo, FechaRecepcion, NombreCliente";
        loadSQL(s);
    }

    public void loadHojaRutasFechaPDFProceso(string fechaFinal, string codigoPais)
    {
        string s = "select a.CodigoPais, r.IdHojaRuta as [Codigo Hoja], CONVERT(VARCHAR, FechaRecepcion, 106) as Fecha, NombreAduana as Aduana, NombreCliente as Cliente, Proveedor, DescripcionProducto as Producto, NombreEmpleado as Gestor, descripcion as Flujo";
        s += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s += " inner join Codigos c on (r.IdFlujo = c.codigo and c.clasificacion = 'FLUJO')";
        s += " inner join Empleados e on (r.IdReceptor = e.IdEmpleado) inner join Clientes cl";
        s += " on (cl.CodigoCliente = r.CodigoCliente)";
        s += " inner join TiempoFlujo ti on (r.IdHojaRuta = ti.IdHojaRuta and ti.IdFlujo = 1 and CONVERT(VARCHAR, ti.FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "')";
        s += " inner join (";
        s += " select t.IdHojaRuta from";
        s += " TiempoFlujo t inner join HojaRuta r2 on (r2.IdHojaRuta = t.IdHojaRuta and r2.IdFlujo <> 6)";
        s += " where CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and r2.Estado = '0' and t.IdFlujo = 1";
        s += " union";
        s += " select IdHojaRuta from";
        s += " TiempoFlujo tf where tf.IdFlujo = 6 and CONVERT(VARCHAR, tf.FechaProceso, 111) > '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and tf.Estado = '0'";
        s += " ) as tp";
        s += " on (r.IdHojaRuta = tp.IdHojaRuta)";
        s += " where r.Estado = '0' and a.CodigoPais = '" + codigoPais + "'";
        s += " order by NombreAduana, r.IdFlujo, FechaRecepcion, NombreCliente";
        loadSQL(s);
    }

    public void loadHojaRutasFechaPDFTodas(string fechaInicial, string fechaFinal)
    {
        string s = "select a.CodigoPais, r.IdHojaRuta as [Codigo Hoja], CONVERT(VARCHAR, FechaRecepcion, 106) as Fecha, NombreAduana as Aduana, NombreCliente as Cliente, Proveedor, DescripcionProducto as Producto, NombreEmpleado as Gestor, descripcion as Flujo";
        s += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s += " inner join Codigos c on (r.IdFlujo = c.codigo and c.clasificacion = 'FLUJO')";
        s += " inner join Empleados e on (r.IdReceptor = e.IdEmpleado) inner join Clientes cl";
        s += " on (cl.CodigoCliente = r.CodigoCliente)";
        s += " inner join TiempoFlujo ti on (r.IdHojaRuta = ti.IdHojaRuta and ti.IdFlujo = 1 and CONVERT(VARCHAR, ti.FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "')";
        s += " inner join (";
        s += " select t.IdHojaRuta from";
        s += " TiempoFlujo t inner join HojaRuta r2 on (r2.IdHojaRuta = t.IdHojaRuta and r2.IdFlujo <> 6)";
        s += " where CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and r2.Estado = '0' and t.IdFlujo = 1";
        s += " union";
        s += " select IdHojaRuta from";
        s += " TiempoFlujo tf where tf.IdFlujo = 6 and CONVERT(VARCHAR, tf.FechaProceso, 111) > '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and tf.Estado = '0'";
        s += " union";
        s += " select IdHojaRuta from";
        s += " TiempoFlujo tf where tf.IdFlujo = 6 and CONVERT(VARCHAR, tf.FechaProceso, 111) >= '" + Convert.ToDateTime(fechaInicial).ToString("yyyy/MM/dd") + "' and CONVERT(VARCHAR, tf.FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and tf.Estado = '0'";
        s += " ) as tp";
        s += " on (r.IdHojaRuta = tp.IdHojaRuta)";
        s += " where r.Estado = '0'";
        s += " order by a.CodigoPais, NombreAduana, r.IdFlujo, FechaRecepcion, NombreCliente";
        loadSQL(s);
    }

    public void loadHojaRutasFechaPDFTodasPais(string codigoPais, string fechaInicial, string fechaFinal)
    {
        string s = "select a.CodigoPais, r.IdHojaRuta as [Codigo Hoja], CONVERT(VARCHAR, FechaRecepcion, 106) as Fecha, NombreAduana as Aduana, NombreCliente as Cliente, Proveedor, DescripcionProducto as Producto, NombreEmpleado as Gestor, descripcion as Flujo";
        s += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s += " inner join Codigos c on (r.IdFlujo = c.codigo and c.clasificacion = 'FLUJO')";
        s += " inner join Empleados e on (r.IdReceptor = e.IdEmpleado) inner join Clientes cl";
        s += " on (cl.CodigoCliente = r.CodigoCliente)";
        s += " inner join TiempoFlujo ti on (r.IdHojaRuta = ti.IdHojaRuta and ti.IdFlujo = 1 and CONVERT(VARCHAR, ti.FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "')";
        s += " inner join (";
        s += " select t.IdHojaRuta from";
        s += " TiempoFlujo t inner join HojaRuta r2 on (r2.IdHojaRuta = t.IdHojaRuta and r2.IdFlujo <> 6)";
        s += " where CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and r2.Estado = '0' and t.IdFlujo = 1";
        s += " union";
        s += " select IdHojaRuta from";
        s += " TiempoFlujo tf where tf.IdFlujo = 6 and CONVERT(VARCHAR, tf.FechaProceso, 111) > '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and tf.Estado = '0'";
        s += " union";
        s += " select IdHojaRuta from";
        s += " TiempoFlujo tf where tf.IdFlujo = 6 and CONVERT(VARCHAR, tf.FechaProceso, 111) >= '" + Convert.ToDateTime(fechaInicial).ToString("yyyy/MM/dd") + "' and CONVERT(VARCHAR, tf.FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and tf.Estado = '0'";
        s += " ) as tp";
        s += " on (r.IdHojaRuta = tp.IdHojaRuta)";
        s += " where r.Estado = '0' and a.CodigoPais = '" + codigoPais + "'";
        s += " order by NombreAduana, r.IdFlujo, FechaRecepcion, NombreCliente";
        loadSQL(s);
    }

    public void loadHojaRutasPaisAduanasFechaTodas(string codigoPais, string fechaInicio, string fechaFinal)
    {
        string s = "select a.CodigoPais, a.CodigoAduana, a.NombreAduana, Cantidad = count(*)";
        s += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s += " inner join TiempoFlujo ti on (r.IdHojaRuta = ti.IdHojaRuta and ti.IdFlujo = 1 and CONVERT(VARCHAR, ti.FechaProceso, 101) <= '" + fechaFinal + "')";
        s += " inner join (";
        s += " select t.IdHojaRuta from";
        s += " TiempoFlujo t inner join HojaRuta r2 on (r2.IdHojaRuta = t.IdHojaRuta and r2.IdFlujo <> 6)";
        s += " where CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and r2.Estado = '0' and t.IdFlujo = 1";
        s += " union";
        s += " select IdHojaRuta from";
        s += " TiempoFlujo tf where tf.IdFlujo = 6 and CONVERT(VARCHAR, tf.FechaProceso, 111) > '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and tf.Estado = '0'";
        s += " union";
        s += " select IdHojaRuta from";
        s += " TiempoFlujo tf where tf.IdFlujo = 6 and CONVERT(VARCHAR, tf.FechaProceso, 111) >= '" + Convert.ToDateTime(fechaInicio).ToString("yyyy/MM/dd") + "' and CONVERT(VARCHAR, tf.FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and tf.Estado = '0'";
        s += " ) as tp";
        s += " on (r.IdHojaRuta = tp.IdHojaRuta)";
        s += " where r.Estado = '0' and a.CodigoPais = '" + codigoPais + "'";
        s += " Group by a.CodigoPais, a.CodigoAduana, a.NombreAduana";
        loadSQL(s);
    }

    public void loadHojaRutasPaisesFecha(string fechaFinal, string codigoPais)
    {
        string s = "select a.CodigoPais, cod.descripcion as Pais, Cantidad = count(*)";
        s += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s += " inner join Codigos cod on (a.CodigoPais = cod.codigo and cod.clasificacion = 'PAISES')";
        s += " inner join TiempoFlujo ti on (r.IdHojaRuta = ti.IdHojaRuta and ti.IdFlujo = 1 and CONVERT(VARCHAR, ti.FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "')";
        s += " inner join (";
        s += " select t.IdHojaRuta from";
        s += " TiempoFlujo t inner join HojaRuta r2 on (r2.IdHojaRuta = t.IdHojaRuta and r2.IdFlujo <> 6)";
        s += " where CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and r2.Estado = '0' and t.IdFlujo = 1";
        s += " union";
        s += " select IdHojaRuta from";
        s += " TiempoFlujo tf where tf.IdFlujo = 6 and CONVERT(VARCHAR, tf.FechaProceso, 111) > '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and tf.Estado = '0'";
        s += " ) as tp";
        s += " on (r.IdHojaRuta = tp.IdHojaRuta)";
        s += " where r.Estado = '0'";
        s += " Group by a.CodigoPais, cod.descripcion";
        loadSQL(s);
    }

    public void loadHojaRutasPaisesFechaTodas(string fechaInicial, string fechaFinal)
    {
        string s = "select a.CodigoPais, cod.descripcion as Pais, Cantidad = count(*)";
        s += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s += " inner join Codigos cod on (a.CodigoPais = cod.codigo and cod.clasificacion = 'PAISES')";
        s += " inner join TiempoFlujo ti on (r.IdHojaRuta = ti.IdHojaRuta and ti.IdFlujo = 1 and CONVERT(VARCHAR, ti.FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "')";
        s += " inner join (";
        s += " select t.IdHojaRuta from";
        s += " TiempoFlujo t inner join HojaRuta r2 on (r2.IdHojaRuta = t.IdHojaRuta and r2.IdFlujo <> 6)";
        s += " where CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and r2.Estado = '0' and t.IdFlujo = 1";
        s += " union";
        s += " select IdHojaRuta from";
        s += " TiempoFlujo tf where tf.IdFlujo = 6 and CONVERT(VARCHAR, tf.FechaProceso, 111) > '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and tf.Estado = '0'";
        s += " union";
        s += " select IdHojaRuta from";
        s += " TiempoFlujo tf where tf.IdFlujo = 6 and CONVERT(VARCHAR, tf.FechaProceso, 111) >= '" + Convert.ToDateTime(fechaInicial).ToString("yyyy/MM/dd") + "' and CONVERT(VARCHAR, tf.FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and tf.Estado = '0'";
        s += " ) as tp";
        s += " on (r.IdHojaRuta = tp.IdHojaRuta)";
        s += " where r.Estado = '0'";
        s += " Group by a.CodigoPais, cod.descripcion";
        loadSQL(s);
    }

    public void loadHojaRutasPaisesFinalizadas(string fechaInicio, string fechaFinal, string codigoPais)
    {
        string s1 = "select a.CodigoPais, cod.descripcion as Pais, Cantidad = count(*)";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join Codigos cod on (a.CodigoPais = cod.codigo and cod.clasificacion = 'PAISES')";
        s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta)";
        string s2 = s1;
        s1 = s2 + " where CONVERT(VARCHAR, FechaProceso, 111) >= '" + Convert.ToDateTime(fechaInicio).ToString("yyyy/MM/dd") + "' and CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and t.IdFlujo = 6 and r.Estado = '0'";
        s1 = s1 + " and a.CodigoPais = '" + codigoPais + "'";
        s1 += " Group by a.CodigoPais, cod.descripcion";
        loadSQL(s1);
    }

    public void loadHojasRutasAlertas(string fechaInicio, string fechaFinal, string criterio)
    {
        string s1 = "declare @Duration as DateTime";
        s1 += " set @Duration = GETDATE()";
        s1 += " select a.CodigoPais, a.NombreAduana, NombreCliente, tf.IdHojaRuta, ('Dias: ' + cast((DateDiff(second, tf.FechaProceso, tf2.FechaProceso)) / 86400 as varchar(50)) + ' Horas: ' + cast(((DateDiff(second, tf.FechaProceso, tf2.FechaProceso)) % 86400) / 3600 as varchar(50)) + ' Mins: ' + cast(((DateDiff(second, tf.FechaProceso, tf2.FechaProceso)) % 3600) / 60 as varchar(50))) as Tiempo, r.IdFlujo, c.descripcion, cr.CodigoRegimen, cr.CodigoTransporte, tf.FechaProceso as FechaI, tf2.FechaProceso as FechaF";
        s1 += " from TiempoFlujo tf";
        s1 += " inner join (select IdHojaRuta, FechaProceso from TiempoFlujo tf where tf.IdFlujo = 4) as tf2";
        s1 += " on (tf.IdHojaRuta = tf2.IdHojaRuta)";
        s1 += " inner join HojaRuta r on (tf.IdHojaRuta = r.IdHojaRuta)";
        s1 += " inner join ComplementoRecepcion cr on (cr.IdHojaRuta = r.IdHojaRuta)";
        s1 += " inner join Codigos c on (c.codigo = r.IdFlujo and c.clasificacion = 'FLUJO')";
        s1 += " inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join Clientes cl on (r.CodigoCliente = cl.CodigoCliente)";
        s1 += " where tf.IdFlujo = 3 and " + criterio + " and CONVERT(VARCHAR, tf.FechaProceso, 111) >= '" + fechaInicio + "' and CONVERT(VARCHAR, tf.FechaProceso, 111) <= '" + fechaFinal + "' and r.Estado = '0'";
        s1 += " union";
        s1 += " select a.CodigoPais, a.NombreAduana, NombreCliente, tf.IdHojaRuta, ('Dias: ' + cast((DateDiff(second, tf.FechaProceso, @Duration)) / 86400 as varchar(50)) + ' Horas: ' + cast(((DateDiff(second, tf.FechaProceso, @Duration)) % 86400) / 3600 as varchar(50)) + ' Mins: ' + cast(((DateDiff(second, tf.FechaProceso, @Duration)) % 3600) / 60 as varchar(50))) as Tiempo, r.IdFlujo, c.descripcion, cr.CodigoRegimen, cr.CodigoTransporte, tf.FechaProceso as FechaI,  @Duration as FechaF";
        s1 += " from TiempoFlujo tf inner join HojaRuta r on (tf.IdHojaRuta = r.IdHojaRuta)";
        s1 += " inner join ComplementoRecepcion cr on (cr.IdHojaRuta = r.IdHojaRuta)";
        s1 += " inner join Codigos c on (c.codigo = r.IdFlujo and c.clasificacion = 'FLUJO')";
        s1 += " inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join Clientes cl on (r.CodigoCliente = cl.CodigoCliente)";
        s1 += " where tf.IdFlujo = 3 and " + criterio + " and r.IdFlujo = 3 and CONVERT(VARCHAR, tf.FechaProceso, 111) >= '" + fechaInicio + "' and CONVERT(VARCHAR, tf.FechaProceso, 111) <= '" + fechaFinal + "' and r.Estado = '0'";
        s1 += " order by a.CodigoPais, NombreAduana";
        loadSQL(s1);
    }

    public void loadHojasRutasAlertasFacturacion(string fechaInicio, string fechaFinal, string codigoPais)
    {
        string s1 = "select a.CodigoPais, r.CodigoAduana, NombreAduana, cantidadAlerta = count(*), 'ENTREGA CARGA CLIENTE - ENVIO DOCUMENTO A FACURACION' as Rango,";
        s1 += " cantidad = (select count(*)";
        s1 += " from HojaRuta r1 inner join Aduanas a on (r1.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join TramiteAduanero t1 on (r1.IdHojaRuta = t1.IdHojaRuta and t1.Codigo = 13)";
        string s2 = s1;
        s1 = s2 + " where CONVERT(VARCHAR, t1.Fecha, 111) >= '" + fechaInicio + "' and CONVERT(VARCHAR, t1.Fecha, 111) <= '" + fechaFinal + "'";
        s1 += " and r.CodigoAduana = r1.CodigoAduana";
        s1 += " group by r1.CodigoAduana), codigo='1'";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join TramiteAduanero t1 on (r.IdHojaRuta = t1.IdHojaRuta and t1.Codigo = 13)";
        s1 += " left join TramiteAduanero t2 on (r.IdHojaRuta = t2.IdHojaRuta and t2.Codigo = 14)";
        string s3 = s1;
        s1 = s3 + " where CONVERT(VARCHAR, t1.Fecha, 111) >= '" + fechaInicio + "' and CONVERT(VARCHAR, t1.Fecha, 111) <= '" + fechaFinal + "' and a.CodigoPais = '" + codigoPais + "'";
        s1 += " and r.Estado = '0' and (case when t2.Fecha is null then cast((DateDiff(second, t1.Fecha, GETDATE())) / 86400 as numeric(18,0)) else cast((DateDiff(second, t1.Fecha, t2.Fecha)) / 86400 as numeric(18,0)) end) >= 2";
        s1 += " group by a.CodigoPais, r.CodigoAduana, NombreAduana";
        s1 += " union";
        s1 += " select a.CodigoPais, r.CodigoAduana, NombreAduana, cantidadAlerta = count(*), 'ENVIO DOCUMENTO A FACURACION - RECIBO DE DOCUMENTOS PARA FACTURACION' as Rango,";
        s1 += " cantidad = (select count(*)";
        s1 += " from HojaRuta r1 inner join Aduanas a on (r1.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join TramiteAduanero t1 on (r1.IdHojaRuta = t1.IdHojaRuta and t1.Codigo = 14)";
        string s4 = s1;
        s1 = s4 + " where CONVERT(VARCHAR, t1.Fecha, 111) >= '" + fechaInicio + "' and CONVERT(VARCHAR, t1.Fecha, 111) <= '" + fechaFinal + "'";
        s1 += " and r.CodigoAduana = r1.CodigoAduana";
        s1 += " group by r1.CodigoAduana), codigo='2'";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join TramiteAduanero t1 on (r.IdHojaRuta = t1.IdHojaRuta and t1.Codigo = 14)";
        s1 += " left join TramiteAduanero t2 on (r.IdHojaRuta = t2.IdHojaRuta and t2.Codigo = 15)";
        string s5 = s1;
        s1 = s5 + " where CONVERT(VARCHAR, t1.Fecha, 111) >= '" + fechaInicio + "' and CONVERT(VARCHAR, t1.Fecha, 111) <= '" + fechaFinal + "' and a.CodigoPais = '" + codigoPais + "'";
        s1 += " and r.Estado = '0' and (case when t2.Fecha is null then cast((DateDiff(second, t1.Fecha, GETDATE())) / 86400 as numeric(18,0)) else cast((DateDiff(second, t1.Fecha, t2.Fecha)) / 86400 as numeric(18,0)) end) >= 3";
        //s1 += " group by a.CodigoPais, r.CodigoAduana, NombreAduana";
        //s1 += " union";
        //s1 += " select a.CodigoPais, r.CodigoAduana, NombreAduana, cantidadAlerta = count(*), 'RECIBO DE DOCUMENTOS PARA FACTURACION - ENTREGA DE FACTURA AL CLIENTE' as Rango,";
        //s1 += " cantidad = (select count(*)";
        //s1 += " from HojaRuta r1 inner join Aduanas a on (r1.CodigoAduana = a.CodigoAduana)";
        //s1 += " inner join TramiteAduanero t1 on (r1.IdHojaRuta = t1.IdHojaRuta and t1.Codigo = 15)";
        //string s6 = s1;
        //s1 = s6 + " where CONVERT(VARCHAR, t1.Fecha, 111) >= '" + fechaInicio + "' and CONVERT(VARCHAR, t1.Fecha, 111) <= '" + fechaFinal + "'";
        //s1 += " and r.CodigoAduana = r1.CodigoAduana";
        //s1 += " group by r1.CodigoAduana), codigo='3'";
        //s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        //s1 += " inner join TramiteAduanero t1 on (r.IdHojaRuta = t1.IdHojaRuta and t1.Codigo = 15)";
        //s1 += " left join TramiteAduanero t2 on (r.IdHojaRuta = t2.IdHojaRuta and t2.Codigo = 18)";
        //string s7 = s1;
        //s1 = s7 + " where CONVERT(VARCHAR, t1.Fecha, 111) >= '" + fechaInicio + "' and CONVERT(VARCHAR, t1.Fecha, 111) <= '" + fechaFinal + "' and a.CodigoPais = '" + codigoPais + "'";
        //s1 += " and r.Estado = '0' and (case when t2.Fecha is null then cast((DateDiff(second, t1.Fecha, GETDATE())) / 86400 as numeric(18,0)) else cast((DateDiff(second, t1.Fecha, t2.Fecha)) / 86400 as numeric(18,0)) end) >= 2";
        s1 += " group by a.CodigoPais, r.CodigoAduana, NombreAduana order by a.CodigoPais, NombreAduana, Rango";
        loadSQL(s1);
    }

    public void loadHojasRutasClientesPorAduanas(string fechaInicio, string fechaFinal, string codigoAduana, string codigoCliente)
    {
        string s1 = "select NombreAduana as Aduana, c.NombreCliente, r.IdHojaRuta, Tipo = 'Ingresada' from";
        s1 += " Clientes c inner join HojaRuta r on (c.CodigoCliente = r.CodigoCliente)";
        s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta)";
        s1 += " inner join Aduanas a on (a.CodigoAduana = r.CodigoAduana)";
        s1 = s1 + " where CONVERT(VARCHAR, FechaProceso, 111) >= '" + Convert.ToDateTime(fechaInicio).ToString("yyyy/MM/dd") + "'";
        s1 = s1 + " and CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "'";
        string s2 = s1;
        s1 = s2 + " and r.Estado = '0' and t.IdFlujo = 1 and r.CodigoCliente = '" + codigoCliente + "' and r.CodigoAduana = '" + codigoAduana + "'";
        s1 += " union";
        s1 += " select NombreAduana as Aduana, c.NombreCliente, r.IdHojaRuta, Tipo = 'Pendiente o en Proceso' from";
        s1 += " Clientes c inner join HojaRuta r on (c.CodigoCliente = r.CodigoCliente)";
        s1 = s1 + " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta and t.IdFlujo = 1 and CONVERT(VARCHAR, t.FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "')";
        s1 += " inner join (";
        s1 += " select t.IdHojaRuta from";
        s1 += " TiempoFlujo t inner join HojaRuta r2 on (r2.IdHojaRuta = t.IdHojaRuta and r2.IdFlujo <> 6)";
        string s3 = s1;
        s1 = s3 + " where CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and r2.CodigoAduana = '" + codigoAduana + "' and r2.Estado = '0' and t.IdFlujo = 1";
        s1 += " union";
        s1 += " select IdHojaRuta from";
        s1 = s1 + " TiempoFlujo tf where tf.IdFlujo = 6 and CONVERT(VARCHAR, tf.FechaProceso, 111) > '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "'";
        s1 += " ) as tp";
        s1 += " on (r.IdHojaRuta = tp.IdHojaRuta)";
        s1 += " inner join Aduanas a on (a.CodigoAduana = r.CodigoAduana)";
        s1 += " where r.Estado = '0'";
        string s4 = s1;
        s1 = s4 + " and r.CodigoCliente = '" + codigoCliente + "' and r.CodigoAduana = '" + codigoAduana + "'";
        s1 += " union";
        s1 += " select NombreAduana as Aduana, c.NombreCliente, r.IdHojaRuta, Tipo = 'Terminada' from";
        s1 += " Clientes c inner join HojaRuta r on (c.CodigoCliente = r.CodigoCliente)";
        s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta)";
        s1 += " inner join Aduanas a on (a.CodigoAduana = r.CodigoAduana)";
        s1 = s1 + " where CONVERT(VARCHAR, FechaProceso, 111) >= '" + Convert.ToDateTime(fechaInicio).ToString("yyyy/MM/dd") + "'";
        s1 = s1 + " and CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "'";
        string s5 = s1;
        s1 = s5 + " and r.Estado = '0' and t.IdFlujo = 6 and r.CodigoCliente = '" + codigoCliente + "' and r.CodigoAduana = '" + codigoAduana + "'";
        s1 += " order by NombreAduana, Tipo, r.IdHojaRuta";
        loadSQL(s1);
    }

    public void loadHojasRutasClientesPorAduanas(string fechaInicio, string fechaFinal, string codigoAduana)
    {
        string s1 = "select p.CodigoPais, p.NombreAduana as Aduana, p.CodigoCliente, p.NombreCliente as Cliente, case when i.cantidad >= 0 then i.cantidad else '0' end as [ingresadas] ,p.cantidad as [pendientes o en proceso], case when f.cantidad >= 0 then f.cantidad else '0' end as terminadas";
        s1 += " from";
        s1 += " (select a.CodigoPais, NombreAduana, c.CodigoCliente, c.NombreCliente, cantidad = count(*) from";
        s1 += " Clientes c inner join HojaRuta r on (c.CodigoCliente = r.CodigoCliente)";
        s1 += " inner join Aduanas a on (a.CodigoAduana = r.CodigoAduana)";
        s1 = s1 + " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta and t.IdFlujo = 1 and CONVERT(VARCHAR, t.FechaProceso, 111) <= '" + fechaFinal + "')";
        s1 += " inner join (";
        s1 += " select t.IdHojaRuta from";
        s1 += " TiempoFlujo t inner join HojaRuta r2 on (r2.IdHojaRuta = t.IdHojaRuta and r2.IdFlujo <> 6)";
        string s2 = s1;
        s1 = s2 + " where CONVERT(VARCHAR, FechaProceso, 111) <= '" + fechaFinal + "' and r2.CodigoAduana = '" + codigoAduana + "' and r2.Estado = '0' and t.IdFlujo = 1";
        s1 += " union";
        s1 += " select IdHojaRuta from";
        s1 = s1 + " TiempoFlujo tf where tf.IdFlujo = 6 and CONVERT(VARCHAR, tf.FechaProceso, 111) > '" + fechaFinal + "'";
        s1 += " ) as tp";
        s1 += " on (r.IdHojaRuta = tp.IdHojaRuta)";
        s1 = s1 + " where r.Estado = '0' and r.CodigoAduana = '" + codigoAduana + "'";
        s1 += " group by a.CodigoPais, NombreAduana, c.CodigoCliente,c.NombreCliente";
        s1 += " ) as p left join";
        s1 += " (select a.CodigoPais, NombreAduana, c.CodigoCliente, c.NombreCliente, cantidad = count(*) from";
        s1 += " Clientes c inner join HojaRuta r on (c.CodigoCliente = r.CodigoCliente)";
        s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta)";
        s1 += " inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 = s1 + " where CONVERT(VARCHAR, FechaProceso, 111) >= '" + fechaInicio + "'";
        s1 = s1 + " and CONVERT(VARCHAR, FechaProceso, 111) <= '" + fechaFinal + "'";
        s1 = s1 + " and r.Estado = '0' and t.IdFlujo = 6  and r.CodigoAduana = '" + codigoAduana + "'";
        s1 += " group by a.CodigoPais, NombreAduana, c.CodigoCliente,c.NombreCliente) as f on (p.CodigoCliente = f.CodigoCliente)";
        s1 += " left join (select a.CodigoPais, NombreAduana, c.CodigoCliente, c.NombreCliente, cantidad = count(*) from";
        s1 += " Clientes c inner join HojaRuta r on (c.CodigoCliente = r.CodigoCliente)";
        s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta)";
        s1 += " inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 = s1 + " where CONVERT(VARCHAR, FechaProceso, 111) >= '" + fechaInicio + "'";
        s1 = s1 + " and CONVERT(VARCHAR, FechaProceso, 111) <= '" + fechaFinal + "'";
        s1 = s1 + " and r.Estado = '0' and t.IdFlujo = 1  and r.CodigoAduana = '" + codigoAduana + "'";
        s1 += " group by a.CodigoPais, NombreAduana, c.CodigoCliente,c.NombreCliente) as i on (p.CodigoCliente = i.CodigoCliente)";
        s1 += " order by p.NombreCliente";
        loadSQL(s1);
    }

    public void loadHojasRutasClientesTodas(string fechaInicio, string fechaFinal, string codigoPais)
    {
        string s = "select p.CodigoCliente, p.NombreCliente as Cliente, case when i.cantidad >= 0 then i.cantidad else '0' end as [ingresadas] ,p.cantidad as [pendientes o en proceso], case when f.cantidad >= 0 then f.cantidad else '0' end as terminadas";
        s += " from";
        s += " (select c.CodigoCliente, c.NombreCliente, cantidad = count(*) from";
        s += " Clientes c inner join HojaRuta r on (c.CodigoCliente = r.CodigoCliente)";
        s += " inner join Aduanas a on (a.CodigoAduana = r.CodigoAduana)";
        s += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta and t.IdFlujo = 1 and CONVERT(VARCHAR, t.FechaProceso, 111) <= '" + fechaFinal + "')";
        s += " inner join (";
        s += " select t.IdHojaRuta from";
        s += " TiempoFlujo t inner join HojaRuta r2 on (r2.IdHojaRuta = t.IdHojaRuta and r2.IdFlujo <> 6)";
        s += " where CONVERT(VARCHAR, FechaProceso, 111) <= '" + fechaFinal + "' and r2.Estado = '0' and t.IdFlujo = 1";
        s += " union";
        s += " select IdHojaRuta from";
        s += " TiempoFlujo tf where tf.IdFlujo = 6 and CONVERT(VARCHAR, tf.FechaProceso, 111) > '" + fechaFinal + "'";
        s += " ) as tp";
        s += " on (r.IdHojaRuta = tp.IdHojaRuta)";
        s += " where r.Estado = '0' and a.CodigoPais = '" + codigoPais + "'";
        s += " group by c.CodigoCliente,c.NombreCliente) as p left join";
        s += " (select c.CodigoCliente, c.NombreCliente, cantidad = count(*) from";
        s += " Clientes c inner join HojaRuta r on (c.CodigoCliente = r.CodigoCliente)";
        s += " inner join Aduanas a on (a.CodigoAduana = r.CodigoAduana)";
        s += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta)";
        s += " where CONVERT(VARCHAR, FechaProceso, 111) >= '" + fechaInicio + "'";
        s += " and CONVERT(VARCHAR, FechaProceso, 111) <= '" + fechaFinal + "'";
        s += " and r.Estado = '0' and t.IdFlujo = 6 and a.CodigoPais = '" + codigoPais + "'";
        s += " group by c.CodigoCliente,c.NombreCliente) as f on (p.CodigoCliente = f.CodigoCliente)";
        s += " left join (select c.CodigoCliente, c.NombreCliente, cantidad = count(*) from";
        s += " Clientes c inner join HojaRuta r on (c.CodigoCliente = r.CodigoCliente)";
        s += " inner join Aduanas a on (a.CodigoAduana = r.CodigoAduana)";
        s += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta)";
        s += " where CONVERT(VARCHAR, FechaProceso, 111) >= '" + fechaInicio + "'";
        s += " and CONVERT(VARCHAR, FechaProceso, 111) <= '" + fechaFinal + "'";
        s += " and r.Estado = '0' and t.IdFlujo = 1 and a.CodigoPais = '" + codigoPais + "'";
        s += " group by c.CodigoCliente,c.NombreCliente) as i on (p.CodigoCliente = i.CodigoCliente)";
        s += " order by p.NombreCliente";
        loadSQL(s);
    }

    public void loadHojasRutasClientesTodas(string fechaInicio, string fechaFinal, string codigoCliente, string codigoPais)
    {
        string s1 = "select a.CodigoPais, NombreAduana as Aduana, c.NombreCliente, r.IdHojaRuta, Tipo = 'Ingresada' from";
        s1 += " Clientes c inner join HojaRuta r on (c.CodigoCliente = r.CodigoCliente)";
        s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta)";
        s1 += " inner join Aduanas a on (a.CodigoAduana = r.CodigoAduana)";
        s1 = s1 + " where CONVERT(VARCHAR, FechaProceso, 111) >= '" + Convert.ToDateTime(fechaInicio).ToString("yyyy/MM/dd") + "'";
        s1 = s1 + " and CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "'";
        string s2 = s1;
        s1 = s2 + " and r.Estado = '0' and t.IdFlujo = 1 and r.CodigoCliente = '" + codigoCliente + "' and a.CodigoPais = '" + codigoPais + "'";
        s1 += " union";
        s1 += " select a.CodigoPais, NombreAduana as Aduana, c.NombreCliente, r.IdHojaRuta, Tipo = 'Pendiente o en Proceso' from";
        s1 += " Clientes c inner join HojaRuta r on (c.CodigoCliente = r.CodigoCliente)";
        s1 = s1 + " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta and t.IdFlujo = 1 and CONVERT(VARCHAR, t.FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "')";
        s1 += " inner join (";
        s1 += " select t.IdHojaRuta from";
        s1 += " TiempoFlujo t inner join HojaRuta r2 on (r2.IdHojaRuta = t.IdHojaRuta and r2.IdFlujo <> 6)";
        s1 = s1 + " where CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and r2.Estado = '0' and t.IdFlujo = 1";
        s1 += " union";
        s1 += " select IdHojaRuta from";
        s1 = s1 + " TiempoFlujo tf where tf.IdFlujo = 6 and CONVERT(VARCHAR, tf.FechaProceso, 111) > '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "'";
        s1 += " ) as tp";
        s1 += " on (r.IdHojaRuta = tp.IdHojaRuta)";
        s1 += " inner join Aduanas a on (a.CodigoAduana = r.CodigoAduana)";
        s1 += " where r.Estado = '0'";
        s1 = s1 + " and r.CodigoCliente = '" + codigoCliente + "'";
        s1 += " union";
        s1 += " select a.CodigoPais, NombreAduana as Aduana, c.NombreCliente, r.IdHojaRuta, Tipo = 'Terminada' from";
        s1 += " Clientes c inner join HojaRuta r on (c.CodigoCliente = r.CodigoCliente)";
        s1 += " inner join TiempoFlujo t on (r.IdHojaRuta = t.IdHojaRuta)";
        s1 += " inner join Aduanas a on (a.CodigoAduana = r.CodigoAduana)";
        s1 = s1 + " where CONVERT(VARCHAR, FechaProceso, 111) >= '" + Convert.ToDateTime(fechaInicio).ToString("yyyy/MM/dd") + "'";
        s1 = s1 + " and CONVERT(VARCHAR, FechaProceso, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "'";
        string s3 = s1;
        s1 = s3 + " and r.Estado = '0' and t.IdFlujo = 6 and r.CodigoCliente = '" + codigoCliente + "' and a.CodigoPais = '" + codigoPais + "'";
        s1 += " order by NombreAduana, Tipo, r.IdHojaRuta";
        loadSQL(s1);
    }

    public void loadHojasRutasDetalleAlertasEntregaCargaCliente(string fechaInicio, string fechaFinal, string nombreAduana)
    {
        string s1 = "select NombreAduana as Aduana, NombreCliente as Cliente, r.IdHojaRuta, NumFactura as Factura, Proveedor, DescripcionProducto as Producto, CodigoRegimen as Regimen, (case when t2.Fecha is null then cast((DateDiff(second, t1.Fecha, GETDATE())) / 86400 as numeric(18,0)) else cast((DateDiff(second, t1.Fecha, t2.Fecha)) / 86400 as numeric(18,0)) end) as DiasRango, cod.descripcion as EstatusActual";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join Clientes cl on (r.CodigoCliente = cl.CodigoCliente)";
        s1 += " inner join ComplementoRecepcion c on (r.IdHojaRuta = c.IdHojaRuta)";
        s1 += " inner join TramiteAduanero t1 on (r.IdHojaRuta = t1.IdHojaRuta and t1.Codigo = 13)";
        s1 += " inner join Codigos cod on (cod.codigo = r.IdFlujo and cod.clasificacion = 'FLUJO')";
        s1 += " left join TramiteAduanero t2 on (r.IdHojaRuta = t2.IdHojaRuta and t2.Codigo = 14)";
        string s2 = s1;
        s1 = s2 + " where CONVERT(VARCHAR, t1.Fecha, 111) >= '" + fechaInicio + "' and CONVERT(VARCHAR, t1.Fecha, 111) <= '" + fechaFinal + "'";
        s1 += " and r.Estado = '0' and (case when t2.Fecha is null then cast((DateDiff(second, t1.Fecha, GETDATE())) / 86400 as numeric(18,0)) else cast((DateDiff(second, t1.Fecha, t2.Fecha)) / 86400 as numeric(18,0)) end) >= 2";
        s1 = s1 + " and NombreAduana like '%" + nombreAduana + "%'";
        s1 += " order by NombreCliente, r.IdHojaRuta";
        loadSQL(s1);
    }

    public void loadHojasRutasDetalleAlertasEnvioDocumentacion(string fechaInicio, string fechaFinal, string nombreAduana)
    {
        string s1 = "select NombreAduana as Aduana, NombreCliente as Cliente, r.IdHojaRuta, NumFactura as Factura, Proveedor, DescripcionProducto as Producto, CodigoRegimen as Regimen, (case when t2.Fecha is null then cast((DateDiff(second, t1.Fecha, GETDATE())) / 86400 as numeric(18,0)) else cast((DateDiff(second, t1.Fecha, t2.Fecha)) / 86400 as numeric(18,0)) end) as DiasRango, cod.descripcion as EstatusActual";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join Clientes cl on (r.CodigoCliente = cl.CodigoCliente)";
        s1 += " inner join ComplementoRecepcion c on (r.IdHojaRuta = c.IdHojaRuta)";
        s1 += " inner join TramiteAduanero t1 on (r.IdHojaRuta = t1.IdHojaRuta and t1.Codigo = 14)";
        s1 += " inner join Codigos cod on (cod.codigo = r.IdFlujo and cod.clasificacion = 'FLUJO')";
        s1 += " left join TramiteAduanero t2 on (r.IdHojaRuta = t2.IdHojaRuta and t2.Codigo = 15)";
        string s2 = s1;
        s1 = s2 + " where CONVERT(VARCHAR, t1.Fecha, 111) >= '" + fechaInicio + "' and CONVERT(VARCHAR, t1.Fecha, 111) <= '" + fechaFinal + "'";
        s1 += " and r.Estado = '0' and (case when t2.Fecha is null then cast((DateDiff(second, t1.Fecha, GETDATE())) / 86400 as numeric(18,0)) else cast((DateDiff(second, t1.Fecha, t2.Fecha)) / 86400 as numeric(18,0)) end) >= 3";
        s1 = s1 + " and NombreAduana like '%" + nombreAduana + "%'";
        s1 += " order by NombreCliente, r.IdHojaRuta";
        loadSQL(s1);
    }

    //public void loadHojasRutasDetalleAlertasReciboDocumentacion(string fechaInicio, string fechaFinal, string nombreAduana)
    //{
    //    string s1 = "select NombreAduana as Aduana, NombreCliente as Cliente, r.IdHojaRuta, NumFactura as Factura, Proveedor, DescripcionProducto as Producto, CodigoRegimen as Regimen, (case when t2.Fecha is null then cast((DateDiff(second, t1.Fecha, GETDATE())) / 86400 as numeric(18,0)) else cast((DateDiff(second, t1.Fecha, t2.Fecha)) / 86400 as numeric(18,0)) end) as DiasRango, cod.descripcion as EstatusActual";
    //    s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
    //    s1 += " inner join Clientes cl on (r.CodigoCliente = cl.CodigoCliente)";
    //    s1 += " inner join ComplementoRecepcion c on (r.IdHojaRuta = c.IdHojaRuta)";
    //    s1 += " inner join TramiteAduanero t1 on (r.IdHojaRuta = t1.IdHojaRuta and t1.Codigo = 15)";
    //    s1 += " inner join Codigos cod on (cod.codigo = r.IdFlujo and cod.clasificacion = 'FLUJO')";
    //    s1 += " left join TramiteAduanero t2 on (r.IdHojaRuta = t2.IdHojaRuta and t2.Codigo = 18)";
    //    string s2 = s1;
    //    s1 = s2 + " where CONVERT(VARCHAR, t1.Fecha, 111) >= '" + fechaInicio + "' and CONVERT(VARCHAR, t1.Fecha, 111) <= '" + fechaFinal + "'";
    //    s1 += " and r.Estado = '0' and (case when t2.Fecha is null then cast((DateDiff(second, t1.Fecha, GETDATE())) / 86400 as numeric(18,0)) else cast((DateDiff(second, t1.Fecha, t2.Fecha)) / 86400 as numeric(18,0)) end) >= 2";
    //    s1 = s1 + " and NombreAduana like '%" + nombreAduana + "%'";
    //    s1 += " order by NombreCliente, r.IdHojaRuta";
    //    loadSQL(s1);
    //}

    public void loadMaxCodigoHojaRutaAduana(string codAduana)
    {
        string s = "select Top 1 IdHojaRuta=substring(IdHojaRuta,charindex('-',IdHojaRuta,charindex('-',IdHojaRuta)+1)+1 , len(IdHojaRuta)) + 1 from HojaRuta";
        s = s + " where CodigoAduana = '" + codAduana + "' order by substring(IdHojaRuta,charindex('-',IdHojaRuta,charindex('-',IdHojaRuta)+1)+1 , len(IdHojaRuta)) + 1  desc";
        loadSQL(s);
    }

    public void loadReporteEmbotelladora(string fecha, string codigoCliente, string codigoPais)
    {
        string s1 = "Select r.CodigoAduana, NombreAduana, r.IdHojaRuta, ReferenciaCliente, Correlativo, ValorSerie,";
        s1 += " DescripcionProducto, c.descripcion as regimen, NumFactura, CONVERT(VARCHAR, ti.Fecha, 101) as FechaE,";
        s1 += " CONVERT(VARCHAR, ti.Fecha, 108) as HoraE, upper(c1.descripcion) as pais, Destino,";
        s1 += " Motorista, Impuesto, (case when tf.Fecha is null then '-' else CONVERT(VARCHAR, tf.Fecha, 101) end) as FechaS";
        s1 += " , (case when tf.Fecha is null then '-' else CONVERT(VARCHAR, tf.Fecha, 108) end) as HoraS, NombreCliente";
        s1 += " from HojaRuta r inner join ComplementoRecepcion com on (r.IdHojaRuta = com.IdHojaRuta)";
        s1 += " inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join TramiteAduanero ti on (ti.IdHojaRuta = r.IdHojaRuta and ti.Codigo = '1')";
        s1 += " inner join TramiteAduanero tm on (tm.IdHojaRuta = r.IdHojaRuta and tm.Codigo = '8')";
        s1 += " left join TramiteAduanero tf on (tf.IdHojaRuta = r.IdHojaRuta and tf.Codigo = '13')";
        if (codigoPais == "HN")
            s1 += " inner join Codigos c on (c.codigo = com.CodigoRegimen and c.clasificacion = 'REGIMEN')";
        else
            s1 += " inner join Codigos c on (c.codigo = com.CodigoRegimen and c.clasificacion = 'REGIMENGUATEMALA')";
        s1 += " inner join Codigos c1 on (c1.codigo = com.CodigoPais and c1.clasificacion = 'PAISES')";
        s1 += " inner join Clientes cli on (cli.CodigoCliente = r.CodigoCliente)";
        string s2 = s1;
        s1 = s2 + " where r.Estado = '0' and r.CodigoCliente = " + codigoCliente + " and CONVERT(VARCHAR, tm.Fecha, 111) = '" + Convert.ToDateTime(fecha).ToString("yyyy/MM/dd") + "' and a.CodigoPais = '" + codigoPais + "'";
        s1 += " order by NombreAduana, Impuesto, substring(r.IdHojaRuta,charindex('-',r.IdHojaRuta,charindex('-',r.IdHojaRuta)+1)+1 , len(r.IdHojaRuta))";
        loadSQL(s1);
    }

    public void loadReporteTiemposProceso(string fechaInicial, string fechaFinal, string codigoPais, string desde, string hasta)
    {
        string s1 = "select a.CodigoPais, NombreCliente as Cliente, NombreAduana, r.IdHojaRuta, CodigoRegimen, ('Dias: ' + cast((DateDiff(second, t1.Fecha, t2.Fecha)) / 86400 as varchar(50)) + ' Horas: ' + cast(((DateDiff(second, t1.Fecha, t2.Fecha)) % 86400) / 3600 as varchar(50)) + ' Mins: ' + cast(((DateDiff(second, t1.Fecha, t2.Fecha)) % 3600) / 60 as varchar(50))) as Tiempo, cast((DateDiff(second, t1.Fecha, t2.Fecha)) / 86400 as numeric(18,0)) as DiasRango";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join Clientes cl on (r.CodigoCliente = cl.CodigoCliente)";
        s1 += " inner join ComplementoRecepcion c on (r.IdHojaRuta = c.IdHojaRuta)";
        s1 = s1 + " inner join TramiteAduanero t1 on (r.IdHojaRuta = t1.IdHojaRuta and t1.Codigo = " + desde + ")";
        s1 += " inner join Codigos cod on (cod.codigo = r.IdFlujo and cod.clasificacion = 'FLUJO')";
        s1 = s1 + " inner join TramiteAduanero t2 on (r.IdHojaRuta = t2.IdHojaRuta and t2.Codigo = " + hasta + ")";
        s1 = s1 + " where CONVERT(VARCHAR, t1.Fecha, 111) >= '" + Convert.ToDateTime(fechaInicial).ToString("yyyy/MM/dd") + "'";
        string s2 = s1;
        s1 = s2 + " and CONVERT(VARCHAR, t1.Fecha, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and r.Estado = '0' and a.CodigoPais = '" + codigoPais + "'";
        s1 += " order by a.NombreAduana, NombreCliente, cast(substring(r.IdHojaRuta,charindex('-',r.IdHojaRuta,charindex('-',r.IdHojaRuta)+1)+1 , len(r.IdHojaRuta)) as numeric(18,0))";
        loadSQL(s1);
    }

    public void loadReporteTiemposProceso(string fechaInicial, string fechaFinal, string codigoPais, string desde, string hasta, string codigoAduana)
    {
        string s1 = "select a.CodigoPais, NombreCliente as Cliente, NombreAduana, r.IdHojaRuta, CodigoRegimen, ('Dias: ' + cast((DateDiff(second, t1.Fecha, t2.Fecha)) / 86400 as varchar(50)) + ' Horas: ' + cast(((DateDiff(second, t1.Fecha, t2.Fecha)) % 86400) / 3600 as varchar(50)) + ' Mins: ' + cast(((DateDiff(second, t1.Fecha, t2.Fecha)) % 3600) / 60 as varchar(50))) as Tiempo, cast((DateDiff(second, t1.Fecha, t2.Fecha)) / 86400 as numeric(18,0)) as DiasRango";
        s1 += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        s1 += " inner join Clientes cl on (r.CodigoCliente = cl.CodigoCliente)";
        s1 += " inner join ComplementoRecepcion c on (r.IdHojaRuta = c.IdHojaRuta)";
        s1 = s1 + " inner join TramiteAduanero t1 on (r.IdHojaRuta = t1.IdHojaRuta and t1.Codigo = " + desde + ")";
        s1 += " inner join Codigos cod on (cod.codigo = r.IdFlujo and cod.clasificacion = 'FLUJO')";
        s1 = s1 + " inner join TramiteAduanero t2 on (r.IdHojaRuta = t2.IdHojaRuta and t2.Codigo = " + hasta + ")";
        s1 = s1 + " where CONVERT(VARCHAR, t1.Fecha, 111) >= '" + Convert.ToDateTime(fechaInicial).ToString("yyyy/MM/dd") + "'";
        string s2 = s1;
        s1 = s2 + " and CONVERT(VARCHAR, t1.Fecha, 111) <= '" + Convert.ToDateTime(fechaFinal).ToString("yyyy/MM/dd") + "' and r.Estado = '0' and a.CodigoPais = '" + codigoPais + "' and r.CodigoAduana = '" + codigoAduana + "'";
        s1 += " order by NombreCliente, r.CodigoAduana, cast(substring(r.IdHojaRuta,charindex('-',r.IdHojaRuta,charindex('-',r.IdHojaRuta)+1)+1 , len(r.IdHojaRuta)) as numeric(18,0))";
        loadSQL(s1);
    }

    public void loadResumenHojaRutaPais(string idHojaRuta, string codPais)
    {
        string s = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor , NombreAduana as Aduana from";
        s += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana)";
        s = s + " where a.CodigoPais = '" + codPais + "' and IdHojaRuta like '%" + idHojaRuta + "%' and r.Estado = '0' and cast(IdFlujo as numeric(18,0)) < 6";
        loadSQL(s);
    }

    public void loadResumenHojaRuta(string idHojaRuta)
    {
        string s = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor , NombreAduana as Aduana from";
        s += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana)";
        s = s + " where IdHojaRuta like '%" + idHojaRuta + "%' and r.Estado = '0' and cast(IdFlujo as numeric(18,0)) < 6";
        loadSQL(s);
    }

    public void loadResumenHojaRuta()
    {
        string s = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor from";
        s += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        s += " where r.Estado = '0' and IdFlujo <> '6'";
        loadSQL(s);
    }

    public void loadResumenHojaRutaPais(string codPais)
    {
        string s = "select a.CodigoPais as Pais, IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor from";
        s += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        s += " where a.CodigoPais = '" + codPais + "' and r.Estado = '0' and IdFlujo <> '6'";
        loadSQL(s);
    }

    public void loadResumenHojaRutaAduana(string codigoAduana, string idHojaRuta, string idFlujo, bool orden)
    {
        string s1 = "select a.CodigoPais as Pais, r.IdHojaRuta, FechaRecepcion, r.NumFactura as Factura, NombreCliente as Cliente, r.Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor from";
        s1 += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
        if (orden)
            s1 += " inner join OrdenTrabajo o on (r.IdHojaRuta = o.IdHojaRuta)";
        string s2 = s1;
        s1 = s2 + " where r.IdHojaRuta like '%" + idHojaRuta + "%' and r.CodigoAduana = '" + codigoAduana + "' and r.Estado = '0' and cast(IdFlujo as numeric(18,0)) <= " + idFlujo;
        loadSQL(s1);
    }

    public void loadResumenHojaRutaAduana(string codigoAduana)
    {
        string s = "select a.CodigoPais as Pais, r.IdHojaRuta, FechaRecepcion, r.NumFactura as Factura, NombreCliente as Cliente, r.Proveedor, NombreAduana as Aduana, NombreEmpleado as Gestor from";
        s += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana) inner join Empleados as e on (r.IdReceptor = e.IdEmpleado)";
//        s += " inner join OrdenTrabajo o on (r.IdHojaRuta = o.IdHojaRuta)";
        s = s + " where r.Estado = '0' and r.CodigoAduana = '" + codigoAduana + "' and cast(IdFlujo as numeric(18,0)) <= 3";
        loadSQL(s);
    }

    public void loadResumenHojaRutaEmpleado(string idEmpleado, string idHojaRuta)
    {
        string s1 = "select IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana from";
        s1 += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana)";
        string s2 = s1;
        s1 = s2 + " where IdHojaRuta like '%" + idHojaRuta + "%' and IdReceptor = " + idEmpleado + " and r.Estado = '0' and IdFlujo <> '6'";
        loadSQL(s1);
    }

    public void loadResumenHojaRutaEmpleado(string idEmpleado)
    {
        string s = "select IdHojaRuta, FechaRecepcion, NumFactura as Factura, NombreCliente as Cliente, Proveedor, NombreAduana as Aduana from";
        s += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana)";
        s = s + " where IdReceptor = " + idEmpleado + " and r.Estado = '0' and IdFlujo <> '6'";
        loadSQL(s);
    }

    public void loadResumenHojaRutaOrdenesTrabajoEmpleado(string idEmpleado, string idHoja, string idFlujo, bool orden)
    {
        string s1 = "select a.CodigoPais as Pais, r.IdHojaRuta, FechaRecepcion, r.NumFactura as Factura, NombreCliente as Cliente, r.Proveedor, NombreAduana as Aduana from";
        s1 += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana)";
        if (orden)
            s1 += " inner join OrdenTrabajo o on (r.IdHojaRuta = o.IdHojaRuta)";
        string s2 = s1;
        s1 = s2 + " where r.IdHojaRuta like '%" + idHoja + "%' and IdReceptor = " + idEmpleado + " and r.Estado = '0' and cast(IdFlujo as numeric(18,0)) <= " + idFlujo;
        loadSQL(s1);
    }

    public void loadResumenHojaRutaOrdenesTrabajoInplant(string idHoja, string idFlujo, bool orden)
    {
        string s1 = "select a.CodigoPais as Pais, r.IdHojaRuta, FechaRecepcion, r.NumFactura as Factura, NombreCliente as Cliente, r.Proveedor, NombreAduana as Aduana from";
        s1 += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s1 += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana)";
        if (orden)
            s1 += " inner join OrdenTrabajo o on (r.IdHojaRuta = o.IdHojaRuta)";
        string s2 = s1;
        s1 = s2 + " where r.IdHojaRuta like '%" + idHoja + "%' and r.Estado = '0' and cast(IdFlujo as numeric(18,0)) <= " + idFlujo;
        loadSQL(s1);
    }

    public void loadResumenHojaRutaOrdenesTrabajoEmpleado(string idEmpleado)
    {
        string s = "select a.CodigoPais as Pais, r.IdHojaRuta, FechaRecepcion, r.NumFactura as Factura, NombreCliente as Cliente, r.Proveedor, NombreAduana as Aduana from";
        s += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana)";
        s += " inner join OrdenTrabajo o on (r.IdHojaRuta = o.IdHojaRuta)";
        s = s + " where IdReceptor = " + idEmpleado + " and r.Estado = '0' and cast(IdFlujo as numeric(18,0)) <= 1";
        loadSQL(s);
    }

    public void loadResumenHojaRutasEmpleado(string idEmpleado)
    {
        string s = "select a.CodigoPais as Pais, r.IdHojaRuta, FechaRecepcion, r.NumFactura as Factura, NombreCliente as Cliente, r.Proveedor, NombreAduana as Aduana from";
        s += " HojaRuta as r inner join Clientes as c on (r.CodigoCliente = c.CodigoCliente)";
        s += " inner join Aduanas as a on (r.CodigoAduana = a.CodigoAduana)";
        s = s + " where IdReceptor = " + idEmpleado + " and r.Estado = '0' and cast(IdFlujo as numeric(18,0)) <= 3";
        loadSQL(s);
    }
} // class HojaRutaBO

