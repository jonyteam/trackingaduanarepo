using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for TramiteAduaneroBO
/// </summary>
public class TramiteAduaneroBO: CapaBase
{

    class Campos
    {
        public static string IDHOJARUTA = "IdHojaRuta";
        public static string CODIGO = "Codigo";
        public static string OBSERVACION = "Observacion";
        public static string FECHA = "Fecha";
        public static string ESTADO = "Estado";
    }
    
    public TramiteAduaneroBO(GrupoLis.Login.Login log): base(log, true)
	{
        this.coreSQL = "Select * from TramiteAduanero";
        this.initializeSchema("TramiteAduanero");
    }

    public void loadTramiteAduanero()
    {
        string sql = this.coreSQL;
        sql += " where Estado = '0'";
        this.loadSQL(sql);
    }

    public void loadTramiteAduanero(string idHoja)
    {
        string sql = this.coreSQL;
        sql += " where IdHojaRuta = '" + idHoja + "' and Estado = '0' order by cast(Codigo as numeric(18,0))";
        this.loadSQL(sql);
    }

    public void loadTramiteAduanero(string idHoja, string codigo)
    {
        string sql = this.coreSQL;
        sql += " where IdHojaRuta = '" + idHoja + "' and Codigo = '" + codigo + "' and Estado = '0'";
        this.loadSQL(sql);
    }

    public void loadMaxMinFechaProcesoAduanero(string idHoja)
    {
        string sql = "select  dias = cast((DateDiff(second, tf.FechaProceso, tf2.FechaProceso)) / 86400 as varchar(50)), horas = cast(((DateDiff(second, tf.FechaProceso, tf2.FechaProceso)) % 86400) / 3600 as varchar(50)),";
        sql += " minutos = cast(((DateDiff(second, tf.FechaProceso, tf2.FechaProceso)) % 3600) / 60 as varchar(50)), segundos = cast(((DateDiff(second, tf.FechaProceso, tf2.FechaProceso)) % 60) as varchar(50))";
        sql += " from TiempoFlujo tf inner join TiempoFlujo tf2 on (tf.IdHojaRuta = tf2.IdHojaRuta)";
        sql += " where tf.IdHojaRuta = '" + idHoja + "' and tf.IdFlujo = '3' and tf2.IdFlujo = '4' and tf.Estado = '0'";
        this.loadSQL(sql);
    }

    public void loadMaxMinFechaProcesoAduanero(string idHoja, string idFlujo)
    {
        string sql = "select  dias = cast((DateDiff(second, Min(Fecha), Max(Fecha))) / 86400 as varchar(50)), horas = cast(((DateDiff(second, Min(Fecha), Max(Fecha))) % 86400) / 3600 as varchar(50)),";
        sql += " minutos = cast(((DateDiff(second, Min(Fecha), Max(Fecha))) % 3600) / 60 as varchar(50)), segundos = cast(((DateDiff(second, Min(Fecha), Max(Fecha))) % 60) as varchar(50))";
        sql += " from TramiteAduanero ta inner join HojaRuta r on (ta.IdHojaRuta = r.IdHojaRuta)";
        sql += " where ta.IdHojaRuta = '" + idHoja + "' and r.IdFlujo = '" + idFlujo + "' and r.Estado = '0'";
        this.loadSQL(sql);
    }

    public void loadFechaProcesoAduanero(string idHoja, string codigo)
    {
        string sql = "select CONVERT(VARCHAR, Fecha, 101) from TramiteAduanero";
        sql += " where IdHojaRuta = '" + idHoja + "' and cast(Codigo as numeric(18,0)) = " + codigo + " and Estado = '0'";
        this.loadSQL(sql);
    }

    public void loadTiempoProcesoAduanero(string idHoja, string codigo)
    {
        string sql = "select CONVERT(VARCHAR, Fecha, 108) from TramiteAduanero";
        sql += " where IdHojaRuta = '" + idHoja + "' and cast(Codigo as numeric(18,0)) = " + codigo + " and Estado = '0'";
        this.loadSQL(sql);
    }

    public void loadMaxMinFechaProcesoFacturacion(string idHoja, string codigo)
    {
        string sql = "select  Min(Fecha) as minimo, Max(Fecha) as maximo from TramiteAduanero";
        sql += " where IdHojaRuta = '" + idHoja + "' and cast(Codigo as numeric(18,0)) >= " + codigo + " and Estado = '0'";
        this.loadSQL(sql);
    }

    public void loadProcesoFacturacion(string idHoja, string codigo)
    {
        string sql = "select  * from TramiteAduanero";
        sql += " where IdHojaRuta = '" + idHoja + "' and cast(Codigo as numeric(18,0)) >= " + codigo + " and Estado = '0'";
        this.loadSQL(sql);
    }

    public void loadProcesoAduanero(string idHoja, string codigo)
    {
        string sql = "select  Min(Fecha) as minimo, Max(Fecha) as maximo from TramiteAduanero";
        sql += " where IdHojaRuta = '" + idHoja + "' and cast(Codigo as numeric(18,0)) <= " + codigo + " and Estado = '0'";
        this.loadSQL(sql);
    }

    public void loadReporteProcesoAduanero(string codPais)
    {
        string sql = "select t.CodigoAduana, NombreAduana, t.CodigoCliente, NombreCliente, count(*) as Cantidad";
        sql += " from HojaRuta t inner join Aduanas a on (t.CodigoAduana = a.CodigoAduana)";
        sql += " inner join Clientes c on (t.CodigoCliente = c.CodigoCliente)";
        sql += " where (t.IdFlujo = '2' or t.IdFlujo = '3') and a.CodigoPais = '" + codPais + "' and t.Estado = '0'";
        sql += " group by t.CodigoAduana, NombreAduana, t.CodigoCliente, NombreCliente";
        sql += " order by NombreAduana, NombreCliente";
        this.loadSQL(sql);
    }

    public void loadReporteProcesoAduanero(string codPais, string codCliente)
    {
        string sql = "select t.CodigoAduana, NombreAduana, t.CodigoCliente, NombreCliente, count(*) as Cantidad";
        sql += " from HojaRuta t inner join Aduanas a on (t.CodigoAduana = a.CodigoAduana)";
        sql += " inner join Clientes c on (t.CodigoCliente = c.CodigoCliente)";
        sql += " where (t.IdFlujo = '2' or t.IdFlujo = '3') and a.CodigoPais = '" + codPais + "' and t.Estado = '0' and t.CodigoCliente = " + codCliente;
        sql += " group by t.CodigoAduana, NombreAduana, t.CodigoCliente, NombreCliente";
        sql += " order by NombreAduana, NombreCliente";
        this.loadSQL(sql);
    }

    public void loadTramitesProcesoAduanero(string codPais, string codCliente)
    {
        string sql = "select IdHojaRuta, CodigoAduana, NombreAduana, NumFactura, ReferenciaCliente, c1.descripcion + ' - ' + c.descripcion as Flujo";
        sql += " from";
        sql += " (select t.IdHojaRuta, t.CodigoAduana, NombreAduana, t.NumFactura, t.ReferenciaCliente, max(c1.codigo) as codFlujo, max(c.codigo) as codEtapa";
        sql += " from HojaRuta t inner join Aduanas a on (t.CodigoAduana = a.CodigoAduana)";
        sql += " inner join TramiteAduanero ta on (t.IdHojaRuta = ta.IdHojaRuta)";
        sql += " inner join Codigos c on (c.codigo = ta.Codigo and (c.clasificacion = 'PROCESOADUANERO' or c.clasificacion = 'PREPROCESO'))";
        sql += " inner join Codigos c1 on (c1.codigo = t.IdFlujo and c1.clasificacion = 'FLUJO') where (t.IdFlujo = '2' or t.IdFlujo = '3')";
        sql += " and a.CodigoPais = '" + codPais + "' and t.Estado = '0' and t.CodigoCliente = " + codCliente;
        sql += " group by t.IdHojaRuta, t.CodigoAduana, NombreAduana, t.NumFactura, t.ReferenciaCliente) as p";
        sql += " inner join Codigos c on (c.codigo = p.codEtapa and (c.clasificacion = 'PROCESOADUANERO' or c.clasificacion = 'PREPROCESO'))";
        sql += " inner join Codigos c1 on (c1.codigo = p.codFlujo and c1.clasificacion = 'FLUJO')";
        sql += " order by p.NombreAduana, p.IdHojaRuta";
        this.loadSQL(sql);
    }

    public void loadTramiteProcesoAduanero(string idHoja, string codigoAduana)
    {
        string sql = "select cod.descripcion as etapa, CONVERT(VARCHAR(26),Fecha) as fecha, ta.Observacion";
        sql += " from HojaRuta r inner join TramiteAduanero ta on (r.IdHojaRuta = ta.IdHojaRuta) inner join Codigos cod on (ta.Codigo = cod.codigo and (cod.clasificacion = 'PREPROCESO' or cod.clasificacion = 'PROCESOADUANERO'))";
        sql += " where ta.IdHojaRuta = '" + idHoja + "' and r.Estado = '0' and r.CodigoAduana = '" + codigoAduana + "' order by cast(ta.Codigo as numeric)";
        this.loadSQL(sql);
    }

    public void loadTramiteProcesoAduaneroPais(string idHoja, string codPais)
    {
        string sql = "select cod.descripcion as etapa, CONVERT(VARCHAR(26),Fecha) as fecha, ta.Observacion";
        sql += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana) inner join TramiteAduanero ta on (r.IdHojaRuta = ta.IdHojaRuta) inner join Codigos cod on (ta.Codigo = cod.codigo and (cod.clasificacion = 'PREPROCESO' or cod.clasificacion = 'PROCESOADUANERO'))";
        sql += " where ta.IdHojaRuta = '" + idHoja + "' and r.Estado = '0' and a.CodigoPais = '" + codPais + "' order by cast(ta.Codigo as numeric)";
        this.loadSQL(sql);
    }

    public void loadTramiteProcesoAduaneroGestor(string idHoja, string idEmpleado)
    {
        string sql = "select cod.descripcion as etapa, CONVERT(VARCHAR(26),Fecha) as fecha, ta.Observacion";
        sql += " from HojaRuta r inner join TramiteAduanero ta on (r.IdHojaRuta = ta.IdHojaRuta) inner join Codigos cod on (ta.Codigo = cod.codigo and (cod.clasificacion = 'PREPROCESO' or cod.clasificacion = 'PROCESOADUANERO'))";
        sql += " where ta.IdHojaRuta = '" + idHoja + "' and r.Estado = '0' and r.IdReceptor = " + idEmpleado + " order by cast(ta.Codigo as numeric)";
        this.loadSQL(sql);
    }

    public void loadTramiteProcesoAduanero(string idHoja)
    {
        string sql = "select cod.descripcion as etapa, CONVERT(VARCHAR(26),Fecha) as fecha, ta.Observacion";
        sql += " from HojaRuta r inner join TramiteAduanero ta on (r.IdHojaRuta = ta.IdHojaRuta) inner join Codigos cod on (ta.Codigo = cod.codigo and (cod.clasificacion = 'PREPROCESO' or cod.clasificacion = 'PROCESOADUANERO'))";
        sql += " where ta.IdHojaRuta = '" + idHoja + "' and r.Estado = '0' order by cast(ta.Codigo as numeric)";
        this.loadSQL(sql);
    }

    public void loadDetalleReporteProcesoAduanero(string codPais, string codCliente, string codAduana)
    {
        string sql = "select r.IdHojaRuta, r.DescripcionProducto, r.NumFactura, t1.Fecha as t1, t2.Fecha as t2, t3.Fecha as t3, t4.Fecha as t4, t5.Fecha as t5, t6.Fecha as t6, t7.Fecha as t7,";
        sql += " t8.Fecha as t8, t9.Fecha as t9, t10.Fecha as t10, t11.Fecha as t11, t12.Fecha as t12";
        sql += " from HojaRuta r inner join Aduanas a on (r.CodigoAduana = a.CodigoAduana)";
        sql += " left join TramiteAduanero t1 on (t1.IdHojaRuta = r.IdHojaRuta and t1.Codigo = '1')";
        sql += " left join TramiteAduanero t2 on (t2.IdHojaRuta = r.IdHojaRuta and t2.Codigo = '2')";
        sql += " left join TramiteAduanero t3 on (t3.IdHojaRuta = r.IdHojaRuta and t3.Codigo = '3')";
        sql += " left join TramiteAduanero t4 on (t4.IdHojaRuta = r.IdHojaRuta and t4.Codigo = '4')";
        sql += " left join TramiteAduanero t5 on (t5.IdHojaRuta = r.IdHojaRuta and t5.Codigo = '5')";
        sql += " left join TramiteAduanero t6 on (t6.IdHojaRuta = r.IdHojaRuta and t6.Codigo = '6')";
        sql += " left join TramiteAduanero t7 on (t7.IdHojaRuta = r.IdHojaRuta and t7.Codigo = '7')";
        sql += " left join TramiteAduanero t8 on (t8.IdHojaRuta = r.IdHojaRuta and t8.Codigo = '8')";
        sql += " left join TramiteAduanero t9 on (t9.IdHojaRuta = r.IdHojaRuta and t9.Codigo = '9')";
        sql += " left join TramiteAduanero t10 on (t10.IdHojaRuta = r.IdHojaRuta and t10.Codigo = '10')";
        sql += " left join TramiteAduanero t11 on (t11.IdHojaRuta = r.IdHojaRuta and t11.Codigo = '11')";
        sql += " left join TramiteAduanero t12 on (t12.IdHojaRuta = r.IdHojaRuta and t12.Codigo = '12')";
        sql += " where (r.IdFlujo = '2' or r.IdFlujo = '3') and r.CodigoCliente = " + codCliente + " and a.CodigoPais = '" + codPais + "' and a.CodigoAduana = '" + codAduana + "' and r.Estado = '0'";
        this.loadSQL(sql);
    }

    public void loadMaxMinFecha(string idHoja)
    {
        string sql = "select  Min(Fecha) as minimo, Max(Fecha) as maximo from TramiteAduanero";
        sql += " where IdHojaRuta = '" + idHoja + "' and Estado = '0'";
        this.loadSQL(sql);
    }

    public int darBajaTramiteAduanero(string codigo)
    {
        return this.executeCustomSQL("update TramiteAduanero set Estado = '1' where IdHojaRuta = '" + codigo + "'");
    }

    public string IDHOJARUTA
    {
        get
        {
            return (string)registro[Campos.IDHOJARUTA].ToString();
        }
        set
        {
            registro[Campos.IDHOJARUTA] = value;
        }
    }

    public string CODIGO
    {
        get
        {
            return (string)registro[Campos.CODIGO].ToString();
        }
        set
        {
            registro[Campos.CODIGO] = value;
        }
    }

    public string OBSERVACION
    {
        get
        {
            return (string)registro[Campos.OBSERVACION].ToString();
        }
        set
        {
            registro[Campos.OBSERVACION] = value;
        }
    }

    public string FECHA
    {
        get
        {
            return (string)registro[Campos.FECHA].ToString();
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }

    public DateTime FECHADT
    {
        get
        {
            return Convert.ToDateTime(registro[Campos.FECHA]);
        }
        set
        {
            registro[Campos.FECHA] = value;
        }
    }

    public DateTime FECHAMIN
    {
        get
        {
            return (DateTime)registro["minimo"];
        }
    }

    public DateTime FECHAMAX
    {
        get
        {
            return (DateTime)registro["maximo"];
        }
    }

    public string ESTADO
    {
        get
        {
            return (string)registro[Campos.ESTADO].ToString();
        }
        set
        {
            registro[Campos.ESTADO] = value;
        }
    }

}
