﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;

using GrupoLis.Ebase;
using GrupoLis.Login;

public class CodigosBO : CapaBase
{
    class Campos
    {
        public static string CATEGORIA = "Categoria";
        public static string CODIGO = "Codigo";
        public static string ORDEN = "Orden";
        public static string DESCRIPCION = "Descripcion";
    }

    public CodigosBO(GrupoLis.Login.Login conector)
        : base(conector, true)
    {
        coreSQL = " SELECT * FROM Codigos ";
        initializeSchema("Codigos");
    }

    public void loadOtrosDocumentos(string regimen, string codPais)
    {
        string sql = this.coreSQL;
        sql += " WHERE Categoria = 'TIPODOCUMENTO' AND Eliminado = '0' ";
        sql += " AND Codigo NOT IN (SELECT CodDocumento FROM DocumentosRegimen ";
        sql += " WHERE CodRegimen = '" + regimen + "' AND Eliminado = '0') ";
        this.loadSQL(sql);
    }

    public void loadAllCampos()
    {
        loadSQL(coreSQL + " WHERE (Eliminado=0)");
    }

    public void loadAllCampos(String categoria)
    {
        string sql = " SELECT *, Codigo + ' | ' + Descripcion AS CodigoDescripcion FROM Codigos ";
        sql += " WHERE (Eliminado=0)  AND Categoria=@Categoria order by 4 ";
        loadSQL(sql, new Parametro("@Categoria", categoria));
    }
    //

    public void loadEspeciesnoAdmin()
    {
        loadSQL(coreSQL + " WHERE (Eliminado=0) and Categoria = 'ESPECIESFISCALES' and Codigo in ('DA','DTI','DVA','F','HN-MI','HN-MN','P','SS','ZL','RT','SSB') ORDER BY Orden");
    }

    public void Regimen(string regimen)
    {
        loadSQL(" SELECT Codigo, Codigo +'|'+Descripcion as Regimen FROM Codigos WHERE (Eliminado=0) and Categoria = '" + regimen + "'");
    }
    public void loadDivision(string Division)
    {
        loadSQL(coreSQL + " WHERE (Eliminado=0) and Categoria = '" + Division + "' order by 4");
    }

    public void CargarDivision(string Division, string Codigo)
    {
        loadSQL(coreSQL + " WHERE (Eliminado=0) and Categoria = '" + Division + "' and Codigo = '" + Codigo + "'order by 4");
    }
    //public void loadAllCamposRegimen(String categoria)
    //{
    //    string sql = " SELECT *, Codigo + ' | ' + Descripcion AS CodigoDescripcion FROM Codigos ";
    //    loadSQL(sql + " WHERE (Eliminado=0) AND Categoria=@Categoria", new Parametro("@Categoria", categoria));
    //}

    public void loadPaisesHojaRuta()
    {
        string sql = " SELECT * FROM Codigos ";
        sql += " WHERE Categoria = 'PAISES' AND Codigo IN ('H','G','S','N') ORDER BY Orden ";
        this.loadSQL(sql);
    }
    public void loadPaiseXHojaRuta(string pais)
    {
        string sql = " SELECT * FROM Codigos ";
        sql += " WHERE Categoria = 'PAISES' AND Codigo IN ('" + pais + "') ORDER BY Orden ";
        this.loadSQL(sql);
    }

    public void loadAllCamposCodigos(String categoria)
    {
        loadSQL(coreSQL + " WHERE (Eliminado=0) AND Categoria=@Categoria ORDER BY 4 ", new Parametro("@Categoria", categoria));
    }

    public void loadAllCamposCodigos(String categoria, String descripcion)
    {
        string sql = coreSQL;
        sql += " WHERE Categoria = '" + categoria + "' AND Descripcion = '" + descripcion + "' AND Eliminado = 0 ";
        this.loadSQL(sql);
    }

    public void loadFlujosGuia(int codEstadoInicial, int codEstadoFinal)
    {
        string sql = coreSQL;
        sql += " WHERE Eliminado = 0 AND Categoria = 'FLUJOTERRESTRE' AND Codigo BETWEEN " + codEstadoInicial + " AND " + codEstadoFinal + " ORDER BY 4 ";
        this.loadSQL(sql);
    }

    public void loadEventosSinArriboCarga()
    {
        string sql = coreSQL;
        sql += " WHERE Eliminado = 0 AND Categoria = 'EVENTOS' AND Codigo != '1' ORDER BY 4 ";
        this.loadSQL(sql);
    }
    //public void loadEventosSinArriboCarga()
    //{
    //    string sql = coreSQL;
    //    sql += " WHERE Eliminado = 0 AND Categoria = 'EVENTOS' AND Codigo != '1' ORDER BY 4 ";
    //    this.loadSQL(sql);
    //}
    public void loadAllCampos(String categoria, String codigo)
    {
        loadSQL(coreSQL + " WHERE (Eliminado=0) AND Categoria=@Categoria and Codigo=@Codigo", new Parametro("@Categoria", categoria), new Parametro("@Codigo", codigo));
    }

    public void loadCodigosXCategoriaCodigos(String categoria, String codigos)
    {
        string sql = coreSQL;
        sql += " WHERE Categoria = '" + categoria + "' AND Codigo IN (" + codigos + ") AND Eliminado = 0 ";
        this.loadSQL(sql);
    }

    public void loadCodigosXCategoriaCodigosNOT(String categoria, String codigos)
    {
        string sql = coreSQL;
        sql += " WHERE Categoria = '" + categoria + "' AND Codigo NOT IN (" + codigos + ") AND Eliminado = 0 ";
        this.loadSQL(sql);
    }

    public void updateCodigo(String categoria, String codigoViejo, Hashtable nuevosValores)
    {
        if ((nuevosValores != null) && (nuevosValores.Count > 0) && (categoria.Length > 0) && (codigoViejo.Length > 0))
        {
            StringBuilder sb = new StringBuilder("UPDATE Codigos SET ");
            List<Parametro> parametros = new List<Parametro>(nuevosValores.Count);

            bool cc = false;

            foreach (DictionaryEntry de in nuevosValores)
            {
                string key = de.Key.ToString();

                if (cc) sb.Append(',');

                sb.AppendFormat("{0}=@{0}", key);
                parametros.Add(new Parametro("@" + key, de.Value));

                cc = true;
            }

            sb.Append(" WHERE Categoria=@Categoria AND Codigo=@CodigoViejo");
            parametros.Add(new Parametro("@Categoria", categoria));
            parametros.Add(new Parametro("@CodigoViejo", codigoViejo));

            executeCustomSQL(sb.ToString(), parametros.ToArray());
        }
    }

    public void insertCodigo(String categoria, Hashtable nuevosValores)
    {
        if ((nuevosValores != null) && (nuevosValores.Count > 0))
        {
            StringBuilder sbf = new StringBuilder();
            StringBuilder sbv = new StringBuilder();

            List<Parametro> parametros = new List<Parametro>(nuevosValores.Count);

            bool cc = false;

            foreach (DictionaryEntry de in nuevosValores)
            {
                if (cc) { sbf.Append(','); sbv.Append(','); }

                string key = de.Key.ToString();

                sbf.Append(key);
                sbv.Append("@" + key);
                parametros.Add(new Parametro("@" + key, de.Value));

                cc = true;
            }

            sbf.Append(cc ? ",Categoria" : "Categoria");
            sbv.Append(cc ? ",@Categoria" : "@Categoria");
            parametros.Add(new Parametro("@Categoria", categoria));

            executeCustomSQL(String.Format("INSERT INTO Codigos ({0}) VALUES ({1})", sbf.ToString(), sbv.ToString()), parametros.ToArray());
        }
    }

    public void deleteCodigo(String categoria, String codigo)
    {
        executeCustomSQL("UPDATE Codigos SET Eliminado=1 WHERE Categoria=@Categoria AND Codigo=@Codigo",
            new Parametro("@Categoria", categoria),
            new Parametro("@Codigo", codigo));
    }

    #region Campos
    public String CATEGORIA
    {
        get
        {
            return Convert.ToString(registro[Campos.CATEGORIA]);
        }
        set
        {
            registro[Campos.CATEGORIA] = value;
        }
    }

    public String CODIGO
    {
        get
        {
            return Convert.ToString(registro[Campos.CODIGO]);
        }
        set
        {
            registro[Campos.CODIGO] = value.Trim();
        }
    }

    public Int32 ORDEN
    {
        get
        {
            return Convert.ToInt32(registro[Campos.ORDEN]);
        }
        set
        {
            registro[Campos.ORDEN] = value;
        }
    }

    public String DESCRIPCION
    {
        get
        {
            return Convert.ToString(registro[Campos.DESCRIPCION]);
        }
        set
        {
            registro[Campos.DESCRIPCION] = value.Trim();
        }
    }
    #endregion
}
