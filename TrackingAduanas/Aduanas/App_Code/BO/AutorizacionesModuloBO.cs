using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using GrupoLis.Ebase;
using GrupoLis.Login;

/// <summary>
/// Summary description for AutorizacionesModuloBO
/// </summary>
public class AutorizacionesModuloBO : CapaBase
{

    class Campos
    {
        public static string IDROL = "IdRol";
        public static string IDMODULO = "IdModulo";
        public static string ELIMINADO = "Eliminado";
    }

    public AutorizacionesModuloBO(GrupoLis.Login.Login log)
        : base(log, true)
    {
        this.coreSQL = "Select * from AutorizacionesModulo";
        this.initializeSchema("AutorizacionesModulo");
    }

    public void loadAutorizaciones()
    {
        string sql = this.coreSQL;
        sql += " where Eliminado = '0'";
        this.loadSQL(sql);
    }

    public void loadAutorizacionesEstado()
    {
        this.loadSQL(this.coreSQL);
    }

    public void loadAutorizacionesEstadoDescs()
    {
        string sql = "select am.IdRol, am.IdModulo, m.Descripcion as ModuloDescripcion, r.Descripcion as RolDescripcion, am.Eliminado from AutorizacionesModulo as am";
        sql += " inner join Modulos as m on (m.IdModulo=am.IdModulo and m.Eliminado='0')";
        sql += " inner join Roles as r on (r.IdRol=am.IdRol and r.Eliminado='0')";
        sql += " order by m.Descripcion, r.Descripcion";
        this.loadSQL(sql);
    }

    public void loadAutorizacionesRoles(string idRol)
    {
        string sql = this.coreSQL;
        sql += " where IdRol = " + idRol + " and Eliminado = '0'";
        this.loadSQL(sql);
    }


    public void loadAutorizacionesModulo(string idModulo)
    {
        string sql = this.coreSQL;
        sql += " where IdModulo = " + idModulo + " and Eliminado = '0'";
        this.loadSQL(sql);
    }

    public void loadAutorizacionesModulosRoles(string idRol, string idModulo)
    {
        string sql = this.coreSQL;
        sql += " where IdRol = " + idRol + " and IdModulo = " + idModulo + " and Eliminado = '0'";
        this.loadSQL(sql);
    }

    public void loadAutorizacionesModulosRolesEstado(string idRol, string idModulo)
    {
        string sql = this.coreSQL;
        sql += " where IdRol = " + idRol + " and IdModulo = " + idModulo;
        this.loadSQL(sql);
    }

    public void deleAutorizacionesModulos(string idRol, string idModulo)
    {
        string sql = this.coreSQL;
        sql += " where IdRole = " + idRol + " and IdModulo = " + idModulo;
        this.executeCustomSQL(String.Format("delete from AutorizacionesModulo where IdRol={0} and IdModulo={1}", idRol, idModulo));
    }

    public string IDROL
    {
        get
        {
            return (string)registro[Campos.IDROL].ToString();
        }
        set
        {
            registro[Campos.IDROL] = value;
        }
    }

    public string IDMODULO
    {
        get
        {
            return (string)registro[Campos.IDMODULO].ToString();
        }
        set
        {
            registro[Campos.IDMODULO] = value;
        }
    }

    public string ELIMINADO
    {
        get
        {
            return (string)registro[Campos.ELIMINADO].ToString();
        }
        set
        {
            registro[Campos.ELIMINADO] = value;
        }
    }

    public int eliminarAutorizacionesModulo(string codigo)
    {
        return this.executeCustomSQL("delete from AutorizacionesModulo where IdRol = " + codigo);
    }

    public int darBajaAutorizacionesModulo(string codigo)
    {
        return this.executeCustomSQL("update AutorizacionesModulo set Eliminado = '1' where IdRol = " + codigo);
    }

}
