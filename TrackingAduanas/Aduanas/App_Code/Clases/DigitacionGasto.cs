﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de DigitacionGasto
/// </summary>
public class DigitacionGasto
{
    public string ID  ;
    public string IDINSTRUCCION ;
    public string REFERENCIA;
    public string REFERENCIAMAESTRA;
   
	public DigitacionGasto( string id , string hj , string RM)
	{
        this.ID = id;
        this.IDINSTRUCCION = hj;
        this.REFERENCIAMAESTRA = RM;
  
	}

    public void referencia (string rf)
    {
        this.REFERENCIA = rf;
    }
}