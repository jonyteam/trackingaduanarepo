﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de MovimientosRemision
/// </summary>
public class MovimientosRemisionVm
{
    public MovimientosRemisionVm()
    { }

    public int Item { get; set; }
    public int IdRemision { get; set; }
    public string Serie { get; set; }
    public string IdInstruccion { get; set; }
    public string CodAduana { get; set; }
    public string CodEstado { get; set; }
    public string RtnRemitente { get; set; }
    public string RtnEmpresa { get; set; }
    public string RtnMotorista { get; set; }
    public string Licencia { get; set; }
    public string SelloFiscal { get; set; }
    public DateTime? Fecha { get; set; }
    public DateTime? FechaFinTraslado { get; set; }
    public string IdUsuario { get; set; }
    public string Observacion { get; set; }




 


}