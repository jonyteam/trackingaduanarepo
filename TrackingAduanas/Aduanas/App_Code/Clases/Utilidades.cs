﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;
using System.Web.SessionState;
using System.Collections.Specialized;
using GrupoLis.Login;
using System.Text.RegularExpressions;
using System.Security.Principal;
using Telerik.Web;
using System.Security.Cryptography;
using System.Net;

public class Utilidades
{
    private static SortedList<String, String> _gridFilterMenuText = null;
    internal enum CheckSession { Ok, Logout, Back }

    internal interface PaginaInterface
    {
        HttpContext Context { get; }
        HttpSessionState Session { get; }
        HttpRequest Request { get; }
        HttpResponse Response { get; }
        String QueryFormat { get; }
        Boolean CanGoBack { get; }
        void RegisterStartupScript(String scriptName, String script);
    }

    internal class PaginaController
    {
        private NameValueCollection _parameters = ConfigurationManager.AppSettings;
        internal string modulo = "";
        private PaginaInterface Pagina;
        internal GrupoLis.Login.Login _logApp = null;

        internal void conectar()
        {
            if (_logApp == null)
                _logApp = new GrupoLis.Login.Login();

            if (!_logApp.conectado)
            {
                _logApp.tipoConexion = TipoConexion.SQL_SERVER;
                _logApp.SERVIDOR = _parameters.Get("Servidor");
                _logApp.DATABASE = _parameters.Get("Database");
                _logApp.USER = _parameters.Get("User");
                _logApp.PASSWD = _parameters.Get("Password");
                _logApp.conectar();
            }
        }

        internal void desconectar()
        {
            if (_logApp != null && _logApp.conectado)
                _logApp.desconectar();
        }


        internal string parameterForm(string name, string startup)
        {
            if (Pagina.Request.Form.Get(name) != null && Pagina.Request.Form.Get(name).Length > 0)
                if (!Regex.IsMatch(Pagina.Request.Form.Get(name), Pagina.QueryFormat))
                    throw new ArgumentException("Invalid name parameter");
            return (null != Pagina.Request.Form.Get(name)) ? Pagina.Request.Form.Get(name) : startup;
        }

        internal string parameterQueryString(string name, string startup)
        {
            if (Pagina.Request.QueryString.Get(name) != null && Pagina.Request.QueryString.Get(name).Length > 0)
                if (!Regex.IsMatch(Pagina.Request.QueryString.Get(name), Pagina.QueryFormat))
                    throw new ArgumentException("Invalid name parameter");
            return (null != Pagina.Request.QueryString.Get(name)) ? Pagina.Request.QueryString.Get(name) : startup;
        }


        internal PaginaController(PaginaInterface pagina)
        {
            Pagina = pagina;
        }

        internal CheckSession OnLoad(EventArgs e)
        {
            Pagina.Response.Buffer = true;
            Pagina.Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
            Pagina.Response.Expires = -1500;
            Pagina.Response.CacheControl = "no-cache";

            if (Pagina.Session["IdSession"] != null && Pagina.Session["IdSession"].ToString() == HttpContext.Current.Request.Cookies.Get("ASP.NET_SessionId").Value)
            {
                if (Pagina.Context.User.Identity.IsAuthenticated)
                {
                    if (findRoleAutorizado())
                        return CheckSession.Ok;
                    else
                        return CheckSession.Back;
                }
                else
                    return CheckSession.Logout;
            }

            return CheckSession.Logout;
        }

        internal void logout()
        {
            try
            {
                HttpCookie httpCook = Pagina.Context.Request.Cookies.Get(FormsAuthentication.FormsCookieName);
                FormsAuthenticationTicket formsAuthTicket = FormsAuthentication.Decrypt(httpCook.Value);
                Pagina.Context.Request.Cookies.Remove(FormsAuthentication.FormsCookieName);
                Pagina.Context.Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
                string strEncryptedTicket = FormsAuthentication.Encrypt(formsAuthTicket);
                httpCook = new HttpCookie(FormsAuthentication.FormsCookieName, strEncryptedTicket);
                httpCook.Expires = DateTime.Now;
                Pagina.Context.Response.Cookies.Add(httpCook);
            }
            catch (Exception)
            {
            }
            finally
            {
                Pagina.Session.Clear();
                ScriptManager.RegisterStartupScript((Control)Pagina, Pagina.GetType(), "Logout", "parent.top.location.href='LoginTracking.aspx';", true);
            }
        }

        internal bool findRoleAutorizado()
        {
            IPrincipal user = HttpContext.Current.User;
            conectar();
            ModulosBO modulos = new ModulosBO(_logApp);
            modulos.loadRolesModulo((modulo != "") ? modulo : "ninguno");
            for (int i = 0; i < modulos.TABLA.Rows.Count; i++)
            {
                if (user.IsInRole(modulos.DESCRIPCION))
                {
                    desconectar();
                    return true;
                }
                modulos.regSiguiente();
            }
            desconectar();
            return false;
        }

        internal void redirectTo(String url)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("parent.iframePrincipal.location.href='{0}';", url);
            ScriptManager.RegisterStartupScript((Control)Pagina, Pagina.GetType(), "redirectTo", sb.ToString(), true);
        }

        internal void redirectParentTo(String url)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("parent.top.location.href='{0}';", url);
            ScriptManager.RegisterStartupScript((Control)Pagina, Pagina.GetType(), "redirectParentTo", sb.ToString(), true);
        }

        internal void registrarMensaje(string mensaje)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("radalert('" + mensaje + "');");
            Pagina.RegisterStartupScript("mensaje", sb.ToString());
        }

        internal void registrarMensaje1(string mensaje)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("alert('" + mensaje + "');");
            Pagina.RegisterStartupScript("mensaje", sb.ToString());
        }

        internal void goBack()
        {
            if (!Pagina.CanGoBack) return;

            StringBuilder sb = new StringBuilder();
            //sb.Append("function goBack(sender, eventArgs) { window.history.back(1); }");
            //sb.Append("var myalert = radalert('No tiene acceso a esta etapa', 400, 200, 'Alerta');");
            //sb.Append("myalert.add_close(goBack);");
            sb.Append("alert('No tiene acceso a esta etapa');");
            if (Path.GetFileName(Pagina.Request.PhysicalPath) != "Inicio.aspx")
                //sb.Append("parent.right.location.href='Inicio.aspx';");
                sb.Append("window.history.back(1);");

            //sb.Append("radalert('No tiene acceso a esta etapa');");

            Pagina.RegisterStartupScript("redireccionar", sb.ToString());
        }

        internal NameValueCollection Parameters
        {
            get
            {
                if (_parameters == null)
                    _parameters = ConfigurationManager.AppSettings;

                return _parameters;
            }
        }


    }


    /// <summary>
    /// Descripción breve de PaginaBase
    /// </summary>
    public class PaginaBase : System.Web.UI.Page, PaginaInterface
    {
        #region Statics
        protected const String FormatoDatosSoporte = "<table style=\"width:50%; font-weight:bold; border-style:none; \"><tr><td style=\"width:30%\">Tramite:</td><td style=\"width:70%\">{0}</td></tr><tr><td style=\"width:30%\">Remitente:</td><td style=\"width:70%\">{1}</td></tr><tr><td style=\"width:30%\">Area:</td><td style=\"width:70%\">{2}</td></tr><tr><td style=\"width:30%\">Encargado:</td><td style=\"width:70%\">{3}</td></tr></table>";

        private String _skin = "Default";
        public static void SetGridFilterMenu(Telerik.Web.UI.GridFilterMenu filterMenu)
        {
            if (_gridFilterMenuText == null)
            {
                _gridFilterMenuText = new SortedList<String, String>();

                _gridFilterMenuText.Add("NOFILTER", "No Filtrar");
                _gridFilterMenuText.Add("CONTAINS", "Contiene");
                _gridFilterMenuText.Add("DOESNOTCONTAIN", "No Contiene");
                _gridFilterMenuText.Add("STARTSWITH", "Comienza con");
                _gridFilterMenuText.Add("ENDSWITH", "Termina con");
                _gridFilterMenuText.Add("EQUALTO", "Es Igual a");
                _gridFilterMenuText.Add("NOTEQUALTO", "No es Igual a");
                _gridFilterMenuText.Add("GREATERTHAN", "Es Mayor que");
                _gridFilterMenuText.Add("LESSTHAN", "Es Menor que");
                _gridFilterMenuText.Add("GREATERTHANOREQUALTO", "Es Mayor o Igual a");
                _gridFilterMenuText.Add("LESSTHANOREQUALTO", "Es Menor o Igual a");
                _gridFilterMenuText.Add("BETWEEN", "Esta Entre");
                _gridFilterMenuText.Add("NOTBETWEEN", "No Esta Entre");
                _gridFilterMenuText.Add("ISEMPTY", "Esta Vacio");
                _gridFilterMenuText.Add("NOTISEMPTY", "No Esta Vacio");
                _gridFilterMenuText.Add("ISNULL", "Es Nulo");
                _gridFilterMenuText.Add("NOTISNULL", "No Es Nulo");
                _gridFilterMenuText.Add("CUSTOM", "Personalizado");
            }

            foreach (Telerik.Web.UI.RadMenuItem item in filterMenu.Items)
            {
                String key = item.Text.ToUpper();

                if (_gridFilterMenuText.ContainsKey(key))
                    item.Text = _gridFilterMenuText[key];
            }
        }

        private Control FindControlRecursive(Control parent, Type controlType)
        {
            Control res = null;

            foreach (Control control in parent.Controls)
            {
                if (controlType.IsAssignableFrom(control.GetType()))
                    res = control;
                else
                    res = FindControlRecursive(control, controlType);

                if (res != null)
                    break;
            }

            return res;
        }

        public override void RegisterStartupScript(string key, string script)
        {
            Telerik.Web.UI.RadAjaxManager ajaxmgr = (Telerik.Web.UI.RadAjaxManager)FindControlRecursive(this, typeof(Telerik.Web.UI.RadAjaxManager));

            if (ajaxmgr != null)
                ajaxmgr.ResponseScripts.Add(script);
            else
                ScriptManager.RegisterStartupScript(this, this.GetType(), key, script, true);

            //base.RegisterStartupScript(key, String.Format("<script language='javascript'>{0}</script>", script));
        }

        public static String hashMD5(String str)
        {
            // First we need to convert the string into bytes, which
            // means using a text encoder.
            Encoder enc = System.Text.Encoding.Unicode.GetEncoder();

            // Create a buffer large enough to hold the string
            byte[] unicodeText = new byte[str.Length * 2];
            enc.GetBytes(str.ToCharArray(), 0, str.Length, unicodeText, 0, true);

            // Now that we have a byte array we can ask the CSP to hash it
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] result = md5.ComputeHash(unicodeText);

            // Build the final string by converting each byte
            // into hex and appending it to a StringBuilder
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                sb.Append(result[i].ToString("X2"));
            }

            // And return it
            return sb.ToString();
        }
        #endregion

        protected String GetTelerikResourceUrl(String path)
        {
            return ClientScript.GetWebResourceUrl(typeof(Telerik.Web.IControl), String.Format("Telerik.Web.UI.Skins.{0}.{1}", _skin, path));
        }

        protected string parameterQueryString(string name, string startup)
        {
            return _paginaController.parameterQueryString(name, startup);
        }

        protected string parameterForm(string name, string startup)
        {
            return _paginaController.parameterForm(name, startup);
        }

        protected void conectar()
        {
            _paginaController.conectar();
        }

        protected void desconectar()
        {
            _paginaController.desconectar();
        }

        internal PaginaController _paginaController;

        public PaginaBase()
            : base()
        {
            _paginaController = new PaginaController(this);
        }

        protected override void OnLoad(EventArgs e)
        {
            switch (_paginaController.OnLoad(e))
            {
                case CheckSession.Ok:
                    if (Session["Skin"] == null)
                    {
                        try
                        {
                            conectar();

                            UserSettingsBO bo = new UserSettingsBO(logApp);

                            bo.loadAllCampos(Int32.Parse(Session["IdUsuario"].ToString()));

                            Session["Skin"] = bo.SKIN;
                        }
                        catch { }
                        finally
                        {
                            desconectar();
                        }
                    }
                    _skin = Session["Skin"] != null ? Session["Skin"].ToString() : "Default";
                    base.OnLoad(e);
                    if (!IsPostBack)
                    {
                        setSkin(this);
                    }
                    break;

                case CheckSession.Logout:
                    logout();
                    break;

                case CheckSession.Back:
                    goBack();
                    if (!this.CanGoBack)
                    {
                        base.OnLoad(e);
                    }
                    break;
            }
        }

        private void SetRadGridSkin(Control control)
        {
            Telerik.Web.UI.RadGrid radgrid = (Telerik.Web.UI.RadGrid)control;
            radgrid.FilterMenu.Skin = _skin;
        }

        private void setSkin(Control parent)
        {
            foreach (Control control in parent.Controls)
            {
                if (control is ISkinnableControl)
                {
                    if (!(control is Telerik.Web.UI.RadMenu))
                    {
                        ISkinnableControl skinctrl = (ISkinnableControl)control;
                        skinctrl.Skin = _skin;
                    }
                    if (control is Telerik.Web.UI.RadGrid)
                        SetRadGridSkin(control);
                }

                if (control.HasControls())
                    setSkin(control);
            }
        }

        protected virtual void logout()
        {
            _paginaController.logout();
        }

        protected virtual bool findRole()
        {
            return _paginaController.findRoleAutorizado();
        }

        protected void redirectTo(String url)
        {
            _paginaController.redirectTo(url);
        }

        protected void redirectParentTo(String url)
        {
            _paginaController.redirectParentTo(url);
        }

        protected void goBack()
        {
            _paginaController.goBack();
        }

        protected void registrarMensaje(string mensaje)
        {
            _paginaController.registrarMensaje(mensaje);
        }

        protected void registrarMensaje1(string mensaje)
        {
            _paginaController.registrarMensaje1(mensaje);
        }

        protected NameValueCollection mParamethers
        {
            get
            {
                return _paginaController.Parameters;
            }
        }

        protected string MODULO
        {
            set
            {
                _paginaController.modulo = value;
            }
        }

        protected string Skin { get { return _skin; } }

        protected bool tienePermiso(string permiso)
        {
            bool resp = false;
            try
            {
                conectar();
                ModulosBO modulos = new ModulosBO(logApp);
                modulos.loadPermisosModuloUsuario(_paginaController.modulo, Session["IdUsuario"].ToString());
                for (int i = 0; i < modulos.TABLA.Rows.Count; i++)
                {
                    if (permiso.ToUpper() == modulos.TABLA.Rows[i]["Descripcion"].ToString().ToUpper())
                    {
                        resp = true;
                        break;
                    }
                }
            }
            catch
            {
            }
            finally
            {
                desconectar();
            }
            return resp;
        }

        protected void sendMail(string to, string subject, string body)
        {
            try
            {
                ServicePointManager.ServerCertificateValidationCallback += (o, c, ch, er) => true;
                StringBuilder bodytmp = new StringBuilder();

                MailMessage mailmsg = new MailMessage();
                mailmsg.From = new MailAddress("soportegef@grupovesta.com", "Vesta");
                mailmsg.To.Add(new MailAddress(to.Trim()));
                mailmsg.Subject = subject;

                bodytmp.Append("<html xmlns=\"http://www.w3.org/1999/xhtml\"> <head> <title>" + subject + "</title> </head> ");
                bodytmp.Append("<body> ");

                bodytmp.Append(body);

                bodytmp.Append("</body> </html>");

                AlternateView avHTML = AlternateView.CreateAlternateViewFromString(bodytmp.ToString(), null, MediaTypeNames.Text.Html);

                mailmsg.AlternateViews.Add(avHTML);

                StreamWriter sw = new StreamWriter(new MemoryStream(), Encoding.UTF8);
                sw.Write(body.ToString());
                sw.Flush();

                sw.BaseStream.Position = 0;
                //mailmsg.Attachments.Add(new Attachment(sw.BaseStream, "Template.html", MediaTypeNames.Text.Html));
                SmtpClient smtpClient = new SmtpClient("192.168.205.252");
                smtpClient.Port = 587;
                smtpClient.EnableSsl = true;
                smtpClient.Send(mailmsg);

                sw.Close();
            }
            catch (Exception)
            { }
        }

        protected void sendMailConParametros(string para, string subject, String body, string Copias, string nombreUsuario, string fileName, string user, string pass, string extencion)
        {
            try
            {
                // SmtpClient SmtpServer = new SmtpClient("mail.grupovesta.com");
                ServicePointManager.ServerCertificateValidationCallback += (o, c, ch, er) => true;
                StringBuilder bodytmp = new StringBuilder();
                //string bodytmp = @"<html><body><img src=""cid:FirmaJuandeDios""></body></html>";
                //string firma = @"<img src="+ "cid:" + to + ">";
                MailMessage mailmsg = new MailMessage();
                mailmsg.From = new MailAddress("soportevesta@grupovesta.net", user);
                mailmsg.To.Add(new MailAddress(para));
                mailmsg.CC.Add(Copias);
                //mailmsg.Bcc.Add(new MailAddress(mailFrom, nombreUsuario));                
                mailmsg.Subject = subject;
                mailmsg.Attachments.Add(new Attachment(@"J:\Files1\Aduanas\" + fileName + extencion));

                //bodytmp.Append("<html xmlns=\"http://www.w3.org/1999/xhtml\"> <head> <title>" + subject + "</title> </head> ");
                bodytmp.Append("<html> <head> <title>" + subject + "</title> </head> ");
                bodytmp.Append("<body> ");
                bodytmp.Append("<table>");
                bodytmp.Append("<tr>");
                bodytmp.Append("<td></td>");
                bodytmp.Append("<td></td>");
                bodytmp.Append("<td></td>");
                bodytmp.Append("</tr>");
                bodytmp.Append("<tr>");
                bodytmp.Append("<td>" + body + "</td>");
                bodytmp.Append("<td></td>");
                bodytmp.Append("<td></td>");
                bodytmp.Append("</tr>");
                bodytmp.Append("<tr>");
                bodytmp.Append("<td> " + "<img src=J:\\Files1\\Aduanas\\FirmaVesta.jpg</td>");
                bodytmp.Append("<td></td>");
                bodytmp.Append("<td></td>");
                bodytmp.Append("</tr>");
                bodytmp.Append("</table>");
                bodytmp.Append("</body>");
                bodytmp.Append("</html>");

                AlternateView avHTML = AlternateView.CreateAlternateViewFromString(bodytmp.ToString(), null, MediaTypeNames.Text.Html);

                //CODIGO PARA AGREGAR IMAGEN AL CUERPO DEL CORREO
                //LinkedResource yourPictureRes = new LinkedResource(@"J:\Files1\Aduanas\FirmaVesta.jpg", MediaTypeNames.Image.Jpeg);
                //yourPictureRes.ContentId = mailFrom;
                //avHTML.LinkedResources.Add(yourPictureRes);
                //CODIGO PARA AGREGAR IMAGEN AL CUERPO DEL CORREO

                mailmsg.AlternateViews.Add(avHTML);

                mailmsg.IsBodyHtml = true;

                StreamWriter sw = new StreamWriter(new MemoryStream(), Encoding.UTF8);
                sw.Write(body.ToString());
                sw.Flush();

                sw.BaseStream.Position = 0;
                //mailmsg.Attachments.Add(new Attachment(sw.BaseStream, "Template.html", MediaTypeNames.Text.Html));

                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com");
                smtpClient.Port = 587;
                smtpClient.EnableSsl = true;
               // smtpClient.Credentials = new System.Net.NetworkCredential(user, pass);
                smtpClient.Credentials = new System.Net.NetworkCredential("soportevesta@grupovesta.net", "Ve$ta123%15");
                smtpClient.Send(mailmsg);

                sw.Close();
            }
            catch (Exception ex)
            {
                //LogError(ex.InnerException + ex.Message, "sendMailConParametros", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), Session["IdUsuario"].ToString(), "Utilidades.cs");
            }
        }
        protected void sendMailConParametrosSinArchivo(string para, string subject, String body, string Copias, string nombreUsuario, string user, string pass)
        {
            #region temp
            try
            {
                ServicePointManager.ServerCertificateValidationCallback += (o, c, ch, er) => true;
                StringBuilder bodytmp = new StringBuilder();
                MailMessage mailmsg = new MailMessage();
                mailmsg.From = new MailAddress("trackingaduanas@grupovesta.com", user);
                //mailmsg.From = new MailAddress("yzuniga@grupovesta.net", "yzuniga@grupovesta.net");
                mailmsg.To.Add(new MailAddress(para));
                mailmsg.CC.Add(Copias);
                mailmsg.Subject = subject;

                //bodytmp.Append("<html xmlns=\"http://www.w3.org/1999/xhtml\"> <head> <title>" + subject + "</title> </head> ");
                bodytmp.Append("<html> <head> <title>" + subject + "</title> </head> ");
                bodytmp.Append("<body> ");
                bodytmp.Append("<table>");
                bodytmp.Append("<tr>");
                bodytmp.Append("<td></td>");
                bodytmp.Append("<td></td>");
                bodytmp.Append("<td></td>");
                bodytmp.Append("</tr>");
                bodytmp.Append("<tr>");
                bodytmp.Append("<td>" + body + "</td>");
                bodytmp.Append("<td></td>");
                bodytmp.Append("<td></td>");
                bodytmp.Append("</tr>");
                bodytmp.Append("<tr>");
                bodytmp.Append("<td></td>");
                bodytmp.Append("<td></td>");
                bodytmp.Append("</tr>");
                bodytmp.Append("</table>");
                bodytmp.Append("</body>");
                bodytmp.Append("</html>");

                AlternateView avHTML = AlternateView.CreateAlternateViewFromString(bodytmp.ToString(), null, MediaTypeNames.Text.Html);

                mailmsg.AlternateViews.Add(avHTML);

                mailmsg.IsBodyHtml = true;

                StreamWriter sw = new StreamWriter(new MemoryStream(), Encoding.UTF8);
                sw.Write(body.ToString());
                sw.Flush();

                sw.BaseStream.Position = 0;

                SmtpClient smtpClient = new SmtpClient("192.168.205.252");
                smtpClient.Port = 587;
                smtpClient.EnableSsl = true;
                smtpClient.Credentials = new System.Net.NetworkCredential(user, pass);

                smtpClient.Send(mailmsg);

                sw.Close();
            }
            catch (Exception ex)
            {
                //LogError(ex.InnerException + ex.Message, "sendMailConParametros", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), Session["IdUsuario"].ToString(), "Utilidades.cs");
            }
            #endregion
        }

        protected void llenarBitacora(string accion, string idusuario, string idDocumento)
        {
            try
            {
                conectar();
                BitacoraBO lErr = new BitacoraBO(logApp);
                lErr.loadBitacora("-1");
                lErr.newLine();
                lErr.IDUSUARIO = idusuario;
                lErr.ACCION = accion;
                lErr.IDDOCUMENTO = idDocumento;
                lErr.FECHA = DateTime.Now.ToString();
                lErr.commitLine();
                lErr.actualizar();
            }
            catch { }
        }

        protected void logError(string error, string idusuario, string Modulo)
        {
            try
            {
                conectar();
                LogErroresBO lErr = new LogErroresBO(logApp);
                lErr.loadError("-1");
                lErr.newLine();
                lErr.ERROR = error;
                lErr.IDUSUARIO = idusuario;
                lErr.MODULO = Modulo;
                lErr.FECHA = DateTime.Now.ToString();
                lErr.commitLine();
                lErr.actualizar();
            }
            catch
            { }
        }

        public virtual String QueryFormat { get { return @"^[\w-=, ]{1,300}$"; } }

        public new HttpContext Context { get { return base.Context; } }

        protected GrupoLis.Login.Login logApp { get { return _paginaController._logApp; } }

        public virtual Boolean CanGoBack { get { return true; } }
    }


    public class PaginaMasterBase : System.Web.UI.MasterPage, PaginaInterface
    {
        internal PaginaController _paginaController;
        private String _skin = "Default";

        protected string parameterQueryString(string name, string startup)
        {
            return _paginaController.parameterQueryString(name, startup);
        }

        protected string parameterForm(string name, string startup)
        {
            return _paginaController.parameterForm(name, startup);
        }

        protected void conectar()
        {
            _paginaController.conectar();
        }

        protected void desconectar()
        {
            _paginaController.desconectar();
        }

        public PaginaMasterBase()
            : base()
        {
            _paginaController = new PaginaController(this);
        }

        protected override void OnLoad(EventArgs e)
        {
            switch (_paginaController.OnLoad(e))
            {
                case CheckSession.Ok:
                    if (Session["Skin"] == null)
                    {
                        try
                        {
                            conectar();

                            UserSettingsBO bo = new UserSettingsBO(logApp);

                            bo.loadAllCampos(Int32.Parse(Session["IdUsuario"].ToString()));

                            Session["Skin"] = bo.SKIN;
                        }
                        catch { }
                        finally
                        {
                            desconectar();
                        }
                    }
                    _skin = Session["Skin"] != null ? Session["Skin"].ToString() : "Default";
                    base.OnLoad(e);
                    if (!IsPostBack)
                    {
                        setSkin(this);
                    }
                    break;

                case CheckSession.Logout:
                    logout();
                    break;

                case CheckSession.Back:
                    goBack();
                    if (!this.CanGoBack)
                    {
                        base.OnLoad(e);
                    }
                    break;
            }
        }

        private void SetRadGridSkin(Control control)
        {
            Telerik.Web.UI.RadGrid radgrid = (Telerik.Web.UI.RadGrid)control;
            radgrid.FilterMenu.Skin = _skin;
        }

        private void setSkin(Control parent)
        {
            foreach (Control control in parent.Controls)
            {
                if (control is ISkinnableControl)
                {
                    ISkinnableControl skinctrl = (ISkinnableControl)control;
                    skinctrl.Skin = _skin;

                    if (control is Telerik.Web.UI.RadGrid)
                        SetRadGridSkin(control);
                }

                if (control.HasControls())
                    setSkin(control);
            }
        }

        protected virtual void logout()
        {
            _paginaController.logout();
        }

        protected virtual bool findRole()
        {
            return _paginaController.findRoleAutorizado();
        }

        protected void redirectTo(String url)
        {
            _paginaController.redirectTo(url);
        }

        protected void redirectParentTo(String url)
        {
            _paginaController.redirectParentTo(url);
        }

        protected void goBack()
        {
            _paginaController.goBack();
        }

        protected void registrarMensaje(string mensaje)
        {
            _paginaController.registrarMensaje(mensaje);
        }

        public void RegisterStartupScript(String scriptName, String script)
        {
            Page.RegisterStartupScript(scriptName, script);
        }

        protected NameValueCollection mParamethers
        {
            get
            {
                return _paginaController.Parameters;
            }
        }

        protected string MODULO
        {
            set
            {
                _paginaController.modulo = value;
            }
        }

        public virtual String QueryFormat { get { return @"^[\w-=, ]{1,70}$"; } }

        public new HttpContext Context { get { return base.Context; } }

        protected GrupoLis.Login.Login logApp { get { return _paginaController._logApp; } }

        public virtual Boolean CanGoBack { get { return true; } }
    }

}
