﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GridEspeciesFiscales
/// </summary>
public class GridEspeciesFiscales
{

    public decimal Item { get; set; }
    public string Serie { get; set; }
    public string Aduana { get; set; }
    public string Especie { get; set; }
    public string Estado { get; set; }
    public DateTime? Fecha { get; set; }
    public string Usuario { get; set; }
    public string Observacion { get; set; }

	public GridEspeciesFiscales()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}