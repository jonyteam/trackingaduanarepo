﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for MovimientosEspeciesFiscalesVm
/// </summary>
public class MovimientosEspeciesFiscalesVm
{

    public decimal Item { get; set; }
    public string IdMovimiento { get; set; }
    public string IdEspecieFiscal { get; set; }
    public string CodPais { get; set; }
    public string CodAduana { get; set; }
    public string RangoInicial { get; set; }
    public string RangoFinal { get; set; }
    public int? Cantidad { get; set; }
    public DateTime? Fecha { get; set; }
    public string CodTipoMovimiento { get; set; }
    public decimal? IdUsuario { get; set; }
    public string CodEstado { get; set; }
    public string IdCompra { get; set; }

	public MovimientosEspeciesFiscalesVm()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}