﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GridInstruccionesEspecies
/// </summary>
public class GridInstruccionesEspecies
{

    public decimal Item { get; set; }
    public string Instruccion { get; set; }
    public string Serie { get; set; }
    public string Usuario { get; set; }
    public string Estado { get; set; }
	public GridInstruccionesEspecies()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}