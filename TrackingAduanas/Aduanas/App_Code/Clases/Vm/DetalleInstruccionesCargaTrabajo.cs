﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DetalleInstruccionesCargaTrabajo
/// </summary>
public class DetalleInstruccionesCargaTrabajo
{
	public DetalleInstruccionesCargaTrabajo()
	{
		
	}

    public string Cliente { set; get; }
    public string Instruccion { set; get; }
    public string Fecha { set; get; }
    public string Dia { set; get; }
    public string Regimen { set; get; }
    public string Producto { set; get; }
    public string CanalSelectividad { set; get; }
}