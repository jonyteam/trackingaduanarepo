﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AnulacionEspeciesFiscalesRepository
/// </summary>
public class AnulacionEspeciesFiscalesRepository
{

    private readonly AduanasDataContext _aduanasDc;
	public AnulacionEspeciesFiscalesRepository(AduanasDataContext aduanasDc)
	{
	    _aduanasDc = aduanasDc;
	}

    public AnulacionEspeciesFiscalesRepository()
    {
        _aduanasDc = new AduanasDataContext();
    }

    public IEnumerable<GridEspeciesFiscales> GetAnulacionEspeciesBySerie(string serie, string idSerie)
    {

        var respuesta = new List<GridEspeciesFiscales>();

        var query = _aduanasDc.AnulacionesEspeciesFiscales
            .Join(_aduanasDc.Aduanas, x => x.CodigoAduana, a => a.CodigoAduana, (x, a) => new {x = x, a = a})
            .Join(_aduanasDc.Usuarios, x => x.x.IdUsuario, u => u.IdUsuario, (x, u) => new {x = x, u = u})
            .Where(w => w.x.x.Serie.Contains(serie) && w.x.x.IdEspecieFiscal == idSerie).
            OrderByDescending(o => o.x.x.Fecha)
            .ToList()
            .FirstOrDefault();

        if (query != null)
            respuesta.Add(new GridEspeciesFiscales()
            {
                Item = query.x.x.Item,
                Serie = query.x.x.Serie,
                Especie = query.x.x.IdEspecieFiscal,
                Aduana = query.x.a.NombreAduana,
                Usuario = query.u.Nombre + " " + query.u.Apellido,
                Estado = Convert.ToString(query.x.x.Carga),
                Fecha = query.x.x.Fecha,
                Observacion = query.x.x.Observacion

            });

        return respuesta;
    } 
}