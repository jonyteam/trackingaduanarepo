﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;

/// <summary>
/// Summary description for MovimientosEspeciesFiscalesRepository
/// </summary>
public class MovimientosEspeciesFiscalesRepository
{

    private AduanasDataContext _aduanasDc;
	public MovimientosEspeciesFiscalesRepository( AduanasDataContext aduanasDc)
	{
	    _aduanasDc = aduanasDc;
	}

    public MovimientosEspeciesFiscalesRepository()
    {
        _aduanasDc = new AduanasDataContext();
    }

    public List<GridEspeciesFiscales> GetMovimientosEspeciesBySerie(string serie, string idSerie)
    {
        var respuesta = new List<GridEspeciesFiscales>();
        var query = _aduanasDc.MovimientosEspeciesFiscales
            .Join(_aduanasDc.Aduanas, x => x.CodigoAduana, a => a.CodigoAduana, (x, a) => new {x = x, a = a})
            .Join(_aduanasDc.Codigos, x => x.x.CodEstado, c => c.Codigo1, (x, c) => new {x = x, c = c})
            .Join(_aduanasDc.Usuarios, x => x.x.x.IdUsuario, u => u.IdUsuario, (x, u) => new {x = x, u = u})
            .Where(
                w =>
                    w.x.x.x.RangoInicial.Contains(serie) && w.x.x.x.IdEspecieFiscal == idSerie &&
                    Convert.ToInt32(w.x.x.x.CodEstado) <= 5)
            .OrderByDescending(o => o.x.x.x.Fecha)
            .Select(s =>  new
            {
                s.x.x.x.Item,
                s.x.x.x.RangoInicial,
                s.x.x.x.IdEspecieFiscal,
                s.x.x.a.NombreAduana,
                s.x.x.x.CodEstado,
                Usuario = s.u.Nombre +" "+ s.u.Apellido
            })
            .FirstOrDefault();

        if (query != null)
            respuesta.Add(new GridEspeciesFiscales()
            {
                Item = query.Item,
                Serie = query.RangoInicial,
                Especie = query.IdEspecieFiscal,
                Aduana = query.NombreAduana,
                Estado = query.CodEstado,
                Observacion = "",
                Usuario = query.Usuario
            });
        return respuesta;
    }

    public IEnumerable<GridEspeciesFiscales> GetMovimientosEspeciePerdida(string serie, string idSerie)
    {
        var respuesta = new List<GridEspeciesFiscales>();

        var query = _aduanasDc.MovimientosEspeciesFiscales
            .Join(_aduanasDc.Aduanas, x => x.CodigoAduana, a => a.CodigoAduana, (x, a) => new {x = x, a = a})
            .Join(_aduanasDc.Usuarios, x => x.x.IdUsuario, u => u.IdUsuario, (x, u) => new {x = x, u = u})
            .Where(
                w =>
                    w.x.x.RangoInicial.Contains(serie) && w.x.x.IdEspecieFiscal == idSerie &&
                    Convert.ToInt32(w.x.x.CodEstado) == 100)
            .OrderByDescending(o => o.x.x.Fecha)
            .Select(s => new
            {
                Item = s.x.x.Item,
                Serie = s.x.x.RangoInicial,
                Aduanas = s.x.a.NombreAduana,
                Estado = s.x.x.CodEstado,
                Especie = s.x.x.IdEspecieFiscal,
                Observacion = "",
                s.x.x.Fecha,
                Usuario = s.u.Nombre + " "+ s.u.Apellido
            })
            .FirstOrDefault();

        if (query != null)
            respuesta.Add(new GridEspeciesFiscales()
            {
                Item = query.Item,
                Serie = query.Serie,
                Especie = query.Especie,
                Aduana = query.Aduanas,
                Estado = "Perdida",
                Fecha = query.Fecha,
                Usuario = query.Usuario,
                Observacion = query.Observacion
            });
        return respuesta;
    }

    public MovimientosEspeciesFiscalesVm ResucitarEspecie(string serie, string idEspecie)
    {
        var fecha = DateTime.Now;

        var movimientoEspecie = _aduanasDc.MovimientosEspeciesFiscales
            .Where(w => w.RangoInicial.Contains(serie) && w.IdEspecieFiscal == idEspecie)
            .OrderByDescending(o => o.Fecha)
            .Select(s => new MovimientosEspeciesFiscalesVm()
            {
                Item = s.Item,
                IdMovimiento = s.IdMovimiento,
                IdEspecieFiscal = s.IdEspecieFiscal,
                CodPais = s.CodPais,
                CodAduana = s.CodigoAduana,
                RangoInicial = s.RangoInicial,
                RangoFinal = s.RangoInicial,
                Cantidad = 1,
                Fecha = fecha,
                CodTipoMovimiento = s.CodTipoMovimiento,
                CodEstado = "1",
                IdUsuario = s.IdUsuario,
                IdCompra = s.IdCompra
            })
            .FirstOrDefault();
        return movimientoEspecie;
    }

    public void InsertarRegistro(MovimientosEspeciesFiscalesVm mef)
    {
        var movimientoEspeciesFiscales = new MovimientosEspeciesFiscales()
        {
            Item = mef.Item,
            IdMovimiento = mef.IdMovimiento,
            IdEspecieFiscal = mef.IdEspecieFiscal,
            CodPais = mef.CodPais,
            CodigoAduana = mef.CodAduana,
            RangoInicial = mef.RangoInicial,
            RangoFinal = mef.RangoFinal,
            Cantidad = mef.Cantidad,
            Fecha = mef.Fecha,
            CodTipoMovimiento = mef.CodTipoMovimiento,
            CodEstado = mef.CodEstado,
            IdUsuario = mef.IdUsuario,
            IdCompra = mef.IdCompra
        };

        _aduanasDc.MovimientosEspeciesFiscales.InsertOnSubmit(movimientoEspeciesFiscales);
        _aduanasDc.SubmitChanges();
    }
   
}

