﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Linq;
using System.Web;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;
using OfficeOpenXml.FormulaParsing.Excel.Functions.DateTime;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using  System.Data;
using Toolkit.Core.Extensions;

/// <summary>
/// Summary description for ReporteGeneral
/// </summary>
public class ReporteGeneralRepository
{


    private AduanasDataContext _aduanasDC;
    string [] estadoFlujo = new string[] { "99", "H-6" };
	public ReporteGeneralRepository( AduanasDataContext aduanasDC)
	{

	    _aduanasDC = aduanasDC;
	}

    public ReporteGeneralRepository()
    {
        _aduanasDC = new AduanasDataContext();
    }

    public IEnumerable<Aduanas> GetAduanas(IEnumerable<string> aduana)
    {
        var aduanas = _aduanasDC.Aduanas
            .Where(w => w.CodEstado == "0" && aduana.Contains(w.CodigoAduana))
            .ToList();
        return aduanas;
    }

    public IEnumerable<Instrucciones> GetInstrucciones(IEnumerable<string> clientes)
    {
        var instrucciones = _aduanasDC.Instrucciones
            .Where(w => w.CodEstado != "100" && w.IdEstadoFlujo == "99" || w.IdEstadoFlujo == "H-6" && clientes.Contains(w.IdCliente))
            .ToList();
        return instrucciones;
    } 

    public System.Data.DataTable GetResumen(List<string> clientes, List<string> aduanas, DateTime? mesInicio, DateTime? mesFin )
    {

        System.Data.DataTable dt = new System.Data.DataTable();
        dt.Columns.Add("Key");
        dt.Columns.Add("Aduana");
        dt.Columns.Add("Exportacion");
        dt.Columns.Add("Importacion");
        dt.Columns.Add("Transito");

        var aduanasQuery = GetAduanas(aduanas);
            /*_aduanasDC.Aduanas.Where(w => w.CodEstado == "0" && w.CodPais == "H" 
                            && aduanas.Contains(w.CodigoAduana)).
                            ToList();*/

        /*var clientesQuery = _aduanasDC.Clientes.Where(w => w.Eliminado == '0' && w.CodigoPais == "H")
        .Select(s => s.CodigoSAP).ToList();*/


        var instrucciones = _aduanasDC.Instrucciones
        .Where(i => i.CodEstado != "100" && estadoFlujo.Contains(i.IdEstadoFlujo) && i.CodPaisHojaRuta == "H" &&
        aduanas.Contains(i.CodigoAduana) && clientes.Contains(i.IdCliente) &&
        (i.FechaFinalFlujo.Value >= mesInicio && i.FechaFinalFlujo.Value <= mesFin)) 
        .Select(s => new
        {
            s.IdInstruccion,
            s.CodigoAduana,
            s.CodRegimen
        })
        .ToList();

        var query = instrucciones
            .Join(aduanasQuery, i => i.CodigoAduana, a => a.CodigoAduana, (i, a) => new {i = i, a = a})
            .GroupBy(g => g.a.CodigoAduana)
            .OrderBy(o => o.Key)
            .Select(s => new
            {
                s.Key,
                Aduana = s.FirstOrDefault().a.NombreAduana,
                Exportacion =
                    s.Where(
                        w =>
                            w.i.CodRegimen.StartsWith("1") || w.i.CodRegimen.StartsWith("2") ||
                            w.i.CodRegimen.StartsWith("3")).Count(),
                Importacion =
                    s.Where(
                        w =>
                            w.i.CodRegimen.StartsWith("4") || w.i.CodRegimen.StartsWith("5") ||
                            w.i.CodRegimen.StartsWith("6") || w.i.CodRegimen.StartsWith("7")).Count(),
                Transito = s.Where(w => w.i.CodRegimen.StartsWith("8")).Count(),


                /*Total =
                    s.Where(
                        w =>
                            w.i.CodRegimen.StartsWith("1") || w.i.CodRegimen.StartsWith("2") ||
                            w.i.CodRegimen.StartsWith("3")).Count() +
                    s.Where(
                        w =>
                            w.i.CodRegimen.StartsWith("4") || w.i.CodRegimen.StartsWith("5") ||
                            w.i.CodRegimen.StartsWith("6") || w.i.CodRegimen.StartsWith("7")).Count() +
                    s.Where(w => w.i.CodRegimen.StartsWith("8")).Count()*/



            }).ToList();

        System.Data.DataRow dr;
        
        for (int i = 0; i < query.Count; i++)
        {
            dr = dt.NewRow();
            dr["Key"] = query[i].Key;
            dr["Aduana"] = query[i].Aduana;
            dr["Exportacion"] = query[i].Exportacion;
            dr["Importacion"] = query[i].Importacion;
            dr["Transito"] = query[i].Transito;

            dt.Rows.Add(dr);
        }


        dr = dt.NewRow();        
        dr["Key"] = "T";
        dr["Aduana"] = "Total";
        dr["Exportacion"] = query.Sum(s => s.Exportacion);
        dr["Importacion"] = query.Sum(s => s.Importacion);
        dr["Transito"] = query.Sum(s => s.Transito);
        dt.Rows.Add(dr);
        
        return dt;

    }

    public IEnumerable<object> Reporte()
    {
        var aduanaAereasTerrestres = new List<string>() { "014", "017", "020", "018", "021", "019", "015", "022" };
	var startDate = new DateTime(2015,05,02);
	var endDate = new DateTime(2016,07,10);
	int export = 0;
	int tiempos = 1;
	int gastos = 1;
	
	var estadoFlujo = new string[] {"99", "H-6"};
	var aduanasQuery = _aduanasDC.Aduanas.Where(w => w.CodEstado == "0" && w.CodPais == "H" && aduanaAereasTerrestres.Contains(w.CodigoAduana))
	.ToList();

	var clientesQuery = _aduanasDC.Clientes.Where(w => w.Eliminado == 0 && w.CodigoPais == "H").ToList();

	var instrucciones = _aduanasDC.Instrucciones
			.Where(i => i.CodEstado != "100" && estadoFlujo.Contains(i.IdEstadoFlujo) && i.CodPaisHojaRuta == "H" && 
			aduanaAereasTerrestres.Contains(i.CodigoAduana)  && 
			(i.FechaFinalFlujo.Value >= startDate && i.FechaFinalFlujo.Value <= endDate))
			.ToList();
	if (export == 0)
	{
		instrucciones.Where(w => w.CodRegimen.StartsWith("1") || w.CodRegimen.StartsWith("2") || w.CodRegimen.StartsWith("3")).ToList();
	}
	else if (export == 1)
	{
		instrucciones.Where(w => w.CodRegimen.StartsWith("4") || w.CodRegimen.StartsWith("5") || w.CodRegimen.StartsWith("6")
							|| w.CodRegimen.StartsWith("7")).ToList();
	}
	else if (export == 3)
	{
	    instrucciones.Where(w => w.CodRegimen.StartsWith("8"));
	}
	
	var codigosRegimen = _aduanasDC.Codigos
		.Where(w => w.Categoria == "REGIMENHONDURAS" && w.Eliminado == 0).ToList();

        var CodigosPaises = _aduanasDC.Codigos
            .Where(w => w.Categoria == "PAISES" && w.Eliminado == '0');

	var docEmbarque = _aduanasDC.Codigos
		.Where(w => w.Eliminado == 0 && w.Categoria == "DOCUMENTOEMBARQUE").ToList();

	var query = instrucciones
			.Join(aduanasQuery, i => i.CodigoAduana, a => a.CodigoAduana, (i, a) => new { i = i, a = a })
			.Join(CodigosPaises, i => i.i.CodPaisOrigen, cp => cp.Codigo1, (i, cp) => new { i = i, cp = cp })
			.Join(docEmbarque, i => i.i.i.CodDocumentoEmbarque, de => de.Codigo1, (i, de) => new { i = i, de = de })
			.Join(clientesQuery, i => i.i.i.i.IdCliente, cl => cl.CodigoSAP, (i, cl) => new { i = i, cl = cl })
			.Join(codigosRegimen, i => i.i.i.i.i.CodRegimen, cr => cr.Codigo1, (i, cr) => new { i = i, cr = cr })
			.Where(w => w.i.i.i.i.i.CodEstado != "100" && (w.i.i.i.i.i.IdEstadoFlujo == "99" || w.i.i.i.i.i.IdEstadoFlujo == "H-6")
					&& (w.i.i.i.i.i.FechaFinalFlujo.Value >= startDate && w.i.i.i.i.i.FechaFinalFlujo.Value <= endDate))
			.Select(s => new
			{
				IdInstruccion = s.i.i.i.i.i.IdInstruccion,
                Cliente = s.i.cl.Nombre,
				Proveedor = s.i.i.i.i.i.Proveedor,
				Factura = s.i.i.i.i.i.NumeroFactura,
				Regimen = s.cr.Descripcion,
				Correlativo = s.i.i.i.i.i.NoCorrelativo,
				PaisOrigen = s.i.i.i.cp.Descripcion,
				ValirCIF = s.i.i.i.i.i.ValorCIF,
				Documento = s.i.i.de.Descripcion
			}).ToList();

        //TiemposFLujo(query);

        return null;
    }

    //public IEnumerable<object> TiemposFLujo(IEnumerable<object> instrucciones )
    //{
    //    var tiempos = instrucciones
    //        .GroupJoin(_aduanasDC.TiemposFlujoCarga.Where(w => w.Eliminado == false && w.IdEstado == "14"), i => i)
    //}
}