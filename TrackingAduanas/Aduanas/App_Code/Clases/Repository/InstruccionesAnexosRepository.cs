﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EntityFramework.Extensions;
using Toolkit.Core.Extensions;

/// <summary>
/// Summary description for InstruccionesAnexosRepository
/// </summary>
public class InstruccionesAnexosRepository
{

    private readonly AduanasDataContext _aduanasDC;
	public InstruccionesAnexosRepository(AduanasDataContext aduanasDC)
	{
	    _aduanasDC = aduanasDC;
	}

    public InstruccionesAnexosRepository()
    {
        _aduanasDC = new AduanasDataContext();
    }

    public InstruccionesAnexo ExisteInstruccion(string idInstruccion)
    {
        var query = _aduanasDC.InstruccionesAnexos.FirstOrDefault(x => x.IdInstruccion == idInstruccion);
        return query;
    }

    public void InsertarFacturaFecha(InstruccionesAnexo instrucciones)
    {
        _aduanasDC.InstruccionesAnexos.InsertOnSubmit(instrucciones);
        _aduanasDC.SubmitChanges();
    }

    public void Actualizar(InstruccionesAnexo ia)
    {
        var resp = _aduanasDC.InstruccionesAnexos.FirstOrDefault(
            w => w.IdInstruccion == ia.IdInstruccion && w.Eliminado == false);
        if (resp == null)
            return;

        resp.Factura = ia.Factura;
        resp.FechaFactura = ia.FechaFactura;
        _aduanasDC.SubmitChanges();
    }

    public Instrucciones LoadInstruccion(string idInstruccion)
    {
        var query = _aduanasDC.Instrucciones.FirstOrDefault(w => w.IdInstruccion == idInstruccion && w.CodEstado != "100");
        return query;
    }

    public IEnumerable<object> LlenarGrid(string fechaInicio, string fechaFin, string cliente)
    {

        var clientes = new List<string>() {cliente, "8000194", "8000339"};

        var query = _aduanasDC.Instrucciones
            .Join(_aduanasDC.RegimenesHonduras, x => x.CodRegimen, r => r.Codigo, (x, r) => new {x = x, r = r})
            .Join(_aduanasDC.Clientes, x => x.x.IdCliente, c => c.CodigoSAP,
                (x, c) => new {x = x, c = c})
            .Where(w => w.x.x.CodPaisHojaRuta == "H" && w.x.x.CodEstado != "100" && w.x.x.IdEstadoFlujo == "99" && clientes.Contains(w.x.x.IdCliente)
                        && w.x.x.Fecha >= fechaInicio.ToDateTime() && w.x.x.Fecha <= fechaFin.ToDateTime())
            .Select(s => new
            {
                IdInstruccion = s.x.x.IdInstruccion,
                Regimen = s.x.r.Descripcion,
                Cliente = s.c.Nombre,
                Producto = s.x.x.Producto
            }).ToList();

        return  query;
    }

    public void GuardarFacturaPedido(InstruccionesAnexo ia, bool existe)
    {
        //var resp = _aduanasDC.InstruccionesAnexos.FirstOrDefault(w => w.IdInstruccion == ia.IdInstruccion);
        
        if (!existe)
        {
            _aduanasDC.InstruccionesAnexos.InsertOnSubmit(ia);
        }
        else
        {
            var anexo = new InstruccionesAnexo();
            anexo.FechaFactura = ia.FechaFactura;
            anexo.PedidoCliente = ia.PedidoCliente;
            //anexo.NotasRembolso = ia.NotasRembolso;
        }
        _aduanasDC.SubmitChanges();
    }
}