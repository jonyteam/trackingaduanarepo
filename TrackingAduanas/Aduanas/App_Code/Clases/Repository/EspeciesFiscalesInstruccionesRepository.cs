﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for EspeciesFiscalesInstruccionesRepository
/// </summary>
public class EspeciesFiscalesInstruccionesRepository
{

    private readonly AduanasDataContext _aduanasDc;
	public EspeciesFiscalesInstruccionesRepository( AduanasDataContext aduanasDc)
	{
	    _aduanasDc = aduanasDc;
	}

    public EspeciesFiscalesInstruccionesRepository()
    {
        _aduanasDc = new AduanasDataContext();
    }

    public IEnumerable<GridInstruccionesEspecies> GetEspeciesInstruccionesBySerie(string serie, string idEspecie)
    {

        var query = _aduanasDc.EspeciesFiscalesInstrucciones
            .Join(_aduanasDc.Usuarios, x => x.IdUsuario, u => u.IdUsuario, (x, u) => new {x = x, u = u})
            .Where(w => w.x.Serie.Contains(serie) && w.x.CodEspecieFiscal == idEspecie && w.x.Eliminado == 0)
            .Select(s => new GridInstruccionesEspecies()
            {
                Item = s.x.Item,
                Instruccion = s.x.IdInstruccion,
                Serie = s.x.Serie,
                Usuario = s.u.Nombre + " " + s.u.Apellido,
                Estado = Convert.ToString(s.x.Eliminado)
            })
            .ToList();
        return query;
    }
}