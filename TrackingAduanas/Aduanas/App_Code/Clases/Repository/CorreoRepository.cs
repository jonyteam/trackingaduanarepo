﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.ServiceModel;
using System.Web;

/// <summary>
/// Summary description for CorreoRepository
/// </summary>
public class CorreoRepository
{

    private AduanasDataContext _aduanasDc;
	public CorreoRepository(AduanasDataContext _aduanasDc)
	{
	    _aduanasDc = _aduanasDc;
	}

    public CorreoRepository()
    {
        _aduanasDc = new AduanasDataContext();
    }

    public Correo GetCorreoById(int id)
    {

        return _aduanasDc.Correo.FirstOrDefault(c => c.Id == id);
    }
}