﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UsuarioRepository
/// </summary>
public class UsuarioRepository
{

    private AduanasDataContext _aduanasDc;
	public UsuarioRepository(AduanasDataContext aduanasDc)
	{
	    _aduanasDc = aduanasDc;
	}

    public UsuarioRepository()
    {
        _aduanasDc = new AduanasDataContext();
    }

    public Usuario GetUsuarioById(int idUsuario)
    {
        return _aduanasDc.Usuarios.FirstOrDefault(u => u.IdUsuario == idUsuario);
    }
}