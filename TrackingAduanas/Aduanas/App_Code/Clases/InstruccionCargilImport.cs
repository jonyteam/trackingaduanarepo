﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using Toolkit.Core.Extensions;

/// <summary>
/// Summary description for InstruccionCargilImport
/// </summary>
public class InstruccionCargilImport
{
    private List<string> _excel;
    private AduanasDataContext _aduanasDC;
    private bool import;


    public InstruccionCargilImport(List<string> excel)
    {
        this._excel = excel;
        _aduanasDC = new AduanasDataContext();
    }


    private string[] GenerarArregloPalabra(string arreglo)
    {
        var palabras = arreglo.Split(' ').Where(w => !string.IsNullOrEmpty(w)).ToArray();
        return palabras;
    }

    public string GetDeclaracion()
    {
        var palabras = GenerarArregloPalabra(_excel[0]);
        string declaracion = "";
        string palabra = "";

        for (var i = 0; i <= palabras.Length; i++)
        {
            palabra += palabras[i] + " ";

            if (palabra.Trim() == "Declaracion de Exportacion:" || palabra.Trim() == "Declaracion de Exportación:" ||
                palabra.Trim() == "Declaración de Exportación:")
            {
                declaracion = palabras[i + 1];
                declaracion += " " + palabras[i + 2];
                break;

            }
            else if (palabra.Trim() == "Declaracion de Importación:" || palabra.Trim() == "Declaracion de Importacion:" ||
                     palabra.Trim() == "Declaración de Importación:")
            {
                declaracion = palabras[i + 1];
                declaracion += " " + palabras[i + 2];
                import = true;
                break;
            }
            //
        }

        return declaracion;
    }

    public string GetDivision()
    {
        return "";
    }

    public string[] GetPaises()
    {
        string[] paises = new string[2];
        string palabra = "";

        int indice = GetIndice("Geografia");

        string[] palabras = GenerarArregloPalabra(_excel[indice]);

        bool limpiar = false;

        for (int i = 0; i < palabras.Length; i++)
        {
            palabra += palabras[i] + " ";
            if (palabra.Trim() == "Pais de Salida:" || palabra.Trim() == "País de Salida" ||
                palabra.Trim() == "Pais de Salida")
            {
                paises[0] = palabras[i + 1];
                limpiar = true;
                //i += 2;
            }
            else if (palabra.Trim() == "Pais de Destino:" || palabra.Trim() == "País de Destino:" ||
                     palabra.Trim() == "País de Destino" || palabra.Trim() == "Pais de Destino")
            {
                paises[1] = palabras[i + 1];
                break;
            }
            if (palabras[i] == "Pais" || palabras[i] == "País:" || palabras[i] == "País")
            {
                limpiar = false;
            }
            else if (limpiar)
            {
                palabra = "";
            }
        }

        return paises;
    }

    public string GetCodAduana()
    {

        string aduana = "";

        int indice = GetIndice("Transporte");

        string[] palabras = GenerarArregloPalabra(_excel[indice - 2]);
        string palabra = "";

        for (int i = 0; i < palabras.Length; i++)
        {
            palabra += palabras[i] + " ";
            if (palabra.Trim() == "Aduana de salida:" || palabra.Trim() == "Aduana de destino:")
            {
                aduana = palabras[i + 3];
                i += 3;
                if (i < palabras.Length - 1)
                {
                    aduana += " " + palabras[i + 1];
                }
                break;
            }
        }

        var codAduana = _aduanasDC.Aduanas.FirstOrDefault(f => f.CodEstado == "0" && f.NombreAduana == aduana.Trim() && f.CodPais == "H").CodigoAduana;

        return codAduana;
    }


    public string[] GetRegimenInconterm()
    {

        string palabra = "";
        int indice = GetIndice("Transporte");

        var palabras = GenerarArregloPalabra(_excel[indice]);
        string[] regimen_incoterm = new string[2];
        //bool limpiar = false;


        for (int i = 0; i < palabras.Length; i++)
        {
            palabra += palabras[i] + " ";

            if (palabra.Trim() == "Codigo Tipo Entrada:" || palabra.Trim() == "Código Tipo Entrada:" ||
                palabra.Trim() == "Código Tipo de Entrada:" || palabra.Trim() == "Codigo Tipo de Entrada:") //Regimen 
            {
                regimen_incoterm[0] = palabras[i + 1];
                //limpiar = true;

            }
            else if (palabra.Trim() == "Numero de Registro:" || palabra.Trim() == "Número de Registro:") //Correlativo
            {
                Array.Resize(ref regimen_incoterm, regimen_incoterm.Length + 1);
                regimen_incoterm[1] = palabras[i + 1];
                //limpiar = true;


            }
            else if (palabra.Trim() == "Incoterm:") //Incoterm
            {
                if (regimen_incoterm.Length > 2)
                {
                    regimen_incoterm[2] = palabras[i + 1];
                }
                else
                {
                    regimen_incoterm[1] = palabras[i + 1];
                }
            }
            palabra = LimpiarVariable(palabra);

        }

        return regimen_incoterm;
    }

    public string[] GetTransporte()
    {

        string palabra = "";

        int indice = GetIndice("Medio de Transporte en la Frontera");

        string[] palabras = GenerarArregloPalabra(_excel[indice]);
        string[] transporte = new string[2];
        bool limpiar = false;

        for (int i = 0; i < palabras.Length; i++)
        {
            palabra += palabras[i] + " ";

            if (palabra.Trim() == "Media de Transporte:" || palabra.Trim() == "Medio de Transporte:")
            {
                transporte[0] = palabras[i + 2];
                limpiar = true;
            }
            else if (palabra.Trim() == "Nombre del transport:")
            {
                transporte[1] = palabras[i + 1];
                transporte[1] += " " + palabras[i + 2];
                break;
            }
            if (palabras[i] == "Nombre")
                limpiar = false;
            else if (limpiar)
                palabra = "";
        }

        transporte[0] =
            _aduanasDC.Codigos.FirstOrDefault(f => f.Categoria == "TIPOTRANSPORTE" && (f.Descripcion.Contains(transporte[0].ToUpper())
                                                         || f.Descripcion.Contains(transporte[0].ToLower()))).Codigo1;

        return transporte;
    }

    public string[] Costos()
    {
        var costos = new string[2];
        decimal valorCIF = 0;
        var indice = GetIndice("Cabecera Logística Costo");

        for (var i = indice; i < _excel.Count; i ++)
        {
            var palabra = _excel[i];
            if (_excel[i] == "Valor de la mercancía" || _excel[i] == "Valor de la Mercancia" || _excel[i] == "Valor de la mercancia") //Valor FOB pos 1
            {
                costos[1] = _excel[i + 1];
                i += 2;
            }
            else if (palabra == "Costo de Transporte" || palabra == "Costo Transporte" || palabra == "Flete ( Agregar o Deducir )") //Flete pos 2
            {
                Array.Resize(ref costos, costos.Length + 1);
                costos[2] = _excel[i + 1];
                i += 2;
            }
            else if (_excel[i].Contains("Cargos por Seguro")) //Seguro pos 3
            {
                Array.Resize(ref costos, costos.Length + 1);
                costos[3] = _excel[i + 1];
                i += 2;
            }
            if (palabra == "Referencias")
                break;
            /*else if (_excel[i].Contains("Transporte y Seguro Subtotal")) //Valor CIF
            {
                Array.Resize(ref costos, costos.Length + 1);
                costos[3] = _excel[i + 1];
                break;
            }*/
        }

        for (int i = 1; i < costos.Length; i++)
        {
                valorCIF = valorCIF + decimal.Parse(costos[i]);
        }
        costos[0] = valorCIF.ToString();

        return costos;
    }

    public string ReferenciaCliente()
    {
        int indice = GetIndice("Referencias");
        string refCliente = "";

        for (int i = indice; i < _excel.Count; i++)
        {
            var palabra = _excel[i];

            if (palabra == "Sistema Logico")
            {
                refCliente = _excel[i + 2];
                break;
            }
        }

        return refCliente;
    }

    public string NumeroFactura()
    {

        var factura = "";

        int indice = GetIndice("Factura/Contrato");

        for (int i = indice; i < _excel.Count; i++)
        {
            var palabra = _excel[i];
            if (palabra == "Tipo")
            {
                factura = _excel[i + 1];
                break;
            }
        }

        return factura;
    }

    public decimal GetPeso()
    {
        int indice;
        indice = GetIndice("Detalles de Items");
        double items = 0;
        decimal valor = 0;
        const double convercion = 2.20462;
        for (int i = indice + 2; i < _excel.Count;)
        {
            var palabra = _excel[i];
            if (palabra == "Total")
            {
                items = _excel[i + 2].ToInt();
                var unidadMedida = _excel[i + 3];
                if (unidadMedida == "LB")
                {
                    valor = Convert.ToDecimal((items * convercion));
                    valor = Decimal.Round(valor, 5);
                }
                else if (unidadMedida == "KG")
                {
                    valor = Convert.ToDecimal(items);
                    break;
                }
            }
        }
        return valor;
    }

    public string GetDocTransporte()
    {
        int indice = GetIndice("Documento Existe");


        string[] docTransporte = new string[2];
        string palabra = "";
        palabra = _excel[indice];
        int count = 47;
        if (import)
        {
            if (palabra.Substring(0,1)  == "/" || palabra.Substring(0,1) == "-" || palabra.Substring(0, 1) == " ")
            {
                docTransporte[0] = _excel[51];
            }
            else
            {
                docTransporte[0] = _excel[50];
            }
        }
        else
        {
            for (int i = count; i < _excel.Count; i++)
            {
                palabra = _excel[i];
                if (palabra == "HDOC")  
                {
                    i += 5;
                }
            }
        }

        if (palabra.Substring(0, 1) == "/" || palabra.Substring(0, 1) == "-")
        {
            docTransporte[0] = _excel[51];
        }

        return "";
    }

    

    private string LimpiarVariable(string cadena)
    {
       
        switch (cadena.Trim())
        {
            case "Codigo":
                break;
            case "Codigo Tipo":
                break;
            case "Codigo Tipo de":
                break;
            case "Codigo Tipo de Entrada:":;
                break;
            case "Incoterm:":
                break;
            case "Numero":
                break;
            case "Numero de":
                break;
            case "Numero de Registro:":
                break;
            default:
                cadena = "";
                break;
        }
        return cadena;
    }

    private int GetIndice(string palabra)
    {

        var indice = _excel.IndexOf(palabra);
        return indice + 1;
    }

}



//for (int j = 0; j < items.Length; j++)
//{
//    if (j > 1)
//    {
//        Array.Resize(ref items, items.Length + 1);
//    }
//    items[j] = _excel[i - 2];
//}