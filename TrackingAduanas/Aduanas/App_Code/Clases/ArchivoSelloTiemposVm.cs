﻿using System;

public class ArchivoSelloTiemposVm
{
    public string IdInstruccion { get; set; }
    public string FacturaNo { get; set; }
    public string Referencia { get; set; }
    public string Cliente { get; set; }
    public DateTime Aprobado { get; set; }
    public string Correlativo { get; set; }
    public string CanalSelectividad { get; set; }
    public string IdTransaccion { get; set; }
    public DateTime Fecha { get; set; }
    public bool Eliminado { get; set; }
    public decimal Usuario { get; set; }
    
    public ArchivoSelloTiemposVm()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}