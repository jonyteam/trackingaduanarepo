﻿/// <summary>
/// Summary description for UsuarioVm
/// </summary>
public class UsuarioVm
{
    public decimal IdUsuario { get; set; }
    public string Nombre { get; set; }
    public string Apellido { get; set; }
    public string NombreCompleto { get; set; }

    public UsuarioVm()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}