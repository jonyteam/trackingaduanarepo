﻿using System;

public class TiemposFlujoCargaVm
{
    public string IdInstruccion { get; set; }
    public string Estado { get; set; }
    public DateTime? Fecha { get; set; }
    public string Observacion { get; set; }
    public string Usuario { get; set; }

    public TiemposFlujoCargaVm()
    {
        //
        // TODO: Add constructor logic here
        //
    }
}