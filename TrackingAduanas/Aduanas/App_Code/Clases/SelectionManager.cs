﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Telerik.Web;
using Telerik.Web.UI;

    public static class ProductsSelectionManager
    {

        public static void KeepSelection( Telerik.Web.UI.RadGrid grid)
        {
            //
            // se obtienen los id de producto checkeados de la pagina actual
            //
            List<int> checkedProd = (from item in grid.MasterTableView.Items.Cast<GridDataItem>()
                                     let check = (CheckBox)item.FindControl("chbAgrupar")
                                     where check.Checked
                                     select Convert.ToInt32(item["IdSolicitud"].Text)).ToList();


            
            // se recupera de session la lista de seleccionados previamente
            //
            List<int> productsIdSel =  HttpContext.Current.Session["ProdSelection"] as List<int>;

            if (productsIdSel == null)
                productsIdSel = new List<int>();

            //
            // se cruzan todos los registros de la pagina actual del gridview con la lista de seleccionados,
            // si algun item de esa pagina fue marcado previamente no se devuelve
            //
            productsIdSel = (from item in productsIdSel
                             join item2 in grid.MasterTableView.Items.Cast<GridDataItem>()
                                on item equals Convert.ToInt32(item2["IdSolicitud"].Text) into g
                             where !g.Any()
                             select item).ToList();

            //
            // se agregan los seleccionados
            //
            productsIdSel.AddRange(checkedProd);

            HttpContext.Current.Session["ProdSelection"] = productsIdSel;

        }

        public static void RestoreSelection(Telerik.Web.UI.RadGrid grid)
        {

            List<int> productsIdSel = HttpContext.Current.Session["ProdSelection"] as List<int>;

            if (productsIdSel == null)
                return;

            //
            // se comparan los registros de la pagina del grid con los recuperados de la Session
            // los coincidentes se devuelven para ser seleccionados
            //
            List<GridDataItem> result = (from item in grid.MasterTableView.Items.Cast<GridDataItem>()
                                            join item2 in productsIdSel
                                            on Convert.ToInt32(item["IdSolicitud"].Text) equals item2 into g
                                         where g.Any()
                                         select item).ToList();

            //
            // se recorre cada item para marcarlo
            //
           // foreach(item result )
            
            result.ForEach(x => ((CheckBox)x.FindControl("chbAgrupar")).Checked = true);


            foreach (GridDataItem grdItem in result)
            {
                CheckBox chkSeleccion = (CheckBox)grdItem.FindControl("chbAgrupar");

                chkSeleccion.Checked = true;

            }

            //foreach (GridDataItem Item in grid.MasterTableView.Items.Cast<GridDataItem>()) {
               
            //    CheckBox chkSeleccion = (CheckBox)Item.FindControl("chbAgrupar");

            //    foreach (GridDataItem grdItem in result)
            //    {
            //        CheckBox chkSeleccion2 = (CheckBox)grdItem.FindControl("chbAgrupar");

            //        chkSeleccion.Checked = true;

            //    }
            //}
            
        }


    }

