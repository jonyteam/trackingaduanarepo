﻿
using System;
using System.Collections.Generic;
//using System.EnterpriseServices.Internal;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;

/// <summary>
/// Summary description for ProcesoFlujoCargaGridVm
/// </summary>
public class NotasVm
{
    public int Id { get; set; }
    public string IdInstruccion { get; set; }
    public string NoFactura { get; set; }
    public string Estado { get; set; }
    public DateTime? Fecha { get; set; }
    public decimal? IdUsuario { get; set; }
    public string Usuario { get; set; }
    public string Observacion { get; set; }
    public string Eliminado { get; set; }

    public NotasVm()
    { }
}