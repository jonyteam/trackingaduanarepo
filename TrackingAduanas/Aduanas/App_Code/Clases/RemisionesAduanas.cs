﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de RemisionesAduanas
/// </summary>
public class RemisionesAduanasVM
{
	public RemisionesAduanasVM()
    { }
        public Int16 Item { get; set; }
        public string   CodAduana  { get; set; }
        public DateTime? FechaLimite { get; set; }
        public string RangoInicial   { get; set; }
        public string  RangoFinal    { get; set; }
        public string  CodEstado  { get; set; } 
        public string   Observacion  { get; set; }
        public decimal Cantidad { get; set; }
        public string SerieActual { get; set; }
        public Int16 Disponible { get; set; }
        public string Rango { get; set; }
   


	
}