﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

using Telerik.Web.UI;

public partial class ReporteEventos : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Reporte Piza Hut";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
   
        /*SetGridFilterMenu(rgRoles.FilterMenu);
        rgRoles.Skin = Skin;*/
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Mantenimiento Unidades", "Consultas Varias");
            
            //if (!(Anular || Ingresar || Modificar))
            //    redirectTo("Inicio.aspx");

            try
            {
                conectar();
                CodigosBO bo = new CodigosBO(logApp);
                bo.loadEspeciesnoAdmin();
                cmbespeciefiscal.DataSource = bo.TABLA;
                cmbespeciefiscal.DataBind();
            }

            catch { }
            finally { desconectar(); }
        }
    }
    public override bool CanGoBack { get { return false; } }
    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

     protected void RadButton1_Click(object sender, EventArgs e)
    {
        llegarGrid();
    }

    private void llegarGrid()
    {

        if (txtrango.Text != "")
        {
            try
            {
                string aduana = "";

                conectar();
                EspeciesFiscalesInstruccionesBO bo = new EspeciesFiscalesInstruccionesBO(logApp);
                MovimientosEspeciesFiscalesBO mef = new MovimientosEspeciesFiscalesBO(logApp);
                if (User.IsInRole("Administradores") || User.IsInRole("Facturación"))
                {
            
                        bo.ConsultaVaria(txtrango.Text.Trim(), "", cmbespeciefiscal.SelectedValue);

                        if (bo.totalRegistros < 1)
                        {
                            mef.CargarMovimiento(txtrango.Text.Trim(), "", cmbespeciefiscal.SelectedValue);
                        }
            
                }
                else
                {
                        if (Session["CodigoAduana"].ToString() == "PRINC")
                        {
                            aduana = "014";
                        }
                        else
                        {
                            aduana = Session["CodigoAduana"].ToString();
                        }

                        bo.ConsultaVaria(txtrango.Text.Trim(), aduana, cmbespeciefiscal.SelectedValue);

                        if (bo.totalRegistros < 1)
                        {
                            mef.CargarMovimiento(txtrango.Text.Trim(), aduana, cmbespeciefiscal.SelectedValue);
                        }
            

                }


                if (bo.totalRegistros < 1)
                {
                    rgrango1.DataSource = mef.TABLA;
                    rgrango1.DataBind();
                }
                else
                {
                    rgrango1.DataSource = bo.TABLA;
                    rgrango1.DataBind();
                }
         
            }
            catch { }

            finally { desconectar(); }
        }
    }

    protected void rgrango_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {


    }
    protected void rgrango1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llegarGrid();

    }

    private void LlegarGridEspeciesHojas()
    {

        try 
        {
            conectar();
        }

        catch { }
                
        finally { desconectar();}
    }

    protected void btnbuscarr_Click(object sender, EventArgs e)
    {
        llenargridRegimen();
        this.txthoja.Enabled = false;
        this.cmbregimen.Enabled = true;
    }

    private void llenargridRegimen()
    {   

     try
        {
            conectar();
            GestionCargaBO GC = new GestionCargaBO(logApp);
            CodigosBO C = new CodigosBO(logApp);
            Session["Hoja"] = "";
            Session ["Hoja"] = this.txthoja.Text;
            GC.CargaTiempoRegimen(this.txthoja.Text);
            rgRegimen.DataSource = GC.TABLA;
            rgRegimen.DataBind();
            string pais = this.txthoja.Text.Substring(0, 1);
            string paisregimen = "";
            if (pais == "H")
            { paisregimen = "REGIMENHONDURAS"; }
            if (pais == "N")
            { paisregimen = "REGIMENNICARAGUA"; }
            if (pais == "G")
            { paisregimen = "REGIMENGUATEMALA"; }
            if (pais == "S")
            { paisregimen = "REGIMENEL SALVADOR"; }
            C.Regimen(paisregimen);
            cmbregimen.DataSource = C.TABLA;
            cmbregimen.DataBind();
        }
        catch (Exception)
        {

            
        }
    

        finally { desconectar(); }
    }

    protected void RadTabStrip2_TabClick(object sender, RadTabStripEventArgs e)
    {
        if (RadTabStrip2.SelectedIndex == 1 && User.IsInRole("Administradores"))
        {
            txthoja.Visible = true;
            cmbregimen.Visible = true;
            btnbuscarr.Visible = true;
            btncambiar.Visible = true;
            rgRegimen.Visible = true;
            txthoja.Enabled = true;
            cmbregimen.Enabled = true;
            btnbuscarr.Enabled = true;
            btncambiar.Enabled = true;
            rgRegimen.Enabled = true;
        }
        else
        {
            txthoja.Text = "";
        
        }
    }
    protected void RadButton1_Click1(object sender, EventArgs e)
    {
        try
        {
            conectar();
            GestionCargaBO GC = new GestionCargaBO(logApp);
            //GestionCargaBO GC1 = new GestionCargaBO(logApp);
            InstruccionesBO I = new InstruccionesBO(logApp);
            FlujoCargaBO FC = new FlujoCargaBO(logApp);
            FlujoCargaBO FC1 = new FlujoCargaBO(logApp);
            string pais = this.txthoja.Text.Substring(0, 1);
            string RegimenViejo = "";
            I.loadInstruccion(this.txthoja.Text);
            if (I.IDESTADOFLUJO == "99"||I.IDESTADOFLUJO =="0")
            {
                I.CODREGIMEN = cmbregimen.SelectedValue;

            }
            else
            {
                //string pais = txthoja.Text.Substring(0,1);
                FC.loadFlujoCarga(I.IDESTADOFLUJO);
                FC1.loadCodEstadoFlujoCargaXHoja(cmbregimen.SelectedValue, FC.CODESTADOFLUJOCARGA,pais);

                if (FC1.totalRegistros == 0)
                {
                    Int32 estado;
                    //FC.CODESTADOFLUJOCARGA = estado;
                    estado = Convert.ToInt16(FC.CODESTADOFLUJOCARGA.ToString());// (FC.CODESTADOFLUJOCARGA) + 1);
                    estado = (estado + 1);
                    FC1.loadCodEstadoFlujoCargaXHoja(cmbregimen.SelectedValue, Convert.ToString(estado),pais);
                }
                RegimenViejo = I.CODREGIMEN;
                I.CODREGIMEN = cmbregimen.SelectedValue;
                I.IDESTADOFLUJO = FC1.IDESTADOFLUJO;
            }
            llenarBitacora("Se cambio el regimen de la hoja de ruta " + I.IDINSTRUCCION + " de " + RegimenViejo + " a" + cmbregimen.SelectedValue, Session["IdUsuario"].ToString(), I.IDINSTRUCCION);
            I.commitLine();
            I.actualizar();
            
            foreach (GridItem item in rgRegimen.Items)
            {
                GC.loadGestionCargaXIdGestionCarga(item.Cells[2].Text);
                if (GC.IDESTADOFLUJO != "0")
                {
                    FC.loadFlujoCarga(GC.IDESTADOFLUJO);
                    FC1.loadCodEstadoFlujoCarga(cmbregimen.SelectedValue, FC.CODESTADOFLUJOCARGA, pais);
                    if (FC1.totalRegistros == 1)
                    {
                        GC.IDESTADOFLUJO = FC1.IDESTADOFLUJO;
                    }
                    else
                    {
                        GC.ELIMINADO = "1";
                    }
                }
                GC.commitLine();
                GC.actualizar();
                
                
            }

            rgRegimen.Rebind();

        }
        catch (Exception)
        {
            
 
        }

    }
    protected void rgRegimen_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenargridRegimen();
    }
}
