﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="DeclaracionAduaneraSarah.aspx.cs" Inherits="FcJefeDeAduanaPuertoEstados" %>



<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 700px;
            font-size: 14px;
            font-family: "Times New Roman", Serif;
            border: 2px solid black;
            padding: 2px;
        }
        .auto-style2 {
            width: 350px;
            height: 24px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="miDiv" runat="server" class="panelCentrado2">
        <table width="100%">
            <tr>
                <td>

    <table style='width: 700px; text-align: center; border-style:ridge; background-color:white; border-bottom-style:none;' cellpadding="1" cellspacing="1"  >
		<tr><td><b>DECLARACION ADUANERA</b></td></tr>
		<tr><td>
            <asp:label runat="server" id="AgenteAduanero2" text="Label"></asp:label>
        </td></tr>
	</table>
	
	<table cellpadding="1" cellspacing="1" class="auto-style1" style="background-color:white">
		<tr>
			<td style='background-color:white; border-style:ridge' class="auto-style2">
				 <b>Declaracción : </b> <asp:Label ID="Correlativo" runat="server" Text="Label"></asp:Label>
			</td>
			<td style='background-color:white;border-style:ridge' class="auto-style2" >
				<b>Nro. Preimpreso : </b>  <asp:label id="Preimpreso" runat="server" text="Label"></asp:label>
			</td>
		</tr>
		<tr>
			<td style='background-color:white;border-style:ridge' >
				<b>Regimen Aduanero : </b> <asp:label id="RegimenAduanero" runat="server" text="Label"></asp:label>
				<br/>
			</td>
			<td style='background-color:white;border-style:ridge' >
				<b>Importador/Exportador : </b> <asp:label id="ImportadorExportador" runat="server" text="Label"></asp:label>
				<br/>
			</td>
		</tr>
		<tr>
			<td style="border-style:ridge;" class="auto-style2" >
				<b>Fecha Oficialización : </b> <asp:label id="FechaOficializacion" runat="server" text="Label"></asp:label>
			</td>
			<td style="border-style:ridge;" class="auto-style2">
				<b>Estado Declaración : </b> <asp:label id="EstadoDeclaracion" runat="server" text="Label"></asp:label>
			</td>
		</tr>
		<tr>
			<td style="border-style:ridge;" class="auto-style2">
				<b>Empresa Declarante : </b> <asp:label runat="server" id="AgenteAduanero" text="Label"></asp:label>
				<br/>
			</td>
			<td style="border-style:ridge;" class="auto-style2">
				<b>Aduana de Ingreso/Salida : </b> <asp:label runat="server" id="AduanaIngreso" text="Label"></asp:label>
				<br/>
			</td>
		</tr>
		<tr>
			<td style="border-style:ridge;" class="auto-style2">
				<b>Pais de Origen : </b> <asp:label runat="server" id="PaisOrigen" text="Label"></asp:label>
			</td>
			<td style="border-style:ridge;" class="auto-style2">
				<b>Pais Proc./Dest. :  </b><asp:label runat="server" id="PaisProcedencia" text="Label"></asp:label>
			</td>
		</tr>
		<tr>
			<td style="border-style:ridge;" class="auto-style2">
				<b>Condición de Entrega : </b> <asp:label runat="server" id="CondicionEntrega" text="Label"></asp:label>
			</td>
			<td style="border-style:ridge;" class="auto-style2">
				<b> Valor Factura, Flete, Seguro y Otros : </b> <br/> <asp:label runat="server" id="ValorFacturaFleteSeg" text="Label"></asp:label>
			</td>
		</tr>
	</table>
	<br/>
	
		<table cellpadding="1" cellspacing="1" class="auto-style1">
		<tr style='background-color:#D3D3D3;'><td align="center"><b>BULTOS - T&iacute;tulo de Transporte: <asp:label runat="server" id="Correlativo2" text="Label"></asp:label> </b></td></tr>
		
		<tr><td align='center'>
            <telerik:RadGrid ID="RadGridBultos" runat="server" AutoGenerateColumns="False" Culture="es-ES" GroupPanelPosition="Top" CellSpacing="0" GridLines="None" OnNeedDataSource="RadGridBultos_NeedDataSource">
<MasterTableView>
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

<RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>

    <Columns>
        <telerik:GridBoundColumn DataField="NoItem" FilterControlAltText="Filter NoItem column" HeaderText="Nro" UniqueName="NoItem">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Cantidad" FilterControlAltText="Filter cantidad column" HeaderText="Cantidad" UniqueName="cantidad">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Embalaje" FilterControlAltText="Filter Embalaje column" HeaderText="Embalaje" UniqueName="Embalaje">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="PesoKilos" FilterControlAltText="Filter PesoKilos column" HeaderText="Peso en Kilos" UniqueName="PesoKilos">
        </telerik:GridBoundColumn>
    </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
</MasterTableView>

<FilterMenu EnableImageSprites="False"></FilterMenu>
            </telerik:RadGrid>
            </td></tr>
	</table>
	<br/>

	
	<table cellpadding="1" cellspacing="1" class="auto-style1">
		<tr style='background-color:#D3D3D3;'><td align="center"><b>ITEMS - Cantidad:  <asp:label runat="server" id="CantidadComercial2" text="Label"></asp:label> 
        </b></td></tr>
		
		<tr><td style='text-align: center;'>
            <telerik:RadGrid ID="RadGridItems" runat="server" AutoGenerateColumns="False" Culture="es-ES" GroupPanelPosition="Top" OnNeedDataSource="RadGridItems_NeedDataSource" CellSpacing="0" GridLines="None">
<MasterTableView>
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

<RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>

    <Columns>
        <telerik:GridBoundColumn DataField="NoItem" FilterControlAltText="Filter NoItem column" HeaderText="Item" UniqueName="NoItem">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="CantidadComercial" FilterControlAltText="Filter CantidadComercial column" HeaderText="C.Comercial" UniqueName="CantidadComercial">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="PosicionArancelaria" FilterControlAltText="Filter PosicionArancelaria column" HeaderText="Posicion" UniqueName="PosicionArancelaria">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="DescripcionMercancia" FilterControlAltText="Filter DescripcionMercancia column" HeaderText="Descripcion" UniqueName="DescripcionMercancia">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="PesoNeto" FilterControlAltText="Filter PesoNeto column" HeaderText="P.N. Kgs" UniqueName="PesoNeto">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="ValorUsd" FilterControlAltText="Filter ValorUsd column" HeaderText="V.F. USD" UniqueName="ValorUsd">
        </telerik:GridBoundColumn>
    </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
</MasterTableView>

<FilterMenu EnableImageSprites="False"></FilterMenu>
            </telerik:RadGrid>
            </td></tr>
	</table>
	<br/>
	<table cellpadding="1" cellspacing="1" class="auto-style1">
		<tr style='background-color:#D3D3D3;'><td colspan="2" align="center"><b>CONCEPTOS </b></td></tr>
		
		<tr><td colspan="2">
            <telerik:RadGrid ID="RadGridConceptos" runat="server" AutoGenerateColumns="False" CellSpacing="0" Culture="es-ES" GridLines="None" GroupPanelPosition="Top" OnNeedDataSource="RadGridConceptos_NeedDataSource">
<MasterTableView>
<CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>

<RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
</RowIndicatorColumn>

<ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
</ExpandCollapseColumn>

    <Columns>
        <telerik:GridBoundColumn DataField="Concepto" FilterControlAltText="Filter Concepto column" HeaderText="Conceptos" UniqueName="Concepto">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Obligacion" FilterControlAltText="Filter Obligacion column" HeaderText="Obligación" UniqueName="Obligacion">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="ImporteLps" FilterControlAltText="Filter ImporteLps column" HeaderText="Importe Lps" UniqueName="ImporteLps">
        </telerik:GridBoundColumn>
    </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
</MasterTableView>

<FilterMenu EnableImageSprites="False"></FilterMenu>
            </telerik:RadGrid>
            </td></tr>
		<tr style='background-color:#D3D3D3;'><td align="right">Total a Pagar</td><td style='text-align: right;'>
            <asp:label runat="server" id="ImporteLps" text="Label"></asp:label>
        </td></tr>
		<tr style='background-color:#D3D3D3;'><td align="right">Total a Garantizar</td><td style='text-align: right;'>
            <asp:label runat="server" id="totalgarantia" text="Label"></asp:label>
            </td></tr>
	</table>


                </td>
            </tr>
        </table>
    </div>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgAsistenteOperaciones">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgAsistenteOperaciones" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>
