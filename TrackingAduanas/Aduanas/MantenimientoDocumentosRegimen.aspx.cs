﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class MantenimientoDocumentosRegimen : Utilidades.PaginaBase
{

    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Mantenimiento Documentos Regimen";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgDocumentos.FilterMenu);
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Mantenimiento Documentos", "Documentos");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void SetVisible(TableCellCollection cells, String commandName, bool visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is ImageButton)
                {
                    ImageButton btn = (ImageButton)control;

                    if (btn.CommandName == commandName)
                        btn.Visible = visible;
                }
            }
        }
    }

    protected Object GetPaises()
    {
        try
        {
            conectar();
            CodigosBO bo = new CodigosBO(logApp);
            bo.loadPaisesHojaRuta();
            return bo.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }
        return null;
    }

    protected Object GetDocumentos()
    {
        try
        {
            conectar();
            CodigosBO bo = new CodigosBO(logApp);
            bo.loadAllCampos("TIPODOCUMENTO");
            return bo.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }
        return null;
    }

    protected void rgDocumentos_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridDocumentos();
    }

    private void llenarGridDocumentos()
    {
        try
        {
            conectar();
            DocumentosRegimenBO bo = new DocumentosRegimenBO(logApp);
            bo.loadDocumentosRegimenALL();
            rgDocumentos.DataSource = bo.TABLA;
        }
        catch (Exception) { }
        finally
        {
            desconectar();
        }
    }

    protected void GetDatosEditados(DocumentosRegimenBO bo, GridItem item)
    {
        GridEditableItem editedItem = item as GridEditableItem;
        RadComboBox codRegimen = (RadComboBox)editedItem.FindControl("edRegimen");
        RadComboBox codDocumento = (RadComboBox)editedItem.FindControl("edDocumento");
        RadComboBox pais = (RadComboBox)editedItem.FindControl("edPais");
        bo.CODPAIS = pais.SelectedValue;
        bo.CODREGIMEN = codRegimen.SelectedValue;
        bo.CODDOCUMENTO = codDocumento.SelectedValue;
    }

    protected void rgDocumentos_InsertCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            AduanasBO a = new AduanasBO(logApp);
            DocumentosRegimenBO bo = new DocumentosRegimenBO(logApp);
            bo.loadDocumentosXRegimen("-1");
            bo.newLine();

            GetDatosEditados(bo, e.Item);
            //a.loadAduanas(bo.CODIGOADUANA);
            //if (a.totalRegistros <= 0)
            //{
            //    a.loadNombreAduanas(bo.NOMBREADUANA, bo.CODPAIS);
            //    if (a.totalRegistros <= 0)
            //    {
            bo.FECHA = DateTime.Now.ToString();
            bo.ELIMINADO = "0";
            bo.IDUSUARIO = Session["IdUsuario"].ToString();
            bo.commitLine();
            bo.actualizar();
            registrarMensaje("Documento agregado a regimen exitosamente");
            llenarBitacora("Se agregó el documento " + bo.CODDOCUMENTO + " al régimen " + bo.CODREGIMEN, Session["IdUsuario"].ToString(), bo.CODDOCUMENTO + " al " + bo.CODREGIMEN);
            rgDocumentos.Rebind();
            //}
            //else
            //    registrarMensaje("Ya existe la aduana " + a.CODIGOADUANA + " para este país con este mismo nombre");
            //}
            //else
            //    registrarMensaje("Este código de aduana ya existe para la aduana " + a.NOMBREADUANA);
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoDocumentosRegimen");
            if (ex.Message.Contains("Violation of PRIMARY KEY constraint 'PK_DocumentosRegimen'. Cannot insert duplicate key in object 'dbo.DocumentosRegimen'"))
                registrarMensaje("El documento ya existe en ese regimen");
        }
        finally
        {
            desconectar();
        }
    }

    protected void rgDocumentos_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();
            GridEditableItem editedItem = e.Item as GridEditableItem;
            String codRegimen = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["CodRegimen"]);
            String codDocumento = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["CodDocumento"]);

            DocumentosRegimenBO bo = new DocumentosRegimenBO(logApp);
            bo.loadDocumentosXRegimenDoc(codRegimen, codDocumento);

            GetDatosEditados(bo, e.Item);

            bo.actualizar();
            registrarMensaje("Documento actualizada exitosamente");
            llenarBitacora("Se modificó el documento " + bo.CODDOCUMENTO + " del régimen " + bo.CODREGIMEN, Session["IdUsuario"].ToString(), bo.CODDOCUMENTO);
            rgDocumentos.Rebind();
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoDocumentosRegimen");
            if (ex.Message.Contains("Violation of PRIMARY KEY constraint 'PK_DocumentosRegimen'. Cannot insert duplicate key in object 'dbo.DocumentosRegimen'"))
                registrarMensaje("El documento ya existe en ese regimen");
        }
        finally
        {
            desconectar();
        }
    }

    protected void rgDocumentos_DeleteCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            GridEditableItem editedItem = e.Item as GridEditableItem;
            String codRegimen = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["CodRegimen"]);
            String codDocumento = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["CodDocumento"]);
            DocumentosRegimenBO bo = new DocumentosRegimenBO(logApp);
            bo.loadDocumentosXRegimenDoc(codRegimen, codDocumento);
            bo.ELIMINADO = "1";
            bo.actualizar();
            registrarMensaje("Documento eliminado de régimen exitosamente");
            llenarBitacora("Se eliminó el documento " + codDocumento + " del régimen " + codRegimen, Session["IdUsuario"].ToString(), "");
            rgDocumentos.Rebind();
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoDocumentosRegimen");
        }
        finally
        {
            desconectar();
        }

    }

    protected void rgDocumentos_Init(object sender, EventArgs e)
    {
        GridFilterMenu menu = rgDocumentos.FilterMenu;
        menu.Items.RemoveAt(rgDocumentos.FilterMenu.Items.Count - 2);
    }

    #region Regimenes
    public RadComboBox edPais;
    public RadComboBox edRegimen;

    protected void rgClientes_ItemDataBound(object sender, GridItemEventArgs e)
    {
        try
        {
            if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
            {
                conectar();
                CodigosBO co = new CodigosBO(logApp);
                GridEditFormItem editForm = e.Item as GridEditFormItem;
                edRegimen = editForm.FindControl("edRegimen") as RadComboBox;

                co.loadAllCampos("REGIMEN" + edPais.SelectedItem.Text.Trim());
                edRegimen.DataSource = co.TABLA;
                edRegimen.DataBind();

                String codRegimen = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["CodRegimen"]);
                edRegimen.SelectedValue = codRegimen;
                //edMonRemFin.Value = co.CODIGO;
                //Session["monRemFin"] = edMonRemFin.Value;
            }
        }
        catch (Exception)
        { }
    }

    protected void rgClientes_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
        {
            conectar();
            GridEditFormItem editForm = e.Item as GridEditFormItem;
            edPais = editForm.FindControl("edPais") as RadComboBox;
            edPais.AutoPostBack = true;
            edPais.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(edPais_SelectedIndexChanged);
        }
        //if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
        //{
        //    conectar();
        //    GridEditFormItem editForm = e.Item as GridEditFormItem;
        //    edRegimen = editForm.FindControl("edRegimen") as RadComboBox;
        //    edRegimen.AutoPostBack = true;
        //    edRegimen.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(edRegimen_SelectedIndexChanged);
        //}
    }

    public void edPais_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            conectar();
            GridEditFormItem editForm = (sender as RadComboBox).NamingContainer as GridEditFormItem;
            edRegimen = editForm.FindControl("edRegimen") as RadComboBox;
            CodigosBO co = new CodigosBO(logApp);
            co.loadAllCampos("REGIMEN" + edPais.SelectedItem.Text.Trim());
            edRegimen.DataSource = co.TABLA;
            edRegimen.DataBind();
            //edMonitoreoFin.SelectedValue = edMonRemFin.Value;
            edRegimen.SelectedValue = Session["monRemFin"].ToString();
        }
        catch (Exception)
        { }
    }
    #endregion
}