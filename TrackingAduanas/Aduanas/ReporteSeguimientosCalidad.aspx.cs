﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.OleDb;
using Telerik.Web.UI;
using Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using System.Collections;

public partial class ReporteSegumientoCalidad : Utilidades.PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Seguimiento", "Reporte Seguimiento");
            CargarDatosInicio();
        }
    }
    //protected bool Consultar { get { return tienePermiso("Consultar"); } }
    public override bool CanGoBack { get { return false; } }
    protected void CargarDatosInicio()
    {
        try
        {
            conectar();
            CodigosBO C = new CodigosBO(logApp);
            C.loadPaisesHojaRuta();
            cmbPais.DataSource = C.TABLA;
            cmbPais.DataBind();
            AduanasBO A = new AduanasBO(logApp);
            A.loadAllCampoAduanasReportes(cmbPais.SelectedValue);
            cmbAduana.DataSource = A.TABLA;
            cmbAduana.DataBind();
        }
        catch (Exception)
        {
        }
        finally { desconectar(); }
    }
    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        llenarGrid();
    }
    private void llenarGrid()
    {
        try
        {
            conectar();
            InstruccionesBO I = new InstruccionesBO(logApp);
            I.ReporteSeguimientoCalidad(dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"), cmbPais.SelectedValue, cmbAduana.SelectedValue);
            grReporte.DataSource = I.TABLA;
            grReporte.DataBind();
            btnGuardar.Visible = true;
        }
        catch { }
        finally { desconectar(); }
    }
    protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenarGrid();
    }
    protected void writeToExcel()
    {
        if (grReporte.Items.Count >= 0)
        {
            btnGuardar.Visible = false;
            string nameFile = "Control de TramitesT.xlsx";
            string nameDest = "Control de Tramites.xlsx";
            Application.Lock();
            if (File.Exists("J:\\Files1\\Aduanas\\" + nameDest))
            {
                File.Delete("J:\\Files1\\Aduanas\\" + nameDest);
                File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
            }
            else
            {
                File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
            }
            var mTemplateFileName = @"J:\Files1\Aduanas\" + nameDest;

            var xlApp = new ApplicationClass() { DisplayAlerts = false, AskToUpdateLinks = false };
            var workbooks = xlApp.Workbooks;
            var workbook = workbooks.Open(mTemplateFileName, 2, false);

            try
            {
                conectar();
                #region Escribir Excell
                const string mInputWorkSheetName = "Datos";
                var inputWorkSheet = workbook.Worksheets.Cast<Worksheet>().FirstOrDefault(w => w.Name == mInputWorkSheetName);
                int pos = 2;
                foreach (GridDataItem gridItem in grReporte.Items)
                {
                    pos = pos + 1;

                    inputWorkSheet.Cells.Range["A" + pos].Value = gridItem.Cells[2].Text;// Id Instruccion
                    inputWorkSheet.Cells.Range["B" + pos].Value = gridItem.Cells[3].Text;// Cliente
                    inputWorkSheet.Cells.Range["C" + pos].Value = gridItem.Cells[4].Text;// Producto
                    inputWorkSheet.Cells.Range["D" + pos].Value = gridItem.Cells[5].Text;// Tipo cargamento
                    inputWorkSheet.Cells.Range["E" + pos].Value = gridItem.Cells[6].Text;// Cod Régimen
                    inputWorkSheet.Cells.Range["F" + pos].Value = gridItem.Cells[7].Text;// Envio de Doc. Completo a Aduana
                    inputWorkSheet.Cells.Range["G" + pos].Value = gridItem.Cells[8].Text;// Recepcion de Doc. Completos Aduana

                    inputWorkSheet.Cells.Range["H" + pos].Value = gridItem.Cells[9].Text;// Arribo de Carga
                    inputWorkSheet.Cells.Range["I" + pos].Value = gridItem.Cells[10].Text;// Manifiesto
                    inputWorkSheet.Cells.Range["J" + pos].Value = gridItem.Cells[11].Text;// Documentos mínimos (CF)
                    inputWorkSheet.Cells.Range["K" + pos].Value = gridItem.Cells[12].Text;// Envio de Doc. Competos a Centralizacion
                    inputWorkSheet.Cells.Range["L" + pos].Value = gridItem.Cells[13].Text;// Recepcion de Doc. Completos a Centralizacion
 
                    inputWorkSheet.Cells.Range["M" + pos].Value = gridItem.Cells[14].Text;// Validación Electrónica
                    inputWorkSheet.Cells.Range["N" + pos].Value = gridItem.Cells[15].Text;// Pago de Impuestos
                    inputWorkSheet.Cells.Range["O" + pos].Value = gridItem.Cells[16].Text;// Asignación Canal Selectividad
                    inputWorkSheet.Cells.Range["P" + pos].Value = gridItem.Cells[17].Text;// Color
                    inputWorkSheet.Cells.Range["Q" + pos].Value = gridItem.Cells[18].Text;// Confirmación de cita OPC
                    inputWorkSheet.Cells.Range["R" + pos].Value = gridItem.Cells[19].Text;// Revisión de Mercancía
                    inputWorkSheet.Cells.Range["S" + pos].Value = gridItem.Cells[20].Text;// Emisión Pase Salida
                    inputWorkSheet.Cells.Range["T" + pos].Value = gridItem.Cells[21].Text;// Confirmación de Gate Pass
                    inputWorkSheet.Cells.Range["U" + pos].Value = gridItem.Cells[22].Text;// Entrega de Servicio
                    inputWorkSheet.Cells.Range["V" + pos].Value = gridItem.Cells[23].Text;// Observación general
                    inputWorkSheet.Cells.Range["W" + pos].Value = gridItem.Cells[24].Text;// Gestor
                    inputWorkSheet.Cells.Range["X" + pos].Value = gridItem.Cells[25].Text;// Tiempo total proceso (dias)
                    /*
                    inputWorkSheet.Cells.Range["X" + pos].Value = gridItem.Cells[24].Text;// Pais Origen
                    inputWorkSheet.Cells.Range["Y" + pos].Value = gridItem.Cells[59].Text;// Puerto Origen
                    inputWorkSheet.Cells.Range["Z" + pos].Value = gridItem.Cells[25].Text;// Tipo Importacion
                    inputWorkSheet.Cells.Range["AA" + pos].Value = gridItem.Cells[26].Text;// Regimen
                    inputWorkSheet.Cells.Range["AB" + pos].Value = gridItem.Cells[27].Text;// Status
                    inputWorkSheet.Cells.Range["AC" + pos].Value = gridItem.Cells[28].Text;// Naviera/Currier
                    inputWorkSheet.Cells.Range["AD" + pos].Value = gridItem.Cells[29].Text;// # de Contenedor
                    inputWorkSheet.Cells.Range["AE" + pos].Value = gridItem.Cells[30].Text;// Descripcion de Contenedor
                    inputWorkSheet.Cells.Range["AF" + pos].Value = gridItem.Cells[31].Text;// BL/Guia/Carta Porte
                    inputWorkSheet.Cells.Range["AG" + pos].Value = gridItem.Cells[32].Text;// Peso en Libra*/
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                }
                inputWorkSheet.Cells.Range["A1"].Value = "Trámites en proceso y finalizados el " + dpFechaInicio.SelectedDate + " en " + cmbAduana.Text;
                inputWorkSheet.Cells.Range["AE2"].Value = DateTime.Now;
                grReporte.Rebind();
                #endregion
                workbook.Save();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                workbook.Close();
                xlApp.Quit();
                desconectar();
                Application.UnLock();
            }
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + nameDest);
            Response.Charset = "";
            Response.TransmitFile("J:\\Files1\\Aduanas\\" + nameDest);
            Response.Flush();
            Response.End();
        }
    }
    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        writeToExcel();
    }
    protected void cmbPais_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            conectar();
            AduanasBO A = new AduanasBO(logApp);
            A.loadAllCampoAduanasReportes(cmbPais.SelectedValue);
            cmbAduana.DataSource = A.TABLA;
            cmbAduana.DataBind();
        }
        catch (Exception)
        {
        }
        finally { desconectar(); }
    }
}
