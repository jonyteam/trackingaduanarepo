﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;

using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;

public partial class CargarArchivo : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Envio Facturacion";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        RadWindowManager1.Windows.Clear();
        SetGridFilterMenu(rgIngresos.FilterMenu);
        rgIngresos.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgIngresos.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Cargar Documentos", "Informe de Gastos");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
            if (User.IsInRole("Administradores"))
            {
                imgBuscar.Enabled = true;
            }
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void SetVisible(TableCellCollection cells, String commandName, bool visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is ImageButton)
                {
                    ImageButton btn = (ImageButton)control;

                    if (btn.CommandName == commandName)
                        btn.Visible = visible;
                }
            }
        }
    }

    protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
    {
        if (e.Argument == "Rebind")
        {
            rgIngresos.Rebind();
        }
    }

    #region Ingresos
    protected void rgIngresos_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridIngresos();
    }

    private void llenarGridIngresos()
    {
        try
        {
            conectar();
            DetalleGastosBO DG = new DetalleGastosBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            if (User.IsInRole("Administradores") && User.Identity.Name == "dabonilla" || User.IsInRole("Administradores") && User.Identity.Name == "aalvarado" || User.IsInRole("Administradores") && User.Identity.Name == "lramirez" || User.IsInRole("Administradores") && User.Identity.Name == "mnapky" || User.IsInRole("Administradores") && User.Identity.Name == "gcorrales" || User.IsInRole("Administradores") && User.Identity.Name == "ycaballero")
                DG.RevisarGastos();
            else if (User.IsInRole("Administrador Pais") || User.IsInRole("Administrador Pais SVGT") || User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                DG.RevisarGastosXPias(u.CODPAIS);
            }
            rgIngresos.DataSource = DG.TABLA;
            rgIngresos.DataBind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void rgIngresos_Init(object sender, System.EventArgs e)
    {

        GridFilterMenu filterMenu = rgIngresos.FilterMenu;

        int currentItemIndex = 0;
        while (currentItemIndex < filterMenu.Items.Count)
        {
            RadMenuItem item = filterMenu.Items[currentItemIndex];
            if (item.Text.Contains("Empty") || item.Text.Contains("Null"))
            {
                filterMenu.Items.Remove(item);
            }
            else currentItemIndex++;
        }
    }

    protected void rgIngresos_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            GridDataItem editedItem = e.Item as GridDataItem;
            if (e.CommandName == "Tiempo")
            {

            }
            else if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
                ConfigureExport();
        }
        catch { }
    }
    private void ConfigureExport()
    {
        String filename = "Ingresos_" + DateTime.Now.ToShortDateString();
        rgIngresos.GridLines = GridLines.Both;
        rgIngresos.ExportSettings.FileName = filename;
        rgIngresos.ExportSettings.IgnorePaging = true;
        rgIngresos.ExportSettings.OpenInNewWindow = false;
        rgIngresos.ExportSettings.ExportOnlyData = true;
    }
    #endregion
    protected void rgIngresos_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
    {
        try
        {
            conectar();
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            string HojaRuta = dataItem.Cells[2].Text;
            DetalleGastosBO DG = new DetalleGastosBO(logApp);
            DG.DetalleGastosXHoja(HojaRuta);
            e.DetailTableView.DataSource = DG.TABLA;

        }
        catch (Exception)
        {

        }
        finally { desconectar(); }

    }
    protected void imgBuscar_Click(object sender, ImageClickEventArgs e)
    {
        conectar();
        DetalleGastosBO DG = new DetalleGastosBO(logApp);
        DG.RevisarGastos(txtInstruccion.Text);
        rgIngresos.DataSource = DG.TABLA;
        rgIngresos.DataBind();
    }
}