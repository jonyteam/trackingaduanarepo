﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ReporteGeneral.aspx.cs" Inherits="ReporteGeneral" %>

<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="HeadContent">

    <style type="text/css">

        .tablaprincipal {
            margin-left: 250px;

        }
        #divGridReporteGeneral {
            margin-left: 248px;
            text-align: center;
        }
        .datePicker {
            margin-left: 200px;
            margin-top: -23px;
            margin-bottom: 25px;
        }
        .mesFin {
            margin-left: 200px;
            margin-top: -23px;
        }

        </style>

</asp:Content>
<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="MainContent">
    <telerik:RadAjaxPanel runat="server" ID="radPanel" LoadingPanelID="RadAjaxLoadingPanel1">
        <div>
        <%--<asp:Label runat="server" Text="Reoprte General"></asp:Label>--%>
        <div>
            <table style="width: 60%" border="1" id="tblPrincipal" class="tablaprincipal">
                <tr>
                    <td><asp:Label runat="server" Text="Cliente:"></asp:Label></td>
                    <td>
                        <telerik:RadComboBox runat="server" ID="cmbCliente" CheckBoxes="True" DataValueField="CodigoSAP" DataTextField="Nombre" Width="50%"
                            EmptyMessage="Seleccione un cliente"></telerik:RadComboBox>
                    </td>
                    <td rowspan="6">
                        <asp:Label runat="server" Text="Por:"></asp:Label>
                        <asp:CheckBoxList runat="server" ID="chkList" >
                            <asp:ListItem Value="1" Text="Sellos de Tiempo"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Impuestos"></asp:ListItem>
                            <asp:ListItem Value="3" Text="Eventos"></asp:ListItem>
                            <asp:ListItem Value="4" Text="Gastos"></asp:ListItem>
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" Text="Tipo Aduana"></asp:Label>
                    </td>
                    <td >
                        <telerik:RadComboBox runat="server" ID="cmbTipoAduana">
                            <Items>
                                <telerik:RadComboBoxItem runat="server" Value="0" Text="Seleccion...."/>
                                <telerik:RadComboBoxItem runat="server" Value="1" Text="Terrestre/Aerea"/>
                                <telerik:RadComboBoxItem runat="server" Value="2" Text="Maritimo"/>
                            </Items>
                        </telerik:RadComboBox>
                    </td>
                </tr>
                <tr>
                    <td rowspan="4">
                        <asp:Label runat="server" Text="Perido a Evaluar:"></asp:Label>
                    </td>
                    <td>
                        <telerik:RadAjaxLoadingPanel runat="server" ID="loadingPanel">
                        </telerik:RadAjaxLoadingPanel>
                        <telerik:RadComboBox runat="server" ID="cmbFecha" OnSelectedIndexChanged="cmbFecha_SelectedIndexChanged" AutoPostBack="True">
                            <Items>
                               
                                <telerik:RadComboBoxItem runat="server" Text="Fecha" Value="1"/>
                                <telerik:RadComboBoxItem runat="server" Text="Semana" Value="2"/>
                                <telerik:RadComboBoxItem runat="server" Text="Mes" Value="3"/>
                            </Items>
                        </telerik:RadComboBox>
                    </td>
                </tr>
                <tr>
                     <td>
                         <div>
                             <telerik:RadDatePicker ID="rdFechaInicio" runat="server" Visible="True"></telerik:RadDatePicker>
                         </div>
                         <div class="datePicker">
                             <telerik:RadDatePicker ID="rdFechaFin" runat="server" Visible="True"></telerik:RadDatePicker> 
                         </div>
                         <div>
                             <telerik:RadDatePicker ID="rdSemanaInicio" runat="server" Visible="False"></telerik:RadDatePicker>
                         </div>
                         <div class="datePicker">
                             <telerik:RadDatePicker ID="rdSemanaFin" runat="server" Visible="False"></telerik:RadDatePicker>
                         </div>
                         <div>
                             <telerik:RadMonthYearPicker ID="rdMesInicio" runat="server" Visible="False"></telerik:RadMonthYearPicker>
                         </div>
                         <div class="mesFin">
                             <telerik:RadMonthYearPicker ID="rdMesFin" runat="server" Visible="False" ></telerik:RadMonthYearPicker>
                         </div>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <table style="margin-left: 50%">
                <tr>
                    <td>
                        <div>
                            <asp:ImageButton runat="server" ID="btnGenerarReporte" ImageUrl="~/Images/24/view_24.png" OnClick="btnGenerarReporte_OnClick"></asp:ImageButton>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <br/>
    <br/>
    <div id="divGridReporteGeneral">
        <telerik:RadGrid runat="server" ID="rgReporteGeneral" AutoGenerateColumns="False" PageSize="15" AllowPaging="True"
            AllowSorting="True" GridLines="Horizontal" Width="800px" Visible="False"  OnItemCommand="rgReporteGeneral_OnItemCommand" CellSpacing="0">
            <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
            <MasterTableView DataKeyNames="Key" CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros."
                NoMasterRecordsText="No hay instrucciones." GroupLoadMode="Client">
                <CommandItemSettings ShowAddNewRecordButton="false" 
                ShowExportToExcelButton="False" />
                <%--<CommandItemSettings RefreshText="Volver a Cargar Datos" ExportToPdfText="Export to PDF"
                 ShowAddNewRecordButton="False" ShowExportToExcelButton="True">
                </CommandItemSettings>--%>
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn AllowSorting="False" DataField="Key" HeaderText="ID" UniqueName="Key" Visible="False">
                        <HeaderStyle Width="50px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Aduana" HeaderText="Aduanas" UniqueName="Aduana">
                        <HeaderStyle Width="150px" />
                        <ItemStyle HorizontalAlign="Left" Width="100px" Wrap="False" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn AllowSorting="True" DataField="Exportacion" HeaderText="Exportaciones" UniqueName="Exportacion">
                        <HeaderStyle Width="100px" Wrap="False" />
                    </telerik:GridBoundColumn>
                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Export" ImageUrl="~/Images/24/Add2_24.png" UniqueName="btnExport">
                        
                       
                    </telerik:GridButtonColumn>
                    <telerik:GridBoundColumn AllowSorting="True" DataField="Importacion" HeaderText="Importaciones" UniqueName="Importacion">
                        <HeaderStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Import" ImageUrl="~/Images/24/Add2_24.png" UniqueName="btnImport">
                       
                    </telerik:GridButtonColumn>
                    <telerik:GridBoundColumn AllowSorting="False" DataField="Transito" HeaderText="Transitos" UniqueName="Transito">
                        <HeaderStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Trensitos" ImageUrl="~/Images/24/Add2_24.png" UniqueName="btnTransitos">
                        
                    </telerik:GridButtonColumn>
                    <%--<telerik:GridBoundColumn DataField="Total" HeaderText="Total"  UniqueName="Total">
                        <HeaderStyle Width="100px" HorizontalAlign="Center"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridButtonColumn ButtonType="ImageButton" UniqueName="btnTotal" CommandName="Total" ImageUrl="~/Images/24/Add2_24.png">
                        <ItemStyle Width="75px" />
                   </telerik:GridButtonColumn>--%>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <ClientSettings ReorderColumnsOnClient="False">
                <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                                DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                                DropHereToReorder="Suelte aquí para Re-Ordenar" />
                <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
            </ClientSettings>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
    </div>
    <div>
        <asp:Panel runat="server" ID="Panel1">
            <asp:Timer runat="server" ID="Timer1" Interval="300000" OnTick="Timer1_OnTick" ClientIDMode="AutoID"></asp:Timer>
            <telerik:RadWindowManager runat="server" ID="DetailWindow" Behaviors="Close"></telerik:RadWindowManager>
        </asp:Panel>
    </div>
    </telerik:RadAjaxPanel>
    
    <telerik:RadAjaxManager runat="server" ID="AjaxPanel1"  DefaultLoadingPanelID="RadAjaxLoadingPanel1"> 
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="cmbFecha">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rdFechaInicio" />
                    <telerik:AjaxUpdatedControl ControlID="rdFechaFin" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cmbFecha">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rdSemanaInicio" />
                    <telerik:AjaxUpdatedControl ControlID="rdSemanaFin"/>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cmbFecha">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rdMesInicio" />
                    <telerik:AjaxUpdatedControl ControlID="rdMesFin"/>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rgReporteGeneral">
                <UpdatedControls>
                    <%--<telerik:AjaxUpdatedControl ControlID="rgReporteGeneral" />--%>
                    <telerik:AjaxUpdatedControl ControlID="DetailWindow" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnGenerarReporte">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgReporteGeneral" />
                   
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
     <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
          

            function OnClientClose() {
                $find("<%= AjaxPanel1.ClientID%>").ajaxRequest("Rebind");
            }
        </script>
    </telerik:RadScriptBlock>
</asp:Content>


