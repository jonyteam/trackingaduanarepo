﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="EditarTiemposInstrucciones.aspx.cs" Inherits="EditarTiemposInstrucciones" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanelFlujoGuias" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 | args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }                   
            </script>
        </telerik:RadScriptBlock>
        <input id="edIdInstruccion" runat="server" type="hidden" />
        <input id="edIdEstadoFlujo" runat="server" type="hidden" />
        <input id="edEstadoFlujo" runat="server" type="hidden" />
        <input id="edCodPaisHojaRuta" runat="server" type="hidden" />
        <input id="edCodRegimen" runat="server" type="hidden" />
        <input id="edRegimen" runat="server" type="hidden" />
        <input id="edFecha" runat="server" type="hidden" />
        <input id="edHora" runat="server" type="hidden" />
        <input id="edObservacion" runat="server" type="hidden" />
        <input id="edCorrelativo" runat="server" type="hidden" />
        <input id="edImpuesto" runat="server" type="hidden" />
        <input id="edColor" runat="server" type="hidden" />
        <input id="edTramitador" runat="server" type="hidden" />
        <input id="edAforador" runat="server" type="hidden" />
        <table id="Table1" runat="server" style="width: 100%">
            <tr>
                <td style="width: 7%">
                    <asp:Label ID="lblInstruccion" Font-Bold="true" runat="server" Text="Instrucción:"></asp:Label>
                </td>
                <td style="width: 9%">
                    <telerik:RadTextBox ID="txtInstruccion" runat="server" MaxLength="15" Width="85%">
                    </telerik:RadTextBox>
                </td>
                <td style="width: 85%">
                    <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Images/24/view_24.png"
                        OnClick="btnBuscar_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="rgInstrucciones" runat="server" AllowFilteringByColumn="True"
            AllowSorting="True" AutoGenerateColumns="False" GridLines="None" Height="410px"
            OnNeedDataSource="rgInstrucciones_NeedDataSource" AllowPaging="True" ShowFooter="True"
            ShowStatusBar="True" PageSize="20" OnInit="rgInstrucciones_Init" OnItemCommand="rgInstrucciones_ItemCommand"
            OnItemDataBound="rgInstrucciones_ItemDataBound" 
            OnItemCreated="rgInstrucciones_ItemCreated" CellSpacing="0">
            <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
            <MasterTableView DataKeyNames="IdInstruccion,IdEstadoFlujo,CodPaisHojaRuta,EstadoFlujo,CodRegimen,Regimen"
                CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay instrucciones."
                GroupLoadMode="Client">
                <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                    ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px"></HeaderStyle>
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px"></HeaderStyle>
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="IdInstruccion" HeaderText="Instruccion No." UniqueName="IdInstruccion"
                        FilterControlWidth="60%">
                        <HeaderStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Nombre" HeaderText="Cliente" UniqueName="Nombre"
                        FilterControlWidth="60%">
                        <HeaderStyle Width="250px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ReferenciaCliente" HeaderText="Referencia Cliente"
                        UniqueName="ReferenciaCliente" FilterControlWidth="60%">
                        <HeaderStyle Width="120px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CodPaisHojaRuta" HeaderText="País" UniqueName="CodPaisHojaRuta"
                        FilterControlWidth="60%" Visible="false">
                        <HeaderStyle Width="140px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CodRegimen" HeaderText="Código Régimen" UniqueName="CodRegimen"
                        FilterControlWidth="60%" Visible="false">
                        <ItemStyle Width="10%" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Regimen" HeaderText="Régimen" UniqueName="Regimen"
                        FilterControlWidth="60%">
                        <HeaderStyle Width="250px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CodigoAduana" HeaderText="CodigoAduana" UniqueName="CodigoAduana"
                        FilterControlWidth="60%" Visible="false">
                        <ItemStyle Width="10%" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="" AllowFiltering="false" ShowFilterIcon="false">
                        <ItemTemplate>
                            <telerik:RadComboBox ID="edCampo" runat="server" Width="100%" AutoPostBack="true"
                                DataTextField="Descripcion" DataValueField="Codigo">
                            </telerik:RadComboBox>
                        </ItemTemplate>
                        <HeaderStyle Width="170px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Fecha" AllowFiltering="false" ShowFilterIcon="false">
                        <ItemTemplate>
                            <telerik:RadDatePicker ID="dtFechaSellado" runat="server" Width="45%" Culture="es-HN"
                                EnableTyping="False">
                                <Calendar ID="Calendar1" runat="server" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"
                                    UseRowHeadersAsSelectors="False">
                                </Calendar>
                                <DateInput ID="DateInput1" runat="server" DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"
                                    ReadOnly="True">
                                </DateInput><DatePopupButton HoverImageUrl="" ImageUrl="" />
                            </telerik:RadDatePicker>
                            <telerik:RadTimePicker ID="dtHoraSellado" runat="server" Width="42%" Culture="es-HN"
                                EnableTyping="True">
                                <TimeView ID="TimeView2" runat="server" CellSpacing="-1" Columns="4" Culture="es-HN"
                                    Interval="00:30:00" TimeFormat="T">
                                </TimeView>
                                <TimePopupButton HoverImageUrl="" ImageUrl="" />
                            </telerik:RadTimePicker>
                        </ItemTemplate>
                        <%--<ItemStyle Width="250px" />--%>
                        <HeaderStyle Width="220px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="IdEstadoFlujo" HeaderText="IdEstadoFlujo" UniqueName="IdEstadoFlujo"
                        FilterControlWidth="60%" Visible="false">
                        <ItemStyle Width="10%" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Observación" AllowFiltering="false" ShowFilterIcon="false">
                        <ItemTemplate>
                            <telerik:RadTextBox ID="edObservacion" runat="server" MaxLength="200" Width="95%">
                            </telerik:RadTextBox>
                        </ItemTemplate>
                        <HeaderStyle Width="250px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridButtonColumn ButtonType="ImageButton" HeaderText="Guardar" CommandName="Guardar"
                        ImageUrl="~/Images/24/lock2_24.png" UniqueName="btnGuardar">
                        <ItemStyle Width="75px" />
                        <HeaderStyle Width="75px" />
                    </telerik:GridButtonColumn>
                    <telerik:GridTemplateColumn AllowFiltering="false" DataField="IdEstadoFlujo" HeaderText="Modificar Estado"
                        UniqueName="IdEstadoFlujo">
                        <ItemTemplate>
                            <asp:ImageButton ID="btnModificarEstado" runat="server" CommandArgument='<%# String.Format("{0}", Eval("IdInstruccion").ToString()) %>'
                                CommandName='<%# (Eval("IdEstadoFlujo").ToString() == "99" ? "Habilitar" : Eval("IdEstadoFlujo").ToString() == "98" ? "Deshabilitar" : "") %>'
                                Visible='<%# (Eval("IdEstadoFlujo").ToString() == "99" ? true : Eval("IdEstadoFlujo").ToString() == "98" ? true : false) %>'
                                ImageUrl='<%# (Eval("IdEstadoFlujo").ToString() == "99" ? "Images/16/check2_16.png" : "Images/16/delete2_16.png") %>'
                                OnClick="btnModificarEstado_Click" />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
            <HeaderStyle Width="180px" />
            <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                <Selecting AllowRowSelect="True" />
                <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                    DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                    DropHereToReorder="Suelte aquí para Re-Ordenar" />
                <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
            </ClientSettings>
            <FilterMenu EnableTheming="True">
                <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
            </FilterMenu>
            <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
            <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
        </telerik:RadGrid>
        <table width="100%">
            <tr>
                <td align="center">
                    <asp:ImageButton ID="btnLimpiar" runat="server" ImageUrl="~/Images/24/document_plain_24.png"
                        OnClick="btnLimpiar_Click" ToolTip="Limpiar Pantalla" />
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
</asp:Content>
