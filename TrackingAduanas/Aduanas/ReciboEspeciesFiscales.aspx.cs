﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;

public partial class ReciboEspeciesFiscales : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Recibo Especies Fiscales";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgRangoEspeciesFiscales.FilterMenu);
        rgRangoEspeciesFiscales.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgRangoEspeciesFiscales.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Recibo Especies Fiscales", "Recibo Especies Fiscales");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
            mpEspeciesFiscales.SelectedIndex = 0;
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    #region Recibo Especies Fiscales
    protected void rgRangoEspeciesFiscales_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenarGridRangoEspeciesFiscales();
    }

    private void llenarGridRangoEspeciesFiscales()
    {
        try
        {
            conectar();
            MovimientosEspeciesFiscalesBO mef = new MovimientosEspeciesFiscalesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (User.IsInRole("Administradores") & u.CODPAIS == "H")
                mef.loadRangosEspeciesFiscalesRecibo("Admin", "");
            else
                mef.loadRangosEspeciesFiscalesRecibo("", u.CODIGOADUANA);
            rgRangoEspeciesFiscales.DataSource = mef.TABLA;
        }
        catch { }
    }

    protected void rgRangoEspeciesFiscales_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            TrasladosEspeciesFiscalesBO tef = new TrasladosEspeciesFiscalesBO(logApp);
            MovimientosEspeciesFiscalesBO mef = new MovimientosEspeciesFiscalesBO(logApp);
            GridEditableItem editedItem = e.Item as GridEditableItem;
            if (e.CommandName == "Sellar")
            {
                String idMovimiento = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdMovimiento"]);
                RadDatePicker edFecha = (RadDatePicker)editedItem.FindControl("dtFechaSellado");
                RadTimePicker edHora = (RadTimePicker)editedItem.FindControl("dtHoraSellado");
                RadTextBox edObservacion = (RadTextBox)editedItem.FindControl("edObservacion");
                if (edFecha.SelectedDate.HasValue)
                {
                    string fecha = edFecha.SelectedDate.Value.ToShortDateString();
                    if (edHora.SelectedDate.HasValue)
                    {
                        string hora = edHora.SelectedDate.Value.ToShortTimeString();
                        string observacion = edObservacion.Text.Trim();
                        mef.loadMovimientosEspeciesFiscalesXIdMov(idMovimiento);
                        tef.loadTrasladosEspeciesFiscalesXIdMov(idMovimiento);
                        if (mef.totalRegistros > 0 & tef.totalRegistros > 0)
                        {
                            mef.CODESTADO = "1";    //recibido
                            mef.actualizar();
                            tef.FECHARECIBIDO = fecha + " " + hora;
                            tef.OBSERVACION = observacion;
                            tef.IDUSUARIORECIBO = Session["IdUsuario"].ToString();
                            tef.CARGA = "0";
                            tef.actualizar();
                            registrarMensaje("Especies fiscales recibidas exitosamente");
                            rgRangoEspeciesFiscales.Rebind();
                        }
                        else
                            registrarMensaje("Codigo de envío ya no existe");
                    }
                    else
                        registrarMensaje("Hora no puede estar vacía");
                }
                else
                    registrarMensaje("Fecha no puede estar vacía");
            }
            //if (e.CommandName == "Delete")
            //{
            //    String idCompra = editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdCompra"].ToString();
            //    cef.loadComprasEspeciesFiscalesXId(idCompra);
            //    if (cef.totalRegistros > 0)
            //    {
            //        mef.loadMovimientosEspeciesFiscales(cmbEspecieFiscal.SelectedValue, cmbAduanaOrigen.SelectedValue);
            //        if (cef.CANTIDAD == mef.CANTIDAD)
            //        {
            //            cef.CODESTADO = "100";
            //            cef.actualizar();
            //            registrarMensaje("Compra de especies fiscales eliminada exitosamente");
            //        }
            //        else
            //            registrarMensaje("Compra no se puede eliminar, ya tiene documentos utilizados");
            //    }
            //    else
            //        registrarMensaje("Compra no existe");
            //}
            if (rgRangoEspeciesFiscales.Items.Count >= 1 & (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName))
                ConfigureExportRangoEF();
            else if (rgRangoEspeciesFiscales.Items.Count < 1 & (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName))
                e.Canceled = true;
        }
        catch { }
    }

    private void ConfigureExportRangoEF()
    {
        String filename = "Recibo Especies Fiscales " + DateTime.Now.ToShortDateString();
        rgRangoEspeciesFiscales.ExportSettings.FileName = filename;
        rgRangoEspeciesFiscales.ExportSettings.ExportOnlyData = true;
        rgRangoEspeciesFiscales.ExportSettings.IgnorePaging = true;
        rgRangoEspeciesFiscales.ExportSettings.OpenInNewWindow = true;
        rgRangoEspeciesFiscales.MasterTableView.ExportToExcel();
    }

    protected void rgRangoEspeciesFiscales_Init(object sender, EventArgs e)
    {
        GridFilterMenu menu = rgRangoEspeciesFiscales.FilterMenu;
        menu.Items.RemoveAt(rgRangoEspeciesFiscales.FilterMenu.Items.Count - 2);
    }
    #endregion
}