﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

using Telerik.Web.UI;

public partial class ReporteEventos : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Reporte Piza Hut";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
   
        /*SetGridFilterMenu(rgRoles.FilterMenu);
        rgRoles.Skin = Skin;*/
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Mantenimiento Unidades", "Pizza Hut");
            
            //if (!(Anular || Ingresar || Modificar))
            //    redirectTo("Inicio.aspx");
        }
    }
    public override bool CanGoBack { get { return false; } }
    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void SetVisible(TableCellCollection cells, String commandName, bool visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is ImageButton)
                {
                    ImageButton btn = (ImageButton)control;

                    if (btn.CommandName == commandName)
                        btn.Visible = visible;
                }
            }
        }
    }


     protected void RadButton1_Click(object sender, EventArgs e)
    {
        /*loadGridEventos();
        loadGridEventos2();*/
    }
    #region Consulta

     protected void llenargrid()
     {
         try
         {
             conectar();
             InstruccionesBO bo = new InstruccionesBO(logApp);
             bo.ReporteOger(fechainicio.SelectedDate.Value.ToString("yyyy/MM/dd"),fechafin.SelectedDate.Value.ToString("yyyy/MM/dd"));
             Reporte.DataSource = bo.TABLA;
             Reporte.DataBind();
         }

         catch { }
         
         finally
         {
             desconectar();
         }

     }

    #endregion
     protected void RadButton2_Click(object sender, EventArgs e)
     {
         if (fechainicio.SelectedDate <= fechafin.SelectedDate)
         {
             llenargrid();
         }
         else
         {
             registrarMensaje2("Fecha inicio no puede ser mayor que fecha final");
         }
     }

     private void registrarMensaje2(string mensaje)
     {
         StringBuilder sb = new StringBuilder();
         sb.Append("<script>");
         sb.Append("alert('" + mensaje + "');");
         sb.Append("</script>");
         ScriptManager.RegisterStartupScript(this, this.GetType(), "mensaje", sb.ToString(), false);
     }

}
