﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="MantenimientoModPerRol.aspx.cs" Inherits="MantenimientoModPerRol" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadTabStrip ID="RadTabStrip1" runat="server" MultiPageID="mpPaginas" SelectedIndex="0">
        <Tabs>
            <telerik:RadTab runat="server" Text="Modulos" PageViewID="pvModulos">
            </telerik:RadTab>
            <telerik:RadTab runat="server" Text="Permisos" PageViewID="pvPermisos">
            </telerik:RadTab>
            <telerik:RadTab runat="server" Text="Perfiles" PageViewID="pvPerfiles">
            </telerik:RadTab>
            <telerik:RadTab runat="server" Text="Permisos de Modulos" PageViewID="pvPermisosModulos">
            </telerik:RadTab>
            <telerik:RadTab runat="server" Text="Autorizaciones de Modulos" PageViewID="pvAutorizacionesModulos">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="mpPaginas" runat="server" SelectedIndex="0" Width="99%">
        <telerik:RadPageView ID="pvModulos" runat="server" Width="100%">
            <telerik:RadGrid ID="rgModulos" runat="server" AllowFilteringByColumn="True" AllowSorting="True"
                AutoGenerateColumns="False" GridLines="Horizontal" Width="100%" Height="420px"
                OnNeedDataSource="rgModulos_NeedDataSource" OnUpdateCommand="rgModulos_UpdateCommand"
                OnDeleteCommand="rgModulos_DeleteCommand" OnInsertCommand="rgModulos_InsertCommand"
                AllowPaging="True" ShowFooter="True" ShowStatusBar="True" PageSize="20" 
                OnItemDataBound="rgModulos_ItemDataBound" CellSpacing="0">
                <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                <MasterTableView DataKeyNames="IdModulo" CommandItemDisplay="<%# Ingresar ? GridCommandItemDisplay.Top : GridCommandItemDisplay.None %>"
                    NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay Módulos definidos.">
                    <CommandItemSettings AddNewRecordText="Agregar Módulo" RefreshText="Volver a Cargar Datos" />
                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridEditCommandColumn AutoPostBackOnFilter="True" CancelText="Cancelar"
                            EditText="Editar" InsertText="Insertar" UpdateText="Grabar" ButtonType="ImageButton">
                            <HeaderStyle Width="5%" />
                            <ItemStyle Width="5%" />
                        </telerik:GridEditCommandColumn>
                        <telerik:GridButtonColumn ConfirmText="¿Esta seguro de que desea eliminar este módulo?"
                            ConfirmDialogType="RadWindow" ConfirmTitle="Eliminar Módulo" UniqueName="DeleteColumn"
                            ButtonType="ImageButton" CommandName="Delete">
                            <HeaderStyle Width="5%" />
                            <ItemStyle Width="5%" />
                        </telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="Descripcion" HeaderText="Descripción" UniqueName="column1"
                            Aggregate="Count" FooterText="Cantidad: " FilterControlWidth="70%">
                            <HeaderStyle Width="90%" />
                            <ItemStyle Width="90%" />
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings EditFormType="Template" CaptionDataField="Modulo" CaptionFormatString="">
                        <EditColumn UniqueName="EditCommandColumn1">
                        </EditColumn>
                        <FormTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# ((Container is GridEditFormInsertItem) ? "Agregar Nuevo Módulo" : "Editar Módulo Existente") %>'
                                Font-Size="Medium" Font-Bold="true" /><table style="width: 100%">
                                    <tr>
                                        <td style="width: 10%">
                                            <asp:Label ID="Label2" runat="server" Text="Descripción" Font-Bold="true" />
                                        </td>
                                        <td style="width: 90%">
                                            <telerik:RadTextBox ID="edDescripcion" runat="server" Text='<%# Bind( "Descripcion") %>'
                                                EmptyMessage="Escriba la Descripción aquí" Width="50%" /><asp:RequiredFieldValidator
                                                    ID="rfDescripcion" runat="server" EnableClientScript="true" Display="Dynamic"
                                                    ErrorMessage="La Descripción no puede estar vacía" ControlToValidate="edDescripcion" />
                                        </td>
                                    </tr>
                                </table>
                            <asp:ImageButton ID="btnSave" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                AlternateText='<%# (Container is GridEditFormInsertItem) ? "Agregar Módulo" : "Actualizar Módulo" %>'
                                ImageUrl='<%# GetTelerikResourceUrl("Grid.Update.gif") %>' />&#160;&nbsp<asp:ImageButton
                                    ID="btnCancel" runat="server" CommandName="Cancel" AlternateText="Cancelar" ImageUrl='<%# GetTelerikResourceUrl("Grid.Cancel.gif") %>'
                                    CausesValidation="false" /></FormTemplate>
                    </EditFormSettings>
                </MasterTableView><ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                    <Selecting AllowRowSelect="True" />
                    <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                        DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                        DropHereToReorder="Suelte aquí para Re-Ordenar" />
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                </ClientSettings>
                <FilterMenu EnableTheming="True">
                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                </FilterMenu>
                <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
            </telerik:RadGrid></telerik:RadPageView>
        <telerik:RadPageView ID="pvPermisos" runat="server">
            <telerik:RadGrid ID="rgPermisos" runat="server" AllowFilteringByColumn="True" AllowSorting="True"
                AutoGenerateColumns="False" GridLines="Horizontal" Width="100%" Height="420px"
                OnNeedDataSource="rgPermisos_NeedDataSource" OnUpdateCommand="rgPermisos_UpdateCommand"
                OnDeleteCommand="rgPermisos_DeleteCommand" OnInsertCommand="rgPermisos_InsertCommand"
                AllowPaging="True" ShowFooter="True" ShowStatusBar="True" PageSize="20" OnItemDataBound="rgPermisos_ItemDataBound">
                <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                <MasterTableView DataKeyNames="IdPermiso" CommandItemDisplay="<%# Ingresar ? GridCommandItemDisplay.Top : GridCommandItemDisplay.None %>"
                    NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay Permisos definidos.">
                    <CommandItemSettings AddNewRecordText="Agregar Permiso" RefreshText="Volver a Cargar Datos" />
                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridEditCommandColumn AutoPostBackOnFilter="True" CancelText="Cancelar"
                            EditText="Editar" InsertText="Insertar" UpdateText="Grabar" ButtonType="ImageButton">
                            <HeaderStyle Width="5%" />
                            <ItemStyle Width="5%" />
                        </telerik:GridEditCommandColumn>
                        <telerik:GridButtonColumn ConfirmText="¿Esta seguro de que desea eliminar este permiso?"
                            ConfirmDialogType="RadWindow" ConfirmTitle="Eliminar Permiso" UniqueName="DeleteColumn"
                            ButtonType="ImageButton" CommandName="Delete">
                            <HeaderStyle Width="5%" />
                            <ItemStyle Width="5%" />
                        </telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="Descripcion" HeaderText="Descripción" UniqueName="column1"
                            Aggregate="Count" FooterText="Cantidad: " FilterControlWidth="70%">
                            <HeaderStyle Width="90%" />
                            <ItemStyle Width="90%" />
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings EditFormType="Template" CaptionDataField="Permiso" CaptionFormatString="">
                        <EditColumn UniqueName="EditCommandColumn1">
                        </EditColumn>
                        <FormTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# ((Container is GridEditFormInsertItem) ? "Agregar Nuevo Permiso" : "Editar Permiso Existente") %>'
                                Font-Size="Medium" Font-Bold="true" /><table style="width: 100%">
                                    <tr>
                                        <td style="width: 10%">
                                            <asp:Label ID="Label2" runat="server" Text="Descripción" Font-Bold="true" />
                                        </td>
                                        <td style="width: 90%">
                                            <telerik:RadTextBox ID="edDescripcion" runat="server" Text='<%# Bind( "Descripcion") %>'
                                                EmptyMessage="Escriba la Descripción aquí" Width="50%" /><asp:RequiredFieldValidator
                                                    ID="rfDescripcion" runat="server" EnableClientScript="true" Display="Dynamic"
                                                    ErrorMessage="La Descripción no puede estar vacía" ControlToValidate="edDescripcion" />
                                        </td>
                                    </tr>
                                </table>
                            <asp:ImageButton ID="btnSave" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                AlternateText='<%# (Container is GridEditFormInsertItem) ? "Agregar Permiso" : "Actualizar Permiso" %>'
                                ImageUrl='<%# GetTelerikResourceUrl("Grid.Update.gif") %>' />&#160;&nbsp<asp:ImageButton
                                    ID="btnCancel" runat="server" CommandName="Cancel" AlternateText="Cancelar" ImageUrl='<%# GetTelerikResourceUrl("Grid.Cancel.gif") %>'
                                    CausesValidation="false" /></FormTemplate>
                    </EditFormSettings>
                </MasterTableView><ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                    <Selecting AllowRowSelect="True" />
                    <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                        DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                        DropHereToReorder="Suelte aquí para Re-Ordenar" />
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                </ClientSettings>
                <FilterMenu EnableTheming="True">
                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                </FilterMenu>
                <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
            </telerik:RadGrid></telerik:RadPageView>
        <telerik:RadPageView ID="pvPerfiles" runat="server">
            <telerik:RadGrid ID="rgRoles" runat="server" AllowFilteringByColumn="True" AllowSorting="True"
                AutoGenerateColumns="False" GridLines="Horizontal" Width="100%" Height="420px"
                OnNeedDataSource="rgRoles_NeedDataSource" OnUpdateCommand="rgRoles_UpdateCommand"
                OnDeleteCommand="rgRoles_DeleteCommand" OnInsertCommand="rgRoles_InsertCommand"
                AllowPaging="True" ShowFooter="True" ShowStatusBar="True" PageSize="20" OnItemDataBound="rgRoles_ItemDataBound">
                <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                <MasterTableView DataKeyNames="IdRol" CommandItemDisplay="<%# Ingresar ? GridCommandItemDisplay.Top : GridCommandItemDisplay.None %>"
                    NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay Perfiles definidos.">
                    <CommandItemSettings AddNewRecordText="Agregar Perfil" RefreshText="Volver a Cargar Datos" />
                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridEditCommandColumn AutoPostBackOnFilter="True" CancelText="Cancelar"
                            EditText="Editar" InsertText="Insertar" UpdateText="Grabar" ButtonType="ImageButton">
                            <HeaderStyle Width="5%" />
                            <ItemStyle Width="5%" />
                        </telerik:GridEditCommandColumn>
                        <telerik:GridButtonColumn ConfirmText="¿Esta seguro de que desea eliminar este perfil?"
                            ConfirmDialogType="RadWindow" ConfirmTitle="Eliminar Perfil" UniqueName="DeleteColumn"
                            ButtonType="ImageButton" CommandName="Delete">
                            <HeaderStyle Width="5%" />
                            <ItemStyle Width="5%" />
                        </telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="Descripcion" HeaderText="Descripción" UniqueName="column1"
                            Aggregate="Count" FooterText="Cantidad: " FilterControlWidth="70%">
                            <HeaderStyle Width="90%" />
                            <ItemStyle Width="90%" />
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings EditFormType="Template" CaptionDataField="Rol" CaptionFormatString="">
                        <EditColumn UniqueName="EditCommandColumn1">
                        </EditColumn>
                        <FormTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# ((Container is GridEditFormInsertItem) ? "Agregar Nuevo Perfil" : "Editar Perfil Existente") %>'
                                Font-Size="Medium" Font-Bold="true" /><table style="width: 100%">
                                    <tr>
                                        <td style="width: 10%">
                                        </td>
                                        <td style="width: 90%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%">
                                            <asp:Label ID="Label2" runat="server" Text="Descripción" Font-Bold="true" />
                                        </td>
                                        <td style="width: 90%">
                                            <telerik:RadTextBox ID="edDescripcion" runat="server" Text='<%# Bind( "Descripcion") %>'
                                                EmptyMessage="Escriba la Descripción aquí" Width="50%" /><asp:RequiredFieldValidator
                                                    ID="rfDescripcion" runat="server" EnableClientScript="true" Display="Dynamic"
                                                    ErrorMessage="La Descripción no puede estar vacía" ControlToValidate="edDescripcion" />
                                        </td>
                                    </tr>
                                </table>
                            <asp:ImageButton ID="btnSave" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                AlternateText='<%# (Container is GridEditFormInsertItem) ? "Agregar Perfil" : "Actualizar Perfil" %>'
                                ImageUrl='<%# GetTelerikResourceUrl("Grid.Update.gif") %>' />&#160;&nbsp<asp:ImageButton
                                    ID="btnCancel" runat="server" CommandName="Cancel" AlternateText="Cancelar" ImageUrl='<%# GetTelerikResourceUrl("Grid.Cancel.gif") %>'
                                    CausesValidation="false" /></FormTemplate>
                    </EditFormSettings>
                </MasterTableView><ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                    <Selecting AllowRowSelect="True" />
                    <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                        DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                        DropHereToReorder="Suelte aquí para Re-Ordenar" />
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                </ClientSettings>
                <FilterMenu EnableTheming="True">
                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                </FilterMenu>
                <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
            </telerik:RadGrid></telerik:RadPageView>
        <telerik:RadPageView ID="pvPermisosModulos" runat="server">
            <telerik:RadGrid ID="rgPermisosModulos" runat="server" AllowFilteringByColumn="True"
                AllowSorting="True" AutoGenerateColumns="False" GridLines="Horizontal" Width="100%"
                Height="420px" OnNeedDataSource="rgPermisosModulos_NeedDataSource" OnUpdateCommand="rgPermisosModulos_UpdateCommand"
                OnDeleteCommand="rgPermisosModulos_DeleteCommand" OnInsertCommand="rgPermisosModulos_InsertCommand"
                ShowFooter="True" ShowStatusBar="True" PageSize="20" AllowMultiRowSelection="True"
                AllowPaging="True" ShowGroupPanel="True" OnItemDataBound="rgPermisosModulos_ItemDataBound">
                <HeaderContextMenu EnableTheming="True">
                    <CollapseAnimation Duration="200" Type="OutQuint" />
                </HeaderContextMenu>
                <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                <MasterTableView DataKeyNames="IdPermiso,IdModulo,IdRol" CommandItemDisplay="<%# Ingresar ? GridCommandItemDisplay.Top : GridCommandItemDisplay.None %>"
                    NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay Permisos de Modulos definidos."
                    GroupLoadMode="Client">
                    <CommandItemSettings AddNewRecordText="Agregar Permiso de Modulo" RefreshText="Volver a Cargar Datos" />
                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridEditCommandColumn AutoPostBackOnFilter="True" CancelText="Cancelar"
                            EditText="Editar" InsertText="Insertar" UpdateText="Grabar" ButtonType="ImageButton">
                            <HeaderStyle Width="5%" />
                            <ItemStyle Width="5%" />
                        </telerik:GridEditCommandColumn>
                        <telerik:GridButtonColumn ConfirmText="¿Esta seguro de que desea eliminar este permiso?"
                            ConfirmDialogType="RadWindow" ConfirmTitle="Eliminar Permiso Modulo" UniqueName="DeleteColumn"
                            ButtonType="ImageButton" CommandName="Delete">
                            <HeaderStyle Width="5%" />
                            <ItemStyle Width="5%" />
                        </telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="ModuloDescripcion" HeaderText="Módulo" UniqueName="column1"
                            FilterControlWidth="70%">
                            <HeaderStyle Width="26%" />
                            <ItemStyle Width="26%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="RolDescripcion" HeaderText="Perfil" UniqueName="column2"
                            FilterControlWidth="70%">
                            <HeaderStyle Width="26%" />
                            <ItemStyle Width="26%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="PermisoDescripcion" HeaderText="Permiso" UniqueName="column3"
                            FilterControlWidth="70%" Aggregate="Count" FooterText="Cantidad: ">
                            <HeaderStyle Width="26%" />
                            <ItemStyle Width="26%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn DataField="Estado" UniqueName="column4" HeaderText="Estado"
                            AllowFiltering="false">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnActivo" runat="server" CommandName='<%# (Eval("Eliminado").ToString() == "0" ? "Desactivar" : "Activar") %>'
                                    CommandArgument='<%# String.Format("{0},{1},{2}", Eval("IdPermiso").ToString(), Eval("IdModulo").ToString(), Eval("IdRol").ToString()) %>'
                                    OnClick="btnActivo_Click" ImageUrl='<%# (Eval("Eliminado").ToString() == "0" ? "~/Images/16/check2_16.png" : "~/Images/16/delete2_16.png") %>' /></ItemTemplate>
                            <HeaderStyle Width="12%" />
                            <ItemStyle Width="12%" />
                        </telerik:GridTemplateColumn>
                    </Columns>
                    <GroupByExpressions>
                        <telerik:GridGroupByExpression>
                            <SelectFields>
                                <telerik:GridGroupByField FieldAlias="Módulo" FieldName="ModuloDescripcion"></telerik:GridGroupByField>
                            </SelectFields>
                            <GroupByFields>
                                <telerik:GridGroupByField FieldAlias="Módulo" FieldName="ModuloDescripcion"></telerik:GridGroupByField>
                            </GroupByFields>
                        </telerik:GridGroupByExpression>
                        <telerik:GridGroupByExpression>
                            <SelectFields>
                                <telerik:GridGroupByField FieldAlias="Perfil" FieldName="RolDescripcion"></telerik:GridGroupByField>
                            </SelectFields>
                            <GroupByFields>
                                <telerik:GridGroupByField FieldAlias="Perfil" FieldName="RolDescripcion"></telerik:GridGroupByField>
                            </GroupByFields>
                        </telerik:GridGroupByExpression>
                    </GroupByExpressions>
                    <EditFormSettings EditFormType="Template" CaptionDataField="ModuloDescripcion" CaptionFormatString="">
                        <EditColumn UniqueName="EditCommandColumn1">
                        </EditColumn>
                        <FormTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# ((Container is GridEditFormInsertItem) ? "Agregar Nuevo Permiso de Modulo" : "Editar Permiso de Modulo Existente") %>'
                                Font-Size="Medium" Font-Bold="true" /><table style="width: 100%">
                                    <tr>
                                        <td style="width: 10%">
                                        </td>
                                        <td style="width: 90%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%">
                                            <asp:Label ID="Label2" runat="server" Text="Módulo" Font-Bold="true" />
                                        </td>
                                        <td style="width: 90%">
                                            <telerik:RadComboBox ID="edModulo" runat="server" DataSource='<%# GetModulos() %>'
                                                DataValueField="IdModulo" DataTextField="Descripcion" SelectedValue='<%# Bind( "IdModulo") %>'
                                                Width="50%" Enabled='<%# (Container is GridEditFormInsertItem) ? true : false %>'
                                                Font-Bold='<%# (Container is GridEditFormInsertItem) ? false : true %>' />
                                            <asp:RequiredFieldValidator ID="rfDescripcion" runat="server" EnableClientScript="true"
                                                Display="Dynamic" ErrorMessage="El Modulo no puede estar vacío" ControlToValidate="edModulo" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%">
                                            <asp:Label ID="Label3" runat="server" Text="Perfil" Font-Bold="true" />
                                        </td>
                                        <td style="width: 90%">
                                            <telerik:RadComboBox ID="edRol" runat="server" DataSource='<%# GetRoles() %>' DataTextField="Descripcion"
                                                DataValueField="IdRol" SelectedValue='<%# Bind( "IdRol") %>' Width="50%" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" EnableClientScript="true"
                                                Display="Dynamic" ErrorMessage="El Perfil no puede estar vacío" ControlToValidate="edRol" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%">
                                            <asp:Label ID="Label4" runat="server" Text="Permiso" Font-Bold="true" />
                                        </td>
                                        <td style="width: 90%">
                                            <telerik:RadComboBox ID="edPermiso" runat="server" DataSource='<%# GetPermisos() %>'
                                                DataTextField="Descripcion" DataValueField="IdPermiso" SelectedValue='<%# Bind( "IdPermiso") %>'
                                                Width="50%" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" EnableClientScript="true"
                                                Display="Dynamic" ErrorMessage="El Permiso no puede estar vacío" ControlToValidate="edPermiso" />
                                        </td>
                                    </tr>
                                </table>
                            <asp:ImageButton ID="btnSave" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                AlternateText='<%# (Container is GridEditFormInsertItem) ? "Agregar Permiso de Modulo" : "Actualizar Permiso de Modulo" %>'
                                ImageUrl='<%# GetTelerikResourceUrl("Grid.Update.gif") %>' />&#160;&nbsp<asp:ImageButton
                                    ID="btnCancel" runat="server" CommandName="Cancel" AlternateText="Cancelar" ImageUrl='<%# GetTelerikResourceUrl("Grid.Cancel.gif") %>'
                                    CausesValidation="false" /></FormTemplate>
                    </EditFormSettings>
                </MasterTableView><GroupingSettings CollapseTooltip="Contraer grupo" ExpandTooltip="Expandir grupo"
                    GroupContinuedFormatString="... continuación del grupo de la página anterior. "
                    GroupContinuesFormatString="El grupo continua en la siguiente página." GroupSplitDisplayFormat="Mostrando {0} de {1} elementos."
                    UnGroupTooltip="Arrastre fuera de la barra para desagrupar" ShowUnGroupButton="true" />
                <ClientSettings ReorderColumnsOnClient="True" AllowDragToGroup="True" AllowColumnsReorder="True">
                    <Selecting AllowRowSelect="True"></Selecting>
                    <Resizing AllowRowResize="True" AllowColumnResize="True" EnableRealTimeResize="True"
                        ResizeGridOnColumnResize="False"></Resizing>
                    <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                        DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                        DropHereToReorder="Suelte aquí para Re-Ordenar" />
                    <Scrolling AllowScroll="True" />
                </ClientSettings>
                <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
            </telerik:RadGrid>
        </telerik:RadPageView>
        <telerik:RadPageView ID="pvAutorizacionesModulos" runat="server">
            <telerik:RadGrid ID="rgAutorizacionesModulos" runat="server" AllowFilteringByColumn="True"
                AllowSorting="True" AutoGenerateColumns="False" GridLines="Horizontal" Width="100%"
                Height="420px" OnNeedDataSource="rgAutorizacionesModulos_NeedDataSource" OnUpdateCommand="rgAutorizacionesModulos_UpdateCommand"
                OnDeleteCommand="rgAutorizacionesModulos_DeleteCommand" OnInsertCommand="rgAutorizacionesModulos_InsertCommand"
                AllowPaging="True" ShowFooter="True" ShowStatusBar="True" PageSize="20" OnItemDataBound="rgAutorizacionesModulos_ItemDataBound">
                <HeaderContextMenu EnableTheming="True">
                    <CollapseAnimation Duration="200" Type="OutQuint" />
                </HeaderContextMenu>
                <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                <MasterTableView DataKeyNames="IdModulo,IdRol" CommandItemDisplay="<%# Ingresar ? GridCommandItemDisplay.Top : GridCommandItemDisplay.None %>"
                    NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay Autorizaciones de Modulos definidas."
                    GroupLoadMode="Client">
                    <CommandItemSettings AddNewRecordText="Agregar Autorización de Modulo" RefreshText="Volver a Cargar Datos" />
                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridEditCommandColumn AutoPostBackOnFilter="True" CancelText="Cancelar"
                            EditText="Editar" InsertText="Insertar" UpdateText="Grabar" ButtonType="ImageButton">
                            <HeaderStyle Width="5%" />
                            <ItemStyle Width="5%" />
                        </telerik:GridEditCommandColumn>
                        <telerik:GridButtonColumn ConfirmText="¿Esta seguro de que desea eliminar esta Autorización?"
                            ConfirmDialogType="RadWindow" ConfirmTitle="Eliminar Autorización Modulo" UniqueName="DeleteColumn"
                            ButtonType="ImageButton" CommandName="Delete">
                            <HeaderStyle Width="5%" />
                            <ItemStyle Width="5%" />
                        </telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="ModuloDescripcion" HeaderText="Módulo" UniqueName="column1"
                            FilterControlWidth="70%">
                            <HeaderStyle Width="39%" />
                            <ItemStyle Width="39%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="RolDescripcion" HeaderText="Perfil" UniqueName="column2"
                            FilterControlWidth="70%">
                            <HeaderStyle Width="39%" />
                            <ItemStyle Width="39%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn DataField="Estado" UniqueName="column4" HeaderText="Estado"
                            AllowFiltering="false">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnAMActivo" runat="server" CommandName='<%# (Eval("Eliminado").ToString() == "0" ? "Desactivar" : "Activar") %>'
                                    CommandArgument='<%# String.Format("{0},{1}", Eval("IdModulo").ToString(), Eval("IdRol").ToString()) %>'
                                    OnClick="btnAMActivo_Click" ImageUrl='<%# (Eval("Eliminado").ToString() == "0" ? "~/Images/16/check2_16.png" : "~/Images/16/delete2_16.png") %>' /></ItemTemplate>
                            <HeaderStyle Width="12%" />
                            <ItemStyle Width="12%" />
                        </telerik:GridTemplateColumn>
                    </Columns>
                    <GroupByExpressions>
                        <telerik:GridGroupByExpression>
                            <SelectFields>
                                <telerik:GridGroupByField FieldAlias="Módulo" FieldName="ModuloDescripcion"></telerik:GridGroupByField>
                            </SelectFields>
                            <GroupByFields>
                                <telerik:GridGroupByField FieldAlias="Módulo" FieldName="ModuloDescripcion"></telerik:GridGroupByField>
                            </GroupByFields>
                        </telerik:GridGroupByExpression>
                    </GroupByExpressions>
                    <EditFormSettings EditFormType="Template" CaptionDataField="ModuloDescripcion" CaptionFormatString="">
                        <EditColumn UniqueName="EditCommandColumn1">
                        </EditColumn>
                        <FormTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# ((Container is GridEditFormInsertItem) ? "Agregar Nueva Autorización de Modulo" : "Editar Autorización de Modulo Existente") %>'
                                Font-Size="Medium" Font-Bold="true" /><table style="width: 100%">
                                    <tr>
                                        <td style="width: 10%">
                                        </td>
                                        <td style="width: 90%">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%">
                                            <asp:Label ID="Label2" runat="server" Text="Módulo" Font-Bold="true" />
                                        </td>
                                        <td style="width: 90%">
                                            <telerik:RadComboBox ID="edModulo" runat="server" DataSource='<%# GetModulos() %>'
                                                DataValueField="IdModulo" DataTextField="Descripcion" SelectedValue='<%# Bind( "IdModulo") %>'
                                                Width="50%" Enabled='<%# (Container is GridEditFormInsertItem) ? true : false %>'
                                                Font-Bold='<%# (Container is GridEditFormInsertItem) ? false : true %>' />
                                            <asp:RequiredFieldValidator ID="rfDescripcion" runat="server" EnableClientScript="true"
                                                Display="Dynamic" ErrorMessage="El Modulo no puede estar vacío" ControlToValidate="edModulo" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%">
                                            <asp:Label ID="Label3" runat="server" Text="Perfil" Font-Bold="true" />
                                        </td>
                                        <td style="width: 90%">
                                            <telerik:RadComboBox ID="edRol" runat="server" DataSource='<%# GetRoles() %>' DataTextField="Descripcion"
                                                DataValueField="IdRol" SelectedValue='<%# Bind( "IdRol") %>' Width="50%" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" EnableClientScript="true"
                                                Display="Dynamic" ErrorMessage="El Perfil no puede estar vacío" ControlToValidate="edRol" />
                                        </td>
                                    </tr>
                                </table>
                            <asp:ImageButton ID="btnSave" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                AlternateText='<%# (Container is GridEditFormInsertItem) ? "Agregar Autorización de Modulo" : "Actualizar Autorización de Modulo" %>'
                                ImageUrl='<%# GetTelerikResourceUrl("Grid.Update.gif") %>' />&#160;&nbsp<asp:ImageButton
                                    ID="btnCancel" runat="server" CommandName="Cancel" AlternateText="Cancelar" ImageUrl='<%# GetTelerikResourceUrl("Grid.Cancel.gif") %>'
                                    CausesValidation="false" /></FormTemplate>
                    </EditFormSettings>
                </MasterTableView><GroupingSettings CollapseTooltip="Contraer grupo" ExpandTooltip="Expandir grupo"
                    GroupContinuedFormatString="... continuación del grupo de la página anterior. "
                    GroupContinuesFormatString="El grupo continua en la siguiente página." GroupSplitDisplayFormat="Mostrando {0} de {1} elementos."
                    UnGroupTooltip="Arrastre fuera de la barra para desagrupar" />
                <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True" AllowDragToGroup="True">
                    <Selecting AllowRowSelect="True" />
                    <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                        DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                        DropHereToReorder="Suelte aquí para Re-Ordenar" />
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                </ClientSettings>
                <FilterMenu EnableTheming="True">
                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                </FilterMenu>
                <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
            </telerik:RadGrid></telerik:RadPageView>
    </telerik:RadMultiPage>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgModulos">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgModulos" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rgPermisos">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgPermisos" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rgRoles">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgRoles" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rgPermisosModulos">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgPermisosModulos" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rgAutorizacionesModulos">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgAutorizacionesModulos" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>
