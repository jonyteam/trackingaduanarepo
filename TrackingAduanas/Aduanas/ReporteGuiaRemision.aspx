﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteGuiaRemision.aspx.cs" Inherits="ReporteGuiaRemision" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<%@ Register Assembly="Telerik.ReportViewer.WebForms, Version=6.2.12.1017, Culture=neutral, PublicKeyToken=a9d7983dfcc261be" Namespace="Telerik.ReportViewer.WebForms" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                    //<![CDATA[
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }

                function openWin() {
                    var oWnd = radopen("BuscarTramite.aspx", "RadWindow1");
                    oWnd.add_close(OnClientClose);
                }           
                
            </script>
        </telerik:RadScriptBlock>
        <telerik:RadMultiPage ID="mpReporte" runat="server">
            <telerik:RadPageView ID="RadPageView1" runat="server">
                <table width="60%">
                    <tr>
                        <td colspan="2">
                            <telerik:ReportViewer ID="ReportViewer1" runat="server" Width="100%" Height="500px">
                            </telerik:ReportViewer>
                        </td>
                         <td colspan="2">
                             &nbsp;</td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td style="height: 25px">
                        </td>
                    </tr>
                </table>
                <table width="60%">
                    <tr>
                        <td align="center">
                            <asp:ImageButton ID="btnBack" runat="server" CausesValidation="false" ImageUrl="~/Images/24/arrow_left_green_24.png"
                                OnClick="btnBack_Click" ToolTip="Regresar" />
                            <%--<asp:ImageButton ID="btnImprimir" runat="server" CausesValidation="false" ImageUrl="~/Images/24/printer2_24.png"
                                ToolTip="Imprimir" onclick="btnImprimir_Click" />--%>
                        </td>
                    </tr>
                </table>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </telerik:RadAjaxPanel>
</asp:Content>
