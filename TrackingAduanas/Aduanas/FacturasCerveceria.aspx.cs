﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;

using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.SessionState;
using System.Collections.Specialized;
using GrupoLis.Login;
using System.Text.RegularExpressions;
using System.Security.Principal;
using Telerik.Web;
using System.Security.Cryptography;

public partial class FacturasCerveceria : Utilidades.PaginaBase
{
    private NameValueCollection mParamethers;
    private GrupoLis.Login.Login logAppAduanas;
    private readonly InstruccionesAnexosRepository _iar = new InstruccionesAnexosRepository();
    private bool bandera;

    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Facturas Cerveceria Hondureña";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        mParamethers = ConfigurationManager.AppSettings;
        SetGridFilterMenu(rgInstrucciones.FilterMenu);
        rgInstrucciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgInstrucciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Facturas Cerveceria Hondureña", "Facturas Cervecería Hondureña");
            if (!(Ingresar || Modificar))
                redirectTo("Default.aspx");
            rgInstrucciones.ClientSettings.Scrolling.AllowScroll = true;
            rgInstrucciones.ClientSettings.Scrolling.UseStaticHeaders = true;
            rgInstrucciones.ShowHeader = true;
            rgInstrucciones.ShowFooter = true;
            LlenarObjetos();
        }
    }

    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }

    protected void LlenarObjetos()
    {
        try
        {
            conectar();
            ClientesBO cl = new ClientesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            cl.loadAllCamposClientesXPais("H");
            cmbClientes.DataSource = cl.TABLA;
            cmbClientes.DataBind();
            cmbClientes.SelectedValue = "8000338";
        }
        catch (Exception){ throw; }
        finally{ desconectar(); }
    }

    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        if (dpFechaInicio.SelectedDate != null && dpFechaFinal.SelectedDate != null)
        {
            btnGuardar.Visible = true;
            LlenarGridInstrucciones();
        }

    }

    private void LlenarGridInstrucciones()
    {
        try
        {
            var resp = _iar.LlenarGrid(dpFechaInicio.SelectedDate.ToString(), dpFechaFinal.SelectedDate.ToString(), cmbClientes.SelectedValue);
            if (resp.Count() != 0)
            {
                rgInstrucciones.DataSource = resp;
                rgInstrucciones.DataBind();
                rgInstrucciones.Visible = true;
                btnGuardar.Visible = true;
            }
        }
        catch (Exception ex){}
    }

    private void GuardarCambios()
    {
        foreach (GridDataItem item in rgInstrucciones.Items)
        {

            RadDateTimePicker fechaFactura = (RadDateTimePicker)item.FindControl("dtFechaFactura");
            RadTextBox txtPedidoCliente = (RadTextBox)item.FindControl("TxtPedidoCliente");
            CheckBox chkGuardar = (CheckBox)item.FindControl("chkSeleccionar");
            if (chkGuardar.Checked)
            {
                if (fechaFactura.SelectedDate != null && txtPedidoCliente.Text != "")
                {
                    var ia = new InstruccionesAnexo();
                    var registro = _iar.ExisteInstruccion(item["IdInstruccion"].Text);
                    if (registro == null)
                    {
                        ia.IdInstruccion = item["IdInstruccion"].Text;
                        ia.PedidoCliente = txtPedidoCliente.Text;
                        ia.FechaEntregaFactura = fechaFactura.SelectedDate;
                        ia.Fecha = DateTime.Now;
                        ia.Eliminado = false;
                        ia.IdUsuario = Convert.ToInt32(Session["IdUsuario"].ToString());
                        _iar.GuardarFacturaPedido(ia, false);
                    }
                    else
                    {
                        ia.PedidoCliente = txtPedidoCliente.Text;
                        ia.FechaEntregaFactura = fechaFactura.SelectedDate;
                        _iar.GuardarFacturaPedido(ia, true);
                    }
                }
                else
                {
                    registrarMensaje("El campo Fecha Factura y Pedido son obligatorios.");
                }
            }
        }
    }

    private void ConfigureExport()
    {
        String filename = "Instrucciones" + "_" + DateTime.Now.ToShortDateString();
        rgInstrucciones.GridLines = GridLines.Both;
        rgInstrucciones.ExportSettings.FileName = filename;
        rgInstrucciones.ExportSettings.IgnorePaging = true;
        rgInstrucciones.ExportSettings.OpenInNewWindow = false;
        rgInstrucciones.ExportSettings.ExportOnlyData = true;
    }

    protected void rgInstrucciones_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        LlenarGridInstrucciones();
    }

    protected void btnGuardar_OnClick(object sender, ImageClickEventArgs e)
    {
        GuardarCambios();
    }

    protected void rgInstrucciones_OnItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            if (rgInstrucciones.Items.Count <= 0 & e.CommandName == RadGrid.ExportToExcelCommandName)
                e.Canceled = true;
            else if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                ConfigureExport();
            }
        }
        catch (Exception)
        { }
    }
}