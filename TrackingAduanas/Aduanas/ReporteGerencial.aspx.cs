﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.OleDb;
using System.Text;
using System.Configuration;
using Telerik.Web.UI;
using System.Data;
using System.Net.Mail;
using System.Net.Mime;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.SessionState;
using System.Collections.Specialized;
using GrupoLis.Login;
using System.Text.RegularExpressions;
using System.Security.Principal;
using Telerik.Web;
using System.Security.Cryptography;
public partial class ReporteGerencial : Utilidades.PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgInstrucciones.FilterMenu);
        rgInstrucciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgInstrucciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        SetGridFilterMenu(rgInstrucciones2.FilterMenu);
        rgInstrucciones2.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgInstrucciones2.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Gerencial", "Reporte Gerencial");
            cargarDatosInicio();
        }
    }

    protected void cargarDatosInicio()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            ClientesBO cl = new ClientesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (u.CODPAIS == "H")
                c.loadPaisesHojaRuta();
            else
                c.loadAllCampos("PAISES", u.CODPAIS);
            cmbPais.DataSource = c.TABLA;
            cmbPais.DataBind();
            dpFechaInicio.SelectedDate = DateTime.Now;
            dpFechaFinal.SelectedDate = DateTime.Now;
        }
        catch { }
    }

    public override bool CanGoBack { get { return false; } }

    private void Salir()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("top.location.href = LoginTracking.aspx;");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Salir", sb.ToString(), false);
    }

    #region Gestion Embarque
    protected void rgInstrucciones_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridInstrucciones();
    }

    private void llenarGridInstrucciones()
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            string[] campo = rbFiltrar.SelectedValue.Split("".ToCharArray());
            i.loadInstruccionReporteGerencial(dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"), cmbPais.SelectedValue, rbFiltrar.SelectedValue, campo[0], "", "", "");
            if (i.totalRegistros > 0)
            //i.loadInstruccionEmbarqueDiario(dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"), cmbCliente.SelectedValue);
            //InstruccionesBO ins = new InstruccionesBO(logApp);
            //GestionCargaBO gc = new GestionCargaBO(logApp);
            ////DataTable dt = new DataTable();
            //UsuariosBO u = new UsuariosBO(logApp);
            //if (User.IsInRole("Administradores"))
            //    ins.loadInstruccionesAll();
            //else if (User.IsInRole("Jefes Aduana"))
            //{
            //    u.loadUsuarioLogin(User.Identity.Name);
            //    if (u.totalRegistros > 0)
            //        ins.loadInstruccionesAllXAduana(u.CODIGOADUANA);
            //}
            //else if (User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT"))
            //{
            //    string idU = Session["IdUsuario"].ToString();
            //    u.loadUsuarioLogin(User.Identity.Name);
            //    if (u.totalRegistros > 0)
            //        ins.loadInstruccionesAllXAduanaUsuario(u.CODIGOADUANA, idU);
            //}
            {
                rgInstrucciones.DataSource = i.TABLA;
                try
                {
                    rgInstrucciones.DataBind();
                }
                catch { }

            }
            else
            {
                rgInstrucciones.DataSource = new string[] { };
                rgInstrucciones.DataBind();
            }
        }
        catch (Exception)
        {
            try
            {
                rgInstrucciones.DataSource = new string[] { };
                rgInstrucciones.DataBind();
            }
            catch { }
        }
        finally
        {
            desconectar();
        }
    }

    protected void rgInstrucciones_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();
            if (rgInstrucciones.Items.Count <= 0 & e.CommandName == RadGrid.ExportToExcelCommandName)
                e.Canceled = true;
            else if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                ConfigureExport();
            }
        }
        catch (Exception)
        { }
    }

    private void ConfigureExport()
    {
        rgInstrucciones.AllowFilteringByColumn = false;
        String filename = "GestionCarga" + "_" + DateTime.Now.ToShortDateString();
        rgInstrucciones.GridLines = GridLines.Both;
        rgInstrucciones.ExportSettings.FileName = filename;
        rgInstrucciones.ExportSettings.IgnorePaging = true;
        rgInstrucciones.ExportSettings.OpenInNewWindow = false;
        rgInstrucciones.ExportSettings.ExportOnlyData = true;
    }
    #endregion

    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        if (rbFiltrar.SelectedValue.Length > 0)
        {
            if (dpFechaInicio.SelectedDate <= dpFechaFinal.SelectedDate)
            {
                if (rbFiltrar.SelectedValue != "5")
                {
                    llenarGridInstrucciones();
                    rgInstrucciones.Visible = true;
                    rgInstrucciones2.Visible = false;
                }
                else
                {
                    llenarGridInstrucciones2();
                    rgInstrucciones.Visible = false;
                    rgInstrucciones2.Visible = true;
                }
            }
            else
                registrarMensaje2("Fecha inicio no puede ser mayor que fecha final");
        }
        else
            registrarMensaje2("Seleccione una opcion para generar el reporte");
    }

    private void registrarMensaje2(string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("alert('" + mensaje + "');");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "mensaje", sb.ToString(), false);
    }

    protected void rgInstrucciones_Init(object sender, System.EventArgs e)
    {
        GridFilterMenu menu = rgInstrucciones.FilterMenu;
        menu.Items.RemoveAt(rgInstrucciones.FilterMenu.Items.Count - 2);
    }

    #region Gerencial2
    protected void rgInstrucciones2_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (!e.IsFromDetailTable)
        {
            llenarGridInstrucciones2();
        }
    }

    private void llenarGridInstrucciones2()
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            if (cmbDescripcion1.SelectedValue != "Seleccione")
            {
                string campo = "", agrupar = "";
                switch (cmbDescripcion1.SelectedValue)
                {
                    case "1": campo = "A.NombreAduana AS Descripcion1, A.CodigoAduana AS Codigo1"; agrupar = "A.NombreAduana, A.CodigoAduana"; codigoUno.Value = "A.CodigoAduana AS Codigo1"; agruparUno.Value = "A.CodigoAduana"; break;
                    case "2": campo = "C.Nombre AS Descripcion1, C.CodigoSAP AS Codigo1"; agrupar = "c.Nombre, c.CodigoSAP"; codigoUno.Value = "C.CodigoSAP AS Codigo1"; agruparUno.Value = "C.CodigoSAP"; break;
                    case "3": campo = "C2.Descripcion AS Descripcion1, C2.Codigo AS Codigo1"; agrupar = "C2.Descripcion, C2.Codigo"; codigoUno.Value = "C2.Codigo AS Codigo1"; agruparUno.Value = "C2.Codigo"; break;
                }

                i.loadInstruccionReporteGerencial(dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"), cmbPais.SelectedValue, campo, agrupar, "", "", "");
                if (cmbDescripcion2.SelectedValue != "Seleccione")
                    rgInstrucciones2.MasterTableView.HierarchyDefaultExpanded = true;
                else
                    rgInstrucciones2.MasterTableView.HierarchyDefaultExpanded = false;
                rgInstrucciones2.DataSource = i.TABLA;
                try
                {
                    rgInstrucciones2.DataBind();
                }
                catch (Exception)
                {
                    
                }
               
            }
            else
            {
                rgInstrucciones2.DataSource = new string[] { };
                rgInstrucciones2.DataBind();
            }
        }
        catch (Exception)
        { }
        finally
        {
            desconectar();
        }
    }

    protected void rgInstrucciones2_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            string codigo1 = "";
            if (cmbDescripcion2.SelectedValue != "Seleccione")
            {
                switch (e.DetailTableView.DataMember)
                {

                    case "ClientesOC":
                        {
                            string campo = "", agrupar = "";
                            switch (cmbDescripcion2.SelectedValue)
                            {
                                case "1": campo = codigoUno.Value.Trim() + ", A.NombreAduana AS Descripcion2, A.CodigoAduana AS Codigo2"; agrupar = agruparUno.Value.Trim() + ", A.NombreAduana, A.CodigoAduana"; codigoDos.Value = "A.CodigoAduana AS Codigo2"; agruparDos.Value = "A.CodigoAduana"; break;
                                case "2": campo = codigoUno.Value.Trim() + ", C.Nombre AS Descripcion2, C.CodigoSAP AS Codigo2"; agrupar = agruparUno.Value.Trim() + ", C.Nombre, C.CodigoSAP"; codigoDos.Value = "C.CodigoSAP AS Codigo2"; agruparDos.Value = "C.CodigoSAP"; break;
                                case "3": campo = codigoUno.Value.Trim() + ", C2.Descripcion AS Descripcion2, C2.Codigo AS Codigo2"; agrupar = agruparUno.Value.Trim() + ", C2.Descripcion, C2.Codigo"; codigoDos.Value = "C2.Codigo AS Codigo2"; agruparDos.Value = "C2.Codigo"; break;
                            }
                            codigo1 = dataItem["Codigo1"].Text;
                            i.loadInstruccionReporteGerencial(dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"), cmbPais.SelectedValue, campo, agrupar, "1", codigo1, "");
                            if (cmbDescripcion2.SelectedValue != "Seleccione")
                            {
                                rgInstrucciones2.MasterTableView.HierarchyDefaultExpanded = true;
                                if (cmbDescripcion3.SelectedValue != "Seleccione")
                                    e.DetailTableView.HierarchyDefaultExpanded = true;
                                else
                                    e.DetailTableView.HierarchyDefaultExpanded = false;
                            }
                            else
                            {
                                rgInstrucciones2.MasterTableView.HierarchyDefaultExpanded = false;
                                e.DetailTableView.HierarchyDefaultExpanded = false;
                            }
                            e.DetailTableView.DataSource = i.TABLA;
                            break;
                        }

                    case "Detalle2":
                        {
                            if (cmbDescripcion3.SelectedValue != "Seleccione")
                            {
                                string campo = "", agrupar = "";
                                switch (cmbDescripcion3.SelectedValue)
                                {
                                    case "1": campo = codigoDos.Value.Trim() + ", A.NombreAduana AS Descripcion3, A.CodigoAduana AS Codigo3"; agrupar = agruparDos.Value.Trim() + ", A.NombreAduana, A.CodigoAduana"; break;
                                    case "2": campo = codigoDos.Value.Trim() + ", C.Nombre AS Descripcion3, C.CodigoSAP AS Codigo3"; agrupar = agruparDos.Value.Trim() + ", C.Nombre, C.CodigoSAP"; break;
                                    case "3": campo = codigoDos.Value.Trim() + ", C2.Descripcion AS Descripcion3, C2.Codigo AS Codigo3"; agrupar = agruparDos.Value.Trim() + ", C2.Descripcion, C2.Codigo"; break;
                                }
                                string codigo2 = dataItem["Codigo2"].Text;
                                codigo1 = dataItem["Codigo1"].Text;
                                i.loadInstruccionReporteGerencial(dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"), cmbPais.SelectedValue, campo, agrupar, "2", codigo1, codigo2);
                                //if (cmbDescripcion3.SelectedValue != "Seleccione")
                                //{
                                //    //rgInstrucciones2.MasterTableView.HierarchyDefaultExpanded = true;
                                //    e.DetailTableView.HierarchyDefaultExpanded = true;
                                //}
                                //else
                                //{
                                //    //rgInstrucciones2.MasterTableView.HierarchyDefaultExpanded = false;
                                //    e.DetailTableView.HierarchyDefaultExpanded = false;
                                //}
                                e.DetailTableView.DataSource = i.TABLA;
                            }
                            else
                            {
                                rgInstrucciones2.MasterTableView.HierarchyDefaultExpanded = false;
                                e.DetailTableView.HierarchyDefaultExpanded = false;
                                //e.DetailTableView.DataSource = new string[] { };                                
                            }
                            break;
                        }
                }
            }
            else
            {

                rgInstrucciones2.MasterTableView.HierarchyDefaultExpanded = false;
                e.DetailTableView.HierarchyDefaultExpanded = false;
                //e.DetailTableView.DataSource = new string[] { };
            }
        }
        catch (Exception)
        { }
        finally
        {
            desconectar();
        }
    }

    protected void rgInstrucciones2_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == RadGrid.ExportToExcelCommandName)
            ConfigureExport2();
    }

    private void ConfigureExport2()
    {
        String filename = "Instrucciones " + DateTime.Now.ToShortDateString();
        rgInstrucciones2.ExportSettings.FileName = filename;
        rgInstrucciones2.ExportSettings.ExportOnlyData = true;
        rgInstrucciones2.ExportSettings.IgnorePaging = true;
        rgInstrucciones2.ExportSettings.OpenInNewWindow = true;
        rgInstrucciones2.MasterTableView.ExportToExcel();

    }
    #endregion

    protected void rbFiltrar_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            if (rbFiltrar.SelectedValue == "5")
            {
                c.loadAllCampos("REPORTE");
                cmbDescripcion1.DataSource = c.TABLA;
                cmbDescripcion1.DataBind();
                cmbDescripcion1.Items.Insert(0, new RadComboBoxItem("Seleccione...", "Seleccione"));
                cmbDescripcion1.Visible = true;
            }
            else
            {
                cmbDescripcion1.Visible = false;
                cmbDescripcion2.Visible = false;
                cmbDescripcion3.Visible = false;
            }
        }
        catch { }
    }

    protected void cmbDescripcion1_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            if (cmbDescripcion1.SelectedValue != "Seleccione")
            {
                c.loadCodigosXCategoriaCodigosNOT("REPORTE", cmbDescripcion1.SelectedValue);
                cmbDescripcion2.DataSource = c.TABLA;
                cmbDescripcion2.DataBind();
                cmbDescripcion2.Items.Insert(0, new RadComboBoxItem("Seleccione...", "Seleccione"));
                cmbDescripcion2.Visible = true;
                cmbDescripcion3.Visible = false;
            }
            else
            {
                cmbDescripcion2.Visible = false;
                cmbDescripcion3.Visible = false;
            }
        }
        catch { }
    }

    protected void cmbDescripcion2_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            if (cmbDescripcion2.SelectedValue != "Seleccione")
            {
                c.loadCodigosXCategoriaCodigosNOT("REPORTE", cmbDescripcion1.SelectedValue + "," + cmbDescripcion2.SelectedValue);
                cmbDescripcion3.DataSource = c.TABLA;
                cmbDescripcion3.DataBind();
                cmbDescripcion3.Items.Insert(0, new RadComboBoxItem("Seleccione...", "Seleccione"));
                cmbDescripcion3.Visible = true;
            }
            else
            {
                cmbDescripcion3.Visible = false;
            }
        }
        catch { }
    }
}