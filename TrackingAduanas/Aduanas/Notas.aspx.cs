﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using ObjetosDto;
using ObjetosDto.Data;
using Telerik.Web.UI;

public partial class FcAseguramientoDeServicio : Utilidades.PaginaBase
{
    readonly AduanasDataContext _aduanasDc = new AduanasDataContext();
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Notas";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        RadWindowManager1.Windows.Clear();
        SetGridFilterMenu(rgAsistenteOperaciones.FilterMenu);
        rgAsistenteOperaciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgAsistenteOperaciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Notas", "Notas");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
        }

        if (SessionFileShare.SessionCreada(this))
        {
            try
            {
                var respuesta = SessionFileShare.GetRespuesta(this);
                foreach (var res in respuesta)
                {
                    var estado = res.Estado;
                    if (estado != 1) continue;
                    var idSistema = res.IdSistema;
                    foreach (var docResp in res.DocumentoResponseVms)
                    {
                        //var documento = docResp.Documento;
                        var idDocumento = docResp.IdDocumento.ToString();
                        var noReferencia = docResp.NoReferencia;

                        var pfc =
                            _aduanasDc.ProcesoFlujoCarga.SingleOrDefault(
                                p => p.IdInstruccion == idSistema && p.Eliminado == false);
                        if (pfc == null) continue;
                        switch (idDocumento)
                        {
                            case "1":
                                //  pfc.Correlativo = noReferencia;
                                pfc.Digitalizado = "1";
                                var b = new Bitacora
                                {
                                    IdUsuario = Convert.ToDecimal(Session["IdUsuario"]),
                                    Accion = "Boletin cargado",
                                    IdDocumento = idSistema,
                                    Fecha = DateTime.Now,
                                };
                                _aduanasDc.Bitacora.InsertOnSubmit(b);
                                break;
                            case "2":
                                pfc.ComprobantePago = noReferencia;
                                var b2 = new Bitacora
                                {
                                    IdUsuario = Convert.ToDecimal(Session["IdUsuario"]),
                                    Accion = "Comprobante de pago cargado",
                                    IdDocumento = idSistema,
                                    Fecha = DateTime.Now,
                                };
                                _aduanasDc.Bitacora.InsertOnSubmit(b2);
                                break;
                        }
                    }
                    _aduanasDc.SubmitChanges();
                    Timer1.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                SessionFileShare.DestruirFileShare(this);
            }
        }
    }

    private bool Anular { get { return tienePermiso("ANULAR"); } }
    private bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    private bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
    {
        if (e.Argument != "Rebind") return;
        rgAsistenteOperaciones.Rebind();
        Timer1.Enabled = true;
    }


    #region Aseguramiento De Servicio
    protected void rgAsistenteOperaciones_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        LlenarGrid();
    }

    private void LlenarGrid()
    {
        try
        {
            var query = _aduanasDc.ProcesoFlujoCarga.Where(p => p.Eliminado == false)
                .Join(_aduanasDc.Instrucciones.Select(i => new { i.CodEstado, i.IdInstruccion, i.CodRegimen, i.NumeroFactura, i.IdOficialCuenta ,i.FechaFinalFlujo })
                .Where(i => i.CodEstado != "100" & (i.FechaFinalFlujo > DateTime.Now.AddDays(-5) || i.FechaFinalFlujo==null)),
                 p => p.IdInstruccion, i =>
                 i.IdInstruccion, (p, i) => new { p, i })
                           .Join(_aduanasDc.Codigos.Where(c => c.Eliminado == '0' && c.Categoria == "ESTADO FLUJO CARGA AUTOMATICO"),
                     pit => pit.p.CodEstado, c => c.Codigo1, (pit, c) => new { pit, c })
             
                .Select(pi => new ProcesoFlujoCargaGridVm
                {
                    Id = pi.pit.p.Id,
                    IdInstruccion = pi.pit.p.IdInstruccion,
                    Cliente = pi.pit.p.Cliente,
                    Proveedor = pi.pit.p.Proveedor,
                    CodRegimen = pi.pit.i.CodRegimen,
                    NoFactura = pi.pit.i.NumeroFactura,
                    Descripcion = pi.c.Descripcion,
                    CodigoAduana=pi.pit.p.CodigoAduana,
                  

                })
                .ToList();

            if (User.IsInRole("Administradores"))
            {
                              
              
            }
          
            else
          
            if (User.IsInRole("Oficial de Cuenta")||User.IsInRole("Inplant"))
            {

               //cuando es Oficial de Cuenta el CodigoAduana es condicion
                var idUsuario = _aduanasDc.Usuarios.Where(u => u.IdUsuario == Convert.ToDecimal(Session["IdUsuario"].ToString()) && u.Eliminado == '0')
                    .Select(u => u.IdUsuario).FirstOrDefault();
                query = query.Where(q => q.IdOficialCuenta == idUsuario).ToList();
            }
           
            else
            {
                var aduana = _aduanasDc.Usuarios.Where(u => u.IdUsuario == Convert.ToDecimal(Session["IdUsuario"].ToString()) && u.Eliminado == '0')
                      .Select(u => u.CodigoAduana).FirstOrDefault();
                query = query.Where(q => q.CodigoAduana == aduana).ToList();
            }


            rgAsistenteOperaciones.DataSource = query;
        }
        catch { }
    }

    protected void rgAsistenteOperaciones_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            var editedItem = e.Item as GridEditableItem;
            if (e.CommandName == "Subir")
            {
                if (editedItem != null)
                {
                    var idInstruccion =
                        editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdInstruccion"].ToString();
                    var fact = editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["NoFactura"];
                    var factura = "s/f";
                    if (fact != null)
                        factura = fact.ToString();

                    var documentoRequestVms = new List<DocumentoRequestVm>
                    {
                        
                        new DocumentoRequestVm(2,"Comprobante de pago"),
                        new DocumentoRequestVm(1,"Boletín y Soporte"),
                   
                    };

                    Timer1.Enabled = false;
                    //SessionFileShare.Create(userId: Session["IdUsuario"].ToString(), userName: User.Identity.Name, idDocumento: 1, documento: "boletin", idSistema: idInstruccion, page: this, identificador: factura);
                    SessionFileShare.Create(userId: Session["IdUsuario"].ToString(), userName: User.Identity.Name, documentoDtos: documentoRequestVms, idSistema: idInstruccion, page: this, identificador: factura);
                 //   SessionFileShare.
                }




            }
            if (e.CommandName == "Descargar")
            {
                if (editedItem != null)
                {
                    var idInstruccion = editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdInstruccion"].ToString();
                    Timer1.Enabled = false;
                    SessionFileShare.CreateDownload(idSistema: idInstruccion, page: this);
                }
            }

             if (e.CommandName == "GuardarNota")
             {
                 var idInstruccion = editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdInstruccion"].ToString();
                 var Descripcion = editedItem["Descripcion"].Text;
                 var nota = editedItem.FindControl("txtNotas") as RadTextBox;
              
                    var tfc = new NotasInstrucciones
                    {
                        IdInstruccion = idInstruccion,
                        Estado = Descripcion,
                        Fecha = DateTime.Now,
                        Observacion_ = nota.Text,     
                        IdUsuario = Convert.ToDecimal(Session["IdUsuario"]),
                        Eliminado = false,
                    };
                    _aduanasDc.NotasInstrucciones.InsertOnSubmit(tfc);
                    _aduanasDc.SubmitChanges();
                    nota.Text = "";
                    registrarMensaje("Nota Agregada Exitosamente");
            }  
            if (e.CommandName == "Enviar")
            {
                if (editedItem != null)
                {
                    Timer1.Enabled = false;
                    var id = Convert.ToDecimal(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"]);
                    var fecha = (RadDatePicker)editedItem.FindControl("dtFechaSellado");
                    var hora = (RadTimePicker)editedItem.FindControl("dtHoraSellado");
                    if (fecha.SelectedDate.HasValue)
                    {
                        if (hora.SelectedDate.HasValue)
                        {
                            var query = _aduanasDc.ProcesoFlujoCarga.SingleOrDefault(p => p.Eliminado == false && p.Id == id);
                            if (query != null)
                            {
                                if (!String.IsNullOrEmpty(query.Correlativo) && !String.IsNullOrEmpty(query.ComprobantePago))
                                {
                                  //  query.CodEstado = query.IdInstruccion.Trim().Substring(1, 1) == "r" ? "2" : "1";

                                    query.CodEstado = query.IdInstruccion.Trim().Substring(1, 1) == "r" ? query.CodEstado : "1";
                                    query.RTClienteEnviado = true;
                                    var tfc = new TiemposFlujoCarga
                                    {
                                        IdInstruccion = query.IdInstruccion,
                                        IdEstado = "1",
                                        Fecha = Convert.ToDateTime(fecha.SelectedDate.Value.ToShortDateString() + " " + hora.SelectedDate.Value.ToShortTimeString()),
                                        FechaIngreso = DateTime.Now,
                                        Observacion = "Pago confirmado por aseguramiento de servicio",
                                        Eliminado = false,
                                        IdUsuario = Convert.ToDecimal(Session["IdUsuario"])
                                    };
                                    _aduanasDc.TiemposFlujoCarga.InsertOnSubmit(tfc);

                                    var b = new Bitacora
                                    {
                                        IdUsuario = Convert.ToDecimal(Session["IdUsuario"]),
                                        Accion = "Pago confirmado por aseguramiento de servicio",
                                        IdDocumento = query.IdInstruccion,
                                        Fecha = DateTime.Now,
                                    };
                                    _aduanasDc.Bitacora.InsertOnSubmit(b);

                                    _aduanasDc.SubmitChanges();
                                    RegistrarMensaje2("Registro enviado exitosamente");
                                    rgAsistenteOperaciones.Rebind();
                                    Timer1.Enabled = true;
                                }
                                else
                                {
                                    RegistrarMensaje2("Primero subir correlativo y comprobante de pago");
                                }
                            }
                            else
                            {
                                RegistrarMensaje2("Registro presenta error...favor comunique al administrador del sistema");
                            }
                        }
                        else
                            registrarMensaje("Hora no puede estar vacía");
                    }
                    else
                        registrarMensaje("Fecha no puede estar vacía");
                }
            }
            if (rgAsistenteOperaciones.Items.Count >= 1 &
                (e.CommandName == RadGrid.ExportToExcelCommandName ||
                 e.CommandName == RadGrid.ExportToCsvCommandName))
                ConfigureExport("");
            else if (rgAsistenteOperaciones.Items.Count < 1 &
                     (e.CommandName == RadGrid.ExportToExcelCommandName ||
                      e.CommandName == RadGrid.ExportToCsvCommandName))
                e.Canceled = true;
        }
        catch
        {
        }
    }

    private void ConfigureExport(string descripcion)
    {
        System.Web.HttpPostedFileBase fileBase;
        var filename = descripcion + " " + DateTime.Now.ToShortDateString();
        rgAsistenteOperaciones.ExportSettings.FileName = filename;
        rgAsistenteOperaciones.ExportSettings.ExportOnlyData = true;
        rgAsistenteOperaciones.ExportSettings.IgnorePaging = true;
        rgAsistenteOperaciones.ExportSettings.OpenInNewWindow = true;
        rgAsistenteOperaciones.MasterTableView.ExportToExcel();
    }

    protected void rgAsistenteOperaciones_Init(object sender, EventArgs e)
    {
        var menu = rgAsistenteOperaciones.FilterMenu;
        menu.Items.RemoveAt(rgAsistenteOperaciones.FilterMenu.Items.Count - 2);
    }

    private void RegistrarMensaje2(string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("alert('" + mensaje + "');");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "mensaje", sb.ToString(), false);
    }
    #endregion
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        rgAsistenteOperaciones.Rebind();
    }

    protected void btnCrearInstruccion_Click(object sender, EventArgs e)
    {
        AbrirVentanaInstruccion();
    }

    private void AbrirVentanaInstruccion()
    {
        var window1 = new RadWindow
        {
            NavigateUrl = "FcCrearInstruccionFaucaCliente.aspx",
            VisibleOnPageLoad = true,
            ID = "Instruccion",
            Width = 900,
            Height = 350,
            Animation = WindowAnimation.FlyIn,
            DestroyOnClose = true,
            VisibleStatusbar = false,
            Behaviors = WindowBehaviors.Close,
            Modal = true,
            OnClientClose = "OnClientClose"
        };
        RadWindowManager1.Windows.Add(window1);
    }
    protected void rgAsistenteOperaciones_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
    {
    
        try
        {       GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
                string Hoja = dataItem["IdInstruccion"].Text;
                var query = _aduanasDc.NotasInstrucciones.Where(p => p.Eliminado == false & p.IdInstruccion == Hoja.Trim())
                    .Join(_aduanasDc.Usuarios,
                    p=>p.IdUsuario,
                    u=>u.IdUsuario,
                     (p, u) => new { p, u })
                .Select(pi => new NotasVm
                {
                   IdInstruccion=pi.p.IdInstruccion,
                    Fecha=pi.p.Fecha,
                     Observacion=pi.p.Observacion_,
                      Estado=pi.p.Estado,
                       Usuario=pi.u.Usuario1,
                      
                })
                .ToList();
          
            e.DetailTableView.DataSource = query;


        }
        catch (Exception)
        {

        }
        finally { desconectar(); }

    }
    
}