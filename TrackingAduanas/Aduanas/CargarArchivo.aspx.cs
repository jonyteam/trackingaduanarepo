﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;

using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;

public partial class CargarArchivo : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Cargar Documentos";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        RadWindowManager1.Windows.Clear();
        SetGridFilterMenu(rgIngresos.FilterMenu);
        rgIngresos.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgIngresos.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Cargar Documentos", "");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void SetVisible(TableCellCollection cells, String commandName, bool visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is ImageButton)
                {
                    ImageButton btn = (ImageButton)control;

                    if (btn.CommandName == commandName)
                        btn.Visible = visible;
                }
            }
        }
    }

    protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
    {
        if (e.Argument == "Rebind")
        {
            rgIngresos.Rebind();
        }
    }

    #region Ingresos
    protected void rgIngresos_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridIngresos();
    }

    private void llenarGridIngresos()
    {
        try
        {
            conectar();

            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            if (User.IsInRole("Administradores"))
                i.loadInstruccionesCargarArchivo();
            else if (User.IsInRole("Administrador Pais"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                i.loadInstruccionesCargarArchivoXPais(u.CODPAIS);
            }
            else if (User.IsInRole("Operaciones") || User.IsInRole("Inplant"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.totalRegistros > 0)
                {
                    string idU = Session["IdUsuario"].ToString();
                    if (User.Identity.Name == "davila" || User.Identity.Name == "spolanco"
                        || User.Identity.Name == "jgarcia" || User.Identity.Name == "ncontreras" ||
                        User.Identity.Name == "afonseca" || User.Identity.Name == "cmorales")
                        //i.loadInstruccionesXPaisUsuario(u.CODPAIS, idU);
                        i.loadInstruccionesCargarArchivoXPais(u.CODPAIS);
                    else
                        i.loadInstruccionesCargarArchivoXPais(u.CODPAIS);
                }
            }
            else if (User.IsInRole("Jefes Aduana"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.totalRegistros > 0)
                    i.loadInstruccionesCargarArchivoXAduana(u.CODIGOADUANA);
            }
            else if (User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT"))
            {
                string idU = Session["IdUsuario"].ToString();
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.totalRegistros > 0)
                    if (u.CODPAIS == "H")
                        //i.loadInstruccionXAduanaUsuario(u.CODIGOADUANA, idU);
                        i.loadInstruccionesCargarArchivoXAduana(u.CODIGOADUANA);
                    else
                        i.loadInstruccionesCargarArchivoXPais(u.CODPAIS);
            }
            rgIngresos.DataSource = i.TABLA;
            rgIngresos.DataBind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void rgIngresos_Init(object sender, System.EventArgs e)
    {
        //GridFilterMenu menu = rgIngresos.FilterMenu;
        //menu.Items.RemoveAt(rgIngresos.FilterMenu.Items.Count - 2);

        GridFilterMenu filterMenu = rgIngresos.FilterMenu;

        int currentItemIndex = 0;
        while (currentItemIndex < filterMenu.Items.Count)
        {
            RadMenuItem item = filterMenu.Items[currentItemIndex];
            if (item.Text.Contains("Empty") || item.Text.Contains("Null"))
            {
                filterMenu.Items.Remove(item);
            }
            else currentItemIndex++;
        }
    }

    protected void rgIngresos_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            GridDataItem editedItem = e.Item as GridDataItem;
            if (e.CommandName == "Tiempo")
            {
                cargarWindow("Prueba", "~/SharePointUpload.aspx?codPaisOrigen=" + e.Item.Cells[3].Text, 700, 400);
                
            }
            else if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
                ConfigureExport();
        }
        catch { }
    }

    private void ConfigureExport()
    {
        String filename = "Ingresos_" + DateTime.Now.ToShortDateString();
        rgIngresos.GridLines = GridLines.Both;
        rgIngresos.ExportSettings.FileName = filename;
        rgIngresos.ExportSettings.IgnorePaging = true;
        rgIngresos.ExportSettings.OpenInNewWindow = false;
        rgIngresos.ExportSettings.ExportOnlyData = true;
    }

    protected void cargarWindow(String titleBar, string navigateURL, int width, int heigth)
    {
        RadWindowManager1.Windows.Clear();
        RadWindow window1 = new RadWindow();
        window1.NavigateUrl = navigateURL;
        window1.VisibleOnPageLoad = true;
        window1.Title = titleBar;
        if (width > 0)
        {
            window1.Width = width;
            window1.Height = heigth;
            window1.Behaviors = WindowBehaviors.Move | WindowBehaviors.Pin | WindowBehaviors.Close;
        }
        else
        {
            window1.InitialBehaviors = WindowBehaviors.Maximize;
        }

        window1.Animation = WindowAnimation.FlyIn;
        window1.Modal = true;
        //window1.OnClientClose = "OnClientClose";
        window1.DestroyOnClose = true;
        window1.VisibleStatusbar = false;
        RadWindowManager1.Windows.Add(window1);
    }
    #endregion
}