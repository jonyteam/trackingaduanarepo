﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.OleDb;

using Telerik.Web.UI;
//using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using System.Collections;

public partial class GenerarArchivoSAPDinant : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "ReporteHagamodaPromoda";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //   ((SiteMaster)Master).SetTitulo("Reporte Hagamoda/Promoda","");
        }
    }

    protected bool Consultar { get { return tienePermiso("Consultar"); } }




    private void ConfigureExport()
    {
        String filename = "Reporte_" + DateTime.Now.ToShortDateString();
        RadGrid1.ExportSettings.FileName = filename;
        RadGrid1.ExportSettings.ExportOnlyData = true;
        RadGrid1.ExportSettings.IgnorePaging = true;
        RadGrid1.ExportSettings.OpenInNewWindow = true;
        RadGrid1.MasterTableView.ExportToExcel();
    }

    public override bool CanGoBack { get { return false; } }

    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {

    }


    private void llenarGrid()
    {

        try
        {
            conectar();
            InstruccionesBO bo = new InstruccionesBO(logApp);
            //bo.ReporteHagamodaPromoda();
            bo.ReporteHCC(dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"), cmbCliente.SelectedValue);
            RadGrid1.DataSource = bo.TABLA;
            Session["TotalRegistro"] = bo.totalRegistros;
            //RadGrid1.DataBind();

        }
        catch { }

        finally { desconectar(); }

    }



    protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName)
                ConfigureExport();
        }
        catch (Exception)
        { }
    }
    protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenarGrid();
    }
    protected void RadGrid1_ExcelExportCellFormatting(object sender, ExcelExportCellFormattingEventArgs e)
    {
        e.Cell.Style.Add("border-color", "black");
        e.Cell.Style.Add("border-width", "1");
        e.Cell.Style.Add("border-style", "Solid");
        e.Cell.Style.Add("font-size", "10");

        foreach (GridDataItem gridItem in RadGrid1.Items)
        {
                string alerta = gridItem.Cells[27].Text;
                switch (alerta)
                {
                    case "Verde": SetColor2(gridItem.Cells,  System.Drawing.Color.SpringGreen);
                        break;
                    case "Amarillo": SetColor2(gridItem.Cells,  System.Drawing.Color.Yellow);
                        break;
                    case "Cafe": SetColor2(gridItem.Cells,  System.Drawing.Color.PeachPuff);
                        break;
                    case "Azul": SetColor2(gridItem.Cells,  System.Drawing.Color.Aqua);
                        break;
                    default: break;
                }
        }

    }
    protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
    {
        {
            try
            {
                
            }
            catch { }
        }
    }

    private static void SetColor(IEnumerable cells, String commandName, System.Drawing.Color color)
    {
        foreach (var cell in from TableCell cell in cells let tableCell = cell as GridTableCell where tableCell != null let tcell = tableCell where tcell.Column.UniqueName == commandName select cell)
        {
            cell.BackColor = color;
            cell.ForeColor = color;

        }
    }

    private static void SetColor2(IEnumerable cells, System.Drawing.Color color)
    {
        foreach (var cell in from TableCell cell in cells let tableCell = cell as GridTableCell where tableCell != null select cell)
        {
            cell.BackColor = color;
        }
    }
}
