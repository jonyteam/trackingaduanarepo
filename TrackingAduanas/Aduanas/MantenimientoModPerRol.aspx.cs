﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Telerik.Web.UI;

public partial class MantenimientoModPerRol : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Mantenimiento Módulos Permisos y Perfiles";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgModulos.FilterMenu);
        SetGridFilterMenu(rgPermisos.FilterMenu);
        SetGridFilterMenu(rgRoles.FilterMenu);
        SetGridFilterMenu(rgPermisosModulos.FilterMenu);
        SetGridFilterMenu(rgAutorizacionesModulos.FilterMenu);
        rgModulos.Skin = Skin;
        rgPermisos.Skin = Skin;
        rgRoles.Skin = Skin;
        rgPermisosModulos.Skin = Skin;
        rgAutorizacionesModulos.Skin = Skin;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Mantenimiento Códigos", "Módulos, Permisos y Perfiles");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
        }
    }

    protected bool Anular { get { return tienePermiso("Anular"); } }
    protected bool Ingresar { get { return tienePermiso("Ingresar"); } }
    protected bool Modificar { get { return tienePermiso("Modificar"); } }

    protected void SetVisible(TableCellCollection cells, String commandName, bool visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is ImageButton)
                {
                    ImageButton btn = (ImageButton)control;

                    if (btn.CommandName == commandName)
                        btn.Visible = visible;
                }
            }
        }
    }

    #region Módulos
    private void loadGridModulos()
    {
        try
        {
            conectar();

            ModulosBO bo = new ModulosBO(logApp);
            bo.loadModulos();

            rgModulos.DataSource = bo.TABLA;
            rgModulos.DataBind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected Object GetModulos()
    {
        try
        {
            conectar();

            ModulosBO bo = new ModulosBO(logApp);
            bo.loadModulos();

            return bo.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }

        return null;
    }

    protected void rgModulos_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        loadGridModulos();
    }

    protected void GetDatosEditadosModulos(ModulosBO bo, GridItem item)
    {
        GridEditableItem editedItem = item as GridEditableItem;

        RadTextBox descripcion = (RadTextBox)editedItem.FindControl("edDescripcion");

        bo.DESCRIPCION = descripcion.Text.Trim();
    }

    protected void rgModulos_InsertCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridEditableItem editedItem = e.Item as GridEditableItem;

            ModulosBO bo = new ModulosBO(logApp);
            bo.loadModulos("-1");
            bo.newLine();

            GetDatosEditadosModulos(bo, e.Item);

            bo.commitLine();
            bo.actualizar();
            registrarMensaje("Módulo ingresado exitósamente");
            rgModulos.Rebind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void rgModulos_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridEditableItem editedItem = e.Item as GridEditableItem;

            String IdModulo = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdModulo"]);

            ModulosBO bo = new ModulosBO(logApp);
            bo.loadModulos(IdModulo);

            GetDatosEditadosModulos(bo, e.Item);

            bo.actualizar();

            registrarMensaje("Módulo modificado exitósamente");
            rgModulos.Rebind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void rgModulos_DeleteCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridDataItem editedItem = e.Item as GridDataItem;

            String IdModulo = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdModulo"]);

            ModulosBO bo = new ModulosBO(logApp);
            bo.loadModulos(IdModulo);

            bo.ELIMINADO = "1";

            bo.actualizar();

            registrarMensaje("Módulo eliminado exitósamente");
            rgModulos.Rebind();
        }
        catch { }
        finally
        {
            desconectar();
        }

    }

    protected void rgModulos_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if ((e.Item.ItemType == GridItemType.Item) || (e.Item.ItemType == GridItemType.AlternatingItem) || (e.Item.ItemType == GridItemType.SelectedItem))
        {
            SetVisible(e.Item.Cells, "Edit", Modificar);
            SetVisible(e.Item.Cells, "Delete", Anular);
        }
    }
    #endregion

    #region Permisos
    private void loadGridPermisos()
    {
        try
        {
            conectar();

            PermisosBO bo = new PermisosBO(logApp);
            bo.loadPermisos();

            rgPermisos.DataSource = bo.TABLA;
            rgPermisos.DataBind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected Object GetPermisos()
    {
        try
        {
            conectar();

            PermisosBO bo = new PermisosBO(logApp);
            bo.loadPermisos();

            return bo.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }

        return null;
    }

    protected void rgPermisos_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        loadGridPermisos();
    }

    protected void GetDatosEditadosPermisos(PermisosBO bo, GridItem item)
    {
        GridEditableItem editedItem = item as GridEditableItem;

        RadTextBox descripcion = (RadTextBox)editedItem.FindControl("edDescripcion");

        bo.DESCRIPCION = descripcion.Text.Trim();
    }

    protected void rgPermisos_InsertCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridEditableItem editedItem = e.Item as GridEditableItem;

            PermisosBO bo = new PermisosBO(logApp);
            bo.loadPermisos("-1");
            bo.newLine();

            GetDatosEditadosPermisos(bo, e.Item);

            bo.commitLine();
            bo.actualizar();

            registrarMensaje("Permiso ingresado exitósamente");
            rgPermisos.Rebind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void rgPermisos_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridEditableItem editedItem = e.Item as GridEditableItem;

            String IdPermiso = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdPermiso"]);

            PermisosBO bo = new PermisosBO(logApp);
            bo.loadPermisos(IdPermiso);

            GetDatosEditadosPermisos(bo, e.Item);

            bo.actualizar();

            registrarMensaje("Permiso modificado exitósamente");
            rgPermisos.Rebind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void rgPermisos_DeleteCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridDataItem editedItem = e.Item as GridDataItem;

            String IdPermiso = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdPermiso"]);

            PermisosBO bo = new PermisosBO(logApp);
            bo.loadPermisos(IdPermiso);

            bo.ELIMINADO = "1";

            bo.actualizar();

            registrarMensaje("Permiso eliminado exitósamente");
            rgPermisos.Rebind();
        }
        catch { }
        finally
        {
            desconectar();
        }

    }

    protected void rgPermisos_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if ((e.Item.ItemType == GridItemType.Item) || (e.Item.ItemType == GridItemType.AlternatingItem) || (e.Item.ItemType == GridItemType.SelectedItem))
        {
            SetVisible(e.Item.Cells, "Edit", Modificar);
            SetVisible(e.Item.Cells, "Delete", Anular);
        }
    }
    #endregion

    #region Roles
    private void loadGridRoles()
    {
        try
        {
            conectar();

            RolesBO bo = new RolesBO(logApp);
            bo.loadRoles();

            rgRoles.DataSource = bo.TABLA;
            rgRoles.DataBind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected Object GetRoles()
    {
        try
        {
            conectar();

            RolesBO bo = new RolesBO(logApp);
            bo.loadRoles();

            return bo.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }

        return null;
    }

    protected void rgRoles_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        loadGridRoles();
    }

    protected void GetDatosEditadosRoles(RolesBO bo, GridItem item)
    {
        GridEditableItem editedItem = item as GridEditableItem;

        RadTextBox descripcion = (RadTextBox)editedItem.FindControl("edDescripcion");

        bo.DESCRIPCION = descripcion.Text.Trim();
    }

    protected void rgRoles_InsertCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridEditableItem editedItem = e.Item as GridEditableItem;

            RolesBO bo = new RolesBO(logApp);
            bo.loadRoles("-1");
            bo.newLine();

            GetDatosEditadosRoles(bo, e.Item);

            bo.commitLine();
            bo.actualizar();

            registrarMensaje("Perfil ingresado exitósamente");
            rgRoles.Rebind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void rgRoles_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridEditableItem editedItem = e.Item as GridEditableItem;

            String IdRol = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdRol"]);

            RolesBO bo = new RolesBO(logApp);
            bo.loadRoles(IdRol);

            GetDatosEditadosRoles(bo, e.Item);

            bo.actualizar();

            registrarMensaje("Perfil modificado exitósamente");
            rgRoles.Rebind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void rgRoles_DeleteCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridDataItem editedItem = e.Item as GridDataItem;

            String IdRol = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdRol"]);

            RolesBO bo = new RolesBO(logApp);
            bo.loadRoles(IdRol);

            bo.ELIMINADO = "1";

            bo.actualizar();

            registrarMensaje("Perfil eliminado exitósamente");
            rgRoles.Rebind();
        }
        catch { }
        finally
        {
            desconectar();
        }

    }

    protected void rgRoles_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if ((e.Item.ItemType == GridItemType.Item) || (e.Item.ItemType == GridItemType.AlternatingItem) || (e.Item.ItemType == GridItemType.SelectedItem))
        {
            SetVisible(e.Item.Cells, "Edit", Modificar);
            SetVisible(e.Item.Cells, "Delete", Anular);
        }

    }
    #endregion

    #region Permisos Modulos
    private void loadGridPermisosModulos()
    {
        try
        {
            conectar();

            PermisosModulosBO bo = new PermisosModulosBO(logApp);
            bo.loadPermisosModulosDescs();

            rgPermisosModulos.DataSource = bo.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void rgPermisosModulos_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        loadGridPermisosModulos();
    }

    protected void GetDatosEditadosPermisosModulos(PermisosModulosBO bo, GridItem item)
    {
        GridEditableItem editedItem = item as GridEditableItem;

        RadComboBox permiso = (RadComboBox)editedItem.FindControl("edPermiso");
        RadComboBox modulo = (RadComboBox)editedItem.FindControl("edModulo");
        RadComboBox rol = (RadComboBox)editedItem.FindControl("edRol");

        bo.IDPERMISO = permiso.SelectedValue;
        bo.IDMODULO = modulo.SelectedValue;
        bo.IDROL = rol.SelectedValue;
    }

    protected void rgPermisosModulos_InsertCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridEditableItem editedItem = e.Item as GridEditableItem;

            PermisosModulosBO bo = new PermisosModulosBO(logApp);
            PermisosBO p = new PermisosBO(logApp);
            ModulosBO m = new ModulosBO(logApp);
            RolesBO r = new RolesBO(logApp);

            bo.loadPermisosModulos("-1", "-1", "-1");
            bo.newLine();

            GetDatosEditadosPermisosModulos(bo, e.Item);

            bo.commitLine();
            bo.actualizar();

            registrarMensaje("Permiso a módulo ingresado exitósamente");

            p.loadPermisos(bo.IDPERMISO);
            m.loadModulos(bo.IDMODULO);
            r.loadRoles(bo.IDROL);

            llenarBitacora("Se ingresó el permiso " + p.DESCRIPCION + " al módulo " + m.DESCRIPCION + " con rol " + r.DESCRIPCION, Session["IdUsuario"].ToString(), bo.IDMODULO);
            rgPermisosModulos.Rebind();
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoModPerRol");
        }
        finally
        {
            desconectar();
        }
    }

    protected void rgPermisosModulos_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridEditableItem editedItem = e.Item as GridEditableItem;

            String IdPermiso = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdPermiso"]);
            String IdModulo = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdModulo"]);
            String IdRol = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdRol"]);

            PermisosModulosBO bo = new PermisosModulosBO(logApp);
            PermisosBO p = new PermisosBO(logApp);
            ModulosBO m = new ModulosBO(logApp);
            RolesBO r = new RolesBO(logApp);
            p.loadPermisos(IdPermiso);
            string oldPermiso = p.DESCRIPCION;

            bo.loadPermisosModulos(IdPermiso, IdModulo, IdRol);

            GetDatosEditadosPermisosModulos(bo, e.Item);

            bo.actualizar();

            registrarMensaje("Permiso a módulo modificado exitósamente");

            m.loadModulos(bo.IDMODULO);
            r.loadRoles(bo.IDROL);
            p.loadPermisos(bo.IDPERMISO);
            llenarBitacora("Se modificó el permiso " + oldPermiso + " por " + p.DESCRIPCION + " al módulo " + m.DESCRIPCION + " con rol " + r.DESCRIPCION, Session["IdUsuario"].ToString(), bo.IDMODULO);
            rgPermisosModulos.Rebind();
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoModPerRol");
        }
        finally
        {
            desconectar();
        }
    }

    protected void rgPermisosModulos_DeleteCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridDataItem editedItem = e.Item as GridDataItem;

            String IdPermiso = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdPermiso"]);
            String IdModulo = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdModulo"]);
            String IdRol = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdRol"]);

            PermisosModulosBO bo = new PermisosModulosBO(logApp);
            PermisosBO p = new PermisosBO(logApp);
            ModulosBO m = new ModulosBO(logApp);
            RolesBO r = new RolesBO(logApp);

            bo.deletePermisosModulos(IdPermiso, IdModulo, IdRol);

            registrarMensaje("Permiso a módulo eliminado exitósamente");

            p.loadPermisos(bo.IDPERMISO);
            m.loadModulos(bo.IDMODULO);
            r.loadRoles(bo.IDROL);
            llenarBitacora("Se eliminó el permiso " + p.DESCRIPCION + " al módulo " + m.DESCRIPCION + " con rol " + r.DESCRIPCION, Session["IdUsuario"].ToString(), bo.IDMODULO);
            rgPermisosModulos.Rebind();
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoModPerRol");
        }
        finally
        {
            desconectar();
        }

    }

    protected void btnActivo_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton btn = (ImageButton)sender;
        try
        {
            conectar();

            String[] args = btn.CommandArgument.Split(',');
            String IdPermiso = args[0];
            String IdModulo = args[1];
            String IdRol = args[2];

            PermisosModulosBO bo = new PermisosModulosBO(logApp);
            bo.loadPermisosModulos(IdPermiso, IdModulo, IdRol);

            if (btn.CommandName == "Activar")
            {
                bo.ELIMINADO = "0";
                btn.CommandName = "Desactivar";
                btn.ImageUrl = "~/Imagenes/16/check2_16.png";
            }
            else
            {
                bo.ELIMINADO = "1";
                btn.CommandName = "Activar";
                btn.ImageUrl = "~/Imagenes/16/delete2_16.png";
            }
            bo.actualizar();
            rgPermisosModulos.Rebind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void rgPermisosModulos_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if ((e.Item.ItemType == GridItemType.Item) || (e.Item.ItemType == GridItemType.AlternatingItem) || (e.Item.ItemType == GridItemType.SelectedItem))
        {
            SetVisible(e.Item.Cells, "Edit", Modificar);
            SetVisible(e.Item.Cells, "Delete", Anular);
        }
    }
    #endregion

    #region Autorizaciones Modulos
    private void loadGridAutorizacionesModulos()
    {
        try
        {
            conectar();

            AutorizacionesModuloBO bo = new AutorizacionesModuloBO(logApp);
            bo.loadAutorizacionesEstadoDescs();

            rgAutorizacionesModulos.DataSource = bo.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void rgAutorizacionesModulos_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        loadGridAutorizacionesModulos();
    }

    protected void GetDatosEditadosAutorizacionesModulos(AutorizacionesModuloBO bo, GridItem item)
    {
        GridEditableItem editedItem = item as GridEditableItem;

        RadComboBox modulo = (RadComboBox)editedItem.FindControl("edModulo");
        RadComboBox rol = (RadComboBox)editedItem.FindControl("edRol");

        bo.IDMODULO = modulo.SelectedValue;
        bo.IDROL = rol.SelectedValue;
    }

    protected void rgAutorizacionesModulos_InsertCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridEditableItem editedItem = e.Item as GridEditableItem;

            AutorizacionesModuloBO bo = new AutorizacionesModuloBO(logApp);
            ModulosBO m = new ModulosBO(logApp);
            RolesBO r = new RolesBO(logApp);
            bo.loadAutorizacionesModulo("-1");
            bo.newLine();

            GetDatosEditadosAutorizacionesModulos(bo, e.Item);

            bo.commitLine();
            bo.actualizar();

            registrarMensaje("Autorización a módulo ingresado exitósamente");

            m.loadModulos(bo.IDMODULO);
            r.loadRoles(bo.IDROL);
            llenarBitacora("Se ingresó la autorización del perfil " + r.DESCRIPCION + " al módulo " + m.DESCRIPCION, Session["IdUsuario"].ToString(), bo.IDMODULO);
            rgAutorizacionesModulos.Rebind();
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoModPerRol");
        }
        finally
        {
            desconectar();
        }
    }

    protected void rgAutorizacionesModulos_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridEditableItem editedItem = e.Item as GridEditableItem;

            String IdModulo = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdModulo"]);
            String IdRol = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdRol"]);

            AutorizacionesModuloBO bo = new AutorizacionesModuloBO(logApp);
            ModulosBO m = new ModulosBO(logApp);
            RolesBO r = new RolesBO(logApp);
            r.loadRoles(bo.IDROL);
            string oldRol = r.DESCRIPCION;

            bo.loadAutorizacionesModulosRolesEstado(IdRol, IdModulo);

            GetDatosEditadosAutorizacionesModulos(bo, e.Item);

            bo.actualizar();

            registrarMensaje("Autorización a módulo modificado exitósamente");

            m.loadModulos(bo.IDMODULO);
            r.loadRoles(bo.IDROL);
            llenarBitacora("Se modificó la autorización del perfil " + oldRol + " por el perfil " + r.DESCRIPCION + " al módulo " + m.DESCRIPCION, Session["IdUsuario"].ToString(), bo.IDMODULO);
            rgAutorizacionesModulos.Rebind();
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoModPerRol");
        }
        finally
        {
            desconectar();
        }
    }

    protected void rgAutorizacionesModulos_DeleteCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridDataItem editedItem = e.Item as GridDataItem;

            String IdModulo = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdModulo"]);
            String IdRol = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdRol"]);

            AutorizacionesModuloBO bo = new AutorizacionesModuloBO(logApp);
            ModulosBO m = new ModulosBO(logApp);
            RolesBO r = new RolesBO(logApp);

            bo.deleAutorizacionesModulos(IdRol, IdModulo);

            registrarMensaje("Autorización a módulo eliminado exitósamente");

            m.loadModulos(bo.IDMODULO);
            r.loadRoles(bo.IDROL);
            llenarBitacora("Se eliminó la autorización del perfil " + r.DESCRIPCION + " al módulo " + m.DESCRIPCION, Session["IdUsuario"].ToString(), bo.IDMODULO);
            rgAutorizacionesModulos.Rebind();
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoModPerRol");
        }
        finally
        {
            desconectar();
        }

    }

    protected void btnAMActivo_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton btn = (ImageButton)sender;
        try
        {
            conectar();

            String[] args = btn.CommandArgument.Split(',');
            String IdModulo = args[1];
            String IdRol = args[0];

            AutorizacionesModuloBO bo = new AutorizacionesModuloBO(logApp);
            bo.loadAutorizacionesModulosRolesEstado(IdModulo, IdRol);

            if (btn.CommandName == "Activar")
            {
                bo.ELIMINADO = "0";
                btn.CommandName = "Desactivar";
                btn.ImageUrl = "~/Imagenes/16/check2_16.png";
            }
            else
            {
                bo.ELIMINADO = "1";
                btn.CommandName = "Activar";
                btn.ImageUrl = "~/Imagenes/16/delete2_16.png";
            }

            bo.actualizar();
        }
        catch (Exception) { }
        finally
        {
            desconectar();
        }
    }

    protected void rgAutorizacionesModulos_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if ((e.Item.ItemType == GridItemType.Item) || (e.Item.ItemType == GridItemType.AlternatingItem) || (e.Item.ItemType == GridItemType.SelectedItem))
        {
            SetVisible(e.Item.Cells, "Edit", Modificar);
            SetVisible(e.Item.Cells, "Delete", Anular);
        }
    }
    #endregion
}