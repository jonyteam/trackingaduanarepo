﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Telerik.Web.UI;
using Toolkit.Core.Extensions;

public partial class MantenimientoClientes : Utilidades.PaginaBase
{

    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Mantenimiento Clientes";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgClientes.FilterMenu);
        rgClientes.Skin = Skin;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Mantenimiento Clientes", "Clientes");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Inicio.aspx");
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void SetVisible(TableCellCollection cells, String commandName, bool visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is ImageButton)
                {
                    ImageButton btn = (ImageButton)control;

                    if (btn.CommandName == commandName)
                        btn.Visible = visible;
                }
            }
        }
    }

    #region Clientes
    private void llenarGridClientes()
    {
        try
        {
            conectar();
            ClientesBO clientes = new ClientesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (u.CODPAIS == "H")
                clientes.loadClientes();
            else
                clientes.loadAllCamposClientesXPais(u.CODPAIS);
            rgClientes.DataSource = clientes.TABLA;
            rgClientes.DataBind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected Object GetPaises()
    {
        try
        {
            conectar();
            CodigosBO bo = new CodigosBO(logApp);
            //bo.loadAllCampos("PAISES");
            bo.loadPaisesHojaRuta();
            return bo.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }

        return null;
    }

    protected void rgClientes_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridClientes();
    }

    protected void GetDatosEditados(ClientesBO bo, GridItem item)
    {
        AduanasDataContext _aduanasDC = new AduanasDataContext();

        GridEditableItem editedItem = item as GridEditableItem;

        RadTextBox codigoSAP = (RadTextBox)editedItem.FindControl("edCodigoSAP");
        RadTextBox rtn = (RadTextBox)editedItem.FindControl("edRTN");
        RadTextBox nombre = (RadTextBox)editedItem.FindControl("edNombre");
        RadTextBox direccion = (RadTextBox)editedItem.FindControl("edDireccion");
        //RadTextBox oficialCuenta = (RadTextBox)editedItem.FindControl("edOficialCuenta");
        RadComboBox oficialCuenta = (RadComboBox) editedItem.FindControl("edOficialCuenta");
        RadTextBox contacto = (RadTextBox)editedItem.FindControl("edContacto");
        RadTextBox telefono = (RadTextBox)editedItem.FindControl("edTelefono");
        RadTextBox eMail = (RadTextBox)editedItem.FindControl("edEMail");
        RadComboBox codigoPais = (RadComboBox)editedItem.FindControl("edCodigoPais");


        //var idOficialCuenta  = _aduanasDC.Usuarios.FirstOrDefault(w => w.Nombre.Contains(usuario[0]) && w.Apellido.Contains(usuario[1])).IdUsuario;


        bo.CODIGOSAP = codigoSAP.Text.Trim();
        bo.RTN = rtn.Text.Trim();
        bo.NOMBRE = nombre.Text.Trim();
        bo.DIRECCION = direccion.Text.Trim();
        bo.OFICIALCUENTA = oficialCuenta.Text;
        bo.IDOFICIALCUENTA = (int) oficialCuenta.SelectedValue.ToDecimal();
        bo.CONTACTO = contacto.Text.Trim();
        bo.TELEFONO = telefono.Text.Trim();
        bo.EMAIL = eMail.Text.Trim();
        bo.CODIGOPAIS = codigoPais.SelectedValue;
        
    }

    protected void rgClientes_InsertCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            //conectar();
            //ClientesBO bo = new ClientesBO(logApp);
            //ClientesBO cl = new ClientesBO(logApp);
            //string nuevoId = "";
            ////Generar un nuevo Id. 
            //bo.loadNuevoId("CL-");
            //nuevoId = bo.TABLA.Rows[0]["NuevoId"].ToString();
            //if (nuevoId == "")
            //{
            //    nuevoId = "CL-1";
            //}

            //bo.loadCliente("-1");
            //bo.newLine();

            //GetDatosEditados(bo, e.Item);

            //bo.CODIGOCLIENTE = nuevoId;
            //bo.FECHA = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.fff");
            //bo.ELIMINADO = "0";
            //bo.IDUSUARIO = Session["IdUsuario"].ToString();
            //bo.commitLine();
            //bo.actualizar();

            //bo.loadCliente(bo.CODIGOSAP);
            //if (bo.totalRegistros > 0)
            //{
            //    string alias = "", priNom = "", segNom = "";
            //    string[] cliente = bo.NOMBREEMPRESA.Split(' ');
            //    int tamano = cliente.Length;
            //    int c = 0;
            //    if (tamano >= 2)
            //    {
            //        priNom = cliente[0];
            //        segNom = cliente[1];
            //        int newTam = segNom.Length;

            //        if (newTam > c)
            //        {
            //            alias = cliente[0].Substring(0, 1).ToUpper() + cliente[1].Substring(c, 1).ToUpper();
            //            cl.loadClienteXAlias(alias);
            //            while (cl.totalRegistros > 0 && newTam > c)
            //            {
            //                c++;
            //                alias = cliente[0].Substring(0, 1).ToUpper() + cliente[1].Substring(c, 1).ToUpper();
            //                cl.loadClienteXAlias(alias);
            //            }
            //        }
            //    }
            //    else
            //    {
            //        priNom = cliente[0];
            //        int newTam = priNom.Length;

            //        if (newTam > c)
            //        {
            //            alias = cliente[0].Substring(0, 1).ToUpper() + cliente[0].Substring(c + 1, 1).ToUpper();
            //            cl.loadClienteXAlias(alias);
            //            while (cl.totalRegistros > 0 && newTam > c)
            //            {
            //                c++;
            //                alias = cliente[0].Substring(0, 1).ToUpper() + cliente[0].Substring(c + 1, 1).ToUpper();
            //                cl.loadClienteXAlias(alias);
            //            }
            //        }
            //    }
            //    bo.ALIAS = alias;
            //    bo.actualizar();
            //}

            //registrarMensaje("Cliente ingresado correctamente");

            //rgClientes.Rebind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void rgClientes_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();
            GridEditableItem editedItem = e.Item as GridEditableItem;

            String codCliente = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["CodigoSAP"]);

            ClientesBO bo = new ClientesBO(logApp);
            bo.loadCliente(codCliente);

            GetDatosEditados(bo, e.Item);

            bo.actualizar();
            registrarMensaje("Cliente actualizado exitosamente");

            rgClientes.Rebind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void rgClientes_DeleteCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridDataItem editedItem = e.Item as GridDataItem;

            String codCliente = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["CodigoSAP"]);

            ClientesBO bo = new ClientesBO(logApp);
            bo.loadCliente(codCliente);
            bo.ELIMINADO = "1";
            bo.actualizar();
            registrarMensaje("Cliente eliminado exitosamente");

            rgClientes.Rebind();
        }
        catch { }
        finally
        {
            desconectar();
        }

    }

    protected void rgClientes_ItemDataBound(object sender, GridItemEventArgs e)
    {
        try
        {
            if ((e.Item.ItemType == GridItemType.Item) || (e.Item.ItemType == GridItemType.AlternatingItem) || (e.Item.ItemType == GridItemType.SelectedItem))
            {
                SetVisible(e.Item.Cells, "Edit", Modificar);
                SetVisible(e.Item.Cells, "Delete", Anular);
            }

        }
        catch { }
    }

    protected Object GetOficialCuenta()
    {
        AduanasDataContext _aduanasDC = new AduanasDataContext();

        var roles = _aduanasDC.UsuariosRoles.Where(w => w.IdRol == 9 && w.Eliminado == '0').ToList();

        var of = _aduanasDC.Usuarios.Where(w => w.Eliminado == '0').ToList();

        var resp = of.Join(roles, x => x.IdUsuario, y => y.IdUsuario, (x, y) => new {x = x, y = y})
            .Select(s => new
            {
                Nombre = s.x.Nombre + ' ' + s.x.Apellido,
                IdOficialCuenta = s.x.IdUsuario
            }).ToList();

        return resp;

    }
    #endregion
}
