﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;

using System.Data.OleDb;
//using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;

public partial class GenerarArchivoSAPDinant : Utilidades.PaginaBase
{
    private GrupoLis.Login.Login logAppAduanas;

    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Solicitud Pago";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Hagamoda/Promoda", "SolicitudPago");
        }
    }

    protected bool Consultar { get { return tienePermiso("Consultar"); } }

    private void ConfigureExport()
    {
        String filename = "Reporte_" + DateTime.Now.ToShortDateString();
        RadGrid1.ExportSettings.FileName = filename;
        RadGrid1.ExportSettings.ExportOnlyData = true;
        RadGrid1.ExportSettings.IgnorePaging = true;
        RadGrid1.ExportSettings.OpenInNewWindow = true;
        RadGrid1.MasterTableView.ExportToExcel();
    }

    public override bool CanGoBack { get { return false; } }


    private void llenarGrid()
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
           
            if (User.IsInRole("Administradores") && User.Identity.Name == "dabonilla" || User.IsInRole("Administradores") && User.Identity.Name == "aalvarado" || User.IsInRole("Administradores") && User.Identity.Name == "lramirez" || User.IsInRole("Administradores") && User.Identity.Name == "mnapky" || User.IsInRole("Administradores") && User.Identity.Name == "gcorrales"|| User.IsInRole("Administradores") && User.Identity.Name == "ycaballero")
                i.loadInstruccionesSolicitudPagoConGuiaFinalizada();
         
            
            
            else if (User.IsInRole("Administrador Pais") || User.IsInRole("Administrador Pais SVGT")|| User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT") || User.IsInRole("GestoresSVGT"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.CODPAIS == "S")
                {
                    i.loadInstruccionesSolicitudPagoSalvador("3");
                }
                else
                     i.loadInstruccionesXPaisSolicitudPagos(u.CODPAIS);
            }
            RadGrid1.DataSource = i.TABLA;
     

        }
        catch (Exception)
        {

        }

        finally { desconectar(); }

    }

    protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName)
                ConfigureExport();
        }
        catch (Exception)
        { }
    }
    protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenarGrid();
    }
    protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
    {
        try
        {
            conectar();
            GatosTracking GT = new GatosTracking(logApp);

            foreach (GridDataItem gridItem in RadGrid1.Items)
            {
                string HojaRuta = gridItem.Cells[2].Text;
                GT.LlenarGatos(HojaRuta.Substring(0, 1));
                RadComboBox Material = (RadComboBox)gridItem.FindControl("cmbMaterial");
                RadComboBox MedioPago = (RadComboBox)gridItem.FindControl("cmbMedioPago");

                Material.DataSource = GT.TABLA;
                Material.DataBind();
                Material.Items.Insert(0, new RadComboBoxItem("Seleccione...", "Seleccione"));
                switch (Material.SelectedValue)
                {
                    case "Seleccione":
                        MedioPago.Visible = false;
                        break;
                    default: break;
                }

            }

        }
        catch (Exception)
        {

        }
    }
    protected void cmbMaterial_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {

        try
        {
            GridDataItem editForm = (sender as RadComboBox).NamingContainer as GridDataItem;
            RadComboBox Material = editForm.FindControl("cmbMaterial") as RadComboBox;
            RadComboBox MedioPago = editForm.FindControl("cmbMedioPago") as RadComboBox;
            string HojaRuta = editForm.Cells[2].Text;
            if (Material.SelectedValue == "Seleccione")
            {
                MedioPago.Visible = false;
            }
            else if (Material.SelectedValue != "Seleccione")
            {
                conectar();
                CodigosBO C = new CodigosBO(logApp);
                C.loadAllCampos("MEDIOPAGO");
                MedioPago.Visible = true;
                MedioPago.DataSource = C.TABLA;
                MedioPago.DataBind();
                MedioPago.Items.Insert(0, new RadComboBoxItem("Seleccione...", "Seleccione"));
                cmbMaterial.Value = Material.SelectedValue;
            }
        }
        catch (Exception)
        { }

    }
    protected void cmbMedioPago_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            GridDataItem editForm = (sender as RadComboBox).NamingContainer as GridDataItem;
            RadComboBox PagarA = editForm.FindControl("cmbPagarA") as RadComboBox;
            RadComboBox MedioPago = editForm.FindControl("cmbMedioPago") as RadComboBox;
            string HojaRuta = editForm.Cells[2].Text;
            if (MedioPago.SelectedValue == "Seleccione")
            {
                PagarA.Visible = false;
            }
            else if (MedioPago.SelectedValue != "Seleccione")
            {
                conectar();
                ProveedoresSolicitudBO PS = new ProveedoresSolicitudBO(logApp);
                PS.loadProveedoresPais(HojaRuta.Substring(0, 1));
                PagarA.DataSource = PS.TABLA;
                PagarA.DataBind();
                PagarA.Items.Insert(0, new RadComboBoxItem("Seleccione...", "Seleccione"));
                PagarA.Visible = true;
                cmbMedioPago.Value = MedioPago.SelectedValue;

            }
        }
        catch (Exception)
        { }
    }
    protected void cmbPagarA_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            GridDataItem editForm = (sender as RadComboBox).NamingContainer as GridDataItem;
            RadComboBox PagarA = editForm.FindControl("cmbPagarA") as RadComboBox;
            RadNumericTextBox Monto = editForm.FindControl("txtMonto") as RadNumericTextBox;
            RadNumericTextBox IVA = editForm.FindControl("txtIVA") as RadNumericTextBox;
            RadNumericTextBox MontoPagar = editForm.FindControl("txtValorVenta") as RadNumericTextBox;
            RadComboBox Moneda = editForm.FindControl("cmbMoneda") as RadComboBox;
            RadComboBox MedioPago = editForm.FindControl("cmbMedioPago") as RadComboBox;
            RadButton Solicitar = editForm.FindControl("btnsolicitar") as RadButton;
            RadTextBox Observacion = editForm.FindControl("txtObservacion") as RadTextBox;
            string HojaRuta = editForm.Cells[2].Text;
            if (PagarA.SelectedValue == "Seleccione")
            {
                Monto.Visible = false;
                IVA.Visible = false;
                MontoPagar.Visible = false;
                Moneda.Visible = false;
                Solicitar.Visible = false;
                Observacion.Visible = false;
            }
            else if (PagarA.SelectedValue != "Seleccione")
            {
                conectar();
                CodigosBO C = new CodigosBO(logApp);
                C.loadAllCampos("MONEDA" + HojaRuta.Substring(0, 1));
                Moneda.DataSource = C.TABLA;
                Moneda.DataBind();
                Monto.Visible = true;
                IVA.Visible = true;
                MontoPagar.Visible = true;
                Moneda.Visible = true;
                Solicitar.Visible = true;
                Observacion.Visible = true;
                cmbPagarA.Value = PagarA.SelectedValue;
            }
        }
        catch (Exception)
        { }

    }
    protected void btnSolicitar_Click(object sender, EventArgs e)
    {
        try
        {
            GridDataItem editForm = (sender as RadButton).NamingContainer as GridDataItem;
            RadComboBox Material = editForm.FindControl("cmbMaterial") as RadComboBox;
            RadComboBox MedioPago = editForm.FindControl("cmbMedioPago") as RadComboBox;
            RadComboBox PagarA = editForm.FindControl("cmbPagarA") as RadComboBox;
            RadNumericTextBox Monto = editForm.FindControl("txtMonto") as RadNumericTextBox;
            RadNumericTextBox IVA = editForm.FindControl("txtIVA") as RadNumericTextBox;
            RadNumericTextBox MontoPagar = editForm.FindControl("txtValorVenta") as RadNumericTextBox;
            RadTextBox Observacion = editForm.FindControl("txtObservacion") as RadTextBox;
            RadComboBox Moneda = editForm.FindControl("cmbMoneda") as RadComboBox;
            string HojaRuta = editForm.Cells[2].Text;
            bool estado = false;
            {
                if (Monto.Text != "")
                {
                    if (MontoPagar.Text != "")
                    {
                        if (Material.SelectedValue != "Seleccione")
                        {
                            if (MedioPago.SelectedValue != "Seleccione")
                            {
                                if (PagarA.SelectedValue != "Seleccione")
                                {
                                    if (Moneda.SelectedValue != "Seleccione")
                                    {
                                        conectar();
                                        estado = verificargasto(HojaRuta, Material.SelectedValue);

                                        if (estado)
                                        {
                                          
                                            int verificar = 0;
                                            double tasacambio = 1.00;
                                            if (HojaRuta.Substring(0, 1) == "S")
                                            {
                                                verificar = 1;
                                            }
                                            else
                                            {
                                                TipoCambioBO TC = new TipoCambioBO(logApp);
                                                TC.loadtasacambio(HojaRuta.Substring(0, 1));
                                                if (TC.totalRegistros >= 1)
                                                {
                                                    verificar = TC.totalRegistros;
                                                    tasacambio = TC.TASACAMBIO;
                                                }
                                            }

                                            if (verificar == 1)
                                            {
                                                DetalleGastosBO DG1 = new DetalleGastosBO(logApp);
                                                DetalleGastosBO DG = new DetalleGastosBO(logApp);
                                                DG1.ComprobarGatosXHojaYGasto(HojaRuta, Material.SelectedValue);
                                                if (DG1.totalRegistros == 0 )
                                                {
                                                    DG.BuscarHoja("1");
                                                    DG.newLine();
                                                    DG.IDINSTRUCCION = HojaRuta;
                                                    DG.IDGASTO = Material.SelectedValue;
                                                    DG.MEDIOPAGO = MedioPago.SelectedValue;
                                                    DG.PROVEEDOR = PagarA.SelectedValue;
                                                    DG.MONTO = Convert.ToDouble(Monto.Text);
                                                    if (IVA.Text == "")
                                                    {
                                                        IVA.Text = "0";
                                                    }
                                                    DG.IVA = Convert.ToDouble(IVA.Text);
                                                    DG.TOTALVENTA = Convert.ToDouble(MontoPagar.Text);
                                                    DG.TASACAMBIOFIJA = tasacambio;
                                                    DG.TASACAMBIOVARIABLE = tasacambio;
                                                    DG.MONEDA = Moneda.SelectedValue;
                                                    DG.ESTADO = "0";
                                                    DG.FECHACREACION = DateTime.Now.ToString();
                                                    DG.USUARIO = Session["IdUsuario"].ToString();
                                                    if (cmbMedioPago.Value == "CC")
                                                    {
                                                        DG.ESTADO = "7";
                                                    }
                                                    DG.OBSERVACION = Observacion.Text;
                                                    DG.CODESTADOFACTURACION = "0";
                                                    DG.REVERSARPARTIDA = "2";
                                                    DG.commitLine();

                                                    DG.actualizar();
                                                    RadGrid1.Rebind();
                                                    registrarMensaje("Gasto ingresado de manera exitosa!");
                                                }
                                                else if (DG1.totalRegistros == 1)
                                                {
                                                    registrarMensaje("El Gasto que intenta ingresar ya existe!");
                                                }
                                                else
                                                {
                                                    registrarMensaje("Favor comunicarse con el administrador del sistema");
                                                }
                                            }
                                            else
                                                registrarMensaje("No Hay Tasa de Cambio Ingresara el Dia de Hoy");
                                        }
                                        else
                                            registrarMensaje("El Gasto ya ha sido ingresado");
                                    }
                                    else
                                        registrarMensaje("Favor Seleccion Algún Tipo de Moneda");
                                }
                                else
                                    registrarMensaje("Favor Seleccion Algún Proveedor");
                            }
                            else
                                registrarMensaje("Favor Seleccion Algún Tipo de Pago");
                        }
                        else
                            registrarMensaje("Favor Seleccione Algún Tipo de Material");
                    }
                    else
                        registrarMensaje("Favor Ingrese El Valor Negociado");
                }
                else
                {
                    registrarMensaje("Favor Ingrese Algún Valor al Monto");
                }
            }

            llenarGrid();
        }
        catch { }
        finally { desconectar(); }
    }

    private bool verificargasto(string HojaRuta, string Idgasto)
    {
   
        DetalleGastosBO bo = new DetalleGastosBO(logApp);
        bo.LlenarGatosXHojaYGasto(HojaRuta, Idgasto);
        if (bo.totalRegistros >= 1 & HojaRuta.Substring(0, 1) != "S")
        {
            return false;
        }
        else
            return true;
    }


    #region Conexion
    private void conectarAduanas(string Pais)
    {
        if (logAppAduanas == null)
            logAppAduanas = new GrupoLis.Login.Login();
        if (!logAppAduanas.conectado)
        {
            logAppAduanas.tipoConexion = TipoConexion.SQL_SERVER;
            logAppAduanas.SERVIDOR = mParamethers.Get("Servidor2");
            if (Pais.ToString() == "S")
            {
                logAppAduanas.DATABASE = mParamethers.Get("CajaChicaSV");
            }
            if (Pais.ToString() == "N")
            {
                logAppAduanas.DATABASE = mParamethers.Get("CajaChicaNI");
            }
            if (Pais.ToString() == "G")
            {
                logAppAduanas.DATABASE = mParamethers.Get("CajaChicaGT");
            }
            logAppAduanas.USER = mParamethers.Get("User2");
            logAppAduanas.PASSWD = mParamethers.Get("Password2");
            logAppAduanas.conectar();
        }
    }

    private void desconectarAduanas()
    {
        if (logAppAduanas.conectado)
            logAppAduanas.desconectar();
    }
    #endregion




}