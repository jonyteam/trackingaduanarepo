﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Instrucciones.aspx.cs" Inherits="Instrucciones" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style3 {
            width: 10%;
            height: 36px;
        }

        .style4 {
            width: 23%;
            height: 36px;
        }

        .style5 {
            width: 24%;
        }

        .style6 {
            width: 24%;
            height: 36px;
        }

        .style7 {
            width: 11%;
        }

        .style8 {
            width: 11%;
            height: 36px;
            text-align: left;
        }

        .auto-style1 {
            width: 14%;
        }

        .auto-style2 {
            width: 21%;
        }

        .auto-style3 {
            width: 12%;
        }

        .auto-style4 {
            font-size: small;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                //<![CDATA[
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0
                    || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                        var ValoFOB = document.getElementById("txtValorFOB");
                        var Flete = document.getElementById("txtFlete");
                        var Seguro = document.getElementById("txtSeguro");
                        var Otros = document.getElementById("txtOtros");
                        var result = $find("<%=txtValorCIF.ClientID%>");
                        result = eval(ValoFOB + Suma + Flete + Suma + Seguro + Suma + Otros);
                    }
                }
            </script>
        </telerik:RadScriptBlock>
        <telerik:RadMultiPage ID="mpInstrucciones" runat="server" BackColor="AliceBlue">
            <telerik:RadPageView ID="pvGridInstrucciones" runat="server" Width="100%">
                <table width="100%">
                    <tr>
                        <td>
                            <telerik:RadGrid ID="rgInstrucciones" runat="server" AllowFilteringByColumn="True"
                                AllowSorting="True" AutoGenerateColumns="False" GridLines="None" Width="100%"
                                Height="400px" OnNeedDataSource="rgInstrucciones_NeedDataSource" AllowPaging="True"
                                ShowFooter="True" ShowStatusBar="True" PageSize="50" OnInit="rgInstrucciones_Init"
                                OnItemCommand="rgInstrucciones_ItemCommand" OnItemDataBound="rgInstrucciones_ItemDataBound"
                                CellSpacing="0">
                                <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                                <MasterTableView DataKeyNames="IdInstruccion,IdTramite,CodigoAduana,IdEstadoFlujo"
                                    CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay instrucciones."
                                    GroupLoadMode="Client">
                                    <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                                        ShowExportToExcelButton="true" ShowExportToCsvButton="true" />
                                    <RowIndicatorColumn>
                                        <HeaderStyle Width="20px"></HeaderStyle>
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn>
                                        <HeaderStyle Width="20px"></HeaderStyle>
                                    </ExpandCollapseColumn>
                                    <Columns>
                                        <telerik:GridButtonColumn ButtonType="ImageButton" HeaderText="Editar" CommandName="Editar"
                                            ImageUrl="Images/16/pencil_16.png" UniqueName="btnEditar">
                                            <ItemStyle Width="35px" />
                                            <HeaderStyle Width="35px" />
                                        </telerik:GridButtonColumn>
                                        <telerik:GridButtonColumn ConfirmText="¿Está seguro de que desea eliminar esta instrucción?"
                                            ConfirmDialogType="RadWindow" ConfirmTitle="Eliminar Instrucción" UniqueName="DeleteColumn"
                                            ButtonType="ImageButton" CommandName="Delete" HeaderText="Eliminar">
                                            <ItemStyle Width="35px" />
                                            <HeaderStyle Width="35px" />
                                        </telerik:GridButtonColumn>
                                        <telerik:GridButtonColumn ConfirmText="¿Está seguro de que desea duplicar esta instrucción?"
                                            ConfirmDialogType="RadWindow" ConfirmTitle="Duplicar Instrucción" UniqueName="DuplicateColumn"
                                            ButtonType="ImageButton" ImageUrl="~/Images/24/cd_new_24.png" CommandName="Duplicar"
                                            HeaderText="Duplicar">
                                            <ItemStyle Width="35px" />
                                            <HeaderStyle Width="35px" />
                                        </telerik:GridButtonColumn>
                                        <telerik:GridTemplateColumn AllowFiltering="false" DataField="IdEstadoFlujo" HeaderText="Comenzar Flujo"
                                            UniqueName="CodEstado">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnComenzar" runat="server" CommandArgument='<%# String.Format("{0}", Eval("IdInstruccion").ToString()) %>'
                                                    CommandName='<%# (Eval("IdEstadoFlujo").ToString() == "0" ? "Comenzar" : "Nada") %>'
                                                    Visible='<%# (Eval("IdEstadoFlujo").ToString() == "0" ? true : false) %>' ImageUrl='<%# (Eval("IdEstadoFlujo").ToString() == "0" ? "Images/16/media_play_green_16.png" : "~/Images/gris.png") %>'
                                                    OnClick="btnComenzar_Click" />
                                            </ItemTemplate>
                                            <ItemStyle Width="35px" />
                                            <HeaderStyle Width="35px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="IdInstruccion" HeaderText="Instrucción No." UniqueName="IdInstruccion"
                                            FilterControlWidth="80%">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Nombre" HeaderText="Cliente" UniqueName="Nombre"
                                            FilterControlWidth="80%">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="NumeroFactura" FilterControlAltText="Filter NumeroFactura column"
                                            HeaderText="Numero de Factura" UniqueName="NumeroFactura">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ReferenciaCliente" HeaderText="Referencia Cliente"
                                            UniqueName="ReferenciaCliente" FilterControlWidth="80%" FooterText="Total registros: "
                                            Aggregate="Count">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="IdEstadoFlujo" FilterControlWidth="80%" HeaderText="IdEstadoFlujo"
                                            UniqueName="IdEstadoFlujo" Visible="false">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <%-- <telerik:GridBoundColumn DataField="CodigoRuta" HeaderText="CodigoRuta" UniqueName="CodigoRuta"
                                            Visible="false" FilterControlWidth="80%">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>--%>
                                    </Columns>
                                    <EditFormSettings>
                                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                        </EditColumn>
                                    </EditFormSettings>
                                </MasterTableView>
                                <HeaderStyle Width="180px" />
                                <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                                    <Selecting AllowRowSelect="True" />
                                    <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                                        DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                                        DropHereToReorder="Suelte aquí para Re-Ordenar" />
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
                                </ClientSettings>
                                <FilterMenu EnableTheming="True">
                                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                                </FilterMenu>
                                <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                                <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
                            </telerik:RadGrid>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td align="center">
                            <asp:ImageButton ID="btnBack2" ToolTip="Regresar" ImageUrl="~/Images/24/arrow_left_green_24.png"
                                runat="server" CausesValidation="false" OnClick="btnBack_Click" />
                        </td>
                    </tr>
                </table>
            </telerik:RadPageView>
            <telerik:RadPageView ID="pvAdministrarIntrucciones" runat="server" Width="100%">
                <div id="miDiv" runat="server" class="panelCentrado">
                    <table width="100%">
                        <tr align="center">
                            <td style="width: 30%" align="center"></td>
                            <td style="width: 40%" align="center">
                                <asp:Panel ID="pnl" runat="server">
                                    <table width="100%">
                                        <tr align="center">
                                            <td style="width: 20%">Trámite:
                                            </td>
                                            <td style="width: 30%" align="center">
                                                <telerik:RadTextBox ID="txtTramite" Width="90%" runat="server" Enabled="false" ForeColor="Black">
                                                </telerik:RadTextBox>
                                            </td>
                                            <td style="width: 20%">Instrucción:
                                            </td>
                                            <td style="width: 30%" align="center">
                                                <telerik:RadTextBox ID="txtIntruccion" Width="90%" runat="server" Enabled="false"
                                                    ForeColor="Black">
                                                </telerik:RadTextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                            <td style="width: 30%" align="center"></td>
                        </tr>
                    </table>
                    <asp:Panel ID="Panel1" runat="server" GroupingText=" " Width="98%" BorderColor="White">
                        <table width="100%">
                            <tr>
                                <td class="auto-style4">Tipo de Instrucción:</td>
                                <td colspan="3" style="font-size: 0pt;">
                                    <telerik:RadComboBox ID="cmbTipoInstruccion" runat="server" Culture="es-ES" DataTextField="Descripcion" DataValueField="Codigo" OnSelectedIndexChanged="cmbPaisHojaRuta_SelectedIndexChanged" Style="font-size: medium" Width="91%">
                                        <Items>
                                            <telerik:RadComboBoxItem runat="server" Text="Selecione Tipo de Instruccion" Value="0" />
                                            <telerik:RadComboBoxItem runat="server" Text="Recibo Talonario" Value="1" />
                                        </Items>
                                    </telerik:RadComboBox>
                                </td>
                                <td class="style7">&nbsp;</td>
                                <td class="style5">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="width: 10%">País: </td>
                                <td style="width: 23%">
                                    <telerik:RadComboBox ID="cmbPaisHojaRuta" runat="server" AutoPostBack="true" DataTextField="Descripcion" DataValueField="Codigo" OnSelectedIndexChanged="cmbPaisHojaRuta_SelectedIndexChanged" Width="91%">
                                    </telerik:RadComboBox>
                                </td>
                                <td style="width: 10%">Aduana: </td>
                                <td style="width: 23%">
                                    <telerik:RadComboBox ID="cmbAduanas" runat="server" AutoPostBack="true" DataTextField="NombreAduana" DataValueField="CodigoAduana" OnSelectedIndexChanged="cmbAduanas_SelectedIndexChanged" Width="90%">
                                    </telerik:RadComboBox>
                                </td>
                                <td class="style7">Fecha Recepción: </td>
                                <td class="style5">
                                    <telerik:RadDateTimePicker ID="dtpFechaRecepcion" runat="server" MaxDate="2040-12-31" MinDate="2012-01-01" Width="90%">
                                    </telerik:RadDateTimePicker>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%">Cliente:
                                </td>
                                <td style="width: 23%">
                                    <input id="edInstruccion" runat="server" type="hidden" /><input id="edTramite" runat="server"
                                        type="hidden" /><input id="edDocumentoOld" runat="server" type="hidden" /><input
                                            id="edDuplicar" runat="server" type="hidden" value="false" />
                                    <telerik:RadComboBox
                                                ID="cmbClientes" runat="server" Width="90%" DataTextField="Nombre" DataValueField="CodigoSAP"
                                                AutoPostBack="true" OnSelectedIndexChanged="cmbClientes_SelectedIndexChanged"
                                                Filter="StartsWith">
                                            </telerik:RadComboBox>
                                </td>
                                <td style="width: 10%">Oficial Cuenta:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadTextBox ID="txtOficialCuenta" Width="90%" runat="server" MaxLength="150"
                                        Enabled="false">
                                    </telerik:RadTextBox>
                                </td>
                                <td class="style7">Numero Factura:
                                </td>
                                <td class="style5">
                                    <telerik:RadTextBox ID="txtNumFactura" runat="server">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style3">División
                                </td>
                                <td class="style4">
                                    <telerik:RadComboBox ID="cmbDivision" runat="server" DataTextField="Descripcion"
                                        DataValueField="Codigo" Filter="StartsWith" Width="90%" Enabled="False" AutoPostBack="True"
                                        OnSelectedIndexChanged="cmbDivision_SelectedIndexChanged">
                                    </telerik:RadComboBox>
                                </td>
                                <td class="style3">Incoterm
                                </td>
                                <td class="style4">
                                    <telerik:RadComboBox ID="cmbIncoterm" runat="server" DataTextField="Descripcion"
                                        DataValueField="Codigo" Filter="StartsWith">
                                    </telerik:RadComboBox>
                                </td>
                                <td class="style8">Referencia<br />
                                    Cliente:
                                </td>
                                <td class="style6">
                                    <telerik:RadTextBox ID="txtReferenciaCliente" runat="server" MaxLength="150" Width="160px">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style3">Tipo de Producto:
                                </td>
                                <td class="style4">
                                    <telerik:RadComboBox ID="cmbProductoCargill" runat="server" DataTextField="Descripcion"
                                        DataValueField="Descripcion">
                                    </telerik:RadComboBox>
                                </td>
                                <td class="style3">&nbsp;
                                    <asp:Label ID="lblCargill" runat="server" Text="Carga Cargill:" Visible="False"></asp:Label>
                                </td>
                                <td class="style4">&nbsp;
                                    <telerik:RadComboBox ID="cmbCargaCargill" runat="server" DataTextField="Descripcion" DataValueField="Codigo" Visible="False">
                                    </telerik:RadComboBox>
                                </td>
                                <td class="style8">&nbsp;
                                </td>
                                <td class="style6">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%">Proveedor:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadTextBox ID="txtProveedores" runat="server" Width="80%" Enabled="false">
                                    </telerik:RadTextBox><asp:ImageButton ID="btnAgregarProveedor" runat="server" ImageUrl="Images/24/add2_24.png"
                                        OnClick="btnAgregarProveedor_Click" />
                                </td>
                                <td>Producto:
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="txtProducto" runat="server" MaxLength="150" TextMode="MultiLine"
                                        Width="80%" Enabled="false">
                                    </telerik:RadTextBox><asp:ImageButton ID="btnEditProdDescs0" runat="server" ImageUrl="Images/24/add2_24.png"
                                        OnClick="btnEditProdDescs_Click" />
                                </td>
                                <td class="style7">RTN/NIT:
                                </td>
                                <td class="style5">
                                    <telerik:RadTextBox ID="txtRTN" Width="90%" runat="server" MaxLength="50" Enabled="false">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%">Valor FOB:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadNumericTextBox ID="txtValorFOB" runat="server" MinValue="0" Value="0"
                                        NumberFormat-DecimalDigits="2" NumberFormat-DecimalSeparator="." NumberFormat-GroupSeparator=","
                                        OnTextChanged="txtValorFOB_TextChanged">
                                    </telerik:RadNumericTextBox>
                                </td>
                                <td style="width: 10%">Flete:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadNumericTextBox ID="txtFlete" runat="server" MinValue="0" Value="0" NumberFormat-DecimalDigits="2"
                                        NumberFormat-DecimalSeparator="." NumberFormat-GroupSeparator="," OnTextChanged="txtFlete_TextChanged">
                                    </telerik:RadNumericTextBox>
                                </td>
                                <td class="style7">Seguro:
                                </td>
                                <td class="style5">
                                    <telerik:RadNumericTextBox ID="txtSeguro" runat="server" MinValue="0" Value="0" NumberFormat-DecimalDigits="2"
                                        NumberFormat-DecimalSeparator="." NumberFormat-GroupSeparator="," OnTextChanged="txtSeguro_TextChanged">
                                    </telerik:RadNumericTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%">Otros:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadNumericTextBox ID="txtOtros" runat="server" MinValue="0" Value="0" NumberFormat-DecimalDigits="2"
                                        NumberFormat-DecimalSeparator="." NumberFormat-GroupSeparator="," OnTextChanged="txtOtros_TextChanged">
                                    </telerik:RadNumericTextBox>
                                </td>
                                <td style="width: 10%">Valor CIF:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadNumericTextBox ID="txtValorCIF" runat="server" Enabled="false" MinValue="0"
                                        NumberFormat-DecimalDigits="2" NumberFormat-DecimalSeparator="." NumberFormat-GroupSeparator=","
                                        Value="0">
                                    </telerik:RadNumericTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%">Documento Embarque:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadComboBox ID="cmbDocumentoEmbarque" runat="server" DataTextField="Descripcion"
                                        DataValueField="Codigo" Width="91%" />
                                </td>
                                <td style="width: 10%">Peso (Kgs):
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadNumericTextBox ID="txtPeso" Width="90%" runat="server" Value="0" MinValue="0"
                                        NumberFormat-DecimalDigits="2">
                                    </telerik:RadNumericTextBox>
                                </td>
                                <td class="style7">Cantidad Bultos:
                                </td>
                                <td class="style5">
                                    <telerik:RadNumericTextBox ID="txtCantidadBultos" runat="server" MinValue="0" NumberFormat-DecimalDigits="0"
                                        NumberFormat-GroupSeparator="," Value="0">
                                    </telerik:RadNumericTextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="Panel2" runat="server" GroupingText=" " Width="98%">
                        <table width="100%">
                            <tr>
                                <td style="width: 10%">País Procedencia:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadComboBox ID="cmbPaisProcedencia" runat="server" Width="90%" DataTextField="Descripcion"
                                        DataValueField="Codigo" Filter="StartsWith">
                                    </telerik:RadComboBox>
                                </td>
                                <td class="auto-style1">País Origen:
                                </td>
                                <td class="auto-style2">
                                    <telerik:RadComboBox ID="cmbPaisOrigen" runat="server" Width="90%" DataTextField="Descripcion"
                                        DataValueField="Codigo" Filter="StartsWith">
                                    </telerik:RadComboBox>
                                </td>
                                <td class="auto-style3">País Destino:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadComboBox ID="cmbPaisDestino" runat="server" Width="90%" DataTextField="Descripcion"
                                        DataValueField="Codigo" Filter="StartsWith">
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%">Ciudad Destino:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadTextBox ID="txtCiudadDestino" Width="80%" runat="server" Enabled="false">
                                    </telerik:RadTextBox><asp:ImageButton ID="btnAgregarCiudad" runat="server" ImageUrl="Images/24/add2_24.png"
                                        OnClick="btnAgregarCiudad_Click" />
                                </td>
                                <td class="auto-style1">Contenedores:
                                </td>
                                <td class="auto-style2">
                                    <telerik:RadNumericTextBox ID="txtContenedores" Width="80%" runat="server" MinValue="0"
                                        MaxValue="99" NumberFormat-DecimalDigits="0" Value="1" AutoPostBack="true" OnTextChanged="txtContenedores_TextChanged">
                                    </telerik:RadNumericTextBox><asp:ImageButton ID="btnAgregarContenedores" runat="server"
                                        ImageUrl="Images/24/add2_24.png" OnClick="btnAgregarContenedores_Click" />
                                </td>
                                <td class="auto-style3">Contenedores:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadTextBox ID="txtDescripcionContenedores" Width="90%" runat="server" MaxLength="250"
                                        Enabled="false">
                                    </telerik:RadTextBox>
                                </td>
                                <td style="width: 10%"></td>
                                <td style="width: 23%"></td>
                            </tr>
                            <tr>
                                <td style="width: 10%">Regimen:
                                </td>
                                <td colspan="1" style="width: 23%">
                                    <telerik:RadComboBox ID="cmbRegimen" runat="server" Width="90%" DataTextField="CodigoDescripcion"
                                        DataValueField="Codigo" Filter="StartsWith">
                                    </telerik:RadComboBox>
                                </td>

                                <td class="auto-style1">Gestor:
                                </td>
                                <td class="auto-style2">
                                    <telerik:RadComboBox ID="cmbUsuarioAduana" runat="server" Width="90%" DataTextField="NombreCompleto"
                                        DataValueField="IdUsuario" OnSelectedIndexChanged="cmbUsuarioAduana_SelectedIndexChanged">
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%">&nbsp;</td>
                                <td colspan="1" style="width: 23%">
                                    <asp:CheckBox ID="CBDua" runat="server" Text="Requeire DUA:" />
                                </td>
                                <td colspan="1" class="auto-style1">&nbsp;</td>
                                <td class="auto-style2">&nbsp;</td>
                                <td class="auto-style3">&nbsp;</td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <asp:Panel ID="Panel3" runat="server" GroupingText=" " Width="98%">
                        <table width="100%">
                            <tr>
                                <td style="width: 10%">Tipo Transporte:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadComboBox ID="cmbTipoTransporte" runat="server" Width="90%" DataTextField="Descripcion"
                                        DataValueField="Codigo">
                                    </telerik:RadComboBox>
                                </td>
                                <td style="width: 10%">Tipo Carga:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadComboBox ID="cmbTipoCargamento" runat="server" Width="90%" DataTextField="Descripcion"
                                        DataValueField="Codigo" Filter="Contains">
                                    </telerik:RadComboBox>
                                </td>
                                <td style="width: 10%">Tipo de Embalaje:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadComboBox ID="cmbTipoCarga" runat="server" Width="90%" DataTextField="Descripcion"
                                        DataValueField="Codigo">
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%">Fecha Venc. Tiempo Libre Naviera:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadDateTimePicker ID="dtpFechaVencTiempoLibreNaviera" runat="server" Width="90%"
                                        MinDate="1900-01-01">
                                    </telerik:RadDateTimePicker>
                                </td>
                                <td style="width: 10%">Fecha Venc. Tiempo Libre Puerto:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadDateTimePicker ID="dtpFechaVencTiempoLibrePuerto" runat="server" Width="90%"
                                        MinDate="1900-01-01">
                                    </telerik:RadDateTimePicker>
                                </td>
                                <td style="width: 10%">ETA:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadDateTimePicker ID="dtpETA" runat="server" Width="90%" MinDate="1900-01-01">
                                    </telerik:RadDateTimePicker>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%">Tiempo Estimado Entrega:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadDateTimePicker ID="dtpTiempoEstimadoEntrega" runat="server" Width="90%"
                                        MinDate="1900-01-01">
                                    </telerik:RadDateTimePicker>
                                </td>
                                <td style="width: 10%">Instrucciones Entrega:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadTextBox ID="txtInstruccionesEntrega" runat="server" TextMode="MultiLine"
                                        MaxLength="256" Width="90%">
                                    </telerik:RadTextBox>
                                </td>
                                <td style="width: 10%">Observación:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadTextBox ID="txtObservacion" runat="server" TextMode="MultiLine" MaxLength="256"
                                        Width="90%">
                                    </telerik:RadTextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <table width="100%">
                        <tr align="center">
                            <td colspan="6" align="center">
                                <asp:ImageButton ID="btnGuardar" ToolTip="Salvar" ImageUrl="~/Images/24/disk_blue_ok_24.png"
                                    OnClientClick="radconfirm('Esta seguro de Salvar?',confirmCallBackSalvar, 300, 100); return false;"
                                    runat="server" OnClick="btnGuardar_Click" /><asp:ImageButton ID="btnLimpiar" ToolTip="Limpiar Pantalla" ImageUrl="~/Images/24/document_plain_24.png"
                                        runat="server" OnClick="btnLimpiar_Click" /><asp:ImageButton ID="btnBack" ToolTip="Regresar"
                                            ImageUrl="~/Images/24/arrow_left_green_24.png" runat="server" CausesValidation="false"
                                            OnClick="btnBack_Click" /><asp:ImageButton ID="btnSalvar" OnClick="btnSalvar_Click"
                                                ImageUrl="~/Images/gris.png" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </telerik:RadPageView>
            <telerik:RadPageView ID="pvProductosClientes" runat="server">
                <table width="99.7%">
                    <tr>
                        <td>Productos del Cliente
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 45%">
                            <asp:TextBox ID="edDescripcion" runat="server" Width="99.7%"></asp:TextBox>
                        </td>
                        <td style="width: 5%">
                            <asp:Button ID="btnAgregar" runat="server" OnClick="btnAgregar_Click" Text="+" ToolTip="Agregar Producto" />
                        </td>
                        <td>Productos Seleccionados:
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 45%">
                            <telerik:RadListBox ID="lbDescripciones" runat="server" DataTextField="Descripcion"
                                DataValueField="Id" Width="99.7%" Height="300px" SelectionMode="Multiple" AllowTransfer="true"
                                TransferToID="lbDescripcionesRes" AutoPostBackOnTransfer="true" AllowReorder="true"
                                AutoPostBackOnReorder="true" EnableDragAndDrop="true" Skin="Windows7">
                                <Localization AllToLeft="Todo a la izquierda" AllToRight="Todo a la derecha" MoveDown="Bajar"
                                    MoveUp="Subir" ToLeft="Para la izquierda" ToRight="Para la derecha" />
                                <ButtonSettings TransferButtons="All" />
                            </telerik:RadListBox>
                        </td>
                        <td style="width: 5%" valign="top">
                            <asp:Button ID="btnEliminar" runat="server" OnClick="btnEliminar_Click" Text="-"
                                ToolTip="Eliminar Producto Seleccionado" />
                        </td>
                        <td style="width: 45%">
                            <telerik:RadListBox ID="lbDescripcionesRes" runat="server" Width="99.7%" Height="300px"
                                SelectionMode="Multiple" AllowReorder="true" AutoPostBackOnReorder="true" EnableDragAndDrop="true"
                                Skin="Windows7">
                                <Localization MoveDown="Bajar" MoveUp="Subir" />
                            </telerik:RadListBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="5">
                            <asp:ImageButton ID="btnRegresar" runat="server" ImageUrl="~/Images/24/arrow_left_green_24.png"
                                OnClick="btnRegresar_Click" ToolTip="Regresar" />
                        </td>
                    </tr>
                </table>
            </telerik:RadPageView>
            <telerik:RadPageView ID="pvProveedoresPais" runat="server">
                <table width="99.7%">
                    <tr>
                        <td>Proveedores por País
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 45%">
                            <asp:TextBox ID="edDescripcionProveedor" runat="server" Width="99.7%"></asp:TextBox>
                        </td>
                        <td style="width: 5%">
                            <asp:Button ID="btnAddProv" runat="server" Text="+" ToolTip="Agregar Proveedor" OnClick="btnAddProv_Click" />
                        </td>
                        <td></td>
                        <td></td>
                        <td>&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 45%">
                            <telerik:RadListBox ID="lbProveedores" runat="server" DataTextField="Descripcion"
                                DataValueField="Codigo" Width="99.7%" Height="300px" SelectionMode="Single" Skin="Windows7">
                            </telerik:RadListBox>
                        </td>
                        <td style="width: 5%" valign="top">
                            <asp:Button ID="btnDelProv" runat="server" Text="-" ToolTip="Eliminar Proveedor Seleccionado"
                                OnClick="btnDelProv_Click" />
                        </td>
                        <td></td>
                        <td style="width: 5%; text-align: right; vertical-align: middle" valign="top">
                            <br />
                        </td>
                        <td style="width: 45%">&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:ImageButton ID="btnRegresarProv" runat="server" ImageUrl="~/Images/24/arrow_left_green_24.png"
                                ToolTip="Regresar" OnClick="btnRegresarProv_Click" />
                        </td>
                    </tr>
                </table>
            </telerik:RadPageView>
            <telerik:RadPageView ID="pvContenedores" runat="server">
                <table border="2" width="25%">
                    <tr>
                        <td style="width: 40%;" align="center">No. Contenedor
                        </td>
                        <td style="width: 60%;" align="center">Descripción
                        </td>
                    </tr>
                    <%int cantidad = Convert.ToInt16(txtContenedores.Value);
                      for (int i = 1; i <= cantidad; i++)
                      {%>
                    <tr>
                        <td align="center">
                            <%=i %>
                        </td>
                        <%if (descCont != null)
                              //if (descCont.Length <= cantidad)
                              //{
                              if (i <= descCont.Length)
                              {
                        %>
                        <td align="center">
                            <input name="txtDescContenedores" type="text" value="<%=descCont[i-1].ToString()%>" />
                        </td>
                        <%}
                              else
                              {%>
                        <td align="center">
                            <input name="txtDescContenedores" type="text" />
                        </td>
                        <%}
                          //}
                          //else
                          //{
                        %>
                        <%-- <td align="center">
                            <input name="txtDescContenedores" type="text" />
                        </td>--%>
                        <%//}%>
                    </tr>
                    <%}%>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:ImageButton ID="btnRegresarContenedor" runat="server" ImageUrl="~/Images/24/arrow_left_green_24.png"
                                ToolTip="Regresar" OnClick="btnRegresarContenedor_Click" />
                        </td>
                    </tr>
                </table>
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageView1" runat="server">
                <table width="99.7%">
                    <tr>
                        <td>Ciudad Destino
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 45%">
                            <asp:TextBox ID="edDescripcionCiudad" runat="server" Width="99.7%"></asp:TextBox>
                        </td>
                        <td style="width: 5%">
                            <asp:Button ID="btnAddCiudad" runat="server" Text="+" ToolTip="Agregar Ciudad" OnClick="btnAddCiudad_Click" />
                        </td>
                        <td></td>
                        <td></td>
                        <td>&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 45%">
                            <telerik:RadListBox ID="lbCiudades" runat="server" DataTextField="Descripcion" DataValueField="Codigo"
                                Height="300px" SelectionMode="Single" Width="99.7%">
                            </telerik:RadListBox>
                        </td>
                        <td style="width: 5%" valign="top">
                            <asp:Button ID="btnDelCiudad" runat="server" Text="-" ToolTip="Eliminar Ciudad Seleccionada"
                                OnClick="btnDelCiudad_Click" />
                        </td>
                        <td></td>
                        <td style="width: 5%; text-align: right; vertical-align: middle" valign="top">
                            <br />
                        </td>
                        <td style="width: 45%">&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:ImageButton ID="btnBackCiudad" runat="server" ImageUrl="~/Images/24/arrow_left_green_24.png"
                                ToolTip="Regresar" OnClick="btnBackCiudad_Click" />
                        </td>
                    </tr>
                </table>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </telerik:RadAjaxPanel>
    <telerik:RadAjaxManager ID="AjaxManager" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
        <AjaxSettings>
            <%-- ///////////////////////////////////////////Cliente///////////////////////////////////////////--%>
            <telerik:AjaxSetting AjaxControlID="cmbClientes">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="cmbDivision" />
                    <telerik:AjaxUpdatedControl ControlID="cmbProductoCargill" />
                    <telerik:AjaxUpdatedControl ControlID="cmbCargaCargill" />
                    <telerik:AjaxUpdatedControl ControlID="txtOficialCuenta" />
                    <telerik:AjaxUpdatedControl ControlID="txtRTN" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%-- /////////////////////////////////////////////////////////////////////////////////////////////--%>
            <%-- ///////////////////////////////////////////Aduana///////////////////////////////////////////--%>
            <telerik:AjaxSetting AjaxControlID="cmbAduanas">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="cmbUsuarioAduana" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%-- /////////////////////////////////////////////////////////////////////////////////////////////--%>
            <%-- ///////////////////////////////////////////ValorCIF///////////////////////////////////////////--%>
            <telerik:AjaxSetting AjaxControlID="txtValorFOB">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="txtValorCIF" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="txtFlete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="txtValorCIF" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="txtSeguro">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="txtValorCIF" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="txtOtros">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="txtValorCIF" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <%-- /////////////////////////////////////////////////////////////////////////////////////////////--%>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" />
</asp:Content>
