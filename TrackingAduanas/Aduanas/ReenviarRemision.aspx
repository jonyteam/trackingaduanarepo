﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReenviarRemision.aspx.cs" Inherits="EnviarRemision" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
.RadInput_Default{font:12px "segoe ui",arial,sans-serif}.RadInput{vertical-align:middle;
        }
        .auto-style2 {
            width: 537px;
        }
        .auto-style6 {
            width: 357px;
            }
        .auto-style7 {
            height: 36px;
            width: 303px;
        }
        .auto-style8 {
            width: 357px;
            height: 30px;
        }
        .auto-style12 {
            width: 145px;
            height: 20px;
        }
        .auto-style14 {
            width: 303px;
        }
        .auto-style15 {
            width: 373px;
            height: 36px;
        }
        .auto-style16 {
            width: 379px;
            height: 20px;
        }
        .auto-style17 {
            width: 203px;
            height: 30px;
        }
        .auto-style18 {
            height: 36px;
            width: 203px;
        }
        .auto-style19 {
            width: 203px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <br />
 <%--   <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    </asp:UpdatePanel>--%>
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Width="100%" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart" Style="text-align: left">
          <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                    //<![CDATA[
                    
                function pnlRequestStarted(ajaxPanel, eventArgs) {
                    try {
                        if ((eventArgs.EventTarget == "<%= btnSalvar.UniqueID %>")) {
                            eventArgs.EnableAjax = false;
                        }
                    }
                    catch (err)
                { }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                    }
                }
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }
                   
            </script>
        </telerik:RadScriptBlock>





        <telerik:RadTabStrip ID="RadTabStrip1" runat="server" SelectedIndex="0" MultiPageID="RadMultiPage1" OnTabClick="RadTabStrip1_TabClick">
            <Tabs>
                <telerik:RadTab runat="server" Text="Buscar por Hojas de Ruta " PageViewID="bxhr" Selected="True" />
                <telerik:RadTab runat="server" Text="Buscar por Aduanas" PageViewID="bxa" />
            </Tabs>
      </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" Width="99%">
            <telerik:RadPageView ID="bxhr" runat="server" Width="100%">
                 <table class="auto-style1">
                    <tr>
                        <td class="auto-style12">Hoja de ruta :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; </td>
                        <td class="auto-style16">
                            <telerik:RadTextBox ID="txtInstruccion" runat="server" EmptyMessage="Hoja Ruta" Height="17px" LabelWidth="40%" MaxLength="20" Width="163px">
                            </telerik:RadTextBox>
                            <asp:ImageButton ID="ImageButton3" runat="server" Height="26px" ImageUrl="~/Images/24/view_24.png" OnClick="Unnamed1_Click" Width="29px" />
                        </td>
                        <td class="auto-style6">&nbsp;</td>
                        <td class="auto-style7">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style2" colspan="2">&nbsp;</td>
                        <td class="auto-style14">&nbsp;</td>
                    </tr>
                </table>
            </telerik:RadPageView>
            <telerik:RadPageView ID="bxa" runat="server" Width="100%">
                 <table class="auto-style1">
                    <tr>
                        <td class="auto-style18">Aduanas :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                        <td class="auto-style15">
                            <telerik:RadComboBox ID="cmbAduana" runat="server" AutoPostBack="true" DataTextField="NombreAduana" DataValueField="CodigoAduana" OnSelectedIndexChanged="cmbAduana_SelectedIndexChanged" Width="200px" />
                            &nbsp;
                            <asp:ImageButton ID="ImageButton2" runat="server" Height="26px" ImageUrl="~/Images/24/view_24.png" OnClick="Unnamed1_Click1" Width="29px" />
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style17">
                            Numero de Remision&nbsp;:&nbsp;
                        </td>
                        <td class="auto-style8">
                            <telerik:RadComboBox ID="RadRemisiones" Runat="server" AutoPostBack="True" OnSelectedIndexChanged="RadRemisiones_SelectedIndexChanged" Width="200px">
                            </telerik:RadComboBox>
                            &nbsp; </td>
                    </tr>
                    <tr>
                        <td class="auto-style17"># Ultimas Remisones:</td>
                        <td class="auto-style8">
                            <telerik:RadNumericTextBox ID="RadTop" Runat="server" Culture="es-HN" DbValueFactor="1" Height="16px" LabelWidth="34px" OnTextChanged="RadTop_TextChanged" Value="10" Width="52px">
                            </telerik:RadNumericTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style19">&nbsp;</td>
                        <td class="auto-style2">&nbsp;</td>
                        <td class="auto-style14">&nbsp;</td>
                    </tr>
                </table>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
        <telerik:RadGrid ID="rgInstrucciones" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            GridLines="None" Width="99.9%" Height="420px" CellSpacing="0" AllowPaging="True"
            OnNeedDataSource="rgInstrucciones_NeedDataSource" ShowFooter="True" ShowStatusBar="True"
            PageSize="400" Culture="es-ES">
            <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
            <ClientSettings>
                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
            </ClientSettings>
            <MasterTableView>
                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </RowIndicatorColumn>
                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="NumeroRemision" FilterControlAltText="Filter Idremision column" HeaderText="Remision " UniqueName="Idremision">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column column" HeaderText="Instruccion No."
                        UniqueName="IdInstruccion" DataField="HojaRuta">
                        <HeaderStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Serie" FilterControlAltText="Filter Serie column"
                        HeaderText="Serie" UniqueName="Serie">
                        <HeaderStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CodEspecieFiscal" FilterControlAltText="Filter CodEspecieFiscal column" HeaderText="CodigoSerie" UniqueName="CodEspecieFiscal">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Nombre" FilterControlAltText="Filter column1 column" HeaderText="Cliente" UniqueName="Nombre">
                        <HeaderStyle Width="250px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NumeroFactura" FilterControlAltText="Filter column2 column" HeaderText="Factura" UniqueName="NumeroFactura">
                        <HeaderStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NoCorrelativo" FilterControlAltText="Filter column3 column" HeaderText="Correlativo" UniqueName="NoCorrelativo">
                        <HeaderStyle Width="120px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Proveedor" FilterControlAltText="Filter column4 column" HeaderText="Proveedor" UniqueName="Proveedor">
                        <HeaderStyle Width="250px" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        
                
    </telerik:RadAjaxPanel>

        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" DefaultLoadingPanelID="RadAjaxLoadingPanel1" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
            <AjaxSettings>


                <%-- Cargar ciudades Origen --%>
                <telerik:AjaxSetting AjaxControlID="cmbAduana">
                    <UpdatedControls>

                        <telerik:AjaxUpdatedControl ControlID="RadRemisiones" LoadingPanelID="LoadingPanel" />
                        <telerik:AjaxUpdatedControl ControlID="rgInstrucciones" LoadingPanelID="LoadingPanel" />

                        <%--<telerik:Ajax
                         dControl ControlID="DbCo" />--%>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <%-- Cargar ciudades Destino --%>

                <telerik:AjaxSetting AjaxControlID="RadRemisiones">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="rgInstrucciones" LoadingPanelID="LoadingPanel" />
                        <%--<telerik:AjaxUpdatedControl ControlID="RcD" LoadingPanelID="LoadingPanel" />--%>
                    </UpdatedControls>
                </telerik:AjaxSetting>

                <%-- Carga Origen --%>
                <telerik:AjaxSetting AjaxControlID="ImageButton1">
                    <UpdatedControls>
                           <telerik:AjaxUpdatedControl ControlID="rgInstrucciones" LoadingPanelID="LoadingPanel" />


                    </UpdatedControls>
                </telerik:AjaxSetting>
                <%-- Carga Origen --%>
              
            </AjaxSettings>
        </telerik:RadAjaxManager>
    <p>
    </p>

        <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="~/Images/gris.png" OnClick="btnSalvar_Click" />
          
    <table class="auto-style1">
        <tr>
            <td style="text-align: center">

      <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Images/24/disk_blue_ok_24.png"
                        OnClientClick="radconfirm('Esta seguro que Desea Generar el Reporte?',confirmCallBackSalvar, 300, 100); return false;"
                        ToolTip="Salvar" OnClick="btnGuardar_Click" style="height: 24px" />
            </td>
        </tr>
    </table>
          
</asp:Content>
