﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteDiario.aspx.cs" Inherits="GenerarArchivoSAPDinant" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                    //<![CDATA[
                    
                function pnlRequestStarted(ajaxPanel, eventArgs) {
                    try {
                        if ((eventArgs.EventTarget == "<%= btnSalvar.UniqueID %>")) {
                            eventArgs.EnableAjax = false;
                        }
                    }
                    catch (err)
                { }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                    }
                }
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }
                   
            </script>
        </telerik:RadScriptBlock>
        <table border="1" style="width: 45%">
            <tr>
                <td style="width: 15%">
                    <asp:Label ID="lblFechaInicio0" runat="server" Text="Fecha Inicio:" 
                        ForeColor="Black">
                </asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadDatePicker ID="dpFechaInicio" runat="server" EnableTyping="False" Width="95%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="dpFechaInicio"
                        ErrorMessage="Ingrese Fecha" ForeColor="Red" />
                </td>
                <td style="width: 15%">
                    <asp:Label ID="lblFechaFinal0" runat="server" Text="Fecha Final:" 
                        ForeColor="Black"></asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadDatePicker ID="dpFechaFinal" runat="server" EnableTyping="False" Width="95%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator runat="server" ID="reqName0" ControlToValidate="dpFechaFinal"
                        ErrorMessage="Ingrese Fecha" ForeColor="Red" />
                </td>
                <td style="width: 10%">
                    <asp:Label ID="lblPais" runat="server" forecolor="Black" text="País:"></asp:Label>
                </td>
                <td style="width: 16%">
                    <telerik:RadComboBox ID="cmbPais" runat="server" AutoPostBack="true" 
                        DataTextField="Descripcion" DataValueField="Codigo" 
                        OnSelectedIndexChanged="cmbPais_SelectedIndexChanged">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" forecolor="Black" text="Cliente:"></asp:Label>
                </td>
                <td colspan="5">
                    <telerik:RadComboBox ID="cmbCliente" runat="server" DataTextField="Nombre" 
                        DataValueField="CodigoSAP" Filter="StartsWith" Width="100%">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="6">
                    <asp:ImageButton ID="btnBuscar" runat="server" 
                        imageurl="~/Images/24/view_24.png" onclick="btnBuscar_Click" />
                </td>
            </tr>
        </table>
        <br />
        <telerik:RadGrid ID="RadGrid1" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" 
            GridLines="None" Width="5502px" onitemcommand="RadGrid1_ItemCommand" 
            onneeddatasource="RadGrid1_NeedDataSource" PageSize="20" 
            BorderStyle="Solid">
            <ClientSettings AllowColumnsReorder="True">
                <Selecting AllowRowSelect="True" />
            </ClientSettings>
             <ExportSettings HideStructureColumns="true" 
                FileName ="Reporte" OpenInNewWindow="True">  <Excel Format ="Biff" 
                     AutoFitImages="True" />  </ExportSettings>
            <MasterTableView CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" ExportToPdfText="Export to PDF" ShowAddNewRecordButton = "false" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                    Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                    Visible="True">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="IdInstruccion" 
                        FilterControlAltText="Filter IdInstruccion column" HeaderText="Hoja Ruta" 
                        UniqueName="IdInstruccion">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NombreAduana" 
                        FilterControlAltText="Filter IdInstruccion column" HeaderText="Nombre Aduana" 
                        UniqueName="NombreAduana">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Fecha" 
                        FilterControlAltText="Filter Proveedor column" HeaderText="Fecha de Factura" 
                        UniqueName="Fecha">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Proveedor" 
                        FilterControlAltText="Filter Nombre column" HeaderText="Proveedor" 
                        UniqueName="Proveedor">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NumeroFactura" 
                        FilterControlAltText="Filter Fecha column" HeaderText="Numero de Factura" 
                        UniqueName="NumeroFactura">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Producto" 
                        FilterControlAltText="Filter DocumentoNo column" 
                        HeaderText="Descripcion Producto" UniqueName="Producto">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ValorCIF" 
                        FilterControlAltText="Filter DocumentoNo column" HeaderText="Valor CIF" 
                        UniqueName="ValorCIF">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Cambio" 
                        FilterControlAltText="Filter PesoKgs column" HeaderText="Tasa de Cambio" 
                        UniqueName="Cambio">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PosicionArancelaria" 
                        FilterControlAltText="Filter PesoMiami column" HeaderText="Posicion Arancelaria" 
                        UniqueName="PosicionArancelaria">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NoCorrelativo" 
                        FilterControlAltText="Filter IngresoSwissport column" 
                        HeaderText="Correlativo" UniqueName="NoCorrelativo">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FechaAceptacion" 
                        FilterControlAltText="Filter SalidaSwissport column" 
                        HeaderText="Fecha Aceptacion DUA" UniqueName="FechaAceptacion">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Descripcion" 
                        FilterControlAltText="Filter Producto column" HeaderText="Nombre Regimen" 
                        UniqueName="Descripcion">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ISV" 
                        FilterControlAltText="Filter GC1.Fecha column" HeaderText="ISV" 
                        UniqueName="ISV">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DAI" 
                        FilterControlAltText="Filter ArribodeCarga column" HeaderText="DAI" 
                        UniqueName="DAI">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Selectivo" 
                        FilterControlAltText="Filter ENVIODEPRELIQUIDACION column" 
                        HeaderText="Selectivo" UniqueName="Selectivo">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="STD" 
                        FilterControlAltText="Filter APROBACIONDEPRELIQUIDACION column" 
                        HeaderText="STD" 
                        UniqueName="STD">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Produccion" 
                        FilterControlAltText="Filter ENVIODEBOLETIN column" 
                        HeaderText="Produccion" UniqueName="Produccion">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="OtrosImpuestos" 
                        FilterControlAltText="Filter OtrosImpuestos column" 
                        HeaderText="Otros Impuestos" UniqueName="OtrosImpuestos">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Total" 
                        FilterControlAltText="Filter PagodeImpuestos column" 
                        HeaderText="Total Impuestos" UniqueName="Total">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ISVEXONERADOS" 
                        FilterControlAltText="Filter Color column" HeaderText="ISV EXONERADO" 
                        UniqueName="ISVEXONERADOS">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DAIEXONERADOS" 
                        FilterControlAltText="Filter RevisionMercancia column" 
                        HeaderText="DAI EXONERADOS" UniqueName="DAIEXONERADOS">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="SelectivoEXONERADOS" 
                        FilterControlAltText="Filter EmisionPaseSalida column" 
                        HeaderText="Selectivo EXONERADOS" UniqueName="SelectivoEXONERADOS">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="STDEXONERADOS" 
                        FilterControlAltText="Filter STDEXONERADOS column" 
                        HeaderText="STD EXONERADOS" UniqueName="STDEXONERADOS">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ProduccionEXONERADOS" 
                        FilterControlAltText="Filter ProduccionEXONERADOS column" HeaderText="Produccion EXONERADOS" 
                        UniqueName="ProduccionEXONERADOS">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="OtrosImpuestosEXONERADOS" 
                        FilterControlAltText="Filter OtrosImpuestosEXONERADOS column" 
                        HeaderText="Otros Impuestos EXONERADOS" UniqueName="OtrosImpuestosEXONERADOS">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TotalEXONERADOS" 
                        FilterControlAltText="Filter TotalEXONERADOS column" HeaderText="TOTAL EXONERADOS" 
                        UniqueName="TotalEXONERADOS">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <ItemStyle BorderColor="Black" BorderStyle="Solid" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        <br />
        <br />
        <table width="100%">
            <tr align="center">
                <td colspan="6" align="center">
                    <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="~/Images/gris.png"/>
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
    <%--<GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>--%>
</asp:Content>
