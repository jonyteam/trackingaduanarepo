﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;

using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Office.Interop.Excel;
using System.Xml.Linq;
using System.Web.SessionState;
using System.Collections.Specialized;
using GrupoLis.Login;
using System.Text.RegularExpressions;
using System.Security.Principal;
using Telerik.Web;
using System.Security.Cryptography;


public partial class GestionCarga : Utilidades.PaginaBase
{
    private NameValueCollection mParamethers;
    private GrupoLis.Login.Login logAppAduanas;
    private GrupoLis.Login.Login logAppTerrestre;

    protected override void OnLoad(EventArgs e)
    {
        MODULO = "GestionCarga";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        mParamethers = ConfigurationManager.AppSettings;
        SetGridFilterMenu(rgInstrucciones.FilterMenu);
        rgInstrucciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgInstrucciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            if (!Ingresar)
                redirectTo("Default.aspx");
            rgInstrucciones.ClientSettings.Scrolling.AllowScroll = true;
            rgInstrucciones.ClientSettings.Scrolling.UseStaticHeaders = true;
            rgInstrucciones.ShowHeader = true;
            rgInstrucciones.ShowFooter = true;
            ((SiteMaster)Master).SetTitulo("Gestión Carga", "Gestión Carga");
        }
    }

    protected void rgInstrucciones_Init(object sender, System.EventArgs e)
    {
        GridFilterMenu menu = rgInstrucciones.FilterMenu;
        menu.Items.RemoveAt(rgInstrucciones.FilterMenu.Items.Count - 2);
    }

    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }

    protected void rgInstrucciones_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridInstrucciones();
    }

    private void llenarGridInstrucciones()
    {
        try
        {
            conectar();
            InstruccionesBO ins = new InstruccionesBO(logApp);
            GestionCargaBO gc = new GestionCargaBO(logApp);
            //DataTable dt = new DataTable();
            UsuariosBO u = new UsuariosBO(logApp);
            if (User.IsInRole("Administradores"))
                ins.loadInstruccionesAll();
            else if (User.IsInRole("Jefes Aduana"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.totalRegistros > 0)
                    ins.loadInstruccionesAllXAduana(u.CODIGOADUANA);
            }
            else if (User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT"))
            {
                string idU = Session["IdUsuario"].ToString();
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.totalRegistros > 0)
                    if (u.CODPAIS == "H")
                         if (User.Identity.Name == "eizaguirre")
                         { ins.loadInstruccionesAllXAduanaUsuario("016", idU); }
                         else { ins.loadInstruccionesAllXAduanaUsuario(u.CODIGOADUANA, idU); }

                
                    else
                        //ins.loadInstruccionesAllXUsuario(idU);
                        ins.loadInstruccionesAllXPaisSVGTNI(u.CODPAIS);
            }
            rgInstrucciones.DataSource = ins.TABLA;

            #region nada
            //conectar();
            //InstruccionesBO ins = new InstruccionesBO(logApp);
            //GestionCargaBO gc = new GestionCargaBO(logApp);
            //DataTable dt = new DataTable();
            //ins.loadInstrucciones();
            //if (ins.totalRegistros > 0)
            //{
            //    dt.Columns.Add("IdInstruccion", typeof(String));
            //    dt.Columns.Add("EstadoFlujo", typeof(String));
            //    for (int i = 0; i < ins.totalRegistros; i++)
            //    {

            //    }
            //}
            //dt.AcceptChanges();
            //rgInstrucciones.DataSource = dt;
            //rgInstrucciones.DataBind();

            //DataTable dt = new DataTable();
            //bool agregado = false;
            //g.loadAllGuiasFlujo();
            //if (g.totalRegistros > 0)
            //{
            //    dt.Columns.Add("CorrelativoGuia", typeof(String));
            //    dt.Columns.Add("NombreEmpresa", typeof(String));
            //    dt.Columns.Add("CodigoRuta", typeof(String));
            //    dt.Columns.Add("CiudadOrigen", typeof(String));
            //    dt.Columns.Add("CiudadDestino", typeof(String));
            //    dt.Columns.Add("Motorista", typeof(String));
            //    dt.Columns.Add("CelMotoristaHN", typeof(String));
            //    dt.Columns.Add("PlacaCamion", typeof(String));
            //    dt.Columns.Add("Estado", typeof(String));
            //    dt.Columns.Add("CodEstado", typeof(String));
            //    dt.Columns.Add("CodigoAduana", typeof(String));
            //    dt.Columns.Add("Fecha", typeof(String));
            //    dt.Columns.Add("Producto", typeof(String));
            //    dt.Columns.Add("Referencia1", typeof(String));
            //    dt.Columns.Add("Referencia2", typeof(String));
            //    dt.Columns.Add("Referencia3", typeof(String));
            //    dt.Columns.Add("Referencia4", typeof(String));
            //    for (int i = 0; i < g.totalRegistros; i++)
            //    {
            //        ar.loadAduanasXRuta(g.CODIGORUTA);
            //        if (ar.totalRegistros > 0)
            //        {
            //            tf.getMaxCorrelativo(g.CORRELATIVOGUIA);
            //            if (tf.totalRegistros > 0)
            //            {
            //                agregado = false;
            //                int cont1 = 5, cont2 = 6;
            //                for (int h = 1; h <= ar.totalRegistros; h++)
            //                {
            //                    if (tf.CORRELATIVOGUIA == cont1.ToString() || tf.CORRELATIVOGUIA == cont2.ToString())
            //                    {
            //                        a.loadAduanas(ar.CODIGOADUANA);
            //                        if (a.totalRegistros > 0)
            //                        {
            //                            ///////////nuevo/////////////
            //                            if (User.IsInRole("Supervisores"))
            //                                g2.loadFLujoGuiasSupervisores(g.CORRELATIVOGUIA, a.NOMBREADUANA, g.MONITOREOREMOTOINICIO, g.MONITOREOREMOTOFIN);
            //                            else if (User.IsInRole("Monitoreo"))
            //                                g2.loadFLujoGuiasMonitoreo(g.CORRELATIVOGUIA, a.NOMBREADUANA, g.MONITOREOREMOTOINICIO, g.MONITOREOREMOTOFIN);
            //                            else if (User.IsInRole("Administradores"))
            //                                g2.loadFLujoGuias(g.CORRELATIVOGUIA, a.NOMBREADUANA);
            //                            else
            //                                g2.loadFLujoGuias("", "");
            //                            if (g2.totalRegistros > 0)
            //                            {
            //                                DataRow dr = dt.NewRow();
            //                                dr["CorrelativoGuia"] = g2.CORRELATIVOGUIA;
            //                                dr["NombreEmpresa"] = g2.TABLA.Rows[0]["NombreEmpresa"].ToString();
            //                                dr["CodigoRuta"] = g2.CODIGORUTA;
            //                                dr["CiudadOrigen"] = g2.TABLA.Rows[0]["CiudadOrigen"].ToString();
            //                                dr["CiudadDestino"] = g2.TABLA.Rows[0]["CiudadDestino"].ToString();
            //                                dr["Motorista"] = g2.TABLA.Rows[0]["Motorista"].ToString();
            //                                dr["CelMotoristaHN"] = g2.TABLA.Rows[0]["CelMotoristaHN"].ToString();
            //                                dr["PlacaCamion"] = g2.TABLA.Rows[0]["PlacaCamion"].ToString();
            //                                dr["Estado"] = g2.TABLA.Rows[0]["Estado"].ToString();
            //                                dr["CodEstado"] = g2.CODESTADO;
            //                                dr["CodigoAduana"] = ar.CODIGOADUANA;
            //                                dr["Fecha"] = g2.FECHA;
            //                                dr["Producto"] = g2.PRODUCTO;
            //                                dr["Referencia1"] = g2.REFERENCIA1;
            //                                dr["Referencia2"] = g2.REFERENCIA2;
            //                                dr["Referencia3"] = g2.REFERENCIA3;
            //                                dr["Referencia4"] = g2.REFERENCIA4;
            //                                dt.Rows.Add(dr);
            //                                agregado = true;
            //                            }
            //                        }
            //                    }
            //                    cont1 += 2;
            //                    cont2 += 2;
            //                    ar.regSiguiente();
            //                }
            //                if (agregado == false)
            //                {//////////////////////////////////////
            //                    ///////////nuevo/////////////
            //                    if (User.IsInRole("Supervisores"))
            //                        g2.loadFLujoGuiasSupervisores(g.CORRELATIVOGUIA, "", g.MONITOREOREMOTOINICIO, g.MONITOREOREMOTOFIN);
            //                    else if (User.IsInRole("Monitoreo"))
            //                        g2.loadFLujoGuiasMonitoreo(g.CORRELATIVOGUIA, "", g.MONITOREOREMOTOINICIO, g.MONITOREOREMOTOFIN);
            //                    else if (User.IsInRole("Administradores"))
            //                        g2.loadFLujoGuias(g.CORRELATIVOGUIA, "");
            //                    else
            //                        g2.loadFLujoGuias("", "");
            //                    if (g2.totalRegistros > 0)
            //                    {
            //                        DataRow dr = dt.NewRow();
            //                        dr["CorrelativoGuia"] = g2.CORRELATIVOGUIA;
            //                        dr["NombreEmpresa"] = g2.TABLA.Rows[0]["NombreEmpresa"].ToString();
            //                        dr["CodigoRuta"] = g2.CODIGORUTA;
            //                        dr["CiudadOrigen"] = g2.TABLA.Rows[0]["CiudadOrigen"].ToString();
            //                        dr["CiudadDestino"] = g2.TABLA.Rows[0]["CiudadDestino"].ToString();
            //                        dr["Motorista"] = g2.TABLA.Rows[0]["Motorista"].ToString();
            //                        dr["CelMotoristaHN"] = g2.TABLA.Rows[0]["CelMotoristaHN"].ToString();
            //                        dr["PlacaCamion"] = g2.TABLA.Rows[0]["PlacaCamion"].ToString();
            //                        dr["Estado"] = g2.TABLA.Rows[0]["Estado"].ToString();
            //                        dr["CodEstado"] = g2.CODESTADO;
            //                        dr["CodigoAduana"] = "";
            //                        dr["Fecha"] = g2.FECHA;
            //                        dr["Producto"] = g2.PRODUCTO;
            //                        dr["Referencia1"] = g2.REFERENCIA1;
            //                        dr["Referencia2"] = g2.REFERENCIA2;
            //                        dr["Referencia3"] = g2.REFERENCIA3;
            //                        dr["Referencia4"] = g2.REFERENCIA4;
            //                        dt.Rows.Add(dr);
            //                    }
            //                }
            //            }
            //            else
            //            {
            //                ///////////nuevo/////////////
            //                if (User.IsInRole("Supervisores"))
            //                    g2.loadFLujoGuiasSupervisores(g.CORRELATIVOGUIA, "", g.MONITOREOREMOTOINICIO, g.MONITOREOREMOTOFIN);
            //                else if (User.IsInRole("Monitoreo"))
            //                    g2.loadFLujoGuiasMonitoreo(g.CORRELATIVOGUIA, "", g.MONITOREOREMOTOINICIO, g.MONITOREOREMOTOFIN);
            //                else if (User.IsInRole("Administradores"))
            //                    g2.loadFLujoGuias(g.CORRELATIVOGUIA, "");
            //                else
            //                    g2.loadFLujoGuias("", "");
            //                if (g2.totalRegistros > 0)
            //                {
            //                    DataRow dr = dt.NewRow();
            //                    dr["CorrelativoGuia"] = g2.CORRELATIVOGUIA;
            //                    dr["NombreEmpresa"] = g2.TABLA.Rows[0]["NombreEmpresa"].ToString();
            //                    dr["CodigoRuta"] = g2.CODIGORUTA;
            //                    dr["CiudadOrigen"] = g2.TABLA.Rows[0]["CiudadOrigen"].ToString();
            //                    dr["CiudadDestino"] = g2.TABLA.Rows[0]["CiudadDestino"].ToString();
            //                    dr["Motorista"] = g2.TABLA.Rows[0]["Motorista"].ToString();
            //                    dr["CelMotoristaHN"] = g2.TABLA.Rows[0]["CelMotoristaHN"].ToString();
            //                    dr["PlacaCamion"] = g2.TABLA.Rows[0]["PlacaCamion"].ToString();
            //                    dr["Estado"] = g2.TABLA.Rows[0]["Estado"].ToString();
            //                    dr["CodEstado"] = g2.CODESTADO;
            //                    dr["CodigoAduana"] = "";
            //                    dr["Fecha"] = g2.FECHA;
            //                    dr["Producto"] = g2.PRODUCTO;
            //                    dr["Referencia1"] = g2.REFERENCIA1;
            //                    dr["Referencia2"] = g2.REFERENCIA2;
            //                    dr["Referencia3"] = g2.REFERENCIA3;
            //                    dr["Referencia4"] = g2.REFERENCIA4;
            //                    dt.Rows.Add(dr);
            //                }
            //            }
            //            //g.regSiguiente();
            //        }
            //        else
            //        {
            //            ///////////nuevo/////////////
            //            if (User.IsInRole("Supervisores"))
            //                g2.loadFLujoGuiasSupervisores(g.CORRELATIVOGUIA, "", g.MONITOREOREMOTOINICIO, g.MONITOREOREMOTOFIN);
            //            else if (User.IsInRole("Monitoreo"))
            //                g2.loadFLujoGuiasMonitoreo(g.CORRELATIVOGUIA, "", g.MONITOREOREMOTOINICIO, g.MONITOREOREMOTOFIN);
            //            else if (User.IsInRole("Administradores"))
            //                g2.loadFLujoGuias(g.CORRELATIVOGUIA, "");
            //            else
            //                g2.loadFLujoGuias("", "");
            //            if (g2.totalRegistros > 0)
            //            {
            //                DataRow dr = dt.NewRow();
            //                dr["CorrelativoGuia"] = g2.CORRELATIVOGUIA;
            //                dr["NombreEmpresa"] = g2.TABLA.Rows[0]["NombreEmpresa"].ToString();
            //                dr["CodigoRuta"] = g2.CODIGORUTA;
            //                dr["CiudadOrigen"] = g2.TABLA.Rows[0]["CiudadOrigen"].ToString();
            //                dr["CiudadDestino"] = g2.TABLA.Rows[0]["CiudadDestino"].ToString();
            //                dr["Motorista"] = g2.TABLA.Rows[0]["Motorista"].ToString();
            //                dr["CelMotoristaHN"] = g2.TABLA.Rows[0]["CelMotoristaHN"].ToString();
            //                dr["PlacaCamion"] = g2.TABLA.Rows[0]["PlacaCamion"].ToString();
            //                dr["Estado"] = g2.TABLA.Rows[0]["Estado"].ToString();
            //                dr["CodEstado"] = g2.CODESTADO;
            //                dr["CodigoAduana"] = "";
            //                dr["Fecha"] = g2.FECHA;
            //                dr["Producto"] = g2.PRODUCTO;
            //                dr["Referencia1"] = g2.REFERENCIA1;
            //                dr["Referencia2"] = g2.REFERENCIA2;
            //                dr["Referencia3"] = g2.REFERENCIA3;
            //                dr["Referencia4"] = g2.REFERENCIA4;
            //                dt.Rows.Add(dr);
            //            }
            //        }
            //        g.regSiguiente();
            //    }
            //}
            //dt.AcceptChanges();
            //rgGuias.DataSource = dt;
            ////rgGuias.DataBind();
            #endregion

        }
        catch (Exception) { }
        finally
        {
            desconectar();
        }
    }

    protected void rgInstrucciones_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            DocumentosRegimenBO dr = new DocumentosRegimenBO(logApp);
            DocumentosInstruccionesBO di = new DocumentosInstruccionesBO(logApp);
            DatosGestionDocumentosBO dgd = new DatosGestionDocumentosBO(logApp);
            //PermisosInstruccionesBO pi = new PermisosInstruccionesBO(logApp);
            EventosBO ev = new EventosBO(logApp);
            EspeciesFiscalesInstruccionesBO efi = new EspeciesFiscalesInstruccionesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            FlujoCargaBO FC = new FlujoCargaBO(logApp);
            string idU = Session["IdUsuario"].ToString();
            u.loadUsuarioLogin(User.Identity.Name);
           
            GridEditableItem editedItem = e.Item as GridEditableItem;
            if (e.CommandName == "Sellar")
            {
                edIdInstruccion.Value = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdInstruccion"]);
                edCodRegimen.Value = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["CodRegimen"]);
                bool permitirGA = false;
                bool permiso = true;
                if (u.CODPAIS == "N" && User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT"))
                {
                    FC.restricionNicaragua(edCodRegimen.Value, edIdInstruccion.Value);
                    if (FC.totalRegistros == 1)
                    {
                        permiso = false;
                    }
                }
                if (edCodRegimen.Value.Trim() == "9999" ||  permiso )
                    permitirGA = true;
                ev.loadEventosXInstruccionEventoArriboFin(edIdInstruccion.Value.Trim(), "1");
                if (ev.totalRegistros > 0 || permitirGA)
                {
                    edCodPaisHojaRuta.Value = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["CodPaisHojaRuta"]);
                    edRegimen.Value = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Regimen"]);
                    edIdEstadoFlujo.Value = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdEstadoFlujo"]);
                    edEstadoFlujo.Value = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["EstadoFlujo"]);
                    RadComboBox evento = (RadComboBox)editedItem.FindControl("edEventos");
                    RadDatePicker fecha = (RadDatePicker)editedItem.FindControl("dtFechaSellado");
                    RadTimePicker hora = (RadTimePicker)editedItem.FindControl("dtHoraSellado");
                    RadTextBox observacion = (RadTextBox)editedItem.FindControl("edObservacion");
                    RadTextBox correlativo = (RadTextBox)editedItem.FindControl("edCorrelativo");
                    RadNumericTextBox impuesto = (RadNumericTextBox)editedItem.FindControl("edImpuesto");
                    RadComboBox color = (RadComboBox)editedItem.FindControl("edColor");
                    RadComboBox tramitador = (RadComboBox)editedItem.FindControl("edTramitador");
                    RadComboBox aforador = (RadComboBox)editedItem.FindControl("edAforador");
                    RadNumericTextBox ISV = (RadNumericTextBox)editedItem.FindControl("edISV");
                    RadNumericTextBox DAI = (RadNumericTextBox)editedItem.FindControl("edDAI");
                    RadNumericTextBox Selectivo = (RadNumericTextBox)editedItem.FindControl("edSelectivo");
                    RadNumericTextBox STD = (RadNumericTextBox)editedItem.FindControl("edSTD");
                    RadNumericTextBox ProducciónConsumo = (RadNumericTextBox)editedItem.FindControl("edProduccionConsumo");
                    RadNumericTextBox Otros = (RadNumericTextBox)editedItem.FindControl("edOtros");
                    

                    if (!String.IsNullOrEmpty(correlativo.Text.Trim()) || correlativo.Visible == false)
                    {
                        //if (!String.IsNullOrEmpty(impuesto.Text) || impuesto.Visible == false)
                        //{
                            if (!color.IsEmpty || color.Visible == false)
                            {
                                if (!tramitador.IsEmpty || tramitador.Visible == false)
                                {
                                    if (!aforador.IsEmpty || aforador.Visible == false)
                                    {
                                        if (fecha.SelectedDate.HasValue)
                                        {
                                            edFecha.Value = fecha.SelectedDate.Value.ToShortDateString();
                                            if (hora.SelectedDate.HasValue)
                                            {
                                                //validar que todos los documentos mínimos por regimen estén registrados
                                                i.loadInstruccion(edIdInstruccion.Value);
                                                dr.loadDocumentosXRegimen(i.CODREGIMEN);
                                                int contador = 0;
                                                if (dr.totalRegistros > 0)
                                                {
                                                    for (int j = 1; j <= dr.totalRegistros; j++)
                                                    {
                                                        di.loadDocumentosXInstrucciones(edIdInstruccion.Value, dr.CODDOCUMENTO);
                                                        if (di.totalRegistros > 0)
                                                            contador += 1;
                                                        dr.regSiguiente();
                                                    }
                                                }
                                                else
                                                    contador = 1;

                                                //validar que se halla ingresado el numero de manifiesto antes de comenzar el flujo
                                                bool existeManifiesto = false;
                                                if (edRegimen.Value.Trim().Contains("xportac"))
                                                    existeManifiesto = true;
                                                else
                                                {
                                                    dgd.loadDatosGestionDocumentos(edIdInstruccion.Value);
                                                    if (dgd.totalRegistros > 0)
                                                    {
                                                        if (!String.IsNullOrEmpty(dgd.NOMANIFIESTO.Trim()))
                                                            existeManifiesto = true;
                                                    }
                                                }
                                                //bool especiesFiscalesRegistradas = false;
                                                ////validar que se halla registrado las especies fiscales antes de comenzar el flujo
                                                //efi.loadEspeciesFiscalesInstruccionesXInstruccion(edIdInstruccion.Value);
                                                //if (efi.totalRegistros > 0)
                                                //{
                                                //    if (!String.IsNullOrEmpty(efi.NECESARIO.Trim()))
                                                //        especiesFiscalesRegistradas = true;
                                                //}

                                                if (contador == dr.totalRegistros || permitirGA)
                                                {
                                                    if (existeManifiesto || permitirGA)
                                                    {
                                                        //if (especiesFiscalesRegistradas)
                                                        //{
                                                        edHora.Value = hora.SelectedDate.Value.ToShortTimeString();
                                                        edObservacion.Value = observacion.Text.Trim();
                                                        edCorrelativo.Value = correlativo.Text.Trim();
                                                        //edImpuesto.Value = impuesto.Text;
                                                        if (STD.Text == "")
                                                        {
                                                            edSTD.Value = "0";
                                                        }
                                                        else
                                                        {
                                                            edSTD.Value = STD.Text;
                                                        }

                                                        if (Selectivo.Text == "")
                                                        {
                                                            edSelectivo.Value = "0";
                                                        }
                                                        else
                                                        {
                                                            edSelectivo.Value = Selectivo.Text;
                                                        }

                                                        if (ISV.Text == "")
                                                        {
                                                            edISV.Value = "0";
                                                        }
                                                        else
                                                        {
                                                            edISV.Value = ISV.Text;
                                                        }

                                                        if (DAI.Text == "")
                                                        {
                                                            edDAI.Value = "0";
                                                        }
                                                        else
                                                        {
                                                            edDAI.Value = DAI.Text;
                                                        }
                                                        if (ProducciónConsumo.Text == "")
                                                        {
                                                            edProduccionConsumo.Value = "0";
                                                        }
                                                        else
                                                        {
                                                            edProduccionConsumo.Value = ProducciónConsumo.Text;
                                                        }
                                                          if (Otros.Text == "")
                                                        {
                                                            edOtros.Value = "0";
                                                        }
                                                        else
                                                        {
                                                            edOtros.Value = Otros.Text;
                                                        }
                                                        if (!color.IsEmpty)
                                                            edColor.Value = color.SelectedItem.Text;
                                                        edTramitador.Value = tramitador.SelectedValue;
                                                        edAforador.Value = aforador.SelectedValue;
                                                        registrarEstadosFlujoCarga();
                                                        fecha.Clear();
                                                        hora.Clear();
                                                        observacion.Text = "";
                                                        //}
                                                        //else
                                                        //    registrarMensaje("Todavía no puede registrar Entrega de Servicio, falta registrar las especies fiscales para esta instrucción"); return;
                                                    }
                                                    else
                                                        registrarMensaje("Todavía no puede registrar " + edEstadoFlujo.Value.Trim() + ", falta registrar el número de manifiesto para esta instrucción"); return;
                                                }
                                                else
                                                    registrarMensaje("Todavía no puede registrar " + edEstadoFlujo.Value.Trim() + ", falta registrar algunos documentos mínimos para esta instrucción"); return;
                                            }
                                            else
                                                registrarMensaje("Hora no puede estar vacía");
                                        }
                                        else
                                            registrarMensaje("Fecha no puede estar vacía");
                                    }
                                    else
                                    {
                                        if (edEstadoFlujo.Value.Contains("Validación Electrónica"))
                                            registrarMensaje("El aforador no puede estar vacío");
                                    }
                                }
                                else
                                {
                                    if (edEstadoFlujo.Value.Contains("Emisión de Pase de Salida"))
                                        registrarMensaje("El tramitador no puede estar vacío");
                                }
                            }
                            else
                            {
                                if (edEstadoFlujo.Value.Contains("Asignación Canal de Selectividad (Colores)"))
                                    registrarMensaje("El color no puede estar vacío");
                            }
                        //}
                        //else
                        //{
                        //    if (edEstadoFlujo.Value.Contains("Pago de Impuestos"))
                        //        registrarMensaje("El valor del impuesto no puede estar vacío");
                        //}
                    }
                    else
                    {
                        if (edEstadoFlujo.Value.Contains("Validación Electrónica"))
                            registrarMensaje("El No. Correlativo no puede estar vacío");
                    }
                }//////aqui
                else
                    registrarMensaje("Primero finalizar evento arribo de carga");
            }
            else if (rgInstrucciones.Items.Count <= 0 & e.CommandName == RadGrid.ExportToExcelCommandName)
                e.Canceled = true;
            else if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                ConfigureExport();
            }
        }
        catch (Exception)
        { }
    }

    private void ConfigureExport()
    {
        rgInstrucciones.Columns[4].Visible = false;
        rgInstrucciones.Columns[5].Visible = false;
        rgInstrucciones.Columns[6].Visible = false;
        rgInstrucciones.Columns[7].Visible = false;
        rgInstrucciones.Columns[8].Visible = false;
        rgInstrucciones.Columns[9].Visible = false;
        rgInstrucciones.Columns[10].Visible = false;
        rgInstrucciones.Columns[11].Visible = false;
        rgInstrucciones.Columns[12].Visible = false;
        String filename = "Instrucciones" + "_" + DateTime.Now.ToShortDateString();
        rgInstrucciones.GridLines = GridLines.Both;
        rgInstrucciones.ExportSettings.FileName = filename;
        rgInstrucciones.ExportSettings.IgnorePaging = true;
        rgInstrucciones.ExportSettings.OpenInNewWindow = false;
        rgInstrucciones.ExportSettings.ExportOnlyData = true;
    }

    private void registrarEstadosFlujoCarga()
    {
        try
        {
            conectar();
            GestionCargaBO gc = new GestionCargaBO(logApp);
            InstruccionesBO ins = new InstruccionesBO(logApp);
            FlujoCargaBO fc = new FlujoCargaBO(logApp);
            ClientesBO c = new ClientesBO(logApp);
            EventosBO e = new EventosBO(logApp);
            InstruccionesBO i2 = new InstruccionesBO(logApp);
            string fechaEstadoAnterior = "";
            bool registrar = false, noHayEventos = false;
            gc.getMaximaFechaXInstruccion(edIdInstruccion.Value);
            if (gc.totalRegistros > 0 & !String.IsNullOrEmpty(gc.FECHA))
            {
                fechaEstadoAnterior = gc.FECHA;
            }
            else
            {
                ins.loadInstruccion(edIdInstruccion.Value);
                fechaEstadoAnterior = ins.FECHARECEPCION;
            }

            if (Convert.ToDateTime(fechaEstadoAnterior) < Convert.ToDateTime(edFecha.Value + " " + edHora.Value))
                registrar = true;

            fc.loadEstadoFlujoCargaXPaisRegimen(edCodPaisHojaRuta.Value.Trim(), edCodRegimen.Value.Trim());
            if (fc.totalRegistros > 0)
            {
                string[] arreglo = new string[fc.totalRegistros];
                for (int i = 0; i < fc.totalRegistros; i++)
                {
                    arreglo[i] = fc.IDESTADOFLUJO;
                    fc.regSiguiente();
                }

                int pos = -1;
                string nuevoIdEstadoFlujo = "";
                string proxNuevoIdEstadoFlujo = "";
                for (int j = 0; j < arreglo.Length; j++)
                {
                    if (edIdEstadoFlujo.Value.Trim() == arreglo[j])
                    {
                        pos = j;
                        if (j < arreglo.Length - 1)
                        {
                            nuevoIdEstadoFlujo = arreglo[j + 1];
                            if (j + 1 < arreglo.Length - 1) //si el siguiente estado no es el ultimo estado
                                proxNuevoIdEstadoFlujo = arreglo[j + 2];
                            else
                                noHayEventos = true;
                        }
                        else
                            //Terrestre();
                            nuevoIdEstadoFlujo = "99"; //instruccion finalizada
                        break;
                    }
                }

                if (nuevoIdEstadoFlujo == "99")
                {
                    e.loadEventosXInstruccion(edIdInstruccion.Value.Trim());
                    if (e.totalRegistros <= 0)
                        noHayEventos = true;
                    else
                        noHayEventos = false;
                }
                else
                    noHayEventos = true;

                if (registrar == true)
                {
                    if (noHayEventos == true)
                    {
                        i2.loadInstruccion(edIdInstruccion.Value.Trim());
                        if (nuevoIdEstadoFlujo == "99" && i2.ARRIBOCARGA == "1")
                        {
                            gc.loadGestionCargaXInstruccionEvento(edIdInstruccion.Value, edIdEstadoFlujo.Value);
                            if (gc.totalRegistros <= 0)
                            {
                                gc.newLine();
                                gc.IDINSTRUCCION = edIdInstruccion.Value;
                                gc.IDESTADOFLUJO = edIdEstadoFlujo.Value;
                                gc.FECHA = edFecha.Value + " " + edHora.Value;
                                gc.FECHAINGRESO = DateTime.Now.ToString();
                                gc.OBSERVACION = edObservacion.Value;
                                gc.ELIMINADO = "0";
                                gc.IDUSUARIO = Session["IdUsuario"].ToString();
                                gc.commitLine();
                                gc.actualizar();
                                i2.FECHAFINALFLUJO = edFecha.Value + " " + edHora.Value;
                                i2.actualizar();
                            }

                            bool esTramitador = false;
                            ins.loadInstruccion(edIdInstruccion.Value.Trim());
                            if (ins.totalRegistros > 0) //& (!String.IsNullOrEmpty(nuevoIdEstadoFlujo) || !String.IsNullOrEmpty(nuevoIdEstadoFlujo))
                            {
                                string complemento = "";
                                if (edEstadoFlujo.Value.Contains("Validación Electrónica"))
                                {
                                    ins.NOCORRELATIVO = edCorrelativo.Value;
                                    ins.AFORADOR = edAforador.Value;
                                    complemento = "No. Correlativo: " + ins.NOCORRELATIVO + ", Aforador: " + ins.AFORADOR;
                                }
                                else if (edEstadoFlujo.Value.Contains("Pago de Impuestos"))
                                {
                                    try
                                    {
                                        ins.ISV = double.Parse(edISV.Value);
                                        ins.DAI = double.Parse(edDAI.Value);
                                        ins.SELECTIVO = double.Parse(edSelectivo.Value);
                                        ins.STD = double.Parse(edSTD.Value);
                                        ins.PRODUCCIONCONSUMO = double.Parse(edProduccionConsumo.Value);
                                        ins.OTROSIMPUESTOS = double.Parse(edOtros.Value);
                                        ins.IMPUESTO = (double.Parse(edDAI.Value) + double.Parse(edSelectivo.Value) + double.Parse(edSTD.Value) + double.Parse(edISV.Value) + double.Parse(edProduccionConsumo.Value) + double.Parse(edOtros.Value));
                                    }
                                    catch { }
                                }
                                else if (edEstadoFlujo.Value.Contains("Asignación Canal de Selectividad"))
                                {
                                    ins.COLOR = edColor.Value;
                                }
                                else if (edEstadoFlujo.Value.Contains("Emisión de Pase de Salida"))
                                {
                                    ins.TRAMITADOR = edTramitador.Value;
                                    esTramitador = true;
                                }

                                if (edEstadoFlujo.Value.Contains("Asignación Canal de Selectividad"))
                                {
                                    if (edColor.Value.Trim() == "Verde")
                                        ins.IDESTADOFLUJO = proxNuevoIdEstadoFlujo;
                                    else
                                        ins.IDESTADOFLUJO = nuevoIdEstadoFlujo;
                                }
                                else
                                    ins.IDESTADOFLUJO = nuevoIdEstadoFlujo;
                                ins.actualizar();

                            }

                            if (esTramitador)
                            {
                                try
                                {
                                    conectarAduanas();
                                    EncabezadoEsquemaBO ee = new EncabezadoEsquemaBO(logAppAduanas);
                                    ee.loadEncabezadoEsquema(edIdInstruccion.Value.Trim());
                                    if (ee.totalRegistros > 0)
                                    {
                                        ee.Tramitador = edTramitador.Value;
                                        ee.actualizar();
                                    }
                                }
                                catch { }
                            }

                            registrarMensaje(edEstadoFlujo.Value + " registrado exitosamente");
                            llenarBitacora("Se selló el estado del flujo de carga " + edEstadoFlujo.Value, Session["IdUsuario"].ToString(), edIdInstruccion.Value);

                            conectar();
                            c.loadCliente(ins.IDCLIENTE);
                            if (c.totalRegistros > 0)
                            {
                                InstruccionAduanaBO bo = new InstruccionAduanaBO(logApp);
                                bo.correos(ins.IDINSTRUCCION);
                                if (edEstadoFlujo.Value.Contains("Canal de Selectividad"))
                                {
                                    //enviar correo con notificación del canal de selectividad
                                    sendMail(c.EMAIL, "Canal de Selectividad", "Cliente: " + bo.NOMBRE + "| Proveedor: " + bo.PROVEEDOR + "| Producto: " + bo.PRODUCTO + "| Factura: " + bo.REFERENCIACLIENTE + ", Se asignó el canal de selectividad a la instrucción No. " + ins.IDINSTRUCCION + ", con color " + edColor.Value.Trim());
                                }
                                else if (edEstadoFlujo.Value.Contains("Entrega de Servicio"))
                                {
                                    //enviar correo con notificación de la entrega de servicio
                                    sendMail(c.EMAIL, "Entrega de Servicio", "Cliente: " + bo.NOMBRE + "| Proveedor: " + bo.PROVEEDOR + "| Producto: " + bo.PRODUCTO + "| Factura: " + bo.REFERENCIACLIENTE + ", Se registró la entrega de servicio de la instrucción No. " + ins.IDINSTRUCCION);
                                    Terrestre();
                                }
                            }
                            rgInstrucciones.Rebind();
                        }
                        else
                            if (nuevoIdEstadoFlujo != "99")
                            {  gc.loadGestionCargaXInstruccionEvento(edIdInstruccion.Value, edIdEstadoFlujo.Value);
                            if (gc.totalRegistros <= 0)
                            {
                                gc.newLine();
                                gc.IDINSTRUCCION = edIdInstruccion.Value;
                                gc.IDESTADOFLUJO = edIdEstadoFlujo.Value;
                                gc.FECHA = edFecha.Value + " " + edHora.Value;
                                gc.FECHAINGRESO = DateTime.Now.ToString();
                                gc.OBSERVACION = edObservacion.Value;
                                gc.ELIMINADO = "0";
                                gc.IDUSUARIO = Session["IdUsuario"].ToString();
                                gc.commitLine();
                                gc.actualizar();
                            }

                            bool esTramitador = false;
                            ins.loadInstruccion(edIdInstruccion.Value.Trim());
                            if (ins.totalRegistros > 0) //& (!String.IsNullOrEmpty(nuevoIdEstadoFlujo) || !String.IsNullOrEmpty(nuevoIdEstadoFlujo))
                            {
                                string complemento = "";
                                if (edEstadoFlujo.Value.Contains("Validación Electrónica"))
                                {
                                    ins.NOCORRELATIVO = edCorrelativo.Value;
                                    ins.AFORADOR = edAforador.Value;
                                    complemento = "No. Correlativo: " + ins.NOCORRELATIVO + ", Aforador: " + ins.AFORADOR;
                                }
                                else if (edEstadoFlujo.Value.Contains("Pago de Impuestos"))
                                {
                                    try
                                    {
                                        ins.ISV = double.Parse(edISV.Value);
                                        ins.DAI = double.Parse(edDAI.Value);
                                        ins.SELECTIVO = double.Parse(edSelectivo.Value);
                                        ins.STD = double.Parse(edSTD.Value);
                                        ins.PRODUCCIONCONSUMO = double.Parse(edProduccionConsumo.Value);
                                        ins.OTROSIMPUESTOS = double.Parse(edOtros.Value);
                                        ins.IMPUESTO = (double.Parse(edDAI.Value) + double.Parse(edSelectivo.Value) + double.Parse(edSTD.Value) + double.Parse(edISV.Value) + double.Parse(edProduccionConsumo.Value) + double.Parse(edOtros.Value));
                                    }
                                    catch { }
                                }
                                else if (edEstadoFlujo.Value.Contains("Asignación Canal de Selectividad"))
                                {
                                    ins.COLOR = edColor.Value;
                                }
                                else if (edEstadoFlujo.Value.Contains("Emisión de Pase de Salida"))
                                {
                                    ins.TRAMITADOR = edTramitador.Value;
                                    esTramitador = true;
                                }

                                if (edEstadoFlujo.Value.Contains("Asignación Canal de Selectividad"))
                                {
                                    if (edColor.Value.Trim() == "Verde")
                                        ins.IDESTADOFLUJO = proxNuevoIdEstadoFlujo;
                                    else
                                        ins.IDESTADOFLUJO = nuevoIdEstadoFlujo;
                                }
                                else
                                
                                ins.IDESTADOFLUJO = nuevoIdEstadoFlujo;
                                ins.actualizar();
                            }

                            if (esTramitador)
                            {
                                try
                                {
                                    conectarAduanas();
                                    EncabezadoEsquemaBO ee = new EncabezadoEsquemaBO(logAppAduanas);
                                    ee.loadEncabezadoEsquema(edIdInstruccion.Value.Trim());
                                    if (ee.totalRegistros > 0)
                                    {
                                        ee.Tramitador = edTramitador.Value;
                                        ee.actualizar();
                                    }
                                }
                                catch { }
                            }

                            registrarMensaje(edEstadoFlujo.Value + " registrado exitosamente");
                            llenarBitacora("Se selló el estado del flujo de carga " + edEstadoFlujo.Value, Session["IdUsuario"].ToString(), edIdInstruccion.Value);

                            conectar();
                            c.loadCliente(ins.IDCLIENTE);
                            if (c.totalRegistros > 0)
                            {
                                InstruccionAduanaBO bo = new InstruccionAduanaBO(logApp);
                                bo.correos(ins.IDINSTRUCCION);
                                if (edEstadoFlujo.Value.Contains("Canal de Selectividad"))
                                {
                                    //enviar correo con notificación del canal de selectividad
                                    sendMail(c.EMAIL, "Canal de Selectividad", "Cliente: " + bo.NOMBRE + "| Proveedor: " + bo.PROVEEDOR + "| Producto: " + bo.PRODUCTO + "| Factura: " + bo.REFERENCIACLIENTE + ", Se asignó el canal de selectividad a la instrucción No. " + ins.IDINSTRUCCION + ", con color " + edColor.Value.Trim());
                                }
                                else if (edEstadoFlujo.Value.Contains("Entrega de Servicio"))
                                {
                                    //enviar correo con notificación de la entrega de servicio
                                    sendMail(c.EMAIL, "Entrega de Servicio", "Cliente: " + bo.NOMBRE + "| Proveedor: " + bo.PROVEEDOR + "| Producto: " + bo.PRODUCTO + "| Factura: " + bo.REFERENCIACLIENTE + ", Se registró la entrega de servicio de la instrucción No. " + ins.IDINSTRUCCION);
                                    Terrestre();
                                }
                            }
                            rgInstrucciones.Rebind();
                        }
                        else

                            registrarMensaje("No se puede finalizar la hoja de ruta si no se ha sellado el arribo de la carga");
                    }
                    else
                        registrarMensaje("No se puede registrar este estado, hay al menos algún evento sin finalizar");
                }
                else
                    registrarMensaje("La fecha debe ser mayor a la fecha del estado anterior");
            }
            else
                registrarMensaje("La fecha actual tiene que ser mayor que la fecha del estado anterior, la cual es " + fechaEstadoAnterior);
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "GestionCarga");
            if (ex.Message.Contains(""))
                rgInstrucciones.Rebind();
        }
        finally
        {
            desconectar();
        }
    }

    protected void btnLimpiar_Click(object sender, ImageClickEventArgs e)
    {
        limpiar();
    }

    private void limpiar()
    {
        try
        {
            foreach (GridDataItem grdItem in rgInstrucciones.Items)
            {
                RadDatePicker dtFecha = (RadDatePicker)grdItem.FindControl("dtFechaSellado");
                RadTimePicker dtHora = (RadTimePicker)grdItem.FindControl("dtHoraSellado");
                RadTextBox edObservacion = (RadTextBox)grdItem.FindControl("edObservacion");
                RadTextBox edDescripcion1 = (RadTextBox)grdItem.FindControl("edDescripcion1");
                RadComboBox edDescripcion2 = (RadComboBox)grdItem.FindControl("edDescripcion2");
                dtFecha.Clear();
                dtHora.Clear();
                edObservacion.Text = "";
                edDescripcion1.Text = "";
                edDescripcion2.SelectedValue = "0";
            }
        }
        catch (Exception)
        { }
    }

    protected void rgInstrucciones_ItemDataBound(object sender, GridItemEventArgs e)
    {
        try
        {
            foreach (GridDataItem gridItem in rgInstrucciones.Items)
            {
                string estadoFlujo = gridItem.Cells[10].Text;
                string codigoAduana = gridItem.Cells[11].Text;
                //RadTextBox descripcion1 = (RadTextBox)gridItem.FindControl("edDescripcion1");
                //RadNumericTextBox impuesto = (RadNumericTextBox)gridItem.FindControl("edImpuesto");
                //RadComboBox descripcion2 = (RadComboBox)gridItem.FindControl("edDescripcion2");
                RadTextBox correlativo = (RadTextBox)gridItem.FindControl("edCorrelativo");
                RadNumericTextBox impuesto = (RadNumericTextBox)gridItem.FindControl("edImpuesto");
                RadNumericTextBox ISV = (RadNumericTextBox)gridItem.FindControl("edISV");
                RadNumericTextBox DAI = (RadNumericTextBox)gridItem.FindControl("edDAI");
                RadNumericTextBox Selectivo = (RadNumericTextBox)gridItem.FindControl("edSelectivo");
                RadNumericTextBox STD = (RadNumericTextBox)gridItem.FindControl("edSTD");
                RadNumericTextBox ProducciónConsumo = (RadNumericTextBox)gridItem.FindControl("edProduccionConsumo");
                RadNumericTextBox Otros = (RadNumericTextBox)gridItem.FindControl("edOtros");
                RadComboBox color = (RadComboBox)gridItem.FindControl("edColor");
                RadComboBox tramitador = (RadComboBox)gridItem.FindControl("edTramitador");
                RadComboBox aforador = (RadComboBox)gridItem.FindControl("edAforador");
                if (estadoFlujo.Contains("Validación Electrónica"))
                {
                    correlativo.Visible = true;
                    aforador.Visible = true;
                    ISV.Visible = false;
                    DAI.Visible = false;
                    Selectivo.Visible = false;
                    STD.Visible = false;
                    ProducciónConsumo.Visible = false;
                    Otros.Visible = false;
                    color.Visible = false;
                    tramitador.Visible = false;
                    conectar();
                    UsuariosBO u = new UsuariosBO(logApp);
                    u.loadUsuariosNombreCompletoXAduana(codigoAduana, "Aforadores");
                    aforador.DataTextField = u.TABLA.Columns["NombreCompleto"].ToString();
                    aforador.DataValueField = u.TABLA.Columns["IdUsuario"].ToString();
                    aforador.DataSource = u.TABLA;
                    aforador.DataBind();
                }
                else if (estadoFlujo.Contains("Pago de Impuestos"))
                {
                    ISV.Visible = true;
                    DAI.Visible = true;
                    Selectivo.Visible = true;
                    STD.Visible = true;
                    ProducciónConsumo.Visible = true;
                    Otros.Visible = true;
                    correlativo.Visible = false;
                    aforador.Visible = false;
                    color.Visible = false;
                    tramitador.Visible = false;
                }
                else if (estadoFlujo.Contains("Asignación Canal de Selectividad (Colores)"))
                {
                    ISV.Visible = false;
                    DAI.Visible = false;
                    Selectivo.Visible = false;
                    STD.Visible = false;
                    ProducciónConsumo.Visible = false;
                    Otros.Visible = false;
                    correlativo.Visible = false;
                    aforador.Visible = false;
                    color.Visible = true;
                    tramitador.Visible = false;
                    conectar();
                    CodigosBO c = new CodigosBO(logApp);
                    c.loadAllCampos("COLORES");
                    color.DataTextField = c.TABLA.Columns["Descripcion"].ToString();
                    color.DataValueField = c.TABLA.Columns["Codigo"].ToString();
                    color.DataSource = c.TABLA;
                    color.DataBind();
                }
                else if (estadoFlujo.Contains("Emisión de Pase de Salida"))
                {
                    ISV.Visible = false;
                    DAI.Visible = false;
                    Selectivo.Visible = false;
                    STD.Visible = false;
                    ProducciónConsumo.Visible = false;
                    Otros.Visible = false;
                    correlativo.Visible = false;
                    aforador.Visible = false;
                    color.Visible = false;
                    tramitador.Visible = true;
                    conectar();
                    UsuariosBO u = new UsuariosBO(logApp);
                    u.loadUsuariosNombreCompletoXAduana(codigoAduana, "Tramitadores");
                    tramitador.DataTextField = u.TABLA.Columns["NombreCompleto"].ToString();
                    tramitador.DataValueField = u.TABLA.Columns["IdUsuario"].ToString();
                    tramitador.DataSource = u.TABLA;
                    tramitador.DataBind();
                }
                else
                {
                    ISV.Visible = false;
                    DAI.Visible = false;
                    Selectivo.Visible = false;
                    STD.Visible = false;
                    ProducciónConsumo.Visible = false;
                    Otros.Visible = false;
                    correlativo.Visible = false;
                    aforador.Visible = false;
                    color.Visible = false;
                    tramitador.Visible = false;
                }
            }
        }
        catch { }
    }

    protected void SetVisible(TableCellCollection cells, String commandName, string visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is RadTextBox)
                {
                    RadTextBox txt = (RadTextBox)control;
                    if (txt.ID == commandName)
                        txt.Visible = true;
                    else
                        txt.Visible = false;
                }
            }
        }
    }

    #region Conexion
    private void conectarAduanas()
    {
        if (logAppAduanas == null)
            logAppAduanas = new GrupoLis.Login.Login();
        if (!logAppAduanas.conectado)
        {
            logAppAduanas.tipoConexion = TipoConexion.SQL_SERVER;
            logAppAduanas.SERVIDOR = mParamethers.Get("Servidor");
            logAppAduanas.DATABASE = mParamethers.Get("DatabaseEsquemaTramites2");
            logAppAduanas.USER = mParamethers.Get("User");
            logAppAduanas.PASSWD = mParamethers.Get("Password");
            logAppAduanas.conectar();
        }
    }

    private void desconectarAduanas()
    {
        if (logAppAduanas.conectado)
            logAppAduanas.desconectar();
    }
    #endregion
    #region Conexion 2
    private void conectarTerrestre()
    {
        if (logAppTerrestre == null)
            logAppTerrestre = new GrupoLis.Login.Login();
        if (!logAppTerrestre.conectado)
        {
            logAppTerrestre.tipoConexion = TipoConexion.SQL_SERVER;
            logAppTerrestre.SERVIDOR = mParamethers.Get("ServidorAzure");
            logAppTerrestre.DATABASE = mParamethers.Get("DatabaseTT");
            logAppTerrestre.USER = mParamethers.Get("UserAzure");
            logAppTerrestre.PASSWD = mParamethers.Get("PasswordAzure");
            logAppTerrestre.conectar();
        }
    }

    private void desconectarTerrestre()
    {
        if (logAppTerrestre.conectado)
            logAppTerrestre.desconectar();
    }
    #endregion

    private void Terrestre()
  {
      try
      {
          conectar();
          InstruccionesBO ins = new InstruccionesBO(logApp);

          ins.LoadGuias(edIdInstruccion.Value);
          string guia = ins.CODIGOGUIA;
          ins.LoadGuiasHN(guia);
          string aduana = ins.CODIGOADUANA;
          if (ins.totalRegistros > 0)
          {
              conectarTerrestre();
              GuiasBO g = new GuiasBO(logApp);
              TiempoFlujosBO tf = new TiempoFlujosBO(logApp);
              g.CargarGuia(guia);
              if (g.CODESTADO == "7")
              {
                  tf.AgregarTiempo();
                  tf.newLine();
                  tf.CODIGOGUIA = guia;
                  tf.CODFLUJO = (int.Parse("7").ToString());
                  tf.CODIGOADUANA = aduana;
                  tf.FECHA = edFecha.Value + " " + edHora.Value;
                  tf.OBSERVACION = edObservacion.Value;
                  tf.CORRELATIVOGUIA = (int.Parse("7").ToString());
                  tf.ELIMINADO = "0";
                  tf.IDUSUARIO = (int.Parse("66").ToString());
                  tf.FECHASISTEMA = DateTime.Now.ToString();
                  tf.commitLine();
                  tf.actualizar();

                  g.CODESTADO = (int.Parse("8").ToString());
                  g.commitLine();
                  g.actualizar();

              }

          }
      }

      catch { }
      finally { desconectarTerrestre(); }
  }
    #region Prueba

    #endregion 
}