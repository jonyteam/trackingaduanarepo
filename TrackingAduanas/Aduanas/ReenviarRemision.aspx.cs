﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.SessionState;
using System.Collections.Specialized;
using GrupoLis.Login;
using System.Text.RegularExpressions;
using System.Security.Principal;
using Telerik.Web;
using Telerik.Web.UI;
using System.Security.Cryptography;
using Microsoft.Office.Interop.Excel;

public partial class EnviarRemision : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Reenvio Remision";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgInstrucciones.FilterMenu);
        rgInstrucciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgInstrucciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            if (!Modificar)
                redirectTo("Default.aspx");
            rgInstrucciones.ClientSettings.Scrolling.AllowScroll = true;
            rgInstrucciones.ClientSettings.Scrolling.UseStaticHeaders = true;
            rgInstrucciones.ShowHeader = true;
            rgInstrucciones.ShowFooter = true;
            ((SiteMaster)Master).SetTitulo("Reenvio Remisiones", "Reenvio Remisiones");
            loadAduanasOrigen();
            loadRemisiones();

          //  this.llenargrid("", this.RadRemisiones.Text);
           
           
        }
       

    }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void rgInstrucciones_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenargrid(this.txtInstruccion.Text, " ");
    }

    protected void llenargrid(string Instrucion ,String remision )
    {
        try
        {
            conectar();
            InstruccionesBO I = new InstruccionesBO(logApp);
            if (User.IsInRole("Administradores"))
            {
                I.CargarRemisionAdministradorRenvio(Instrucion,remision);
            }
            else
            {
                I.CargarRemision(Session["CodigoAduana"].ToString());
            }
            rgInstrucciones.DataSource = I.TABLA;
            rgInstrucciones.DataBind();
        }
        catch (Exception)
        {
        }
        finally { }
    }


    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        if (this.rgInstrucciones.MasterTableView.Items.Count <= 0)
        {
            registrarMensaje("No ha selecicionado remisiones, Favor dar click en buscar");
        }
        else
        {
            EscribirExcel();
        }
    }
    protected void btnGuardar_Click(object sender, ImageClickEventArgs e)
    {

    }

    protected void EscribirExcel()
    {
        string NumeroRemision = "", NombreUsuarioRemision = "";
        string Nombre_Aduana = "";
        if (rgInstrucciones.Items.Count >= 0)
        {
            btnSalvar.Visible = false;
            string nameFile = "RemisionT.xlsx";
            string nameDest = "Remision.xlsx";
            bool hayRegistro = false;
           
          
            Application.Lock();
            if (File.Exists("J:\\Files1\\Aduanas\\" + nameDest))
            {
                File.Delete("J:\\Files1\\Aduanas\\" + nameDest);
                File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
            }
            else
            {
                File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
            }
            var mTemplateFileName = @"J:\Files1\Aduanas\" + nameDest;

            var xlApp = new ApplicationClass() { DisplayAlerts = false, AskToUpdateLinks = false };
            var workbooks = xlApp.Workbooks;
            var workbook = workbooks.Open(mTemplateFileName, 2, false);

            try
            {
                conectar();
                #region Llenar Remision
                const string mInputWorkSheetName = "Datos";
                var inputWorkSheet = workbook.Worksheets.Cast<Worksheet>().FirstOrDefault(w => w.Name == mInputWorkSheetName);
                int pos = 13, con = 0;
       
                UsuariosBO U = new UsuariosBO(logApp);

                U.DatosRemisionReenvio(Session["IdUsuario"].ToString());
                foreach (GridDataItem gridItem in rgInstrucciones.Items)
                {
                  
                   
                        con = con + 1;
                        hayRegistro = true;
                        if (gridItem.Cells[3].Text == "&nbsp;")
                        {
                            gridItem.Cells[3].Text = "";
                        }
                        if (gridItem.Cells[4].Text == "&nbsp;")
                        {
                            gridItem.Cells[4].Text = "";
                        }
                        inputWorkSheet.Cells.Range["A" + pos].Value = con;// Item
                        inputWorkSheet.Cells.Range["B" + pos].Value = gridItem.Cells[3].Text;// Hoja de Ruta
                        if (gridItem.Cells[5].Text == "P")
                        {
                            inputWorkSheet.Cells.Range["C" + pos].Value = gridItem.Cells[4].Text;// Serie--Cuando sea Poliza
                            inputWorkSheet.Cells.Range["D" + pos].Value = gridItem.Cells[8].Text;// Correlativo
                        }
                        else if (gridItem.Cells[5].Text == "F")
                        {
                            inputWorkSheet.Cells.Range["E" + pos].Value = gridItem.Cells[4].Text;// Serie--Cuando sea Fauca
                            inputWorkSheet.Cells.Range["F" + pos].Value = gridItem.Cells[8].Text;// Correlativo
                        }
                        else
                        {
                            inputWorkSheet.Cells.Range["G" + pos].Value = gridItem.Cells[8].Text;// Correlativo
                        }
                        inputWorkSheet.Cells.Range["H" + pos].Value = gridItem.Cells[6].Text;// Cliente
                        inputWorkSheet.Cells.Range["L" + pos].Value = DateTime.Today;// Fecha
                        pos = pos + 1;
                      
                    
                }

                if (this.txtInstruccion.Text == "")
                {
                  Nombre_Aduana=this.cmbAduana.Text;
                }
                else
                {
                    string[] stringArray = this.txtInstruccion.Text.Split('-');
                    this.txtInstruccion.Text.IndexOf("-");
                    Nombre_Aduana = this.cmbAduana.FindItemByValue(stringArray[1]).Text;
                }

                inputWorkSheet.Cells.Range["D" + (pos + 3)].Value = U.TABLA.Rows[0]["Nombre"].ToString();// Nombre
                NombreUsuarioRemision = U.TABLA.Rows[0]["Nombre"].ToString();
                NumeroRemision = rgInstrucciones.Items[0]["Idremision"].Text;
                 

                inputWorkSheet.Cells.Range["D" + 9].Value = Nombre_Aduana;// Aduana
                inputWorkSheet.Cells.Range["I" + 6].Value = NumeroRemision; // Numero de Remision
                inputWorkSheet.Cells.Range["K" + 9].Value = DateTime.Today;
          
                #endregion
                workbook.Save();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                workbook.Close();
                xlApp.Quit();
                desconectar();
                Application.UnLock();
                desconectar();
            }
          
         //  EnviarCorreo(NumeroRemision, NombreUsuarioRemision);
     

            // rgInstrucciones.Rebind();

            llenargrid(" ", " ");
            registrarMensaje("La remision " + NumeroRemision + " sido creada favor revisar el documento descargado");
            this.txtInstruccion.Text = "";
            this.btnSalvar.Visible = true;

            if (hayRegistro)
            {
                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + nameDest);
                Response.Charset = "";
                Response.TransmitFile("J:\\Files1\\Aduanas\\" + nameDest);
                Response.Flush();
                Response.End();

            }

         

        }
  
       
    }

    protected void EnviarCorreo(string NumeroRemision, string NombreUsuarioRemision)
    {
        try
        {
            conectar();
            CorreosBO C = new CorreosBO(logApp);
            //C.LoadCorreosXPais(Session["Pais"].ToString(),this.cmbAduana.SelectedValue);
            if ( this.txtInstruccion.Text=="")
            {
                C.LoadCorreosXPais("H", this.cmbAduana.SelectedValue);
            }
            else
            {
                string[] stringArray = this.txtInstruccion.Text.Split('-');
                this.txtInstruccion.Text.IndexOf("-");
                C.LoadCorreosXPais("H", stringArray[1]);
            }
          
            string Asunto = "Reenvio de Remision Numero " + NumeroRemision;
            string Cuerpo = NombreUsuarioRemision + "; adjunto la Remision numero " + NumeroRemision + "Favor tomar nota; Este mensaje es generado automaticamente, favor no responderlo";
            string Copias = C.COPIAS;
            string NombreUsuario = "Tracking Aduanas";
            string NombredelArchivo = "Remision";
            string usuario = "trackingaduanas";
            string pass = "nMgtdA$7PaRjQphEYZBD";
            string para = C.CORREOS;
            sendMailConParametros(para, Asunto, Cuerpo, Copias, NombreUsuario, NombredelArchivo, usuario, pass, ".xlsx");
        }
        catch (Exception)
        {

        }
        finally { desconectar(); }

    }
 
  

    #region Busqueda de Remisiones 
        private void loadAduanasOrigen()
         {
             try
             {

                 conectar();
                 RemisionesBO R = new RemisionesBO(logApp);
                 R.LoadAduanas();
                 cmbAduana.DataSource = R.TABLA;
                 cmbAduana.DataTextField = R.TABLA.Columns["NombreAduana"].ToString();
                 cmbAduana.DataValueField = R.TABLA.Columns["cod"].ToString();
                 cmbAduana.DataBind();
                 desconectar();




             }
             catch { }
         }
        private void loadRemisiones()
        {
            try
            {
                conectar();
                RemisionesBO R = new RemisionesBO(logApp);
                R.LoadRemisionXAduanas(RadTop.Text, cmbAduana.SelectedValue);
                RadRemisiones.DataSource = R.TABLA;
                RadRemisiones.DataTextField = R.TABLA.Columns["NumeroRemision"].ToString();
                RadRemisiones.DataValueField = R.TABLA.Columns["Orden"].ToString();
                RadRemisiones.DataBind();
                RadRemisiones.SelectedIndex = 0;
                desconectar();
       
      
            }
            catch { }
        }
        protected void cmbAduana_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            loadRemisiones();
            this.txtInstruccion.Text = "";
            llenargrid(" ", " ");
      
        
        }
        protected void RadRemisiones_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
        {
            llenargrid(" " , " ");
        }
        protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
        {

        }
        protected void RadTop_TextChanged(object sender, EventArgs e)
        {
            loadRemisiones();
            this.txtInstruccion.Text = "";

        }
        protected void Unnamed1_Click1(object sender, ImageClickEventArgs e)
        {


            llenargrid(" ", this.RadRemisiones.Text);


        }
        protected void Unnamed1_Click(object sender, ImageClickEventArgs e)
        {


            llenargrid(this.txtInstruccion.Text, " ");


        }

    #endregion 
        protected void RadTabStrip1_TabClick(object sender, RadTabStripEventArgs e)
        {

            llenargrid(" "," ");
        }

        private void ConfigureExport(string remision )
        {
           String filename = "Remision " + remision + "_" + DateTime.Now.ToShortDateString();
           rgInstrucciones.GridLines = GridLines.Both;
           rgInstrucciones.ExportSettings.FileName = filename;
           rgInstrucciones.ExportSettings.IgnorePaging = true;
           rgInstrucciones.ExportSettings.OpenInNewWindow = false;
           rgInstrucciones.ExportSettings.ExportOnlyData = true;
        }

 

}