﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.SessionState;
using System.Collections.Specialized;
using GrupoLis.Login;
using System.Text.RegularExpressions;
using System.Security.Principal;
using Telerik.Web;
using Telerik.Web.UI;
using System.Security.Cryptography;
using Microsoft.Office.Interop.Excel;

public partial class EnviarRemision : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Enviar Remision";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgInstrucciones.FilterMenu);
        rgInstrucciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgInstrucciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            if (!Modificar)
                redirectTo("Default.aspx");
            rgInstrucciones.ClientSettings.Scrolling.AllowScroll = true;
            rgInstrucciones.ClientSettings.Scrolling.UseStaticHeaders = true;
            rgInstrucciones.ShowHeader = true;
            rgInstrucciones.ShowFooter = true;
            ((SiteMaster)Master).SetTitulo("Enviar Remision", "Enviar Remision");
        }
    }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void rgInstrucciones_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenargrid();
    }

    protected void llenargrid()
    {
        try
        {
            conectar();
            InstruccionesBO I = new InstruccionesBO(logApp);
            if (User.IsInRole("Administradores"))
            {
                I.CargarRemisionAdministrador();
            }
            else
            {
                I.CargarRemision(Session["CodigoAduana"].ToString());
            }
            rgInstrucciones.DataSource = I.TABLA;
            rgInstrucciones.DataBind();
        }
        catch (Exception)
        {
        }
        finally { }
    }
    protected void btnGuardar_Click(object sender, ImageClickEventArgs e)
    {
        EscribirExcel();
    }
    protected void EscribirExcel()
    {
        if (rgInstrucciones.Items.Count >= 0)
        {
            btnGuardar.Visible = false;
            string nameFile = "RemisionT.xlsx";
            string nameDest = "Remision.xlsx";
            bool hayRegistro = false;
            string NumeroRemision = "", NombreUsuarioRemision = "";
            Application.Lock();
            if (File.Exists("J:\\Files1\\Aduanas\\" + nameDest))
            {
                File.Delete("J:\\Files1\\Aduanas\\" + nameDest);
                File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
            }
            else
            {
                File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
            }
            var mTemplateFileName = @"J:\Files1\Aduanas\" + nameDest;

            var xlApp = new ApplicationClass() { DisplayAlerts = false, AskToUpdateLinks = false };
            var workbooks = xlApp.Workbooks;
            var workbook = workbooks.Open(mTemplateFileName, 2, false);

            try
            {
                conectar();
                #region Llenar Remision
                const string mInputWorkSheetName = "Datos";
                var inputWorkSheet = workbook.Worksheets.Cast<Worksheet>().FirstOrDefault(w => w.Name == mInputWorkSheetName);
                int pos = 13, con = 0;
                RemisionesBO R = new RemisionesBO(logApp);
                UsuariosBO U = new UsuariosBO(logApp);
                NumeracionesBO N = new NumeracionesBO(logApp);
                U.DatosRemision(Session["IdUsuario"].ToString());
                foreach (GridDataItem gridItem in rgInstrucciones.Items)
                {
                    System.Web.UI.WebControls.CheckBox chkAceptar = (System.Web.UI.WebControls.CheckBox)gridItem.FindControl("chbAceptar");
                    if (chkAceptar.Checked == true)
                    {
                        con = con + 1;
                        hayRegistro = true;
                        if (gridItem.Cells[3].Text == "&nbsp;")
                        {
                            gridItem.Cells[3].Text = "";
                        }
                        if (gridItem.Cells[4].Text == "&nbsp;")
                        {
                            gridItem.Cells[4].Text = "";
                        }
                        inputWorkSheet.Cells.Range["A" + pos].Value = con;// Item
                        inputWorkSheet.Cells.Range["B" + pos].Value = gridItem.Cells[3].Text;// Hoja de Ruta
                        if (gridItem.Cells[5].Text == "P")
                        {
                            inputWorkSheet.Cells.Range["C" + pos].Value = gridItem.Cells[4].Text;// Serie--Cuando sea Poliza
                            inputWorkSheet.Cells.Range["D" + pos].Value = gridItem.Cells[8].Text;// Correlativo
                        }
                        else if (gridItem.Cells[5].Text == "F")
                        {
                            inputWorkSheet.Cells.Range["E" + pos].Value = gridItem.Cells[4].Text;// Serie--Cuando sea Fauca
                            inputWorkSheet.Cells.Range["F" + pos].Value = gridItem.Cells[8].Text;// Correlativo
                        }
                        else
                        {
                            inputWorkSheet.Cells.Range["G" + pos].Value = gridItem.Cells[8].Text;// Correlativo
                        }
                        inputWorkSheet.Cells.Range["H" + pos].Value = gridItem.Cells[6].Text;// Cliente
                        inputWorkSheet.Cells.Range["L" + pos].Value = DateTime.Today;// Fecha
                        pos = pos + 1;
                        R.newLine();
                        R.LoadRemision("1");
                        R.NUMEROREMISION = U.TABLA.Rows[0]["Numero"].ToString();
                        R.IDINSTRUCCION = gridItem.Cells[3].Text;
                        R.FECHAENVIO = DateTime.Now.ToString();
                        R.USUARIOENVIO = Session["IdUsuario"].ToString();
                        R.commitLine();
                        R.actualizar();
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    }
                }
                inputWorkSheet.Cells.Range["D" + (pos + 3)].Value = U.TABLA.Rows[0]["Nombre"].ToString();// Nombre
                NombreUsuarioRemision = U.TABLA.Rows[0]["Nombre"].ToString();
                NumeroRemision = U.TABLA.Rows[0]["Numero"].ToString();
                inputWorkSheet.Cells.Range["D" + 9].Value = U.TABLA.Rows[0]["NombreAduana"].ToString();// Aduana
                inputWorkSheet.Cells.Range["I" + 6].Value = U.TABLA.Rows[0]["Numero"].ToString();// Numero de Remision
                inputWorkSheet.Cells.Range["K" + 9].Value = DateTime.Today;
                N.LoadNumeraciones(Session["CodigoAduana"].ToString());
                if (N.totalRegistros > 0)
                {
                    N.NUMERACION = N.NUMERACION + 1;
                    N.actualizar();
                }
                #endregion
                workbook.Save();
                //workbook.ExportAsFixedFormat(XlFixedFormatType.xlTypePDF,"J:\\Files1\\Aduanas\\Remisiones\\Remision " + U.TABLA.Rows[0]["Numero"].ToString());
            }
            catch (Exception ex)
            {
            }
            finally
            {
                workbook.Close();
                xlApp.Quit();
                desconectar();
                Application.UnLock();
                desconectar();
            }
          try
        {

            EnviarCorreo(NumeroRemision, NombreUsuarioRemision);
            rgInstrucciones.Rebind();
            if (hayRegistro)
            {
                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + nameDest);
                Response.Charset = "";
                Response.TransmitFile("J:\\Files1\\Aduanas\\" + nameDest);
                Response.Flush();
                Response.End();
            }
        }
          catch (Exception ex)
          { 

          }
        }

    }

    protected void EnviarCorreo(string NumeroRemision, string NombreUsuarioRemision)
    {
        try
        {
            conectar();
            CorreosBO C = new CorreosBO(logApp);
            C.LoadCorreosXPais(Session["Pais"].ToString(), Session["CodigoAduana"].ToString());
            string Asunto = "Remision Numero " + NumeroRemision;

            string Cuerpo = NombreUsuarioRemision + "<tr>Adjunto la remision numero " + NumeroRemision + "</tr><tr>Favor tomar nota.</tr> <tr></tr><tr>Este mensaje es generado automaticamente, favor no responderlo</tr>";

            string Copias = C.COPIAS;
            string NombreUsuario = "Tracking Aduanas";
            string NombredelArchivo = "Remision";
            string usuario = "trackingaduanas";
            string pass = "nMgtdA$7PaRjQphEYZBD";
            string para = C.CORREOS;
            sendMailConParametros(para, Asunto, Cuerpo, Copias, NombreUsuario, NombredelArchivo, usuario, pass, ".xlsx");
          //  sendMailConParametros(para, Asunto, Cuerpo, Copias, NombreUsuario, "\\Remisiones\\" + NombredelArchivo + " " + NumeroRemision, usuario, pass, ".pdf");
        }
        catch (Exception)
        {

        }
        finally { desconectar(); }

    }
}