﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ObjetosDto;
using Telerik.Web.UI;
using Utilities.Web;

public partial class FcJefeDeAduanaEstados : Utilidades.PaginaBase
{
    readonly AduanasDataContext _aduanasDc = new AduanasDataContext();
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Jefe De Aduana";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgAsistenteOperaciones.FilterMenu);
        rgAsistenteOperaciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgAsistenteOperaciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Jefe De Aduana Estados", "Jefe De Aduana Estados");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
        }
    }

    private bool Anular { get { return tienePermiso("ANULAR"); } }
    private bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    private bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    public override bool CanGoBack { get { return false; } }

    #region Jefe De Aduana Estados
    protected void rgAsistenteOperaciones_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        LlenarGrid();
    }

#region Comentado
    //private void LlenarGrid()
    //{
    //    try
    //    {
    //        var fechaActual = DateTime.Now;
    //        var estados = new List<string> { "3", "4", "5", "6", "7", "8", "10", "11" };

    //        var tfg = _aduanasDc.TiemposFlujoCarga.Where(t => t.Eliminado == false&& t.Fecha>Convert.ToDateTime("2016-05-01"))
    //             .OrderByDescending(t => t.Id)
    //             .GroupBy(t => t.IdInstruccion)
    //             .Select(g => new { g, count = g.Count() })
    //             .ToList();

    //        var idsTf = tfg.SelectMany(tg => tg.g.Select(t => t).Zip(Enumerable.Range(1, tg.count), (j, i) => new { j.Id, rn = i }))
    //            .Where(s => s.rn == 1)
    //            .Select(s => s.Id)
    //            .ToList();

    //        var query = _aduanasDc.ProcesoFlujoCarga
    //            .Where(p => p.Eliminado == false && estados.Contains(p.CodEstado) && p.CodigoAduana != "016")
    //            .Join(_aduanasDc.Instrucciones.Select(i => new { i.CodEstado, i.IdInstruccion, i.CodRegimen, i.NumeroFactura })
    //            .Where(i => i.CodEstado != "100"), p => p.IdInstruccion, i => i.IdInstruccion, (p, i) => new { p, i })
    //            .Join(_aduanasDc.TiemposFlujoCarga.Where(t => idsTf.Contains(t.Id)).Select(t => new { t.IdInstruccion, t.IdEstado, t.Fecha }),
    //            pi => pi.p.IdInstruccion, t => t.IdInstruccion, (pi, t) => new { pi, t })
    //            .Join(_aduanasDc.Codigos.Where(c => c.Eliminado == '0' && c.Categoria == "ESTADO FLUJO CARGA AUTOMATICO"),
    //            pit => pit.t.IdEstado, c => c.Codigo1, (pit, c) => new { pit, c })
    //            .Select(pitc => new ProcesoFlujoCargaGridVm
    //            {
    //                Id = pitc.pit.pi.p.Id,
    //                IdInstruccion = pitc.pit.pi.p.IdInstruccion,
    //                Cliente = pitc.pit.pi.p.Cliente,
    //                Proveedor = pitc.pit.pi.p.Proveedor,
    //                CodRegimen = pitc.pit.pi.i.CodRegimen,
    //                NoFactura = pitc.pit.pi.i.NumeroFactura,
    //                Correlativo = pitc.pit.pi.p.Correlativo,
    //                CanalSelectividad = pitc.pit.pi.p.CanalSelectividad,
    //                CodigoAduana = pitc.pit.pi.p.CodigoAduana,
    //                Estado = pitc.c.Descripcion,
    //                FechaUltimoSello = pitc.pit.t.Fecha,
    //                TiempoTranscurrido = "Días: " + (fechaActual - pitc.pit.t.Fecha).Days
    //                + " Horas: " + (fechaActual - pitc.pit.t.Fecha).Hours
    //                + " Minutos: " + (fechaActual - pitc.pit.t.Fecha).Minutes,
    //                IdTramitador = pitc.pit.pi.p.IdTramitador,
    //            })
    //               .ToList();

    //        if (User.IsInRole("Jefes Aduana"))
    //        {
    //            //cuando es jefe de aduana el CodigoAduana es condicion
    //            var codigoAduana = _aduanasDc.Usuarios.Where(u => u.IdUsuario == Convert.ToDecimal(Session["IdUsuario"].ToString()) && u.Eliminado == '0')
    //            .Select(u => u.CodigoAduana).SingleOrDefault();
    //            if (codigoAduana != "016")
    //            {
    //                query = query.Where(q => q.CodigoAduana == codigoAduana).ToList();
    //            }
    //            else
    //                query = new List<ProcesoFlujoCargaGridVm>();
    //        }
    //        else if (!User.IsInRole("Administradores"))
    //        {
    //            query = new List<ProcesoFlujoCargaGridVm>();
    //        }
    //        rgAsistenteOperaciones.DataSource = query;
    //    }
    //    catch { }
    //}
#endregion

    private void LlenarGrid()
    {
        try
        {
            var fechaActual = DateTime.Now;
            var estados = new List<string> { "3", "4", "5", "6", "7", "8", "10", "11" };

            //var tfg = _aduanasDc.TiemposFlujoCargas.Where(t => t.Eliminado == false)
            //     .OrderByDescending(t => t.Id)
            //     .GroupBy(t => t.IdInstruccion)
            //     .Select(g => new { g, count = g.Count() })
            //     .ToList();

            var pfc = _aduanasDc.ProcesoFlujoCarga
                .Where(p => p.Eliminado == false && estados.Contains(p.CodEstado) && p.CodigoAduana != "016");

            var instruciones = pfc.Select(p => p.IdInstruccion).ToList();

            var tfg = _aduanasDc.TiemposFlujoCarga.Where(t => t.Eliminado == false && instruciones.Contains(t.IdInstruccion))
                 .OrderByDescending(t => t.Id)
                 .GroupBy(t => t.IdInstruccion)
                 .Select(g => new { g, count = g.Count() })
                 .ToList();

            var idsTf = tfg.SelectMany(tg => tg.g.Select(t => t).Zip(Enumerable.Range(1, tg.count), (j, i) => new { j.Id, rn = i }))
                .Where(s => s.rn == 1)
                .Select(s => s.Id)
                .ToList();

            var query = pfc
                //aduanasDc.ProcesoFlujoCargas
                //.Where(p => p.Eliminado == false && estados.Contains(p.CodEstado) && p.CodigoAduana != "016")
                .Join(_aduanasDc.Instrucciones.Select(i => new { i.CodEstado, i.IdInstruccion, i.CodRegimen, i.NumeroFactura })
                .Where(i => i.CodEstado != "100"), p => p.IdInstruccion, i => i.IdInstruccion, (p, i) => new { p, i })
                .Join(_aduanasDc.TiemposFlujoCarga.Where(t => idsTf.Contains(t.Id)).Select(t => new { t.IdInstruccion, t.IdEstado, t.Fecha }),
                pi => pi.p.IdInstruccion, t => t.IdInstruccion, (pi, t) => new { pi, t })
                .Join(_aduanasDc.Codigos.Where(c => c.Eliminado == '0' && c.Categoria == "ESTADO FLUJO CARGA AUTOMATICO"),
                pit => pit.t.IdEstado, c => c.Codigo1, (pit, c) => new { pit, c })
                .Select(pitc => new ProcesoFlujoCargaGridVm
                {
                    Id = pitc.pit.pi.p.Id,
                    IdInstruccion = pitc.pit.pi.p.IdInstruccion,
                    Cliente = pitc.pit.pi.p.Cliente,
                    Proveedor = pitc.pit.pi.p.Proveedor,
                    CodRegimen = pitc.pit.pi.i.CodRegimen,
                    NoFactura = pitc.pit.pi.i.NumeroFactura,
                    Correlativo = pitc.pit.pi.p.Correlativo,
                    CanalSelectividad = pitc.pit.pi.p.CanalSelectividad,
                    CodigoAduana = pitc.pit.pi.p.CodigoAduana,
                    Estado = pitc.c.Descripcion,
                    FechaUltimoSello = pitc.pit.t.Fecha,
                    TiempoTranscurrido = "Días: " + (fechaActual - pitc.pit.t.Fecha).Days
                    + " Horas: " + (fechaActual - pitc.pit.t.Fecha).Hours
                    + " Minutos: " + (fechaActual - pitc.pit.t.Fecha).Minutes,
                    IdTramitador = pitc.pit.pi.p.IdTramitador,
                })
                   .ToList();

            if (User.IsInRole("Jefes Aduana"))
            {
                //cuando es jefe de aduana el CodigoAduana es condicion
                var codigoAduana = _aduanasDc.Usuarios.Where(u => u.IdUsuario == Convert.ToDecimal(Session["IdUsuario"].ToString()) && u.Eliminado == '0')
                .Select(u => u.CodigoAduana).SingleOrDefault();
                if (codigoAduana != "016")
                {
                    query = query.Where(q => q.CodigoAduana == codigoAduana).ToList();
                }
                else
                    query = new List<ProcesoFlujoCargaGridVm>();
            }
            else if (!User.IsInRole("Administradores"))
            {
                query = new List<ProcesoFlujoCargaGridVm>();
            }
            rgAsistenteOperaciones.DataSource = query;
        }
        catch { }
    }
    protected void rgAsistenteOperaciones_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            var editedItem = e.Item as GridEditableItem;
            if (e.CommandName == "Descargar")
            {
                if (editedItem != null)
                {
                    var idInstruccion = editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdInstruccion"].ToString();
                    SessionFileShare.CreateDownload(idSistema: idInstruccion, page: this);
                }
            }
            if (e.CommandName == "Guardar")
            {
                if (editedItem != null)
                {
                    var edTramitador = (RadComboBox)editedItem.FindControl("edTramitador");
                    var id = Convert.ToDecimal(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"]);

                    if (!String.IsNullOrEmpty(edTramitador.SelectedValue))
                    {

                        var pfc = _aduanasDc.ProcesoFlujoCarga.SingleOrDefault(p => p.Id == id && p.Eliminado == false);
                        if (pfc != null)
                        {
                            pfc.IdTramitador = Convert.ToDecimal(edTramitador.SelectedValue);
                          //  pfc.CodEstado = "11";

                            var tfc = new TiemposFlujoCarga
                            {
                                IdInstruccion = pfc.IdInstruccion,
                                IdEstado = "11",    //revisar que estado es para asignacion de tramitador
                                Fecha = DateTime.Now,
                                FechaIngreso = DateTime.Now,
                                Observacion = "Asignación de tramitador: " + edTramitador.Text,
                                Eliminado = false,
                                IdUsuario = Convert.ToDecimal(Session["IdUsuario"])
                            };
                            _aduanasDc.TiemposFlujoCarga.InsertOnSubmit(tfc);

                            _aduanasDc.SubmitChanges();
                            RegistrarMensaje2("Tramitador asignado exitosamente");
                            rgAsistenteOperaciones.Rebind();
                        }
                        else
                        {
                            RegistrarMensaje2("Registro presenta error...favor comunique al administrador del sistema");
                        }
                    }
                    else
                        RegistrarMensaje2("Seleccione un tramitador");
                }
            }
            if (rgAsistenteOperaciones.Items.Count >= 1 & (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName))
                ConfigureExport("");
            else if (rgAsistenteOperaciones.Items.Count < 1 & (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName))
                e.Canceled = true;
        }
        catch { }
    }

    private void ConfigureExport(string descripcion)
    {
        HttpPostedFileBase fileBase;
        var filename = descripcion + " " + DateTime.Now.ToShortDateString();
        rgAsistenteOperaciones.ExportSettings.FileName = filename;
        rgAsistenteOperaciones.ExportSettings.ExportOnlyData = true;
        rgAsistenteOperaciones.ExportSettings.IgnorePaging = true;
        rgAsistenteOperaciones.ExportSettings.OpenInNewWindow = true;
        rgAsistenteOperaciones.MasterTableView.ExportToExcel();
    }

    protected void rgAsistenteOperaciones_Init(object sender, EventArgs e)
    {
        var menu = rgAsistenteOperaciones.FilterMenu;
        menu.Items.RemoveAt(rgAsistenteOperaciones.FilterMenu.Items.Count - 2);
    }

    private void RegistrarMensaje2(string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("alert('" + mensaje + "');");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "mensaje", sb.ToString(), false);
    }
    #endregion
    protected void rgAsistenteOperaciones_ItemDataBound(object sender, GridItemEventArgs e)
    {
        try
        {
            foreach (GridDataItem gridItem in rgAsistenteOperaciones.Items)
            {
                var edTramitador = (RadComboBox)gridItem.FindControl("edTramitador");

                //var codigoAduana = _aduanasDc.Usuarios.Where(u => u.IdUsuario == Convert.ToDecimal(Session["IdUsuario"].ToString()) && u.Eliminado == '0')
                //    .Select(u => u.CodigoAduana).SingleOrDefault();
                var codigoAduana = gridItem["CodigoAduana"].Text;
                var idTramitador = gridItem["IdTramitador"].Text;
                if (!String.IsNullOrEmpty(codigoAduana))
                {
                    var query = _aduanasDc.Usuarios.Where(u => u.Eliminado == '0' && u.CodigoAduana == codigoAduana)
                        .Select(u => new { u.CodigoAduana, u.IdUsuario, u.Nombre, u.Apellido })
                        .Join(_aduanasDc.UsuariosRoles.Where(r => r.Eliminado == '0' && r.IdRol == 3), u => u.IdUsuario, r => r.IdUsuario, (u, r) => new { u, r })
                        .Select(ur => new UsuarioVm
                        {
                            IdUsuario = ur.u.IdUsuario,
                            NombreCompleto = ur.u.Nombre.Trim() + " " + ur.u.Apellido.Trim(),
                        }).ToList();

                    edTramitador.SelectedValue = idTramitador;
                    edTramitador.DataSource = query;
                    edTramitador.DataBind();
                }
            }
        }
        catch { }
    }

    protected void rgAsistenteOperaciones_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
    {
        try
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            switch (e.DetailTableView.DataMember)
            {
                case "DetalleEstados":
                    {
                        var idInstruccion = dataItem.Cells[3].Text;
                        //var idInstruccion = dataItem["IdInstruccion"].Text;

                        var query = _aduanasDc.TiemposFlujoCarga.Where(
                            t => t.Eliminado == false && t.IdInstruccion == idInstruccion)
                            .Join(_aduanasDc.Codigos.Where(c => c.Eliminado == '0' && c.Categoria == "ESTADO FLUJO CARGA AUTOMATICO"), t => t.IdEstado, c => c.Codigo1, (t, c) => new { t, c })
                            .Join(_aduanasDc.Usuarios.Where(u => u.Eliminado == '0').Select(u => new { u.IdUsuario, u.Usuario1 }),
                            tc => tc.t.IdUsuario, u => u.IdUsuario, (tc, u) => new { tc, u })
                            .Select(tcu => new TiemposFlujoCargaVm
                            {
                                IdInstruccion = tcu.tc.t.IdInstruccion,
                                Estado = tcu.tc.c.Descripcion,
                                Fecha = tcu.tc.t.Fecha,
                                Observacion = tcu.tc.t.Observacion,
                                Usuario = tcu.u.Usuario1
                            }).ToList();
                        if (query.Any())
                            e.DetailTableView.DataSource = query;
                        else
                            e.DetailTableView.DataSource = new List<string>();
                        break;
                    }
            }
        }
        catch (Exception)
        { }
    }
}