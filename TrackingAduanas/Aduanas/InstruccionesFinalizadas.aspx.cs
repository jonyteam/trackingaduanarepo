﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;

using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;

public partial class InstruccionesFinalizadas : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Instrucciones";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgInstrucciones.FilterMenu);
        rgInstrucciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgInstrucciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Instrucciones Finalizadas", "Instrucciones Finalizadas");
            mpInstrucciones.SelectedIndex = 0;
            conectar();
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuarioLogin(User.Identity.Name);
            if (User.IsInRole("Administradores") || User.IsInRole("Administrador Pais") || User.IsInRole("Administrador Pais SVGT")|| (User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT") & u.CODPAIS != "H"))
                Table1.Visible = true;
            else
                Table1.Visible = false;
            //if (Anular || Ingresar || Modificar)
            //    redirectTo("Default.aspx");
        }
    }

    //protected bool Anular { get { return tienePermiso("ANULAR"); } }
    //protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    //protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    #region Instrucciones
    protected void rgInstrucciones_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridInstrucciones();
    }

    private void llenarGridInstrucciones()
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            if (User.IsInRole("Administradores"))
                i.loadInstruccionesFinalizadas();
            else if (User.IsInRole("Operaciones") || User.IsInRole("Oficial de Cuenta") || User.IsInRole("Administrador Pais"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.totalRegistros > 0)
                    i.loadInstruccionesXPaisFinalizadas(u.CODPAIS);
            }
            else if (User.IsInRole("Jefes Aduana"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.totalRegistros > 0)
                    i.loadInstruccionXAduanaFinalizadas(u.CODIGOADUANA);
            }
            else if (User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT"))
            {
                string idU = Session["IdUsuario"].ToString();
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.totalRegistros > 0)
                    if (u.CODPAIS == "H")
                        i.loadInstruccionXAduanaUsuarioFinalizadas(u.CODIGOADUANA, idU);
                    else
                        i.loadInstruccionXUsuarioFinalizadas(idU);
            }
            rgInstrucciones.DataSource = i.TABLA;
        }
        catch { }
    }

    private void llenarGridInstruccion(string idInstruccion)
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            i.loadInstruccionFinalizada(idInstruccion);
            rgInstrucciones.DataSource = i.TABLA;
            rgInstrucciones.DataBind();
        }
        catch { }
    }

    protected void rgInstrucciones_Init(object sender, System.EventArgs e)
    {
        GridFilterMenu menu = rgInstrucciones.FilterMenu;
        menu.Items.RemoveAt(rgInstrucciones.FilterMenu.Items.Count - 2);
    }

    protected void rgInstrucciones_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            HistorialImpresionesBO hi = new HistorialImpresionesBO(logApp);
            GridEditableItem editedItem = e.Item as GridEditableItem;
            if (e.CommandName == "Reporte")
            {
                string idInstruccion = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdInstruccion"]);
                if (!String.IsNullOrEmpty(idInstruccion.Trim()))
                {
                    //registra de la impresion del archivo
                    try
                    {
                        hi.loadHistorialImpresiones("-1");
                        hi.newLine();
                        hi.DOCUMENTO = idInstruccion;
                        hi.DESCRIPCION = "Se imprimió instrucción de Instrucciones Finalizadas";
                        hi.FECHA = DateTime.Now.ToString();
                        hi.IDUSUARIO = Session["IdUsuario"].ToString();
                        hi.commitLine();
                        hi.actualizar();
                    }
                    catch { }
                    try
                    {
                        Response.Redirect("ReporteInstrucciones.aspx?IdInstruccion=" + idInstruccion);
                    }
                    catch { }
                }
                else
                    registrarMensaje("Instrucción no existe, consulte con el administrador del sistema");
            }
            else if (rgInstrucciones.Items.Count <= 0 & e.CommandName == RadGrid.ExportToExcelCommandName)
                e.Canceled = true;
            else if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                ConfigureExport();
            }
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "InstruccionesFinalizadas");
        }
        finally
        {
            desconectar();
        }
    }

    private void ConfigureExport()
    {
        rgInstrucciones.Columns[0].Visible = false;
        String filename = "InstruccionesFinalizadas" + "_" + DateTime.Now.ToShortDateString();
        rgInstrucciones.GridLines = GridLines.Both;
        rgInstrucciones.ExportSettings.FileName = filename;
        rgInstrucciones.ExportSettings.IgnorePaging = true;
        rgInstrucciones.ExportSettings.OpenInNewWindow = false;
        rgInstrucciones.ExportSettings.ExportOnlyData = true;
    }

    public override bool CanGoBack { get { return false; } }
    #endregion

    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            conectar();
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (User.IsInRole("Administradores"))
                llenarGridInstruccion(txtInstruccion.Text.Trim());
            else if (User.IsInRole("Administrador Pais") || User.IsInRole("Administrador Pais SVGT")& (txtInstruccion.Text.Trim().Substring(0, 1) == u.CODPAIS))
                llenarGridInstruccion(txtInstruccion.Text.Trim());
            else
            {
                rgInstrucciones.Rebind();
                registrarMensaje("No puede consultar instrucciones de otro país");
            }
        }
        catch { }
    }
}