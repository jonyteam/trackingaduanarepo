﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteDiarioAseguramiento.aspx.cs" Inherits="ReporteEventos" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                    //<![CDATA[
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }

                function openWin() {
                    var oWnd = radopen("BuscarTramite.aspx", "RadWindow1");
                    oWnd.add_close(OnClientClose);
                }           
                
            </script>
        </telerik:RadScriptBlock>
        <table border="1" style="width: 45%">
            <tr>
                <td style="width: 10%">
                    <asp:Label ID="lblPais" runat="server" Text="País:" ForeColor="Black"></asp:Label>
                </td>
                <td style="width: 16%">
                    <telerik:RadComboBox ID="cmbPais" runat="server" DataTextField="Descripcion" DataValueField="Codigo">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Images/24/view_24.png"
                        OnClick="btnBuscar_Click" />
                </td>
            </tr>
        </table>
        <br />
        <telerik:RadGrid ID="RadGrid1" 
       
            runat="server" AutoGenerateColumns="False" CellSpacing="0" Culture="es-ES" GridLines="None" OnItemCommand="RadGrid1_ItemCommand"
            
           >
            



            <GroupingSettings />
            



            <ExportSettings>
                <Pdf AllowPrinting="False" />
            </ExportSettings>
            <ValidationSettings EnableModelValidation="False" EnableValidation="False" />
            



            <MasterTableView CommandItemDisplay="Top">
                         <CommandItemSettings ShowAddNewRecordButton="false"  ShowRefreshButton="False" 
                            ShowExportToExcelButton="true" ShowExportToCsvButton="false" />

               <Columns>
                            <telerik:GridBoundColumn DataField="IdInstruccion" FilterControlAltText="Filter IdInstruccion column" HeaderText="Hoja de Ruta" UniqueName="IdInstruccion">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Aduana" FilterControlAltText="Filter Aduana column" HeaderText="Aduana" UniqueName="Aduana">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Cliente" FilterControlAltText="Filter Cliente column" HeaderText="Cliente" UniqueName="Cliente">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Producto" FilterControlAltText="Filter Producto column" HeaderText="Producto" UniqueName="Producto">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ArribodeCarga" FilterControlAltText="Filter ArribodeCarga column" HeaderText="ArribodeCarga" UniqueName="ArribodeCarga">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ValidacionElectonica" FilterControlAltText="Filter ValidacionElectonica column" HeaderText="ValidacionElectonica" UniqueName="ValidacionElectonica">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PagodeImpuestos" FilterControlAltText="Filter PagodeImpuestos column" HeaderText="PagodeImpuestos" UniqueName="PagodeImpuestos">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Color" FilterControlAltText="Filter Color column" HeaderText="Color" UniqueName="Color">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="EntregadeServicio" FilterControlAltText="Filter EntregadeServicio column" HeaderText="EntregadeServicio" UniqueName="EntregadeServicio">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Regimen" FilterControlAltText="Filter Regimen column" HeaderText="Regimen" UniqueName="Regimen">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CiudadDestino" FilterControlAltText="Filter CiudadDestino column" HeaderText="CiudadDestino" UniqueName="CiudadDestino">
                            </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>



        </telerik:RadGrid>
    </telerik:RadAjaxPanel>
</asp:Content>
