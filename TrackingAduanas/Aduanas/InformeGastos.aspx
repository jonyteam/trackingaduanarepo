﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="InformeGastos.aspx.cs" Inherits="CargarArchivo" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .wrap
        {
            white-space: normal;
            width: 98px;
        }
        
        .style1
        {
            width: 100px;
        }
        .auto-style1 {
            width: 15%;
        }
        .auto-style2 {
            width: 7%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    .<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest"
        ClientEvents-OnRequestStart="requestStart">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgIngresos">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgIngresos" LoadingPanelID="LoadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgIngresos" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" EnableSkinTransparency="true">
        <div class="loading">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Ajax/Img/loading1.gif" AlternateText="loading">
            </asp:Image>
        </div>
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            //<![CDATA[
            function requestStart(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
            }

            function OnClientClose() {
                $find("<%= RadAjaxManager1.ClientID%>").ajaxRequest("Rebind");
            }
        </script>
    </telerik:RadScriptBlock>
    <telerik:RadMultiPage ID="mpRequerimiento" runat="server" SelectedIndex="0">
        <telerik:RadPageView ID="RadPageView1" runat="server" Width="99.9%">
            <table width="100%">
                <tr>
                    <td style="width: 100%" colspan="4">
                        <asp:Label ID="lblIngresos" runat="server" Text="Informe de Gastos" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <telerik:RadTextBox ID="txtInstruccion" Runat="server" EmptyMessage="Ingrese Hoja de Ruta" LabelWidth="64px" Width="160px">
                        </telerik:RadTextBox>
                    </td>
                    <td class="auto-style2">
                        <asp:ImageButton ID="imgBuscar" runat="server" Enabled="False" ImageUrl="~/Images/24/view_24.png" OnClick="imgBuscar_Click" />
                    </td>
                    <td style="width: 100%">&nbsp;</td>
                    <td style="width: 100%">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="4">
                        <input id="IdTransaccion" runat="server" type="hidden" />
                        <telerik:RadGrid ID="rgIngresos" runat="server" AllowFilteringByColumn="True" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" GridLines="None" Height="420px" OnDetailTableDataBind="rgIngresos_DetailTableDataBind" OnInit="rgIngresos_Init" OnItemCommand="rgIngresos_ItemCommand" OnNeedDataSource="rgIngresos_NeedDataSource" PageSize="20" ShowFooter="True" ShowStatusBar="True" Width="99.9%">
                            <PagerStyle Mode="NextPrevAndNumeric" NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente" PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}." PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                            <MasterTableView CommandItemDisplay="Top" DataKeyNames="" GroupLoadMode="Client" NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay ingresos.">
                                <DetailTables>
                                    <telerik:GridTableView runat="server" AllowFilteringByColumn="False">
                                        <CommandItemSettings ExportToPdfText="Export to PDF" />
                                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                                        </ExpandCollapseColumn>
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="Gasto" FilterControlAltText="Filter Gssto column" HeaderStyle-Width="140px" HeaderText="Gasto" UniqueName="Gasto">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="MedioPago" FilterControlAltText="Filter MedioPago column" HeaderStyle-Width="90px" HeaderText="Medio de Pago" UniqueName="MedioPago">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Monto" DataFormatString="{0:#,###.00}" FilterControlAltText="Filter Monto column" HeaderStyle-Width="90px" HeaderText="Monto" UniqueName="Monto">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="IVA" DataFormatString="{0:#,###.00}" FilterControlAltText="Filter ISV column" HeaderStyle-Width="90px" HeaderText="IVA" UniqueName="ISV">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ValorNegociado" DataFormatString="{0:#,###.00}" FilterControlAltText="Filter Monto column" HeaderStyle-Width="90px" HeaderText="Valor Negociado" UniqueName="ValorNegociado">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Proveedor" FilterControlAltText="Filter Proveedor column" HeaderStyle-Width="250px" HeaderText="Proveedor" UniqueName="Proveedor">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EstadoActual" FilterControlAltText="Filter EstadoActual column" HeaderStyle-Width="150px" HeaderText="Estado Actual" UniqueName="EstadoActual">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="UltimaFecha" FilterControlAltText="Filter FechaUltimaModificacion column" HeaderStyle-Width="150px" HeaderText="Ultima Modificacion" UniqueName="FechaUltimaModificacion">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Facturado" FilterControlAltText="Filter Facturado column" HeaderStyle-Width="150px" HeaderText="Facturado" UniqueName="Facturado">
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                    </telerik:GridTableView>
                                </DetailTables>
                                <CommandItemSettings RefreshText="Volver a Cargar Datos" ShowAddNewRecordButton="false" ShowExportToCsvButton="false" ShowExportToExcelButton="true" />
                                <RowIndicatorColumn>
                                    <HeaderStyle Width="20px" />
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn>
                                    <HeaderStyle Width="20px" />
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridBoundColumn DataField="HojaRuta" FilterControlAltText="Filter HojaRuta column" HeaderText="Hoja de Ruta" UniqueName="HojaRuta">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Clientes" FilterControlAltText="Filter Clientes column" HeaderText="Clientes" UniqueName="Clientes">
                                    </telerik:GridBoundColumn>
                                    <%--<telerik:GridBoundColumn DataField="ArchivosCargardos" HeaderText="Archivos Cargardos" UniqueName="ArchivosCargardos"
                                        FilterControlWidth="80%">
                                        <HeaderStyle Width="130px" />
                                    </telerik:GridBoundColumn>--%>
                                    <telerik:GridBoundColumn DataField="Aduana" FilterControlAltText="Filter Aduana column" HeaderText="Aduana" UniqueName="Aduana">
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                            </MasterTableView>
                            <HeaderStyle Width="180px" />
                            <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                                <Selecting AllowRowSelect="True" />
                                <ClientMessages DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño" DropHereToReorder="Suelte aquí para Re-Ordenar" PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas" />
                                <Scrolling AllowScroll="True" SaveScrollPosition="True" UseStaticHeaders="True" />
                            </ClientSettings>
                            <FilterMenu EnableTheming="True">
                                <CollapseAnimation Duration="200" Type="OutQuint" />
                            </FilterMenu>
                            <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                            <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
                        </telerik:RadGrid>
                    </td>
                </tr>
            </table>
            <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            </telerik:RadWindowManager>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <script type="text/javascript">
        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                            args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                            args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                args.set_enableAjax(false);
            }
        }
        var message = "Grupo Vesta, Derechos Reservados";

        function click(e) {
            if (document.all) {
                if (event.button == 2) {
                    alert(message);
                    return false;
                }
            }
            if (document.layers) {
                if (e.which == 3) {
                    alert(message);
                    return false;
                }
            }
        }
        if (document.layers) {
            document.captureEvents(Event.MOUSEDOWN);
        }
        document.onmousedown = click;

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }

        function Close() {
            var oWindow = GetRadWindow();
            oWindow.argument = null;
            oWindow.close();
        }
    </script>
</asp:Content>
