﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;

public partial class AnulacionEspeciesFiscales : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Inventario Usuarios";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgRangoEspeciesFiscales.FilterMenu);
        rgRangoEspeciesFiscales.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgRangoEspeciesFiscales.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Inventario Aduana", "Inventario Aduana");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
            mpEspeciesFiscales.SelectedIndex = 0;
            limpiarControles();
            cargarDatosInicioRangos();
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    #region Anulación Especies Fiscales

    protected void rgRangoEspeciesFiscales_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenarGridRangoEspeciesFiscales();
    }

    private void llenarGridRangoEspeciesFiscales()
    {
        try
        {
            conectar();
            MovimientosEspeciesFiscalesBO mef = new MovimientosEspeciesFiscalesBO(logApp);
            UsuariosBO U = new UsuariosBO(logApp);
            U.loadUsuario(Session["IdUsuario"].ToString());
            mef.loadRangosEspeciesFiscalesAnulacion(U.CODIGOADUANA, cmbEspecieFiscal.SelectedValue);
            rgRangoEspeciesFiscales.DataSource = mef.TABLA;
            rgRangoEspeciesFiscales.DataBind();
        }
        catch { }
    }

    protected void rgRangoEspeciesFiscales_Init(object sender, EventArgs e)
    {
        GridFilterMenu menu = rgRangoEspeciesFiscales.FilterMenu;
        menu.Items.RemoveAt(rgRangoEspeciesFiscales.FilterMenu.Items.Count - 2);
    }

    private void cargarDatosInicioRangos()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            c.loadEspeciesnoAdmin();
            cmbEspecieFiscal.DataSource = c.TABLA;
            cmbEspecieFiscal.DataBind();
            cmbEspecieFiscal.Items.Remove(9);
        }
        catch { }
    }

    
    protected void btnLimpiar_Click(object sender, ImageClickEventArgs e)
    {
        limpiarControles();
    }

    private void limpiarControles()
    {
        try
        {
            cargarDatosInicioRangos();
            cmbEspecieFiscal.ClearSelection();
            llenarGridRangoEspeciesFiscales();
        }
        catch { }
    }
    #endregion

    protected void cmbEspecieFiscal_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        llenarGridRangoEspeciesFiscales();
    }

}