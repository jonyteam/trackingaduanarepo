﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;

public partial class BusquedaAvanzada : Utilidades.PaginaBase
{
    protected string codigoAduana = "";
    protected string nombreAduana = "";
    //private int numRows;
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Busqueda Avanzada";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        codigoAduana = parameterForm("CodigoAduana", "");
        nombreAduana = parameterForm("NombreAduana", "");
        SetGridFilterMenu(rgInstrucciones.FilterMenu);
        rgInstrucciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgInstrucciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Busqueda Avanzada", "Búsqueda Avanzada");
            mpBusquedaAvanzada.SelectedIndex = 0;

            try
            {
                conectar();
                AduanasBO a = new AduanasBO(logApp);
                UsuariosBO u = new UsuariosBO(logApp);
                u.loadUsuario(Session["IdUsuario"].ToString());
                a.loadAduanas(u.CODIGOADUANA);
                codigoAduana = a.CODIGOADUANA;
                nombreAduana = a.NOMBREADUANA;
            }
            catch { }
            finally
            {
                desconectar();
            }
        }
    }

    #region Instrucciones
    protected void rgInstrucciones_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        try
        {
            llenarGrid(edCampos.Value, edCriterio.Value);
        }
        catch
        {
            rgInstrucciones.Visible = false;
        }
        //if (IsPostBack)
        //    llenarGrid(edCampos.Value, edCriterio.Value);
        //llenarGridInstrucciones();
    }

    protected void rgInstrucciones_Init(object sender, System.EventArgs e)
    {
        GridFilterMenu menu = rgInstrucciones.FilterMenu;
        menu.Items.RemoveAt(rgInstrucciones.FilterMenu.Items.Count - 2);
    }

    protected void rgInstrucciones_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            GridEditableItem editedItem = e.Item as GridEditableItem;
            InstruccionesBO i = new InstruccionesBO(logApp);
            if (e.CommandName == "Gestion")
            {
                //idInstruccion.Value = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdInstruccion"]);

                //mpBusquedaAvanzada.SelectedIndex = 1;
            }
            else if (rgInstrucciones.Items.Count <= 0 & e.CommandName == RadGrid.ExportToExcelCommandName)
                e.Canceled = true;
            else if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
                ConfigureExport();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void btnBack_Click(object sender, ImageClickEventArgs e)
    {
        mpBusquedaAvanzada.SelectedIndex = 0;
    }
    #endregion

    public override bool CanGoBack { get { return false; } }

    protected void cmbCampo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (User.IsInRole("Jefes Aduana") && cmbCampo.SelectedValue == "Aduana")
            {
                //txtValor.Text = nombreAduana;
                txtValor.Enabled = false;
                edFecha.Visible = false;
            }
            else
            {
                txtValor.Enabled = true;
                txtValor.Text = "";
                edFecha.Visible = false;
            }
            if (cmbCampo.SelectedValue.Contains("Fecha"))
            {
                txtValor.Visible = false;
                edFecha.Visible = true;
            }
            else
                txtValor.Visible = true;
        }
        catch { }
    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        bool valor = false;
        edBag.Value = "true";
        try
        {
            if (!String.IsNullOrEmpty(cmbCampo.SelectedValue))
            {
                if (!String.IsNullOrEmpty(cmbOperador.SelectedValue))
                {
                    if (cmbCampo.SelectedValue.Contains("Fecha") & edFecha.SelectedDate.HasValue)
                        valor = true;
                    else if (cmbCampo.SelectedValue.Contains("Fecha") & !edFecha.SelectedDate.HasValue)
                        valor = false;
                    if (!cmbCampo.SelectedValue.Contains("Fecha") & !String.IsNullOrEmpty(txtValor.Text.Trim()))
                        valor = true;
                    else if (!cmbCampo.SelectedValue.Contains("Fecha") & String.IsNullOrEmpty(txtValor.Text.Trim()))
                        valor = false;

                    if (valor)
                    {
                        string s1 = "";
                        string s2 = "";
                        string s3 = "";
                        if (cmbSimbolo.SelectedValue.Length > 0)
                        {
                            s1 = s1 + cmbSimbolo.SelectedValue + " ";
                            cmbSimbolo.SelectedValue = "";
                        }
                        if (cmbCampo.SelectedValue.Length > 0)
                        {
                            s3 = cmbCampo.SelectedValue;
                            if (cmbCampo.SelectedValue.Contains("Fecha"))
                                s1 = s1 + "CONVERT(VARCHAR, " + cmbCampo.SelectedValue + ", 111) ";
                            else
                                s1 = s1 + cmbCampo.SelectedValue + " ";
                            cmbCampo.SelectedValue = "";
                        }
                        if (cmbOperador.SelectedValue.Length > 0)
                        {
                            s1 = s1 + cmbOperador.SelectedValue + " ";
                            s2 = cmbOperador.SelectedItem.Text;
                            cmbOperador.SelectedValue = "";
                        }
                        if (txtValor.Text.Length > 0)
                        {
                            if (s3.Contains("Impuesto") || s3.Contains("Anticipo") || s3.Contains("Multa") || s3.Contains("Demora") || s3.Contains("Liquidacion") || s3.Contains("Provisionamiento") || s3.Contains("Fondos") || s3.Contains("pa.Cod"))
                                s1 = s1 + txtValor.Text + " ";
                            else if (s2 == "Contiene")
                                s1 = s1 + "'%" + ((s3.Contains("NombreEmpleado")) ? txtValor.Text : txtValor.Text.ToUpper()) + "%' ";
                            else
                                s1 = s1 + "'" + txtValor.Text + "' ";
                            txtValor.Text = "";
                        }
                        if (edFecha.SelectedDate.HasValue)
                        {
                            s1 = s1 + "'" + edFecha.SelectedDate.Value.ToString("yyyy/MM/dd") + "' ";
                            edFecha.Clear();
                        }
                        if (cmbCriterio.SelectedValue.Length > 0)
                        {
                            s1 = s1 + cmbCriterio.SelectedValue + " ";
                            cmbCriterio.SelectedValue = "";
                        }
                        lstCriterios.Items.Add(new RadListBoxItem(s1));
                        ScriptManager.RegisterClientScriptBlock(rgInstrucciones, GetType(), "refreshColGridHojas", "s();", true);
                    }
                    else
                        registrarMensaje("Debe ingresar el valor");
                }
                else
                    registrarMensaje("Debe seleccionar un operador");
            }
            else
                registrarMensaje("Debe seleccionar un campo");
        }
        catch { }
    }

    private void ConfigureExport()
    {
        String filename = "Instrucciones_" + DateTime.Now.ToShortDateString();
        rgInstrucciones.GridLines = GridLines.Both;
        rgInstrucciones.ExportSettings.FileName = filename;
        rgInstrucciones.ExportSettings.IgnorePaging = true;
        rgInstrucciones.ExportSettings.OpenInNewWindow = false;
        rgInstrucciones.ExportSettings.ExportOnlyData = true;
    }

    
    protected void btnLimpiar_Click(object sender, EventArgs e)
    {
        try
        {
            lstCriterios.Items.Clear();
            ScriptManager.RegisterClientScriptBlock(rgInstrucciones, GetType(), "refreshColGridHojas", "s();", true);
        }
        catch { }
    }

    protected void btnProcesar_Click(object sender, EventArgs e)
    {
        try
        {
            string s = ",";
            if (lstCriterios.Items.Count > 0)
            {
                if (lstCamposMostrar.Items.Count > 0)
                {
                    string criterio = "";
                    for (int i = 0; i < lstCriterios.Items.Count; i++)
                    {
                        criterio += lstCriterios.Items[i].Text;
                    }
                    //History1.AddHistoryPoint("criterio", criterio);
                    foreach (RadListBoxItem listItem in lstCamposMostrar.Items)
                    {
                        s = s + listItem.Value + ",";
                    }
                    s = s.Substring(0, s.Length - 1);
                    //History1.AddHistoryPoint("campos", s);
                    edCampos.Value = s;
                    edCriterio.Value = criterio;
                    llenarGrid(s, criterio);
                    rgInstrucciones.RaisePostBackEvent(RadGrid.ExportToExcelCommandName);
                    lstCriterios.Items.Clear();
                    return;
                }
                else
                    registrarMensaje("Debe haber por lo menos una campo a mostrar");
            }
            registrarMensaje("Debe ingresar un criterio para continuar");
        }
        catch { }
    }

    private void llenarGrid(string campos, string criterio)
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuarioXUsuario(User.Identity.Name);
            if (User.IsInRole("Administradores"))
                i.loadInstruccionBusquedaAvanzada(campos, criterio, "Todas", "", "");
            else if (User.IsInRole("Administrador Pais") || User.IsInRole("Administrador Pais SVGT")|| User.IsInRole("Operaciones") || User.IsInRole("Inplant") || User.IsInRole("Oficial de Cuenta")
                || (User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT") & u.CODPAIS != "H"))
                i.loadInstruccionBusquedaAvanzada(campos, criterio, "Por Pais", u.CODPAIS, "");
            else if (User.IsInRole("Jefes Aduana") || (User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT") || u.CODPAIS == "H"))
                i.loadInstruccionBusquedaAvanzada(campos, criterio, "Por Aduana", u.CODPAIS, u.CODIGOADUANA);
            rgInstrucciones.DataSource = i.TABLA;
            try
            {
                if (edBag.Value == "true")
                {
                    rgInstrucciones.DataBind();
                    rgInstrucciones.Visible = true;
                }
            }
            catch { }
            edBag.Value = "false";
            //HojaRutaBO hojaRutaBO = new HojaRutaBO(logApp);
            //if (User.IsInRole("Jefes Aduana"))
            //    //hojaRutaBO.loadBusquedaHojaRuta(campos, criterio, codigoAduana);
            //else if (!User.IsInRole("Administradores") && !User.IsInRole("Gerentes"))
            //    //hojaRutaBO.loadBusquedaHojaRuta(campos, criterio + " and a.CodigoPais = '" + Session["Pais"].ToString() + "'");
            //else
            //    hojaRutaBO.loadBusquedaHojaRuta(campos, criterio);
            //GridViewHojasRutas.DataSource = hojaRutaBO.TABLA;
            //GridViewHojasRutas.DataBind();
            //ScriptManager.RegisterClientScriptBlock(GridViewHojasRutas, GetType(), "refreshColGridHojas", "s();", true);
        }
        catch (Exception)
        {
        }
        finally
        {
            desconectar();
        }
    }

    protected void btnExport_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            String filename = "Reporte Avanzado ";
            rgInstrucciones.GridLines = GridLines.None;
            rgInstrucciones.ExportSettings.FileName = filename;
            rgInstrucciones.ExportSettings.IgnorePaging = true;
            rgInstrucciones.ExportSettings.OpenInNewWindow = true;
            rgInstrucciones.ExportSettings.ExportOnlyData = true;
            if (sender == btnExport2)
                rgInstrucciones.ExportSettings.Excel.Format = GridExcelExportFormat.ExcelML;

            rgInstrucciones.MasterTableView.ExportToExcel();
        }
        catch (Exception)
        { }
    }
}