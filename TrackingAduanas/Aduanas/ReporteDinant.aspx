﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteDinant.aspx.cs" Inherits="GenerarArchivoSAPDinant" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style12
        {
            width: 100%;
            height: 50px;
        }
        .style13
        {
            width: 22%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="pnlRequestStarted">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">

                function pnlRequestStarted(ajaxPanel, eventArgs) {
                    try {
                        if ((eventArgs.EventTarget == "<%= btnSalvar.UniqueID %>")) {
                            eventArgs.EnableAjax = false;
                        }
                    }
                    catch (err)
                { }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                    }
                }                                  
            </script>
        </telerik:RadScriptBlock>
        <input id="cmbMaterial" runat="server" type="hidden"  />
        <input id="cmbMedioPago" runat="server" type="hidden"  />
        <input id="cmbPagarA" runat="server" type="hidden"  />
        <input id="txtMonto" runat="server" type="hidden"  />
        <input id="txtIVA" runat="server" type="hidden"  />
        <input id="txtMontoPagar" runat="server" type="hidden"  />
        <input id="cmbMoneda" runat="server" type="hidden"  />
        <input id="edFecha" runat="server" type="hidden" />
        <input id="edHora" runat="server" type="hidden" />
        
        <br />
        <table border="1" style="width: 45%">
            <tr>
                <td style="width: 15%">
                    <asp:Label ID="lblFechaInicio0" runat="server" forecolor="Black" 
                        text="Fecha Inicio:">
                </asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadDatePicker ID="dpFechaInicio" runat="server" EnableTyping="False" 
                        Width="95%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        controltovalidate="dpFechaInicio" errormessage="Ingrese Fecha" 
                        forecolor="Red" />
                </td>
                <td style="width: 15%">
                    <asp:Label ID="lblFechaFinal0" runat="server" forecolor="Black" 
                        text="Fecha Final:"></asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadDatePicker ID="dpFechaFinal" runat="server" EnableTyping="False" 
                        Width="95%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator ID="reqName0" runat="server" 
                        controltovalidate="dpFechaFinal" errormessage="Ingrese Fecha" forecolor="Red" />
                </td>
                <td style="width: 10%">
                    <asp:Label ID="lblPais" runat="server" forecolor="Black" text="País:"></asp:Label>
                </td>
                <td style="width: 16%">
                    <telerik:RadComboBox ID="cmbPais" runat="server" AutoPostBack="true" 
                        DataTextField="Descripcion" DataValueField="Codigo" 
                        OnSelectedIndexChanged="cmbPais_SelectedIndexChanged">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" forecolor="Black" text="Cliente:"></asp:Label>
                </td>
                <td colspan="5">
                    <telerik:RadComboBox ID="cmbCliente" runat="server" DataTextField="Nombre" 
                        DataValueField="CodigoSAP" Filter="StartsWith" Width="100%">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="6">
                    <asp:ImageButton ID="btnBuscar0" runat="server" 
                        imageurl="~/Images/24/view_24.png" onclick="btnBuscar_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="rgReporte" runat="server" 
            AllowSorting="True" AutoGenerateColumns="False" BorderStyle="Solid" 
            CellSpacing="0" GridLines="None" PageSize="20" 
            style="margin-right: 0px" Width="3300px" >
            <ClientSettings AllowColumnsReorder="True">
                <Selecting AllowRowSelect="True" />
            </ClientSettings>
            <ExportSettings FileName="Reporte" HideStructureColumns="true" 
                OpenInNewWindow="True">
                <Excel AutoFitImages="True" Format="Biff" />
            </ExportSettings>
            <MasterTableView CommandItemDisplay="Top" >
                <CommandItemSettings ExportToPdfText="Export to PDF" 
                    ShowAddNewRecordButton="false" ShowExportToExcelButton="false" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                    Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                    Visible="True">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="Aduana" 
                        FilterControlAltText="Filter Aduana column" HeaderText="Aduana" 
                        UniqueName="Aduana">
                        <HeaderStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TipoImportacion" 
                        FilterControlAltText="Filter TipoImportacion column" HeaderText="TipoImportacion"
                        UniqueName="TipoImportacion">
                        <HeaderStyle Width="75px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Division" 
                        FilterControlAltText="Filter Division column" HeaderText="Division" 
                        UniqueName="Division">
                        <HeaderStyle Width="75px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="HojaRuta" 
                        FilterControlAltText="Filter HojaRuta column" HeaderText="HojaRuta" 
                        UniqueName="HojaRuta">
                        <HeaderStyle Width="95px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ordenes" 
                        FilterControlAltText="Filter Ordenes column" HeaderText="Ordenes" 
                        UniqueName="Ordenes">
                        <HeaderStyle Width="80px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Proveedor" 
                        FilterControlAltText="Filter Proveedor column" HeaderText="Proveedor" 
                        UniqueName="Proveedor">
                        <HeaderStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Producto" 
                        FilterControlAltText="Filter Producto column" HeaderText="Producto" 
                        UniqueName="Producto">
                        <HeaderStyle Width="200px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ETA" 
                        FilterControlAltText="Filter ETA column" HeaderText="ETA" 
                        UniqueName="ETA">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ArriboCarga" 
                        FilterControlAltText="Filter ArriboCarga column" HeaderText="ArriboCarga" 
                        UniqueName="ArriboCarga">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ETAPlanta" 
                        FilterControlAltText="Filter ETAPlanta column" HeaderText="ETAPlanta" 
                        UniqueName="ETAPlanta">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NumeroFactura" 
                        FilterControlAltText="Filter NumeroFactura column" HeaderText="NumeroFactura" 
                        UniqueName="NumeroFactura">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PaisOrigen" 
                        FilterControlAltText="Filter PaisOrigen column" HeaderText="Pais de Origen" 
                        UniqueName="PaisOrigen">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Naviera/Courier" 
                        FilterControlAltText="Filter Naviera/Courier column" 
                        HeaderText="Naviera/Courier" UniqueName="Naviera/Courier">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="#Contenedor" 
                        FilterControlAltText="Filter #Contenedor column" HeaderText="# Contenedor" 
                        UniqueName="#Contenedor">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="BL/Guia/CartaPorte" 
                        FilterControlAltText="Filter BL/Guia/CartaPorte column" 
                        HeaderText="BL/Guia/CartaPorte" UniqueName="BL/Guia/CartaPorte">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CantidadBultos" 
                        FilterControlAltText="Filter CantidadBultos column" 
                        HeaderText="Cantidad Bultos" UniqueName="CantidadBultos">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PesoKilogramos" 
                        FilterControlAltText="Filter PesoKilogramos column" 
                        HeaderText="Peso Kilogramos" UniqueName="PesoKilogramos">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ReciboCopiasEmail" 
                        FilterControlAltText="Filter ReciboCopiasEmail column" 
                        HeaderText="Recibo Copias Email" UniqueName="ReciboCopiasEmail">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ReciboOriginales" 
                        FilterControlAltText="Filter ReciboOriginales column" 
                        HeaderText="Recibo Originales" UniqueName="ReciboOriginales">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EstadoActual" 
                        FilterControlAltText="Filter EstadoActual column" HeaderText="Flujo Actual" 
                        UniqueName="EstadoActual">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Comentario" 
                        FilterControlAltText="Filter Comentario column" HeaderText="Observacion" 
                        UniqueName="Comentario">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Comentarios2" 
                        FilterControlAltText="Filter Comentarios column" HeaderText="Comentarios" 
                        UniqueName="Comentarios2">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Color" 
                        FilterControlAltText="Filter Color column" HeaderText="Color" 
                        UniqueName="Color">
                    </telerik:GridBoundColumn>
                </Columns>
                <%--<Columns>
                    <telerik:GridBoundColumn DataField="Aduana" 
                        FilterControlAltText="Filter Aduana column" HeaderText="Aduana" 
                        UniqueName="Aduana">
                        <HeaderStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TipoImportacion" 
                        FilterControlAltText="Filter TipoImportacion column" HeaderText="TipoImportacion"
                        UniqueName="TipoImportacion">
                        <HeaderStyle Width="75px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Division" 
                        FilterControlAltText="Filter Division column" HeaderText="Division" 
                        UniqueName="Division">
                        <HeaderStyle Width="75px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="HojaRuta" 
                        FilterControlAltText="Filter HojaRuta column" HeaderText="HojaRuta" 
                        UniqueName="HojaRuta">
                        <HeaderStyle Width="95px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Ordenes" 
                        FilterControlAltText="Filter Ordenes column" HeaderText="Ordenes" 
                        UniqueName="Ordenes">
                        <HeaderStyle Width="80px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Proveedor" 
                        FilterControlAltText="Filter Proveedor column" HeaderText="Proveedor" 
                        UniqueName="Proveedor">
                        <HeaderStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Producto" 
                        FilterControlAltText="Filter Producto column" HeaderText="Producto" 
                        UniqueName="Producto">
                        <HeaderStyle Width="200px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ETA" 
                        FilterControlAltText="Filter ETA column" HeaderText="ETA" 
                        UniqueName="ETA">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ArriboCarga" 
                        FilterControlAltText="Filter ArriboCarga column" HeaderText="ArriboCarga" 
                        UniqueName="ArriboCarga">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ETAPlanta" 
                        FilterControlAltText="Filter ETAPlanta column" HeaderText="ETAPlanta" 
                        UniqueName="ETAPlanta">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NumeroFactura" 
                        FilterControlAltText="Filter NumeroFactura column" HeaderText="NumeroFactura" 
                        UniqueName="NumeroFactura">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PaisOrigen" 
                        FilterControlAltText="Filter PaisOrigen column" HeaderText="Pais de Origen" 
                        UniqueName="PaisOrigen">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Naviera/Courier" 
                        FilterControlAltText="Filter Naviera/Courier column" 
                        HeaderText="Naviera/Courier" UniqueName="Naviera/Courier">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="#Contenedor" 
                        FilterControlAltText="Filter #Contenedor column" HeaderText="# Contenedor" 
                        UniqueName="#Contenedor">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="BL/Guia/CartaPorte" 
                        FilterControlAltText="Filter BL/Guia/CartaPorte column" 
                        HeaderText="BL/Guia/CartaPorte" UniqueName="BL/Guia/CartaPorte">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CantidadBultos" 
                        FilterControlAltText="Filter CantidadBultos column" 
                        HeaderText="Cantidad Bultos" UniqueName="CantidadBultos">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PesoKilogramos" 
                        FilterControlAltText="Filter PesoKilogramos column" 
                        HeaderText="Peso Kilogramos" UniqueName="PesoKilogramos">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ReciboCopiasEmail" 
                        FilterControlAltText="Filter ReciboCopiasEmail column" 
                        HeaderText="Recibo Copias Email" UniqueName="ReciboCopiasEmail">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ReciboOriginales" 
                        FilterControlAltText="Filter ReciboOriginales column" 
                        HeaderText="Recibo Originales" UniqueName="ReciboOriginales">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EstadoActual" 
                        FilterControlAltText="Filter EstadoActual column" HeaderText="Estado Actual" 
                        UniqueName="EstadoActual">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Comentario" 
                        FilterControlAltText="Filter Comentario column" HeaderText="Observacion" 
                        UniqueName="Comentario">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Comentarios" 
                        FilterControlAltText="Filter Comentarios column" HeaderText="Comentarios" 
                        UniqueName="Comentarios">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Color" 
                        FilterControlAltText="Filter Color column" HeaderText="Color" 
                        UniqueName="Color">
                    </telerik:GridBoundColumn>
                </Columns>--%>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <ItemStyle BorderColor="Black" BorderStyle="Solid" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        <table class="style12">
            <tr>
                <td style="text-align: center">
                     <asp:ImageButton ID="btnGuardar" runat="server" 
                         ImageUrl="~/Images/24/disk_blue_ok_24.png" 
                         OnClientClick="radconfirm('Esta seguro que Desea Generar el Reporte?',confirmCallBackSalvar, 300, 100); return false;" 
                         ToolTip="Salvar" Visible="False" />
                     <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="~/Images/gris.png" 
                         OnClick="btnSalvar_Click" />
                     <asp:Button ID="btnCargar" runat="server" Text="Cargar Archivo" 
                         Visible="false" />
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
    <%--<GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>--%>
</asp:Content>
