﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MantenimientoSolicitudes.aspx.cs"
    Inherits="MantenimientoUsuarios" MasterPageFile="~/Site.master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style2 {
            width: 101px;
        }

        .auto-style3 {
            width: 489px;
        }

        .auto-style4 {
            width: 158px;
        }

        .auto-style7 {
            width: 45%;
        }

        .auto-style8 {
            width: 629px;
        }

        .auto-style9 {
            top: 5px;
            width: 629px;
        }

        .auto-style10 {
            width: 128px;
        }
        .auto-style11 {
            width: 24px;
        }
        .auto-style12 {
            width: 498px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <br />
    <telerik:RadTabStrip ID="RadTabStrip1" runat="server" SelectedIndex="0" MultiPageID="RadMultiPage1">
        <Tabs>
            <telerik:RadTab runat="server" Text="Gastos" PageViewID="pvGastos" Selected="True" />
            <telerik:RadTab runat="server" Text="Proveedores" PageViewID="pvProveedores" />
            <telerik:RadTab runat="server" PageViewID="pvMapeo" Text="Mapeo">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" Width="99%">
        <telerik:RadPageView ID="pvGastos" runat="server" Width="100%">
            <telerik:RadGrid ID="rgGastos" runat="server" AllowFilteringByColumn="True" AllowPaging="True"
                AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" GridLines="Horizontal"
                Height="550px" OnDeleteCommand="rgUsuarios_DeleteCommand" OnInsertCommand="rgUsuarios_InsertCommand"
                OnItemCommand="rgUsuarios_ItemCommand" OnItemCreated="rgUsuarios_ItemCreated"
                OnItemDataBound="rgUsuarios_ItemDataBound" OnNeedDataSource="rgUsuarios_NeedDataSource"
                OnUpdateCommand="rgUsuarios_UpdateCommand" PageSize="20" ShowFooter="True" ShowStatusBar="True"
                Width="100%" Culture="es-ES" Style="text-align: left">
                <HeaderContextMenu EnableTheming="True">
                    <CollapseAnimation Duration="200" Type="OutQuint" />
                </HeaderContextMenu>
                <PagerStyle Mode="NextPrevAndNumeric" NextPagesToolTip="Páginas Siguientes" NextPageToolTip="Página Siguiente"
                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                    PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                <MasterTableView CommandItemDisplay="<%# Ingresar ? GridCommandItemDisplay.Top : GridCommandItemDisplay.None %>"
                    DataKeyNames="CodPais,IdGasto,Gasto" NoDetailRecordsText="No hay registros."
                    NoMasterRecordsText="No hay Gastos definidos.">
                    <CommandItemSettings AddNewRecordText="Agregar Gasto" RefreshText="Volver a Cargar Datos" />
                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px" />
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px" />
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridEditCommandColumn AutoPostBackOnFilter="True"
                            CancelText="Cancelar" EditText="Editar" InsertText="Insertar" UpdateText="Grabar"
                            Visible="true">
                            <HeaderStyle Width="30px" />
                            <ItemStyle Width="30px" />
                        </telerik:GridEditCommandColumn>
                        <telerik:GridButtonColumn CommandName="Delete" ConfirmDialogType="RadWindow"
                            ConfirmText="¿Esta seguro de que desea eliminar este Gasto?" ConfirmTitle="Eliminar Gasto"
                            UniqueName="DeleteColumn" Visible="true" ImageUrl="mvwres://Telerik.Web.UI, Version=2012.3.1016.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.Cancel.gif" Text="Eliminar">
                            <HeaderStyle Width="40px" />
                            <ItemStyle Width="40px" />
                        </telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="Gasto" FilterControlAltText="Filter Gasto column"
                            FilterControlWidth="80%" HeaderText="Gasto" UniqueName="Gasto">
                            <HeaderStyle Width="190px" />
                            <ItemStyle Width="190px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Material" FilterControlAltText="Filter Material column"
                            FilterControlWidth="60%" HeaderText="Material" UniqueName="Material">
                            <HeaderStyle Width="85px" />
                            <ItemStyle Width="85px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Balance" FilterControlAltText="Filter Balance column"
                            FilterControlWidth="60%" HeaderText="Balance" UniqueName="Balance">
                            <HeaderStyle Width="100px" />
                            <ItemStyle Width="100px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Pais" FilterControlAltText="Filter Pais column"
                            FilterControlWidth="60%" HeaderText="Pais" UniqueName="Pais">
                            <HeaderStyle Width="110px" />
                            <ItemStyle Width="110px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Estado" FilterControlAltText="Filter Estado column"
                            FilterControlWidth="60%" HeaderText="Estado" UniqueName="Estado">
                            <HeaderStyle Width="115px" />
                            <ItemStyle Width="115px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Impuesto" FilterControlAltText="Filter Impuesto column"
                            FilterControlWidth="60%" HeaderText="Impuesto" UniqueName="Impuesto">
                            <HeaderStyle Width="90px" />
                            <ItemStyle Width="90px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CodPais" FilterControlAltText="Filter CodPais column"
                            HeaderText="CodPais" UniqueName="CodPais" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CodImpuesto" FilterControlAltText="Filter CodImpuesto column"
                            HeaderText="CodImpuesto" UniqueName="CodImpuesto" Visible="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="IdGasto" FilterControlAltText="Filter IdGasto column"
                            HeaderText="IdGasto" UniqueName="IdGasto">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings CaptionDataField="Categoria" CaptionFormatString="" EditFormType="Template">
                        <EditColumn FilterControlAltText="Filter EditCommandColumn1 column"
                            UniqueName="EditCommandColumn1">
                        </EditColumn>
                        <FormTemplate>
                            <asp:Label ID="Label1" runat="server" Font-Bold="true" Font-Size="Medium"
                                Text='<%# ((Container is GridEditFormInsertItem) ? "Agregar Nuevo Gasto" : "Editar Gasto Existente") %>' />
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 10%"></td>
                                    <td style="width: 90%"></td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="lbGasto" runat="server" Font-Bold="True" Text="Gasto" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadTextBox ID="edGasto" runat="server"
                                            EmptyMessage="Escriba el Usuario aquí" MaxLength="50" Skin="<%# Skin %>"
                                            Text='<%# Bind("Gasto") %>' Width="50%" />
                                        <asp:RequiredFieldValidator ID="rfUsuario" runat="server"
                                            ControlToValidate="edGasto" Display="Dynamic" EnableClientScript="true"
                                            ErrorMessage="El Gasto no puede estar vacío" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="lblMaterial" runat="server" Font-Bold="True" Text="Material" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadNumericTextBox ID="edMaterial" runat="server"
                                            EmptyMessage="Material" MaxLength="4" NumberFormat-DecimalDigits="0"
                                            NumberFormat-GroupSeparator="" Skin="<%# Skin %>"
                                            Text='<%# Bind("Material") %>' Width="50%" />
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="El Celular no puede estar vacío" ControlToValidate="edCelular" />--%>
                                        <asp:RequiredFieldValidator ID="rfUsuario0" runat="server"
                                            ControlToValidate="edMaterial" Display="Dynamic" EnableClientScript="true"
                                            ErrorMessage="El Material no puede estar vacío" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="lblBalance" runat="server" Font-Bold="True" Text="Balance" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadNumericTextBox ID="edBalance" runat="server" EmptyMessage="Balance"
                                            MaxLength="8" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator=""
                                            Skin="<%# Skin %>" Text='<%# Bind("Balance") %>' Width="50%" />
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="El Teléfono no puede estar vacío" ControlToValidate="edTelefono" />--%>
                                        <asp:RequiredFieldValidator ID="rfUsuario1" runat="server"
                                            ControlToValidate="edGasto" Display="Dynamic" EnableClientScript="true"
                                            ErrorMessage="La Cuenta de Balance no puede estar vacío" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="lblPais" runat="server" Font-Bold="true" Text="País" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadComboBox ID="edPais" runat="server" AutoPostBack="false"
                                            CausesValidation="false" DataSource="<%# GetPaises() %>"
                                            DataTextField="Descripcion" DataValueField="Codigo"
                                            EmptyMessage="Seleccione el País" Filter="Contains"
                                            SelectedValue='<%# Bind("CodPais") %>' Skin="<%# Skin %>" Width="50%" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="lblImpuesto" runat="server" Font-Bold="True" Text="Impuesto" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadComboBox ID="edImpuesto" runat="server" CausesValidation="False"
                                            EmptyMessage="Impuesto" SelectedValue='<%# Bind("CodImpuesto")%>'
                                            Skin="<%# Skin %>" Width="50%">
                                            <Items>
                                                <telerik:RadComboBoxItem runat="server" Owner="edImpuesto" Text="Si"
                                                    Value="1" />
                                                <telerik:RadComboBoxItem runat="server" Owner="edImpuesto" Text="No"
                                                    Value="0" />
                                            </Items>
                                        </telerik:RadComboBox>
                                    </td>
                                </tr>
                            </table>
                            <asp:ImageButton ID="btnSave" runat="server"
                                AlternateText='<%# (Container is GridEditFormInsertItem) ? "Agregar Gasto" : "Actualizar Gasto" %>'
                                CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                ImageUrl="Images/16/check2_16.png" />
                            &nbsp;
                            <asp:ImageButton ID="btnCancel" runat="server" AlternateText="Cancelar"
                                CausesValidation="false" CommandName="Cancel"
                                ImageUrl="Images/16/delete2_16.png" />
                        </FormTemplate>
                    </EditFormSettings>
                </MasterTableView>
                <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                    <Selecting AllowRowSelect="True" />
                    <ClientMessages DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                        DropHereToReorder="Suelte aquí para Re-Ordenar" PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas" />
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                </ClientSettings>
                <FilterMenu EnableTheming="True">
                    <CollapseAnimation Duration="200" Type="OutQuint" />
                </FilterMenu>
                <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
            </telerik:RadGrid>
        </telerik:RadPageView>
        <telerik:RadPageView ID="pvProveedores" runat="server" Width="100%">
            <telerik:RadGrid ID="rgProveedores" runat="server" AllowFilteringByColumn="True"
                AllowSorting="True" AutoGenerateColumns="False" GridLines="Horizontal" Width="100%"
                Height="550px" OnNeedDataSource="rgRoles_NeedDataSource" OnUpdateCommand="rgRoles_UpdateCommand"
                OnDeleteCommand="rgRoles_DeleteCommand" OnInsertCommand="rgRoles_InsertCommand"
                AllowPaging="True" ShowFooter="True" ShowStatusBar="True" PageSize="20" OnItemDataBound="rgRoles_ItemDataBound"
                CellSpacing="0" Culture="es-ES" Style="text-align: left">
                <HeaderContextMenu EnableTheming="True">
                    <CollapseAnimation Duration="200" Type="OutQuint" />
                </HeaderContextMenu>
                <GroupPanel Text="Arrastre el encabezado de una columna aquí para agrupar por esa columna">
                </GroupPanel><PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                <MasterTableView DataKeyNames="CodPais,IdProveedor" CommandItemDisplay="<%# Ingresar ? GridCommandItemDisplay.Top : GridCommandItemDisplay.None %>"
                    NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay Usuarios definidos."
                    GroupLoadMode="Client">
                    <CommandItemSettings AddNewRecordText="Agregar Nuevo Proveedor" RefreshText="Volver a Cargar Datos" />
                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridEditCommandColumn AutoPostBackOnFilter="True" CancelText="Cancelar"
                            EditText="Editar" InsertText="Insertar" UpdateText="Grabar">
                            <HeaderStyle Width="30px" />
                            <ItemStyle Width="30px" />
                        </telerik:GridEditCommandColumn>
                        <telerik:GridButtonColumn ConfirmText="¿Esta seguro de que desea eliminar este Proveedor?"
                            ConfirmDialogType="RadWindow" ConfirmTitle="Eliminar Proveedor" UniqueName="DeleteColumn" CommandName="Delete" Text="Eliminar">
                            <HeaderStyle Width="30px" />
                            <ItemStyle Width="30px" />
                        </telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="Nombre" HeaderText="Nombre" UniqueName="Nombre"
                            FilterControlAltText="Filter Nombre column" FilterControlWidth="80%">
                            <HeaderStyle Width="260px" />
                            <ItemStyle Width="260px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Cuenta" HeaderText="Cuenta" UniqueName="Cuenta"
                            FilterControlAltText="Filter Cuenta column" FilterControlWidth="60%">
                            <HeaderStyle Width="90px" />
                            <ItemStyle Width="90px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Pais" HeaderText="Pais" UniqueName="Pais" FilterControlAltText="Filter Pais column"
                            FilterControlWidth="70%">
                            <HeaderStyle Width="110px" />
                            <ItemStyle Width="110px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Estado" FilterControlAltText="Filter Estado column"
                            HeaderText="Estado" UniqueName="Estado">
                            <HeaderStyle Width="120px" />
                            <ItemStyle Width="120px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="IdProveedor" FilterControlAltText="Filter IdProveedor column"
                            HeaderText="IdProveedor" UniqueName="IdProveedor" Visible="False">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <GroupByExpressions>
                    </GroupByExpressions>
                    <EditFormSettings EditFormType="Template" CaptionDataField="ModuloDescripcion" CaptionFormatString="">
                        <EditColumn UniqueName="EditCommandColumn1">
                        </EditColumn>
                        <FormTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# ((Container is GridEditFormInsertItem) ? "Agregar Proveedor " : "Editar Proveedor") %>'
                                Font-Size="Medium" Font-Bold="true" />
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 10%"></td>
                                    <td style="width: 90%"></td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="Label2" runat="server" Text="Proveedor" Font-Bold="True" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadTextBox ID="edProveedor" runat="server" EmptyMessage="Proveedor" LabelWidth="64px"
                                            Width="160px" Text='<%# Bind("Nombre") %>'>
                                        </telerik:RadTextBox>
                                        <asp:RequiredFieldValidator ID="rfUsuario" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="El Proveedor no puede estar vacío" ControlToValidate="edProveedor" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="Label3" runat="server" Text="Cuenta" Font-Bold="True" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadNumericTextBox ID="edCuenta" runat="server" Culture="es-HN" DbValueFactor="1"
                                            LabelWidth="64px" Text='<%# Bind("Cuenta") %>' MaxLength="12" Width="160px">
                                            <NumberFormat ZeroPattern="n" />
                                        </telerik:RadNumericTextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="La Cuenta no puede estar vacío" ControlToValidate="edCuenta" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="Label4" runat="server" Font-Bold="True" Text="Pais" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadComboBox ID="cmbPais" runat="server" SelectedValue='<%# Bind("CodPais") %>'>
                                            <Items>
                                                <telerik:RadComboBoxItem runat="server" Text="El Salvador" Value="S" />
                                                <telerik:RadComboBoxItem runat="server" Text="Nicaragua" Value="N" />
                                                <telerik:RadComboBoxItem runat="server" Text="Guatemala" Value="G" />
                                            </Items>
                                        </telerik:RadComboBox>
                                    </td>
                                </tr>
                            </table>
                            <asp:ImageButton ID="btnSave" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                AlternateText='<%# (Container is GridEditFormInsertItem) ? "Agregar Usuario al Perfil" : "Actualizar Usuario del Perfil" %>'
                                ImageUrl='Images/16/check2_16.png' />&#160;&nbsp<asp:ImageButton
                                    ID="btnCancel" runat="server" CommandName="Cancel" AlternateText="Cancelar" ImageUrl='Images/16/delete2_16.png'
                                    CausesValidation="false" />
                        </FormTemplate>
                    </EditFormSettings>
                </MasterTableView><GroupingSettings CollapseTooltip="Contraer grupo" ExpandTooltip="Expandir grupo"
                    GroupContinuedFormatString="... continuación del grupo de la página anterior. "
                    GroupContinuesFormatString="El grupo continua en la siguiente página." GroupSplitDisplayFormat="Mostrando {0} de {1} elementos."
                    UnGroupTooltip="Arrastre fuera de la barra para desagrupar" />
                <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                    <Selecting AllowRowSelect="True" />
                    <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                        DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                        DropHereToReorder="Suelte aquí para Re-Ordenar" />
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                </ClientSettings>
                <FilterMenu EnableTheming="True">
                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                </FilterMenu>
                <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
            </telerik:RadGrid>
        </telerik:RadPageView>
        <telerik:RadPageView ID="pvMapeo" runat="server">
            <table class="auto-style1">
                <tr>
                    <td class="auto-style3">&nbsp;</td>
                    <td class="auto-style2">
                        <asp:Label ID="Label5" runat="server" Text="Seleccione País:"></asp:Label>
                    </td>
                    <td class="auto-style4">
                        <telerik:RadComboBox ID="cmbPaises" runat="server" DataTextField="Descripcion" DataValueField="Codigo" OnSelectedIndexChanged="cmbPaises_SelectedIndexChanged" AutoPostBack="True">
                        </telerik:RadComboBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <table class="auto-style1">
                <tr>
                    <td>&nbsp;</td>
                    <td style="text-align: center" class="auto-style8">
                        <asp:Label ID="Label6" runat="server" Font-Bold="True" Text="Gastos de la Solicitud"></asp:Label>
                    </td>
                    <td style="text-align: center">
                        <asp:Label ID="Label7" runat="server" Font-Bold="True" Text="Gastos de Caja Chica"></asp:Label>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="auto-style9">
                        <table class="auto-style7">
                            <tr>
                                <td align="left" valign="top">
                                    <telerik:RadGrid ID="rgSolicitud" runat="server" AutoGenerateColumns="False" CellSpacing="0" Culture="es-ES" GridLines="None" Width="100%" AllowFilteringByColumn="True" OnSelectedIndexChanged="rgSolicitud_SelectedIndexChanged">
                                        <ClientSettings EnablePostBackOnRowClick="true">
                                            <Selecting AllowRowSelect="True" />
                                        </ClientSettings>
                                        <MasterTableView>
                                            <CommandItemSettings ExportToPdfText="Export to PDF" />
                                            <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                                <HeaderStyle Width="20px" />
                                            </RowIndicatorColumn>
                                            <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                                                <HeaderStyle Width="20px" />
                                            </ExpandCollapseColumn>
                                            <Columns>
                                                <telerik:GridClientSelectColumn FilterControlAltText="Filter column column" HeaderText="Selec" UniqueName="column">
                                                </telerik:GridClientSelectColumn>
                                                <telerik:GridBoundColumn DataField="IdGasto" FilterControlAltText="Filter column1 column" HeaderText="Id" UniqueName="IdGasto">
                                                    <HeaderStyle Width="50px" />
                                                    <ItemStyle Width="50px" />
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Gasto" FilterControlAltText="Filter column2 column" HeaderText="Gasto" UniqueName="Gasto">
                                                    <HeaderStyle Width="150px" />
                                                    <ItemStyle Width="150px" />
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Balance" FilterControlAltText="Filter column3 column" HeaderText="Cuenta" UniqueName="Balance">
                                                    <HeaderStyle Width="110px" />
                                                    <ItemStyle Width="110px" />
                                                </telerik:GridBoundColumn>
                                            </Columns>
                                            <EditFormSettings>
                                                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                                </EditColumn>
                                            </EditFormSettings>
                                        </MasterTableView>
                                        <FilterMenu EnableImageSprites="False">
                                        </FilterMenu>
                                    </telerik:RadGrid>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="left" valign="top">
                        <telerik:RadGrid ID="rgCajaChica" runat="server" AutoGenerateColumns="False" CellSpacing="0" Culture="es-ES" GridLines="None" AllowFilteringByColumn="True" Width="100%" OnSelectedIndexChanged="rgCajaChica_SelectedIndexChanged">
                            <ClientSettings EnablePostBackOnRowClick="true">
                                <Selecting AllowRowSelect="True" />
                            </ClientSettings>
                            <MasterTableView>
                                <CommandItemSettings ExportToPdfText="Export to PDF" />
                                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                                    <HeaderStyle Width="20px" />
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                                    <HeaderStyle Width="20px" />
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridClientSelectColumn FilterControlAltText="Filter column column" HeaderText="Selec" UniqueName="column">
                                    </telerik:GridClientSelectColumn>
                                    <telerik:GridBoundColumn DataField="cod_descripcion" FilterControlAltText="Filter column1 column" HeaderText="Id" UniqueName="Id">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="descripcion" FilterControlAltText="Filter column2 column" HeaderText="Gasto" UniqueName="Gasto">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="cuenta" FilterControlAltText="Filter column3 column" HeaderText="Cuenta" UniqueName="Cuenta">
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                            </MasterTableView>
                            <FilterMenu EnableImageSprites="False">
                            </FilterMenu>
                        </telerik:RadGrid>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <br />
            <table class="auto-style1">
                <tr>
                    <td class="auto-style12">
                        <asp:TextBox ID="txtIdGastoAduana" runat="server" Visible="False" style="margin-right: 364px" Width="120px"></asp:TextBox>
                    </td>
                    <td class="auto-style2">
                        <asp:Label ID="lblGastoAduana" runat="server" Text="Gasto Aduanas:"></asp:Label>
                    </td>
                    <td class="auto-style10">
                        <asp:TextBox ID="txtGastoAduanas" runat="server"></asp:TextBox>
                    </td>
                    <td align="left" class="auto-style11" rowspan="2" valign="middle">
                        <asp:ImageButton ID="bntGuardar" runat="server" ImageUrl="~/Images/24/disk_blue_ok_24.png" OnClick="ImageButton1_Click" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style12">
                        <asp:TextBox ID="txtIdGastoCajaChica" runat="server" Visible="False" Width="120px"></asp:TextBox>
                    </td>
                    <td class="auto-style2">
                        <asp:Label ID="lblGastoCajaChica" runat="server" Text="Gasto Caja Chica:"></asp:Label>
                    </td>
                    <td class="auto-style10">
                        <asp:TextBox ID="txtGastoCajaChica" runat="server"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgUsuarios">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgUsuarios" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rgRoles">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgRoles" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxPanel runat="server" LoadingPanelID="LoadingPanel" ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/jscript">

                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }
                function confirmCallBackSalvarMaritimo(arg) {
                }
                var message = "Grupo Vesta, Derechos Reservados";

                function click(e) {
                    if (document.all) {
                        if (event.button == 2) {
                            alert(message);
                            return false;
                        }
                    }
                    if (document.layers) {
                        if (e.which == 3) {
                            alert(message);
                            return false;
                        }
                    }
                }
                if (document.layers) {
                    document.captureEvents(Event.MOUSEDOWN);
                }
                document.onmousedown = click;

                function OnClientClose(oWnd, args) {
                    //get the transferred arguments
                    var arg = args.get_argument();
                    if (arg) {
                        var val1 = arg.val1;
                        var valC = arg.valC;


                        if (valC == "IdGasto") {
                            var txt1 = $find("<%=txtGastoAduanas.ClientID%>");
                        }

                        txt1.set_value(val1);

                        //var btn = document.getElementById('');
                        //if (btn != null)
                        //btn.click();
                    }
                }
            </script>
        </telerik:RadScriptBlock>
    </telerik:RadAjaxPanel>
</asp:Content>
