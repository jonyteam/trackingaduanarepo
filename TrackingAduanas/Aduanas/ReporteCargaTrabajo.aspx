﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" 
    CodeFile="ReporteCargaTrabajo.aspx.cs" Inherits="ReporteCargaTrabajo" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content runat="server" ID="Content1" ContentPlaceHolderID="HeadContent">

    <style type="text/css">

        .tablaprincipal {
            margin-left: 350px;
        }
        #divGridReporteGeneral {
            margin-left: 250px;
            text-align: center;
        }
        .datePicker {
            margin-left: 200px;
            margin-top: -23px;
            margin-bottom: 25px;
        }
        .mesFin {
            margin-left: 200px;
            margin-top: -23px;
            width: 157px;
        }

        .auto-style1 {
            width: 346px;
        }

        .auto-style2 {
            width: 121px;
        }

        .auto-style3 {
            margin-left: 350px;
            width: 536px;
        }

        </style>

</asp:Content>

<asp:Content runat="server" ID="Content2" ContentPlaceHolderID="MainContent">
    <telerik:RadAjaxPanel runat="server" ID="radPanel" ClientEvents-OnRequestStart="requestStart" LoadingPanelID="RadAjaxLoadingPanel1">
        <div id="tablaBusqueda">
        <div>
            <br />
            <table border="1" id="tblPrincipal" class="auto-style3">
                <tr >
                    <td rowspan="4" >
                        <asp:Label runat="server" Text=" <b>&nbsp;&nbsp;Rango de meses :</b>" ForeColor="GrayText"></asp:Label>
                    </td>
                </tr>
                <tr>
                     <td class="auto-style1">                       
                         <div style="width: 140px">
                             <telerik:RadMonthYearPicker ID="rdMesInicio" runat="server" Visible="True"></telerik:RadMonthYearPicker>
                         </div>
                         <div class="mesFin">
                             <telerik:RadMonthYearPicker ID="rdMesFin" runat="server" Visible="True" ></telerik:RadMonthYearPicker>                            
                         </div>
                    </td>
                </tr>
            </table>
        </div>

        <div>
            <table style="margin-left: 50%">
                <tr>
                    <td>
                        <div>
                            <asp:ImageButton runat="server" ID="ImageButton1" ImageUrl="~/Images/24/view_24.png" OnClick="btnGenerarReporte_OnClick"></asp:ImageButton>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        </div>

        <div id="radGridPrincipal">
        <telerik:RadGrid RenderMode="Lightweight"  ID="rgReporteCargaTrabajo" runat="server" PageSize="70" 
            OnItemCommand="rgReporteCargaTrabajo_ItemCommand"  
            AllowSorting="True"  AllowPaging="True" Skin="Office2010Silver" ShowFooter="True"
            AutoGenerateColumns="False"  OnDetailTableDataBind="rgReporteCargaTrabajo_DetailTableDataBind" 
            OnNeedDataSource="rgReporteCargaTrabajo_NeedDataSource">
            <HeaderContextMenu EnableTheming="True">
                 <CollapseAnimation Duration="200" Type="OutQuint" />
            </HeaderContextMenu>
            <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente" 
                PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}." 
                Mode="NextPrevAndNumeric" 
                PrevPagesToolTip="Paginas Anteriores" 
                PrevPageToolTip="Página Anterior" 
                PageSizeLabelText="Cambiar Número De Registros"/>
            <MasterTableView CommandItemDisplay="Top" HeaderStyle-Font-Bold="true"  GroupHeaderItemStyle-CssClass="bold" NoDetailRecordsText="No hay registros." 
                NoMasterRecordsText="No hay instrucciones." GroupLoadMode="Client" Width="100%" ShowGroupFooter="true" 
                AllowMultiColumnSorting="true" >
                <CommandItemSettings ShowAddNewRecordButton="False" RefreshText=""
                                ShowExportToExcelButton="True" />
                <RowIndicatorColumn>
                     <HeaderStyle Width="20px" ></HeaderStyle>
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                     <HeaderStyle Width="20px"></HeaderStyle>
                </ExpandCollapseColumn>
                <GroupByExpressions >                
                    <telerik:GridGroupByExpression >
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="Rol" FieldName="Rol" FormatString="{0:D}"
                                HeaderValueSeparator=" : "></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldName="Rol" SortOrder="Descending"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="Ubicacion" FieldName="Ubicacion"></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldName="Ubicacion"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>
                <Columns>
                    <telerik:GridBoundColumn SortExpression="Ubicacion" Visible ="false" HeaderText="Ubicacion" HeaderButtonType="TextButton"
                        DataField="Ubicacion">
                         <HeaderStyle Width="150px" />
                         <ItemStyle Width="150px" />
                    </telerik:GridBoundColumn>

                    <telerik:GridBoundColumn SortExpression="RolReal" Visible ="false" HeaderText="RolReal" HeaderButtonType="TextButton"
                        DataField="RolReal">
                         <HeaderStyle Width="150px" />
                         <ItemStyle Width="150px" />
                    </telerik:GridBoundColumn>

                    <telerik:GridBoundColumn SortExpression="Eliminado" Visible ="false" HeaderText="Eliminado" HeaderButtonType="TextButton"
                        DataField="Eliminado">
                         <HeaderStyle Width="150px" />
                         <ItemStyle Width="150px" />
                    </telerik:GridBoundColumn>

                    <telerik:GridBoundColumn SortExpression="IdUsuario" Visible="false" HeaderText="IdUsuario" HeaderButtonType="TextButton"
                        DataField="IdUsuario">
                         <HeaderStyle Width="150px" />
                         <ItemStyle Width="150px" />
                    </telerik:GridBoundColumn>

                     <telerik:GridBoundColumn SortExpression="Usuario" HeaderText="Usuario" HeaderButtonType="TextButton"
                        DataField="Usuario">
                         <HeaderStyle Width="150px" />
                         <ItemStyle Width="150px" />
                    </telerik:GridBoundColumn>

                    <telerik:GridBoundColumn ItemStyle-ForeColor="White" SortExpression="" HeaderText="" HeaderButtonType="TextButton"
                        DataField="CantDiaMasCargado">
                        <HeaderStyle Width="1px" />
                         <ItemStyle Width="1px" />
                    </telerik:GridBoundColumn>

                    <telerik:GridBoundColumn SortExpression="DiaMasCargado" HeaderText="Dia Mas Cargado" HeaderButtonType="TextButton"
                        DataField="DiaMasCargado">
                        <HeaderStyle Width="150px" />
                         <ItemStyle Width="150px" />
                    </telerik:GridBoundColumn>                 

                    <telerik:GridTemplateColumn DataField="CantDiaMasCargado" HeaderButtonType="TextButton"  HeaderText="Cantidad Tramites" >
                        <ItemTemplate>
                            <asp:LinkButton ForeColor="Black"  
                                Font-Underline="false" ID="CantDiaMasCargado"  runat="server" Text='<%# Eval("CantDiaMasCargado").ToString() %>'
                                CommandArgument= '<%# Eval("IdUsuario").ToString() %>'  CommandName='<%# Eval("DiaMasCargado").ToString() %>'                          
                                OnClick="MostrarDiaMasCargado_Click" >
                            </asp:LinkButton>                    
                        </ItemTemplate>   
                        <HeaderStyle Width="150px" />
                        <ItemStyle Width="150px" />                     
                    </telerik:GridTemplateColumn>


                    <telerik:GridNumericColumn SortExpression="Lunes" DataType="System.decimal" HeaderText="Lunes" DataField="Lunes" 
                        UniqueName="Lunes" DataFormatString="{0:F2}" Visible="true" Aggregate="Sum" footerText="Promedio :">
                    </telerik:GridNumericColumn>

                    <telerik:GridNumericColumn SortExpression="Martes" DataType="System.decimal" HeaderText="Martes" DataField="Martes" 
                        UniqueName="Martes" DataFormatString="{0:F2}" Visible="true" Aggregate="Sum" footerText="Promedio :">
                    </telerik:GridNumericColumn>

                    <telerik:GridNumericColumn SortExpression="Miercoles" DataType="System.decimal" HeaderText="Miercoles" DataField="Miercoles" 
                        UniqueName="Miercoles" DataFormatString="{0:F2}" Visible="true" Aggregate="Sum" footerText="Promedio :">
                    </telerik:GridNumericColumn>

                    <telerik:GridNumericColumn SortExpression="Jueves" DataType="System.decimal" HeaderText="Jueves" DataField="Jueves" 
                        UniqueName="Jueves" DataFormatString="{0:F2}" Visible="true" Aggregate="Sum" footerText="Promedio :">
                    </telerik:GridNumericColumn>

                    <telerik:GridNumericColumn SortExpression="Viernes" DataType="System.decimal" HeaderText="Viernes" DataField="Viernes" 
                        UniqueName="Viernes" DataFormatString="{0:F2}" Visible="true" Aggregate="Sum" footerText="Promedio :">
                    </telerik:GridNumericColumn>

                    <telerik:GridNumericColumn SortExpression="Sabado" DataType="System.decimal" HeaderText="Sabado" DataField="Sabado" 
                        UniqueName="Sabado" DataFormatString="{0:F2}" Visible="true" Aggregate="Sum" footerText="Promedio :">
                    </telerik:GridNumericColumn>

                    <telerik:GridNumericColumn SortExpression="Domingo" DataType="System.decimal" HeaderText="Domingo" DataField="Domingo" 
                        UniqueName="Domingo" DataFormatString="{0:F2}" Visible="true" Aggregate="Sum" footerText="Promedio :">
                    </telerik:GridNumericColumn>

                    <telerik:GridNumericColumn SortExpression="Total" DataType="System.Int32" HeaderText="Total" DataField="Total" 
                        UniqueName="Total" DataFormatString="{0:D}" Visible="true" Aggregate="Sum" footerText="Promedio :">
                    </telerik:GridNumericColumn>

                </Columns>               
                <DetailTables>                   
                    <telerik:GridTableView ShowFooter="true" ShowGroupFooter="true" PageSize="100" CommandItemDisplay="Top" >
                        <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                        PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}." 
                        Mode="NextPrevAndNumeric" 
                        PrevPagesToolTip="Paginas Anteriores" 
                        PrevPageToolTip="Página Anterior" 
                        PageSizeLabelText="Cambiar Número De Registros"  />
                        <GroupByExpressions>
                            <telerik:GridGroupByExpression>
                                <SelectFields>
                                    <telerik:GridGroupByField FieldAlias="Cliente" FieldName="Cliente" FormatString="{0:D}"
                                        HeaderValueSeparator=" : "></telerik:GridGroupByField>
                                </SelectFields>
                                <GroupByFields>
                                    <telerik:GridGroupByField FieldName="Cliente" SortOrder="Descending"></telerik:GridGroupByField>
                                </GroupByFields>
                            </telerik:GridGroupByExpression>
                        </GroupByExpressions>
                        <Columns>
                            <telerik:GridBoundColumn Aggregate="Count" DataField="Instruccion" HeaderText="Instruccion"
                               FooterText="Total Instrucciones : ">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Producto" HeaderText="Producto" UniqueName="Producto">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Regimen" HeaderText="Regimen" UniqueName="Regimen">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CanalSelectividad" HeaderText="Canal De Selectividad" UniqueName="CanalSelectividad">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Fecha" HeaderText="Fecha" UniqueName="Fecha">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Dia" HeaderText="Dia" UniqueName="Dia">
                            </telerik:GridBoundColumn>
                        </Columns>
                        <CommandItemSettings ShowAddNewRecordButton="False" RefreshText="" ShowExportToExcelButton="True" />
                    </telerik:GridTableView>     
                               
                </DetailTables>
            </MasterTableView>


            <ClientSettings ReorderColumnsOnClient="True" AllowColumnsReorder="True">
                <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                                DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                                DropHereToReorder="Suelte aquí para Re-Ordenar" />
                <Resizing AllowRowResize="True" AllowColumnResize="True" EnableRealTimeResize="True"
                    ResizeGridOnColumnResize="False"></Resizing>
                <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" ></Scrolling>
                <Selecting ></Selecting>

            </ClientSettings>

            <FilterMenu EnableTheming="True">
                 <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
             </FilterMenu>

            <GroupingSettings ShowUnGroupButton="true"></GroupingSettings>
            <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
        </telerik:RadGrid>   
        </div>

        <div>
            <asp:Panel runat="server" ID="Panel1">
                <telerik:RadWindowManager runat="server" ID="DetailWindow" Behaviors="Close"></telerik:RadWindowManager>
            </asp:Panel>
        </div>
        </telerik:RadAjaxPanel>

        <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" >
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="rgReporteCargaTrabajo">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="DetailWindow" />
                        <telerik:AjaxUpdatedControl ControlID="rgReporteCargaTrabajo">
                        </telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="ImageButton1">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="rgReporteCargaTrabajo" LoadingPanelID="RadAjaxLoadingPanel1" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>

        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            function OnClientClose()
            {
                $find("<%= RadAjaxManager1.ClientID%>").ajaxRequest("Rebind");
            }
            function requestStart(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0
                || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
            }
        </script>
        </telerik:RadScriptBlock>     
</asp:Content>

