﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web.UI;
using GrupoLis.Login;
using System.Collections.Generic;

public partial class FcCrearInstruccionFaucaCliente : Utilidades.PaginaBase
{
    readonly AduanasDataContext _aduanas = new AduanasDataContext();

    private GrupoLis.Login.Login _logAppAduanas;


    private void SumaCostos()
    {
        try
        {
            txtValorCIF.Value = txtValorFOB.Value + txtFlete.Value + txtSeguro.Value + txtOtros.Value;
        }
        catch { }
    }

    protected void txtValorFOB_TextChanged(object sender, EventArgs e)
    {
        if (txtValorFOB.Text == "")
            txtValorFOB.Value = 0;
        SumaCostos();
    }

    protected void txtFlete_TextChanged(object sender, EventArgs e)
    {
        if (txtFlete.Text == "")
            txtFlete.Value = 0;
        SumaCostos();
    }

    protected void txtSeguro_TextChanged(object sender, EventArgs e)
    {
        if (txtSeguro.Text == "")
            txtSeguro.Value = 0;
        SumaCostos();
    }

    protected void txtOtros_TextChanged(object sender, EventArgs e)
    {
        if (txtOtros.Text == "")
            txtOtros.Value = 0;
        SumaCostos();
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtValorCIF.Value > 0)
            {
                #region Crear Instrucción

                string mensaje = "", mensaje2 = "";
                var idInstruccion = "";
                var fecha = DateTime.Now;
                var idTramite = "TR" + fecha.Year.ToString().Substring(2) + fecha.Month.ToString("00") +
                            fecha.Day.ToString("00") + "." + fecha.Hour.ToString("00") + fecha.Minute.ToString("00") +
                            fecha.Second.ToString("00") + "." + fecha.Millisecond.ToString("000");

                var gestor = _aduanas.UsuariosRoles.Where(w => w.Eliminado == '0' && w.IdRol == 5)  //Gestores
                    .Join(_aduanas.Usuarios.Where(u => u.Eliminado == '0' && u.CodigoAduana == cmbAduana.SelectedValue),
                    w => w.IdUsuario, u => u.IdUsuario, (c, u) => new { c, u })
                    .Select(cu => new { cu.u.IdUsuario }).FirstOrDefault();

                var maxInstruccion = _aduanas.Instrucciones.Where(w => w.CodigoAduana == cmbAduana.SelectedValue)
                    .OrderByDescending(w => w.Item).Select(w => w.IdInstruccion).FirstOrDefault();
                if (maxInstruccion == null)
                    idInstruccion = cmbPais.SelectedValue + "-" + cmbAduana.SelectedValue + "-" + "1"; //H-015-25876
                else
                {
                    if (cmbEspecieFiscal.SelectedValue == "RT")
                        idInstruccion = cmbPais.SelectedValue + "r" + cmbAduana.SelectedValue + "-" + (int.Parse(maxInstruccion.Trim().Substring(6, maxInstruccion.Trim().Length - 6)) + 1);
                    else
                        idInstruccion = cmbPais.SelectedValue + "-" + cmbAduana.SelectedValue + "-" + (int.Parse(maxInstruccion.Trim().Substring(6, maxInstruccion.Trim().Length - 6)) + 1);
                }

                var instruccion = new Instrucciones
                {
                    IdTramite = idTramite,
                    IdInstruccion = idInstruccion,
                    CodPaisHojaRuta = cmbPais.SelectedValue,
                    CodigoAduana = cmbAduana.SelectedValue,
                    FechaRecepcion = dtpFechaRecepcion.SelectedDate,
                    IdCliente = cmbCliente.SelectedValue,
                    Proveedor = cmbProveedor.Text.Trim(),
                    ValorFOB = Convert.ToDecimal(txtValorFOB.Value),
                    Flete = Convert.ToDecimal(txtFlete.Value),
                    Seguro = Convert.ToDecimal(txtSeguro.Value),
                    Otros = Convert.ToDecimal(txtOtros.Value),
                    ValorCIF = Convert.ToDecimal(txtValorCIF.Value),
                    CodRegimen = cmbRegimen.SelectedValue,
                    Fecha = DateTime.Now,
                    CodEstado = "0",
                    IdEstadoFlujo = "0",
                    IdUsuario = Convert.ToDecimal(Session["IdUsuario"].ToString()),
                    NumeroFactura = txtNumeroFactura.Text.Trim(),
                    ArriboCarga = '0',
                };
                var collectionProductos = cmbProducto.CheckedItems;
                if (collectionProductos.Count > 0)
                {
                    var productos = collectionProductos.Aggregate("", (current, item) => current + (item.Text + ", "));
                    instruccion.Producto = productos.Substring(0, productos.Length - 2);
                }
                if (gestor != null) instruccion.IdUsuarioAduana = gestor.IdUsuario;
                _aduanas.Instrucciones.InsertOnSubmit(instruccion);

                if (cmbCliente.Text.Contains("CARGILL"))
                {
                    var icc = new InstruccionesCargillCarga
                    {
                        IdInstruccion = instruccion.IdInstruccion,
                        TipoCargaCargill = "3",
                        Estado = false,
                        IdUsuario = Convert.ToDecimal(instruccion.IdUsuario),
                        FechaCreacion = DateTime.Now,
                    };
                    _aduanas.InstruccionesCargillCarga.InsertOnSubmit(icc);
                }

                try
                {
                    conectar();
                    //Generacion de Gastos Masivos
                    //Gastos_Masivos(cmbCliente.SelectedValue, idInstruccion, cmbCliente.Text);
                    //if (cmbCliente.Text.Contains("CARGILL"))
                    //{
                    //    CargaCargill(ins.IDINSTRUCCION);
                    //}


                    var clbo = new ClientesBO(logApp);
                    clbo.loadClienteMensaje(cmbCliente.SelectedValue);
                    if (clbo.totalRegistros > 0)
                    {
                        mensaje = "X";
                        mensaje2 = clbo.MENSAJE;
                    }
                }
                catch (Exception ex)
                {
                    logError(ex.Message, Session["IdUsuario"].ToString(), "FcCrearInstruccionFaucaCliente");
                }
                finally
                {
                    desconectar();
                }


                try
                {
                    ConectarAduanas();
                    var hr = new HojaRutaBO(_logAppAduanas);
                    var cr = new ComplementoRecepcionBO(_logAppAduanas);
                    var ee = new EncabezadoEsquemaBO(_logAppAduanas);
                    var hrfp = new HojaRutaFlujoPortuarioBO(_logAppAduanas);
                    var hret = new HojaRutaEsquemaTramitesBO(_logAppAduanas);
                    var eed = new EsquemaEnvioDoctosBO(_logAppAduanas);
                    hr.loadHojaRuta("-1");
                    hr.newLine();
                    hr.IDHOJARUTA = idInstruccion;
                    hr.FECHARECEPCION = dtpFechaRecepcion.SelectedDate.ToString();
                    hr.CODIGOADUANA = cmbAduana.SelectedValue;
                    hr.CODIGOCLIENTE = cmbCliente.SelectedValue;
                    //hr.REFERENCIACLIENTE = txtReferenciaCliente.Text;
                    //hr.PROVEEDOR = txtProveedores.Text;
                    hr.PROVEEDOR = cmbProveedor.Text.Trim();
                    //hr.DESCRIPCIONPRODUCTO = txtProducto.Text;
                    hr.DESCRIPCIONPRODUCTO = cmbProducto.Text.Trim();
                    //u.loadUsuario(cmbUsuarioAduana.SelectedValue);
                    //hr.IDRECEPTOR = u.IDUSUARIOADUANAS;
                    hr.IDRECEPTOR = instruccion.IdUsuarioAduana.ToString();
                    if (cmbAduana.SelectedValue == "015" || cmbAduana.SelectedValue == "018" ||
                        cmbAduana.SelectedValue == "019" || cmbAduana.SelectedValue == "020" ||
                        cmbAduana.SelectedValue == "021" || cmbAduana.SelectedValue == "022" ||
                        cmbAduana.SelectedValue == "027")
                        hr.IDFLUJO = "6";
                    else
                        hr.IDFLUJO = "0";
                    hr.ESTADO = "0";
                    hr.commitLine();
                    hr.actualizar();

                    cr.loadComplementoRecepcion("-1");
                    cr.newLine();
                    cr.IDHOJARUTA = idInstruccion;
                    cr.VALORCIF = Convert.ToDouble(txtValorCIF.Value);
                    cr.VALORFOB = Convert.ToDouble(txtValorFOB.Value);
                    cr.FLETE = Convert.ToDouble(txtFlete.Value);
                    cr.SEGURO = Convert.ToDouble(txtSeguro.Value);
                    //string codPais = "";
                    //if (cmbPaisOrigen.SelectedValue == "H")
                    //    codPais = "HN";
                    //else if (cmbPaisOrigen.SelectedValue == "G")
                    //    codPais = "GT";
                    //else if (cmbPaisOrigen.SelectedValue == "S")
                    //    codPais = "SV";
                    //else if (cmbPaisOrigen.SelectedValue == "N")
                    //    codPais = "NI";
                    //else
                    //    codPais = cmbPaisOrigen.SelectedValue;
                    //cr.CODIGOPAIS = codPais;
                    cr.CODIGOREGIMEN = cmbRegimen.SelectedValue;
                    //cr.DESTINO = txtCiudadDestino.Text;

                    //cr.PESO = Convert.ToDouble(txtPeso.Value);
                    //cr.FECHAVENCIMIENTO = dtpFechaVencTiempoLibreNaviera.SelectedDate.ToString();
                    //cr.TIPOCARGA = cmbTipoCargamento.SelectedValue;
                    cr.ESTADO = "0";
                    cr.commitLine();
                    cr.actualizar();

                    ee.loadEncabezadoEsquema("-1");
                    ee.newLine();
                    ee.IdHojaRuta = idInstruccion;
                    ee.CodigoCliente = cmbCliente.SelectedValue;
                    ee.ValorCIF = Convert.ToDouble(txtValorCIF.Value);
                    //ee.Peso = Convert.ToDouble(txtPeso.Value) / 1000;
                    //ee.DescripcionProducto = txtProducto.Text;
                    ee.DescripcionProducto = cmbProducto.Text.Trim();
                    //ee.TipoCarga = cmbTipoCargamento.SelectedValue;
                    //ee.Contenedores = Int16.Parse(txtContenedores.Text);
                    //ee.TipoCargamento = cmbTipoCarga.SelectedValue;
                    //c.loadCliente(cmbClientes.SelectedValue);
                    //ee.Importador = c.NOMBRE;
                    ee.Importador = cmbCliente.Text.Trim();
                    //ee.VecimientoNaviera = Convert.ToDateTime(dtpFechaVencTiempoLibreNaviera.SelectedDate);
                    if (cmbAduana.SelectedValue == "015" || cmbAduana.SelectedValue == "018" ||
                        cmbAduana.SelectedValue == "019" || cmbAduana.SelectedValue == "020" ||
                        cmbAduana.SelectedValue == "021" || cmbAduana.SelectedValue == "022" ||
                        cmbAduana.SelectedValue == "027")
                    {
                        if (cmbPais.SelectedValue == "H" &&
                            (cmbRegimen.SelectedValue == "1000" || cmbRegimen.SelectedValue == "1050" ||
                             cmbRegimen.SelectedValue == "1051" || cmbRegimen.SelectedValue == "1052" ||
                             cmbRegimen.SelectedValue == "1100" || cmbRegimen.SelectedValue == "2000" ||
                             cmbRegimen.SelectedValue == "2100" || cmbRegimen.SelectedValue == "2200" ||
                             cmbRegimen.SelectedValue == "3040" || cmbRegimen.SelectedValue == "3051" ||
                             cmbRegimen.SelectedValue == "3052" || cmbRegimen.SelectedValue == "3053" ||
                             cmbRegimen.SelectedValue == "3054" || cmbRegimen.SelectedValue == "3059" ||
                             cmbRegimen.SelectedValue == "3070" || cmbRegimen.SelectedValue == "3154" ||
                             cmbRegimen.SelectedValue == "3155" || cmbRegimen.SelectedValue == "5110" ||
                             cmbRegimen.SelectedValue == "5300" ||
                             cmbRegimen.SelectedValue == "3051" && cmbCliente.SelectedValue == "8000017") ||
                            cmbRegimen.SelectedValue == "CB")
                        {
                            ee.Status = "2";
                            ee.Flujo = 9;
                        }
                        else
                        {
                            ee.Status = "1";
                            ee.Flujo = 2;
                        }
                    }
                    ee.commitLine();
                    ee.actualizar();

                    //nueva modificación 09/10/2012
                    hrfp.loadHojaRutaFlujo(-1);
                    hrfp.newLine();
                    hrfp.IDHOJARUTA = idInstruccion;
                    hrfp.IDFLUJO = 1;
                    hrfp.OBS = "Sistema Aduanas";
                    hrfp.IDUSUARIO = "121";
                    hrfp.commitLine();
                    hrfp.actualizar();

                    hret.loadAllCamposHojaRuta("-1");
                    hret.newLine();
                    hret.IdHojaRuta = idInstruccion;
                    hret.IdConceptoSAP = 139;
                    hret.Monto = 0;
                    hret.ValorReal = 0;
                    hret.Solicitar = "1";
                    hret.Aprobar = "1";
                    hret.NumRecibo = idInstruccion;
                    hret.commitLine();
                    hret.actualizar();

                    //////////////////////////////////Darwin 17/07/2014///////////////////////////////////
                    eed.newLine();
                    eed.loadAllDoctos("-1");
                    eed.IdHojaRuta = idInstruccion;
                    eed.IdConceptoSAP = 139;
                    eed.IdEmpleado = "121";
                    eed.commitLine();
                    eed.actualizar();

                    //////////////////////////////////Darwin 17/07/2014///////////////////////////////////
                }
                catch (Exception ex)
                {
                    logError(ex.Message, Session["IdUsuario"].ToString(),
                        "FcCrearInstruccionFaucaCliente, crear HojaRuta y ComplementoRecepción");
                }
                finally
                {
                    DesconectarAduanas();
                }

                if (mensaje == "X")
                {
                    registrarMensaje("Instrucción " + idInstruccion + " ingresada exitosamente, en tramite " + idTramite + " " + mensaje2);
                }
                else
                {
                    registrarMensaje("Instrucción " + idInstruccion + " ingresada exitosamente, en tramite " + idTramite);
                }

                try
                {
                    conectar();
                    llenarBitacora("Se ingresó la instrucción " + idInstruccion + ", en trámite " + idTramite,
                        Session["IdUsuario"].ToString(), idInstruccion);
                    //if (User.IsInRole("Inplant") || permitir)
                    //    Response.Redirect("CrearInstrucciones.aspx");
                    //else
                    //    mpInstrucciones.SelectedIndex = 0;
                    ///////////////////////////////////////////////////////Darwin Noviembre 2014///////////////////////////////////////////////////////
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    var dg = new DetalleGastosBO(logApp);
                    dg.loadHojaxFactura(instruccion.NumeroFactura, instruccion.IdInstruccion.Substring(0, 1));
                    if (dg.totalRegistros > 0)
                    {
                        dg.ActualizarHojaxFactura(instruccion.NumeroFactura, instruccion.IdInstruccion.Substring(0, 1),
                            instruccion.IdInstruccion);
                    }
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //rgInstrucciones.Rebind();
                    //limpiarControles();
                }
                catch (Exception)
                { }
                finally
                {
                    desconectar();
                }

                #endregion

                var codEstado = "";
                if (cmbEspecieFiscal.SelectedValue == "RT")
                    codEstado = "2";
                else if (cmbEspecieFiscal.SelectedValue == "F")
                    if (cmbRegimen.SelectedValue == "1000" && cmbAduana.SelectedValue == "016")
                        codEstado = "3";
                    else
                        codEstado = "2";

                var pfc = new ProcesoFlujoCarga
                {
                    IdInstruccion = instruccion.IdInstruccion,
                    CodigoAduana = instruccion.CodigoAduana,
                    Cliente = cmbCliente.Text.Trim(),
                    Proveedor = instruccion.Proveedor,
                    Paga = "Cliente",
                    CodEstado = codEstado,
                    Fecha = DateTime.Now,
                    Eliminado = false,
                    IdUsuario = Convert.ToDecimal(Session["IdUsuario"].ToString())
                };
                if (codEstado == "3")
                {
                    pfc.Revision = "0";
                    pfc.GatePass = true;
                }
                _aduanas.ProcesoFlujoCarga.InsertOnSubmit(pfc);

                _aduanas.SubmitChanges();

                //btnGuardar.OnClientClicked = "Close()";
                //btnGuardar.Enabled = false;
            }
            else
                registrarMensaje("ValorCIF debe ser mayor que cero");
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "FcCrearInstruccionFaucaCliente");
        }
        finally
        {
            desconectar();
        }
    }

    #region Gastos Masivos

    //private Boolean Gastos_Masivos(String idCliente, String hojaRuta, String nombre)
    //{
    //    const bool crea = false;
    //    var gm = new PlantillaGastosMBO(logApp);
    //    gm.loadGastos_masivos(idCliente);
    //    try
    //    {
    //        if (gm.totalRegistros > 0)
    //        {
    //            var verificar = 0;
    //            var tasacambio = 1.00;
    //            if (hojaRuta.Substring(0, 1) == "S")
    //            {
    //                verificar = 1;
    //            }
    //            else
    //            { // verifica la tasa de cambio actual de un pais
    //                var tc = new TipoCambioBO(logApp);
    //                tc.loadtasacambio(hojaRuta.Substring(0, 1));
    //                if (tc.totalRegistros >= 1)
    //                {
    //                    verificar = tc.totalRegistros;
    //                    tasacambio = tc.TASACAMBIO;
    //                }
    //            }

    //            if (verificar == 1)
    //            {
    //                for (var i = 0; i < gm.totalRegistros; i++)
    //                {
    //                    var dg1 = new DetalleGastosBO(logApp);
    //                    var dg = new DetalleGastosBO(logApp);
    //                    dg.BuscarHoja("1");
    //                    dg.newLine();
    //                    dg.IDINSTRUCCION = hojaRuta;
    //                    dg.IDGASTO = gm.IDGASTO;
    //                    dg.MEDIOPAGO = gm.MEDIOPAGO;
    //                    dg.PROVEEDOR = gm.IDPROVEEDOR;
    //                    dg.MONTO = gm.COSTO * tasacambio;
    //                    dg.IVA = gm.IVA;
    //                    dg.TOTALVENTA = (gm.VENTA * tasacambio);
    //                    dg.TASACAMBIOFIJA = tasacambio;
    //                    dg.TASACAMBIOVARIABLE = tasacambio;

    //                    var c = new CodigosBO(logApp);
    //                    c.loadAllCampos("MONEDA" + hojaRuta.Substring(0, 1));
    //                    dg.MONEDA = c.CODIGO;
    //                    dg.ESTADO = dg.MONTO > 0 ? "0" : "10";

    //                    dg.ESTADO = "0";
    //                    dg.FECHACREACION = DateTime.Now.ToString();
    //                    dg.USUARIO = Session["IdUsuario"].ToString();

    //                    if (gm.MEDIOPAGO == "CC" && dg.MONTO > 0)
    //                    {
    //                        dg.ESTADO = "7";
    //                    }
    //                    dg.OBSERVACION = gm.OBSERVACION;
    //                    dg.CODESTADOFACTURACION = "0";
    //                    dg.REVERSARPARTIDA = "2";
    //                    dg.commitLine();
    //                    dg.actualizar();
    //                    gm.regSiguiente();
    //                }
    //                //  registrarMensaje("Gastos predetrminados  ingresados  exitosamente!");
    //            }
    //            else
    //            {
    //                // Eviar correo electronicoa a la nueva categoria indicando que no se crearon los gastor porque la tasa esta descatualizaza y quedebn ser creados automaticamente 
    //                EnviarCorreo(hojaRuta, nombre);
    //            }
    //        }
    //    }
    //    catch
    //    { }
    //    return crea;
    //}

    //private void CargaCargill(string idInstruccion)
    //{
    //    try
    //    {
    //        var ic = new InstruccionesCargillCargaBO(logApp);
    //        ic.VerificarRegistro(idInstruccion);
    //        if (ic.totalRegistros == 0)
    //        {
    //            ic.newLine();
    //        }
    //        ic.IDINSTRUCCION = idInstruccion;
    //        ic.TIPOCARGACARGILL = cmbCargaCargill.SelectedValue;
    //        ic.IDUSUARIO = int.Parse(Session["IdUsuario"].ToString());
    //        ic.commitLine();
    //        ic.actualizar();
    //    }
    //    catch (Exception)
    //    {
    //    }
    //}

    private void EnviarCorreo(string idinstruccion, string cliente)
    {
        try
        {
            var c = new CorreosBO(logApp);
            c.LoadCorreosXPais("N", "Gastos_Masivos");

            var asunto = " Notificacion de Gastos masivos  en Instrucción " + idinstruccion + " para el cliente " + cliente;
            var cuerpo = " Los [ Gastos  Masivos ] No fueron generados  automáticamente  en la Instrucción <b> " + idinstruccion + " …</b> para el cliente <font color=\"red\">" + cliente + "</font> debido a que" +
                            " la tasa de cambio no estaba actualizada, por lo tanto  <b> los gastos deben ser ingresados manualmente </b>. Favor tomar nota;" +
                            "<br><br><br> Este mensaje es generado  automáticamente , favor no responderlo";
            var copias = c.COPIAS;
            const string nombreUsuario = "Tracking Aduanas";
            const string nombredelArchivo = "Remision";
            const string usuario = "trackingaduanas";
            const string pass = "nMgtdA$7PaRjQphEYZBD";
            var para = c.CORREOS;
            SendMailConParametros(para, asunto, cuerpo, copias, nombreUsuario, nombredelArchivo, usuario, pass, ".xlsx");
        }
        catch (Exception)
        { }
    }

    private void SendMailConParametros(string para, string subject, String body, string copias, string nombreUsuario, string fileName, string user, string pass, string extencion)
    {
        try
        {
            // SmtpClient SmtpServer = new SmtpClient("mail.grupovesta.com");
            ServicePointManager.ServerCertificateValidationCallback += (o, c, ch, er) => true;
            StringBuilder bodytmp = new StringBuilder();
            //string bodytmp = @"<html><body><img src=""cid:FirmaJuandeDios""></body></html>";
            //string firma = @"<img src="+ "cid:" + to + ">";
            MailMessage mailmsg = new MailMessage();
            mailmsg.From = new MailAddress("trackingaduanas@grupovesta.com", user);
            mailmsg.To.Add(new MailAddress(para));
            mailmsg.CC.Add(copias);
            //mailmsg.Bcc.Add(new MailAddress(mailFrom, nombreUsuario));                
            mailmsg.Subject = subject;
            //   mailmsg.Attachments.Add(new Attachment(@"J:\Files1\Aduanas\" + fileName + ".xlsx"));

            //bodytmp.Append("<html xmlns=\"http://www.w3.org/1999/xhtml\"> <head> <title>" + subject + "</title> </head> ");
            bodytmp.Append("<html> <head> <title>" + subject + "</title> </head> ");
            bodytmp.Append("<body> ");
            bodytmp.Append("<table>");
            bodytmp.Append("<tr>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("</tr>");
            bodytmp.Append("<tr>");
            bodytmp.Append("<td>" + body + "</td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("</tr>");
            bodytmp.Append("<tr>");
            //  bodytmp.Append("<td> " + "<img src=J:\\Files1\\Aduanas\\FirmaVesta.jpg</td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("</tr>");
            bodytmp.Append("</table>");
            bodytmp.Append("</body>");
            bodytmp.Append("</html>");

            AlternateView avHTML = AlternateView.CreateAlternateViewFromString(bodytmp.ToString(), null, MediaTypeNames.Text.Html);

            //CODIGO PARA AGREGAR IMAGEN AL CUERPO DEL CORREO
            //LinkedResource yourPictureRes = new LinkedResource(@"J:\Files1\Aduanas\FirmaVesta.jpg", MediaTypeNames.Image.Jpeg);
            //yourPictureRes.ContentId = mailFrom;
            //avHTML.LinkedResources.Add(yourPictureRes);
            //CODIGO PARA AGREGAR IMAGEN AL CUERPO DEL CORREO

            mailmsg.AlternateViews.Add(avHTML);

            mailmsg.IsBodyHtml = true;

            StreamWriter sw = new StreamWriter(new MemoryStream(), Encoding.UTF8);
            sw.Write(body.ToString());
            sw.Flush();

            sw.BaseStream.Position = 0;
            //mailmsg.Attachments.Add(new Attachment(sw.BaseStream, "Template.html", MediaTypeNames.Text.Html));

            SmtpClient smtpClient = new SmtpClient("190.4.28.53");
            smtpClient.Port = 587;
            smtpClient.EnableSsl = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(user, pass);

            smtpClient.Send(mailmsg);

            sw.Close();
        }
        catch (Exception ex)
        {
            //LogError(ex.InnerException + ex.Message, "sendMailConParametros", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), Session["IdUsuario"].ToString(), "Utilidades.cs");
        }
    }
    #endregion

    private void RegistrarMensaje2(string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("alert('" + mensaje + "');");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "mensaje", sb.ToString(), false);
    }

    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        try
        {
            btnCancelar.OnClientClicked = "Close()";
        }
        catch (Exception)
        { }
    }

    protected void cmbEspecieFiscal_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            var regimen = "";
            if (cmbEspecieFiscal.SelectedValue == "F")
                regimen = "1000";
            else if (cmbEspecieFiscal.SelectedValue == "RT")
                regimen = "4000";
            var regimenes = _aduanas.Codigos.Where(c => c.Categoria == "REGIMENHONDURAS" && c.Codigo1 == regimen && c.Eliminado == '0')    //Exportación Definitiva
                .Select(c => new { c.Codigo1, Descripcion = c.Codigo1 + " | " + c.Descripcion }).ToList();
            cmbRegimen.DataSource = regimenes;
            cmbRegimen.DataBind();

        }
        catch (Exception)
        { }
    }

    protected void cmbCliente_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            var producto = _aduanas.ProductosClientes.Where(c => c.CodigoCliente == Convert.ToDecimal(cmbCliente.SelectedValue))
                .Select(c => new { c.Descripcion }).ToList();
            cmbProducto.DataSource = producto;
            cmbProducto.DataBind();
        }
        catch (Exception)
        { }
    }

    protected void cmbProducto_ItemChecked(object sender, Telerik.Web.UI.RadComboBoxItemEventArgs e)
    { }

    #region Conexion
    private void ConectarAduanas()
    {
        if (_logAppAduanas == null)
            _logAppAduanas = new GrupoLis.Login.Login(); ;
        if (_logAppAduanas.conectado) return;
        _logAppAduanas.tipoConexion = TipoConexion.SQL_SERVER;
        _logAppAduanas.SERVIDOR = mParamethers.Get("Servidor");
        _logAppAduanas.DATABASE = mParamethers.Get("DatabaseEsquemaTramites2");
        _logAppAduanas.USER = mParamethers.Get("User");
        _logAppAduanas.PASSWD = mParamethers.Get("Password");
        _logAppAduanas.conectar();
    }

    private void DesconectarAduanas()
    {
        if (_logAppAduanas.conectado)
            _logAppAduanas.desconectar();
    }
    #endregion



    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Crear Instruccion Fauca Cliente";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadData();


            var paginaMasterBase = (SiteMaster)Master;
            if (paginaMasterBase != null)
                paginaMasterBase.SetTitulo("Crear Instrucción Fauca Cliente", "Crear Instrucción FC");
            if (!Ingresar)
                redirectTo("Default.aspx");
        }
    }

    private bool Ingresar { get { return tienePermiso("INGRESAR"); } }

    public override bool CanGoBack { get { return false; } }

    private void LoadData()
    {
        try
        {
            var idUsuario = Convert.ToDecimal(Session["IdUsuario"].ToString());
            var paises = _aduanas.Codigos.Where(c => c.Categoria == "PAISES" && c.Eliminado == '0')
                .Join(_aduanas.Usuarios.Where(u => u.Eliminado == '0' && u.IdUsuario == idUsuario),
                c => c.Codigo1, u => u.CodPais, (c, u) => new { c, u })
                .Select(cu => cu.c).ToList();

            //IEnumerable<Aduana> aduanas;
            //if (User.IsInRole("Oficial de Cuenta") || User.IsInRole("Inplant"))
            //{
            //    cmbAduana.Enabled = true;
            //    cmbAduana.EmptyMessage = "Seleccione";
            //    aduanas = _aduanas.Aduanas.Where(c => c.CodEstado == "0" && c.CodPais == "H")
            //        .Select(c => c).ToList();
            //}
            //else
            //{
            //    cmbAduana.Enabled = false;
            //    aduanas = _aduanas.Aduanas.Where(c => c.CodEstado == "0")
            //    .Join(_aduanas.Usuarios.Where(u => u.Eliminado == '0' && u.IdUsuario == idUsuario),
            //    c => c.CodigoAduana, u => u.CodigoAduana, (c, u) => new { c, u })
            //    .Select(cu => cu.c).ToList();
            //}
            IEnumerable<AduanasVm> aduanas;
            if (User.IsInRole("Oficial de Cuenta") || User.IsInRole("Inplant"))
            {
                cmbAduana.Enabled = true;
                cmbAduana.EmptyMessage = "Seleccione";
                aduanas = _aduanas.Aduanas.Where(c => c.CodEstado == "0" && c.CodPais == "H")
                    .Select(c => new AduanasVm
                    {
                        CodigoAduana = c.CodigoAduana,
                        NombreAduana = c.NombreAduana
                    }).ToList();
            }
            else
            {
                cmbAduana.Enabled = false;
                aduanas = _aduanas.Aduanas.Where(c => c.CodEstado == "0")
                .Join(_aduanas.Usuarios.Where(u => u.Eliminado == '0' && u.IdUsuario == idUsuario),
                c => c.CodigoAduana, u => u.CodigoAduana, (c, u) => new { c, u })
                 .Select(cu => new AduanasVm
                 {
                     CodigoAduana = cu.c.CodigoAduana,
                     NombreAduana = cu.c.NombreAduana
                 }).ToList();
            }

            var especieFiscal = _aduanas.Codigos.Where(c => c.Categoria == "ESPECIESFISCALES" && (c.Codigo1 == "F" || c.Codigo1 == "RT") && c.Eliminado == '0')
                .Select(c => new { c.Codigo1, c.Descripcion }).ToList();
            var clientes = _aduanas.Clientes.Where(c => c.Eliminado == '0' && c.MarcadoFc == true)
                .Select(c => new { c.CodigoSAP, c.Nombre }).ToList();
            var proveedores = _aduanas.ProveedoresPais.Where(c => c.MarcadoFc == true)
                .Join(_aduanas.Usuarios.Where(u => u.Eliminado == '0' && u.IdUsuario == idUsuario),
                c => c.CodPais, u => u.CodPais, (c, u) => new { c, u })
                .Select(cu => cu.c).ToList();

            cmbPais.DataSource = paises;
            cmbPais.DataBind();
            cmbAduana.DataSource = aduanas;
            cmbAduana.DataBind();
            cmbEspecieFiscal.DataSource = especieFiscal;
            cmbEspecieFiscal.DataBind();
            cmbCliente.DataSource = clientes;
            cmbCliente.DataBind();
            dtpFechaRecepcion.SelectedDate = DateTime.Now;
            cmbProveedor.DataSource = proveedores;
            cmbProveedor.DataBind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }
}