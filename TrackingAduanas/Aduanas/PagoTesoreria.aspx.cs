﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;
using Utilities.Web;
using ObjetosDto;
using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;
//using Microsoft.SharePoint.Client;
using System.Net;

public partial class GenerarArchivoSAPDinant : Utilidades.PaginaBase
{
    private GrupoLis.Login.Login logAppAduanas;
    string HojaRuta = "";
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Pago Tesoreria";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
         if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Hagamoda/Promoda", "Pago Tesoreria");
        }


         //#region Digitalizacion de Documentos
         //if (SessionFileShare.SessionCreada(this))
         //{
         //    var resp = SessionFileShare.GetRespuestaDownload(this);
         //    int respuesta = 0;
         //    string Solicitud = Request.QueryString.Get("IdSolicitud");
         //    string correlativo = "";

         //    var listTemp = resp.

         //    listTemp.ForEach(f =>
         //    {
         //        var id = f.IdSistema;
         //        respuesta = f.Estado;

         //    });

         //    if (respuesta == 1)
         //    {

         //        var obj = listTemp.FirstOrDefault(a => idHojaRuta == a.IdSistema);
         //        correlativo = obj.DocumentoResponseVms.FirstOrDefault().NoReferencia;
         //        this.Digitalizar(idHojaRuta, correlativo);
         //        mes = true;
         //    }



         //    SessionFileShare.DestruirFileShare(this);

         //    if (mes == true)
         //    {
         //        registrarMensaje("Documento digitalizado correctamente");
         //        // rgIngresos.Rebind();

         //    }
         //    else
         //    {
         //        registrarMensaje("Los Documentos no fueron digitalizados correctamente");
         //    }
         //}

         //#endregion

    }

    protected bool Consultar { get { return tienePermiso("Consultar"); } }



    private void ConfigureExport()
    {
        String filename = "Reporte_" + DateTime.Now.ToShortDateString();
        rgGastos.ExportSettings.FileName = filename;
        rgGastos.ExportSettings.ExportOnlyData = true;
        rgGastos.ExportSettings.IgnorePaging = true;
        rgGastos.ExportSettings.OpenInNewWindow = true;
        rgGastos.MasterTableView.ExportToExcel();
    }

    public override bool CanGoBack { get { return false; } }


    private void llenarGrid()
    {
        try
        {
            conectar();

            UsuariosBO u = new UsuariosBO(logApp);
            DetalleGastosBO DG = new DetalleGastosBO(logApp);
            if (User.IsInRole("Administradores") && User.Identity.Name == "dabonilla" || User.IsInRole("Administradores") && User.Identity.Name == "aalvarado" || User.IsInRole("Administradores") && User.Identity.Name == "lramirez" || User.IsInRole("Administradores") && User.Identity.Name == "mnapky" || User.IsInRole("Administradores") && User.Identity.Name == "gcorrales"|| User.IsInRole("Administradores") && User.Identity.Name == "ycaballero")
                DG.loadInstruccionesSolicitudPagoIMPAgrupado();
            else if (User.IsInRole("Tesoreria"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                DG.loadInstruccionesSolicitudPagoIMPXPaisAgrupado(u.CODPAIS);
            }
            rgGastos.DataSource = DG.TABLA;

        }
        catch (Exception)
        {

        }

        finally { desconectar(); }

    }
    private static string sourceURL = "http://192.168.205.121:11400/";
    //protected void descargArchivo(string fileURL, string fileName)
    //{
    //    try
    //    {
    //        Stream res = DownloadFile(sourceURL, fileURL, "Administrator", ConfigurationManager.AppSettings["passSP"].ToString(), "spvesta");

    //        MemoryStream memoryStream;
    //        memoryStream = new MemoryStream();
    //        res.CopyTo(memoryStream);
    //        memoryStream.Position = 0;

    //        byte[] file = memoryStream.ToArray();
    //        if (file != null)
    //        {

    //            Response.Clear();
    //            Response.ClearHeaders();
    //            Response.ClearContent();
    //            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
    //            Response.AddHeader("Content-Length", file.Length.ToString());
    //            Response.ContentType = "Application/pdf";//MimeMapping.GetMimeMapping(fileName);
    //            Response.BinaryWrite(file);
    //            Response.End();
    //        }
    //        else
    //        {
    //            registrarMensaje1("Error al bajar archivo, por favor intente mas tarde.");
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        string resss = ex.Message;
    //    }
    //}
    //protected Stream DownloadFile(string siteURL, string fileURL, string user, string pass, string domain)
    //{
    //    Stream success = null;

    //    try
    //    {
    //        using (ClientContext clientContext = new ClientContext(siteURL))
    //        {
    //            clientContext.Credentials = new System.Net.NetworkCredential(user, pass, domain);

    //            FileInformation fInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(clientContext, fileURL);

    //            success = fInfo.Stream;
    //        }
    //    }
    //    catch (Exception)
    //    {
    //    }
    //    finally
    //    {
    //    }
    //    return success;
    //}
    //protected void descargararchivo(string hoja, string y, string fecha, string IdDigitalizacion)
    //{
    //    if (hoja.Substring(0, 1) == "N")
    //    {
    //        descargArchivo("/NICARAGUA/" + y + "/" + fecha + "/" + IdDigitalizacion, IdDigitalizacion);
    //    }
    //    else if (hoja.Substring(0, 1) == "S")
    //    {
    //        descargArchivo("/El SALVADOR/" + y + "/" + fecha + "/" + IdDigitalizacion, IdDigitalizacion);
    //        //descargArchivo("/LAPRUEBA/" + y + "/" + fecha + "/" + IdDigitalizacion, IdDigitalizacion);
    //    }
    //    else if (hoja.Substring(0, 1) == "G")
    //    {
    //        descargArchivo("/GUATEMALA/" + y + "/" + fecha + "/" + IdDigitalizacion, IdDigitalizacion);
    //    }
    //}

    protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenarGrid();
    }

    #region Conexion
    private void conectarAduanas(string Pais)
    {
        if (logAppAduanas == null)
            logAppAduanas = new GrupoLis.Login.Login();
        if (!logAppAduanas.conectado)
        {
            logAppAduanas.tipoConexion = TipoConexion.SQL_SERVER;
            logAppAduanas.SERVIDOR = mParamethers.Get("Servidor2");
            if (Pais.ToString() == "S")
            {
                logAppAduanas.DATABASE = mParamethers.Get("CajaChicaSV");
            }
            if (Pais.ToString() == "N")
            {
                logAppAduanas.DATABASE = mParamethers.Get("CajaChicaNI");
            }
            if (Pais.ToString() == "G")
            {
                logAppAduanas.DATABASE = mParamethers.Get("CajaChicaGT");
            }
            logAppAduanas.USER = mParamethers.Get("User2");
            logAppAduanas.PASSWD = mParamethers.Get("Password2");
            logAppAduanas.conectar();
        }
    }

    private void desconectarAduanas()
    {
        if (logAppAduanas.conectado)
            logAppAduanas.desconectar();
    }
    #endregion

    //protected void btnAgrupar_Click(object sender, EventArgs e)
    //{
    //    cargarWindow("Ingresar Numero SAP", "~/NumerodeSAP.aspx", 700, 400);
    //}
    //protected void cargarWindow(String titleBar, string navigateURL, int width, int heigth)
    //{
    //    RadWindowManager2.Windows.Clear();
    //    RadWindow window1 = new RadWindow();
    //    window1.NavigateUrl = navigateURL;
    //    window1.VisibleOnPageLoad = true;
    //    window1.Title = titleBar;
    //    if (width > 0)
    //    {
    //        window1.Width = width;
    //        window1.Height = heigth;
    //        window1.Behaviors = WindowBehaviors.Move | WindowBehaviors.Pin | WindowBehaviors.Close;
    //    }
    //    else
    //    {
    //        window1.InitialBehaviors = WindowBehaviors.Maximize;
    //    }

    //    window1.Animation = WindowAnimation.FlyIn;
    //    window1.Modal = true;
    //    window1.DestroyOnClose = true;
    //    window1.VisibleStatusbar = false;
    //    RadWindowManager2.Windows.Add(window1);
    //}
    //protected void CerrarWindow()
    //{
    //    RadWindowManager2.Windows.Clear();
    //    RadWindow window1 = new RadWindow();
    //    window1.VisibleOnPageLoad = false;
    //    window1.Animation = WindowAnimation.FlyIn;
    //    window1.Modal = true;
    //    window1.DestroyOnClose = true;
    //    window1.VisibleStatusbar = false;
    //    RadWindowManager2.Windows.Add(window1);
    //}
    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        GridDataItem editForm = (sender as RadButton).NamingContainer as GridDataItem;
      
           SessionFileShare.CreateDownload(idSistema:  editForm["HojaRuta"].Text, page: this);
       
        //foreach (GridDataItem grdItem in rgGastos.Items)
        //{
        //    CheckBox Descargar = (CheckBox)grdItem.FindControl("chbDescarga");
        //    if (Descargar.Checked == true)
        //    {
        //        conectar();
        //        DetalleGastosBO DG = new DetalleGastosBO(logApp);
        //        Descargar.Checked = false;
        //        DG.DescargaSharePoint(grdItem.Cells[11].Text);
        //        descargararchivo(grdItem.Cells[7].Text, grdItem.Cells[8].Text, DG.TABLA.Rows[0]["FechaDigitalizacion"].ToString(), DG.TABLA.Rows[0]["IdDigitalizacion"].ToString());
        //    }
        //}
    }
    protected void rgGastos_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
       
            GridDataItem editedItem = e.Item as GridDataItem;
          
           RadTextBox NumeroSAP = (RadTextBox)editedItem.FindControl("txtNumeroSAP");


  
            if (e.CommandName == "Guardar")
            {
                if (NumeroSAP.Text.Trim()!=String.Empty)
                {

                  conectar();

                string Referencia = editedItem["IdDigitalizacion"].Text;
                string Proveedor =  editedItem["Proveedor"].Text;
                string total =  editedItem["total"].Text;


                UsuariosBO u = new UsuariosBO(logApp);
                DetalleGastosBO DG = new DetalleGastosBO(logApp);
                EnviarCorreo_Aprobacion(Proveedor, Referencia, total);
                DG.ActualizaRSapXReferencia(Referencia, NumeroSAP.Text);
               

                desconectar();

                registrarMensaje("Gastos ingresados correctamente");
                rgGastos.Rebind();
                llenarGrid();
                return;

                }
                else
                { 
                    registrarMensaje("Por favor Ingrese el Número SAP ");
                    NumeroSAP.Focus();
                }
            }


            if (e.CommandName == "descarga")
            {

                SessionFileShare.CreateDownload(idSistema: editedItem["HojaRuta"].Text, page: this);
            }
         
        }
        catch { }
    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        //CerrarWindow();
        if (Session["Recibo"].ToString() != "")
        {
            
            foreach (GridDataItem grdItem in rgGastos.Items)
            {
                CheckBox chkRango = (CheckBox)grdItem.FindControl("chbAgrupar");
                RadTextBox NumeroSAP = (RadTextBox)grdItem.FindControl("txtNumeroSAP");
                if (chkRango.Checked == true )
                {
                    try
                    {
                        conectar();
                        DetalleGastosBO DG = new DetalleGastosBO(logApp);
                        DG.LlenarGatosXHojaYGastoTesoreria(grdItem.Cells[7].Text, grdItem.Cells[11].Text);
                        if (DG.totalRegistros >= 1)
                        {
                            DG.NUMEROSAP = Session["Recibo"].ToString();
                            DG.FECHANUMEROSAP = DateTime.Now.ToString();
                            DG.ESTADO = "10";
                            DG.actualizar();
                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally { desconectar(); }

                }
            }
            registrarMensaje("Gastos ingresados correctamente");
            rgGastos.Rebind();
            llenarGrid();
        }
        else
            foreach (GridDataItem grdItem in rgGastos.Items)
            {
                CheckBox chkRango = (CheckBox)grdItem.FindControl("chbAgrupar");
                RadTextBox NumeroSAP = (RadTextBox)grdItem.FindControl("txtNumeroSAP");
                if (NumeroSAP.Text != "")
                {
                    try
                    {
                        conectar();

                        DetalleGastosBO DG = new DetalleGastosBO(logApp);
                        DG.LlenarGatosXHojaYGastoTesoreria(grdItem.Cells[7].Text, grdItem.Cells[11].Text);
                        if (DG.totalRegistros >= 1)
                        {
                            DG.NUMEROSAP = NumeroSAP.Text;
                            DG.FECHANUMEROSAP = DateTime.Now.ToString();
                            DG.ESTADO = "4";
                            DG.actualizar();
                        }
                    }
                    catch (Exception)
                    {

                    }
                    finally { desconectar(); }

                }
            }
       
    }

    protected void rgIngresos_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
    {
        try
        {

            conectar();
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            string Referencia = dataItem["IdDigitalizacion"].Text;

            UsuariosBO u = new UsuariosBO(logApp);
            DetalleGastosBO DG = new DetalleGastosBO(logApp);



            if (User.IsInRole("Administradores") && User.Identity.Name == "dabonilla" || User.IsInRole("Administradores") && User.Identity.Name == "aalvarado" || User.IsInRole("Administradores") && User.Identity.Name == "lramirez" || User.IsInRole("Administradores") && User.Identity.Name == "mnapky" || User.IsInRole("Administradores") && User.Identity.Name == "gcorrales" || User.IsInRole("Administradores") && User.Identity.Name == "ycaballero")
                DG.loadInstruccionesSolicitudPagoIMPREFERENCIA(Referencia);
            else if (User.IsInRole("Tesoreria"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                DG.loadInstruccionesSolicitudPagoIMPXPaisREFERENCIA(u.CODPAIS, Referencia);
            }
  
         
            e.DetailTableView.DataSource = DG.TABLA;


        }
        catch (Exception)
        {

        }
        finally { desconectar(); }

    }

      #region Enviar correo


    protected void EnviarCorreo_Aprobacion( string Proveedor, string  Referencia , string total)
    {
        try
        {

            string moneda;
            CorreosBO C = new CorreosBO(logApp);          
            DetalleGastosBO DG = new DetalleGastosBO(logApp);
            DetalleGastosBO DG_referencia = new DetalleGastosBO(logApp);
            GatosTracking GT = new GatosTracking(logApp);

            DG.BuscarReferencia(Referencia);
            UsuariosBO nuevo_solicito = new UsuariosBO(logApp);
            UsuariosBO Usuario_confirmo = new UsuariosBO(logApp);
            nuevo_solicito.loadUsuario(DG.USUARIODIGITALIZACION);

            DG_referencia.loadInstruccionesSolicitudPagoIMPREFERENCIA(Referencia);
           
            String Hojas_Gasto = "";
            Usuario_confirmo.loadUsuario(Session["IdUsuario"].ToString());
      
            for (int i = 0; i < DG_referencia.totalRegistros; i++) {
                GT.LoadGatos(DG_referencia.IDGASTO);
                Hojas_Gasto += "Hoja de Ruta:" + DG_referencia.IDINSTRUCCION.ToString() + "- Gasto:" + GT.GASTO + " "+ DG_referencia.MONEDA+"<br>";
                DG_referencia.regSiguiente();
            
            }
                         
            string Asunto = " Confirmación de Pago Tesorería ";
            string Cuerpo = "Referencia: [ " + Referencia + " ].<br>Proveedor:" + Proveedor + ".<br>Total:" + total + " .<br>";
                   Cuerpo += Hojas_Gasto;       
                   Cuerpo+=     "<br><br><br> Este mensaje es generado  automáticamente , favor no responderlo";
                   string Copias = nuevo_solicito.EMAIL.ToString();
            string NombreUsuario = "Tracking Aduanas";
            string NombredelArchivo = "Confirmación de Pago";
            string usuario = "trackingaduanas";
            string pass = "nMgtdA$7PaRjQphEYZBD";
            string para =Usuario_confirmo.EMAIL.ToString();
            sendMailConParametros(para, Asunto, Cuerpo, Copias, NombreUsuario, NombredelArchivo, usuario, pass, ".xlsx");
        }
        catch (Exception)
        {

        }


    }


    protected void sendMailConParametros(string para, string subject, String body, string Copias, string nombreUsuario, string fileName, string user, string pass, string extencion)
    {
        try
        {
            // SmtpClient SmtpServer = new SmtpClient("mail.grupovesta.com");
            ServicePointManager.ServerCertificateValidationCallback += (o, c, ch, er) => true;
            StringBuilder bodytmp = new StringBuilder();
            //string bodytmp = @"<html><body><img src=""cid:FirmaJuandeDios""></body></html>";
            //string firma = @"<img src="+ "cid:" + to + ">";
            MailMessage mailmsg = new MailMessage();
            mailmsg.From = new MailAddress("trackingaduanas@grupovesta.com", user);
            mailmsg.To.Add(new MailAddress(para));
            mailmsg.CC.Add(Copias);
            //mailmsg.Bcc.Add(new MailAddress(mailFrom, nombreUsuario));                
            mailmsg.Subject = subject;
            //   mailmsg.Attachments.Add(new Attachment(@"J:\Files1\Aduanas\" + fileName + ".xlsx"));

            //bodytmp.Append("<html xmlns=\"http://www.w3.org/1999/xhtml\"> <head> <title>" + subject + "</title> </head> ");
            bodytmp.Append("<html> <head> <title>" + subject + "</title> </head> ");
            bodytmp.Append("<body> ");
            bodytmp.Append("<table>");
            bodytmp.Append("<tr>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("</tr>");
            bodytmp.Append("<tr>");
            bodytmp.Append("<td>" + body + "</td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("</tr>");
            bodytmp.Append("<tr>");
            //  bodytmp.Append("<td> " + "<img src=J:\\Files1\\Aduanas\\FirmaVesta.jpg</td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("</tr>");
            bodytmp.Append("</table>");
            bodytmp.Append("</body>");
            bodytmp.Append("</html>");

            AlternateView avHTML = AlternateView.CreateAlternateViewFromString(bodytmp.ToString(), null, MediaTypeNames.Text.Html);

            //CODIGO PARA AGREGAR IMAGEN AL CUERPO DEL CORREO
            //LinkedResource yourPictureRes = new LinkedResource(@"J:\Files1\Aduanas\FirmaVesta.jpg", MediaTypeNames.Image.Jpeg);
            //yourPictureRes.ContentId = mailFrom;
            //avHTML.LinkedResources.Add(yourPictureRes);
            //CODIGO PARA AGREGAR IMAGEN AL CUERPO DEL CORREO

            mailmsg.AlternateViews.Add(avHTML);

            mailmsg.IsBodyHtml = true;

            StreamWriter sw = new StreamWriter(new MemoryStream(), Encoding.UTF8);
            sw.Write(body.ToString());
            sw.Flush();

            sw.BaseStream.Position = 0;
            //mailmsg.Attachments.Add(new Attachment(sw.BaseStream, "Template.html", MediaTypeNames.Text.Html));

            SmtpClient smtpClient = new SmtpClient("190.4.28.53");
            smtpClient.Port = 587;
            smtpClient.EnableSsl = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(user, pass);

            smtpClient.Send(mailmsg);

            sw.Close();
        }
        catch (Exception ex)
        {
            //LogError(ex.InnerException + ex.Message, "sendMailConParametros", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), Session["IdUsuario"].ToString(), "Utilidades.cs");
        }
    }
    #endregion


    


   
}

