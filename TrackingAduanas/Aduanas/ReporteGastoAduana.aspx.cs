﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.OleDb;
using System.Text;
using System.Configuration;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;

using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.SessionState;
using System.Collections.Specialized;
using GrupoLis.Login;
using System.Text.RegularExpressions;
using System.Security.Principal;
using Telerik.Web;
using System.Security.Cryptography;
public partial class ReporteGestionCarga : Utilidades.PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgReporte.FilterMenu);
        rgReporte.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgReporte.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Gestión Carga", "Reporte Gestión Carga");
            cargarDatosInicio();
        }
    }

    protected void cargarDatosInicio()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            ClientesBO cl = new ClientesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (u.CODPAIS == "H")
                c.loadPaisesHojaRuta();
            else
                c.loadAllCampos("PAISES", u.CODPAIS);
            cmbPais.DataSource = c.TABLA;
            cmbPais.DataBind();
            cl.loadAllCamposClientesXPais(cmbPais.SelectedValue);
            cmbCliente.DataSource = cl.TABLA;
            cmbCliente.DataBind();
        }
        catch { }
    }

    public override bool CanGoBack { get { return false; } }

    private void Salir()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("top.location.href = LoginTracking.aspx;");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Salir", sb.ToString(), false);
    }

    protected void cmbPais_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        CargarDatos();
    }

    protected void CargarDatos()
    {
        try
        {
            conectar();
            ClientesBO cl = new ClientesBO(logApp);
            AduanasBO A = new AduanasBO(logApp);
            GatosTracking GT = new GatosTracking(logApp);
            ////////////////////////////Clientes////////////////////////////
            cl.loadAllCamposClientesXPais(cmbPais.SelectedValue);
            cmbCliente.DataSource = cl.TABLA;
            cmbCliente.DataBind();
            ////////////////////////////Aduanas////////////////////////////
            A.loadPaisAduanas(cmbPais.SelectedValue);
            cmbAduanas.DataSource = A.TABLA;
            cmbAduanas.DataBind();
            ////////////////////////////Gastos////////////////////////////
            GT.LlenarGatos(cmbPais.SelectedValue);
            cmbGastos.DataSource = GT.TABLA;
            cmbGastos.DataBind();
        }
        catch { }
        finally { desconectar(); }
    }
    #region Reporte
    protected void rgInstrucciones_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridInstrucciones();
    }

    private void llenarGridInstrucciones()
    {
        try
        {
            conectar();
            DetalleGastosBO DG = new DetalleGastosBO(logApp);
            DG.CargarDatosReporteGastos(cmbCliente.SelectedValue, cmbGastos.SelectedValue, dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"), cmbAduanas.SelectedValue);
            rgReporte.DataSource = DG.TABLA;
            rgReporte.DataBind();
        }
        catch (Exception) { }
        finally
        {
            desconectar();
        }
    }

    protected void rgInstrucciones_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();
            if (rgReporte.Items.Count <= 0 & e.CommandName == RadGrid.ExportToExcelCommandName)
                e.Canceled = true;
            else if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                ConfigureExport();
            }
        }
        catch (Exception)
        { }
    }

    private void ConfigureExport()
    {
        String filename = "Reporte Gasto Terrestre" + "_" + DateTime.Now.ToShortDateString();
        rgReporte.GridLines = GridLines.Both;
        rgReporte.ExportSettings.FileName = filename;
        rgReporte.ExportSettings.IgnorePaging = true;
        rgReporte.ExportSettings.OpenInNewWindow = false;
        rgReporte.ExportSettings.ExportOnlyData = true;
    }
    #endregion

    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        if (dpFechaInicio.SelectedDate <= dpFechaFinal.SelectedDate)
        {
            llenarGridInstrucciones();
            rgReporte.Visible = true;
        }
        else
        {
            registrarMensaje2("Fecha inicio no puede ser mayor que fecha final");
            rgReporte.Visible = false;
        }
    }

    private void registrarMensaje2(string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("alert('" + mensaje + "');");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "mensaje", sb.ToString(), false);
    }

    protected void rgInstrucciones_Init(object sender, System.EventArgs e)
    {
        GridFilterMenu menu = rgReporte.FilterMenu;
        menu.Items.RemoveAt(rgReporte.FilterMenu.Items.Count - 2);
    }
}