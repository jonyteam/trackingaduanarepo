﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteMovimientosArancelarios.aspx.cs" Inherits="GenerarArchivoSAPDinant" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style2
        {
            width: 22%;
        }
        .style3
        {
            width: 15%;
        }
        .style4
        {
            width: 22px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                    //<![CDATA[
                    
                function pnlRequestStarted(ajaxPanel, eventArgs) {
                    try {
                        if ((eventArgs.EventTarget == "<%= btnSalvar.UniqueID %>")) {
                            eventArgs.EnableAjax = false;
                        }
                    }
                    catch (err)
                { }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                    }
                }
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }
                   
            </script>
        </telerik:RadScriptBlock>
        <table border="1" style="width: 55%">
            <tr>
                <td style="width: 15%">
                    <asp:Label ID="lblFechaInicio" runat="server" Text="Fecha Inicio:" ForeColor="Black"></asp:Label>
                </td>
                <td class="style2" style ="width: 20%">
                    <telerik:RadDatePicker ID="dpFechaInicio" runat="server" EnableTyping="False" 
                        Width="125px">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="dpFechaInicio"
                        ErrorMessage="Ingrese Fecha" ForeColor="Red" />
                </td>
                <td class="style3" style ="width: 15%">
                    <asp:Label ID="lblFechaFinal" runat="server" Text="Fecha Final:" ForeColor="Black"></asp:Label>
                </td>
                <td class="style4" style ="width: 20%">
                    <telerik:RadDatePicker ID="dpFechaFinal" runat="server" EnableTyping="False" 
                        Width="125px">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator runat="server" ID="reqName" ControlToValidate="dpFechaFinal"
                        ErrorMessage="Ingrese Fecha" ForeColor="Red" />
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <input id="iTipoFlete" runat="server" type="hidden" />
                    <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Images/24/view_24.png"
                        OnClick="btnBuscar_Click" />
                </td>
            </tr>
        </table>
        <br />
        <telerik:RadGrid ID="RadGrid1" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" 
            GridLines="None" Width="1413px" onitemcommand="RadGrid1_ItemCommand" 
            onneeddatasource="RadGrid1_NeedDataSource" PageSize="20">
            <ClientSettings AllowColumnsReorder="True">
                <Selecting AllowRowSelect="True" />
            </ClientSettings>
             <ExportSettings HideStructureColumns="true" 
                FileName ="Reporte Movimientos" OpenInNewWindow="True">  <Excel Format ="Biff" />  </ExportSettings>
            <MasterTableView CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" ExportToPdfText="Export to PDF" ShowAddNewRecordButton = "false" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                    Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                    Visible="True">
                </ExpandCollapseColumn>
                <Columns>
        <telerik:GridBoundColumn DataField="RTN" FilterControlWidth="60px" 
            HeaderText="RTN" UniqueName="RTN">
        </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FDCNumber" 
                        FilterControlAltText="Filter FDCNumber column" HeaderText="FDC Number" 
                        UniqueName="FDCNumber">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="BUname" 
                        FilterControlAltText="Filter BUname column" HeaderText="BU name" 
                        UniqueName="BUname">
                    </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="CodPaisHojaRuta" HeaderText="Pais" 
            UniqueName="CodPaisHojaRuta" FilterControlWidth="80px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Descripcion" HeaderText="Descripcion" 
            UniqueName="Descripcion" FilterControlWidth="120px" AllowFiltering="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="PosicionArancelaria" HeaderText="PosicionArancelaria" 
            UniqueName="PosicionArancelaria" FilterControlWidth="50px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="NumeroTransacciones" HeaderText="Numero Transacciones" 
            UniqueName="NumeroTransacciones" FilterControlWidth="100px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="DAI" HeaderText="DAI" 
            UniqueName="DAI" FilterControlWidth="45px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Selectivo" 
            HeaderText="Selectivo" UniqueName="Selectivo" 
            FilterControlWidth="60px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="ISV" HeaderText="ISV" 
            UniqueName="ISV" FilterControlWidth="60px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Nombre" HeaderText="Nombre" 
            UniqueName="Nombre" FilterControlWidth="50px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Pais" HeaderText="Pais" 
            UniqueName="Pais" FilterControlWidth="30px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Origen" HeaderText="Pais Origen" UniqueName="Descripcion" 
            FilterControlWidth="100px" AllowFiltering="False">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="PeriodoFin" HeaderText="PeriodoFin" 
            UniqueName="PeriodoFin" FilterControlWidth="100px">
        </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Preferencia" 
                        FilterControlAltText="Filter Preferencia column" HeaderText="Preferencia" 
                        UniqueName="Preferencia">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CodigoTransaccion" 
                        FilterControlAltText="Filter CodigoTransaccion column" 
                        HeaderText="CodigoTransaccion" UniqueName="CodigoTransaccion">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn 
                        FilterControlAltText="Filter column column" HeaderText="Country of Consignment" 
                        UniqueName="Import" DataField="Import">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn 
                        FilterControlAltText="Filter column1 column" HeaderText="Country of Destination" 
                        UniqueName="Export" DataField="Export">
                    </telerik:GridBoundColumn>
    </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        <br />
        <br />
        <table width="100%">
            <tr align="center">
                <td colspan="6" align="center">
                    <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="~/Images/gris.png"/>
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
    <%--<GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>--%>
</asp:Content>
