﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="SolicitudPagosSinFactura.aspx.cs" Inherits="GenerarArchivoSAPDinant" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style2
        {
            width: 22%;
        }
        .style3
        {
            width: 15%;
        }
        .style4
        {
            width: 22px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                    //<![CDATA[
                    
                function pnlRequestStarted(ajaxPanel, eventArgs) {
                    try {
                        if ((eventArgs.EventTarget == "<%= btnSalvar.UniqueID %>")) {
                            eventArgs.EnableAjax = false;
                        }
                    }
                    catch (err)
                { }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                    }
                }
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }
                   
            </script>
        </telerik:RadScriptBlock>
        <input id="cmbMaterial" runat="server" type="hidden"  />
        <input id="cmbMedioPago" runat="server" type="hidden"  />
        <input id="cmbPagarA" runat="server" type="hidden"  />
        <input id="txtMonto" runat="server" type="hidden"  />
        <input id="txtIVA" runat="server" type="hidden"  />
        <input id="txtMontoPagar" runat="server" type="hidden"  />
        <input id="cmbMoneda" runat="server" type="hidden"  />

        <br />
        <telerik:RadGrid ID="RadGrid1" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" BorderStyle="Solid" 
            CellSpacing="0" GridLines="None" onitemcommand="RadGrid1_ItemCommand" 
            onneeddatasource="RadGrid1_NeedDataSource" PageSize="20" 
            style="margin-right: 0px" Width="1500px" 
            onitemdatabound="RadGrid1_ItemDataBound" AllowFilteringByColumn="True">
            <ClientSettings AllowColumnsReorder="True">
                <Selecting AllowRowSelect="True" />
            </ClientSettings>
            <ExportSettings FileName="Reporte" HideStructureColumns="true" 
                OpenInNewWindow="True">
                <Excel AutoFitImages="True" Format="Biff" />
            </ExportSettings>
            <MasterTableView CommandItemDisplay="Top">
                <CommandItemSettings ExportToPdfText="Export to PDF" 
                    ShowAddNewRecordButton="false" ShowExportToExcelButton="true" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                    Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                    Visible="True">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn1 column" 
                        HeaderText="Numero de Factura" UniqueName="TemplateColumn1">
                        <ItemTemplate>
                            <telerik:RadTextBox ID="txtNroFactura" Runat="server" 
                                EmptyMessage="Numero Factura" LabelWidth="64px" Width="160px">
                            </telerik:RadTextBox>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="IdInstruccion" 
                        FilterControlAltText="Filter IdInstruccion column" HeaderText="Hoja de Ruta" 
                        UniqueName="IdInstruccion" Visible="False">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter Material column" 
                        HeaderText="Material" UniqueName="Material">
                        <ItemTemplate>
                            <telerik:RadComboBox ID="cmbMaterial" Runat="server" AutoPostBack="True" 
                                DataTextField="Gasto" DataValueField="IdGasto" 
                                onselectedindexchanged="cmbMaterial_SelectedIndexChanged">
                            </telerik:RadComboBox>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter MedioPago column" 
                        HeaderText="Medio Pago" UniqueName="MedioPago">
                        <ItemTemplate>
                            <telerik:RadComboBox ID="cmbMedioPago" Runat="server" AutoPostBack="True" 
                                DataTextField="Descripcion" DataValueField="Codigo" 
                                onselectedindexchanged="cmbMedioPago_SelectedIndexChanged" Visible="False">
                            </telerik:RadComboBox>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter PagarA column" 
                        HeaderText="Pagar A" UniqueName="PagarA">
                        <ItemTemplate>
                            <telerik:RadComboBox ID="cmbPagarA" Runat="server" AutoPostBack="True" 
                                DataTextField="Nombre" DataValueField="Cuenta" 
                                onselectedindexchanged="cmbPagarA_SelectedIndexChanged" Visible="False" 
                                Filter="Contains">
                            </telerik:RadComboBox>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter Monto column" 
                        HeaderText="Monto" UniqueName="Monto">
                        <ItemTemplate>
                            <telerik:RadNumericTextBox ID="txtMonto" Runat="server" EmptyMessage="Monto" 
                                Visible="False">
                            </telerik:RadNumericTextBox>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter IVA column" 
                        HeaderText="IVA" UniqueName="IVA">
                        <ItemTemplate>
                            <telerik:RadNumericTextBox ID="txtIVA" Runat="server" EmptyMessage="IVA" 
                                Visible="False">
                            </telerik:RadNumericTextBox>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter MontoPagar column" 
                        HeaderText="Valor Negociado" UniqueName="MontoPagar">
                        <ItemTemplate>
                            <telerik:RadNumericTextBox ID="txtValorVenta" Runat="server" 
                                EmptyMessage="Monto a Pagar" Visible="False">
                            </telerik:RadNumericTextBox>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter Moneda column" 
                        HeaderText="Moneda" UniqueName="Moneda">
                        <ItemTemplate>
                            <telerik:RadComboBox ID="cmbMoneda" Runat="server" DataTextField="Descripcion" 
                                DataValueField="Codigo" Visible="False">
                            </telerik:RadComboBox>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" 
                        UniqueName="TemplateColumn">
                        <ItemTemplate>
                            <telerik:RadTextBox ID="txtObservacion" Runat="server" 
                                EmptyMessage="Ingrese una observacion" LabelWidth="64px" Visible="False" 
                                Width="160px">
                            </telerik:RadTextBox>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter Solicitar column" 
                        HeaderText="Solicitar" UniqueName="Solicitar">
                        <ItemTemplate>
                            <telerik:RadButton ID="btnsolicitar" runat="server" 
                                onclick="btnSolicitar_Click" Text="Solicitar" Visible="False">
                            </telerik:RadButton>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <ItemStyle BorderColor="Black" BorderStyle="Solid" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        <br />
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        </telerik:RadWindowManager>
        <br />
        <br />
        <table width="100%">
            <tr align="center">
                <td colspan="6" align="center">
                    <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="~/Images/gris.png"/>
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
    <%--<GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>--%>
</asp:Content>
