﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HabilitarEspecies.aspx.cs"
    Inherits="ReporteEventos" MasterPageFile="~/Site.master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style1 {
            width: 14%;
            height: 33px;
            text-align: center;
        }

        .style2 {
            width: 7%;
            height: 35px;
            text-align: left;
        }

        .style3 {
            width: 14%;
            height: 35px;
            text-align: center;
        }

        .style4 {
            width: 7%;
            height: 33px;
            text-align: left;
        }

        .style5 {
            width: 100%;
        }

        .style6 {
            width: 11%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="200px"
        Width="100%" Style="text-align: left"  LoadingPanelID="LoadingPanel" >
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                //<![CDATA[
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0
                    || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }

            </script>
        </telerik:RadScriptBlock>
        <table id="Table1" runat="server" style="width: 100%">
            <tr>
                <td class="style2">
                    <asp:Label ID="lblInstruccion" Font-Bold="true" runat="server"
                        Text="Serie:"></asp:Label>
                </td>
                <td class="style3">
                    <telerik:RadTextBox ID="txtSerie" runat="server" MaxLength="15" Width="140px"
                        EmptyMessage="Serie" LabelWidth="40%">
                    </telerik:RadTextBox>
                </td>
                <td style="width: 85%; text-align: left;" colspan="2">
                    <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Images/24/view_24.png"
                        OnClick="btnBuscar_Click" />
                </td>
            </tr>
            <tr>
                <td class="style4">
                    <asp:Label ID="lblEspecie" Font-Bold="True" runat="server" Text="Tipo Especie Fiscal:"></asp:Label>
                </td>
                <td class="style1">
                    <telerik:RadComboBox ID="cmbEspecieFiscal" runat="server" DataTextField="Descripcion"
                        DataValueField="Codigo" Width="140px" AutoPostBack="True" OnSelectedIndexChanged="cmbEspecieFiscal_SelectedIndexChanged">
                    </telerik:RadComboBox>
                </td>
                <td style="text-align: left;" class="style6">
                    &nbsp;</td>
                <td style="width: 85%; text-align: left;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style4">
                    &nbsp;</td>
                <td class="style1">
                    &nbsp;</td>
                <td style="text-align: left;" class="style6">
                    &nbsp;</td>
                <td style="width: 85%; text-align: left;">
                    &nbsp;</td>
            </tr>
        </table>
        <telerik:RadGrid ID="rgEspecies" runat="server" AllowFilteringByColumn="True" 
                AllowPaging="True" AllowSorting="True" AutoGenerateColumns="True" 
                CellSpacing="0" GridLines="None" OnNeedDataSource="rgEspecies_NeedDataSource" 
                PageSize="20" Visible="False" style="margin-top: 0px" OnItemDataBound="rgEspecies_ItemDataBound">
                <ClientSettings AllowColumnsReorder="True">
                    <Selecting AllowRowSelect="True" />
                    <Selecting AllowRowSelect="True" />
                </ClientSettings>
                <MasterTableView>
                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                        Visible="True">
                        <HeaderStyle Width="20px" />
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                        Visible="True">
                        <HeaderStyle Width="20px" />
                    </ExpandCollapseColumn>
                    
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                </MasterTableView>
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
        <table class="style5">
            <tr>
                <td>        
                    <asp:ImageButton ID="btnGuardar" runat="server"
                        ImageUrl="~/Images/16/add2_16.png"
                          ConfirmText="¿Está seguro que desea generar reporte de esta instrucción?"
                          ConfirmDialogType="RadWindow" 
                          ConfirmTitle="return confirm('¿Está seguro que desea Liberar o Anular Especie Fiscal?')"
                         
                          onclientclick="return confirm('¿Está seguro que desea Liberar o Anular Especie Fiscal?')"
                          OnClick="btnGuardar_Click" 
                        ToolTip="Salvar" Visible="False" Height="36px" Width="47px" />
                </td>
                    <%--onclientclick="return confirm('¿Está seguro que desea Liberar o Anular instrucción?')"--%>
            </tr>
        </table>


        <script type="text/javascript">
            function Clicking(sender, args) {
                args.set_cancel(!window.confirm("¿Está seguro que desea Habilitar la instrucción?"));
            }
        </script>

    </telerik:RadAjaxPanel>


</asp:Content>
