﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="CargarArchivo.aspx.cs" Inherits="CargarArchivo" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .wrap
        {
            white-space: normal;
            width: 98px;
        }
        
        .style1
        {
            width: 100px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest"
        ClientEvents-OnRequestStart="requestStart">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgIngresos">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgIngresos" LoadingPanelID="LoadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgIngresos" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" EnableSkinTransparency="true">
        <div class="loading">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Ajax/Img/loading1.gif" AlternateText="loading">
            </asp:Image>
        </div>
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            //<![CDATA[
            function requestStart(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
            }

            function OnClientClose() {
                $find("<%= RadAjaxManager1.ClientID%>").ajaxRequest("Rebind");
            }
        </script>
    </telerik:RadScriptBlock>
    <telerik:RadMultiPage ID="mpRequerimiento" runat="server" SelectedIndex="0" style="text-align: left">
        <telerik:RadPageView ID="RadPageView1" runat="server" Width="99.9%">
            <table width="100%">
                <tr>
                    <td style="width: 100%">
                        <asp:Label ID="lblIngresos" runat="server" Text="Cargar" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="IdTransaccion" runat="server" type="hidden" />
                        <telerik:RadGrid ID="rgIngresos" runat="server" AllowFilteringByColumn="True" AllowSorting="True"
                            AutoGenerateColumns="False" GridLines="None" Width="99.9%" Height="420px" OnNeedDataSource="rgIngresos_NeedDataSource"
                            AllowPaging="True" ShowFooter="True" ShowStatusBar="True" PageSize="20" OnInit="rgIngresos_Init"
                            OnItemCommand="rgIngresos_ItemCommand" CellSpacing="0">
                            <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                                PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                                Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                            <MasterTableView DataKeyNames="IdInstruccion" CommandItemDisplay="Top" NoDetailRecordsText="No hay registros."
                                NoMasterRecordsText="No hay ingresos." GroupLoadMode="Client">
                                <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                                    ShowExportToExcelButton="true" ShowExportToCsvButton="false" />
                                <RowIndicatorColumn>
                                    <HeaderStyle Width="20px"></HeaderStyle>
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn>
                                    <HeaderStyle Width="20px"></HeaderStyle>
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Tiempo" ImageUrl="Images/16/index_up_16.png"
                                        UniqueName="Tiempo" HeaderText="Tiempo">
                                        <HeaderStyle Width="75px" />
                                    </telerik:GridButtonColumn>
                                    <telerik:GridBoundColumn DataField="IdInstruccion" HeaderText="Instrucción No." UniqueName="IdInstruccion"
                                        FilterControlWidth="80%">
                                        <ItemStyle Width="10%" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn DataField="Cliente" HeaderText="Cliente" UniqueName="Cliente"
                                        FilterControlWidth="80%">
                                        <ItemStyle Width="10%" />
                                    </telerik:GridBoundColumn>
                                    <%--<telerik:GridBoundColumn DataField="ArchivosCargardos" HeaderText="Archivos Cargardos" UniqueName="ArchivosCargardos"
                                        FilterControlWidth="80%">
                                        <HeaderStyle Width="130px" />
                                    </telerik:GridBoundColumn>--%>
                                </Columns>
                            </MasterTableView>
                            <HeaderStyle Width="180px" />
                            <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                                <Selecting AllowRowSelect="True" />
                                <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                                    DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                                    DropHereToReorder="Suelte aquí para Re-Ordenar" />
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
                            </ClientSettings>
                            <FilterMenu EnableTheming="True">
                                <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                            </FilterMenu>
                            <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                            <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
                        </telerik:RadGrid>
                    </td>
                </tr>
            </table>
            <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
            </telerik:RadWindowManager>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
</asp:Content>
