﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="MantenimientoFlujoCarga.aspx.cs" Inherits="MantenimientoFlujoCarga" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadGrid ID="rgEstadosFlujo" runat="server" AllowFilteringByColumn="True"
        AllowSorting="True" AutoGenerateColumns="False" GridLines="None" Width="100%"
        Height="420px" OnNeedDataSource="rgEstadosFlujo_NeedDataSource" OnUpdateCommand="rgEstadosFlujo_UpdateCommand"
        OnDeleteCommand="rgEstadosFlujo_DeleteCommand" OnInsertCommand="rgEstadosFlujo_InsertCommand"
        AllowPaging="True" ShowFooter="True" ShowStatusBar="True" PageSize="50" OnInit="rgEstadosFlujo_Init"
        OnItemCreated="rgEstadosFlujo_ItemCreated" OnItemDataBound="rgEstadosFlujo_ItemDataBound">
        <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
            PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
            Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
        <MasterTableView DataKeyNames="IdEstadoFlujo,CodRegimen" CommandItemDisplay="TopAndBottom"
            NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay estados de flujo de carga definidos."
            GroupLoadMode="Client">
            <CommandItemSettings AddNewRecordText="Agregar Estado de Flujo de Carga" RefreshText="Volver a Cargar Datos" />
            <RowIndicatorColumn>
                <HeaderStyle Width="20px"></HeaderStyle>
            </RowIndicatorColumn>
            <ExpandCollapseColumn>
                <HeaderStyle Width="20px"></HeaderStyle>
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridEditCommandColumn AutoPostBackOnFilter="True" CancelText="Cancelar"
                    EditText="Editar" InsertText="Insertar" UpdateText="Grabar" ButtonType="ImageButton">
                    <HeaderStyle Width="5%" />
                    <ItemStyle Width="5%" />
                </telerik:GridEditCommandColumn>
                <telerik:GridButtonColumn ConfirmText="¿Esta seguro que desea eliminar este estado de flujo de carga?"
                    ConfirmDialogType="RadWindow" ConfirmTitle="Eliminar Estado de Flujo de Carga"
                    UniqueName="DeleteColumn" ButtonType="ImageButton" CommandName="Delete">
                    <HeaderStyle Width="5%" />
                    <ItemStyle Width="5%" />
                </telerik:GridButtonColumn>
                <telerik:GridBoundColumn DataField="IdEstadoFlujo" HeaderText="EstadoFlujo No." UniqueName="IdEstadoFlujo"
                    FilterControlWidth="80%">
                    <HeaderStyle Width="16%" />
                    <ItemStyle Width="16%" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Pais" HeaderText="País" UniqueName="Pais" FilterControlWidth="80%">
                    <HeaderStyle Width="16%" />
                    <ItemStyle Width="16%" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Regimen" HeaderText="Régimen" UniqueName="Regimen"
                    FilterControlWidth="80%">
                    <HeaderStyle Width="46%" />
                    <ItemStyle Width="46%" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="EstadoFlujoCarga" HeaderText="Estado Flujo Carga"
                    UniqueName="EstadoFlujoCarga" FilterControlWidth="80%">
                    <HeaderStyle Width="46%" />
                    <ItemStyle Width="46%" />
                </telerik:GridBoundColumn>
                <%-- <telerik:GridBoundColumn DataField="CodEstadoFlujoCarga" HeaderText="CodEstadoFlujoCarga"
                    UniqueName="CodEstadoFlujoCarga" FilterControlWidth="80%">
                    <HeaderStyle Width="16%" />
                    <ItemStyle Width="16%" />
                </telerik:GridBoundColumn>--%>
                <%--<telerik:GridBoundColumn DataField="EstadoFlujo" HeaderText="Estado" UniqueName="EstadoFlujo"
                    FilterControlWidth="80%" Aggregate="Count" FooterText="Cantidad: ">
                    <HeaderStyle Width="30%" />
                    <ItemStyle Width="30%" />
                </telerik:GridBoundColumn>--%>
                <%--   <telerik:GridBoundColumn DataField="Obligatorio" HeaderText="Obligatorio" UniqueName="Obligatorio"
                    FilterControlWidth="80%" >
                    <HeaderStyle Width="10%" />
                    <ItemStyle Width="10%" />
                </telerik:GridBoundColumn>--%>
                <telerik:GridBoundColumn DataField="Orden" HeaderText="Orden" UniqueName="Orden"
                    FilterControlWidth="80%">
                    <HeaderStyle Width="10%" />
                    <ItemStyle Width="10%" />
                </telerik:GridBoundColumn>
            </Columns>
            <GroupByExpressions>
                <telerik:GridGroupByExpression>
                    <SelectFields>
                        <telerik:GridGroupByField FieldAlias="País" FieldName="Pais"></telerik:GridGroupByField>
                    </SelectFields>
                    <GroupByFields>
                        <telerik:GridGroupByField FieldAlias="País" FieldName="Pais"></telerik:GridGroupByField>
                    </GroupByFields>
                </telerik:GridGroupByExpression>
                <telerik:GridGroupByExpression>
                    <SelectFields>
                        <telerik:GridGroupByField FieldAlias="Régimen" FieldName="Regimen"></telerik:GridGroupByField>
                    </SelectFields>
                    <GroupByFields>
                        <telerik:GridGroupByField FieldAlias="Régimen" FieldName="Regimen"></telerik:GridGroupByField>
                    </GroupByFields>
                </telerik:GridGroupByExpression>
            </GroupByExpressions>
            <EditFormSettings EditFormType="Template" CaptionDataField="Centros" CaptionFormatString="">
                <EditColumn UniqueName="EditCommandColumn1">
                </EditColumn>
                <FormTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# ((Container is GridEditFormInsertItem) ? "Agregar Nuevo Estado de Flujo de Carga" : "Editar Estado de Flujo de Carga Existente") %>'
                        Font-Size="Medium" Font-Bold="true" />
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 10%">
                            </td>
                            <td style="width: 90%">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 10%">
                                <asp:Label ID="lblPais" runat="server" Text="Pais" Font-Bold="true" />
                            </td>
                            <td style="width: 90%">
                                <telerik:RadComboBox ID="edPais" runat="server" Skin='<%# Skin %>' DataTextField="Descripcion"
                                    Filter="Contains" DataValueField="Codigo" DataSource='<%# GetPaises() %>' Enabled='<%# (Container is GridEditFormInsertItem) ? true : false %>'
                                    ForeColor="Black" SelectedValue='<%# Bind("CodPais") %>' EmptyMessage="Seleccione el Pais"
                                    Width="50%" CausesValidation="false" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 10%">
                                <asp:Label ID="lblRegimen" runat="server" Text="Régimen" Font-Bold="true" />
                            </td>
                            <td style="width: 90%">
                                <telerik:RadComboBox ID="edRegimen" runat="server" Skin='<%# Skin %>' DataTextField="CodigoDescripcion"
                                    Filter="Contains" DataValueField="Codigo" EmptyMessage="Seleccione el Régimen"
                                    Width="50%" CausesValidation="false" Enabled='<%# (Container is GridEditFormInsertItem) ? true : false %>'
                                    ForeColor="Black" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 10%">
                                <asp:Label ID="lbEstadoFlujo" runat="server" Text="Estado de Flujo" Font-Bold="true" />
                            </td>
                            <td style="width: 90%">
                                <telerik:RadComboBox ID="edEstadoFlujo" runat="server" Skin='<%# Skin %>' DataTextField="Descripcion"
                                    Filter="Contains" DataValueField="Codigo" DataSource='<%# GetEstadosFlujoCarga() %>'
                                    SelectedValue='<%# Bind("CodEstadoFlujoCarga") %>' EmptyMessage="Seleccione el Pais"
                                    Width="50%" CausesValidation="false" />
                            </td>
                        </tr>
                        <%-- <tr>
                            <td style="width: 10%">
                                <asp:Label ID="lbEstadoFlujo" runat="server" Text="Estado de Flujo" Font-Bold="true" />
                            </td>
                            <td style="width: 90%">
                                <telerik:RadTextBox ID="edEstadoFlujo" runat="server" Text='<%# Bind("EstadoFlujo") %>'
                                    EmptyMessage="Escriba el Estado de Flujo aquí" Width="50%" />
                                <asp:RequiredFieldValidator ID="rfEstadoFlujo" runat="server" EnableClientScript="true"
                                    Display="Dynamic" ErrorMessage="El estado de flujo no puede estar vacío" ControlToValidate="edEstadoFlujo" />
                            </td>
                        </tr>--%>
                        <%--<tr>
                            <td style="width: 10%">
                                <asp:Label ID="lblObligatorio" runat="server" Text="Obligatorio" Font-Bold="true" />
                            </td>
                            <td style="width: 90%">
                                <telerik:RadComboBox ID="edObligatorio" runat="server" Skin='<%# Skin %>' DataTextField="Descripcion"
                                    Filter="Contains" DataValueField="Descripcion" DataSource='<%# GetDecision() %>'
                                    SelectedValue='<%# Bind("Obligatorio") %>' EmptyMessage="Seleccione si es o no obligatorio"
                                    Width="50%" CausesValidation="false" />
                            </td>
                        </tr>--%>
                        <tr>
                            <td style="width: 10%">
                                <asp:Label ID="lblOrden" runat="server" Text="Orden" Font-Bold="true" />
                            </td>
                            <td style="width: 90%">
                                <telerik:RadNumericTextBox ID="edOrden" runat="server" Text='<%# Bind("Orden") %>'
                                    EmptyMessage="Escriba el orden aquí" Width="50%" NumberFormat-DecimalDigits="0"
                                    NumberFormat-GroupSeparator="" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" EnableClientScript="true"
                                    Display="Dynamic" ErrorMessage="El orden no puede estar vacío" ControlToValidate="edOrden" />
                            </td>
                        </tr>
                    </table>
                    <asp:ImageButton ID="btnSave" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                        AlternateText='<%# (Container is GridEditFormInsertItem) ? "Agregar Aduana" : "Actualizar Aduana" %>'
                        ImageUrl='Images/16/check2_16.png' />
                    &nbsp
                    <asp:ImageButton ID="btnCancel" runat="server" CommandName="Cancel" AlternateText="Cancelar"
                        ImageUrl='Images/16/forbidden_16.png' CausesValidation="false" />
                </FormTemplate>
            </EditFormSettings>
        </MasterTableView>
        <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
            <Selecting AllowRowSelect="True" />
            <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                DropHereToReorder="Suelte aquí para Re-Ordenar" />
            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
        </ClientSettings>
        <FilterMenu EnableTheming="True">
            <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
        </FilterMenu>
        <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
        <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
    </telerik:RadGrid>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgEstadosFlujo">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgEstadosFlujo" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>
