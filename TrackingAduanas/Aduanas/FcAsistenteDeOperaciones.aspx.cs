﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using ObjetosDto;
using Telerik.Web.UI;

public partial class FcAsistenteDeOperaciones : Utilidades.PaginaBase
{
    readonly AduanasDataContext _aduanasDc = new AduanasDataContext();
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Asistente de Operaciones";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgAsistenteOperaciones.FilterMenu);
        rgAsistenteOperaciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgAsistenteOperaciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Asistente de Operaciones", "Asistente de Operaciones");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
        }
        Timer1.Enabled = true;
    }

    private bool Anular { get { return tienePermiso("ANULAR"); } }
    private bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    private bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    #region Asistente de Operaciones
    protected void rgAsistenteOperaciones_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        LlenarGrid();
    }

    private void LlenarGrid()
    {
        try
        {

            var noAsistente = new List<string>() { "8000838", "8000079", "8000900","8000839", "8000840", "8000006" };

            var lista = _aduanasDc.Instrucciones.Where(w => w.IdCliente == "8000839" && w.CodRegimen == "4000")
                    .Join(_aduanasDc.ProcesoFlujoCarga.Where(p => p.Eliminado == false && p.CodEstado == "1"), i => i.IdInstruccion, p => p.IdInstruccion,
                            (i,p) => new {i = i, p = p})
                    .Select(s => new {
                        Id = s.p.Id,
                        IdInstruccion = s.i.IdInstruccion,
                        Cliente = s.p.Cliente,
                        Proveedor = s.p.Proveedor,
                        CodRegimen = s.i.CodRegimen,
                        NoFactura = s.i.NumeroFactura,
                        Correlativo = s.i.NoCorrelativo
                    }).ToList(); 
                    

            var query = _aduanasDc.ProcesoFlujoCarga.Where(p => p.Eliminado == false && p.CodEstado == "1")
               .Join(_aduanasDc.Instrucciones.Select(i => new { i.CodEstado, i.IdInstruccion, i.CodRegimen, i.NumeroFactura, i.IdCliente })
               //.Join(Codigos.Where(w => w.Categoria == "ASISTENTEOPERACIONES"), i => i.IdCliente, c => c.Codigo, (i, c) => new { i = i, c = c })
               .Where(i => i.CodEstado != "100" && (!noAsistente.Contains(i.IdCliente))), p => p.IdInstruccion, i => i.IdInstruccion, (p, i) => new { p, i })
               .Select(pi => new
               {
                   Id = pi.p.Id,
                   IdInstruccion = pi.p.IdInstruccion,
                   Cliente = pi.p.Cliente,
                   Proveedor = pi.p.Proveedor,
                   CodRegimen = pi.i.CodRegimen,
                   NoFactura = pi.i.NumeroFactura,
                   Correlativo = pi.p.Correlativo
               }).ToList();

            var union = query.Union(lista).ToList();

            //var query = _aduanasDc.ProcesoFlujoCarga.Where(p => p.Eliminado == false && p.CodEstado == "1")
            //    .Join(_aduanasDc.Instrucciones.Select(i => new { i.CodEstado, i.IdInstruccion, i.CodRegimen, i.NumeroFactura })
            //    .Where(i => i.CodEstado != "100"), p => p.IdInstruccion, i => i.IdInstruccion, (p, i) => new { p, i })
            //    .Select(pi => new ProcesoFlujoCargaGridVm
            //    {
            //        Id = pi.p.Id,
            //        IdInstruccion = pi.p.IdInstruccion,
            //        Cliente = pi.p.Cliente,
            //        Proveedor = pi.p.Proveedor,
            //        CodRegimen = pi.i.CodRegimen,
            //        NoFactura = pi.i.NumeroFactura,
            //        Correlativo = pi.p.Correlativo
            //    })
            //    .ToList();

            rgAsistenteOperaciones.DataSource = union;
        }
        catch { }
    }

    protected void rgAsistenteOperaciones_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            var editedItem = e.Item as GridEditableItem;
            if (e.CommandName == "Descargar")
            {
                if (editedItem != null)
                {
                    var idInstruccion = editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdInstruccion"].ToString();
                    Timer1.Enabled = false;
                    SessionFileShare.CreateDownload(idSistema: idInstruccion, page: this);
                }
            }
            if (rgAsistenteOperaciones.Items.Count >= 1 & (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName))
                ConfigureExport("");
            else if (rgAsistenteOperaciones.Items.Count < 1 & (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName))
                e.Canceled = true;
        }
        catch { }
    }

    private void ConfigureExport(string descripcion)
    {
        System.Web.HttpPostedFileBase fileBase;
        var filename = descripcion + " " + DateTime.Now.ToShortDateString();
        rgAsistenteOperaciones.ExportSettings.FileName = filename;
        rgAsistenteOperaciones.ExportSettings.ExportOnlyData = true;
        rgAsistenteOperaciones.ExportSettings.IgnorePaging = true;
        rgAsistenteOperaciones.ExportSettings.OpenInNewWindow = true;
        rgAsistenteOperaciones.MasterTableView.ExportToExcel();
    }

    protected void rgAsistenteOperaciones_Init(object sender, EventArgs e)
    {
        var menu = rgAsistenteOperaciones.FilterMenu;
        menu.Items.RemoveAt(rgAsistenteOperaciones.FilterMenu.Items.Count - 2);
    }

    private void RegistrarMensaje2(string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("alert('" + mensaje + "');");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "mensaje", sb.ToString(), false);
    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
        rgAsistenteOperaciones.Rebind();
    }
    #endregion
}