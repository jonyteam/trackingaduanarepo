﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

//using System.Diagnostics;

public partial class CrearInstrucciones : Utilidades.PaginaBase
{
    private NameValueCollection mParamethers;
    protected GrupoLis.Login.Login logAppPN;
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Crear Instrucciones";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        mParamethers = ConfigurationManager.AppSettings;
        SetGridFilterMenu(rgInstrucciones.FilterMenu);
        rgInstrucciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgInstrucciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Crear Instrucciones", "Crear Instrucciones");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    private void cargarDatosInicio()
    {
        try
        {
            //conectar();
            //ClientesBO cliente = new ClientesBO(logApp);
            ////cliente.loadAllCamposClientesS();
            //cliente.loadAllCamposClientesG();
            //cmbCliente.DataSource = cliente.TABLA;
            //cmbCliente.DataBind();
        }
        catch (Exception)
        {
        }
        finally
        {
            desconectar();
        }
    }

    protected void rgInstrucciones_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridInstrucciones();
    }

    private void llenarGridInstrucciones()
    {
        try
        {
            conectarPN();
            conectar();
            InstruccionAduanaBO ia = new InstruccionAduanaBO(logAppPN);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (User.IsInRole("Administradores") || User.IsInRole("Inplant") || User.Identity.Name == "davila")
                ia.loadInstruccionesAduana(mParamethers.Get("Database"));
            else if (User.IsInRole("Administrador Pais") || User.IsInRole("Administrador Pais SVGT") || User.IsInRole("Operaciones") || User.IsInRole("Jefe Centralización"))
            {
                string idU = Session["IdUsuario"].ToString();
                if (User.Identity.Name == "spolanco"
                    || User.Identity.Name == "jgarcia" || User.Identity.Name == "ncontreras" ||
                    User.Identity.Name == "afonseca" || User.Identity.Name == "cmorales")
                    ia.loadInstruccionesAduanaXUsuario(u.CODPAIS, mParamethers.Get("Database"), idU);
                else
                    ia.loadInstruccionesAduana(u.CODPAIS, mParamethers.Get("Database"));
            }
            else if (User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT") || User.IsInRole("Analista de Documentos"))
            {
                string idU = Session["IdUsuario"].ToString();
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.totalRegistros > 0)
                    if (u.CODPAIS == "H")
                        //ins.loadInstruccionesAllXAduanaUsuario(u.CODIGOADUANA, idU);
                        if (User.Identity.Name == "eizaguirre")
                        { ia.loadInstruccionesAduanaXAd(u.CODPAIS, mParamethers.Get("Database"), "016"); }
                        else
                        {
                            ia.loadInstruccionesAduanaXAd(u.CODPAIS, mParamethers.Get("Database"), u.CODIGOADUANA);
                        }
                    else
                        //ins.loadInstruccionesAllXUsuario(idU);
                        ia.loadInstruccionesAduana(u.CODPAIS, mParamethers.Get("Database"));
            }
            //else
            //ia.loadInstruccionesAduanaXAd(u.CODPAIS, mParamethers.Get("Database"), u.CODIGOADUANA);
            rgInstrucciones.DataSource = ia.TABLA;
            rgInstrucciones.DataBind();
            //rgInstrucciones.DataSource = new string[] { };
        }
        catch { }
        finally
        {
            desconectarPN();
            desconectar();
        }
    }

    protected void cmbCliente_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        llenarGridInstrucciones();
    }

    protected void rgInstrucciones_Init(object sender, System.EventArgs e)
    {
        GridFilterMenu menu = rgInstrucciones.FilterMenu;
        menu.Items.RemoveAt(rgInstrucciones.FilterMenu.Items.Count - 2);
    }

    protected void btnCrear_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Response.Redirect("Instrucciones.aspx?IdInstruccion=CREAR");
        }
        catch (Exception)
        { }
    }

    protected void rgInstrucciones_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            GridDataItem editedItem = e.Item as GridDataItem;
            if (e.CommandName == "Crear")
            {
                String estado = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Estado"]);
                String instruccion = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Instruccion"]);
                if (estado == "1" || estado == "2")
                    Response.Redirect("Instrucciones.aspx?IdInstruccion=" + instruccion.Trim() + "&Estado=" + estado + "&Opcion=modificar");
            }
            else if (e.CommandName == "Duplicar")
            {
                String estado = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Estado"]);
                String instruccion = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Instruccion"]);
                if (estado == "1")
                    Response.Redirect("Instrucciones.aspx?IdInstruccion=" + instruccion.Trim() + "&Estado=" + estado + "&Opcion=duplicar");
                else
                    registrarMensaje("No puede duplicar esta instrucción");
            }
            else if (rgInstrucciones.Items.Count <= 0 & e.CommandName == RadGrid.ExportToExcelCommandName)
                e.Canceled = true;
            else if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
                ConfigureExport();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    private void ConfigureExport()
    {
        rgInstrucciones.Columns[0].Visible = false;
        String filename = "CrearInstrucciones_" + DateTime.Now.ToShortDateString();
        rgInstrucciones.GridLines = GridLines.Both;
        rgInstrucciones.ExportSettings.FileName = filename;
        rgInstrucciones.ExportSettings.IgnorePaging = true;
        rgInstrucciones.ExportSettings.OpenInNewWindow = false;
        rgInstrucciones.ExportSettings.ExportOnlyData = true;
    }

    protected void btnVerInstruccion_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Response.Redirect("Instrucciones.aspx?IdInstruccion=VER");
        }
        catch { }
    }

    protected void conectarPN()
    {
        if (logAppPN == null)
            logAppPN = new GrupoLis.Login.Login();

        if (!logAppPN.conectado)
        {
            logAppPN.tipoConexion = TipoConexion.SQL_SERVER;
            logAppPN.SERVIDOR = mParamethers.Get("Servidor");
            logAppPN.DATABASE = mParamethers.Get("DatabasePN"); ;
            logAppPN.USER = mParamethers.Get("User");
            logAppPN.PASSWD = mParamethers.Get("Password");
            logAppPN.conectar();
        }
    }

    protected void desconectarPN()
    {
        if (logAppPN.conectado)
            logAppPN.desconectar();
    }

}