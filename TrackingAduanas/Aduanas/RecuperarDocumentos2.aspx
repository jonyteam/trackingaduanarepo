﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="RecuperarDocumentos2.aspx.cs" Inherits="CargarArchivo" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .wrap {
            white-space: normal;
            width: 98px;
        }

        .style1 {
            width: 100px;
        }
        .auto-style1 {}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest"
        ClientEvents-OnRequestStart="requestStart">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgIngresos">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgIngresos" LoadingPanelID="LoadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgIngresos" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" EnableSkinTransparency="true">
        <div class="loading">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Ajax/Img/loading1.gif" AlternateText="loading"></asp:Image>
        </div>
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            //<![CDATA[
            function requestStart(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
            }

            function OnClientClose() {
                $find("<%= RadAjaxManager1.ClientID%>").ajaxRequest("Rebind");
            }
        </script>
    </telerik:RadScriptBlock>
    <telerik:RadMultiPage ID="mpRequerimiento" runat="server" SelectedIndex="0">
        <telerik:RadPageView ID="RadPageView1" runat="server" Width="99.9%">
            <table width="100%">
                <tr>
                    <td>Ingrese Fecha pago Facturacion:
                        <telerik:RadDatePicker ID="RdFechaTesoreria" Runat="server">
                        </telerik:RadDatePicker>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        </asp:UpdatePanel>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:UpdatePanel ID="UpdatePanel" runat="server">
                                    <ContentTemplate>
                                        <table class="auto-style117">
                                            <tr>
                                                <td class="auto-style123">Hojas de Ruta Selecionadas</td>
                                                <td class="auto-style123">Agregar gastos</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <telerik:RadListBox ID="RadListBox1" runat="server" AllowDelete="True" Culture="es-ES" Font-Size="X-Small" Height="130px" SelectionMode="Multiple" style="top: 0px; left: 1px">
                                                        <ButtonSettings TransferButtons="All" />
                                                    </telerik:RadListBox>
                                                </td>
                                                <td>
                                                    <telerik:RadGrid ID="rgIngresos" runat="server" AllowFilteringByColumn="True" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" Culture="es-ES" GridLines="None" Height="420px" OnInit="rgIngresos_Init" OnItemCommand="rgIngresos_ItemCommand" OnNeedDataSource="rgIngresos_NeedDataSource" PageSize="1000" ShowFooter="True" ShowStatusBar="True" Width="99.9%">
                                                        <PagerStyle Mode="NextPrevAndNumeric" NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente" PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}." PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                                                        <mastertableview commanditemdisplay="Top" datakeynames="" grouploadmode="Client" nodetailrecordstext="No hay registros." nomasterrecordstext="No hay ingresos.">
                                                            <CommandItemSettings RefreshText="Volver a Cargar Datos" ShowAddNewRecordButton="false" ShowExportToCsvButton="false" ShowExportToExcelButton="true" />
                                                            <rowindicatorcolumn>
                                                                <HeaderStyle Width="20px" />
                                                            </rowindicatorcolumn>
                                                            <expandcollapsecolumn>
                                                                <HeaderStyle Width="20px" />
                                                            </expandcollapsecolumn>
                                                            <Columns>
                                                                <%--<telerik:GridBoundColumn DataField="ArchivosCargardos" HeaderText="Archivos Cargardos" UniqueName="ArchivosCargardos"
                                        FilterControlWidth="80%">
                                        <HeaderStyle Width="130px" />
                                    </telerik:GridBoundColumn>--%>
                                                                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Tiempo" HeaderText="Tiempo" ImageUrl="Images/16/lock2_16.png" UniqueName="Tiempo">
                                                                    <HeaderStyle Width="75px" />
                                                                </telerik:GridButtonColumn>
                                                                <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" HeaderText="Agrupar" UniqueName="TemplateColumn">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chbAgrupar" runat="server" Text="Agrupar" />
                                                                    </ItemTemplate>
                                                                </telerik:GridTemplateColumn>
                                                                <telerik:GridBoundColumn DataField="HojaRuta" FilterControlWidth="80%" HeaderText="Instrucción No." UniqueName="IdInstruccion">
                                                                    <ItemStyle Width="10%" />
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="Aduana" FilterControlAltText="Filter Aduana column" HeaderText="Aduana" UniqueName="Aduana">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="Proveedor" FilterControlAltText="Filter Proveedor column" HeaderText="Proveedor" UniqueName="Proveedor">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="Material" FilterControlAltText="Filter Material column" HeaderText="Material" UniqueName="Material">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="Clientes" FilterControlWidth="80%" HeaderText="Cliente" UniqueName="Cliente">
                                                                    <ItemStyle Width="10%" />
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="Id" Display="False" FilterControlAltText="Filter Id column" HeaderText="IdSolicitud" UniqueName="Id">
                                                                </telerik:GridBoundColumn>
                                                                <telerik:GridBoundColumn DataField="IdTramite" Display="False" FilterControlAltText="Filter IdTramite column" HeaderText="IdTramite" UniqueName="IdTramite">
                                                                </telerik:GridBoundColumn>
                                                            </Columns>
                                                            <editformsettings>
                                                                <editcolumn filtercontrolalttext="Filter EditCommandColumn column">
                                                                </editcolumn>
                                                            </editformsettings>
                                                        </mastertableview>
                                                        <HeaderStyle Width="180px" />
                                                        <clientsettings allowcolumnsreorder="True" reordercolumnsonclient="True">
                                                            <Selecting AllowRowSelect="True" />
                                                            <ClientMessages DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño" DropHereToReorder="Suelte aquí para Re-Ordenar" PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas" />
                                                            <Scrolling AllowScroll="True" SaveScrollPosition="True" UseStaticHeaders="True" />
                                                        </clientsettings>
                                                        <filtermenu enabletheming="True">
                                                            <CollapseAnimation Duration="200" Type="OutQuint" />
                                                        </filtermenu>
                                                        <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                                                        <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
                                                    </telerik:RadGrid>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
            <telerik:RadWindowManager ID="RadWindowManager1" runat="server"></telerik:RadWindowManager>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <%--<telerik:GridBoundColumn DataField="ArchivosCargardos" HeaderText="Archivos Cargardos" UniqueName="ArchivosCargardos"
                                        FilterControlWidth="80%">
                                        <HeaderStyle Width="130px" />
                                    </telerik:GridBoundColumn>--%>
    
</asp:Content>
