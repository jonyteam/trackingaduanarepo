﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NumerodeSAP.aspx.cs" Inherits="SharePointUpload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        div.hide {
            visibility: hidden;
        }

        div.views {
            visibility: visible;
        }

        #centro {
            vertical-align: central;
            text-align: center;
        }
        .style3
        {
            text-align: center;
        }
        .style4
        {
            width: 74%;
        }
    </style>
</head>
<body>
    <form id="form2" runat="server" aria-autocomplete="both">
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server"></telerik:RadScriptManager>
        <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server"></telerik:RadStyleSheetManager>
        <telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" DecoratedControls="All" Skin="Metro" />
        <div align="center" style="padding-top: 20px">
            <table style="width: 100%; height: auto">
                <tr>
                    <td align="right" style="text-align: center" width="100%">
                        <asp:Panel ID="PanelUploadFile" runat="server" Width="600px" Font-Names="Arial" 
                            BorderStyle="Inset" BorderWidth="8px" Height="300px" HorizontalAlign="Left" 
                            BorderColor="#25A0DA">
                            <table align="left" style="height: 387px" width="100%">
                                <tr>
                                    <td>
                                        <div id="Encabezado">
                                            <table style="width: 100%; height: 186px;">
                                                <tr>
                                                    <td colspan="2" style="text-align: center">
                                                        <asp:Label ID="LabelTitulo" runat="server" Text="Identificador:" Font-Bold="True"></asp:Label>
                                                        <asp:Label ID="LabelTituloSistema" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="Blue"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style3">
                                                        &nbsp;</td>
                                                    <td class="style4">
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td class="style3">
                                                        Numero SAP:</td>
                                                    <td class="style4">
                                                        <telerik:RadTextBox ID="txtrecibo" Runat="server" Width="279px">
                                                        </telerik:RadTextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style3" colspan="2">
                                                        <telerik:RadButton ID="btnaceptar" runat="server" onclick="RadButton1_Click" 
                                                            Text="Aceptar">
                                                        </telerik:RadButton>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </div>

        <script type="text/javascript">
            function onRequestStart(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                            args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                            args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
            }
            var message = "Grupo Vesta, Derechos Reservados";

            function click(e) {
                if (document.all) {
                    if (event.button == 2) {
                        alert(message);
                        return false;
                    }
                }
                if (document.layers) {
                    if (e.which == 3) {
                        alert(message);
                        return false;
                    }
                }
            }
            if (document.layers) {
                document.captureEvents(Event.MOUSEDOWN);
            }
            document.onmousedown = click;

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function Close() {
                var oWindow = GetRadWindow();
                oWindow.argument = null;
                oWindow.close();
            }
            </script>
    </form>
</body>
</html>
