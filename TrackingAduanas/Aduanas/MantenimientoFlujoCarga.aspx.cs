﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class MantenimientoFlujoCarga : Utilidades.PaginaBase
{

    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Mantenimiento Flujo Carga";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgEstadosFlujo.FilterMenu);
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Mantenimiento Flujo Carga", "Flujo Carga");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void SetVisible(TableCellCollection cells, String commandName, bool visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is ImageButton)
                {
                    ImageButton btn = (ImageButton)control;

                    if (btn.CommandName == commandName)
                        btn.Visible = visible;
                }
            }
        }
    }

    protected Object GetEstadosFlujoCarga()
    {
        try
        {
            conectar();
            CodigosBO bo = new CodigosBO(logApp);
            bo.loadAllCampos("ESTADOSFLUJOCARGA");
            return bo.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }
        return null;
    }

    protected Object GetPaises()
    {
        try
        {
            conectar();
            CodigosBO bo = new CodigosBO(logApp);
            bo.loadPaisesHojaRuta();
            return bo.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }
        return null;
    }

    protected Object GetDecision()
    {
        try
        {
            conectar();
            CodigosBO bo = new CodigosBO(logApp);
            bo.loadAllCampos("DECISION");
            return bo.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }
        return null;
    }

    protected void rgEstadosFlujo_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridEstadosFlujo();
    }

    private void llenarGridEstadosFlujo()
    {
        try
        {
            conectar();
            FlujoCargaBO fc = new FlujoCargaBO(logApp);
            fc.loadFlujoCargaAll();
            rgEstadosFlujo.DataSource = fc.TABLA;
            rgEstadosFlujo.DataBind();
        }
        catch (Exception) { }
        finally
        {
            desconectar();
        }
    }

    protected void GetDatosEditados(FlujoCargaBO bo, GridItem item)
    {
        GridEditableItem editedItem = item as GridEditableItem;
        RadComboBox estadoFlujo = (RadComboBox)editedItem.FindControl("edEstadoFlujo");
        RadComboBox pais = (RadComboBox)editedItem.FindControl("edPais");
        RadComboBox regimen = (RadComboBox)editedItem.FindControl("edRegimen");
        //RadComboBox obligatorio = (RadComboBox)editedItem.FindControl("edObligatorio");
        RadNumericTextBox orden = (RadNumericTextBox)editedItem.FindControl("edOrden");
        bo.CODESTADOFLUJOCARGA = estadoFlujo.SelectedValue;
        bo.ESTADOFLUJO = estadoFlujo.SelectedItem.Text.Trim();
        bo.CODPAIS = pais.SelectedValue;
        bo.CODREGIMEN = regimen.SelectedValue;
        //bo.OBLIGATORIO = obligatorio.SelectedValue;
        bo.ORDEN = Convert.ToInt32(orden.Value);
    }

    protected void rgEstadosFlujo_InsertCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            FlujoCargaBO bo = new FlujoCargaBO(logApp);
            FlujoCargaBO fc = new FlujoCargaBO(logApp);
            string nuevoId = "";
            bo.loadFlujoCarga("-1");
            bo.newLine();

            GetDatosEditados(bo, e.Item);
            fc.loadNuevoId(bo.CODPAIS.Trim() + "-");
            nuevoId = fc.TABLA.Rows[0]["NuevoId"].ToString();
            if (nuevoId == "")
            {
                nuevoId = bo.CODPAIS.Trim() + "-1";
            }

            bo.IDESTADOFLUJO = nuevoId;
            bo.FECHA = DateTime.Now.ToString();
            bo.ELIMINADO = "0";
            bo.IDUSUARIO = Session["IdUsuario"].ToString();

            bo.commitLine();
            bo.actualizar();
            registrarMensaje("Estado de Flujo de Carga ingresado exitosamente");

            llenarBitacora("Se ingresó el estado de flujo de carga " + bo.ESTADOFLUJO, Session["IdUsuario"].ToString(), bo.ESTADOFLUJO);
            rgEstadosFlujo.Rebind();
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoGestionCarga");
        }
        finally
        {
            desconectar();
        }
    }

    protected void rgEstadosFlujo_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridEditableItem editedItem = e.Item as GridEditableItem;

            String idEstadoFlujo = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdEstadoFlujo"]);

            FlujoCargaBO fc = new FlujoCargaBO(logApp);
            FlujoCargaBO bo = new FlujoCargaBO(logApp);
            bo.loadFlujoCarga(idEstadoFlujo);
            string oldEstado = bo.ESTADOFLUJO;

            GetDatosEditados(bo, e.Item);
            fc.loadFlujoCargaXEstadoFlujoyPais(bo.ESTADOFLUJO, bo.CODPAIS);
            if (fc.totalRegistros <= 0)
            {
                bo.actualizar();
                registrarMensaje("Estado de Flujo de Carga actualizada exitosamente");

                llenarBitacora("Se modificó el estado de flujo de carga " + oldEstado + " por " + bo.ESTADOFLUJO, Session["IdUsuario"].ToString(), bo.ESTADOFLUJO);
                rgEstadosFlujo.Rebind();
            }
            else if (idEstadoFlujo == fc.IDESTADOFLUJO)
            {
                bo.actualizar();
                registrarMensaje("Estado de Flujo de Carga actualizada exitosamente");

                llenarBitacora("Se modificó la estado de flujo de carga " + oldEstado + " por " + bo.ESTADOFLUJO, Session["IdUsuario"].ToString(), bo.ESTADOFLUJO);
                rgEstadosFlujo.Rebind();
            }
            else
                registrarMensaje("Ya existe el estado de flujo de carga " + fc.ESTADOFLUJO + " para este país con este mismo nombre");
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoGestionCarga");
        }
        finally
        {
            desconectar();
        }
    }

    protected void rgEstadosFlujo_DeleteCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridDataItem editedItem = e.Item as GridDataItem;

            String idEstadoFlujo = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdEstadoFlujo"]);
            FlujoCargaBO bo = new FlujoCargaBO(logApp);
            bo.loadFlujoCarga(idEstadoFlujo);
            bo.ELIMINADO = "1";
            bo.actualizar();
            registrarMensaje("Estado de Flujo de Carga eliminado exitosamente");

            llenarBitacora("Se elimino el estado de flujo de carga " + bo.ESTADOFLUJO, Session["IdUsuario"].ToString(), bo.ESTADOFLUJO);
            rgEstadosFlujo.Rebind();
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoGestionCarga");
        }
        finally
        {
            desconectar();
        }

    }

    protected void rgEstadosFlujo_Init(object sender, EventArgs e)
    {
        GridFilterMenu menu = rgEstadosFlujo.FilterMenu;
        menu.Items.RemoveAt(rgEstadosFlujo.FilterMenu.Items.Count - 2);
    }

    #region Regimenes
    public RadComboBox edPais;
    public RadComboBox edRegimen;
    protected void rgEstadosFlujo_ItemDataBound(object sender, GridItemEventArgs e)
    {
        try
        {
            if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
            {
                conectar();
                CodigosBO co = new CodigosBO(logApp);
                GridEditFormItem editForm = e.Item as GridEditFormItem;
                edRegimen = editForm.FindControl("edRegimen") as RadComboBox;

                co.loadAllCampos("REGIMEN" + edPais.SelectedItem.Text.Trim());
                edRegimen.DataSource = co.TABLA;
                edRegimen.DataBind();

                String codRegimen = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["CodRegimen"]);
                edRegimen.SelectedValue = codRegimen;
            }
        }
        catch (Exception)
        { }
    }

    protected void rgEstadosFlujo_ItemCreated(object sender, GridItemEventArgs e)
    {
        try
        {
            if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
            {
                conectar();
                GridEditFormItem editForm = e.Item as GridEditFormItem;
                edPais = editForm.FindControl("edPais") as RadComboBox;
                edPais.AutoPostBack = true;
                edPais.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(edPais_SelectedIndexChanged);
            }
        }
        catch { }
    }

    public void edPais_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            conectar();
            GridEditFormItem editForm = (sender as RadComboBox).NamingContainer as GridEditFormItem;
            edRegimen = editForm.FindControl("edRegimen") as RadComboBox;
            CodigosBO co = new CodigosBO(logApp);
            co.loadAllCampos("REGIMEN" + edPais.SelectedItem.Text.Trim());
            edRegimen.DataSource = co.TABLA;
            edRegimen.DataBind();
        }
        catch (Exception)
        { }
    }
    #endregion

}