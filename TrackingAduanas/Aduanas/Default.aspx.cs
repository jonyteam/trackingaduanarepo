﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using Telerik.Web.UI;

public partial class _Default : Utilidades.PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (IsIe6)
            {// The ".gif" files will be loaded for IE6 browser. The size of the .gif files is 300x187
                RadRotator1.ItemHeight = Unit.Pixel(187);
            }
            else
            {// The ".png" files will be loaded for all other browsers. The size of the .png files is 300x220
                RadRotator1.ItemHeight = Unit.Pixel(220); //220
            }

            RadRotator1.Width = Unit.Pixel(810);//810
            RotatorWrapper.CssClass = "rotNoButtonsBack";// Change the class of the bachground panel

            RadRotator1.RotatorType = RotatorType.Carousel;//             
            List<ImageData> datasource = new List<ImageData>();
            datasource.Add(new ImageData("Images/Impresiones/VestaLogistics1.jpg"));
            datasource.Add(new ImageData("Images/Impresiones/VestaArgos.jpg"));
            datasource.Add(new ImageData("Images/Impresiones/VestaLogistics2.jpg"));
            datasource.Add(new ImageData("Images/Impresiones/VestaWarehouse1.jpg"));
            datasource.Add(new ImageData("Images/Impresiones/VestaWarehouse2.jpg"));
            datasource.Add(new ImageData("Images/Impresiones/VestaTrading1.jpg"));
            datasource.Add(new ImageData("Images/Impresiones/VestaTrading3.jpg"));
            RadRotator1.DataSource = datasource;
            RadRotator1.DataBind();
        }
    }

    public class ImageData
    {
        private String _imageUrl;

        public ImageData(String imageurl)
        {
            _imageUrl = imageurl;
        }

        public String ImageUrl { get { return _imageUrl; } }
    }
    
    
    public override bool CanGoBack { get { return false; } }

    private bool IsIe6
    {
        get
        {
            bool ie6 = Context.Request.Browser.IsBrowser("IE") && Context.Request.Browser.MajorVersion < 7;// Is IE 6
            return ie6;
        }
    }

}
