﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CambioFecha.aspx.cs" Inherits="CambioFecha" MasterPageFile="~/Site.master"%>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="Server">
    


    <table>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Hoja de Ruta: "></asp:Label>            
            </td>
            <td>
                <telerik:RadTextBox runat="server" ID="txtHojaRuta"></telerik:RadTextBox>
            </td>
            <td>
                <telerik:RadButton ID="btnBuscar" runat="server" Text="Buscar Instruccion" OnClick="btnBuscar_OnClick" Width="108px"/>
            </td>
        </tr>
        
        <tr>
            <td>
                <asp:Label runat="server" ID="lblEstado" Text="Opcion" Visible="False"></asp:Label>
            </td>
            <td>
                <telerik:RadComboBox ID="rcbEstado" runat="server" EmptyMessage="Seleccione una opcion..." Visible="False"></telerik:RadComboBox>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <telerik:RadButton runat="server" ID="btnUpdate" Visible="False" Text="Actualizar" OnClick="btnUpdate_OnClick" style="width: 100%"/>
            </td>
        </tr>
    </table>
    <br />
</asp:Content>


