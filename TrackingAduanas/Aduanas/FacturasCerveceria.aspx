﻿<%@ Page AutoEventWireup="true" CodeFile="FacturasCerveceria.aspx.cs" Inherits="FacturasCerveceria" Language="C#" MasterPageFile="~/Site.master" Title="" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanelFlujoGuias" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 | args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }                   
            </script>
        </telerik:RadScriptBlock>
        <table id="Table1" runat="server" style="width: 50%">
            <tr>
                <td style="width: 5%">
                    <asp:Label ID="lblFechaInicio" runat="server" Text="Fecha Inicio:" ForeColor="Black"></asp:Label>
                </td>
                <td style="width: 15%">
                    <telerik:RadDatePicker ID="dpFechaInicio" runat="server" EnableTyping="False" Width="50%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="dpFechaInicio"
                        ErrorMessage="Ingrese Fecha" ForeColor="Red" />
                </td>
            </tr>
            <tr>
                <td style="width: 5%">
                    <asp:Label ID="lblFechaFinal" runat="server" Text="Fecha Final:" ForeColor="Black"></asp:Label>
                </td>
                <td style="width: 15%">
                     <telerik:RadDatePicker ID="dpFechaFinal" runat="server" EnableTyping="False" Width="50%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="dpFechaInicio"
                        ErrorMessage="Ingrese Fecha" ForeColor="Red" />
                </td>
            </tr>
            <tr>
                <td style="width: 5%">
                    <asp:Label ID="lblCliente" Font-Bold="true" runat="server" Text="Cliente:"></asp:Label>
                </td>
                <td style="width: 25%">
                    <telerik:RadComboBox runat="server" ID="cmbClientes" DataTextField="Nombre" DataValueField="CodigoSAP" Enabled="False" Width="50%">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td colspan="6" align="center">
                    <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Images/24/view_24.png"
                        OnClick="btnBuscar_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="rgInstrucciones" runat="server" AllowFilteringByColumn="True"
            AllowSorting="True" AutoGenerateColumns="False" GridLines="None" Height="600px"
            AllowPaging="True" ShowFooter="True" Visible="False"
            ShowStatusBar="True" PageSize="20" OnNeedDataSource="rgInstrucciones_OnNeedDataSource" OnItemCommand="rgInstrucciones_OnItemCommand" CellSpacing="0">
            <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
            <MasterTableView
                CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay instrucciones."
                GroupLoadMode="Client">
                <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                    ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px"></HeaderStyle>
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px"></HeaderStyle>
                </ExpandCollapseColumn>
               <Columns>
                    <telerik:GridBoundColumn DataField="IdInstruccion" HeaderText="Instruccion No." UniqueName="IdInstruccion"
                        FilterControlWidth="60%" AllowFiltering="True">
                        <HeaderStyle Width="200px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Regimen" HeaderText="Régimen" UniqueName="Regimen"
                        FilterControlWidth="60%" AllowFiltering="True">
                        <HeaderStyle Width="300px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Cliente" HeaderText="Cliente"
                        UniqueName="Cliente" FilterControlWidth="60%" AllowFiltering="True">
                        <ItemStyle Width="90px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Producto" HeaderText="Producto" UniqueName="Producto"
                        FilterControlWidth="60%" AllowFiltering="True">
                        <ItemStyle Width="80px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Pedido Cliente" AllowFiltering="false" ShowFilterIcon="false">
                        <ItemTemplate>
                            <telerik:RadTextBox runat="server" ID="TxtPedidoCliente" Width="90%"></telerik:RadTextBox>
                        </ItemTemplate>
                        <HeaderStyle Width="170px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Fecha Entrega Factura" AllowFiltering="false" ShowFilterIcon="false">
                        <ItemTemplate>
                            <telerik:RadDateTimePicker runat="server" ID="dtFechaFactura"></telerik:RadDateTimePicker>
                        </ItemTemplate>
                        <HeaderStyle Width="180px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Seleccionar" AllowFiltering="False" 
                        ShowFilterIcon="False">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSeleccionar" runat="server"/>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                </Columns>
            </MasterTableView>
            <HeaderStyle Width="180px" />
            <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                <Selecting AllowRowSelect="True" />
                <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                    DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                    DropHereToReorder="Suelte aquí para Re-Ordenar" />
                <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
            </ClientSettings>
            <FilterMenu EnableTheming="True">
                <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
            </FilterMenu>
            <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
            <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
        </telerik:RadGrid>
        <table class="style5" style="margin-left: 550px; height: 36px;">
            <tr>
                <td style="align-items: center">        
                    <asp:ImageButton ID="btnGuardar" runat="server" OnClick="btnGuardar_OnClick" ImageUrl="~/Images/24/disk_blue_ok_24.png"                      
                        ToolTip="Salvar" Visible="False" Height="29px" Width="38px" />
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
</asp:Content>


 