﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Logical;
using Telerik.Web.UI;
using Toolkit.Core.Extensions;
using Utilities.Web;
using  System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;

public partial class ReporteGeneral : Utilidades.PaginaBase
{

    private GrupoLis.Login.Login logAppAduanas;
    private ReporteGeneralRepository _rgr = new ReporteGeneralRepository();
    private  List<string> clientes;


    protected override void OnLoad(EventArgs e)
    {
        MODULO = "ReporteHagamodaPromoda";
        base.OnLoad(e);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        DetailWindow.Windows.Clear();
        SetGridFilterMenu(rgReporteGeneral.FilterMenu);
        rgReporteGeneral.FilterItemStyle.Font.Size = FontUnit.XSmall;
        rgReporteGeneral.FilterMenu.Font.Size = FontUnit.XXSmall;
        /*cmbCliente.AllowCustomText = CheckBoxAllowCustomText.Checked;
        cmbCliente.MarkFirstMatch = CheckBoxMarkFirstMatch.Checked;
        cmbCliente.AutoCompleteSeparator = RadDDLAutocompleteSeparator.SelectedValue;*/

        cmbCliente.Filter = RadComboBoxFilter.StartsWith;

        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Instrucciones Finalizadas", "Reporte Instrucciones Finalizadas.");
            //if(!(Ingresar))
            //    redirectTo("Principal.aspx");
            rgReporteGeneral.Visible = false;
            LlenarObjetos();
            cmbTipoAduana.SelectedValue = "0";
            cmbFecha.SelectedValue = "1";
            rdSemanaInicio.Calendar.FirstDayOfWeek = FirstDayOfWeek.Monday;
            rdSemanaFin.Calendar.FirstDayOfWeek = FirstDayOfWeek.Monday;
        }
    }

    protected bool Consultar { get { return tienePermiso("Consultar"); } }
    public override bool CanGoBack { get { return false; } }

    private void LlenarObjetos()
    {
        try
        {
            conectar();
            ClientesBO cl = new ClientesBO(logApp);
            cl.loadAllCamposClientesXPais("H");
            cmbCliente.DataSource = cl.TABLA;
            cmbCliente.DataBind();

        }
        catch (Exception){throw;}
        finally { desconectar();}
    }

    //protected void AjaxPanel1_OnAjaxRequest(object sender, AjaxRequestEventArgs e)
    //{
    //    if (e.Argument != "Rebind")
    //        return;
    //    //rgReporteGeneral.Rebind();
    //    Timer1.Enabled = true;
    //}

    protected void btnGenerarReporte_OnClick(object sender, ImageClickEventArgs e)
    {

        

        if (cmbTipoAduana.SelectedValue != "0")
        {
            List<ListItem> selected = chkList.Items.Cast<ListItem>()
            .Where(li => li.Selected)
            .ToList();

            if (selected.Count <= 2)
            {
                LlenarGrid();
            }
            else
            {
                registrarMensaje("Solo puede seleccionar dos objetos en la busqueda Por.");
            }
        }
        else
        {
            registrarMensaje("Debe seleccionar un tipo de Aduana.");
        }
        
    }

    public void LlenarGrid()
    {
        
        DataTable dt = new DataTable();
        

        try
        {
                clientes = new List<string>();
                var aduanaAereasTerrestres = new List<string>() {"014", "017", "020", "018", "021", "019", "015", "022"};
                var aduanasMaritimas = new List<string>() {"016", "024", "023"};
                
                var chbClientes = cmbCliente.CheckedItems;


                foreach (var comboBox in chbClientes)
                {
                    if (comboBox.Checked)
                    {
                        clientes.Add(comboBox.Value);
                    }
                }
                if (clientes.Count >= 0)
                {
                    if (cmbFecha.SelectedValue == "1")
                    {

                        if (rdFechaInicio.SelectedDate != null && rdFechaFin.SelectedDate != null)
                        {
                            if (rdFechaInicio.SelectedDate < rdFechaFin.SelectedDate)
                            {
                                if (cmbTipoAduana.SelectedValue == "1")
                                {
                                    var registros = _rgr.GetResumen(clientes, aduanaAereasTerrestres,
                                        rdFechaInicio.SelectedDate.Value,
                                        rdFechaFin.SelectedDate.Value);

                                    rgReporteGeneral.DataSource = registros;
                                }
                                else
                                {
                                    var registros = _rgr.GetResumen(clientes, aduanasMaritimas,
                                        rdFechaInicio.SelectedDate.Value,
                                        rdFechaFin.SelectedDate.Value);
                                    rgReporteGeneral.DataSource = registros;
                                }
                            }
                            else
                            {
                                registrarMensaje("La Fecha de Inicio no debe ser Mayor o igual a la Fecha Fin");
                            }
                        }
                    }
                    else if (cmbFecha.SelectedValue == "2")
                    {
                        if (rdSemanaInicio.SelectedDate != null && rdSemanaFin.SelectedDate != null)
                        {
                            var semanaInicio = GetSemana(rdSemanaInicio.SelectedDate.Value, true);
                            var semanaFin = GetSemana(rdSemanaFin.SelectedDate.Value, false);

                            if (rdSemanaInicio.SelectedDate < rdSemanaFin.SelectedDate)
                            {
                                if (cmbTipoAduana.SelectedValue == "1")
                                {
                                    //string resultado = fecha.ToString("ddMMyyyy");
                                    var respuesta = _rgr.GetResumen(clientes, aduanaAereasTerrestres, semanaInicio,
                                        semanaFin);
                                    rgReporteGeneral.DataSource = respuesta;
                                }
                                else
                                {
                                    var respuesta = _rgr.GetResumen(clientes, aduanasMaritimas, semanaInicio,
                                        semanaFin);
                                    rgReporteGeneral.DataSource = respuesta;
                                }
                            }
                            else
                            {
                                registrarMensaje("La Semana de Inicio no debe ser Mayor o igual a la Semana Fin");
                            }
                        }
                    }
                    else if (cmbFecha.SelectedValue == "3")
                    {
                        var mesFin = GetMonth(2);
                        var mesInicio = GetMonth(1);
                        if (mesInicio != null && mesFin != null)
                        {
                            //rdMesInicio.SelectedDate.Value,rdMesFin.SelectedDate.Value
                            //var time = DateTime.Now.Day;
                            //var sumMes = rdMesFin.SelectedDate.Value.AddMonths(1);

                            if (mesInicio < mesFin)
                            {
                                if (cmbTipoAduana.SelectedValue == "1")
                                {
                                    var resp = _rgr.GetResumen(clientes, aduanaAereasTerrestres,
                                        mesInicio, mesFin);
                                    rgReporteGeneral.DataSource = resp;
                                }
                                else
                                {
                                    var resp = _rgr.GetResumen(clientes, aduanasMaritimas, mesInicio,
                                        mesFin);
                                    rgReporteGeneral.DataSource = resp;
                                }
                            }
                            else
                            {
                                registrarMensaje("El Mes de Inicio no debe ser Mayor o igual al Mes Fin");
                            }
                        }
                    }
                    rgReporteGeneral.DataBind();
                    rgReporteGeneral.Visible = true;
                }
                else
                {
                    registrarMensaje("Debe de seleccionar un cliente.");
                }
        }
        catch (Exception){throw;}
        
    }

    protected void rgReporteGeneral_OnItemCommand(object sender, GridCommandEventArgs e)
    {
        GridEditableItem editedItem = e.Item as GridEditableItem;
        string idAduana = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Key"]);
        string cliente = "";


        if (cmbCliente.CheckedItems.Count > 1)
        {
            foreach (var item in cmbCliente.CheckedItems)
            {
                cliente += "'" + item.Value + "',";
            }
            cliente = cliente.TrimEnd(',');
        }
        else
        {
            cliente = "'"+ cmbCliente.SelectedValue +"'";
        }


        if (e.CommandName == "Export")
        {
            AbrirVentana(idAduana, "E", cliente);
        }else if (e.CommandName == "Import")
        {
            AbrirVentana(idAduana, "I", cliente);
        }else if (e.CommandName == "Trans")
        {
            AbrirVentana(idAduana, "Tr", cliente);
        }
    }

    private void AbrirVentana(string idAduana, string regimen, string cliente)
    {
        string urlBase = "ReporteGeneralDetalle.aspx";
        string fechaI = "";
        string fechaF = "";
        string seleccion = "";
        int tipoAduana = cmbTipoAduana.SelectedValue.ToInt();

        if (cmbFecha.SelectedValue == "1")
        {
            fechaI = rdFechaInicio.SelectedDate.Value.ToString("yyyy-MM-dd");
            fechaF = rdFechaFin.SelectedDate.Value.ToString("yyyy-MM-dd");
        }
        else if(cmbFecha.SelectedValue == "2")
        {
            fechaI = rdSemanaInicio.SelectedDate.Value.ToString("yyyy-MM-dd");
            fechaF = rdSemanaFin.SelectedDate.Value.ToString("yyyy-MM-dd");
        }else if (cmbFecha.SelectedValue == "3")
        {

            fechaI = GetMonth(1).ToString("yyyy-MM-dd");
            fechaF = GetMonth(2).ToString("yyyy-MM-dd");
        }
        
        foreach (ListItem item in chkList.Items)
        {
            if (!item.Selected) continue;

            seleccion += item.Value + ",";
        }

        urlBase += "?idaduana=" + idAduana+ "&regimen=" + regimen + "&fI=" + fechaI + "&fF=" + fechaF +
                   "&busqueda="+ tipoAduana + "&clientes=" + cliente + "&seleccion=" + seleccion;
        var window1 = new RadWindow
        {
            NavigateUrl = urlBase,
            VisibleOnPageLoad = true,
            ID = "Estados",
            Width = 1300,
            Height = 500,
            Animation = WindowAnimation.FlyIn,
            DestroyOnClose = true,
            VisibleStatusbar = false,
            Behaviors = WindowBehaviors.Close,
            Modal = true,
            OnClientClose = "OnClientClose"
        };
        DetailWindow.Windows.Add(window1);
    }

    protected void Timer1_OnTick(object sender, EventArgs e)
    {
        //rgReporteGeneral.Rebind();
    }

    private DateTime GetSemana(DateTime semana, bool inicio)
    {

        var dia = semana.DayOfWeek;
        if (inicio)
        {
            switch (dia)
            {
                case DayOfWeek.Monday:
                    break;
                case DayOfWeek.Tuesday:
                    semana = semana.AddDays(-1);
                    break;
                case DayOfWeek.Wednesday:
                    semana = semana.AddDays(-2);
                    break;
                case DayOfWeek.Thursday:
                    semana = semana.AddDays(-3);
                    break;
                case DayOfWeek.Friday:
                    semana = semana.AddDays(-4);
                    break;
                case DayOfWeek.Saturday:
                    semana = semana.AddDays(-5);
                    break;
                case DayOfWeek.Sunday:
                    semana = semana.AddDays(-6);
                    break;
            }
        }
        else
        {
            switch (dia)
            {
                case DayOfWeek.Monday:
                    semana = semana.AddDays(6);
                    break;
                case DayOfWeek.Tuesday:
                    semana = semana.AddDays(5);
                    break;
                case DayOfWeek.Wednesday:
                    semana = semana.AddDays(4);
                    break;
                case DayOfWeek.Thursday:
                    semana = semana.AddDays(3);
                    break;
                case DayOfWeek.Friday:
                    semana = semana.AddDays(2);
                    break;
                case DayOfWeek.Saturday:
                    semana = semana.AddDays(1);
                    break;
                case DayOfWeek.Sunday:
                    break;
            }
        }
        
        return semana;
    }

    private DateTime GetMonth( int tipo)
    {
        var time = DateTime.Now.Day;
        DateTime mes = new DateTime();
        
        if (tipo == 1)
        {
            mes = rdMesInicio.SelectedDate.Value.AddDays(-(time - 1));
        }
        else if (tipo == 2)
        {
            var sumMes = rdMesFin.SelectedDate.Value.AddMonths(1);
            mes = sumMes.AddDays(-time);
        }

        return mes;
    }
    protected void cmbFecha_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        if (cmbFecha.SelectedValue == "1")
        {
            rdFechaFin.Visible = true;
            rdFechaInicio.Visible = true;
            rdSemanaInicio.Visible = false;
            rdSemanaFin.Visible = false;
            rdMesInicio.Visible = false;
            rdMesFin.Visible = false;
        }
        else if (cmbFecha.SelectedValue == "2")
        {
            rdMesInicio.Visible = false;
            rdMesFin.Visible = false;
            rdFechaFin.Visible = false;
            rdFechaInicio.Visible = false;
            rdSemanaInicio.Visible = true;
            rdSemanaFin.Visible = true;
        }
        else if (cmbFecha.SelectedValue == "3")
        {
            rdFechaFin.Visible = false;
            rdFechaInicio.Visible = false;
            rdSemanaInicio.Visible = false;
            rdSemanaFin.Visible = false;
            rdMesInicio.Visible = true;
            rdMesFin.Visible = true;
        }
    }
}