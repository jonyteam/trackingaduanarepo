﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;

public partial class EventosFin : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "EventosFin";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgEventos.FilterMenu);
        rgEventos.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgEventos.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            if (!Ingresar)
                redirectTo("Default.aspx");
            rgEventos.ClientSettings.Scrolling.AllowScroll = true;
            rgEventos.ClientSettings.Scrolling.UseStaticHeaders = true;
            rgEventos.ShowHeader = true;
            rgEventos.ShowFooter = true;
            ((SiteMaster)Master).SetTitulo("Finalizar Eventos", "Finalizar Eventos");
        }
    }

    protected Object GetEventos()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            c.loadAllCampos("EVENTOS");
            return c.TABLA;
        }
        catch(Exception ex) { }
        finally
        {
            desconectar();
        }

        return null;
    }

    protected void rgEventos_Init(object sender, System.EventArgs e)
    {
        GridFilterMenu menu = rgEventos.FilterMenu;
        //menu.Items.RemoveAt(rgEventos.FilterMenu.Items.Count - 2);
    }

    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }

    protected void rgEventos_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridEventos();
    }

    private void llenarGridEventos()
    {
        try
        {
            conectar();
            EventosBO e = new EventosBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            if (User.IsInRole("Administradores"))
                e.loadEventos();
            else if (User.IsInRole("Administrador Pais")||User.IsInRole("Jefe Centralización"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                e.loadEventosXPais(u.CODPAIS);
            }
            else if (User.IsInRole("Operaciones") || User.IsInRole("Jefes Aduana") || User.IsInRole("Oficial de Cuenta") || User.IsInRole("Aforadores") || User.IsInRole("Inplant") || User.IsInRole("Analista de Documentos"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.CODPAIS == "N")
                {
                    e.loadEventosXPaisUsuarioNI(u.CODPAIS);
                }
                else
                {
                    if (User.IsInRole("Oficial de Cuenta") && u.CODPAIS == "H")
                    {
                        e.loadEventosXPaisUsuarioH(u.CODPAIS);
                    }
                    else
                    {
                    e.loadEventosXPaisUsuarioNI(u.CODPAIS);
                    }
                }
            }
            else if (User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT"))
            {
                string idU = Session["IdUsuario"].ToString();
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.totalRegistros > 0)
                    if (u.CODPAIS == "H")
                          if (User.Identity.Name == "eizaguirre")
                          { e.loadInstruccionesXAduanaIdU("016", idU); }
                          else
                          { e.loadInstruccionesXAduanaIdU(u.CODIGOADUANA, idU); }
                        
                    else
                        //e.loadInstruccionesXIdU(idU);
                        e.loadEventosXPais(u.CODPAIS);
            }
            rgEventos.DataSource = e.TABLA;
            rgEventos.DataBind();
        }
        catch (Exception ex) { }
        finally
        {
            desconectar();
        }
    }

    protected void rgEventos_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            GridEditableItem editedItem = e.Item as GridEditableItem;
            if (e.CommandName == "Sellar")
            {
                edInstruccion.Value = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdInstruccion"]);
                RadComboBox evento = (RadComboBox)editedItem.FindControl("edEventos");
                RadDatePicker fecha = (RadDatePicker)editedItem.FindControl("dtFechaSellado");
                RadTimePicker hora = (RadTimePicker)editedItem.FindControl("dtHoraSellado");
                RadTextBox observacion = (RadTextBox)editedItem.FindControl("edObservacion");
                if (!evento.IsEmpty)
                {
                    edEvento.Value = evento.SelectedValue;
                    if (fecha.SelectedDate.HasValue)
                    {
                        edFecha.Value = fecha.SelectedDate.Value.ToShortDateString();
                        if (hora.SelectedDate.HasValue)
                        {
                            edHora.Value = hora.SelectedDate.Value.ToShortTimeString();
                            edObservacion.Value = observacion.Text.Trim();
                            finalizarEvento();
                            evento.ClearSelection();
                            fecha.Clear();
                            hora.Clear();
                            observacion.Text = "";
                        }
                        else
                            registrarMensaje("Hora no puede estar vacía");
                    }
                    else
                        registrarMensaje("Fecha no puede estar vacía");
                }
                else
                    registrarMensaje("Evento no puede estar vacío");
            }
            else if (e.CommandName == RadGrid.ExportToExcelCommandName)
                ConfigureExport();
        }
        catch (Exception)
        { }
    }

    private void ConfigureExport()
    {
        String filename = "Eventos iniciados " + DateTime.Now.ToShortDateString();
        rgEventos.GridLines = GridLines.Both;
        rgEventos.ExportSettings.FileName = filename;
        rgEventos.ExportSettings.IgnorePaging = true;
        rgEventos.ExportSettings.OpenInNewWindow = false;
        rgEventos.ExportSettings.ExportOnlyData = true;
    }

    private void finalizarEvento()
    {
        try
        {
            conectar();
            EventosBO e = new EventosBO(logApp);
            InstruccionesBO i = new InstruccionesBO(logApp);
            e.loadEventosXInstruccionEvento(edInstruccion.Value, edEvento.Value);
            if (e.totalRegistros > 0)
            {
                DateTime fechaInicio = Convert.ToDateTime(e.FECHAINICIO.Trim());
                DateTime fechaFin = Convert.ToDateTime(edFecha.Value + " " + edHora.Value);
                if (fechaInicio < fechaFin)
                {
                    e.FECHAFIN = edFecha.Value + " " + edHora.Value;
                    e.FECHAFININGRESO = DateTime.Now.ToString();
                    e.OBSERVACIONFIN = edObservacion.Value;
                    e.IDUSUARIOFIN = Session["IdUsuario"].ToString();
                    e.ELIMINADO = "1"; //evento finalizado
                    e.actualizar();
                    registrarMensaje("Evento finalizado exitosamente");

                    i.loadInstruccionXArribo(edInstruccion.Value);

                    if (i.totalRegistros > 0 & edEvento.Value == "1")
                    {
                        i.ARRIBOCARGA = "1";
                        i.actualizar();
                    }
                
                    rgEventos.Rebind();

                }
                else
                    registrarMensaje("La fecha final debe ser mayor que la fecha inicial");
            }
            else
                registrarMensaje("Contactar al encargado del sistema...Quizás el evento ya esta finalizado");
        }
        catch (Exception)
        { }
        finally
        {
            desconectar();
        }
    }

    protected void btnLimpiar_Click(object sender, ImageClickEventArgs e)
    {
        limpiar();
    }

    private void limpiar()
    {
        try
        {
            foreach (GridDataItem grdItem in rgEventos.Items)
            {
                RadDatePicker dtFecha = (RadDatePicker)grdItem.FindControl("dtFechaSellado");
                RadTimePicker dtHora = (RadTimePicker)grdItem.FindControl("dtHoraSellado");
                RadTextBox edObservacion = (RadTextBox)grdItem.FindControl("edObservacion");
                dtFecha.Clear();
                dtHora.Clear();
                edObservacion.Text = "";
            }
        }
        catch (Exception)
        { }
    }
}