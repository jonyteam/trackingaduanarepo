﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Text.RegularExpressions;
using System.IO;
using Microsoft.Office.Interop;
using Toolkit.Core.Extensions;

public partial class InstruccionesCargil : Utilidades.PaginaBase
{
    protected string filename = "";
    private GrupoLis.Login.Login logAppPortal;
    private readonly AduanasDataContext _aduanaDC = new AduanasDataContext();
    private string declaracion = "";
    private bool import;

    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Instrucciones Cargill";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        filename = parameterForm1("filename", "");
        if (!IsPostBack)
        {
            //((SiteMaster)Master).SetTitulo("Instrucciones Cargill", "Instrucciones Cargill");
            //((SiteMaster)Master).SetTitulo("Instrucciones Cargill.", "Instrucciones Cargill");
            if (!(Anular || Ingresar || Modificar))
            {
                redirectTo("Inicio.aspx");
            }
            DirectoryInfo DIR = new DirectoryInfo(fileUpload.TargetPhysicalFolder);
            if (!DIR.Exists)
            {
                DIR.Create();
            }
        }
    }
    protected void SetVisible(TableCellCollection cells, String commandName, bool visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is ImageButton)
                {
                    ImageButton btn = (ImageButton)control;

                    if (btn.CommandName == commandName)
                        btn.Visible = visible;
                }
            }
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void btnCargar_Click(object sender, EventArgs e)
    {
        try
        {

            LabelMensajes.Visible = false;
            RadButtonProcesar.Enabled = false;
            if (fileUpload.UploadedFiles.Count > 0)
            {
                UploadedFile file = fileUpload.UploadedFiles[0];
                if (file.ContentLength <= fileUpload.MaxFileSize)
                {
                    if (!Object.Equals(file, null))
                    {
                        LooongMethodWhichUpdatesTheProgressContext(file);
                    }

                    repeaterResults.DataSource = fileUpload.UploadedFiles;
                    repeaterResults.DataBind();
                    repeaterResults.Visible = true;
                    System.Web.UI.WebControls.Label lbl = (System.Web.UI.WebControls.Label)repeaterResults.Controls[0].FindControl("lblTitulo");
                    filename = file.GetName();

                    file.SaveAs(fileUpload.TargetPhysicalFolder + filename, true);
                    lbl.Text = "Archivo Cargado Exitosamente: ";
                    RadButtonProcesar.Enabled = true;
                    labelNoResults.Text = "";
                }
            }
            else
            {
                if (fileUpload.InvalidFiles.Count > 0)
                {
                    repeaterResults.Visible = true;
                    repeaterResults.DataSource = fileUpload.InvalidFiles;
                    repeaterResults.DataBind();
                    System.Web.UI.WebControls.Label lbl = (System.Web.UI.WebControls.Label)repeaterResults.Controls[0].FindControl("lblTitulo");
                    lbl.Text = "Archivo Invalido:";
                }
                else
                {
                    repeaterResults.Visible = false;
                    registrarMensaje("Debe Seleccionar el archivo a cargar");
                }
                filename = "";
            }
        }
        catch (Exception ex)
        {
            btnCargar.Enabled = true;
            registrarMensaje(ex.Message);
        }
    }

    protected List<string> readExcel(bool bandera)
    {
        var campos = new List<string>();
        try
        {
            //conectarPortal();
            Microsoft.Office.Interop.Excel.Application xlApp;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            Microsoft.Office.Interop.Excel.Range range;

            string str;
            int rCnt = 0;
            int cCnt = 0;
            
            xlApp = new Microsoft.Office.Interop.Excel.ApplicationClass();
            xlWorkBook = xlApp.Workbooks.Open(@"J:\Files1\Aduanas\" + filename, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            range = xlWorkSheet.UsedRange;

            

            for (rCnt = 1; rCnt <= range.Rows.Count; rCnt++)
            {
                for (cCnt = 1; cCnt <= range.Columns.Count; cCnt++)
                {
                    int j = 0;
                    str = (range.Cells[rCnt, cCnt] as Microsoft.Office.Interop.Excel.Range).Text.ToString();
                    if (!String.IsNullOrEmpty(str))
                    {
                        campos.Add(str.Trim());
                    }
                    else
                    {
                        j++;
                        if (j == 5)
                            break;
                            j = 0;
                    }
                }
            }

            if (bandera) 
            {
                CrearHR(campos);
                xlWorkBook.Close(false, null, null);
                xlApp.Quit();

                releaseObject(xlWorkSheet);
                releaseObject(xlWorkBook);
                releaseObject(xlApp);

                LabelMensajes.Text = "Archivo procesado exitosamente...";
            }
            
        }
        catch (Exception ex)
        {
            RadButtonProcesar.Enabled = true;
            LabelMensajes.Visible = false;
            registrarMensaje("Ocurrio un error al leer el Excel, intentelo de nuevo.");
        }

        return campos;
    }

    private IEnumerable<string> LeerExcel2(Microsoft.Office.Interop.Excel.Range range, bool bandera, string regimen)
    {
        int rCnt;
        int cCnt;
        string str = "";

        var count = regimen == "I" ? 70 : 32;

        var excel = new List<string>();

        //string[] campos = new string[1];
        for (rCnt = count; rCnt <= range.Rows.Count; )
        {
            for (cCnt = 1; cCnt <= range.Columns.Count; )
            {
                str = (range.Cells[rCnt, cCnt] as Microsoft.Office.Interop.Excel.Range).Text.ToString();
                if (!String.IsNullOrEmpty(str))
                {
                    excel.Add(str.Trim());
                }
            }
        }

        #region comentado
        /*var palabras = campos[0].Split(' ');

        for (var i = 0; i <= palabras.Length; i++)
        {
            var palabra = palabras[i] + " " + palabras[i + 1] + " " + palabras[i + 2];

            if (palabra.Trim() == "Declaracion de Exportacion:")
            {
                count = 32;
                
            }
            else if (palabra.Trim() == "Declaracion de Importación:")
            {
                import = true;
                count = 70;
            }

            i += 3;
            declaracion = palabras[i + 1];
            declaracion = " " + palabras[i + 2];
        }*/
        #endregion

        return excel;
    }

    private void CrearHR(List<string> campos)
    {
        var _ici = new InstruccionCargilImport(campos);

        string docEmbarque = "";
        var declaracion = _ici.GetDeclaracion();
        var paises = _ici.GetPaises();
        var codAduana = _ici.GetCodAduana();
        var regimenInconterm = _ici.GetRegimenInconterm();
        var tipoTransporte = _ici.GetTransporte();
        var costos = _ici.Costos();
        var NoFactura = _ici.NumeroFactura();
        var refCliente = _ici.ReferenciaCliente();
        var pesoKgs = _ici.GetPeso();
        //var docTransporte = _ici.GetTransporte();

        string correlativo = "";

        var idinstruccion = GenerarInstruccion(codAduana);
        var fecha = DateTime.Now;
        var idTramite = "TR" + fecha.Year.ToString().Substring(2) + fecha.Month.ToString("00") + fecha.Day.ToString("00") + "." + fecha.Hour.ToString("00") + fecha.Minute.ToString("00") + fecha.Second.ToString("00") + "." + fecha.Millisecond.ToString("000");

        if (tipoTransporte[0] == "1")
        {
            docEmbarque = _aduanaDC.Codigos.FirstOrDefault(w => w.Categoria == "DOCUMENTOEMBARQUE" && w.Codigo1 == "50").Codigo1;
        }else if (tipoTransporte[0] == "2")
        {
            docEmbarque = _aduanaDC.Codigos.FirstOrDefault(w => w.Categoria == "DOCUMENTOEMBARQUE" && w.Codigo1 == "51").Codigo1;
        }
        else if (tipoTransporte[0] == "3")
        {
            docEmbarque = _aduanaDC.Codigos.FirstOrDefault(w => w.Categoria == "DOCUMENTOEMBARQUE" && w.Codigo1 == "52").Codigo1;
        }

        #region GuardarInstruccion
        

        var i = new Instrucciones()
        {
            IdTramite = idTramite,
            IdInstruccion = idinstruccion,
            CodPaisProcedencia = paises[0],
            CodPaisDestino = paises[1],
            CodigoAduana = codAduana,
            Incoterm = regimenInconterm.Length > 2 ? regimenInconterm[2] : regimenInconterm[1],
            NoCorrelativo = regimenInconterm.Length > 2 ? regimenInconterm[1] : null,
            CodRegimen = regimenInconterm[1],
            CodTipoTransporte = tipoTransporte[0],
            ValorFOB = Convert.ToDecimal(costos[1]),
            Flete = costos.Length > 2 ? Convert.ToDecimal(costos[2]): Convert.ToDecimal(null),
            Seguro = costos.Length > 2 ? Convert.ToDecimal(costos[3]) : Convert.ToDecimal(null),
            ValorCIF = Convert.ToDecimal(costos[0]),
            CodDocumentoEmbarque = docEmbarque,
            NumeroFactura = NoFactura,
            ReferenciaCliente = refCliente
        };
        #endregion

    }

    private string GenerarInstruccion(string codAduana)
    {
        string idInstruccion = "";
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);

            i.loadMaxInstruccionXAduanaCompleta(codAduana);

            var codPaisAduana =
                _aduanaDC.Aduanas.FirstOrDefault(w => w.CodigoAduana == codAduana && w.CodEstado == "0").CodPais;
            if (codPaisAduana != null)
                idInstruccion = codPaisAduana + "-" + codAduana + "-" + i.TABLA.Rows[0]["MaxIdInstruccion"].ToString();
        }
        catch (Exception ex){}
        finally{desconectar();}

        return idInstruccion;
    }

    private void LooongMethodWhichUpdatesTheProgressContext(UploadedFile file)
    {
        try
        {
            const int total = 100;
            RadProgressContext progress = RadProgressContext.Current;
            for (int i = 0; i < total; i++)
            {
                progress.PrimaryTotal = 1;
                progress.PrimaryValue = 1;
                progress.PrimaryPercent = 100;
                progress.SecondaryTotal = total;
                progress.SecondaryValue = i;
                progress.SecondaryPercent = i;

                progress.CurrentOperationText = file.GetName() + " se está procesando...";

                if (!Response.IsClientConnected)
                {
                    break;
                }
                System.Threading.Thread.Sleep(100);
            }
        }
        catch (Exception ex)
        {
            registrarMensaje(ex.Message);
        }
    }
    protected string parameterForm1(string name, string startup)
    {
        if (Page.Request.Form.Get(name) != null && Page.Request.Form.Get(name).Length > 0)
            if (!Regex.IsMatch(Page.Request.Form.Get(name), @"^[\w-=,. ]{1,70}$"))
                throw new ArgumentException("Invalid name parameter");
        return (null != Page.Request.Form.Get(name)) ? Page.Request.Form.Get(name) : startup;
    }
    private void releaseObject(object obj)
    {
        try
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            obj = null;
        }
        catch (Exception ex)
        {
            obj = null;
            registrarMensaje("Unable to release the Object " + ex.ToString());
        }
        finally
        {
            GC.Collect();
        }
    }

    protected void RadButtonProcesar_Click(object sender, EventArgs e)
    {
        LabelMensajes.Visible = true;
        LabelMensajes.Text = "Procesando... Espere por favor.";
        RadButtonProcesar.Enabled = false;
        readExcel(true);
    }
    #region Conexion
    /*private void conectarPortal()
    {
        if (logAppPortal == null)
            logAppPortal = new GrupoLis.Login.Login();
        if (!logAppPortal.conectado)
        {
            logAppPortal.tipoConexion = TipoConexion.SQL_SERVER;
            logAppPortal.SERVIDOR = mParamethers.Get("Servidor");
            logAppPortal.DATABASE = mParamethers.Get("DatabasePC");
            logAppPortal.USER = mParamethers.Get("User");
            logAppPortal.PASSWD = mParamethers.Get("Password");
            logAppPortal.conectar();
        }
    }*/

    /*private void desconectarPortal()
    {
        if (logAppPortal.conectado)
            logAppPortal.desconectar();
    }*/

    #endregion
}