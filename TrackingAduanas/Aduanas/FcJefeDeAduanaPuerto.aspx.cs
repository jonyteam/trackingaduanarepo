﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ObjetosDto;
using Telerik.Web.UI;

public partial class FcJefeDeAduanaPuerto : Utilidades.PaginaBase
{
    readonly AduanasDataContext _aduanasDc = new AduanasDataContext();
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Jefe De Aduana Puerto";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        RadWindowManager1.Windows.Clear();
        SetGridFilterMenu(rgAsistenteOperaciones.FilterMenu);
        rgAsistenteOperaciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgAsistenteOperaciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Jefe De Aduana Puerto", "Jefe De Aduana Puerto");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
        }
        Timer1.Enabled = true;
    }

    private bool Anular { get { return tienePermiso("ANULAR"); } }
    private bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    private bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
    {
        if (e.Argument != "Rebind") return;
        rgAsistenteOperaciones.Rebind();
        Timer1.Enabled = true;
    }


    #region Jefe De Aduana Puerto
    protected void rgAsistenteOperaciones_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        LlenarGrid();
    }

    private void LlenarGrid()
    {
        try
        {
            var query = _aduanasDc.ProcesoFlujoCarga
                   .Where(p => p.Eliminado == false && p.CodEstado == "2" && p.CodigoAduana == "016")
                   .Join(_aduanasDc.Instrucciones.Select(i => new { i.CodEstado, i.IdInstruccion, i.CodRegimen, i.NumeroFactura, i.Producto })
                   .Where(i => i.CodEstado != "100"), p => p.IdInstruccion, i => i.IdInstruccion, (p, i) => new { p, i })
                   .Select(pi => new ProcesoFlujoCargaGridVm
                   {
                       Id = pi.p.Id,
                       IdInstruccion = pi.p.IdInstruccion,
                       Cliente = pi.p.Cliente,
                       Proveedor = pi.p.Proveedor,
                       CodRegimen = pi.i.CodRegimen,
                       NoFactura = pi.i.NumeroFactura,
                       Correlativo = pi.p.Correlativo,
                       CanalSelectividad = pi.p.CanalSelectividad,
                       Producto = pi.i.Producto,
                       CodigoAduana = pi.p.CodigoAduana
                   })
                   .ToList();

            if (User.IsInRole("Jefes Aduana"))
            {
                //cuando es jefe de aduana el CodigoAduana es condicion
                var codigoAduana = _aduanasDc.Usuarios.Where(u => u.IdUsuario == Convert.ToDecimal(Session["IdUsuario"].ToString()) && u.Eliminado == '0')
                .Select(u => u.CodigoAduana).SingleOrDefault();
                if (codigoAduana == "016")
                {
                    query = query.Where(q => q.CodigoAduana == codigoAduana).ToList();
                }
                else
                    query = new List<ProcesoFlujoCargaGridVm>();
            }
            else if (!User.IsInRole("Administradores"))
            {
                query = new List<ProcesoFlujoCargaGridVm>();
            }
            rgAsistenteOperaciones.DataSource = query;
        }
        catch { }
    }

    protected void rgAsistenteOperaciones_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            var editedItem = e.Item as GridEditableItem;
            if (e.CommandName == "Descargar")
            {
                if (editedItem != null)
                {
                    var idInstruccion = editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdInstruccion"].ToString();
                    Timer1.Enabled = false;
                    SessionFileShare.CreateDownload(idSistema: idInstruccion, page: this);
                }
            }
            if (e.CommandName == "Guardar")
            {
                if (editedItem != null)
                {
                    var edRevision = (RadComboBox)editedItem.FindControl("edRevision");
                    if (!String.IsNullOrEmpty(edRevision.SelectedValue))
                    {
                        var id = Convert.ToDecimal(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"]);

                        var pfc = _aduanasDc.ProcesoFlujoCarga.SingleOrDefault(p => p.Id == id && p.Eliminado == false);
                        if (pfc != null)
                        {
                            pfc.Revision = edRevision.SelectedValue;
                            pfc.CodEstado = "10";

                            var tfc = new TiemposFlujoCarga
                            {
                                IdInstruccion = pfc.IdInstruccion,
                                IdEstado = "10", //revisar que estado es para decision revision
                                Fecha = DateTime.Now,
                                FechaIngreso = DateTime.Now,
                                Observacion = "Decisión revisión en Puerto: " + edRevision.Text,
                                Eliminado = false,
                                IdUsuario = Convert.ToDecimal(Session["IdUsuario"])
                            };
                            _aduanasDc.TiemposFlujoCarga.InsertOnSubmit(tfc);

                            _aduanasDc.SubmitChanges();
                            RegistrarMensaje2("Decisión revisión realizada exitosamente");
                            rgAsistenteOperaciones.Rebind();
                        }
                        else
                        {
                            RegistrarMensaje2("Registro presenta error...favor comunique al administrador del sistema");
                        }
                    }
                    else
                        RegistrarMensaje2("Seleccione si desea revisión");
                    Timer1.Enabled = true;
                }
            }
            if (rgAsistenteOperaciones.Items.Count >= 1 & (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName))
                ConfigureExport("");
            else if (rgAsistenteOperaciones.Items.Count < 1 & (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName))
                e.Canceled = true;
        }
        catch { }
    }

    private void ConfigureExport(string descripcion)
    {
        HttpPostedFileBase fileBase;
        var filename = descripcion + " " + DateTime.Now.ToShortDateString();
        rgAsistenteOperaciones.ExportSettings.FileName = filename;
        rgAsistenteOperaciones.ExportSettings.ExportOnlyData = true;
        rgAsistenteOperaciones.ExportSettings.IgnorePaging = true;
        rgAsistenteOperaciones.ExportSettings.OpenInNewWindow = true;
        rgAsistenteOperaciones.MasterTableView.ExportToExcel();
    }

    protected void rgAsistenteOperaciones_Init(object sender, EventArgs e)
    {
        var menu = rgAsistenteOperaciones.FilterMenu;
        menu.Items.RemoveAt(rgAsistenteOperaciones.FilterMenu.Items.Count - 2);
    }

    private void RegistrarMensaje2(string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("alert('" + mensaje + "');");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "mensaje", sb.ToString(), false);
    }
    #endregion
    protected void rgAsistenteOperaciones_ItemDataBound(object sender, GridItemEventArgs e)
    {
        try
        {
            foreach (GridDataItem gridItem in rgAsistenteOperaciones.Items)
            {
                var edTramitador = (RadComboBox)gridItem.FindControl("edTramitador");
                var codigoAduana = gridItem["CodigoAduana"].Text;
                if (!String.IsNullOrEmpty(codigoAduana))
                {
                    var query = _aduanasDc.Usuarios.Where(u => u.Eliminado == '0' && u.CodigoAduana == codigoAduana)
                        .Select(u => new { u.CodigoAduana, u.IdUsuario, u.Nombre, u.Apellido })
                        .Join(_aduanasDc.UsuariosRoles.Where(r => r.Eliminado == '0' && r.IdRol == 3), u => u.IdUsuario, r => r.IdUsuario, (u, r) => new { u, r })
                        .Select(ur => new UsuarioVm
                        {
                            IdUsuario = ur.u.IdUsuario,
                            NombreCompleto = ur.u.Nombre.Trim() + " " + ur.u.Apellido.Trim(),
                        }).ToList();

                    edTramitador.DataSource = query;
                    edTramitador.DataBind();
                }
            }
        }
        catch { }
    }

    protected void btnReasignar_Click(object sender, EventArgs e)
    {
        try
        {
            AbrirVentana();
            Timer1.Enabled = false;
        }
        catch (Exception)
        { }

    }

    private void AbrirVentana()
    {
        var window1 = new RadWindow
        {
            NavigateUrl = "FcJefeDeAduanaPuertoEstados.aspx",
            VisibleOnPageLoad = true,
            ID = "Estados",
            Width = 1300,
            Height = 500,
            Animation = WindowAnimation.FlyIn,
            DestroyOnClose = true,
            VisibleStatusbar = false,
            Behaviors = WindowBehaviors.Close,
            Modal = true,
            OnClientClose = "OnClientClose"
        };
        RadWindowManager1.Windows.Add(window1);
    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
        rgAsistenteOperaciones.Rebind();
    }
}