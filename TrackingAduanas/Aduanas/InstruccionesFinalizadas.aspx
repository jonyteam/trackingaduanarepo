﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="InstruccionesFinalizadas.aspx.cs" Inherits="InstruccionesFinalizadas" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                    //<![CDATA[
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0
                    || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }
                                          
            </script>
        </telerik:RadScriptBlock>
        <telerik:RadMultiPage ID="mpInstrucciones" runat="server" BackColor="AliceBlue">
            <telerik:RadPageView ID="pvGridInstrucciones" runat="server" Width="100%">
                <table id="Table1" runat="server" style="width: 100%">
                    <tr>
                        <td style="width: 7%">
                            <asp:Label ID="lblInstruccion" Font-Bold="true" runat="server" Text="Instrucción:"></asp:Label>
                        </td>
                        <td style="width: 9%">
                            <telerik:RadTextBox ID="txtInstruccion" runat="server" MaxLength="15" Width="85%">
                            </telerik:RadTextBox>
                        </td>
                        <td style="width: 85%">
                            <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Images/24/view_24.png"
                                OnClick="btnBuscar_Click" />
                        </td>
                    </tr>
                </table>
                <table style="width: 100%">
                    <tr>
                        <td>
                            <telerik:RadGrid ID="rgInstrucciones" runat="server" AllowFilteringByColumn="True"
                                AllowSorting="True" AutoGenerateColumns="False" GridLines="None" Width="100%"
                                Height="400px" OnNeedDataSource="rgInstrucciones_NeedDataSource" AllowPaging="True"
                                ShowFooter="True" ShowStatusBar="True" PageSize="20" OnInit="rgInstrucciones_Init"
                                OnItemCommand="rgInstrucciones_ItemCommand" CellSpacing="0">
                                <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                                <MasterTableView DataKeyNames="IdInstruccion,IdTramite,CodigoAduana" CommandItemDisplay="TopAndBottom"
                                    NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay instrucciones."
                                    GroupLoadMode="Client">
                                    <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                                        ShowExportToExcelButton="true" ShowExportToCsvButton="true" />
                                    <RowIndicatorColumn>
                                        <HeaderStyle Width="20px"></HeaderStyle>
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn>
                                        <HeaderStyle Width="20px"></HeaderStyle>
                                    </ExpandCollapseColumn>
                                    <Columns>
                                        <telerik:GridButtonColumn ConfirmText="¿Está seguro que desea generar reporte de esta instrucción?"
                                            ConfirmDialogType="RadWindow" ConfirmTitle="Generar Reporte Instrucción"
                                            ImageUrl="~/Images/24/document_24.png" CommandName="Reporte" 
                                            HeaderText="Generar Reporte" ButtonType="ImageButton">
                                            <ItemStyle Width="50px" HorizontalAlign="Center" />
                                            <HeaderStyle Width="50px" />
                                        </telerik:GridButtonColumn>
                                        <telerik:GridBoundColumn DataField="IdTramite" HeaderText="Trámite No." UniqueName="IdTramite"
                                            FilterControlWidth="80%">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="IdInstruccion" HeaderText="Instrucción No." UniqueName="IdInstruccion"
                                            FilterControlWidth="80%">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Nombre" HeaderText="Cliente" UniqueName="Nombre"
                                            FilterControlWidth="80%">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="DocumentoNo" HeaderText="Factura" UniqueName="DocumentoNo"
                                            FilterControlWidth="80%">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="NoCorrelativo" HeaderText="Correlativo" UniqueName="NoCorrelativo"
                                            FilterControlWidth="80%">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Proveedor" HeaderText="Proveedor" UniqueName="Proveedor"
                                            FilterControlWidth="80%">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="CodigoAduana" HeaderText="CodigoAduana" UniqueName="CodigoAduana"
                                            FilterControlWidth="80%" Visible="false">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                    <EditFormSettings>
                                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                        </EditColumn>
                                    </EditFormSettings>
                                </MasterTableView>
                                <HeaderStyle Width="180px" />
                                <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                                    <Selecting AllowRowSelect="True" />
                                    <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                                        DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                                        DropHereToReorder="Suelte aquí para Re-Ordenar" />
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
                                </ClientSettings>
                                <FilterMenu EnableTheming="True">
                                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                                </FilterMenu>
                                <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                                <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
                            </telerik:RadGrid>
                        </td>
                    </tr>
                </table>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </telerik:RadAjaxPanel>
    <%--<telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="cmbPaisHojaRuta">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="cmbAduanas" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>--%>
</asp:Content>
