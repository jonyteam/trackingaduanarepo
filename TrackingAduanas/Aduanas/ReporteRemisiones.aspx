﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteRemisiones.aspx.cs" Inherits="ReporteGestionCarga" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <table border="1" style="width: 45%">
        <tr>
            <td style="width: 15%">
                <asp:Label ID="lblFechaInicio" runat="server" Text="Fecha Inicio:" ForeColor="Black"></asp:Label>
            </td>
            <td style="width: 22%">
                <telerik:RadDatePicker ID="dpFechaInicio" runat="server" EnableTyping="False" Width="95%">
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="dpFechaInicio"
                    ErrorMessage="Ingrese Fecha" ForeColor="Red" />
            </td>
            <td style="width: 15%">
                <asp:Label ID="lblFechaFinal" runat="server" Text="Fecha Final:" ForeColor="Black"></asp:Label>
            </td>
            <td style="width: 22%">
                <telerik:RadDatePicker ID="dpFechaFinal" runat="server" EnableTyping="False" Width="95%">
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator runat="server" ID="reqName" ControlToValidate="dpFechaFinal"
                    ErrorMessage="Ingrese Fecha" ForeColor="Red" />
            </td>
            <td style="width: 10%">
                <asp:Label ID="lblPais" runat="server" Text="País:" ForeColor="Black"></asp:Label>
            </td>
            <td style="width: 16%">
                <telerik:RadComboBox ID="cmbPais" runat="server" DataTextField="Descripcion" DataValueField="Codigo"
                    AutoPostBack="true">
                </telerik:RadComboBox>
            </td>
        </tr>
        <tr>
            <td colspan="6" align="center">
                <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Images/24/view_24.png"
                    OnClick="btnBuscar_Click" />
            </td>
        </tr>
    </table>
    <telerik:RadGrid ID="rgRemisiones" runat="server" AllowFilteringByColumn="True"
        Visible="False" AllowSorting="True" AutoGenerateColumns="False" GridLines="None"
        Height="410px" OnNeedDataSource="rgRemisiones_NeedDataSource" AllowPaging="True"
        ShowFooter="True" ShowStatusBar="True" PageSize="20" OnInit="rgRemisiones_Init"
        OnItemCommand="rgRemisiones_ItemCommand" CellSpacing="0" Culture="es-ES">
        <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
            PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
            Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
        <MasterTableView DataKeyNames="" CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros."
            NoMasterRecordsText="No hay Datos." GroupLoadMode="Client">
            <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                ShowExportToExcelButton="true" />
<CommandItemSettings RefreshText="Volver a Cargar Datos" ExportToPdfText="Export to PDF" ShowAddNewRecordButton="False" ShowExportToExcelButton="True"></CommandItemSettings>

            <RowIndicatorColumn>
                <HeaderStyle Width="20px"></HeaderStyle>
            </RowIndicatorColumn>
            <ExpandCollapseColumn>
                <HeaderStyle Width="20px"></HeaderStyle>
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn DataField="NumeroRemision" HeaderText="Numero de Remision" UniqueName="NumeroRemision" FilterControlAltText="Filter NumeroRemision column">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="IdInstruccion" HeaderText="Hoja de Ruta" UniqueName="IdInstruccion" FilterControlAltText="Filter IdInstruccion column">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FechaEnvioSistema" HeaderText="Fecha de Envio" UniqueName="FechaEnvioSistema" FilterControlAltText="Filter FechaEnvioSistema column">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Nombre" HeaderText="Usuario Envio" UniqueName="Nombre" FilterControlAltText="Filter Nombre column">
                </telerik:GridBoundColumn>
            </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
        </MasterTableView>
        <HeaderStyle Width="180px" />
<SortingSettings SortToolTip="Presione aqu&#237; para ordenar" 
            SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente"></SortingSettings>

        <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
            <Selecting AllowRowSelect="True" />
            <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                DropHereToReorder="Suelte aquí para Re-Ordenar" />
            <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
<Selecting AllowRowSelect="True"></Selecting>

<ClientMessages DropHereToReorder="Suelte aqu&#237; para Re-Ordenar" DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tama&#241;o" PagerTooltipFormatString="P&#225;gina &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"></ClientMessages>

<Scrolling AllowScroll="True" UseStaticHeaders="True"></Scrolling>
        </ClientSettings>

<StatusBarSettings ReadyText="Listo" LoadingText="Cargando, por favor espere..."></StatusBarSettings>

        <FilterMenu EnableTheming="True">
            <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
        </FilterMenu>
        <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
        <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
    </telerik:RadGrid>
</asp:Content>
