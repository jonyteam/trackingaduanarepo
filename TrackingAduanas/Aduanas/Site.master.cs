﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Telerik.Web.UI;
using System.Security.Principal;

public partial class SiteMaster : Utilidades.PaginaMasterBase
{
    private ModulosUsuario usuario;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckVencimiento();
            AddControlToolTip(Controls);
        }
    }

    private string[] GetModulosRole()
    {
        string modulosRole = "";
        IPrincipal user = HttpContext.Current.User;
        string idRole = "";
        conectar();
        RolesBO roles = new RolesBO(logApp);
        ModulosBO modulos = new ModulosBO(logApp);
        roles.loadRoles();
        for (int i = 0; i < roles.TABLA.Rows.Count; i++)
        {
            if (user.IsInRole(roles.DESCRIPCION))
            {
                idRole = roles.IDROL;
                modulos.loadModulosRol(idRole);
                for (int j = 0; j < modulos.TABLA.Rows.Count; j++)
                {
                    modulosRole += modulos.DESCRIPCION + ",";
                    modulos.regSiguiente();
                }
            }
            roles.regSiguiente();
        }
        desconectar();
        modulosRole = modulosRole.Remove(modulosRole.Length - 1);
        return modulosRole.Split(",".ToCharArray(), modulosRole.Length);
    }
    
    protected void CheckVencimiento()
    {
        if (Path.GetFileNameWithoutExtension(Request.PhysicalPath).ToLower() == "mantenimientocambiarclave")
            return;

        try
        {
            conectar();
            UsuariosBO bo = new UsuariosBO(logApp);
            bo.loadUsuario(Session["IdUsuario"].ToString());

            if (DateTime.Now > bo.VENCIMIENTO)
            {
                redirectTo("MantenimientoCambiarClave.aspx?titulo=La Clave ya Caduco. Debe Cambiar la Clave.");
            }
        }
        catch
        {
        }
        finally
        {
            desconectar();
        }
    }

    public void AddControlToolTip(Control control)
    {
        if ((control != this) && (control is WebControl))
        {
            String tooltip = String.Empty;

            if (control is Image)
                tooltip = ((WebControl)control).ToolTip.Trim().Length > 0 ? ((WebControl)control).ToolTip : ((Image)control).AlternateText;
            else if (control is WebControl)
                tooltip = ((WebControl)control).ToolTip;
            else if (control is Telerik.Web.UI.RadGrid)
                tooltip = "o";

            if (tooltip.Trim().Length > 0)
                RadToolTipManager1.TargetControls.Add(control.ClientID, tooltip.Trim(), true);
        }
    }
    
    public void AddControlToolTip(ControlCollection controls)
    {
        foreach (Control control in controls)
        {
            AddControlToolTip(control);

            if (control.HasControls())
                AddControlToolTip(control.Controls);
        }
    }

    public void SetTitulo(String pagina, String titulo)
    {
        MainContent.Visible = true;
        lbTitle.Visible = true;
        Page.Title = pagina;
        lbTitle.Text = titulo;       
    }

    protected String Skin { get { return (Session["Skin"] != null ? Session["Skin"].ToString() : "Sunset"); } }

    public override bool CanGoBack { get { return false; } }
}
