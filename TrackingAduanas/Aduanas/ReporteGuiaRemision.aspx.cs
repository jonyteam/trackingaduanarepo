﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;
using Telerik.ReportViewer;

using System.Drawing;
using System.Drawing.Printing;
using Telerik.Reporting.Processing;
using Telerik.Web.UI.GridExcelBuilder;
using System.Text;
using System.IO;
using ReportesAduanas;

using System.Windows.Forms;
using MessagingToolkit.QRCode.Codec;
using System.Drawing.Imaging;


public partial class ReporteGuiaRemision : Utilidades.PaginaBase
{
  //  public static ReportesAduanas.ReporteGeneral reporteremision = new ReportesAduanas.ReporteGeneral();
    public static ReportesAduanas.Facturacion reporFact = new ReportesAduanas.Facturacion();
    public static ReportesAduanas.ReporteRemision remision = new ReportesAduanas.ReporteRemision();
    readonly AduanasDataContext _aduanasDc = new AduanasDataContext();
 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Instrucciones", "Reporte Instrucciones");
            try
            {
                conectar();
                string idInstruccion = Request.QueryString.Get("IdInstruccion").ToString().Trim();


                InstruccionesBO i = new InstruccionesBO(logApp);

                i.ejecutarSPDatosInstruccionRemision(idInstruccion);
                var pi = _aduanasDc.RemisionesInstrucciones.SingleOrDefault(p => p.IdInstruccion == idInstruccion && p.CodEstado != "100");
                var pii = _aduanasDc.AduanasRemision.SingleOrDefault(p => p.Item == pi.IdRemision);
                remision = new ReportesAduanas.ReporteRemision();
               
                QRCodeEncoder Enconder = new QRCodeEncoder();
                Bitmap img = Enconder.Encode(idInstruccion + " | " +pii.Rango+"-"+ pi.Serie);
         
                img.Save("J:\\Files1\\Aduanas\\HND\\QR Imagenes\\" + idInstruccion + ".jpg", ImageFormat.Jpeg);
                remision.DataSource = i.TABLA;

                remision.ReportParameters[1].Value = "J:\\Files1\\Aduanas\\HND\\QR Imagenes\\" + idInstruccion + ".jpg";

                remision.ReportParameters[0].Value = idInstruccion;
               
                ReportViewer1.Report = remision;
                ReportViewer1.Visible = true;
             
            }
            catch { }
            finally
            {
                desconectar();
            }
            mpReporte.SelectedIndex = 0;
        }
    }

    public override bool CanGoBack { get { return false; } }

    protected void btnBack_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Response.Redirect("GuiaRemision.aspx");
        }
        catch { }
    }



    void pd_PrintPage(object sender, PrintPageEventArgs e)
    {
        try
        {
            Font unaFuente = new Font("Gill Sans MT Condensed", 7, FontStyle.Regular);
            e.Graphics.PageUnit = GraphicsUnit.Millimeter;
            e.Graphics.DrawString(remision.ToString(), unaFuente, Brushes.Black, 100, 100, System.Drawing.StringFormat.GenericDefault);
            e.HasMorePages = false;
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "ReporteInstrucciones");
        }

    }

}