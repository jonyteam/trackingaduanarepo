﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="FcAseguramientoDeServicio.aspx.cs" Inherits="FcAseguramientoDeServicio" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            ////<![CDATA[
            //function requestStart(sender, args) {
            //    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
            //        args.set_enableAjax(false);
            //    }
            //}

            function OnClientClose() {
                $find("<%= RadAjaxManager1.ClientID%>").ajaxRequest("Rebind");
            }
        </script>
    </telerik:RadScriptBlock>
    <div id="miDiv" runat="server" class="panelCentrado2">
        <table width="100%">
            <tr>
                <td>
                    <telerik:RadGrid ID="rgAsistenteOperaciones" runat="server" AllowFilteringByColumn="True"
                        AllowSorting="True" AutoGenerateColumns="False" GridLines="None" Width="100%"
                        Height="470px" OnNeedDataSource="rgAsistenteOperaciones_NeedDataSource" AllowPaging="True"
                        ShowFooter="True" ShowStatusBar="True" PageSize="20" OnInit="rgAsistenteOperaciones_Init"
                        OnItemCommand="rgAsistenteOperaciones_ItemCommand" CellSpacing="0" Culture="es-ES">
                        <HeaderContextMenu EnableTheming="True">
                            <CollapseAnimation Duration="200" Type="OutQuint" />
                        </HeaderContextMenu>
                        <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                            PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                            Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                        <MasterTableView DataKeyNames="Id,NoFactura,IdInstruccion" CommandItemDisplay="Top"
                            NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay instrucciones definidas."
                            GroupLoadMode="Client">
                            <CommandItemSettings ShowAddNewRecordButton="False" RefreshText=""
                                ShowExportToExcelButton="False" />
                            <RowIndicatorColumn>
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn>
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridBoundColumn DataField="IdInstruccion" HeaderText="IdInstruccion" UniqueName="Instruccion"
                                    FilterControlWidth="70%" AllowFiltering="true">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Cliente" HeaderText="Cliente" UniqueName="Cliente"
                                    FilterControlWidth="80%">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Proveedor" HeaderText="Proveedor" UniqueName="Proveedor" FilterControlWidth="70%">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CodRegimen" HeaderText="Regimen" UniqueName="CodRegimen"
                                    FilterControlWidth="50%">
                                    <HeaderStyle Width="60px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="NoFactura" HeaderText="Factura" UniqueName="NoFactura"
                                    FilterControlWidth="80%">
                                    <HeaderStyle Width="120px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Subir" HeaderText="Subir Archivo"
                                    ImageUrl="Images/16/index_up_16.png" UniqueName="btnSubir" Display="False">
                                    <HeaderStyle HorizontalAlign="Center" Width="70px" />
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </telerik:GridButtonColumn>
                             

                                <telerik:GridTemplateColumn UniqueName="Descargar" HeaderText="Descargar"
                                    AllowFiltering="false">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnActivo2" runat="server" CommandName="Descargar"
                                            CommandArgument='<%# Eval("IdInstruccion") %>'
                                            ImageUrl="Images/16/index_down_16.png"
                                            Visible='<%# (Eval("Digitalizado")=="" ?  false:true) %>'
                                            />
                                    </ItemTemplate>
                                    <HeaderStyle Width="7%" />
                                    <ItemStyle Width="7%" />
                                </telerik:GridTemplateColumn>

                           
                                <telerik:GridBoundColumn DataField="Correlativo" HeaderText="Correlativo" UniqueName="Correlativo"
                                    FilterControlWidth="70%">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ComprobantePago" HeaderText="Comprobante" UniqueName="ComprobantePago"
                                    FilterControlWidth="70%">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn AllowFiltering="False" FilterControlAltText="Filter TemplateColumn column"
                                    ShowFilterIcon="False" UniqueName="TemplateColumn">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chbAgrupar" runat="server" Text="Seleccionar"
                                            Enabled='<%# (Eval("Digitalizado")=="" ?  false:true) %>'  />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>


                       
                                       <telerik:GridTemplateColumn UniqueName="Enviar" HeaderText="Enviar a"
                                    AllowFiltering="false">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnActivo" runat="server" CommandName="Agente"                                        
                                            ImageUrl="~/Images/16/check2_16.png"
                                            Enabled='<%# (Eval("Digitalizado")=="" ?  false:true) %>'
                                            />
                                    </ItemTemplate>
                                    <HeaderStyle Width="7%" />
                                    <ItemStyle Width="7%" />
                                </telerik:GridTemplateColumn>

                                <telerik:GridBoundColumn DataField="Digitalizado" Display="False" FilterControlAltText="Filter Digitalizado column" HeaderText="Digitalizado" UniqueName="Digitalizado">
                                </telerik:GridBoundColumn>

                            </Columns>

                            <EditFormSettings>
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                            </EditFormSettings>
                        </MasterTableView>
                        <HeaderStyle Width="180px" />
                        <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                            <Selecting AllowRowSelect="True" />
                            <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                                DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                                DropHereToReorder="Suelte aquí para Re-Ordenar" />
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
                        </ClientSettings>
                        <FilterMenu EnableTheming="True">
                            <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                        </FilterMenu>
                        <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                        <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
        <table width="100%">
            <tr style="text-align: center">
                <td style="width: 100%" align="center">
                    <telerik:RadButton ID="btnCrearInstruccion" runat="server" Text="Crear Instrucción" Width="150px" Height="30px"
                        BackColor="#E4E4E4" Font-Bold="True" Font-Size="12pt"
                        CausesValidation="False" OnClick="btnCrearInstruccion_Click">
                    </telerik:RadButton>
                </td>
            </tr>
        </table>
        <asp:Panel ID="Panel1" runat="server">
            <asp:Timer ID="Timer1" runat="server" Interval="180000" OnTick="Timer1_Tick" ClientIDMode="AutoID" />
        </asp:Panel>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server"></telerik:RadWindowManager>
    </div>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgAsistenteOperaciones">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgAsistenteOperaciones" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="Timer1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgAsistenteOperaciones" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCrearInstruccion">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="btnCrearInstruccion" LoadingPanelID="LoadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>

                        <telerik:AjaxSetting AjaxControlID="btnActivo">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgAsistenteOperaciones" LoadingPanelID="LoadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="RadAjaxManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgAsistenteOperaciones" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>
