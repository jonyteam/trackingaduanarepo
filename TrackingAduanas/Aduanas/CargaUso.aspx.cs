﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.OleDb;

using Telerik.Web.UI;
//using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;

public partial class GenerarArchivoSAPDinant : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "ReporteHagamodaPromoda";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Hagamoda/Promoda", "");
        }
    }
    protected bool Consultar { get { return tienePermiso("Consultar"); } }

    protected void rgGuias_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGrid();
    }


    private void ConfigureExport()
    {
        String filename = "Carga_" + DateTime.Now.ToShortDateString();
        rgCarga.ExportSettings.FileName = filename;
        rgCarga.ExportSettings.ExportOnlyData = true;
        rgCarga.ExportSettings.IgnorePaging = true;
        rgCarga.ExportSettings.OpenInNewWindow = true;
        rgCarga.MasterTableView.ExportToExcel();
    }

    public override bool CanGoBack { get { return false; } }

    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {

        llenarGrid();
    }


    private void llenarGrid()
    {

        try
        {
            conectar();




            ComprasEspeciesFiscalesBO CEF1 = new ComprasEspeciesFiscalesBO(logApp);
   

            int edCantidad;

            DataTable tabla = new DataTable();
            DataRow fila;

            tabla.Columns.Add("Fec.docu.", typeof(String));
            tabla.Columns.Add("Clase Dto.", typeof(String));
            tabla.Columns.Add("Sociedad", typeof(String));
            tabla.Columns.Add("Fec.Con.", typeof(String));
            tabla.Columns.Add("Periodo", typeof(String));
            tabla.Columns.Add("Moneda", typeof(String));
            tabla.Columns.Add("No.Dto.", typeof(String));
            tabla.Columns.Add("Referencia", typeof(String));
            tabla.Columns.Add("TxtCabDto", typeof(String));
            tabla.Columns.Add("Divi.", typeof(String));
            tabla.Columns.Add("CLAVE", typeof(String));
            tabla.Columns.Add("Codigo", typeof(String));
            tabla.Columns.Add("ImporteMD", typeof(String));
            tabla.Columns.Add("CeCo", typeof(String));
            tabla.Columns.Add("Asignacion", typeof(String));
            tabla.Columns.Add("Texto", typeof(String));
            tabla.Columns.Add("ImporteML", typeof(String));
            tabla.Columns.Add("Referencia 3", typeof(String));
            if (chbUso.Checked == true)
            {
                EspeciesFiscalesInstruccionesBO EFI = new EspeciesFiscalesInstruccionesBO(logApp);
                #region Carga Uso

                EFI.CargaUso();
                for (int j = 0; j < EFI.totalRegistros; j++)
                {
                    string codEspecieFiscal = EFI.TABLA.Rows[j]["Serie"].ToString();
                    string serie = "";
                    string[] arreglo = codEspecieFiscal.Trim().Split(",".ToCharArray());
                    edCantidad = arreglo.Length;

                    double contador = 0;
                    for (int i = 0; i < edCantidad; i++)
                    {
                        serie = arreglo[i];
                        CEF1.buscarprecio(serie, EFI.TABLA.Rows[j]["CodigoEspecieFiscal"].ToString());
                        if (CEF1.totalRegistros == 0)
                        {
                            contador = 0;
                        }
                        else
                        {
                            contador = double.Parse(CEF1.TABLA.Rows[0]["ImporteMD"].ToString());
                        }
                        fila = tabla.NewRow();
                        fila["Fec.docu."] = EFI.TABLA.Rows[j]["Fec.docu."].ToString();
                        fila["Clase Dto."] = EFI.TABLA.Rows[j]["Clase Dto."].ToString();
                        fila["Sociedad"] = EFI.TABLA.Rows[j]["Sociedad"].ToString();
                        fila["Fec.Con."] = EFI.TABLA.Rows[j]["Fec.Con."].ToString();
                        fila["Periodo"] = System.DateTime.Now.ToString("MM");
                        fila["Moneda"] = EFI.TABLA.Rows[j]["Moneda"].ToString();
                        fila["No.Dto."] = EFI.TABLA.Rows[j]["No.Dto."].ToString();
                        fila["Referencia"] = EFI.TABLA.Rows[j]["Referencia"].ToString();
                        fila["TxtCabDto"] = EFI.TABLA.Rows[j]["TxtCabDto"].ToString();
                        fila["Divi."] = EFI.TABLA.Rows[j]["Divi."].ToString();
                        fila["CLAVE"] = EFI.TABLA.Rows[j]["CLAVE"].ToString();
                        fila["Codigo"] = EFI.TABLA.Rows[j]["Codigo"].ToString();
                        fila["ImporteMD"] = contador;
                        fila["Asignacion"] = (EFI.TABLA.Rows[j]["CodigoEspecieFiscal"].ToString() + "-" + serie.ToString());
                        fila["Texto"] = EFI.TABLA.Rows[j]["Texto"].ToString();
                        fila["ImporteML"] = contador;
                        fila["Referencia 3"] = EFI.TABLA.Rows[j]["Referencia 3"].ToString();
                        tabla.Rows.Add(fila);

                        fila = tabla.NewRow();
                        fila["Fec.docu."] = EFI.TABLA.Rows[j]["Fec.docu."].ToString();
                        fila["Clase Dto."] = EFI.TABLA.Rows[j]["Clase Dto."].ToString();
                        fila["Sociedad"] = EFI.TABLA.Rows[j]["Sociedad"].ToString();
                        fila["Fec.Con."] = EFI.TABLA.Rows[j]["Fec.Con."].ToString();
                        fila["Periodo"] = System.DateTime.Now.ToString("MM");
                        fila["Moneda"] = EFI.TABLA.Rows[j]["Moneda"].ToString();
                        fila["No.Dto."] = EFI.TABLA.Rows[j]["No.Dto."].ToString();
                        fila["Referencia"] = EFI.TABLA.Rows[j]["Referencia"].ToString();
                        fila["TxtCabDto"] = EFI.TABLA.Rows[j]["TxtCabDto"].ToString();
                        fila["Divi."] = EFI.TABLA.Rows[j]["Divi."].ToString();
                        fila["CLAVE"] = EFI.TABLA.Rows[j]["CLAVE2"].ToString();
                        fila["Codigo"] = EFI.TABLA.Rows[j]["Cuenta2"].ToString();
                        fila["ImporteMD"] = contador;
                        fila["Asignacion"] = EFI.TABLA.Rows[j]["Referencia 3"].ToString();
                        fila["Texto"] = EFI.TABLA.Rows[j]["Texto"].ToString();
                        fila["ImporteML"] = contador;
                        fila["Referencia 3"] = (EFI.TABLA.Rows[j]["CodigoEspecieFiscal"].ToString() + "-" + serie.ToString());
                        tabla.Rows.Add(fila);
                    }
                    EFI.regSiguiente();
                }

                #endregion
            }
            if (chbCompras.Checked == true)
            {
                ComprasEspeciesFiscalesBO CEF = new ComprasEspeciesFiscalesBO(logApp);
                #region Carga Compras

                CEF.CargaUso();
                for (int j = 0; j < CEF.totalRegistros; j++)
                {
                    for (int i = 0; i < Int32.Parse(CEF.TABLA.Rows[j]["Cantidad"].ToString()); i++)
                    {
                        fila = tabla.NewRow();
                        fila["Fec.docu."] = CEF.TABLA.Rows[j]["Fec.docu."].ToString();
                        fila["Clase Dto."] = CEF.TABLA.Rows[j]["Clase Dto."].ToString();
                        fila["Sociedad"] = CEF.TABLA.Rows[j]["Sociedad"].ToString();
                        fila["Fec.Con."] = CEF.TABLA.Rows[j]["Fec.Con."].ToString();
                        fila["Periodo"] = System.DateTime.Now.ToString("MM");
                        fila["Moneda"] = CEF.TABLA.Rows[j]["Moneda"].ToString();
                        fila["No.Dto."] = CEF.TABLA.Rows[j]["No.Dto."].ToString();
                        fila["Referencia"] = CEF.TABLA.Rows[j]["Referencia"].ToString();
                        fila["TxtCabDto"] = CEF.TABLA.Rows[j]["TxtCabDto"].ToString();
                        fila["Divi."] = CEF.TABLA.Rows[j]["Divi."].ToString();
                        fila["CLAVE"] = CEF.TABLA.Rows[j]["CLAVE"].ToString();
                        fila["Codigo"] = CEF.TABLA.Rows[j]["Cuenta"].ToString();
                        fila["ImporteMD"] = double.Parse(CEF.TABLA.Rows[j]["ImporteMD"].ToString());
                        fila["Asignacion"] = (CEF.TABLA.Rows[j]["Descripcion"].ToString() + "-" + (Int32.Parse(CEF.TABLA.Rows[j]["RangoInicial"].ToString()) + (i)));//Descripcion
                        fila["Texto"] = CEF.TABLA.Rows[j]["Texto"].ToString();
                        fila["ImporteML"] = double.Parse(CEF.TABLA.Rows[j]["ImporteML"].ToString());
                        tabla.Rows.Add(fila);

                    }

                    fila = tabla.NewRow();
                    fila["Fec.docu."] = CEF.TABLA.Rows[j]["Fec.docu."].ToString();
                    fila["Clase Dto."] = CEF.TABLA.Rows[j]["Clase Dto."].ToString();
                    fila["Sociedad"] = CEF.TABLA.Rows[j]["Sociedad"].ToString();
                    fila["Fec.Con."] = CEF.TABLA.Rows[j]["Fec.Con."].ToString();
                    fila["Periodo"] = System.DateTime.Now.ToString("MM");
                    fila["Moneda"] = CEF.TABLA.Rows[j]["Moneda"].ToString();
                    fila["No.Dto."] = CEF.TABLA.Rows[j]["No.Dto."].ToString();
                    fila["Referencia"] = CEF.TABLA.Rows[j]["Referencia"].ToString();
                    fila["TxtCabDto"] = CEF.TABLA.Rows[j]["TxtCabDto"].ToString();
                    fila["Divi."] = CEF.TABLA.Rows[j]["Divi."].ToString();
                    fila["CLAVE"] = CEF.TABLA.Rows[j]["CLAVE2"].ToString();
                    fila["Codigo"] = CEF.TABLA.Rows[j]["Cuenta2"].ToString();
                    fila["ImporteMD"] = double.Parse(CEF.TABLA.Rows[j]["TotalCompra"].ToString());
                    fila["Asignacion"] = CEF.TABLA.Rows[j]["Referencia"].ToString();
                    fila["Texto"] = CEF.TABLA.Rows[j]["Texto2"].ToString();
                    fila["ImporteML"] = double.Parse(CEF.TABLA.Rows[j]["TotalCompra"].ToString());
                    tabla.Rows.Add(fila);

                }
                #endregion
            }
            if (chbTraslados.Checked == true)
            {
                #region Carga Traslados
                TrasladosEspeciesFiscalesBO TEF = new TrasladosEspeciesFiscalesBO(logApp);
                TEF.CargaUso();

                double contador = 0;
                for (int j = 0; j < TEF.totalRegistros; j++)
                {
                    CEF1.buscarprecio(TEF.TABLA.Rows[j]["RangoInicial"].ToString(), TEF.TABLA.Rows[j]["CodigoEspecie"].ToString());
                    if (CEF1.totalRegistros == 0)
                    {
                        contador = 0;
                    }
                    else
                    {
                        contador = double.Parse(CEF1.TABLA.Rows[0]["ImporteMD"].ToString());
                    }
                    int d = 0;
                    for (int i = Int32.Parse(TEF.TABLA.Rows[j]["RangoInicial"].ToString()); i <= Int32.Parse(TEF.TABLA.Rows[j]["RangoFinal"].ToString()); i++)
                    {
                        d++;
                        fila = tabla.NewRow();
                        fila["Fec.docu."] = TEF.TABLA.Rows[j]["Fec.docu."].ToString();
                        fila["Clase Dto."] = TEF.TABLA.Rows[j]["Clase Dto."].ToString();
                        fila["Sociedad"] = TEF.TABLA.Rows[j]["Sociedad"].ToString();
                        fila["Fec.Con."] = TEF.TABLA.Rows[j]["Fec.Con."].ToString();
                        fila["Periodo"] = System.DateTime.Now.ToString("MM");
                        fila["Moneda"] = TEF.TABLA.Rows[j]["Moneda"].ToString();
                        fila["No.Dto."] = TEF.TABLA.Rows[j]["No.Dto."].ToString();
                        fila["Referencia"] = TEF.TABLA.Rows[j]["Referencia"].ToString();
                        fila["TxtCabDto"] = TEF.TABLA.Rows[j]["TxtCabDto"].ToString();
                        fila["Divi."] = TEF.TABLA.Rows[j]["Divi."].ToString();
                        fila["CLAVE"] = TEF.TABLA.Rows[j]["CLAVE"].ToString();
                        fila["Codigo"] = TEF.TABLA.Rows[j]["Cuenta2"].ToString();
                        fila["ImporteMD"] = contador;
                        fila["Asignacion"] = (TEF.TABLA.Rows[j]["CodigoEspecie"].ToString() + "-" + (Int32.Parse(TEF.TABLA.Rows[j]["RangoInicial"].ToString()) + (d - 1)));//CodigoEspecie
                        fila["Texto"] = TEF.TABLA.Rows[j]["Texto2"].ToString();
                        fila["ImporteML"] = contador;
                        tabla.Rows.Add(fila);
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        fila = tabla.NewRow();
                        fila["Fec.docu."] = TEF.TABLA.Rows[j]["Fec.docu."].ToString();
                        fila["Clase Dto."] = TEF.TABLA.Rows[j]["Clase Dto."].ToString();
                        fila["Sociedad"] = TEF.TABLA.Rows[j]["Sociedad"].ToString();
                        fila["Fec.Con."] = TEF.TABLA.Rows[j]["Fec.Con."].ToString();
                        fila["Periodo"] = System.DateTime.Now.ToString("MM");
                        fila["Moneda"] = TEF.TABLA.Rows[j]["Moneda"].ToString();
                        fila["No.Dto."] = TEF.TABLA.Rows[j]["No.Dto."].ToString();
                        fila["Referencia"] = TEF.TABLA.Rows[j]["Referencia"].ToString();
                        fila["TxtCabDto"] = TEF.TABLA.Rows[j]["TxtCabDto"].ToString();
                        fila["Divi."] = TEF.TABLA.Rows[j]["Divi."].ToString();
                        fila["CLAVE"] = TEF.TABLA.Rows[j]["CLAVE2"].ToString();
                        fila["Codigo"] = TEF.TABLA.Rows[j]["Cuenta"].ToString();
                        fila["ImporteMD"] = contador;
                        fila["Asignacion"] = (TEF.TABLA.Rows[j]["CodigoEspecie"].ToString() + "-" + (Int32.Parse(TEF.TABLA.Rows[j]["RangoInicial"].ToString()) + (d - 1)));
                        fila["Texto"] = TEF.TABLA.Rows[j]["Texto"].ToString();
                        fila["ImporteML"] = contador;
                        tabla.Rows.Add(fila);

                    }

                }
                #endregion
            }
            if (chbAnulacion.Checked == true)
            {
                #region Carga Anulacion Sin Uso
                AnulacionesEspeciesFiscalesBO AEF = new AnulacionesEspeciesFiscalesBO(logApp);
                AEF.CargaUso();
                for (int j = 0; j < AEF.totalRegistros; j++)
                {
                    fila = tabla.NewRow();
                    fila["Fec.docu."] = AEF.TABLA.Rows[j]["Fec.docu."].ToString();
                    fila["Clase Dto."] = AEF.TABLA.Rows[j]["Clase Dto."].ToString();
                    fila["Sociedad"] = AEF.TABLA.Rows[j]["Sociedad"].ToString();
                    fila["Fec.Con."] = AEF.TABLA.Rows[j]["Fec.Con."].ToString();
                    fila["Periodo"] = System.DateTime.Now.ToString("MM");
                    fila["Moneda"] = AEF.TABLA.Rows[j]["Moneda"].ToString();
                    fila["No.Dto."] = AEF.TABLA.Rows[j]["No.Dto."].ToString();
                    fila["Referencia"] = AEF.TABLA.Rows[j]["Referencia"].ToString();
                    fila["TxtCabDto"] = AEF.TABLA.Rows[j]["TxtCabDto"].ToString();
                    fila["Divi."] = AEF.TABLA.Rows[j]["Divi."].ToString();
                    fila["CLAVE"] = AEF.TABLA.Rows[j]["CLAVE"].ToString();
                    fila["Codigo"] = AEF.TABLA.Rows[j]["Cuenta"].ToString();
                    fila["ImporteMD"] = double.Parse(AEF.TABLA.Rows[j]["ImporteMD"].ToString());
                    fila["CeCo"] = AEF.TABLA.Rows[j]["CeCo"].ToString();
                    fila["Asignacion"] = (AEF.IDESPECIEFISCAL + "-" + AEF.TABLA.Rows[j]["Serie"].ToString());
                    fila["Texto"] = AEF.TABLA.Rows[j]["Texto"].ToString();
                    fila["ImporteML"] = double.Parse(AEF.TABLA.Rows[j]["ImporteMD"].ToString());
                    tabla.Rows.Add(fila);

                    fila = tabla.NewRow();
                    fila["Fec.docu."] = AEF.TABLA.Rows[j]["Fec.docu."].ToString();
                    fila["Clase Dto."] = AEF.TABLA.Rows[j]["Clase Dto."].ToString();
                    fila["Sociedad"] = AEF.TABLA.Rows[j]["Sociedad"].ToString();
                    fila["Fec.Con."] = AEF.TABLA.Rows[j]["Fec.Con."].ToString();
                    fila["Periodo"] = System.DateTime.Now.ToString("MM");
                    fila["Moneda"] = AEF.TABLA.Rows[j]["Moneda"].ToString();
                    fila["No.Dto."] = AEF.TABLA.Rows[j]["No.Dto."].ToString();
                    fila["Referencia"] = AEF.TABLA.Rows[j]["Referencia"].ToString();
                    fila["TxtCabDto"] = AEF.TABLA.Rows[j]["TxtCabDto"].ToString();
                    fila["Divi."] = AEF.TABLA.Rows[j]["Divi."].ToString();
                    fila["CLAVE"] = AEF.TABLA.Rows[j]["CLAVE2"].ToString();
                    fila["Codigo"] = AEF.TABLA.Rows[j]["Cuenta2"].ToString();
                    fila["ImporteMD"] = double.Parse(AEF.TABLA.Rows[j]["ImporteMD"].ToString());
                    fila["Asignacion"] = (AEF.IDESPECIEFISCAL + "-" + AEF.TABLA.Rows[j]["Serie"].ToString());
                    fila["Texto"] = AEF.TABLA.Rows[j]["Texto"].ToString();
                    fila["ImporteML"] = double.Parse(AEF.TABLA.Rows[j]["ImporteMD"].ToString());
                    tabla.Rows.Add(fila);

                    AEF.regSiguiente();
                }

                #endregion
            }
            if (chbLiberacion.Checked == true)
            {
                #region Carga Cambio de Especie Fiscuales
                CorreccionesBO C = new CorreccionesBO(logApp);
                C.LoadCarga();
                for (int j = 0; j < C.totalRegistros; j++)
                {
                    string codEspecieFiscal = C.TABLA.Rows[j]["Series"].ToString();
                    string serie = "";
                    string[] arreglo = codEspecieFiscal.Trim().Split(",".ToCharArray());
                    edCantidad = arreglo.Length;

                    double contador = 0;
                    for (int i = 0; i < edCantidad; i++)
                    {
                        serie = arreglo[i];
                        CEF1.buscarprecio(serie, C.TABLA.Rows[j]["CodigoEspecieFiscal"].ToString());
                        if (CEF1.totalRegistros == 0)
                        {
                            contador = 0;
                        }
                        else
                        {
                            contador = double.Parse(CEF1.TABLA.Rows[0]["ImporteMD"].ToString());
                        }

                        fila = tabla.NewRow();
                        fila["Fec.docu."] = C.TABLA.Rows[j]["Fec.docu."].ToString();
                        fila["Clase Dto."] = C.TABLA.Rows[j]["Clase Dto."].ToString();
                        fila["Sociedad"] = C.TABLA.Rows[j]["Sociedad"].ToString();
                        fila["Fec.Con."] = C.TABLA.Rows[j]["Fec.Con."].ToString();
                        fila["Periodo"] = System.DateTime.Now.ToString("MM");
                        fila["Moneda"] = C.TABLA.Rows[j]["Moneda"].ToString();
                        fila["No.Dto."] = C.TABLA.Rows[j]["No.Dto."].ToString();
                        fila["Referencia"] = C.TABLA.Rows[j]["Referencia"].ToString();
                        fila["TxtCabDto"] = C.TABLA.Rows[j]["TxtCabDto"].ToString();
                        fila["Divi."] = C.TABLA.Rows[j]["Divi."].ToString();
                        fila["CLAVE"] = C.TABLA.Rows[j]["CLAVE2"].ToString();
                        fila["Codigo"] = C.TABLA.Rows[j]["Codigo"].ToString();
                        fila["ImporteMD"] = contador;
                        fila["Asignacion"] = (C.TABLA.Rows[j]["CodigoEspecieFiscal"].ToString() + "-" + serie.ToString());
                        fila["Texto"] = C.TABLA.Rows[j]["Texto"].ToString();
                        fila["ImporteML"] = contador;
                        fila["Referencia 3"] = C.TABLA.Rows[j]["Asignacion1"].ToString();
                        tabla.Rows.Add(fila);

                        fila = tabla.NewRow();
                        fila["Fec.docu."] = C.TABLA.Rows[j]["Fec.docu."].ToString();
                        fila["Clase Dto."] = C.TABLA.Rows[j]["Clase Dto."].ToString();
                        fila["Sociedad"] = C.TABLA.Rows[j]["Sociedad"].ToString();
                        fila["Fec.Con."] = C.TABLA.Rows[j]["Fec.Con."].ToString();
                        fila["Periodo"] = System.DateTime.Now.ToString("MM");
                        fila["Moneda"] = C.TABLA.Rows[j]["Moneda"].ToString();
                        fila["No.Dto."] = C.TABLA.Rows[j]["No.Dto."].ToString();
                        fila["Referencia"] = C.TABLA.Rows[j]["Referencia"].ToString();
                        fila["TxtCabDto"] = C.TABLA.Rows[j]["TxtCabDto"].ToString();
                        fila["Divi."] = C.TABLA.Rows[j]["Divi."].ToString();
                        fila["CLAVE"] = C.TABLA.Rows[j]["CLAVE"].ToString();
                        fila["Codigo"] = C.TABLA.Rows[j]["Cuenta2"].ToString();
                        fila["ImporteMD"] = contador;
                        fila["Asignacion"] = C.TABLA.Rows[j]["Asignacion1"].ToString();
                        fila["Texto"] = C.TABLA.Rows[j]["Texto"].ToString();
                        fila["ImporteML"] = contador;
                        fila["Referencia 3"] = (C.TABLA.Rows[j]["CodigoEspecieFiscal"].ToString() + "-" + serie.ToString());
                        tabla.Rows.Add(fila);

                        C.regSiguiente();
                    }
                }

                #endregion
            }
            rgCarga.DataSource = tabla;
            rgCarga.DataBind();

        }
        catch { }

        finally { desconectar(); }

    }



    protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName)
                ConfigureExport();

            conectar();
          
            if (chbUso.Checked == true)
            {
                EspeciesFiscalesInstruccionesBO EFI = new EspeciesFiscalesInstruccionesBO(logApp);
                CorreccionesBO C = new CorreccionesBO(logApp);
                EFI.CargaUsoCambiaEstado();
                C.Actualizar_Adiciones();
            }
            if (chbCompras.Checked == true)
            {
                ComprasEspeciesFiscalesBO CEF = new ComprasEspeciesFiscalesBO(logApp);
                CEF.CargaUsoCambiaEstado();
            }
            if (chbTraslados.Checked == true)
            {
                TrasladosEspeciesFiscalesBO TEF = new TrasladosEspeciesFiscalesBO(logApp);
                TEF.CargaUsoCambiaEstado();
            }
            if (chbAnulacion.Checked == true)
            {
                AnulacionesEspeciesFiscalesBO AEF = new AnulacionesEspeciesFiscalesBO(logApp);
                AEF.CargaUsoCambiaEstado();
            }
            if (chbLiberacion.Checked == true)
            {
                CorreccionesBO C = new CorreccionesBO(logApp);
                C.CargaReasignacionCambioEstado();
            }
        }
        catch (Exception)
        { desconectar(); }
    }
    protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        //llenarGrid();
    }
}

