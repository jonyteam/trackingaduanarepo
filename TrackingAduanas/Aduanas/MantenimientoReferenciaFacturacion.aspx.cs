﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Telerik.Web;
using Telerik.Web.UI;

public partial class MantenimientoCodigos : Utilidades.PaginaBase
{

    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Mantenimiento Facturacion";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgCategorias.FilterMenu);
        //SetGridFilterMenu(rgCodigos.FilterMenu);
        rgCategorias.Skin = Skin;
        //rgCodigos.Skin = Skin;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Mantenimiento Facturación", "Mantenimiento Facturación");
           // loadCategorias();
            //loadGridCodigos();
            this.LLenar_Facturacion();
        }
    }

    #region Categorias
    private void loadGridCategorias()
    {
        try
        {
            conectar();

            CategoriasBO bo = new CategoriasBO(logApp);
            bo.loadAllCampos();

            rgCategorias.DataSource = bo.TABLA;
            rgCategorias.DataBind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void rgCategorias_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        loadGridCategorias();
    }

    protected Hashtable GetDatosEditadosCategorias(GridItem item)
    {
        GridEditableItem editedItem = item as GridEditableItem;

        RadTextBox categoria = (RadTextBox)editedItem.FindControl("edCategoria");
        RadTextBox descripcion = (RadTextBox)editedItem.FindControl("edDescripcion");

        Hashtable newValues = new Hashtable();
        newValues.Add("Categoria", categoria.Text.Trim().ToUpper());
        newValues.Add("Descripcion", descripcion.Text.Trim());

        return newValues;
    }

    protected void rgCategorias_InsertCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridEditableItem editedItem = e.Item as GridEditableItem;

            CategoriasBO bo = new CategoriasBO(logApp);
            bo.insertCategoria(GetDatosEditadosCategorias(e.Item));
            registrarMensaje("Categoria ingresada exitosamente");
           // loadCategorias();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void rgCategorias_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridEditableItem editedItem = e.Item as GridEditableItem;
            string Referencia = editedItem["ReferenciaFacturacion"].Text;
           // Int32 Id = Convert.ToInt32(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdCategoria"]);


       
            DetalleGastosBO Detalles = new DetalleGastosBO(logApp);
            BitacoraBO Bitacora = new BitacoraBO(logApp);
            Detalles.RevertirFacturacion(Referencia);
            this.rgCategorias.DataSource = Detalles.TABLA;



            Bitacora.newLine();
            Bitacora.loadBitacora("-1");
            Bitacora.IDUSUARIO = Session["IdUsuario"].ToString();
            Bitacora.IDDOCUMENTO = Referencia;
            Bitacora.ACCION = "Revertir Facturacion";
            Bitacora.FECHA = DateTime.Today.ToShortTimeString();
            Bitacora.commitLine();
            Bitacora.actualizar();

            this.LLenar_Facturacion();

            registrarMensaje("  La  Facturación :" +Referencia + " ha sido revertida exitosamente.");

           // loadCategorias();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void rgCategorias_DeleteCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridDataItem editedItem = e.Item as GridDataItem;

            Int32 Id = Convert.ToInt32(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdCategoria"]);

            CategoriasBO bo = new CategoriasBO(logApp);
            bo.deleteCategoria(Id);
            registrarMensaje("Categoría eliminada exitosamente");
           // loadCategorias();
        }
        catch { }
        finally
        {
            desconectar();
        }

    }
    #endregion

    #region Codigos
    //private void loadCategorias()
    //{
    //    try
    //    {
    //        conectar();

    //        CategoriasBO bo = new CategoriasBO(logApp);
    //        bo.loadAllCampos();

    //        cbCategorias.DataSource = bo.TABLA;
    //        cbCategorias.DataBind();

    //        rgCodigos.Rebind();
    //    }
    //    catch { }
    //    finally
    //    {
    //        desconectar();
    //    }
    //}

    //private void loadGridCodigos()
    //{
    //    try
    //    {
    //        conectar();

    //        CodigosBO bo = new CodigosBO(logApp);
    //        bo.loadAllCampos(cbCategorias.SelectedValue);

    //        rgCodigos.DataSource = bo.TABLA;
    //        rgCodigos.DataBind();
    //    }
    //    catch { }
    //    finally
    //    {
    //        desconectar();
    //    }
    //}

    protected void rgCodigos_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        //loadGridCodigos();
    }

    protected Hashtable GetDatosEditadosCodigos(GridItem item)
    {
        GridEditableItem editedItem = item as GridEditableItem;

        RadTextBox codigo = (RadTextBox)editedItem.FindControl("edCodigo");
        RadTextBox descripcion = (RadTextBox)editedItem.FindControl("edDescripcion");
        RadNumericTextBox orden = (RadNumericTextBox)editedItem.FindControl("edOrden");

        Hashtable newValues = new Hashtable();
        newValues.Add("Codigo", codigo.Text.Trim().ToUpper());
        newValues.Add("Descripcion", descripcion.Text.Trim());

        if (orden.Value.HasValue)
            newValues.Add("Orden", Convert.ToInt32(orden.Value.Value));
        else
            newValues.Add("Orden", 0);

        return newValues;
    }

    protected void rgCodigos_InsertCommand(object source, GridCommandEventArgs e)
    {

    
 
    }

    protected void rgCodigos_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
       
      
    }

    protected void rgCodigos_DeleteCommand(object source, GridCommandEventArgs e)
    {
  

    }

    protected void cbCategorias_SelectedIndexChanged(object o, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        //rgCodigos.Rebind();
    }
    #endregion

    protected void rgCategorias_Init(object sender, EventArgs e)
    {
        GridFilterMenu menu = rgCategorias.FilterMenu;
        menu.Items.RemoveAt(rgCategorias.FilterMenu.Items.Count - 2);
    }

    protected void rgCodigos_Init(object sender, EventArgs e)
    {
        //GridFilterMenu menu = rgCodigos.FilterMenu;
        //menu.Items.RemoveAt(rgCodigos.FilterMenu.Items.Count - 2);
    }


    protected void LLenar_Facturacion()
    {
        conectar();
        DetalleGastosBO Detalles = new DetalleGastosBO(logApp);
        Detalles.loadRemisiones();
        this.rgCategorias.DataSource = Detalles.TABLA;
        desconectar();
      



    }

    protected void ImageButton1_Command(object sender, CommandEventArgs e)
    {

    }
    protected void ImageButton1_Command1(object sender, CommandEventArgs e)
    {
        conectar();
        DetalleGastosBO Detalles = new DetalleGastosBO(logApp);
        BitacoraBO Bitacora = new BitacoraBO(logApp);
        Detalles.RevertirFacturacion(e.CommandArgument.ToString().Trim());
        this.rgCategorias.DataSource = Detalles.TABLA;
    
      
        
        Bitacora.newLine();
        Bitacora.loadBitacora("-1");
        Bitacora.IDUSUARIO = Session["IdUsuario"].ToString();
        Bitacora.IDDOCUMENTO = e.CommandArgument.ToString().Trim();
        Bitacora.ACCION = "Revertir Facturacion";
        Bitacora.FECHA = DateTime.Today.ToShortTimeString();
        Bitacora.commitLine();
        Bitacora.actualizar();

        this.LLenar_Facturacion();

        registrarMensaje("  La  Facturación :" + e.CommandArgument.ToString().Trim() + " ha sido revertida exitosamente.");
  
     


    }
    protected void rgCategorias_NeedDataSource1(object sender, GridNeedDataSourceEventArgs e)
    {
        LLenar_Facturacion();
    }
}
