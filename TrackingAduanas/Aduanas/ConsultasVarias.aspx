﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ConsultasVarias.aspx.cs"
    Inherits="ReporteEventos" MasterPageFile="~/Site.master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style1
        {
            height: 73px;
            width: 422px;
        }
        .style2
        {
            width: 114px;
            height: 15px;
        }
        .style3
        {
            width: 160px;
            height: 15px;
        }
        .style4
        {
            height: 15px;
            width: 71px;
        }
        .style7
        {
            width: 114px;
            height: 26px;
        }
        .style8
        {
            width: 160px;
            height: 26px;
        }
        .style9
        {
            height: 26px;
            width: 71px;
        }
        .style10
        {
            width: 747px;
            height: 60px;
        }
        .style11
        {
            width: 160px;
        }
        .style12
        {
            width: 160px;
            height: 29px;
            text-align: center;
        }
        .style13
        {
            height: 29px;
        }
    .style14
    {
        width: 489px;
    }
    .style15
    {
        width: 489px;
        height: 29px;
        text-align: left;
    }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <br />
            <telerik:RadTabStrip ID="RadTabStrip2" runat="server" 
        MultiPageID="RadMultiPage1" SelectedIndex="0" AutoPostBack="True" 
        ontabclick="RadTabStrip2_TabClick">
                <Tabs>
                    <telerik:RadTab runat="server" Owner="RadTabStrip2" PageViewID="Consulta" 
                        Text="Consulta" Selected="True">
                    </telerik:RadTab>
                    <telerik:RadTab runat="server" Owner="RadTabStrip2" PageViewID="Prueba" 
                        Text="Cambio Regimen" SelectedIndex="1">
                    </telerik:RadTab>
                </Tabs>
    </telerik:RadTabStrip>
    <br />
    <telerik:RadMultiPage ID="RadMultiPage1" Runat="server" SelectedIndex="0">
        <telerik:RadPageView ID="Consulta" runat="server" Height="112px" Width="1053px">
            <table class="style1">
                <tr>
                    <td class="style7">
                        <asp:Label ID="Label2" runat="server" Text="Seleccione Especie"></asp:Label>
                    </td>
                    <td class="style8">
                        <telerik:RadComboBox ID="cmbespeciefiscal" Runat="server" AutoPostBack="True" 
                            DataTextField="Descripcion" DataValueField="Codigo">
                        </telerik:RadComboBox>
                    </td>
                    <td class="style9" style="margin-left: 40px">
                    </td>
                </tr>
                <tr>
                    <td class="style2">
                        <asp:Label ID="Label1" runat="server" Text="Ingrese Rango"></asp:Label>
                    </td>
                    <td class="style3">
                        <telerik:RadTextBox ID="txtrango" Runat="server" EmptyMessage="Ingrese Rango" 
                            LabelWidth="64px" Width="160px">
                        </telerik:RadTextBox>
                    </td>
                    <td class="style4" style="margin-left: 40px">
                        <telerik:RadButton ID="btnbuscar" runat="server" Height="22px" 
                            onclick="RadButton1_Click" Text="Buscar" Width="67px">
                        </telerik:RadButton>
                    </td>
                </tr>
            </table>
            <br />
            <telerik:RadGrid ID="rgrango1" runat="server" AllowFilteringByColumn="True" 
                AllowPaging="True" AllowSorting="True" CellSpacing="0" GridLines="None" 
                onneeddatasource="rgrango1_NeedDataSource">
                <ClientSettings AllowColumnsReorder="True">
                    <Selecting AllowRowSelect="True" />
                </ClientSettings>
                <MasterTableView>
                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                        Visible="True">
                        <HeaderStyle Width="20px" />
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                        Visible="True">
                        <HeaderStyle Width="20px" />
                    </ExpandCollapseColumn>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                </MasterTableView>
                <HeaderStyle Width="100px" />
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
            <br />
            <telerik:RadGrid ID="rgrango" runat="server" AllowFilteringByColumn="True" 
                AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" 
                CellSpacing="0" GridLines="None" OnNeedDataSource="rgrango_NeedDataSource" 
                PageSize="20" Visible="False">
                <ClientSettings AllowColumnsReorder="True">
                    <Selecting AllowRowSelect="True" />
                    <Selecting AllowRowSelect="True" />
                </ClientSettings>
                <MasterTableView>
                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                        Visible="True">
                        <HeaderStyle Width="20px" />
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                        Visible="True">
                        <HeaderStyle Width="20px" />
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridBoundColumn DataField="HojaRuta" 
                            FilterControlAltText="Filter HojaRuta column" FilterControlWidth="55px" 
                            HeaderText="HojaRuta" UniqueName="HojaRuta">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Cliente" 
                            FilterControlAltText="Filter Cliente column" FilterControlWidth="175px" 
                            HeaderText="Cliente" UniqueName="Cliente">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Especie" 
                            FilterControlAltText="Filter Especie column" FilterControlWidth="100px" 
                            HeaderText="Especie" UniqueName="Especie">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Vesta" 
                            FilterControlAltText="Filter Vesta column" FilterControlWidth="110px" 
                            HeaderText="Vesta Especie" UniqueName="Vesta">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="ClienteB" 
                            FilterControlAltText="Filter Cliente column" FilterControlWidth="115px" 
                            HeaderText="Cliente Especie" UniqueName="ClienteB">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Serie" 
                            FilterControlAltText="Filter Serie column" HeaderText="Serie" 
                            UniqueName="Serie">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Observacion" 
                            FilterControlAltText="Filter Observacion column" FilterControlWidth="125px" 
                            HeaderText="Observacion" UniqueName="Observacion">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Fecha" 
                            FilterControlAltText="Filter Fecha column" FilterControlWidth="110px" 
                            HeaderText="Fecha" UniqueName="Fecha">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Asignado" 
                            FilterControlAltText="Filter Asignado column" FilterControlWidth="120px" 
                            HeaderText="Asignado Por" UniqueName="Asignado">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                </MasterTableView>
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
            <br />
            <br />
        </telerik:RadPageView>
        <telerik:RadPageView ID="Prueba" runat="server" Height="297px">
            <br />
            <br />
            <table class="style10">
                <tr>
                    <td class="style11">
                        Ingrese una Hoja de Ruta:
                    </td>
                    <td class="style14">
                        <telerik:RadTextBox ID="txthoja" Runat="server" 
                            EmptyMessage="Ingrese una Hoja de Ruta" LabelWidth="64px" Width="160px" 
                            Enabled="False" Visible="False">
                        </telerik:RadTextBox>
                    </td>
                    <td style="text-align: center">
                        <telerik:RadButton ID="btnbuscarr" runat="server" onclick="btnbuscarr_Click" 
                            Text="Buscar" Enabled="False" Visible="False">
                        </telerik:RadButton>
                    </td>
                </tr>
                <tr>
                    <td class="style12">
                    </td>
                    <td class="style15">
                        <telerik:RadComboBox ID="cmbregimen" Runat="server" 
                            DataTextField="Regimen" DataValueField="Codigo" Width="100%" 
                            Enabled="False" Visible="False">
                        </telerik:RadComboBox>
                    </td>
                    <td class="style13" style="text-align: center">
                        <telerik:RadButton ID="btncambiar" runat="server" onclick="RadButton1_Click1" 
                            Text="Cambiar" Enabled="False" Visible="False">
                        </telerik:RadButton>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <telerik:RadGrid ID="rgRegimen" runat="server" AllowFilteringByColumn="True" 
                AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" 
                CellSpacing="0" GridLines="None" 
                onneeddatasource="rgRegimen_NeedDataSource" Visible="False">
                <ClientSettings AllowColumnsReorder="True">
                    <Selecting AllowRowSelect="True" />
                </ClientSettings>
                <MasterTableView>
                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                        Visible="True">
                        <HeaderStyle Width="20px" />
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                        Visible="True">
                        <HeaderStyle Width="20px" />
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridBoundColumn DataField="IdGestionCarga" 
                            FilterControlAltText="Filter IdGestionCarga column" UniqueName="IdGestionCarga" 
                            Visible="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="IdInstruccion" 
                            FilterControlAltText="Filter IdInstruccion column" HeaderText="Hoja Ruta" 
                            UniqueName="IdInstruccion">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="IdEstadoFlujo" 
                            FilterControlAltText="Filter IdEstadoFlujo column" UniqueName="IdEstadoFlujo" 
                            Visible="False">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="EstadoFlujo" 
                            FilterControlAltText="Filter EstadoFlujo column" HeaderText="Estado Flujo" 
                            UniqueName="EstadoFlujo">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="FechaIngreso" 
                            FilterControlAltText="Filter FechaIngreso column" HeaderText="Fecha Ingreso" 
                            UniqueName="FechaIngreso">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Observacion" 
                            FilterControlAltText="Filter Observacion column" HeaderText="Observacion" 
                            UniqueName="Observacion">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CodRegimen" 
                            FilterControlAltText="Filter CodRegimen column" HeaderText="Regimen" 
                            UniqueName="CodRegimen">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Nombre" 
                            FilterControlAltText="Filter Nombre column" HeaderText="Nombre" 
                            UniqueName="Nombre">
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings>
                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                        </EditColumn>
                    </EditFormSettings>
                </MasterTableView>
                <FilterMenu EnableImageSprites="False">
                </FilterMenu>
            </telerik:RadGrid>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <br />
            <br />
    
    <br />
            <br />
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgUsuarios">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgUsuarios" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rgRoles">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgRoles" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>
