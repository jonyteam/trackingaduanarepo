﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="MantenimientoDocumentosRegimen.aspx.cs" Inherits="MantenimientoDocumentosRegimen" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadGrid ID="rgDocumentos" runat="server" AllowFilteringByColumn="True" AllowSorting="True"
        AutoGenerateColumns="False" GridLines="None" Width="100%" Height="420px" OnNeedDataSource="rgDocumentos_NeedDataSource"
        OnUpdateCommand="rgDocumentos_UpdateCommand" OnDeleteCommand="rgDocumentos_DeleteCommand"
        OnInsertCommand="rgDocumentos_InsertCommand" AllowPaging="True" ShowFooter="True"
        ShowStatusBar="True" PageSize="20" OnInit="rgDocumentos_Init" OnItemCreated="rgClientes_ItemCreated"
        OnItemDataBound="rgClientes_ItemDataBound">
        <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
            PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
            Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
        <MasterTableView DataKeyNames="CodRegimen,CodDocumento" CommandItemDisplay="TopAndBottom"
            NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay documentos definidos."
            GroupLoadMode="Client">
            <CommandItemSettings AddNewRecordText="Agregar Documento" RefreshText="Volver a Cargar Datos" />
            <RowIndicatorColumn>
                <HeaderStyle Width="20px"></HeaderStyle>
            </RowIndicatorColumn>
            <ExpandCollapseColumn>
                <HeaderStyle Width="20px"></HeaderStyle>
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridEditCommandColumn AutoPostBackOnFilter="True" CancelText="Cancelar"
                    EditText="Editar" InsertText="Insertar" UpdateText="Grabar" ButtonType="ImageButton">
                    <HeaderStyle Width="5%" />
                    <ItemStyle Width="5%" />
                </telerik:GridEditCommandColumn>
                <telerik:GridButtonColumn ConfirmText="¿Esta seguro que desea eliminar este Documento?"
                    ConfirmDialogType="RadWindow" ConfirmTitle="Eliminar Documento" UniqueName="DeleteColumn"
                    ButtonType="ImageButton" CommandName="Delete">
                    <HeaderStyle Width="5%" />
                    <ItemStyle Width="5%" />
                </telerik:GridButtonColumn>
                <telerik:GridBoundColumn DataField="Pais" HeaderText="País" UniqueName="Pais" FilterControlWidth="80%">
                    <HeaderStyle Width="16%" />
                    <ItemStyle Width="16%" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Regimen" HeaderText="Régimen" UniqueName="Regimen"
                    FilterControlWidth="80%">
                    <HeaderStyle Width="16%" />
                    <ItemStyle Width="16%" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Documento" HeaderText="Documento" UniqueName="Documento"
                    FilterControlWidth="80%">
                    <HeaderStyle Width="56%" />
                    <ItemStyle Width="56%" />
                </telerik:GridBoundColumn>
            </Columns>
            <GroupByExpressions>
                <telerik:GridGroupByExpression>
                    <SelectFields>
                        <telerik:GridGroupByField FieldAlias="Regimen" FieldName="Regimen"></telerik:GridGroupByField>
                    </SelectFields>
                    <GroupByFields>
                        <telerik:GridGroupByField FieldAlias="Regimen" FieldName="Regimen"></telerik:GridGroupByField>
                    </GroupByFields>
                </telerik:GridGroupByExpression>
            </GroupByExpressions>
            <EditFormSettings EditFormType="Template" CaptionDataField="Centros" CaptionFormatString="">
                <EditColumn UniqueName="EditCommandColumn1">
                </EditColumn>
                <FormTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# ((Container is GridEditFormInsertItem) ? "Agregar Nueva Aduana" : "Editar Aduana Existente") %>'
                        Font-Size="Medium" Font-Bold="true" />
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 10%">
                            </td>
                            <td style="width: 90%">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 10%">
                                <asp:Label ID="lblPais" runat="server" Text="Pais" Font-Bold="true" />
                            </td>
                            <td style="width: 90%">
                                <telerik:RadComboBox ID="edPais" runat="server" Skin='<%# Skin %>' DataTextField="Descripcion"
                                    Filter="Contains" DataValueField="Codigo" DataSource='<%# GetPaises() %>' SelectedValue='<%# Bind("CodPais") %>'
                                    EmptyMessage="Seleccione el Pais" Width="50%" CausesValidation="false" Enabled='<%# (Container is GridEditFormInsertItem) ? true : false %>'
                                    ForeColor="Black" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 10%">
                                <asp:Label ID="lblRegimen" runat="server" Text="Régimen" Font-Bold="true" />
                            </td>
                            <td style="width: 90%">
                                <telerik:RadComboBox ID="edRegimen" runat="server" Skin='<%# Skin %>' DataTextField="CodigoDescripcion"
                                    Filter="Contains" DataValueField="Codigo" EmptyMessage="Seleccione el Régimen"
                                    Width="80%" CausesValidation="false" Enabled='<%# (Container is GridEditFormInsertItem) ? true : false %>'
                                    ForeColor="Black" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 10%">
                                <asp:Label ID="lblDocumento" runat="server" Text="Documento" Font-Bold="true" />
                            </td>
                            <td style="width: 90%">
                                <telerik:RadComboBox ID="edDocumento" runat="server" Skin='<%# Skin %>' DataTextField="Descripcion"
                                    Filter="Contains" DataValueField="Codigo" DataSource='<%# GetDocumentos() %>'
                                    SelectedValue='<%# Bind("CodDocumento") %>' EmptyMessage="Seleccione el Documento"
                                    Width="80%" CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                    <asp:ImageButton ID="btnSave" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                        AlternateText='<%# (Container is GridEditFormInsertItem) ? "Agregar Documento" : "Actualizar Documento" %>'
                        ImageUrl='Images/16/check2_16.png' />
                    &nbsp
                    <asp:ImageButton ID="btnCancel" runat="server" CommandName="Cancel" AlternateText="Cancelar"
                        ImageUrl='Images/16/forbidden_16.png' CausesValidation="false" />
                </FormTemplate>
            </EditFormSettings>
        </MasterTableView>
        <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
            <Selecting AllowRowSelect="True" />
            <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                DropHereToReorder="Suelte aquí para Re-Ordenar" />
            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
        </ClientSettings>
        <FilterMenu EnableTheming="True">
            <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
        </FilterMenu>
        <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
        <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
    </telerik:RadGrid>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgAduanas">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgAduanas" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>
