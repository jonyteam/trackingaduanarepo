﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MantenimientoClientes.aspx.cs"
    Inherits="MantenimientoClientes" MasterPageFile="~/Site.master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadMultiPage ID="RadMultiPageClientes" runat="server" SelectedIndex="0"
        Width="99%">
        <telerik:RadPageView ID="pvClientes" runat="server" Width="100%">
            <table>
                <tr>
                    <td>
                        <input id="edMonRemFin" runat="server" type="hidden" />
                    </td>
                </tr>
            </table>
            <telerik:RadGrid ID="rgClientes" runat="server" AllowFilteringByColumn="True" AllowSorting="True"
                AutoGenerateColumns="False" GridLines="Horizontal" Width="100%" Height="550px"
                OnNeedDataSource="rgClientes_NeedDataSource" OnUpdateCommand="rgClientes_UpdateCommand"
                OnDeleteCommand="rgClientes_DeleteCommand" OnInsertCommand="rgClientes_InsertCommand"
                AllowPaging="True" ShowFooter="True" ShowStatusBar="True" PageSize="20" OnItemDataBound="rgClientes_ItemDataBound">
                <HeaderContextMenu EnableTheming="True">
                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                </HeaderContextMenu>
                <PagerStyle NextPagesToolTip="Páginas Siguientes" NextPageToolTip="Página Siguiente"
                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                <MasterTableView DataKeyNames="CodigoCliente,CodigoSAP" CommandItemDisplay="<%# Ingresar ? GridCommandItemDisplay.Top : GridCommandItemDisplay.None %>"
                    NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay Clientes definidos.">
                    <CommandItemSettings ShowAddNewRecordButton="false" AddNewRecordText="Agregar Clientes"
                        RefreshText="Volver a Cargar Datos" />
                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridEditCommandColumn AutoPostBackOnFilter="True" CancelText="Cancelar"
                            EditText="Editar" InsertText="Insertar" UpdateText="Grabar" ButtonType="ImageButton"
                            HeaderText="Editar">
                            <HeaderStyle Width="5%" />
                            <ItemStyle Width="5%" />
                        </telerik:GridEditCommandColumn>
                        <telerik:GridButtonColumn ConfirmText="¿Esta seguro de que desea eliminar este cliente?"
                            ConfirmDialogType="RadWindow" ConfirmTitle="Eliminar Cliente" UniqueName="DeleteColumn"
                            ButtonType="ImageButton" CommandName="Delete" HeaderText="Eliminar">
                            <HeaderStyle Width="5%" />
                            <ItemStyle Width="5%" />
                        </telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="RTN" HeaderText="RTN" UniqueName="RTN" FilterControlWidth="50%">
                            <HeaderStyle Width="8%" />
                            <ItemStyle Width="8%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CodigoSAP" HeaderText="Codigo SAP" UniqueName="CodigoSAP"
                            FilterControlWidth="50%">
                            <HeaderStyle Width="8%" />
                            <ItemStyle Width="8%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Nombre" HeaderText="Nombre" UniqueName="Nombre"
                            FilterControlWidth="70%">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="OficialCuenta" HeaderText="OficialCuenta" UniqueName="OficialCuenta"
                            FilterControlWidth="70%">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="eMail" HeaderText="eMail" UniqueName="eMail"
                            FilterControlWidth="70%">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Contacto" HeaderText="Contacto" UniqueName="Contacto"
                            FilterControlWidth="70%">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Telefono" HeaderText="Teléfono" UniqueName="Telefono"
                            FilterControlWidth="70%">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Usuario" HeaderText="Usuario" UniqueName="Usuario"
                            FilterControlWidth="70%">
                            <HeaderStyle Width="8%" />
                            <ItemStyle Width="8%" />
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings EditFormType="Template" CaptionDataField="Categoria" CaptionFormatString="">
                        <FormTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# ((Container is GridEditFormInsertItem) ? "Agregar Nuevo Cliente" : "Editar Cliente Existente") %>'
                                Font-Size="Medium" Font-Bold="true" />
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 12%">
                                    </td>
                                    <td style="width: 88%">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%">
                                        <asp:Label ID="lblCodigoSAP" runat="server" Text="Código SAP" Font-Bold="true" />
                                    </td>
                                    <td style="width: 85%">
                                        <telerik:RadTextBox ID="edCodigoSAP" runat="server" Skin='<%# Skin %>' Text='<%# Bind("CodigoSAP") %>'
                                            EmptyMessage="Escriba el código SAP de la empresa aquí" Width="50%" Enabled='<%# (Container is GridEditFormInsertItem) ? true : false %>'
                                            Font-Bold='<%# (Container is GridEditFormInsertItem) ? false : true %>' />
                                        <asp:RequiredFieldValidator ID="rfCodigoSAP" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="El Código SAP de la Empresa no puede estar vacío"
                                            ControlToValidate="edCodigoSAP" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%">
                                        <asp:Label ID="lblRTNEmpresa" runat="server" Text="RTN" Font-Bold="true" />
                                    </td>
                                    <td style="width: 85%">
                                        <telerik:RadTextBox ID="edRTN" runat="server" Skin='<%# Skin %>' Text='<%# Bind("RTN") %>'
                                            EmptyMessage="Escriba el RTN del cliente aquí" Width="50%" Font-Bold='<%# (Container is GridEditFormInsertItem) ? false : true %>' />
                                        <asp:RequiredFieldValidator ID="rfRTN" runat="server" EnableClientScript="true" Display="Dynamic"
                                            ErrorMessage="El RTN del cliente no puede estar vacío" ControlToValidate="edRTN" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%">
                                        <asp:Label ID="lblNombre" runat="server" Text="Nombre" Font-Bold="true" />
                                    </td>
                                    <td style="width: 85%">
                                        <telerik:RadTextBox ID="edNombre" runat="server" Skin='<%# Skin %>' Text='<%# Bind("Nombre") %>'
                                            EmptyMessage="Escriba el nombre del cliente aquí" Width="50%" />
                                        <asp:RequiredFieldValidator ID="rfNombre" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="El Nombre del cliente no puede estar vacío" ControlToValidate="edNombre" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%">
                                        <asp:Label ID="lblDireccion" runat="server" Text="Direccion" Font-Bold="true" />
                                    </td>
                                    <td style="width: 85%">
                                        <telerik:RadTextBox ID="edDireccion" runat="server" Skin='<%# Skin %>' Text='<%# Bind("Direccion") %>'
                                            EmptyMessage="Escriba la dirección del cliente aquí" Width="50%" TextMode="MultiLine" />
                                        <asp:RequiredFieldValidator ID="rfDireccionEmpresa" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="La Dirección del cliente no puede estar vacía"
                                            ControlToValidate="edDireccion" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%">
                                        <asp:Label ID="lblOficialCuenta" runat="server" Text="Oficial Cuenta" Font-Bold="true" />
                                    </td>
                                    <td style="width: 85%">
                                        <telerik:RadComboBox runat="server" ID="edOficialCuenta" DataTextField="Nombre" DataValueField="IdOficialCuenta"
                                            DataSource='<%# GetOficialCuenta()%>' SelectedValue='<%# Bind("IdOficialCuenta") %>'
                                            EmptyMessage="Seleccione un Oficial de Cuenta" Width="50%"></telerik:RadComboBox>

                                        <%--<telerik:RadTextBox ID="edOficialCuenta" runat="server" Skin='<%# Skin %>' Text='<%# Bind("OficialCuenta") %>'
                                            EmptyMessage="Escriba el nombre del oficial de cuenta aquí" Width="50%" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="El nombre del oficial de cuenta no puede estar vacío"
                                            ControlToValidate="edOficialCuenta" />--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%">
                                        <asp:Label ID="lblContacto" runat="server" Text="Contacto" Font-Bold="true" />
                                    </td>
                                    <td style="width: 85%">
                                        <telerik:RadTextBox ID="edContacto" runat="server" Skin='<%# Skin %>' Text='<%# Bind("Contacto") %>'
                                            EmptyMessage="Escriba el nombre del contacto quí" Width="50%" />
                                        <asp:RequiredFieldValidator ID="rfContacto" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="El nombre del contacto no puede estar vacío"
                                            ControlToValidate="edContacto" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%">
                                        <asp:Label ID="lblTelefono" runat="server" Text="Telefono" Font-Bold="true" />
                                    </td>
                                    <td style="width: 85%">
                                        <telerik:RadTextBox ID="edTelefono" runat="server" Skin='<%# Skin %>' Text='<%# Bind("Telefono") %>'
                                            EmptyMessage="Escriba el teléfono del contacto aquí" Width="50%" />
                                        <asp:RequiredFieldValidator ID="rfTelefono" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="El teléfono del contacto no puede estar vacío"
                                            ControlToValidate="edTelefono" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%">
                                        <asp:Label ID="lbleMail" runat="server" Text="eMail" Font-Bold="true" />
                                    </td>
                                    <td style="width: 85%">
                                        <telerik:RadTextBox ID="edEMail" runat="server" Skin='<%# Skin %>' Text='<%# Bind("eMail") %>'
                                            EmptyMessage="Escriba el eMail aquí" Width="50%">
                                        </telerik:RadTextBox>
                                        <asp:RequiredFieldValidator ID="rfEMail" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="El eMail no puede estar vacío" ControlToValidate="edEMail" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%">
                                        <asp:Label ID="lblCodigoPais" runat="server" Text="País" Font-Bold="true" />
                                    </td>
                                    <td style="width: 85%">
                                        <telerik:RadComboBox ID="edCodigoPais" runat="server" Skin='<%# Skin %>' DataTextField="Descripcion"
                                            Filter="Contains" DataValueField="Codigo" DataSource='<%# GetPaises() %>' SelectedValue='<%# Bind("CodigoPais") %>'
                                            EmptyMessage="Seleccione el País" Width="50%" CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                            <asp:ImageButton ID="btnSave" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                AlternateText='<%# (Container is GridEditFormInsertItem) ? "Agregar Clientes" : "Actualizar Clientes" %>'
                                ImageUrl='Images/16/check2_16.png' />
                            &nbsp
                            <asp:ImageButton ID="btnCancel" runat="server" CommandName="Cancel" AlternateText="Cancelar"
                                ImageUrl='Images/16/forbidden_16.png' CausesValidation="false" />
                        </FormTemplate>
                    </EditFormSettings>
                </MasterTableView>
                <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                    <Selecting AllowRowSelect="True" />
                    <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                        DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                        DropHereToReorder="Suelte aquí para Re-Ordenar" />
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                </ClientSettings>
                <FilterMenu EnableTheming="True">
                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                </FilterMenu>
                <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
            </telerik:RadGrid>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgClientes">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgClientes" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>
