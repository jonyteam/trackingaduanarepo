﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteHagamoda.aspx.cs" Inherits="GenerarArchivoSAPDinant" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style12
        {
            width: 100%;
            height: 50px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="pnlRequestStarted">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">

                function pnlRequestStarted(ajaxPanel, eventArgs) {
                    try {
                        if ((eventArgs.EventTarget == "<%= btnSalvar.UniqueID %>")) {
                            eventArgs.EnableAjax = false;
                        }
                    }
                    catch (err)
                { }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                    }
                }                                  
            </script>
        </telerik:RadScriptBlock>
        <input id="cmbMaterial" runat="server" type="hidden"  />
        <input id="cmbMedioPago" runat="server" type="hidden"  />
        <input id="cmbPagarA" runat="server" type="hidden"  />
        <input id="txtMonto" runat="server" type="hidden"  />
        <input id="txtIVA" runat="server" type="hidden"  />
        <input id="txtMontoPagar" runat="server" type="hidden"  />
        <input id="cmbMoneda" runat="server" type="hidden"  />
        <input id="edFecha" runat="server" type="hidden" />
        <input id="edHora" runat="server" type="hidden" />
        
        <br />
        <table border="1" style="width: 45%">
            <tr>
                <td style="width: 15%">
                    <asp:Label ID="lblFechaInicio" runat="server" forecolor="Black" 
                        text="Fecha Inicio:">
                </asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadDatePicker ID="dpFechaInicio" runat="server" EnableTyping="False" 
                        Width="95%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                        controltovalidate="dpFechaInicio" errormessage="Ingrese Fecha" 
                        forecolor="Red" />
                </td>
                <td style="width: 15%">
                    <asp:Label ID="lblFechaFinal" runat="server" forecolor="Black" 
                        text="Fecha Final:"></asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadDatePicker ID="dpFechaFinal" runat="server" EnableTyping="False" 
                        Width="95%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator ID="reqName" runat="server" 
                        controltovalidate="dpFechaFinal" errormessage="Ingrese Fecha" forecolor="Red" />
                </td>
                <td style="width: 10%">
                    <asp:Label ID="lblPais" runat="server" forecolor="Black" text="País:"></asp:Label>
                </td>
                <td style="width: 16%">
                    <telerik:RadComboBox ID="cmbPais" runat="server" AutoPostBack="true" 
                        DataTextField="Descripcion" DataValueField="Codigo" 
                        OnSelectedIndexChanged="cmbPais_SelectedIndexChanged">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" forecolor="Black" text="Cliente:"></asp:Label>
                </td>
                <td colspan="5">
                    <telerik:RadComboBox ID="cmbCliente" runat="server" DataTextField="Nombre" 
                        DataValueField="CodigoSAP" Filter="StartsWith" Width="100%">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="6">
                    <asp:ImageButton ID="btnBuscar" runat="server" 
                        imageurl="~/Images/24/view_24.png" onclick="btnBuscar_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="rgReporte" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" BorderStyle="Solid" 
            CellSpacing="0" GridLines="None" onitemcommand="RadGrid1_ItemCommand" 
            onneeddatasource="RadGrid1_NeedDataSource" PageSize="20" 
            style="margin-right: 0px" Width="2095px" 
            onitemdatabound="RadGrid1_ItemDataBound" 
            onupdatecommand="RadGrid1_UpdateCommand">
            <ClientSettings AllowColumnsReorder="True">
                <Selecting AllowRowSelect="True" />
            </ClientSettings>
            <ExportSettings FileName="Reporte" HideStructureColumns="true" 
                OpenInNewWindow="True">
                <Excel AutoFitImages="True" Format="Biff" />
            </ExportSettings>
            <MasterTableView CommandItemDisplay="Top" >
                <CommandItemSettings ExportToPdfText="Export to PDF" 
                    ShowAddNewRecordButton="false" ShowExportToExcelButton="false" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                    Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                    Visible="True">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="HojaRuta" 
                        FilterControlAltText="Filter HojaRuta column" HeaderText="Hoja Ruta" 
                        UniqueName="HojaRuta" FilterControlWidth="150px">
                        <ItemStyle Width="70px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Proveedor" 
                        FilterControlAltText="Filter Proveedor column" HeaderText="Proveedor"
                        UniqueName="Proveedor" FilterControlWidth="200px">
                        <ItemStyle Width="120px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Cliente" 
                        FilterControlAltText="Filter Cliente column" HeaderText="Cliente" 
                        UniqueName="Cliente">
                        <ItemStyle Width="70px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="GuiaAerea" 
                        FilterControlAltText="Filter GuiaAerea column" HeaderText="Guia Aerea" 
                        UniqueName="GuiaAerea">
                        <ItemStyle Width="90px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FacturaComercial" 
                        FilterControlAltText="Filter FacturaComercial column" HeaderText="Factura Comercial" 
                        UniqueName="FacturaComercial">
                        <ItemStyle Width="70px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Bultos" 
                        FilterControlAltText="Filter Bultos column" HeaderText="Bultos" 
                        UniqueName="Bultos">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="KGSOrigen" 
                        FilterControlAltText="Filter KGSOrigen column" HeaderText="KGS Origen" 
                        UniqueName="KGSOrigen">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PesoMiami" 
                        FilterControlAltText="Filter PesoMiami column" HeaderText="Peso Miami" 
                        UniqueName="PesoMiami">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PesoIngresoSwissport" 
                        FilterControlAltText="Filter PesoIngresoSwissport column" HeaderText="Peso Ingreso Swissport" 
                        UniqueName="PesoIngresoSwissport">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PesoSalidaSwissport" 
                        FilterControlAltText="Filter PesoSalidaSwissport column" HeaderText="Peso Salida Swissport" 
                        UniqueName="PesoSalidaSwissport">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Producto" 
                        FilterControlAltText="Filter Producto column" HeaderText="Producto" 
                        UniqueName="Producto">
                        <ItemStyle Width="140px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ETA" 
                        FilterControlAltText="Filter ETA column" HeaderText="ETA" 
                        UniqueName="ETA">
                        <ItemStyle Width="130px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ATA" 
                        FilterControlAltText="Filter ATA column" HeaderText="ATA" UniqueName="ATA">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IngresoDeposito" 
                        FilterControlAltText="Filter IngresoDeposito column" 
                        HeaderText="Ingreso Deposito" UniqueName="IngresoDeposito">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PreLiquidacion" 
                        FilterControlAltText="Filter PreLiquidacion column" 
                        HeaderText="Pre Liquidacion" UniqueName="PreLiquidacion">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AprovacionPreLiquidacion" 
                        FilterControlAltText="Filter AprovacionPreLiquidacion column" 
                        HeaderText="Aprovacion Pre-Liquidacion" UniqueName="AprovacionPreLiquidacion">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Liquidacion" 
                        FilterControlAltText="Filter Liquidacion column" HeaderText="Liquidacion" 
                        UniqueName="Liquidacion">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PagoLiquidacion" 
                        FilterControlAltText="Filter PagoLiquidacion column" 
                        HeaderText="Pago Liquidacion" UniqueName="PagoLiquidacion">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Canal" 
                        FilterControlAltText="Filter Canal column" HeaderText="Canal" 
                        UniqueName="Canal">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FechaLiberacion" 
                        FilterControlAltText="Filter FechaLiberacion column" 
                        HeaderText="Fecha Revision" UniqueName="FechaLiberacion">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FechaDespacho" 
                        FilterControlAltText="Filter FechaDespacho column" HeaderText="Fecha Liberacion" 
                        UniqueName="FechaDespacho">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FechaEntrega" 
                        FilterControlAltText="Filter FechaEntrega column" HeaderText="Fecha Despacho" 
                        UniqueName="FechaEntrega">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Comentarios" 
                        FilterControlAltText="Filter Comentarios column" HeaderText="Comentarios" 
                        UniqueName="Comentarios">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EstadoActual" 
                        FilterControlAltText="Filter EstadoActual column" HeaderText="Estado Actual" 
                        UniqueName="EstadoActual">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column column" 
                        HeaderText="Aforador" UniqueName="Aforador" DataField="Aforador">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column1 column" 
                        HeaderText="Tiempo de Proceso" UniqueName="TiempoProceso" 
                        DataField="TiempoProceso">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column2 column" 
                        HeaderText="Tiempo Proceso Arribo de Carga" UniqueName="TiempoProceso2" 
                        DataField="TiempoProceso2">
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <ItemStyle BorderColor="Black" BorderStyle="Solid" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        <table class="style12">
            <tr>
                <td style="text-align: center">
                     <asp:ImageButton ID="btnGuardar" runat="server" 
                         ImageUrl="~/Images/24/disk_blue_ok_24.png" 
                         OnClientClick="radconfirm('Esta seguro que Desea Generar el Reporte?',confirmCallBackSalvar, 300, 100); return false;" 
                         ToolTip="Salvar" Visible="False" />
                     <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="~/Images/gris.png" 
                         OnClick="btnSalvar_Click" />
                     <asp:Button ID="btnCargar" runat="server" Text="Cargar Archivo" 
                         Visible="false" />
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
    <%--<GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>--%>
</asp:Content>
