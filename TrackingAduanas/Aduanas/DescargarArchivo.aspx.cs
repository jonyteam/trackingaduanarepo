﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;

using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;
//using Microsoft.SharePoint.Client;

public partial class DescargarArchivos : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Descargar Documentos";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // RadWindowManager1.Windows.Clear();
        SetGridFilterMenu(rgIngresos.FilterMenu);
        rgIngresos.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgIngresos.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Descargar Documentos", "");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void SetVisible(TableCellCollection cells, String commandName, bool visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is ImageButton)
                {
                    ImageButton btn = (ImageButton)control;

                    if (btn.CommandName == commandName)
                        btn.Visible = visible;
                }
            }
        }
    }

    protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
    {
        if (e.Argument == "Rebind")
        {
            rgIngresos.Rebind();
        }
    }

    #region Ingresos
    protected void rgIngresos_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridIngresos();
    }

    private void llenarGridIngresos()
    {
        try
        {
            //conectar();
            //SolicitudesBO s = new SolicitudesBO(logApp);
            //string idRol = "0", tipoOrigen = "";
            //if (User.IsInRole("Operador Cliente"))
            //{
            //    idRol = "5";
            //    tipoOrigen = "Planta";
            //}
            //else if (User.IsInRole("Proveedor"))
            //{
            //    idRol = "6";
            //    tipoOrigen = "Proveedor";
            //}
            //else if (User.IsInRole("Super Administrador") || User.IsInRole("Administrador"))
            //{
            //    idRol = "5','6";
            //    tipoOrigen = "Planta','Proveedor";
            //}
            //s.loadSolicitudesAll("8,9,10,11,12", idRol, tipoOrigen);
            //rgIngresos.DataSource = s.TABLA;
            //rgIngresos.DataBind();
            AduanasDataContext lo = new AduanasDataContext();
            var docs = lo.AdministrarDocumentos.Where(c => c.Eliminado == false & !c.IdUsuarioDescarga.HasValue).ToList();
            if (docs.Count > 0)
            {
                rgIngresos.DataSource = docs;
                rgIngresos.DataBind();
            }
            else
            {
                rgIngresos.DataSource = new string[] { };
            }
        }
        catch { }
        finally
        {
            //desconectar();
        }
    }

    protected void rgIngresos_Init(object sender, System.EventArgs e)
    {
        //GridFilterMenu menu = rgIngresos.FilterMenu;
        //menu.Items.RemoveAt(rgIngresos.FilterMenu.Items.Count - 2);

        GridFilterMenu filterMenu = rgIngresos.FilterMenu;

        int currentItemIndex = 0;
        while (currentItemIndex < filterMenu.Items.Count)
        {
            RadMenuItem item = filterMenu.Items[currentItemIndex];
            if (item.Text.Contains("Empty") || item.Text.Contains("Null"))
            {
                filterMenu.Items.Remove(item);
            }
            else currentItemIndex++;
        }
    }

    protected void rgIngresos_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            GridDataItem editedItem = e.Item as GridDataItem;
            if (e.CommandName == "Tiempo")
            {
                //descargArchivo("/Crowley/" + e.Item.Cells[5].Text + "/" + e.Item.Cells[4].Text + " - " + e.Item.Cells[6].Text, e.Item.Cells[4].Text + " - " + e.Item.Cells[6].Text, e.Item.Cells[7].Text);
                rgIngresos.Rebind();
            }
            else if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
                ConfigureExport();
        }
        catch { }
    }

    private static string sourceURL = "http://192.168.205.121:11400/";
    //protected void descargArchivo(string fileURL, string fileName, string id)
    //{
    //    try
    //    {
    //        Stream res = DownloadFile(sourceURL, fileURL, "Administrator", ConfigurationManager.AppSettings["passSP"].ToString(), "spvesta");

    //        MemoryStream memoryStream;
    //        memoryStream = new MemoryStream();
    //        res.CopyTo(memoryStream);
    //        memoryStream.Position = 0;

    //        byte[] file = memoryStream.ToArray();
    //        if (file != null)
    //        {
    //            try
    //            {
    //                //nuevo codigo
    //                AduanasDataContext lo = new AduanasDataContext();
    //                //valida que no haya dos documentos iguales cargados
    //                var ad = lo.AdministrarDocumentos.Where(c =>
    //                    c.Id == decimal.Parse(id)).SingleOrDefault();
    //                if (ad != null)
    //                {
    //                    ad.FechaDescarga = DateTime.Now;
    //                    ad.IdUsuarioDescarga = Convert.ToDecimal(Session["IdUsuario"].ToString());
    //                    lo.SubmitChanges();
    //                }
    //            }
    //            catch { }

    //            Response.Clear();
    //            Response.ClearHeaders();
    //            Response.ClearContent();
    //            Response.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
    //            Response.AddHeader("Content-Length", file.Length.ToString());
    //            Response.ContentType = "Application/pdf";//MimeMapping.GetMimeMapping(fileName);
    //            Response.BinaryWrite(file);
    //            Response.End();
    //        }
    //        else
    //        {
    //            registrarMensaje1("Error al bajar archivo, por favor intente mas tarde.");
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        string resss = ex.Message;
    //    }
    //}

    //protected Stream DownloadFile(string siteURL, string fileURL, string user, string pass, string domain)
    //{
    //    Stream success = null;

    //    try
    //    {
    //        using (ClientContext clientContext = new ClientContext(siteURL))
    //        {
    //            clientContext.Credentials = new System.Net.NetworkCredential(user, pass, domain);

    //            FileInformation fInfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(clientContext, fileURL);

    //            success = fInfo.Stream;
    //        }
    //    }
    //    catch (Exception)
    //    {
    //    }
    //    finally
    //    {
    //    }
    //    return success;
    //}
    private void ConfigureExport()
    {
        String filename = "Ingresos_" + DateTime.Now.ToShortDateString();
        rgIngresos.GridLines = GridLines.Both;
        rgIngresos.ExportSettings.FileName = filename;
        rgIngresos.ExportSettings.IgnorePaging = true;
        rgIngresos.ExportSettings.OpenInNewWindow = false;
        rgIngresos.ExportSettings.ExportOnlyData = true;
    }
    #endregion









}