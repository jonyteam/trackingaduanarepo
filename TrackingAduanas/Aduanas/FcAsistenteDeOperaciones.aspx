﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="FcAsistenteDeOperaciones.aspx.cs" Inherits="FcAsistenteDeOperaciones" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="miDiv" runat="server" class="panelCentrado2">
        <table width="100%">
            <tr>
                <td>
                    <telerik:RadGrid ID="rgAsistenteOperaciones" runat="server" AllowFilteringByColumn="True"
                        AllowSorting="True" AutoGenerateColumns="False" GridLines="None" Width="100%"
                        Height="470px" OnNeedDataSource="rgAsistenteOperaciones_NeedDataSource" AllowPaging="True"
                        ShowFooter="True" ShowStatusBar="True" PageSize="20" OnInit="rgAsistenteOperaciones_Init"
                        OnItemCommand="rgAsistenteOperaciones_ItemCommand">
                        <HeaderContextMenu EnableTheming="True">
                            <CollapseAnimation Duration="200" Type="OutQuint" />
                        </HeaderContextMenu>
                        <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                            PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                            Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                        <MasterTableView DataKeyNames="Id,Correlativo,IdInstruccion" CommandItemDisplay="Top"
                            NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay instrucciones definidas."
                            GroupLoadMode="Client">
                            <CommandItemSettings ShowAddNewRecordButton="False" RefreshText=""
                                ShowExportToExcelButton="False" />
                            <RowIndicatorColumn>
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn>
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridBoundColumn DataField="IdInstruccion" HeaderText="IdInstruccion" UniqueName="Instruccion"
                                    FilterControlWidth="80%">
                                    <HeaderStyle Width="120px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Cliente" HeaderText="Cliente" UniqueName="Cliente"
                                    FilterControlWidth="80%">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Proveedor" HeaderText="Proveedor" UniqueName="Proveedor" FilterControlWidth="70%">
                                    <HeaderStyle Width="150px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CodRegimen" HeaderText="Regimen" UniqueName="CodRegimen"
                                    FilterControlWidth="80%">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="NoFactura" HeaderText="Factura" UniqueName="NoFactura"
                                    FilterControlWidth="80%">
                                    <HeaderStyle Width="120px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Correlativo" HeaderText="Correlativo" UniqueName="Correlativo"
                                    FilterControlWidth="80%">
                                    <HeaderStyle Width="120px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Descargar" HeaderText="Descargar"
                                    ImageUrl="Images/16/index_down_16.png" UniqueName="btnDescargar">
                                    <HeaderStyle HorizontalAlign="Center" Width="80px" />
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </telerik:GridButtonColumn>
                            </Columns>
                        </MasterTableView>
                        <HeaderStyle Width="180px" />
                        <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                            <Selecting AllowRowSelect="True" />
                            <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                                DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                                DropHereToReorder="Suelte aquí para Re-Ordenar" />
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
                        </ClientSettings>
                        <FilterMenu EnableTheming="True">
                            <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                        </FilterMenu>
                        <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                        <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
        <asp:Panel ID="Panel1" runat="server">
            <asp:Timer ID="Timer1" runat="server" Interval="180000" OnTick="Timer1_Tick" ClientIDMode="AutoID" />
        </asp:Panel>
    </div>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgAsistenteOperaciones">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgAsistenteOperaciones" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="Timer1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgAsistenteOperaciones" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>
