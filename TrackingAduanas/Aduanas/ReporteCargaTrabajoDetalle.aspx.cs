﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ObjetosDto;
using Telerik.Web.UI;
using Toolkit.Core.Extensions;
using Utilities.Web;

public partial class ReporteCargaTrabajoDetalle : Utilidades.PaginaBase
{
    private List<DetalleInstruccionesCargaTrabajo> instrucciones = new List<DetalleInstruccionesCargaTrabajo>();
    private decimal id;
    private string idAduana;
    private string Fecha;
    private string Usuario;

    protected override void OnLoad(EventArgs e)
    {
        MODULO = "ReporteHagamodaPromoda";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgReporteCargaTrabajoDetalle.FilterMenu);
        rgReporteCargaTrabajoDetalle.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgReporteCargaTrabajoDetalle.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Detalle Dia Mas Cargado.", "Detalle Dia Mas Cargado.");
        }

         id = Request.QueryString["id"].ToDecimal();
         Fecha = Request.QueryString["Fecha"];
         Usuario = Request.QueryString["Usuario"];
    }



    protected void rgReporteCargaTrabajoDetalle_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        LlenarGrid();
    }

    private void LlenarGrid()
    {
        if(Usuario == "Tramitador")
        {
            instrucciones = CargarDetalleTramitadoresFechaMaxima(id, Fecha);
        }
        else if(id.ToString() == "0")
        {
            instrucciones = CargarDetalleSinOficialesCuentaDiaMasCargado(Fecha);
        }
        else
        {
            instrucciones = CargarDetalleOficialesCuentaDiaMasCargado(id, Fecha);
        }
        NombrarDias();
        rgReporteCargaTrabajoDetalle.DataSource = instrucciones;
    }

    public List<DetalleInstruccionesCargaTrabajo> CargarDetalleTramitadoresFechaMaxima(decimal idUsuario, string fecha)
    {
        var context = new AduanasDataContext();

        var instruccionesDetallePorUsuario1 = context.Instrucciones.Join(context.Clientes,
                            i => i.IdCliente,
                            c => c.CodigoSAP,
                            (i, c) => new
                            {
                                ins = i,
                                cliente = c
                            }
                            ).Where(w => w.ins.IdEstadoFlujo == "99" && w.ins.CodEstado != "100"
                                      && w.ins.FechaFinalFlujo.Value.Date == Convert.ToDateTime(fecha)
                                      && w.ins.CodRegimen != "9999"
                                      && w.ins.CodRegimen != "CB"
                                      && w.cliente.Eliminado == '0'
                                      && w.ins.Tramitador == idUsuario
                                      && w.ins.FechaFinalFlujo.Value.Date == Convert.ToDateTime(fecha))
                          .Select(s => new DetalleInstruccionesCargaTrabajo
                          {
                              Cliente = s.cliente.Nombre,
                              Instruccion = s.ins.IdInstruccion,
                              Fecha = s.ins.FechaFinalFlujo.Value.ToShortDateString(),
                              Dia = s.ins.FechaFinalFlujo.Value.DayOfWeek.ToString(),
                              Regimen = s.ins.CodRegimen,
                              Producto = s.ins.Producto,
                              CanalSelectividad = s.ins.Color
                          }).ToList();

        var instruccionesDetallePorUsuario2 = context.TiemposFlujoCarga.Join(context.Instrucciones,
           tfc => tfc.IdInstruccion,
           i => i.IdInstruccion,
           (tfc, i) => new
           {
               tiempoFC = tfc,
               ins = i
           }
           ).Join(context.Clientes,
           i => i.ins.IdCliente,
           c => c.CodigoSAP,
           (i, c) => new
           {
               ins = i,
               cliente = c
           }
        )
           .Where(tfc => tfc.ins.tiempoFC.IdEstado == "9" && tfc.ins.tiempoFC.Eliminado == false 
                  && tfc.ins.ins.CodEstado != "100" && tfc.ins.ins.IdEstadoFlujo == "99"
                  && tfc.ins.ins.FechaFinalFlujo.Value.Date == Convert.ToDateTime(fecha)
                  && tfc.ins.ins.CodRegimen != "9999"
                  && tfc.ins.ins.CodRegimen != "CB"
                  && tfc.ins.tiempoFC.IdUsuario == idUsuario
                  && tfc.ins.tiempoFC.Fecha.Date == Convert.ToDateTime(fecha)).Select(s => new DetalleInstruccionesCargaTrabajo
                  {
                      Cliente = s.cliente.Nombre,
                      Instruccion = s.ins.ins.IdInstruccion,
                      Fecha = s.ins.ins.FechaFinalFlujo.Value.ToShortDateString(),
                      Dia = s.ins.ins.FechaFinalFlujo.Value.DayOfWeek.ToString(),
                      Regimen = s.ins.ins.CodRegimen,
                      Producto = s.ins.ins.Producto,
                      CanalSelectividad = s.ins.ins.Color
                  }).ToList();

        List<DetalleInstruccionesCargaTrabajo> instruccionesDeUsuario = new List<DetalleInstruccionesCargaTrabajo>();
        instruccionesDeUsuario = instruccionesDetallePorUsuario1;
        instruccionesDeUsuario.AddRange(instruccionesDetallePorUsuario2);

        return instruccionesDeUsuario;
    }

    public List<DetalleInstruccionesCargaTrabajo> CargarDetalleOficialesCuentaDiaMasCargado(decimal idUsuario, string fecha)
    {
        var context = new AduanasDataContext();
        List<DetalleInstruccionesCargaTrabajo> instruccionesDeUsuario = new List<DetalleInstruccionesCargaTrabajo>();
        List<Instrucciones> instrucciones = new List<Instrucciones>();

        var aduanas = context.Aduanas.Where(w => w.CodEstado == "0" && w.CodPais == "H").ToList();
        var clientes = context.Clientes.Where(w => w.Eliminado == '0').ToList();

        if (idUsuario != 0)
        {
            instrucciones = context.Instrucciones.Where(w => w.CodEstado != "100" && w.IdEstadoFlujo == "99"
                                                       && w.FechaFinalFlujo.Value.Date == Convert.ToDateTime(fecha)
                                                       && w.CodRegimen != "9999"
                                                       && w.CodRegimen != "CB"
                                                       && w.IdOficialCuenta == idUsuario
                                                       && w.FechaFinalFlujo.Value.Date == Convert.ToDateTime(fecha)).ToList();
        }


        instruccionesDeUsuario = instrucciones.Join(clientes,
            i => i.IdCliente,
            c => c.CodigoSAP,
            (i, c) => new
            {
                ins = i,
                cliente = c
            }
            ).Join(aduanas,
            i => i.ins.CodigoAduana,
            a => a.CodigoAduana,
            (i, a) => new
            {
                ins = i,
                aduana = a
            }
            ).Select(s => new DetalleInstruccionesCargaTrabajo
            {
                Cliente = s.ins.cliente.Nombre,
                Instruccion = s.ins.ins.IdInstruccion,
                Fecha = s.ins.ins.FechaFinalFlujo.Value.ToShortDateString(),
                Dia = s.ins.ins.FechaFinalFlujo.Value.DayOfWeek.ToString(),
                Regimen = s.ins.ins.CodRegimen,
                Producto = s.ins.ins.Producto,
                CanalSelectividad = s.ins.ins.Color
            }).ToList();


        return instruccionesDeUsuario;
    }

    public List<DetalleInstruccionesCargaTrabajo> CargarDetalleSinOficialesCuentaDiaMasCargado(string fecha)
    {
        var context = new AduanasDataContext();
        List<DetalleInstruccionesCargaTrabajo> instruccionesDeUsuario = new List<DetalleInstruccionesCargaTrabajo>();
        List<Instrucciones> instrucciones = new List<Instrucciones>();

        var aduanas = context.Aduanas.Where(w => w.CodEstado == "0" && w.CodPais == "H").ToList();
        var clientes = context.Clientes.Where(w => w.Eliminado == '0').ToList();

        instrucciones = context.Instrucciones.Where(w => w.CodEstado != "100" && w.IdEstadoFlujo == "99"
                                                    && w.FechaFinalFlujo.Value.Date == Convert.ToDateTime(fecha)
                                                    && w.CodRegimen != "9999"
                                                    && w.CodRegimen != "CB"
                                                    && w.IdOficialCuenta == null
                                                    && w.FechaFinalFlujo.Value.Date == Convert.ToDateTime(fecha)).ToList();

        instruccionesDeUsuario = instrucciones.Join(clientes,
            i => i.IdCliente,
            c => c.CodigoSAP,
            (i, c) => new
            {
                ins = i,
                cliente = c
            }
            ).Join(aduanas,
            i => i.ins.CodigoAduana,
            a => a.CodigoAduana,
            (i, a) => new
            {
                ins = i,
                aduana = a
            }
            ).Select(s => new DetalleInstruccionesCargaTrabajo
            {
                Cliente = s.ins.cliente.Nombre,
                Instruccion = s.ins.ins.IdInstruccion,
                Fecha = s.ins.ins.FechaFinalFlujo.Value.ToShortDateString(),
                Dia = s.ins.ins.FechaFinalFlujo.Value.DayOfWeek.ToString(),
                Regimen = s.ins.ins.CodRegimen,
                Producto = s.ins.ins.Producto,
                CanalSelectividad = s.ins.ins.Color
            }).ToList();

        return instruccionesDeUsuario;
    }

    private void NombrarDias()
    {
        for (int i = 0; i < this.instrucciones.Count; i++)
        {
            var Dia = this.instrucciones[i].Dia.ToString();
            if (Dia == "1" || Dia == "Monday")
            {
                this.instrucciones[i].Dia = "Lunes";
            }
            else if (Dia == "2" || Dia == "Tuesday")
            {
                this.instrucciones[i].Dia = "Martes";
            }
            else if (Dia == "3" || Dia == "Wednesday")
            {
                this.instrucciones[i].Dia = "Miercoles";
            }
            else if (Dia == "4" || Dia == "Thursday")
            {
                this.instrucciones[i].Dia = "Jueves";
            }
            else if (Dia == "5" || Dia == "Friday")
            {
                this.instrucciones[i].Dia = "Viernes";
            }
            else if (Dia == "6" || Dia == "Saturday")
            {
                this.instrucciones[i].Dia = "Sabado";
            }
            else if (Dia == "0" || Dia == "Sunday")
            {
                this.instrucciones[i].Dia = "Domingo";
            }
        }
    }

    private void ConfigureExport(string nombre)
    {
        rgReporteCargaTrabajoDetalle.AllowFilteringByColumn = false;
        String filename = nombre + "_" + DateTime.Now.ToShortDateString();
        rgReporteCargaTrabajoDetalle.GridLines = GridLines.Both;
        rgReporteCargaTrabajoDetalle.ExportSettings.FileName = filename;
        rgReporteCargaTrabajoDetalle.ExportSettings.IgnorePaging = true;
        rgReporteCargaTrabajoDetalle.ExportSettings.OpenInNewWindow = false;
        rgReporteCargaTrabajoDetalle.ExportSettings.ExportOnlyData = true;
    }

    protected void rgReporteCargaTrabajoDetalle_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();
            if (rgReporteCargaTrabajoDetalle.Items.Count <= 0 & e.CommandName == RadGrid.ExportToExcelCommandName)
                e.Canceled = true;
            else if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                ConfigureExport("ReporteCargaTrabajoDetalle");
            }
        }
        catch (Exception)
        { }
    }

}