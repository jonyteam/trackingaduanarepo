﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteGastoAduana.aspx.cs" Inherits="ReporteGestionCarga" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 33px;
        }

        .auto-style2 {
            width: 133px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <table border="1" style="width: 45%">
        <tr>
            <td style="width: 15%">
                <asp:Label ID="lblFechaInicio" runat="server" Text="Fecha Inicio:" ForeColor="Black"></asp:Label>
            </td>
            <td style="width: 22%">
                <telerik:RadDatePicker ID="dpFechaInicio" runat="server" EnableTyping="False" Width="95%">
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="dpFechaInicio"
                    ErrorMessage="Ingrese Fecha" ForeColor="Red" />
            </td>
            <td style="width: 15%" colspan="2">
                <asp:Label ID="lblFechaFinal" runat="server" Text="Fecha Final:" ForeColor="Black"></asp:Label>
            </td>
            <td style="width: 22%">
                <telerik:RadDatePicker ID="dpFechaFinal" runat="server" EnableTyping="False" Width="95%">
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator runat="server" ID="reqName" ControlToValidate="dpFechaFinal"
                    ErrorMessage="Ingrese Fecha" ForeColor="Red" />
            </td>
            <td style="width: 10%" colspan="2">
                <asp:Label ID="lblPais" runat="server" Text="País:" ForeColor="Black"></asp:Label>
            </td>
            <td style="width: 16%">
                <telerik:RadComboBox ID="cmbPais" runat="server" DataTextField="Descripcion" DataValueField="Codigo"
                    AutoPostBack="true" OnSelectedIndexChanged="cmbPais_SelectedIndexChanged">
                </telerik:RadComboBox>
            </td>
        </tr>
        <tr>
            <td style="margin-left: 40px">
                <asp:Label ID="Label2" runat="server" Text="Cliente:" ForeColor="Black"></asp:Label>
            </td>
            <td class="auto-style1">
                <telerik:RadComboBox ID="cmbCliente" runat="server" DataTextField="Nombre" DataValueField="CodigoSAP"
                    Width="100%" Filter="StartsWith">
                </telerik:RadComboBox>
            </td>
            <td class="auto-style1">&nbsp;</td>
            <td colspan="3" class="auto-style2">
                <asp:Label ID="Label3" runat="server" Text="Aduana:" ForeColor="Black"></asp:Label>
            </td>
            <td>&nbsp;</td>
            <td>
                <telerik:RadComboBox ID="cmbAduanas" runat="server" DataTextField="NombreAduana" DataValueField="CodigoAduana">
                </telerik:RadComboBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label4" runat="server" Text="Gastos:" ForeColor="Black"></asp:Label>
            </td>
            <td class="auto-style1">
                <telerik:RadComboBox ID="cmbGastos" runat="server" DataTextField="Gasto" DataValueField="IdGasto">
                </telerik:RadComboBox>
            </td>
            <td class="auto-style1">&nbsp;</td>
            <td colspan="3" class="auto-style2">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="8" align="center">
                <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Images/24/view_24.png"
                    OnClick="btnBuscar_Click" />
            </td>
        </tr>
    </table>
    <telerik:RadGrid ID="rgReporte" runat="server" AllowFilteringByColumn="True"
        Visible="False" AllowSorting="True" GridLines="None"
        Height="410px" OnNeedDataSource="rgInstrucciones_NeedDataSource" AllowPaging="True"
        ShowFooter="True" ShowStatusBar="True" PageSize="20" OnInit="rgInstrucciones_Init"
        OnItemCommand="rgInstrucciones_ItemCommand" CellSpacing="0" Culture="es-ES" AutoGenerateColumns="False">
        <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
            PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
            Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
        <MasterTableView DataKeyNames="" CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros."
            NoMasterRecordsText="No hay instrucciones." GroupLoadMode="Client">
            <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                ShowExportToExcelButton="true" />
            <CommandItemSettings RefreshText="Volver a Cargar Datos" ExportToPdfText="Export to PDF" ShowAddNewRecordButton="False" ShowExportToExcelButton="True"></CommandItemSettings>

            <RowIndicatorColumn>
                <HeaderStyle Width="20px"></HeaderStyle>
            </RowIndicatorColumn>
            <ExpandCollapseColumn>
                <HeaderStyle Width="20px"></HeaderStyle>
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn DataField="IdInstruccion" HeaderText="Hoja de Ruta" UniqueName="IdInstruccion" FilterControlWidth="75%">
                    <HeaderStyle Width="110px" />
                    <ItemStyle Width="110px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Nombre" HeaderText="Cliente" UniqueName="Nombre" FilterControlWidth="80%">
                    <HeaderStyle Width="180px" />
                    <ItemStyle Width="180px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Gasto" HeaderText="Gasto" UniqueName="Gasto" FilterControlAltText="Filter Gasto column" FilterControlWidth="75%">
                    <HeaderStyle Width="110px" />
                    <ItemStyle Width="110px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Total" HeaderText="Total" UniqueName="Total" FilterControlAltText="Filter Total column" FilterControlWidth="65%">
                    <HeaderStyle Width="80px" />
                    <ItemStyle Width="80px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Producto" HeaderText="Producto" UniqueName="Producto" FilterControlAltText="Filter Producto column" FilterControlWidth="80%">
                    <HeaderStyle Width="150px" />
                    <ItemStyle Width="150px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FechaCreacionHoja" HeaderText="Fecha Creacion Hoja" UniqueName="FechaCreacionHoja" FilterControlAltText="Filter FechaCreacionHoja column" FilterControlWidth="82%">
                    <HeaderStyle Width="150px" />
                    <ItemStyle Width="150px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FechaCreacionGasto" HeaderText="Fecha Creacion Gasto"
                    UniqueName="FechaCreacionGasto" FilterControlAltText="Filter FechaCreacionGasto column" FilterControlWidth="82%">
                    <HeaderStyle Width="150px" />
                    <ItemStyle Width="150px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FechaEntregaServicio" HeaderText="Fecha Entrega Servicio"
                    UniqueName="FechaEntregaServicio" FilterControlAltText="Filter FechaEntregaServicio column" FilterControlWidth="82%">
                    <HeaderStyle Width="150px" />
                    <ItemStyle Width="150px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ArriboCarga" HeaderText="Arribo Carga"
                    UniqueName="ArriboCarga" FilterControlAltText="Filter ArriboCarga column" FilterControlWidth="82%">
                    <HeaderStyle Width="150px" />
                    <ItemStyle Width="150px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CodigoGuia" HeaderText="Codigo Guia" UniqueName="CodigoGuia" FilterControlAltText="Filter CodigoGuia column" FilterControlWidth="75%">
                    <HeaderStyle Width="110px" />
                    <ItemStyle Width="110px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="NumeroFactura" HeaderText="Numero Factura"
                    UniqueName="NumeroFactura" FilterControlAltText="Filter NumeroFactura column" FilterControlWidth="75%">
                    <HeaderStyle Width="150px" />
                    <ItemStyle Width="150px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Cabezal" HeaderText="Placa Cabezal"
                    UniqueName="Cabezal" FilterControlAltText="Filter Placa column" FilterControlWidth="75%">
                    <HeaderStyle Width="150px" />
                    <ItemStyle Width="150px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Furgon" HeaderText="Placa Furgon"
                    UniqueName="Furgon" FilterControlAltText="Filter Furgon column">
                </telerik:GridBoundColumn>
            </Columns>

            <EditFormSettings>
                <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
            </EditFormSettings>
        </MasterTableView>
        <HeaderStyle Width="180px" />
        <SortingSettings SortToolTip="Presione aqu&#237; para ordenar"
            SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente"></SortingSettings>

        <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
            <Selecting AllowRowSelect="True" />
            <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                DropHereToReorder="Suelte aquí para Re-Ordenar" />
            <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
            <Selecting AllowRowSelect="True"></Selecting>

            <ClientMessages DropHereToReorder="Suelte aqu&#237; para Re-Ordenar" DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tama&#241;o" PagerTooltipFormatString="P&#225;gina &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"></ClientMessages>

            <Scrolling AllowScroll="True" UseStaticHeaders="True"></Scrolling>
        </ClientSettings>

        <StatusBarSettings ReadyText="Listo" LoadingText="Cargando, por favor espere..."></StatusBarSettings>

        <FilterMenu EnableTheming="True">
            <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
        </FilterMenu>
        <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
        <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
    </telerik:RadGrid>
</asp:Content>
