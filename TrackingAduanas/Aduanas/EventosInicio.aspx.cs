﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;//
using GrupoLis.Ebase;//
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;
using System.Data;

using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;

public partial class EventosInicio : Utilidades.PaginaBase
{
    private NameValueCollection mParamethers;
    private GrupoLis.Login.Login logAppTerrestre;
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "EventosInicio";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        mParamethers = ConfigurationManager.AppSettings;
        SetGridFilterMenu(rgInstrucciones.FilterMenu);
        rgInstrucciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgInstrucciones.FilterMenu.Font.Size = FontUnit.XXSmall;


        if (!IsPostBack)
        {
            if (!Ingresar)
                redirectTo("Default.aspx");
            rgInstrucciones.ClientSettings.Scrolling.AllowScroll = true;
            rgInstrucciones.ClientSettings.Scrolling.UseStaticHeaders = true;
            rgInstrucciones.ShowHeader = true;
            rgInstrucciones.ShowFooter = true;
            ((SiteMaster)Master).SetTitulo("Iniciar Eventos", "Iniciar Eventos");
            mpInstrucciones.SelectedIndex = 0;
        }
    }

    protected Object GetEventos()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            c.loadAllCampos("EVENTOS");
            return c.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }

        return null;
    }

    protected void rgInstrucciones_Init(object sender, System.EventArgs e)
    {
        GridFilterMenu menu = rgInstrucciones.FilterMenu;
        menu.Items.RemoveAt(rgInstrucciones.FilterMenu.Items.Count - 2);
    }

    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }

    protected void rgInstrucciones_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridInstrucciones();
    }

    private void llenarGridInstrucciones()
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            if (User.IsInRole("Administradores"))
                i.loadInstrucciones();
            else if (User.IsInRole("Administrador Pais") || User.IsInRole("Jefe Centralización"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                i.loadInstruccionesXPais(u.CODPAIS);
            }
            else if (User.IsInRole("Jefes Aduana") || User.IsInRole("Aforadores") || User.IsInRole("Analista de Documentos"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.totalRegistros > 0)
                    i.loadInstruccionXAduana(u.CODIGOADUANA);
            }
            else if (User.IsInRole("Operaciones") || User.IsInRole("Oficial de Cuenta") || User.IsInRole("Inplant"))
            {//aca se hara el cambio para que cuando el pais sea igual a NI se muestren todas las hojas de ruta
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.CODPAIS == "N")
                {
                    i.loadInstruccionesXPaisUsuarioNI(u.CODPAIS);
                }
                else
                {
                    if (User.IsInRole("Operaciones") || User.IsInRole("Inplant") || User.IsInRole("Oficial de Cuenta"))
                    {
                        i.loadInstruccionesXPaisIns(u.CODPAIS);
                    }
                    else
                    {
                        //if (u.CODPAIS == "H" && User.IsInRole("Oficial de Cuenta"))
                        //{
                        //    i.loadInstruccionesXPaisyCliente(u.CODPAIS, Session["IdUsuario"].ToString());
                        //}
                    }
                }
            }

            else if (User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT"))
            {
                string idU = Session["IdUsuario"].ToString();
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.totalRegistros > 0)
                    if (u.CODPAIS == "H")
                        if (User.Identity.Name == "eizaguirre")
                        { i.loadInstruccionXAduanaUsuario("016", idU); }
                        else
                        {

                            i.loadInstruccionXAduanaUsuario(u.CODIGOADUANA, idU);
                        }
                    else
                        //i.loadInstruccionXUsuario(idU);
                        i.loadInstruccionesXPais(u.CODPAIS);
            }
            rgInstrucciones.DataSource = i.TABLA;
            rgInstrucciones.DataBind();
        }
        catch (Exception) { }
        finally
        {
            desconectar();
        }
    }

    protected void rgInstrucciones_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            GridEditableItem editedItem = e.Item as GridEditableItem;
            if (e.CommandName == "Sellar")
            {
                edInstruccion.Value = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdInstruccion"]);
                RadComboBox evento = (RadComboBox)editedItem.FindControl("edEventos");
                RadDatePicker fecha = (RadDatePicker)editedItem.FindControl("dtFechaSellado");
                RadTimePicker hora = (RadTimePicker)editedItem.FindControl("dtHoraSellado");
                RadTextBox observacion = (RadTextBox)editedItem.FindControl("edObservacion");
                if (!evento.IsEmpty)
                {
                    edEvento.Value = evento.SelectedValue;
                    edEventoDesc.Value = evento.SelectedItem.Text;
                    if (fecha.SelectedDate.HasValue)
                    {
                        edFecha.Value = fecha.SelectedDate.Value.ToShortDateString();
                        if (hora.SelectedDate.HasValue)
                        {
                            edHora.Value = hora.SelectedDate.Value.ToShortTimeString();
                            edObservacion.Value = observacion.Text.Trim();
                            iniciarEvento();
                            evento.ClearSelection();
                            fecha.Clear();
                            hora.Clear();
                            observacion.Text = "";
                            Terrestre();
                        }
                        else
                            registrarMensaje("Hora no puede estar vacía");
                    }
                    else
                        registrarMensaje("Fecha no puede estar vacía");
                }
                else
                    registrarMensaje("Evento no puede estar vacío");
            }
            else if (e.CommandName == RadGrid.ExportToExcelCommandName)
                ConfigureExport();
        }
        catch (Exception)
        { }
        finally
        {
            rgInstrucciones.Rebind();
        }
    }

    private void ConfigureExport()
    {
        String filename = "Eventos iniciados " + DateTime.Now.ToShortDateString();
        rgInstrucciones.GridLines = GridLines.Both;
        rgInstrucciones.ExportSettings.FileName = filename;
        rgInstrucciones.ExportSettings.IgnorePaging = true;
        rgInstrucciones.ExportSettings.OpenInNewWindow = false;
        rgInstrucciones.ExportSettings.ExportOnlyData = true;
    }

    private void iniciarEvento()
    {
        try
        {
            conectar();
            EventosBO e = new EventosBO(logApp);
            InstruccionesBO i = new InstruccionesBO(logApp);
            ClientesBO c = new ClientesBO(logApp);
            //verificar si los eventos se pueden repetir y cuales, validar que no inicie un evento sino esta finalizado
            e.loadEventosXInstruccionYEvento(edInstruccion.Value, edEvento.Value); //verificar esta linea no se si dejarla 
            if (edEvento.Value == "19" || edEvento.Value == "20" || edEvento.Value == "21" || edEvento.Value == "22" || edEvento.Value == "23" || edEvento.Value == "24" || edEvento.Value == "25" || edEvento.Value == "26" || edEvento.Value == "27" || edEvento.Value == "37" 
                || edEvento.Value == "43" || edEvento.Value == "44" || edEvento.Value == "45" || edEvento.Value == "46" || edEvento.Value == "49" || edEvento.Value == "52" || edEvento.Value == "53")
            {
                if (e.totalRegistros <= 0)
                {
                    e.loadEventosXInstruccion("-1");
                    e.newLine();
                    e.IDINSTRUCCION = edInstruccion.Value;
                    e.IDEVENTO = edEvento.Value;
                    e.FECHAINICIO = edFecha.Value + " " + edHora.Value;
                    e.FECHAFIN = edFecha.Value + " " + edHora.Value;
                    e.FECHAINICIOINGRESO = DateTime.Now.ToString();
                    e.FECHAFININGRESO = DateTime.Now.ToString();
                    e.OBSERVACIONINICIO = edObservacion.Value;
                    e.OBSERVACIONFIN = edObservacion.Value;
                    e.ELIMINADO = "1";  //evento iniciado
                    e.IDUSUARIOINICIO = Session["IdUsuario"].ToString();
                    e.IDUSUARIOFIN = Session["IdUsuario"].ToString();
                    e.commitLine();
                    e.actualizar();



                    registrarMensaje("Evento iniciado exitosamente");
                    llenarBitacora(edEventoDesc.Value + " iniciado exitosamente", Session["IdUsuario"].ToString(), edInstruccion.Value);
                    rgInstrucciones.Rebind();

                }
                else
                    registrarMensaje("Este evento ya está registrado");

            }
            else
            {
                if (e.totalRegistros <= 0)
                {
                    e.loadEventosXInstruccion("-1");
                    e.newLine();
                    e.IDINSTRUCCION = edInstruccion.Value;
                    e.IDEVENTO = edEvento.Value;
                    e.FECHAINICIO = edFecha.Value + " " + edHora.Value;
                    e.FECHAINICIOINGRESO = DateTime.Now.ToString();
                    e.OBSERVACIONINICIO = edObservacion.Value;
                    e.ELIMINADO = "0";  //evento iniciado
                    e.IDUSUARIOINICIO = Session["IdUsuario"].ToString();


                    if (edEvento.Value == "1")
                    {
                        e.FECHAFIN = edFecha.Value + " " + edHora.Value;
                        e.FECHAFININGRESO = DateTime.Now.ToString();
                        e.OBSERVACIONFIN = edObservacion.Value;
                        e.IDUSUARIOFIN = Session["IdUsuario"].ToString();
                        e.ELIMINADO = "1"; //evento finalizado
                        
                        i.loadInstruccionXArribo(edInstruccion.Value);

                        if (i.totalRegistros > 0 & edEvento.Value == "1")
                        {
                            i.ARRIBOCARGA = "1";
                            i.actualizar();
                        }
                
          
                    }


                    e.commitLine();
                    e.actualizar();

                    i.loadInstruccionXArribo(edInstruccion.Value);

                    if (i.totalRegistros > 0 & edEvento.Value == "1")
                    {
                        i.ARRIBOCARGA = "1";
                        i.actualizar();
                    }


                    registrarMensaje("Evento iniciado exitosamente");
                    llenarBitacora(edEventoDesc.Value + " iniciado exitosamente", Session["IdUsuario"].ToString(), edInstruccion.Value);
                    rgInstrucciones.Rebind();


                }
                else
                    registrarMensaje("Este evento ya está registrado");
            }
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "EventosInicio");
        }
    }

    protected void btnLimpiar_Click(object sender, ImageClickEventArgs e)
    {
        limpiar();
    }

    private void limpiar()
    {
        try
        {
            foreach (GridDataItem grdItem in rgInstrucciones.Items)
            {
                RadDatePicker dtFecha = (RadDatePicker)grdItem.FindControl("dtFechaSellado");
                RadTimePicker dtHora = (RadTimePicker)grdItem.FindControl("dtHoraSellado");
                RadTextBox edObservacion = (RadTextBox)grdItem.FindControl("edObservacion");
                dtFecha.Clear();
                dtHora.Clear();
                edObservacion.Text = "";
            }
        }
        catch (Exception)
        { }
    }

    protected void rgInstrucciones_ItemDataBound(object sender, GridItemEventArgs e)
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            CodigosBO c = new CodigosBO(logApp);
            GridEditableItem editedItem = e.Item as GridEditableItem;
            if (editedItem != null && e.Item is GridDataItem)
            {
                string idInstruccion = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdInstruccion"]);
                RadComboBox edEventos = (RadComboBox)editedItem.FindControl("edEventos");
                if (!User.IsInRole("Operaciones") && !User.IsInRole("Oficial de Cuenta") && !User.IsInRole("Administradores") && !User.IsInRole("Inplant"))
                    i.loadInstruccion(idInstruccion);
                else
                    i.loadInstruccionGD(idInstruccion);
                if (i.ARRIBOCARGA == "0")
                {
                    c.loadAllCampos("EVENTOS");
                    //c.loadEventosSinArriboCarga();
                    //RadComboBox edEventos = (RadComboBox)editedItem.FindControl("edEventos");
                    edEventos.DataSource = c.TABLA;
                    edEventos.DataBind();
                    //if (!User.IsInRole("Operaciones") && !User.IsInRole("Oficial de Cuenta") && !User.IsInRole("Administradores"))
                    //    edEventos.Enabled = false;
                    //else
                    edEventos.Enabled = true;
                    edEventos.ForeColor = System.Drawing.Color.Black;
                }
                else
                {
                    c.loadEventosSinArriboCarga();
                    //RadComboBox edEventos = (RadComboBox)editedItem.FindControl("edEventos");
                    edEventos.DataSource = c.TABLA;
                    edEventos.DataBind();
                    edEventos.Enabled = true;
                }
            }
        }
        catch (Exception)
        { }
    }

    #region Conexion
    private void conectarTerrestre()
    {
        if (logAppTerrestre == null)
            logAppTerrestre = new GrupoLis.Login.Login();
        if (!logAppTerrestre.conectado)
        {
            logAppTerrestre.tipoConexion = TipoConexion.SQL_SERVER;
            logAppTerrestre.SERVIDOR = mParamethers.Get("ServidorAzure");
            logAppTerrestre.DATABASE = mParamethers.Get("DatabaseTT");
            logAppTerrestre.USER = mParamethers.Get("UserAzure");
            logAppTerrestre.PASSWD = mParamethers.Get("PasswordAzure");
            logAppTerrestre.conectar();
        }
    }

    private void desconectarTerrestre()
    {
        if (logAppTerrestre.conectado)
            logAppTerrestre.desconectar();
    }
    #endregion

    private void Terrestre()
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            EventosBO e = new EventosBO(logApp);

            i.loadInstruccion(edInstruccion.Value);
            string guia = "", aduana = "";
            guia = i.CODIGOGUIA;
            aduana = i.CODIGOADUANA;
            //Miro que la Hoja este en Arribo de Carga, que tenga guia y que no este eliminada
            e.InicioEventoGuia(edInstruccion.Value, edEvento.Value);

            // si cumple con todos los requisitos pasa
            if (e.totalRegistros > 0)
            {
                conectarTerrestre();

                GuiasBO bo = new GuiasBO(logApp);
                TiempoFlujosBO tf = new TiempoFlujosBO(logApp);
                //carga la guia para ver si se le puede asignar el estado
                bo.CargarGuia(guia);
                //si se le puede asignar lo asigna
                if (bo.CODESTADO == "6")
                {
                    //lo guarda
                    tf.AgregarTiempo();
                    tf.newLine();
                    tf.CODIGOGUIA = guia;
                    tf.CODFLUJO = (int.Parse("6").ToString());
                    tf.CODIGOADUANA = aduana;
                    tf.FECHA = edFecha.Value + " " + edHora.Value;
                    tf.OBSERVACION = edObservacion.Value;
                    tf.CORRELATIVOGUIA = "6";
                    tf.ELIMINADO = "0";
                    tf.IDUSUARIO = (int.Parse("66").ToString());
                    tf.FECHASISTEMA = DateTime.Now.ToString();
                    tf.commitLine();
                    tf.actualizar();

                    // cambia el estado de la guia
                    bo.CODESTADO = (int.Parse("7").ToString());
                    bo.commitLine();
                    bo.actualizar();
                }

                desconectarTerrestre();
            }

        }

        catch { }
        finally { desconectar(); }
    }
    //protected void sendMailLocal(string to, string subject, string body)
    //{
    //    StringBuilder bodytmp = new StringBuilder();

    //    MailMessage mailmsg = new MailMessage();
    //    mailmsg.From = new MailAddress("recargas@grupovesta.com", "VESTA");
    //    mailmsg.To.Add(new MailAddress(to.Trim()));
    //    mailmsg.Subject = subject;

    //    bodytmp.Append("<html xmlns=\"http://www.w3.org/1999/xhtml\"> <head> <title>" + subject + "</title> </head> ");
    //    bodytmp.Append("<body> ");

    //    bodytmp.Append(body);

    //    bodytmp.Append("</body> </html>");

    //    AlternateView avHTML = AlternateView.CreateAlternateViewFromString(bodytmp.ToString(), null, MediaTypeNames.Text.Html);

    //    mailmsg.AlternateViews.Add(avHTML);

    //    StreamWriter sw = new StreamWriter(new MemoryStream(), Encoding.UTF8);
    //    sw.Write(body.ToString());
    //    sw.Flush();

    //    sw.BaseStream.Position = 0;
    //    //mailmsg.Attachments.Add(new Attachment(sw.BaseStream, "Template.html", MediaTypeNames.Text.Html));

    //    SmtpClient smtpClient = new SmtpClient("grupovesta.com");
    //    smtpClient.Send(mailmsg);

    //    sw.Close();
    //}


    private void finalizarEvento( string IdInstruccion, string idevento)
    {
        try
        {
            conectar();
            EventosBO e = new EventosBO(logApp);
            InstruccionesBO i = new InstruccionesBO(logApp);
            e.loadEventosXInstruccionEvento(IdInstruccion, idevento);
            if (e.totalRegistros > 0)
            {
                DateTime fechaInicio = Convert.ToDateTime(e.FECHAINICIO.Trim());
                DateTime fechaFin = Convert.ToDateTime(edFecha.Value + " " + edHora.Value);
                if (fechaInicio < fechaFin)
                {
                    e.FECHAFIN = edFecha.Value + " " + edHora.Value;
                    e.FECHAFININGRESO = DateTime.Now.ToString();
                    e.OBSERVACIONFIN = edObservacion.Value;
                    e.IDUSUARIOFIN = Session["IdUsuario"].ToString();
                    e.ELIMINADO = "1"; //evento finalizado
                    e.actualizar();
                    i.loadInstruccionXArribo(IdInstruccion);

                    if (i.totalRegistros > 0 & edEvento.Value == "1")
                    {
                        i.ARRIBOCARGA = "1";
                        i.actualizar();
                    }
                }
                else
                    registrarMensaje("La fecha final debe ser mayor que la fecha inicial");
            }
            else
                registrarMensaje("Contactar al encargado del sistema...Quizás el evento ya esta finalizado");
        }
        catch (Exception)
        { }
        finally
        {
            desconectar();
        }
    }


}