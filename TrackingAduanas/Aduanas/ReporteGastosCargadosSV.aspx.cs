﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.OleDb;

using Telerik.Web.UI;
using System.Reflection;
using System.Text;

public partial class ReporteGastosCargados : Utilidades.PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Gastos Cargados", "Gastos Cargados");
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            //UsuariosBO u = new UsuariosBO(logApp);
            //u.loadUsuario(Session["Pais"])
            c.loadPaisesHojaRuta();
            cmbPais.Dispose();
            cmbPais.ClearSelection();
            cmbPais.DataSource = c.TABLA;
            cmbPais.DataBind();

            if (!User.IsInRole("Administradores"))
            {
                cmbPais.SelectedValue = Session["Pais"].ToString();
                cmbPais.Enabled = false;
            }

        }
    }

    protected bool Consultar { get { return tienePermiso("Consultar"); } }
    private void ConfigureExport()
    {

        String filename = "Control_Gastos_Contabilizados_" + DateTime.Now.ToString();
        rgGatos.ExportSettings.FileName = filename;
        rgGatos.ExportSettings.ExportOnlyData = true;
        rgGatos.ExportSettings.IgnorePaging = true;
        rgGatos.ExportSettings.OpenInNewWindow = true;
        rgGatos.MasterTableView.ExportToExcel();
    }
    public override bool CanGoBack { get { return false; } }
    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        llenarGridGastos();
    }

    #region Compra
    private void llenarGridGastos()
    {
        try
        {
            conectar();
            DetalleGastosBO DG = new DetalleGastosBO(logApp);
            DG.LoadGastosCargados(cmbPais.SelectedValue, rdpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), rdpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"));
            rgGatos.DataSource = DG.TABLA;
            rgGatos.DataBind();
        }
        catch
        { }
        finally { desconectar(); }

    }
    protected void rgCompras_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName)
                ConfigureExport();
        }
        catch (Exception)
        { }
    }
    protected void rgCompras_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridGastos();
    }
    #endregion



}

