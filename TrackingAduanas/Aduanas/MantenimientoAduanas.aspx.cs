﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

public partial class MantenimientoAduanas : Utilidades.PaginaBase
{

    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Mantenimiento Aduanas";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgAduanas.FilterMenu);
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Mantenimiento Aduanas", "Aduanas");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Inicio.aspx");
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void SetVisible(TableCellCollection cells, String commandName, bool visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is ImageButton)
                {
                    ImageButton btn = (ImageButton)control;

                    if (btn.CommandName == commandName)
                        btn.Visible = visible;
                }
            }
        }
    }

    protected Object GetPaises()
    {
        try
        {
            conectar();
            CodigosBO bo = new CodigosBO(logApp);
            bo.loadPaisesHojaRuta();
            return bo.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }
        return null;
    }

    protected void rgAduanas_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridAduanas();
    }

    private void llenarGridAduanas()
    {
        try
        {
            conectar();
            AduanasBO bo = new AduanasBO(logApp);
            bo.loadAduanas();
            rgAduanas.DataSource = bo.TABLA;
            rgAduanas.DataBind();
        }
        catch (Exception) { }
        finally
        {
            desconectar();
        }
    }

    protected void GetDatosEditadosAduanas(AduanasBO bo, GridItem item)
    {
        GridEditableItem editedItem = item as GridEditableItem;
        RadTextBox codaduana = (RadTextBox)editedItem.FindControl("edCodAduana");
        RadTextBox aduana = (RadTextBox)editedItem.FindControl("edAduana");
        RadComboBox pais = (RadComboBox)editedItem.FindControl("edPais");
        bo.CODPAIS = pais.SelectedValue;
        bo.CODIGOADUANA = codaduana.Text.Trim();
        bo.NOMBREADUANA = aduana.Text.Trim();
    }

    protected void rgAduanas_InsertCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            AduanasBO a = new AduanasBO(logApp);
            AduanasBO bo = new AduanasBO(logApp);
            bo.loadAduanas("-1");
            bo.newLine();

            GetDatosEditadosAduanas(bo, e.Item);
            a.loadAduanas(bo.CODIGOADUANA);
            if (a.totalRegistros <= 0)
            {
                a.loadNombreAduanas(bo.NOMBREADUANA, bo.CODPAIS);
                if (a.totalRegistros <= 0)
                {
                    bo.commitLine();
                    bo.actualizar();
                    registrarMensaje("Aduana ingresada exitosamente");

                    llenarBitacora("Se ingresó la aduana " + bo.NOMBREADUANA, Session["IdUsuario"].ToString(), bo.CODIGOADUANA);
                    rgAduanas.Rebind();
                }
                else
                    registrarMensaje("Ya existe la aduana " + a.CODIGOADUANA + " para este país con este mismo nombre");
            }
            else
                registrarMensaje("Este código de aduana ya existe para la aduana " + a.NOMBREADUANA);
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoAduanas");
        }
        finally
        {
            desconectar();
        }
    }

    protected void rgAduanas_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridEditableItem editedItem = e.Item as GridEditableItem;

            String codAduana = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["CodigoAduana"]);

            AduanasBO a = new AduanasBO(logApp);
            AduanasBO bo = new AduanasBO(logApp);
            bo.loadAduanas(codAduana);
            string oldAduana = bo.NOMBREADUANA;

            GetDatosEditadosAduanas(bo, e.Item);

            a.loadNombreAduanas(bo.NOMBREADUANA, bo.CODPAIS);
            if (a.totalRegistros <= 0)
            {
                bo.actualizar();
                registrarMensaje("Aduana actualizada exitosamente");

                llenarBitacora("Se modificó la aduana " + oldAduana + " por " + bo.NOMBREADUANA, Session["IdUsuario"].ToString(), bo.CODIGOADUANA);
                rgAduanas.Rebind();
            }
            else
                registrarMensaje("Ya existe la aduana " + a.CODIGOADUANA + " para este país con este mismo nombre");
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoAduanas");
        }
        finally
        {
            desconectar();
        }
    }

    protected void rgAduanas_DeleteCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridDataItem editedItem = e.Item as GridDataItem;

            String codAduana = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["CodigoAduana"]);
            AduanasBO bo = new AduanasBO(logApp);
            bo.loadAduanas(codAduana);
            bo.CODESTADO = "1";
            bo.actualizar();
            registrarMensaje("Aduana eliminada exitosamente");

            llenarBitacora("Se elimino la aduana " + bo.NOMBREADUANA, Session["IdUsuario"].ToString(), bo.CODIGOADUANA);
            rgAduanas.Rebind();
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoAduanas");
        }
        finally
        {
            desconectar();
        }

    }

    protected void rgAduanas_Init(object sender, EventArgs e)
    {
        GridFilterMenu menu = rgAduanas.FilterMenu;
        menu.Items.RemoveAt(rgAduanas.FilterMenu.Items.Count - 2);
    }
}