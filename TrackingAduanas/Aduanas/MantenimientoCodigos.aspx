﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="MantenimientoCodigos.aspx.cs" Inherits="MantenimientoCodigos" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadTabStrip ID="RadTabStrip1" runat="server" SelectedIndex="1" 
        MultiPageID="RadMultiPage1">
        <Tabs>
            <telerik:RadTab Text="Categorías" PageViewID="pvCategorias">
            </telerik:RadTab>
            <telerik:RadTab Text="Códigos" PageViewID="pvCodigos" Selected="True">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="1" BorderStyle="Groove"
        Width="99%" Height="90%">
        <telerik:RadPageView ID="pvCategorias" runat="server" Width="100%">
            <telerik:RadGrid ID="rgCategorias" runat="server" AllowFilteringByColumn="True" AllowSorting="True"
                AutoGenerateColumns="False" GridLines="None" Width="100%" Height="420px" OnNeedDataSource="rgCategorias_NeedDataSource"
                OnUpdateCommand="rgCategorias_UpdateCommand" OnDeleteCommand="rgCategorias_DeleteCommand"
                OnInsertCommand="rgCategorias_InsertCommand" AllowPaging="True" ShowFooter="True"
                ShowStatusBar="True" PageSize="20" oninit="rgCategorias_Init">
                <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                <MasterTableView DataKeyNames="IdCategoria" CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros."
                    NoMasterRecordsText="No hay Categorías definidas.">
                    <CommandItemSettings AddNewRecordText="Agregar Categoría" RefreshText="Volver a Cargar Datos" />
                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridEditCommandColumn AutoPostBackOnFilter="True" CancelText="Cancelar"
                            EditText="Editar" InsertText="Insertar" UpdateText="Grabar" ButtonType="ImageButton">
                            <HeaderStyle Width="5%" />
                            <ItemStyle Width="5%" />
                        </telerik:GridEditCommandColumn>
                        <telerik:GridButtonColumn ConfirmText="¿Esta seguro de que desea eliminar esta categoría?"
                            ConfirmDialogType="RadWindow" ConfirmTitle="Eliminar Categoría" UniqueName="DeleteColumn"
                            ButtonType="ImageButton" CommandName="Delete">
                            <HeaderStyle Width="5%" />
                            <ItemStyle Width="5%" />
                        </telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="Categoria" HeaderText="Categoría" UniqueName="column">
                            <HeaderStyle Width="23%" />
                            <ItemStyle Width="23%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Descripcion" HeaderText="Descripción" UniqueName="column1"
                            Aggregate="Count" FooterText="Cantidad: ">
                            <HeaderStyle Width="67%" />
                            <ItemStyle Width="67%" />
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings EditFormType="Template" CaptionDataField="Categoria" CaptionFormatString="">
                        <EditColumn UniqueName="EditCommandColumn1">
                        </EditColumn>
                        <FormTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# ((Container is GridEditFormInsertItem) ? "Agregar Nueva Categoría" : "Editar Categoría Existente") %>'
                                Font-Size="Medium" Font-Bold="true" />
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 10%">
                                    </td>
                                    <td style="width: 90%">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="lbCategoria" runat="server" Text="Categoría" Font-Bold="true" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadTextBox ID="edCategoria" runat="server" Text='<%# Bind( "Categoria") %>'
                                            EmptyMessage="Escriba la Categoría aquí" Width="25%" />
                                        <asp:RequiredFieldValidator ID="rfCategoria" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="La Categoría no puede estar vacía" ControlToValidate="edCategoria" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="Label2" runat="server" Text="Descripción" Font-Bold="true" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadTextBox ID="edDescripcion" runat="server" Text='<%# Bind( "Descripcion") %>'
                                            EmptyMessage="Escriba la Descripción aquí" Width="50%" />
                                        <asp:RequiredFieldValidator ID="rfDescripcion" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="La Descripción no puede estar vacía" ControlToValidate="edDescripcion" />
                                    </td>
                                </tr>
                            </table>
                            <asp:ImageButton ID="btnSave" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                AlternateText='<%# (Container is GridEditFormInsertItem) ? "Agregar Categoría" : "Actualizar Categoría" %>'
                                ImageUrl='Images/16/check2_16.png' />
                            &nbsp
                            <asp:ImageButton ID="btnCancel" runat="server" CommandName="Cancel" AlternateText="Cancelar"
                                ImageUrl='Images/16/delete2_16.png' CausesValidation="false" />
                        </FormTemplate>
                    </EditFormSettings>
                </MasterTableView>
                <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                    <Selecting AllowRowSelect="True" />
                    <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                        DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                        DropHereToReorder="Suelte aquí para Re-Ordenar" />
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                </ClientSettings>
                <FilterMenu EnableTheming="True">
                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                </FilterMenu>
                <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
            </telerik:RadGrid>
        </telerik:RadPageView>
        <telerik:RadPageView ID="pvCodigos" runat="server" Width="100%">
            <table style="width: 50%">
                <tr>
                    <td style="width: 30%">
                        <asp:Label ID="lbCategorias" runat="server" Text="Categoría:" Font-Bold="true" Font-Size="Medium" />
                    </td>
                    <td style="width: 70%">
                        <telerik:RadComboBox ID="cbCategorias" runat="server" DataTextField="Descripcion"
                            DataValueField="Categoria" Width="100%" OnSelectedIndexChanged="cbCategorias_SelectedIndexChanged"
                            AutoPostBack="True">
                            <CollapseAnimation Duration="200" Type="OutQuint" />
                        </telerik:RadComboBox>
                    </td>
                </tr>
            </table>
            <telerik:RadGrid ID="rgCodigos" runat="server" AllowFilteringByColumn="True" AllowSorting="True"
                AutoGenerateColumns="False" GridLines="None" Width="100%" Height="400px" OnNeedDataSource="rgCodigos_NeedDataSource"
                OnUpdateCommand="rgCodigos_UpdateCommand" OnDeleteCommand="rgCodigos_DeleteCommand"
                OnInsertCommand="rgCodigos_InsertCommand" AllowPaging="True" ShowFooter="True"
                ShowStatusBar="True" PageSize="20" oninit="rgCodigos_Init">
                <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                <MasterTableView DataKeyNames="Categoria,Codigo" CommandItemDisplay="TopAndBottom"
                    NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay Códigos definidos.">
                    <CommandItemSettings AddNewRecordText="Agregar Código" RefreshText="Volver a Cargar Datos" />
                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridEditCommandColumn AutoPostBackOnFilter="True" CancelText="Cancelar"
                            EditText="Editar" InsertText="Insertar" UpdateText="Grabar" ButtonType="ImageButton">
                            <HeaderStyle Width="5%" />
                            <ItemStyle Width="5%" />
                        </telerik:GridEditCommandColumn>
                        <telerik:GridButtonColumn ConfirmText="¿Esta seguro de que desea eliminar esta Código?"
                            ConfirmDialogType="RadWindow" ConfirmTitle="Eliminar Código" UniqueName="DeleteColumn"
                            ButtonType="ImageButton" CommandName="Delete">
                            <HeaderStyle Width="5%" />
                            <ItemStyle Width="5%" />
                        </telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="Codigo" HeaderText="Código" UniqueName="column">
                            <HeaderStyle Width="16%" />
                            <ItemStyle Width="16%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Descripcion" HeaderText="Descripción" UniqueName="column1"
                            Aggregate="Count" FooterText="Cantidad: ">
                            <HeaderStyle Width="64%" />
                            <ItemStyle Width="64%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Orden" HeaderText="Orden" UniqueName="column2">
                            <HeaderStyle Width="10%" />
                            <ItemStyle Width="10%" />
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings EditFormType="Template" CaptionDataField="Codigo" CaptionFormatString="">
                        <EditColumn UniqueName="EditCommandColumn1">
                        </EditColumn>
                        <FormTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# ((Container is GridEditFormInsertItem) ? "Agregar Nuevo Código" : "Editar Código Existente") %>'
                                Font-Size="Medium" Font-Bold="true" />
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 10%">
                                    </td>
                                    <td style="width: 90%">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="lbCodigo" runat="server" Text="Código" Font-Bold="true" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadTextBox ID="edCodigo" runat="server" Text='<%# Bind( "Codigo") %>'
                                            EmptyMessage="Escriba la Código aquí" Width="25%" />
                                        <asp:RequiredFieldValidator ID="rfCodigo" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="El Código no puede estar vacío" ControlToValidate="edCodigo" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="Label2" runat="server" Text="Descripción" Font-Bold="true" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadTextBox ID="edDescripcion" runat="server" Text='<%# Bind( "Descripcion") %>'
                                            EmptyMessage="Escriba la Descripción aquí" Width="50%" />
                                        <asp:RequiredFieldValidator ID="rfDescripcion" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="La Descripción no puede estar vacía" ControlToValidate="edDescripcion" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="Label3" runat="server" Text="Orden" Font-Bold="true" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadNumericTextBox ID="edOrden" runat="server" Text='<%# Bind("Orden") %>'
                                            NumberFormat-DecimalDigits="0" EmptyMessage="Escriba el Orden aquí" Width="25%" />
                                    </td>
                                </tr>
                            </table>
                            <asp:ImageButton ID="btnSave" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                AlternateText='<%# (Container is GridEditFormInsertItem) ? "Agregar Código" : "Actualizar Código" %>'
                                ImageUrl='Images/16/check2_16.png' />
                            &nbsp
                            <asp:ImageButton ID="btnCancel" runat="server" CommandName="Cancel" AlternateText="Cancelar"
                                ImageUrl='Images/16/delete2_16.png' CausesValidation="false" />
                        </FormTemplate>
                    </EditFormSettings>
                </MasterTableView>
                <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                    <Selecting AllowRowSelect="True" />
                    <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                        DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                        DropHereToReorder="Suelte aquí para Re-Ordenar" />
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                </ClientSettings>
                <FilterMenu EnableTheming="True">
                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                </FilterMenu>
                <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
            </telerik:RadGrid>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgCategorias">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgCategorias" LoadingPanelID="LoadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="cbCategorias" LoadingPanelID="LoadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="rgCodigos" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rgCodigos">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgCodigos" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cbCategorias">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgCodigos" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>


