﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="MantenimientoCambiarClave.aspx.cs" Inherits="MantenimientoCambiarClave" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadScriptBlock ID="scripts" runat="server">

        <script type="text/javascript">
            function confirmCallBackFn(arg) {
                if (arg == true) {
                    var btn = document.getElementById('<%= btnCambiar.ClientID %>');

                    if (btn != null)
                        btn.click();
                }
            }
        </script>

    </telerik:RadScriptBlock>
    <asp:ImageButton ID="btnCambiar" runat="server" ImageUrl="~/Images/gris.png"  OnClick="btnCambiar_Click" />
    <div style="border-width: 2px; border-style:inset">
        <table style="width: 60%">
            <tr>
                <td style="width: 30%">
                    Clave Anterior
                </td>
                <td style="width: 70%">
                    <telerik:RadTextBox ID="edOldPass" runat="server" Width="80%" TextMode="Password">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator ID="rfOldPass" runat="server" ControlToValidate="edOldPass"
                        ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="*" ControlToCompare="edNewPass"
                        ControlToValidate="edOldPass" Display="Dynamic" Operator="NotEqual"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">
                    Nueva Clave
                </td>
                <td style="width: 70%">
                    <telerik:RadTextBox ID="edNewPass" runat="server" Width="80%" TextMode="Password">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator ID="rfNewPass" runat="server" ControlToValidate="edNewPass"
                        ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvNewPass" runat="server" ErrorMessage="*" ControlToCompare="edConPass"
                        ControlToValidate="edNewPass" Display="Dynamic"></asp:CompareValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 30%">
                    Confirmar Clave
                </td>
                <td style="width: 70%">
                    <telerik:RadTextBox ID="edConPass" runat="server" Width="80%" TextMode="Password">
                    </telerik:RadTextBox>
                    <asp:RequiredFieldValidator ID="rfConPass" runat="server" ControlToValidate="edConPass"
                        ErrorMessage="*" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="cvConPass" runat="server" ErrorMessage="*" ControlToCompare="edNewPass"
                        ControlToValidate="edConPass" Display="Dynamic"></asp:CompareValidator>
                </td>
            </tr>
        </table>
    </div>
    <asp:ImageButton ID="btnChangePass" runat="server" CausesValidation="true" ImageUrl="~/Images/24/disk_blue_24.png"
        OnClientClick="radconfirm('¿Esta seguro que desa cambiar la clave?', confirmCallBackFn); return false;" />
</asp:Content>


