﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;

using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;

public partial class InstruccionesFinalizadas : Utilidades.PaginaBase
{


    readonly AduanasDataContext _aduanasDc = new AduanasDataContext();
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Instrucciones";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgInstrucciones.FilterMenu);
        rgInstrucciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgInstrucciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Guía de Remisión", "Guía de Remisión");
            mpInstrucciones.SelectedIndex = 0;
            conectar();
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuarioLogin(User.Identity.Name);
            //if (User.IsInRole("Administradores") || User.IsInRole("Administrador Pais") || User.IsInRole("Administrador Pais SVGT")|| (User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT") & u.CODPAIS != "H"))
            //    Table1.Visible = true;
           // else
              //  Table1.Visible = false;
            //if (Anular || Ingresar || Modificar)
            //    redirectTo("Default.aspx");
        }
    }

    //protected bool Anular { get { return tienePermiso("ANULAR"); } }
    //protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    //protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    #region Instrucciones
    protected void rgInstrucciones_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridInstrucciones();
    }

    private void llenarGridInstrucciones()
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            if (User.IsInRole("Administradores"))
                i.loadInstruccionesRemision();

            else if (User.IsInRole("Jefes Aduana") || User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.totalRegistros > 0)
                    i.loadInstruccionXAduanaFinalizadasRemision(u.CODIGOADUANA);
            }
      
            rgInstrucciones.DataSource = i.TABLA;
        }
        catch { }
    }

    private void llenarGridInstruccion(string idInstruccion)
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            i.loadInstruccionFinalizada(idInstruccion);
            rgInstrucciones.DataSource = i.TABLA;
            rgInstrucciones.DataBind();
        }
        catch { }
    }

    protected void rgInstrucciones_Init(object sender, System.EventArgs e)
    {
        GridFilterMenu menu = rgInstrucciones.FilterMenu;
        menu.Items.RemoveAt(rgInstrucciones.FilterMenu.Items.Count - 2);
    }



    protected void rgIngresos_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
    {
        try
        {

            conectar();
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            string CSAP = dataItem["CodigoSAP"].Text;

            conectar();
          
            InstruccionesBO i = new InstruccionesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            if (User.IsInRole("Administradores"))
                i.loadClienteDetalleRemision(CSAP);

            else if (User.IsInRole("Jefes Aduana") || User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.totalRegistros > 0)
                    i.loadIClienteXAduanaUsuarioDetalleRemision(u.CODIGOADUANA, u.CODPAIS,CSAP);
            }

            e.DetailTableView.DataSource = i.TABLA;


        }
        catch (Exception)
        {

        }
        finally { desconectar(); }

    }
    protected void rgInstrucciones_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            HistorialImpresionesBO hi = new HistorialImpresionesBO(logApp);
         //   GridEditableItem editedItem = e.Item as GridEditableItem;
            GridDataItem editedItem = e.Item as GridDataItem;
         

           // GridEditableItem editedItem = e.Item as GridEditableItem;
          ///  InstruccionesBO i = new InstruccionesBO(logApp);
            InstruccionesBO ins = new InstruccionesBO(logApp);
            MovimientosEspeciesFiscalesBO mef = new MovimientosEspeciesFiscalesBO(logApp);


            if (e.CommandName == "Gestion")
            {
                string idInstruccion = editedItem["IdInstruccion"].Text;
                i.loadInstruccion(idInstruccion);
                this.IdInstruccion.Text = idInstruccion;
                llenarDatos(idInstruccion);
                this.llenarClientes(i.IDCLIENTE);
                this.txtProveedor.Text = i.PROVEEDOR;
                this.txtCiudadDestino.Text = i.CIUDADDESTINO;
                this.txtAduana.Text = Buscar_aduanas(i.CODIGOADUANA);
                this.RadDateFechatraslado.Text = buscar_flujo("H-6", idInstruccion);
                this.txtContenedor.Text = i.DESCRIPCIONCONTENEDORES;
                this.LblFob.Text =  Convert.ToString(i.VALORFOB);
                this.Iblbultos.Text = i.CANTIDADBULTOS.ToString();
                this.IdIDescripcion.Text = i.PRODUCTO.ToString();
                this.llenar_datos_remison(this.IdInstruccion.Text);
                this.txtSello.Text = this.Sello_Seguridad(this.IdInstruccion.Text.Trim());
                mpInstrucciones.SelectedIndex = 1;


           }
            
            
            
            
            if (e.CommandName == "Reporte")
            {
                string idInstruccion2 = Convert.ToString(editedItem["IdInstruccion"].Text);
                if (!String.IsNullOrEmpty(idInstruccion2.Trim()))
                {
                    //registra de la impresion del archivo
                    try
                    {
                        hi.loadHistorialImpresiones("-1");
                        hi.newLine();
                        hi.DOCUMENTO = idInstruccion2;
                        hi.DESCRIPCION = "Se imprimió instrucción de Instrucciones Finalizadas";
                        hi.FECHA = DateTime.Now.ToString();
                        hi.IDUSUARIO = Session["IdUsuario"].ToString();
                        hi.commitLine();
                        hi.actualizar();
                    }
                    catch { }
                    try
                    {
                        Response.Redirect("ReporteGuiaRemision.aspx?IdInstruccion=" + idInstruccion2);
                    }
                    catch { }
                }
                else
                    registrarMensaje("Instrucción no existe, consulte con el administrador del sistema");
            }
            else if (rgInstrucciones.Items.Count <= 0 & e.CommandName == RadGrid.ExportToExcelCommandName)
                e.Canceled = true;
            else if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                ConfigureExport();
            }
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "InstruccionesFinalizadas");
        }
        finally
        {
            desconectar();
        }
    }

    private void ConfigureExport()
    {
        rgInstrucciones.Columns[0].Visible = false;
        String filename = "InstruccionesFinalizadas" + "_" + DateTime.Now.ToShortDateString();
        rgInstrucciones.GridLines = GridLines.Both;
        rgInstrucciones.ExportSettings.FileName = filename;
        rgInstrucciones.ExportSettings.IgnorePaging = true;
        rgInstrucciones.ExportSettings.OpenInNewWindow = false;
        rgInstrucciones.ExportSettings.ExportOnlyData = true;
    }

    public override bool CanGoBack { get { return false; } }
    #endregion




    public string Buscar_Pais_ciudades(string codigo)
    {
        try
        {
            conectar();
            CiudadesPaisBO nombre = new CiudadesPaisBO(logApp);
            nombre.loadCiudadesPais(codigo);
            return nombre.DESCRIPCION.ToString();
            desconectar();
        }
        catch (Exception)
        {
            return "N/A";
            throw;
        }
  

    }

    public string Buscar_aduanas(string codigo)
    {
        try
        {
            conectar();
            AduanasBO aduana = new AduanasBO(logApp);
            aduana.loadAduanas(codigo);
            return aduana.NOMBREADUANA;
            desconectar();

        }
        catch (Exception)
        {
            return "N/A";
            throw;
        }
   
    }

    public string buscar_flujo(string codigo, string hoja)
    {
        try
        {
            conectar();
            GestionCargaBO gestion = new GestionCargaBO(logApp);
            gestion.loadGestionIntrucionFinalizada(hoja);
            return gestion.FECHAINGRESO.ToString();
            desconectar();
        }
        catch (Exception)
        {
            return "N/A";
            throw;
        }
       
    }
    private void llenarDatos(string idInstruccion)
    {
        try
        {
            conectar();
            DatosGestionDocumentosBO dgd = new DatosGestionDocumentosBO(logApp);
            dgd.loadDatosGestionDocumentos(idInstruccion);
            if (dgd.totalRegistros > 0)
            {
                txtEmpresa.Text = dgd.NOMANIFIESTO;
                txtEmpresa.Text = dgd.EMPRESA;
                txtMotorista.Text = dgd.MOTORISTA;
               this.txtMarca.Text = dgd.PLACA;    // PLaca de gestion de documentos        
               this.txtPlacaFurgon.Text = dgd.FURGON;
               this.txtCabezal.Text = dgd.CABEZAL;
            
  
            }
            else
            {

        

            }
        }
        catch { }
    }

    private void llenarClientes(string  Idcliente)
    {
        try
        {
            conectar();
            ClientesBO c = new ClientesBO(logApp);
            c.loadCliente(Idcliente);
            txtCliente.Text = c.NOMBRE;
            this.txtRtncliente.Text = c.RTN;
            desconectar();
        }
        catch {

            txtCliente.Text = "N/A";
            
        
        }
    }

    private DateTime buscar_eventos(string codigo , string hoja)
    {
        try
        {
            EventosBO eventos = new EventosBO(logApp);
            eventos.loadEventosXInstruccionEvento(hoja, codigo);
            return Convert.ToDateTime(eventos.FECHAFIN);
        }
        catch (Exception)
        {
            
            throw;
        }


    }
    private string Buscar_codigo(string codigo, string categoria)
    {
        CodigosBO co = new CodigosBO(logApp);
        co.loadCodigosXCategoriaCodigos(categoria, codigo);
        return co.DESCRIPCION.ToString();
    }
    private void limpiarControles()
    {
       // txtNoManifiesto.Text = "";
        LbSerieremision.Text = "N/A";
        txtEmpresa.Text = "";
        txtMotorista.Text = "";
        txtMarca.Text = "";
        txtPlacaFurgon.Text = "";
        txtCabezal.Text = "";
    }
    protected void btnBack_Click(object sender, ImageClickEventArgs e)
    {
        mpInstrucciones.SelectedIndex = 0;
    }
    protected void btnGuardar_Click(object sender, ImageClickEventArgs e)
    {

        if (this.LbSerieremision.Text == "N/A")
        {
            Asignar_Remision();
        }
        else
        {
            Editar_Remision();

        }
       
    



    }



    public void Asignar_Remision( )
    {
        conectar();

        RemisionesInstruccionesBO rem = new  RemisionesInstruccionesBO (logApp);

        this.conectar();
        rem.loadBuscar_Remision(this.IdInstruccion.Text.ToString().Trim());

        if ( rem.totalRegistros<1){

            if (this.Validar_Controles())
            {
                        #region Selecionar  Serie

                        var query = _aduanasDc.AduanasRemision
                                     .Where(p => p.CodEstado != "100" && p.FechaLimite > DateTime.Today && p.CodAduana==IdInstruccion.Text.ToString().Substring(2, 3) && p.Disponibles>0)
                                     .Select(pi => new RemisionesAduanasVM()
                                     {
                                         Item = Convert.ToInt16(pi.Item),
                                         CodAduana = pi.CodAduana,
                                         FechaLimite = pi.FechaLimite,
                                         RangoInicial = pi.RangoInicial,
                                         RangoFinal = pi.RangoFinal,
                                         CodEstado = pi.CodEstado,
                                         Observacion = pi.Observacion,
                                         Cantidad = Convert.ToInt16(pi.Cantidad),
                                         Disponible = Convert.ToInt16(pi.Disponibles),
                                         SerieActual = pi.SerieActual,
                                     })
                                     .ToList();

                        if (query.Count > 0)
                        {
                            var rma = _aduanasDc.AduanasRemision.SingleOrDefault(p => p.CodEstado != "100" && p.FechaLimite > DateTime.Today && p.CodAduana == IdInstruccion.Text.ToString().Substring(2, 3) && p.Disponibles > 0);
                            #endregion

                            #region Guardar Datos
                            var Nueva_remision = new RemisionesInstrucciones
                            {
                                CodAduana = IdInstruccion.Text.ToString().Substring(2, 3),
                                Fecha = DateTime.Today,
                                IdInstruccion = IdInstruccion.Text,
                                CodEstado = "0",
                               
                                SelloFiscal = this.txtSello.Text,
                                RtnMotorista = this.txtRtnMotorista.Text,
                                RtnEmpresa = this.txtRtnEmpresa.Text,
                                RtnRemitente = this.txtRtnRemitente.Text,
                                IdUsuario = Session["IdUsuario"].ToString(),
                                IdRemision = rma.Item,
                                Serie = rma.SerieActual,
                                Observacion = "Asignacion de remision",
                                Licencia=this.txtLicencia.Text,

                            };

        
                            _aduanasDc.RemisionesInstrucciones.InsertOnSubmit(Nueva_remision);


                            _aduanasDc.SubmitChanges();


                            # endregion
                            #region Actualizar rango De rermisiones

                      

                            _aduanasDc.Dispose();


                            //Actualizar remision 
                            conectar();

                            AduanasRemisionBO Remision = new AduanasRemisionBO(logApp);
                            Remision.ActualizarRemision(rma.Item.ToString(), Serie_Actual(rma.SerieActual, rma.RangoInicial), Convert.ToInt16(rma.Disponibles - 1).ToString());

                            desconectar();

                            this.LbSerieremision.Text = rma.SerieActual;

                            #endregion 

                                }
                   
                            else{

                                    registrarMensaje("La Aduana No cuenta con series Disponibles, o El rango a Expirado");
                                    return;
                            }


                        }

            else
            {
                registrarMensaje("Favor Ingrese toda la información Requerida");
            }
        }
        else
        {

            registrarMensaje("La Instruccion ya contiene una Serie de Remision Asignada");

        }











    }


    public void Editar_Remision()
    {

            if (this.Validar_Controles())
            {

                    var rma = _aduanasDc.RemisionesInstrucciones.SingleOrDefault(p => p.IdInstruccion==this.IdInstruccion.Text);
           
                    #region Guardar Datos

                        rma.RtnMotorista = this.txtRtnMotorista.Text;
                        rma.RtnEmpresa = this.txtRtnEmpresa.Text;
                        rma.RtnRemitente = this.txtRtnRemitente.Text;
                        rma.Licencia = this.txtLicencia.Text;
                        rma.Observacion = "Modificacion";
                        _aduanasDc.SubmitChanges();


                    # endregion
            
                }

                else
                {
                    registrarMensaje("Favor Ingrese toda la información Requerida");
                }
        
 

    }

    public string  Serie_Actual(string serie, string Rango)
    {
        string Serie = (Convert.ToInt16(serie)+1).ToString();
        string compl_zero="";
        int res = Rango.Length - Serie.Length;
        //Serie=Serie.PadLeft(res+1, '0');
       
        for (int i = 0; i < res; i++)
        {
            compl_zero += "0";
        }
        return compl_zero+Serie;

    }

    public Boolean Validar_Controles()
    {  


    if (this.RadDateFechatraslado.Text.ToString()=="")
    {
      
        return false;
    }
    if (this.txtRtnRemitente.Text.ToString() == "")
    {
     
        return false;
    }

    if (this.txtSello.Text.ToString() == "")
    {

        return false;
    }
    if (this.txtRtnEmpresa.Text.ToString() == "")
    {

        return false;
    }
    if (this.txtRtnMotorista.Text.ToString() == "")
    {

        return false;
    }


         return true;


    }


    public void llenar_datos_remison (string  Instruccion ){

        try
        {
            var pi = _aduanasDc.RemisionesInstrucciones.SingleOrDefault(p => p.IdInstruccion == this.IdInstruccion.Text.ToString().Trim() && p.CodEstado!="100");
           
                  
                     this.txtRtnEmpresa.Text=  pi.RtnEmpresa;
                     this.txtRtnRemitente.Text = pi.RtnRemitente;
                     this.txtRtnMotorista.Text = pi.RtnMotorista;
                     this.txtLicencia.Text = pi.Licencia;
                     this.txtSello.Text = pi.SelloFiscal;
                     this.LbSerieremision.Text = pi.Serie;
                   
                   


	}
	catch ( Exception ex)
	{
        this.txtRtnEmpresa.Text = "";
        this.txtRtnRemitente.Text = "";
        this.txtRtnMotorista.Text = "";
        this.txtLicencia.Text = "";
        this.LbSerieremision.Text = "N/A";
        this.txtSello.Text = "N/A";
		
	}

        
    }


    public string Sello_Seguridad(string Instruccion)
    {
        string Sellos = "";
        try 
	{
        conectar();
        EspeciesFiscalesInstruccionesBO sello = new EspeciesFiscalesInstruccionesBO(logApp);
        sello.cargarespeciesxinstruccionSellos(Instruccion);
        for (int i = 0; i < sello.totalRegistros; i++)
        {
            Sellos += sello.SERIE +",";
            sello.regSiguiente();
        }

        desconectar();




        return Sellos.Substring(0,Sellos.Length-1);

	}
	catch (Exception)
	{

        return "N/A";
	}
        



    }


    protected void btnActivo_Command(object sender, CommandEventArgs e)
    {
        HistorialImpresionesBO hi = new HistorialImpresionesBO(logApp);

        if (e.CommandName == "Reporte")
        {
            string idInstruccion2 = e.CommandArgument.ToString();
            if (!String.IsNullOrEmpty(idInstruccion2.Trim()))
            {
                //registra de la impresion del archivo
                try
                {
                    hi.loadHistorialImpresiones("-1");
                    hi.newLine();
                    hi.DOCUMENTO = idInstruccion2;
                    hi.DESCRIPCION = "Se imprimió instrucción de Instrucciones Finalizadas";
                    hi.FECHA = DateTime.Now.ToString();
                    hi.IDUSUARIO = Session["IdUsuario"].ToString();
                    hi.commitLine();
                    hi.actualizar();
                }
                catch { }
                try
                {
                    Response.Redirect("ReporteGuiaRemision.aspx?IdInstruccion=" + idInstruccion2);
                }
                catch { }
            }
            else
                registrarMensaje("Instrucción no existe, consulte con el administrador del sistema");
        }

    }
    protected void btnActivo_Click(object sender, ImageClickEventArgs e)
    {
         HistorialImpresionesBO hi = new HistorialImpresionesBO(logApp);
         ImageButton b =  sender as ImageButton;

        if (b.CommandName == "Reporte")
        {
            string idInstruccion2 = b.CommandArgument.ToString();
            if (!String.IsNullOrEmpty(idInstruccion2.Trim()))
            {
                //registra de la impresion del archivo
                try
                {
                    hi.loadHistorialImpresiones("-1");
                    hi.newLine();
                    hi.DOCUMENTO = idInstruccion2;
                    hi.DESCRIPCION = "Se imprimió instrucción de Instrucciones Finalizadas";
                    hi.FECHA = DateTime.Now.ToString();
                    hi.IDUSUARIO = Session["IdUsuario"].ToString();
                    hi.commitLine();
                    hi.actualizar();
                }
                catch { }
                try
                {
                    Response.Redirect("ReporteGuiaRemision.aspx?IdInstruccion=" + idInstruccion2);
                }
                catch { }
            }
            else
                registrarMensaje("Instrucción no existe, consulte con el administrador del sistema");
        }
    }
}