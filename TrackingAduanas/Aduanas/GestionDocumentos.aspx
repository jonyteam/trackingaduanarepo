﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="GestionDocumentos.aspx.cs" Inherits="GestionDocumentos" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                    //<![CDATA[
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                    }
                }                          
            </script>
        </telerik:RadScriptBlock>
        <telerik:RadMultiPage ID="mpInstrucciones" runat="server">
            <telerik:RadPageView ID="RadPageView1" runat="server" Width="100%">
                <table width="100%">
                    <tr>
                        <td>
                            <telerik:RadGrid ID="rgInstrucciones" runat="server" AllowFilteringByColumn="True"
                                AllowSorting="True" AutoGenerateColumns="False" GridLines="None" Width="100%"
                                Height="400px" OnNeedDataSource="rgInstrucciones_NeedDataSource" AllowPaging="True"
                                ShowFooter="True" ShowStatusBar="True" PageSize="30" OnInit="rgInstrucciones_Init"
                                OnItemCommand="rgInstrucciones_ItemCommand">
                                <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                                <MasterTableView DataKeyNames="IdInstruccion" CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros."
                                    NoMasterRecordsText="No hay instrucciones." GroupLoadMode="Client">
                                    <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                                        ShowExportToExcelButton="true" ShowExportToCsvButton="true" />
                                    <RowIndicatorColumn>
                                        <HeaderStyle Width="20px"></HeaderStyle>
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn>
                                        <HeaderStyle Width="20px"></HeaderStyle>
                                    </ExpandCollapseColumn>
                                    <Columns>
                                        <%-- <telerik:GridTemplateColumn AllowFiltering="false" DataField="CodEstado" HeaderText="Gestion Documentos"
                                            UniqueName="CodEstado">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="btnModificar" runat="server" CommandArgument='<%# String.Format("{0}", Eval("CodigoRemision").ToString()) %>'
                                                    CommandName='<%# (Eval("CodEstado").ToString() == "0" ? "Modificar" : "Nada") %>'
                                                    ImageUrl='<%# (Eval("CodEstado").ToString() == "0" ? "Images/16/pencil_16.png" : "~/Images/gris.png") %>' />
                                            </ItemTemplate>
                                            <ItemStyle Width="10%" />
                                            <HeaderStyle Width="10%" />
                                        </telerik:GridTemplateColumn>--%>
                                        <telerik:GridButtonColumn ButtonType="ImageButton" HeaderText="Gestión" CommandName="Gestion"
                                            ImageUrl="Images/16/pencil_16.png" UniqueName="btnSelect">
                                            <ItemStyle Width="70px" />
                                            <HeaderStyle Width="70px" />
                                        </telerik:GridButtonColumn>
                                        <%--<telerik:GridButtonColumn ConfirmText="¿Está seguro de que desea eliminar esta Remisión?"
                                            ImageUrl="Images/16/index_delete_16.png" ConfirmDialogType="RadWindow" ConfirmTitle="Eliminar Remisión"
                                            UniqueName="DeleteColumn" ButtonType="ImageButton" CommandName="Delete" HeaderText="Eliminar">
                                            <ItemStyle Width="70px" />
                                            <HeaderStyle Width="70px" />
                                        </telerik:GridButtonColumn>--%>
                                        <telerik:GridBoundColumn DataField="IdInstruccion" HeaderText="Instruccion No." UniqueName="IdInstruccion"
                                            FilterControlWidth="80%">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="IdTramite" HeaderText="Tramite No." UniqueName="IdTramite"
                                            FilterControlWidth="80%">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Nombre" HeaderText="Cliente" UniqueName="Nombre"
                                            FilterControlWidth="80%">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ReferenciaCliente" HeaderText="Referencia Cliente"
                                            UniqueName="ReferenciaCliente" FilterControlWidth="80%">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="DocumentoNo" HeaderText="Factura" UniqueName="DocumentoNo"
                                            FilterControlWidth="80%">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <%--<telerik:GridBoundColumn DataField="CodRegimen" HeaderText="Regimen No." UniqueName="CodRegimen"
                                            FilterControlWidth="80%">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>--%>
                                        <%--<telerik:GridBoundColumn DataField="Descripcion" HeaderText="Estado" UniqueName="Descripcion"
                                            FilterControlWidth="80%">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Usuario" HeaderText="Usuario" UniqueName="Usuario"
                                            FilterControlWidth="80%">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>--%>
                                    </Columns>
                                    <%--<GroupByExpressions>
                                        <telerik:GridGroupByExpression>
                                            <SelectFields>
                                                <telerik:GridGroupByField FieldAlias="PaisOrigen" FieldName="PaisOrigen"></telerik:GridGroupByField>
                                            </SelectFields>
                                            <GroupByFields>
                                                <telerik:GridGroupByField FieldAlias="PaisOrigen" FieldName="PaisOrigen"></telerik:GridGroupByField>
                                            </GroupByFields>
                                        </telerik:GridGroupByExpression>
                                    </GroupByExpressions>--%>
                                </MasterTableView>
                                <HeaderStyle Width="180px" />
                                <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                                    <Selecting AllowRowSelect="True" />
                                    <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                                        DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                                        DropHereToReorder="Suelte aquí para Re-Ordenar" />
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
                                </ClientSettings>
                                <FilterMenu EnableTheming="True">
                                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                                </FilterMenu>
                                <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                                <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
                            </telerik:RadGrid>
                        </td>
                    </tr>
                </table>
                <input id="idInstruccion" runat="server" type="hidden" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="RadPageView2" runat="server" Width="100%">
                <table width="100%">
                    <tr>
                        <td style="width: 10%">
                        </td>
                        <td align="left" style="width: 8%">
                            Instrucción:
                        </td>
                        <td align="left" style="width: 18%">
                            <telerik:RadTextBox ID="txtInstruccion" Width="80%" runat="server" Enabled="false"
                                ForeColor="Black">
                            </telerik:RadTextBox>
                        </td>
                        <td align="left" style="width: 8%">
                            Régimen:
                        </td>
                        <td colspan="3" align="left">
                            <telerik:RadTextBox ID="txtRegimen" Width="92%" runat="server" Enabled="false" ForeColor="Black">
                            </telerik:RadTextBox>
                        </td>
                        <td style="width: 10%">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                        </td>
                        <td align="left" style="width: 8%">
                            No. Manifiesto:
                        </td>
                        <td align="left" style="width: 18%">
                            <telerik:RadTextBox ID="txtNoManifiesto" runat="server" Width="80%" MaxLength="50">
                            </telerik:RadTextBox>
                        </td>
                        <td align="left" style="width: 8%">
                            Observacion General:</td>
                        <td align="left" colspan="3">
                            <telerik:RadTextBox ID="txtobservaciongeneral" Runat="server" Width="92%">
                            </telerik:RadTextBox>
                        </td>
                        <td style="width: 10%">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                        </td>
                        <td colspan="7">
                            <asp:Label ID="lblDatosTransporte" runat="server" Text="Datos del Transporte" Font-Size="Small"
                                ForeColor="Black"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                        </td>
                        <td align="left" style="width: 8%">
                            Empresa
                        </td>
                        <td align="left" style="width: 18%">
                            <telerik:RadTextBox ID="txtEmpresa" Width="80%" runat="server" MaxLength="50">
                            </telerik:RadTextBox>
                        </td>
                        <td align="left" style="width: 8%">
                            Motorista:
                        </td>
                        <td align="left" style="width: 18%">
                            <telerik:RadTextBox ID="txtMotorista" Width="80%" runat="server" MaxLength="50">
                            </telerik:RadTextBox>
                        </td>
                        <td align="left" style="width: 8%">
                            Marca Cabezal:
                        </td>
                        <td align="left" style="width: 18%">
                            <telerik:RadTextBox ID="txtPlaca" Width="80%" runat="server" MaxLength="50">
                            </telerik:RadTextBox>
                        </td>
                        <td style="width: 10%">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                        </td>
                        <td align="left" style="width: 8%">
                            Placa Furgón:
                        </td>
                        <td align="left" style="width: 18%">
                            <telerik:RadTextBox ID="txtFurgon" Width="80%" runat="server" MaxLength="50">
                            </telerik:RadTextBox>
                        </td>
                        <td align="left" style="width: 8%">
                            Placa Cabezal:
                        </td>
                        <td align="left" style="width: 18%">
                            <telerik:RadTextBox ID="txtCabezal" runat="server" Width="80%" MaxLength="50">
                            </telerik:RadTextBox>
                        </td>
                        <td align="left" style="width: 8%">
                            Telefono</td>
                        <td align="left" style="width: 18%">
                            <telerik:RadTextBox ID="txtTelefono" runat="server" MaxLength="50" Width="80%">
                            </telerik:RadTextBox>
                        </td>
                        <td style="width: 10%">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                        </td>
                        <td colspan="6" align="center" style="width: 80%">
                            <asp:Label ID="lblDocMinimos" runat="server" Text="Documentos Mínimos por Régimen"
                                Font-Size="Medium" ForeColor="Black"></asp:Label>
                        </td>
                        <td style="width: 10%">
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td style="width: 10%">
                        </td>
                        <td align="center">
                            <telerik:RadGrid ID="rgDocumentos" runat="server" AutoGenerateColumns="False" Width="100%"
                                OnNeedDataSource="rgDocumentos_NeedDataSource" 
                                OnItemCreated="rgDocumentos_ItemCreated" CellSpacing="0" GridLines="None">
                                <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                                <MasterTableView DataKeyNames="CodDocumento" NoDetailRecordsText="No hay registros."
                                    NoMasterRecordsText="No hay documentos mínimos para este régimen.">
                                    <CommandItemSettings ExportToPdfText="Export to Pdf" />
                                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                                        Visible="True">
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                                        Visible="True">
                                    </ExpandCollapseColumn>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="CodDocumento" HeaderText="Código" 
                                            UniqueName="CodDocumento" Visible="False">
                                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Documento" HeaderText="Documento" 
                                            UniqueName="Documento">
                                            <HeaderStyle Font-Bold="true" HorizontalAlign="Left" Width="10%" />
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Número" 
                                            ShowFilterIcon="false">
                                            <ItemTemplate>
                                                <telerik:RadTextBox ID="edValor" runat="server" Width="80%">
                                                </telerik:RadTextBox>
                                            </ItemTemplate>
                                            <HeaderStyle Font-Bold="true" HorizontalAlign="Left" Width="8%" />
                                            <ItemStyle Width="8%" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Original">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ckbDocumentoOriginal" runat="server" AutoPostBack="true" 
                                                    Checked="false" />
                                            </ItemTemplate>
                                            <HeaderStyle Font-Bold="true" HorizontalAlign="Left" Width="4%" />
                                            <ItemStyle Width="4%" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Copia">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ckbDocumentoCopia" runat="server" AutoPostBack="true" 
                                                    Checked="false" />
                                            </ItemTemplate>
                                            <HeaderStyle Font-Bold="true" HorizontalAlign="Left" Width="4%" />
                                            <ItemStyle Width="4%" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Observación" 
                                            ShowFilterIcon="false">
                                            <ItemTemplate>
                                                <telerik:RadTextBox ID="edObservacion" runat="server" Width="95%">
                                                </telerik:RadTextBox>
                                            </ItemTemplate>
                                            <HeaderStyle Font-Bold="true" HorizontalAlign="Left" Width="20%" />
                                            <ItemStyle Width="20%" />
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                    <EditFormSettings>
                                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                        </EditColumn>
                                    </EditFormSettings>
                                </MasterTableView>
                                <FilterMenu EnableImageSprites="False">
                                </FilterMenu>
                                <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
                                </HeaderContextMenu>
                            </telerik:RadGrid>
                        </td>
                        <td style="width: 10%">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                        </td>
                        <td align="center" style="width: 80%">
                            <asp:Label ID="Label1" runat="server" Text="Otros Documentos" Font-Size="Medium"
                                ForeColor="Black"></asp:Label>
                        </td>
                        <td style="width: 10%">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                        </td>
                        <td align="center" style="width: 80%">
                            <telerik:RadGrid ID="rgOtrosDocumentos" runat="server" AutoGenerateColumns="False"
                                Width="100%" OnNeedDataSource="rgOtrosDocumentos_NeedDataSource" OnItemCreated="rgOtrosDocumentos_ItemCreated">
                                <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                                <MasterTableView DataKeyNames="Codigo" NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay otros documentos para este régimen.">
                                    <CommandItemSettings ExportToPdfText="Export to Pdf" />
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="Codigo" HeaderText="Código" UniqueName="Codigo"
                                            Visible="false">
                                            <HeaderStyle Width="10%" HorizontalAlign="Left" Font-Bold="true" />
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Descripcion" HeaderText="Documento" UniqueName="Descripcion">
                                            <HeaderStyle Width="10%" HorizontalAlign="Left" Font-Bold="true" />
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn HeaderText="Número" AllowFiltering="false" ShowFilterIcon="false">
                                            <ItemTemplate>
                                                <telerik:RadTextBox ID="edValor" runat="server" Width="80%">
                                                </telerik:RadTextBox>
                                            </ItemTemplate>
                                            <HeaderStyle Width="8%" HorizontalAlign="Left" Font-Bold="true" />
                                            <ItemStyle Width="8%" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Original">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ckbDocumentoOriginal" runat="server" Checked="false" AutoPostBack="true" />
                                            </ItemTemplate>
                                            <HeaderStyle Width="4%" HorizontalAlign="Left" Font-Bold="true" />
                                            <ItemStyle Width="4%" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Copia">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ckbDocumentoCopia" runat="server" Checked="false" AutoPostBack="true" />
                                            </ItemTemplate>
                                            <HeaderStyle Width="4%" HorizontalAlign="Left" Font-Bold="true" />
                                            <ItemStyle Width="4%" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Observación" AllowFiltering="false" ShowFilterIcon="false">
                                            <ItemTemplate>
                                                <telerik:RadTextBox ID="edObservacion" runat="server" Width="95%">
                                                </telerik:RadTextBox>
                                            </ItemTemplate>
                                            <HeaderStyle Width="20%" HorizontalAlign="Left" Font-Bold="true" />
                                            <ItemStyle Width="20%" />
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                </MasterTableView>
                                <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
                                </HeaderContextMenu>
                            </telerik:RadGrid>
                        </td>
                        <td style="width: 10%">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                        </td>
                        <td align="center" style="width: 80%">
                            <asp:Label ID="Label2" runat="server" Text="Permisos" Font-Size="Medium" ForeColor="Black"></asp:Label>
                        </td>
                        <td style="width: 10%">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                        </td>
                        <td align="center" style="width: 80%">
                            <telerik:RadGrid ID="rgPermisos" runat="server" AutoGenerateColumns="False" Width="100%"
                                OnNeedDataSource="rgPermisos_NeedDataSource" OnItemCreated="rgPermisos_ItemCreated">
                                <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                                <MasterTableView DataKeyNames="Codigo" NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay permisos.">
                                    <CommandItemSettings ExportToPdfText="Export to Pdf" />
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="Codigo" HeaderText="Código" UniqueName="Codigo"
                                            Visible="false">
                                            <HeaderStyle Width="10%" HorizontalAlign="Left" Font-Bold="true" />
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Descripcion" HeaderText="Permiso" UniqueName="Descripcion">
                                            <HeaderStyle Width="10%" HorizontalAlign="Left" Font-Bold="true" />
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn HeaderText="Permiso No." AllowFiltering="false" ShowFilterIcon="false">
                                            <ItemTemplate>
                                                <telerik:RadTextBox ID="edPermisoNo" runat="server" Width="80%" AutoPostBack="true">
                                                </telerik:RadTextBox>
                                            </ItemTemplate>
                                            <HeaderStyle Width="8%" HorizontalAlign="Left" Font-Bold="true" />
                                            <ItemStyle Width="8%" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Necesario">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ckbPermiso" runat="server" Checked="false" Enabled="false" />
                                            </ItemTemplate>
                                            <HeaderStyle Width="4%" HorizontalAlign="Left" Font-Bold="true" />
                                            <ItemStyle Width="4%" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Observación" AllowFiltering="false" ShowFilterIcon="false">
                                            <ItemTemplate>
                                                <telerik:RadTextBox ID="edObservacion" runat="server" MaxLength="250" Width="95%">
                                                </telerik:RadTextBox>
                                            </ItemTemplate>
                                            <HeaderStyle Width="24%" HorizontalAlign="Left" Font-Bold="true" />
                                            <ItemStyle Width="24%" />
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                </MasterTableView>
                                <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
                                </HeaderContextMenu>
                            </telerik:RadGrid>
                        </td>
                        <td style="width: 10%">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                        </td>
                        <td align="center" style="width: 80%">
                            <asp:Label ID="Label3" runat="server" Text="Especies Fiscales" Font-Size="Medium"
                                ForeColor="Black"></asp:Label>
                        </td>
                        <td style="width: 10%">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%">
                        </td>
                        <td align="center" style="width: 80%">
                            <telerik:RadGrid ID="rgEspeciesFiscales" runat="server" AutoGenerateColumns="False"
                                Width="100%" OnNeedDataSource="rgEspeciesFiscales_NeedDataSource" OnItemCreated="rgEspeciesFiscales_ItemCreated"
                                OnItemDataBound="rgEspeciesFiscales_ItemDataBound" CellSpacing="0" 
                                GridLines="None">
                                <PagerStyle NextPagesToolTip="Páginas Siguientes" NextPageToolTip="Página Siguiente"
                                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Páginas Anteriores" PrevPageToolTip="Página Anterior" />
                                <MasterTableView DataKeyNames="Codigo" NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay especies fiscales.">
                                    <CommandItemSettings ExportToPdfText="Export to PDF" />
                                    <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                                        Visible="True">
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                                        Visible="True">
                                    </ExpandCollapseColumn>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="Codigo" HeaderText="Código" 
                                            UniqueName="Codigo" Visible="False">
                                            <HeaderStyle Font-Bold="true" HorizontalAlign="Left" Width="10%" />
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="Descripcion" HeaderText="Especie Fiscal" 
                                            UniqueName="Descripcion">
                                            <HeaderStyle Font-Bold="true" HorizontalAlign="Left" Width="10%" />
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Serie No." 
                                            ShowFilterIcon="false">
                                            <ItemTemplate>
                                                <telerik:RadTextBox ID="edSerieEF" runat="server" Enabled="false" Width="90%">
                                                </telerik:RadTextBox>
                                            </ItemTemplate>
                                            <HeaderStyle Font-Bold="true" HorizontalAlign="Left" Width="18%" />
                                            <ItemStyle Width="18%" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Cantidad" 
                                            ShowFilterIcon="false">
                                            <ItemTemplate>
                                                <telerik:RadNumericTextBox ID="edCantidad" runat="server" MaxValue="500" 
                                                    MinValue="1" NumberFormat-DecimalDigits="0" ShowSpinButtons="true" Value="1" 
                                                    Width="80%">
                                                </telerik:RadNumericTextBox>
                                                <asp:RequiredFieldValidator ID="reqName" runat="server" 
                                                    ControlToValidate="edCantidad" ErrorMessage="*****" ForeColor="Red" 
                                                    SetFocusOnError="true" />
                                            </ItemTemplate>
                                            <HeaderStyle Font-Bold="true" HorizontalAlign="Left" Width="1%" />
                                            <ItemStyle Width="1%" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Necesario">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ckbEspecieFiscal" runat="server" AutoPostBack="true" 
                                                    Checked="false" />
                                            </ItemTemplate>
                                            <HeaderStyle Font-Bold="true" HorizontalAlign="Left" Width="4%" />
                                            <ItemStyle Width="4%" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Cliente">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="ckbCliente" runat="server" AutoPostBack="true" 
                                                    Checked="false" />
                                            </ItemTemplate>
                                            <HeaderStyle Font-Bold="true" HorizontalAlign="Left" Width="4%" />
                                            <ItemStyle Width="4%" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn AllowFiltering="false" HeaderText="Observación" 
                                            ShowFilterIcon="false">
                                            <ItemTemplate>
                                                <telerik:RadTextBox ID="edObservacion" runat="server" MaxLength="250" 
                                                    Width="95%">
                                                </telerik:RadTextBox>
                                            </ItemTemplate>
                                            <HeaderStyle Font-Bold="true" HorizontalAlign="Left" Width="14%" />
                                            <ItemStyle Width="14%" />
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                    <EditFormSettings>
                                        <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                        </EditColumn>
                                    </EditFormSettings>
                                </MasterTableView>
                                <FilterMenu EnableImageSprites="False">
                                </FilterMenu>
                                <HeaderContextMenu CssClass="GridContextMenu GridContextMenu_Default" EnableImageSprites="True">
                                </HeaderContextMenu>
                            </telerik:RadGrid>
                        </td>
                        <td style="width: 10%">
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr align="center">
                        <td colspan="6" align="center">
                            <asp:ImageButton ID="btnGuardar" ToolTip="Salvar" ImageUrl="~/Images/24/disk_blue_ok_24.png"
                                OnClientClick="radconfirm('Esta seguro de Salvar?',confirmCallBackSalvar, 300, 100); return false;"
                                runat="server" />
                            <asp:ImageButton ID="btnBack" ToolTip="Regresar" ImageUrl="~/Images/24/arrow_left_green_24.png"
                                runat="server" CausesValidation="false" OnClick="btnBack_Click" />
                            <asp:ImageButton ID="btnSalvar" OnClick="btnSalvar_Click" ImageUrl="~/Images/gris.png"
                                runat="server" />
                        </td>
                    </tr>
                </table>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </telerik:RadAjaxPanel>
</asp:Content>
