﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using Telerik.Web.UI;
using System.Security.Principal;
using System.Text;

public partial class Principal : Utilidades.PaginaBase
{
    private ModulosUsuario usuario;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckVencimiento();

            AddControlToolTip(Controls);

            Int32 count;
            RadSiteMap item;
            RadMenuItem itemMenu;
            RadMenuItem itemMenu2;
            usuario = new ModulosUsuario(GetModulosRole());

            // Administración


            itemMenu = RadMenuPrincipal.Items.FindItemByAttribute("Name", "Mantenimiento");
            itemMenu2 = itemMenu.Items.FindItemByAttribute("Name", "Mantenimiento");
            if (itemMenu != null)
            {
                item = (RadSiteMap)itemMenu2.FindControl("RadSiteMap1");
                count = 0;
                ShowItems(item, ref count);
                itemMenu.Visible = count > 0;
            }

            itemMenu = RadMenuPrincipal.Items.FindItemByAttribute("Name", "Operaciones");
            itemMenu2 = itemMenu.Items.FindItemByAttribute("Name", "Operaciones");
            if (itemMenu != null)
            {
                item = (RadSiteMap)itemMenu2.FindControl("RadSiteMap2");
                count = 0;
                ShowItems(item, ref count);
                itemMenu.Visible = count > 0;
            }
        }
    }

    private string[] GetModulosRole()
    {
        string modulosRole = "";
        IPrincipal user = HttpContext.Current.User;
        string idRole = "";
        conectar();
        RolesBO roles = new RolesBO(logApp);
        ModulosBO modulos = new ModulosBO(logApp);
        roles.loadRoles();
        for (int i = 0; i < roles.TABLA.Rows.Count; i++)
        {
            if (user.IsInRole(roles.DESCRIPCION))
            {
                idRole = roles.IDROL;
                modulos.loadModulosRol(idRole);
                for (int j = 0; j < modulos.TABLA.Rows.Count; j++)
                {
                    modulosRole += modulos.DESCRIPCION + ",";
                    modulos.regSiguiente();
                }
            }
            roles.regSiguiente();
        }
        desconectar();
        if (modulosRole.Length > 0)
            modulosRole = modulosRole.Remove(modulosRole.Length - 1);
        return modulosRole.Split(",".ToCharArray(), modulosRole.Length);
    }

    private void ShowItems(RadSiteMap parent, ref Int32 count)
    {
        if (parent == null) return;

        foreach (RadSiteMapNode item in parent.Nodes)
        {
            Int32 countNode = 0;
            ShowItemsNode(item, ref countNode);
            item.Visible = countNode > 0;
            if (countNode > 0)
                count++;
        }
    }

    private void ShowItemsNode(RadSiteMapNode parent, ref Int32 count)
    {
        if (parent == null) return;

        foreach (RadSiteMapNode item in parent.Nodes)
        {
            if (usuario.isInModule(item.Value) || item.Value == "Todos")
            {
                item.Visible = true;
                count++;
            }
            else
                item.Visible = false;
        }
    }

    protected void CheckVencimiento()
    {
        if (Path.GetFileNameWithoutExtension(Request.PhysicalPath).ToLower() == "mantenimientocambiarclave")
            return;

        try
        {
            conectar();
            UsuariosBO bo = new UsuariosBO(logApp);
            bo.loadUsuario(Session["IdUsuario"].ToString());

            if (DateTime.Now > bo.VENCIMIENTO)
            {
                redirectTo("MantenimientoCambiarClave.aspx?titulo=La Clave ya Caduco. Debe Cambiar la Clave.");
            }
        }
        catch
        {
        }
        finally
        {
            desconectar();
        }
    }

    public void AddControlToolTip(Control control)
    {
        if ((control != this) && (control is WebControl))
        {
            String tooltip = String.Empty;

            if (control is Image)
                tooltip = ((WebControl)control).ToolTip.Trim().Length > 0 ? ((WebControl)control).ToolTip : ((Image)control).AlternateText;
            else if (control is WebControl)
                tooltip = ((WebControl)control).ToolTip;
            else if (control is Telerik.Web.UI.RadGrid)
                tooltip = "o";

            if (tooltip.Trim().Length > 0)
                RadToolTipManager1.TargetControls.Add(control.ClientID, tooltip.Trim(), true);
        }
    }

    public void AddControlToolTip(ControlCollection controls)
    {
        foreach (Control control in controls)
        {
            AddControlToolTip(control);

            if (control.HasControls())
                AddControlToolTip(control.Controls);
        }
    }

    //protected String Skin { get { return (Session["Skin"] != null ? Session["Skin"].ToString() : "Sunset"); } }

    public override bool CanGoBack { get { return false; } }

}