﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ObjetosDto;
using ObjetosDto.Data;
using Telerik.Web.UI;
using Utilities.Web;

namespace Aduanas
{
    public partial class FileShare : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var sfileshare = SessionFileShare.GetSessionVarFileShare(this);

            if (sfileshare != null)
            {
                CargarPagina();
                respHidden.Value = sfileshare.IdRespuesta.ToString();
            }
            else
            {
                Mensaje("No inicio session en FileShare");
            }
                
        }

        private void CargarPagina()
        {
            var session = SessionFileShare.GetSessionVarFileShare(this);
            if (session.Operacion)
            {
                if (SessionFileShare.MultiTramite)
                {
                    try
                    {
                        var obj = session.Dato;

                        var resp = WebApiCalls.PostJsonToWebApi<DataRequest, RespuestaDataRequest>(session.Direccion.UrlFileShareWebApi,
                            "SyncFolderFilesApi","PostDataRequest",obj);

                        var result = resp.Result;
                        if (result != null)
                        {
                            var jsonresult = result.ToJson();
                            var jsonBase64 = jsonresult.EncodeTo64();

                            session.IdRespuesta = result.IdFolderRequest;
                            Session["FileShareSession"] = session;

                            //CargarWindow("Upload File(s)", "http://fileshare.grupovesta.com/Default.aspx?pagina=FileShareUploadMult&datos="+jsonBase64, 850, 500);
                            CargarWindow("Upload File(s)", session.Direccion.UrlFileShare + "/Default.aspx?pagina=FileShareUploadMult&datos=" + jsonBase64, 850, 500);
                        }
                        
                    }
                    catch (Exception ex)
                    {
                        Mensaje(ex.Message);
                    }
                }
                else
                {
                    try
                    {

                        var obj = session.Dato;

                        var resp = WebApiCalls.PostJsonToWebApi<DataRequest, RespuestaDataRequest>(session.Direccion.UrlFileShareWebApi,
                           "SyncFolderFilesApi", "PostDataRequest", obj);

                        var result = resp.Result;

                        if (result != null)
                        {
                            var jsonresult = result.ToJson();
                            var jsonBase64 = jsonresult.EncodeTo64();

                            session.IdRespuesta = result.IdFolderRequest;
                            Session["FileShareSession"] = session;

                            //CargarWindow("Upload File(s)", "http://localhost:29052/Default.aspx?pagina=FileShareUploadClientSide&datos=" + jsonBase64, 850, 500);
                            CargarWindow("Upload File(s)",
                                session.Direccion.UrlFileShare + "Default.aspx?pagina=FileShareUploadClientSide&datos=" +
                                jsonBase64, 850, 500);
                            decimal? g = (decimal)52.04;
                        }
                        else
                        {
                            Mensaje("Error en la comunicacion");
                        }
                    }
                    catch (Exception ex)
                    {
                        Mensaje(ex.Message);
                    }
                }
            }
            else
            {
                var dato = session.IdSistemaDownload;
                var base64Dato = JsonUtils.EncodeTo64(new DatoDownload().DatoDownloadJson(dato));
                var download = new Download()
                {
                    IdTramite = dato,
                    Estado = 0
                };
                var resp = WebApiCalls.PostJsonToWebApi<Download,long>(session.Direccion.UrlFileShareWebApi,
                           "SyncFolderFilesApi", "PostDownloadRequest", download).Result;

                session.IdRespuesta = resp;

                CargarWindow("Download File(s)", session.Direccion.UrlFileShare + "Default.aspx?pagina=FileShareDloadClienteSide&datos="
                    + base64Dato + "&id=" + resp.ToString(), 850, 540);
               
            }
        }

        private void CargarWindow(String titleBar, string navigateUrl, int width, int heigth)
        {
            RadWindowManager1.Windows.Clear();
            var window1 = new RadWindow
            {
                NavigateUrl = navigateUrl,
                VisibleOnPageLoad = true,
                Title = titleBar,
                Animation = WindowAnimation.FlyIn,
                Modal = true,
                DestroyOnClose = true,
                VisibleStatusbar = false,
                OnClientClose = "close"
            };
            if (width > 0)
            {
                window1.Width = width;
                window1.Height = heigth;
                window1.Behaviors = WindowBehaviors.Pin | WindowBehaviors.Move | WindowBehaviors.Close;
            }
            else
            {
                window1.InitialBehaviors = WindowBehaviors.Maximize;
            }
            RadWindowManager1.Windows.Add(window1);
        }

        private void Mensaje(string msj)
        {
            StringBuilder sb = new StringBuilder();
            var alert = "alert('" + msj + "');";

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", alert, true);
        }

    }
}