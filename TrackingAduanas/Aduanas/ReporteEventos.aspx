﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteEventos.aspx.cs" Inherits="ReporteEventos" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                    //<![CDATA[
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }

                function openWin() {
                    var oWnd = radopen("BuscarTramite.aspx", "RadWindow1");
                    oWnd.add_close(OnClientClose);
                }           
                
            </script>
        </telerik:RadScriptBlock>
        <table border="1" style="width: 45%">
            <tr>
                <td style="width: 15%">
                    <asp:Label ID="lblFechaInicio" runat="server" Text="Fecha Inicio:" ForeColor="Black"></asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadDatePicker ID="dpFechaInicio" runat="server" EnableTyping="False" Width="95%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="dpFechaInicio"
                        ErrorMessage="Ingrese Fecha" ForeColor="Red" />
                </td>
                <td style="width: 15%">
                    <asp:Label ID="lblFechaFinal" runat="server" Text="Fecha Final:" ForeColor="Black"></asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadDatePicker ID="dpFechaFinal" runat="server" EnableTyping="False" Width="95%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator runat="server" ID="reqName" ControlToValidate="dpFechaFinal"
                        ErrorMessage="Ingrese Fecha" ForeColor="Red" />
                </td>
                <td style="width: 10%">
                    <asp:Label ID="lblPais" runat="server" Text="País:" ForeColor="Black"></asp:Label>
                </td>
                <td style="width: 16%">
                    <telerik:RadComboBox ID="cmbPais" runat="server" DataTextField="Descripcion" DataValueField="Codigo">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td colspan="6" align="center">
                    <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Images/24/view_24.png"
                        OnClick="btnBuscar_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="rgInstrucciones" enableajax="True" ShowStatusBar="true" runat="server"
            Visible="false" AllowFilteringByColumn="True" Skin="Default" Width="100%"
            Height="410px" AutoGenerateColumns="False" PageSize="20" AllowSorting="True"
            AllowMultiRowSelection="False" AllowPaging="True" GridLines="None" OnDetailTableDataBind="rgInstrucciones_DetailTableDataBind"
            OnNeedDataSource="rgInstrucciones_NeedDataSource" OnItemCommand="rgInstrucciones_ItemCommand">
            <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView Width="100%" DataKeyNames="IdInstruccion" AllowMultiColumnSorting="True"
                HierarchyDefaultExpanded="true" CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros."
                NoMasterRecordsText="No hay Instruciones." GroupLoadMode="Client">
                <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                    ShowExportToExcelButton="true" ShowExportToCsvButton="false" />
                <DetailTables>
                    <telerik:GridTableView DataKeyNames="IdInstruccion" DataMember="DetalleEventos" Width="100%"
                        GridLines="Horizontal" Style="border-color: #d5b96a" CssClass="RadGrid2" AllowFilteringByColumn="false">
                        <Columns>
                            <telerik:GridBoundColumn SortExpression="Evento" HeaderText="Evento" HeaderButtonType="TextButton"
                                DataField="Evento">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="FechaInicio" HeaderText="FechaInicio" HeaderButtonType="TextButton"
                                DataField="FechaInicio">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn SortExpression="FechaFin" HeaderText="FechaFin" HeaderButtonType="TextButton"
                                DataField="FechaFin">
                            </telerik:GridBoundColumn>
                        </Columns>
                    </telerik:GridTableView>
                </DetailTables>
                <Columns>
                    <telerik:GridBoundColumn SortExpression="IdInstruccion" HeaderText="IdInstruccion"
                        HeaderButtonType="TextButton" DataField="IdInstruccion">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="Nombre" HeaderText="Cliente" HeaderButtonType="TextButton"
                        DataField="Nombre">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn SortExpression="ReferenciaCliente" HeaderText="ReferenciaCliente"
                        HeaderButtonType="TextButton" DataField="ReferenciaCliente">
                    </telerik:GridBoundColumn>
                </Columns>
            </MasterTableView>
            <HeaderStyle Width="180px" />
            <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                <Selecting AllowRowSelect="True" />
                <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                    DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                    DropHereToReorder="Suelte aquí para Re-Ordenar" />
                <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
            </ClientSettings>
            <FilterMenu EnableTheming="True">
                <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
            </FilterMenu>
            <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
            <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
        </telerik:RadGrid>
    </telerik:RadAjaxPanel>
</asp:Content>
