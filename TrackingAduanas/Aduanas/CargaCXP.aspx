﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="CargaCXP.aspx.cs" Inherits="GenerarArchivoSAPDinant" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style12
        {
            width: 100%;
            height: 50px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="pnlRequestStarted">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">

                function pnlRequestStarted(ajaxPanel, eventArgs) {
                    try {
                        if ((eventArgs.EventTarget == "<%= btnSalvar.UniqueID %>")) {
                            eventArgs.EnableAjax = false;
                        }
                    }
                    catch (err)
                { }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                    }
                }                                  
            </script>
        </telerik:RadScriptBlock>
        <input id="cmbMaterial" runat="server" type="hidden"  />
        <input id="cmbMedioPago" runat="server" type="hidden"  />
        <input id="cmbPagarA" runat="server" type="hidden"  />
        <input id="txtMonto" runat="server" type="hidden"  />
        <input id="txtIVA" runat="server" type="hidden"  />
        <input id="txtMontoPagar" runat="server" type="hidden"  />
        <input id="cmbMoneda" runat="server" type="hidden"  />
        <input id="edFecha" runat="server" type="hidden" />
        <input id="edHora" runat="server" type="hidden" />
        
        <br />
        <telerik:RadGrid ID="rgGastos" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" BorderStyle="Solid" 
            CellSpacing="0" GridLines="None" 
            onneeddatasource="RadGrid1_NeedDataSource" PageSize="10000" 
            style="margin-right: 0px; text-align: left;" Width="1500px" Culture="es-ES">
            <ClientSettings AllowColumnsReorder="True">
                <Selecting AllowRowSelect="True" />
            </ClientSettings>
            <ExportSettings FileName="Reporte" HideStructureColumns="true" 
                OpenInNewWindow="True">
                <Excel AutoFitImages="True" Format="Biff" />
            </ExportSettings>
            <MasterTableView CommandItemDisplay="Top" >
                <CommandItemSettings ExportToPdfText="Export to PDF" 
                    ShowAddNewRecordButton="false" ShowExportToExcelButton="true" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                    Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                    Visible="True">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="HojaRuta" 
                        FilterControlAltText="Filter HojaRuta column" HeaderText="Hoja de Ruta" 
                        UniqueName="HojaRuta">
                        <HeaderStyle Width="70px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Clientes" 
                        FilterControlAltText="Filter Cliente column" HeaderText="Cliente"
                        UniqueName="Cliente">
                        <HeaderStyle Width="200px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Id" 
                        FilterControlAltText="Filter Id column" HeaderText="IdSolicutd" 
                        UniqueName="Id">
                        <HeaderStyle Width="20px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Material" 
                        FilterControlAltText="Filter Material column" HeaderText="Matearial" 
                        UniqueName="Material">
                        <HeaderStyle Width="60px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CuentaSAP" 
                        FilterControlAltText="Filter CuentaSAP column" HeaderText="CuentaSAP" 
                        UniqueName="CuentaSAP">
                        <HeaderStyle Width="40px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Monto" 
                        FilterControlAltText="Filter Monto column" HeaderText="Monto" 
                        UniqueName="Monto" DataFormatString="{0:#,###.00}">
                        <HeaderStyle Width="50px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IVA" 
                        FilterControlAltText="Filter IVA column" HeaderText="IVA" UniqueName="IVA" DataFormatString="{0:#,###.00}">
                        <HeaderStyle Width="50px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Total" 
                        FilterControlAltText="Filter Total column" HeaderText="Monto a Pagar" 
                        UniqueName="Total" DataFormatString="{0:#,###.00}">
                        <HeaderStyle Width="50px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="MedioPago" 
                        FilterControlAltText="Filter MedioPago column" HeaderText="Medio Pago" 
                        UniqueName="MedioPago">
                        <HeaderStyle Width="60px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Proveedor" 
                        FilterControlAltText="Filter Proveedor column" HeaderText="Proveedor" 
                        UniqueName="Proveedor">
                        <HeaderStyle Width="200px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CeCo" 
                        FilterControlAltText="Filter CeCo column" HeaderText="CeCo" UniqueName="CeCo" 
                        Visible="False">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Serie" 
                        FilterControlAltText="Filter Serie column" HeaderText="Serie" 
                        UniqueName="Serie">
                        <HeaderStyle Width="50px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CodigoProveedor" 
                        FilterControlAltText="Filter CodigoProveedor column" 
                        HeaderText="Codigo Proveedor" UniqueName="CodigoProveedor">
                        <HeaderStyle Width="50px" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <ItemStyle BorderColor="Black" BorderStyle="Solid" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        <table class="style12">
            <tr>
                <td style="text-align: center">
                     <asp:ImageButton ID="btnGuardar" runat="server" 
                         ImageUrl="~/Images/24/disk_blue_ok_24.png" 
                         OnClientClick="radconfirm('¿Esta Seguro de Generar la Carga?',confirmCallBackSalvar, 300, 100); return false;" 
                         ToolTip="Salvar" Visible="true" />
                     <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="~/Images/gris.png" 
                         OnClick="btnSalvar_Click" />
                     <asp:Button ID="btnCargar" runat="server" Text="Cargar Archivo" 
                         Visible="false" />
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
    <%--<GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>--%>
</asp:Content>
