﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;

public partial class AnulacionEspeciesFiscales : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Anulacion Especies Fiscales";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgRangoEspeciesFiscales.FilterMenu);
        rgRangoEspeciesFiscales.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgRangoEspeciesFiscales.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Anulación Especies Fiscales", "Anulación Especies Fiscales");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
            mpEspeciesFiscales.SelectedIndex = 0;
            limpiarControles();
            cargarDatosInicioRangos();
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    #region Anulación Especies Fiscales
    protected void rgRangoEspeciesFiscales_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenarGridRangoEspeciesFiscales();
    }

    private void llenarGridRangoEspeciesFiscales()
    {
        try
        {
            conectar();
            MovimientosEspeciesFiscalesBO mef = new MovimientosEspeciesFiscalesBO(logApp);
            mef.loadRangosEspeciesFiscalesAnulacion(cmbAduana.SelectedValue, cmbEspecieFiscal.SelectedValue);
            rgRangoEspeciesFiscales.DataSource = mef.TABLA;
            rgRangoEspeciesFiscales.DataBind();
        }
        catch { }
    }

    protected void rgRangoEspeciesFiscales_Init(object sender, EventArgs e)
    {
        GridFilterMenu menu = rgRangoEspeciesFiscales.FilterMenu;
        menu.Items.RemoveAt(rgRangoEspeciesFiscales.FilterMenu.Items.Count - 2);
    }

    private void cargarDatosInicioRangos()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (u.CODPAIS == "H")
                c.loadPaisesHojaRuta();
            else
                c.loadAllCampos("PAISES", u.CODPAIS);
            cmbPais.DataSource = c.TABLA;
            cmbPais.DataBind();
            c.loadEspeciesnoAdmin();
            cmbEspecieFiscal.DataSource = c.TABLA;
            cmbEspecieFiscal.DataBind();
            loadAduanasOrigen();
            c.loadAllCampos("RAZONANULACION");
            cmbRazonAnulacion.DataSource = c.TABLA;
            cmbRazonAnulacion.DataBind();
            cmbEspecieFiscal.Items.Remove(9);
        }
        catch { }
    }

    private void loadAduanasOrigen()
    {
        try
        {
            conectar();
            AduanasBO a = new AduanasBO(logApp);
            a.loadPaisAduanas(cmbPais.SelectedValue);
            cmbAduana.DataSource = a.TABLA;
            cmbAduana.DataBind();
        }
        catch { }
    }

    protected void cmbPais_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        loadAduanasOrigen();
    }

    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            conectar();
            MovimientosEspeciesFiscalesBO mef = new MovimientosEspeciesFiscalesBO(logApp);
            MovimientosEspeciesFiscalesBO mefe = new MovimientosEspeciesFiscalesBO(logApp);
            AnulacionesEspeciesFiscalesBO aef = new AnulacionesEspeciesFiscalesBO(logApp);
            EspeciesFiscalesInstruccionesBO efi = new EspeciesFiscalesInstruccionesBO(logApp);
            EspeciesFiscalesInstruccionesBO efi2 = new EspeciesFiscalesInstruccionesBO(logApp);
            TrasladosEspeciesFiscalesBO tef = new TrasladosEspeciesFiscalesBO(logApp);
            TrasladosEspeciesFiscalesBO tefe = new TrasladosEspeciesFiscalesBO(logApp);
            EspeciesFiscalesClientesBO efc = new EspeciesFiscalesClientesBO(logApp);
            if (!cmbPais.IsEmpty)
            {
                if (!cmbAduana.IsEmpty)
                {
                    if (!cmbEspecieFiscal.IsEmpty)
                    {
                        if (txtSerie.Value.HasValue)
                        {
                            if (!cmbRazonAnulacion.Text.Contains("Seleccione Razón de Anulación"))
                            {
                                int cantidadInicial = 0, cantidadFinal = 0;
                                bool registrar = false;
                                DateTime fecha = DateTime.Now;      //20120109.171835.761                                            
                                string idMovimiento = fecha.Year.ToString() + fecha.Month.ToString("00") + fecha.Day.ToString("00") + "." + fecha.Hour.ToString("00") + fecha.Minute.ToString("00") + fecha.Second.ToString("00") + "." + fecha.Millisecond.ToString("000");
                                mef.loadRangoEspeciesFiscalesXSerie(txtSerie.Text, cmbAduana.SelectedValue, cmbEspecieFiscal.SelectedValue);
                                if (mef.totalRegistros > 0)
                                {
                                    tef.loadTrasladosEspeciesFiscalesXIdMov(mef.IDMOVIMIENTO);
                                    cantidadInicial = int.Parse((txtSerie.Value - int.Parse(mef.RANGOINICIAL)).ToString());
                                    cantidadFinal = int.Parse((int.Parse(mef.RANGOFINAL) - txtSerie.Value).ToString());
                                    if (cantidadInicial == 0 & cantidadFinal > 0)
                                    {
                                        //guardar nuevo registro con las series que quedan
                                        mefe.loadMovimientosEspeciesFiscales("-1");
                                        mefe.newLine();
                                        mefe.IDMOVIMIENTO = idMovimiento;
                                        mefe.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                                        mefe.CODPAIS = cmbPais.SelectedValue;
                                        mefe.CODIGOADUANA = cmbAduana.SelectedValue;
                                        mefe.RANGOINICIAL = (int.Parse(mef.RANGOINICIAL) + 1).ToString();
                                        mefe.RANGOFINAL = mef.RANGOFINAL;
                                        mefe.CANTIDAD = cantidadFinal;
                                        mefe.FECHA = DateTime.Now.ToString();
                                        mefe.CODTIPOMOVIMIENTO = "2";   //Anulacion
                                        mefe.CODESTADO = mef.CODESTADO;
                                        mefe.IDUSUARIO = Session["IdUsuario"].ToString();
                                        mefe.IDCOMPRA = mef.IDCOMPRA;
                                        mefe.commitLine();
                                        mefe.actualizar();
                                        registrar = true;

                                        try
                                        {
                                            if (cmbEspecieFiscal.SelectedValue == "DVA" || cmbEspecieFiscal.SelectedValue == "F")
                                            {
                                                efc.loadEspeciesFiscalesClientesXIdMov(mef.IDMOVIMIENTO);
                                                if (efc.totalRegistros > 0)
                                                {
                                                    string idCliente = efc.IDCLIENTE;
                                                    //guardar nuevo registro con las series que se envian al cliente
                                                    efc.loadEspeciesFiscalesClientesXIdMov("-1");
                                                    efc.newLine();
                                                    efc.IDMOVIMIENTO = idMovimiento;
                                                    efc.IDCLIENTE = idCliente;
                                                    efc.OBSERVACION = "";
                                                    efc.commitLine();
                                                    efc.actualizar();
                                                }
                                            }
                                        }
                                        catch { }

                                    }
                                    else if (cantidadFinal == 0 & cantidadInicial > 0)
                                    {
                                        //guardar nuevo registro con las series que quedan
                                        mefe.loadMovimientosEspeciesFiscales("-1");
                                        mefe.newLine();
                                        mefe.IDMOVIMIENTO = idMovimiento;
                                        mefe.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                                        mefe.CODPAIS = cmbPais.SelectedValue;
                                        mefe.CODIGOADUANA = cmbAduana.SelectedValue;
                                        mefe.RANGOINICIAL = mef.RANGOINICIAL;
                                        mefe.RANGOFINAL = (int.Parse(mef.RANGOFINAL) - 1).ToString(); ;
                                        mefe.CANTIDAD = cantidadInicial;
                                        mefe.FECHA = DateTime.Now.ToString();
                                        mefe.CODTIPOMOVIMIENTO = "2";   //Anulacion
                                        mefe.CODESTADO = mef.CODESTADO;
                                        mefe.IDUSUARIO = Session["IdUsuario"].ToString();
                                        mefe.IDCOMPRA = mef.IDCOMPRA;
                                        mefe.commitLine();
                                        mefe.actualizar();
                                        registrar = true;

                                        try
                                        {
                                            if (cmbEspecieFiscal.SelectedValue == "DVA" || cmbEspecieFiscal.SelectedValue == "F")
                                            {
                                                efc.loadEspeciesFiscalesClientesXIdMov(mef.IDMOVIMIENTO);
                                                if (efc.totalRegistros > 0)
                                                {
                                                    string idCliente = efc.IDCLIENTE;
                                                    //guardar nuevo registro con las series que se envian al cliente
                                                    efc.loadEspeciesFiscalesClientesXIdMov("-1");
                                                    efc.newLine();
                                                    efc.IDMOVIMIENTO = idMovimiento;
                                                    efc.IDCLIENTE = idCliente;
                                                    efc.OBSERVACION = "";
                                                    efc.commitLine();
                                                    efc.actualizar();
                                                }
                                            }
                                        }
                                        catch { }
                                    }
                                    else if (cantidadInicial > 0 & cantidadFinal > 0)
                                    {
                                        //guardar nuevo registro con las series que quedan
                                        mefe.loadMovimientosEspeciesFiscales("-1");
                                        mefe.newLine();
                                        mefe.IDMOVIMIENTO = idMovimiento;
                                        mefe.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                                        mefe.CODPAIS = cmbPais.SelectedValue;
                                        mefe.CODIGOADUANA = cmbAduana.SelectedValue;
                                        mefe.RANGOINICIAL = mef.RANGOINICIAL;
                                        mefe.RANGOFINAL = (int.Parse(txtSerie.Text) - 1).ToString(); ;
                                        mefe.CANTIDAD = cantidadInicial;
                                        mefe.FECHA = DateTime.Now.ToString();
                                        mefe.CODTIPOMOVIMIENTO = "2";   //Anulacion
                                        mefe.CODESTADO = mef.CODESTADO;
                                        mefe.IDUSUARIO = Session["IdUsuario"].ToString();
                                        mefe.IDCOMPRA = mef.IDCOMPRA;
                                        mefe.commitLine();
                                        mefe.actualizar();

                                        //guardar nuevo registro en traslados
                                        if (tef.totalRegistros > 0)
                                        {
                                            tefe.loadTrasladosEspeciesFiscales("-1");
                                            tefe.newLine();
                                            tefe.IDENVIO = idMovimiento;
                                            tefe.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                                            tefe.CODPAIS = cmbPais.SelectedValue;
                                            tefe.CODIGOADUANAORIGEN = tef.CODIGOADUANAORIGEN;
                                            tefe.CODIGOADUANADESTINO = tef.CODIGOADUANADESTINO;
                                            tefe.RANGOINICIAL = mefe.RANGOINICIAL;
                                            tefe.RANGOFINAL = mefe.RANGOFINAL;
                                            tefe.CANTIDAD = mefe.CANTIDAD;
                                            tefe.FECHA = DateTime.Now.ToString();
                                            tefe.IDUSUARIO = mefe.IDUSUARIO;
                                            tefe.IDMOVIMIENTO = mef.IDMOVIMIENTO;
                                            tefe.commitLine();
                                            tefe.actualizar();
                                        }

                                        try
                                        {
                                            if (cmbEspecieFiscal.SelectedValue == "DVA" || cmbEspecieFiscal.SelectedValue == "F")
                                            {
                                                efc.loadEspeciesFiscalesClientesXIdMov(mef.IDMOVIMIENTO);
                                                if (efc.totalRegistros > 0)
                                                {
                                                    string idCliente = efc.IDCLIENTE;
                                                    //guardar nuevo registro con las series que se envian al cliente
                                                    efc.loadEspeciesFiscalesClientesXIdMov("-1");
                                                    efc.newLine();
                                                    efc.IDMOVIMIENTO = idMovimiento;
                                                    efc.IDCLIENTE = idCliente;
                                                    efc.OBSERVACION = "";
                                                    efc.commitLine();
                                                    efc.actualizar();
                                                }
                                            }
                                        }
                                        catch { }

                                        fecha = DateTime.Now;      //20120109.171835.761                                            
                                        idMovimiento = fecha.Year.ToString() + fecha.Month.ToString("00") + fecha.Day.ToString("00") + "." + fecha.Hour.ToString("00") + fecha.Minute.ToString("00") + fecha.Second.ToString("00") + "." + fecha.Millisecond.ToString("000");

                                        //guardar nuevo registro con las series que quedan
                                        mefe.loadMovimientosEspeciesFiscales("-1");
                                        mefe.newLine();
                                        mefe.IDMOVIMIENTO = idMovimiento;
                                        mefe.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                                        mefe.CODPAIS = cmbPais.SelectedValue;
                                        mefe.CODIGOADUANA = cmbAduana.SelectedValue;
                                        mefe.RANGOINICIAL = (int.Parse(txtSerie.Text) + 1).ToString();
                                        mefe.RANGOFINAL = mef.RANGOFINAL;
                                        mefe.CANTIDAD = cantidadFinal;
                                        mefe.FECHA = DateTime.Now.ToString();
                                        mefe.CODTIPOMOVIMIENTO = "2";   //Anulacion
                                        mefe.CODESTADO = mef.CODESTADO;
                                        mefe.IDUSUARIO = Session["IdUsuario"].ToString();
                                        mefe.IDCOMPRA = mef.IDCOMPRA;
                                        mefe.commitLine();
                                        mefe.actualizar();

                                        //guardar nuevo registro en traslados
                                        if (tef.totalRegistros > 0)
                                        {
                                            tefe.loadTrasladosEspeciesFiscales("-1");
                                            tefe.newLine();
                                            tefe.IDENVIO = idMovimiento;
                                            tefe.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                                            tefe.CODPAIS = cmbPais.SelectedValue;
                                            tefe.CODIGOADUANAORIGEN = tef.CODIGOADUANAORIGEN;
                                            tefe.CODIGOADUANADESTINO = tef.CODIGOADUANADESTINO;
                                            tefe.RANGOINICIAL = mefe.RANGOINICIAL;
                                            tefe.RANGOFINAL = mefe.RANGOFINAL;
                                            tefe.CANTIDAD = mefe.CANTIDAD;
                                            tefe.FECHA = DateTime.Now.ToString();
                                            tefe.IDUSUARIO = mefe.IDUSUARIO;
                                            tefe.IDMOVIMIENTO = mef.IDMOVIMIENTO;
                                            tefe.commitLine();
                                            tefe.actualizar();
                                        }

                                        try
                                        {
                                            if (cmbEspecieFiscal.SelectedValue == "DVA" || cmbEspecieFiscal.SelectedValue == "F")
                                            {
                                                efc.loadEspeciesFiscalesClientesXIdMov(mef.IDMOVIMIENTO);
                                                if (efc.totalRegistros > 0)
                                                {
                                                    string idCliente = efc.IDCLIENTE;
                                                    //guardar nuevo registro con las series que se envian al cliente
                                                    efc.loadEspeciesFiscalesClientesXIdMov("-1");
                                                    efc.newLine();
                                                    efc.IDMOVIMIENTO = idMovimiento;
                                                    efc.IDCLIENTE = idCliente;
                                                    efc.OBSERVACION = "";
                                                    efc.commitLine();
                                                    efc.actualizar();
                                                }
                                            }
                                        }
                                        catch { }

                                        registrar = true;
                                    }
                                    else if (cantidadInicial == 0 & cantidadFinal == 0)
                                        registrar = true;

                                    if (tef.totalRegistros > 0)
                                    {
                                        tef.ELIMINADO = "1";
                                        tef.actualizar();
                                    }

                                    if (registrar == true)
                                    {
                                        //guardar registro con la serie anulada
                                        aef.loadAnulacionesEspeciesFiscales("-1");
                                        aef.newLine();
                                        aef.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                                        aef.CODPAIS = cmbPais.SelectedValue;
                                        aef.CODIGOADUANA = cmbAduana.SelectedValue;
                                        aef.SERIE = txtSerie.Text;
                                        aef.FECHA = DateTime.Now.ToString();
                                        aef.CODRAZONANULACION = cmbRazonAnulacion.SelectedValue;
                                        aef.OBSERVACION = txtObservacion.Text;
                                        aef.IDUSUARIO = Session["IdUsuario"].ToString();
                                        aef.IDMOVIMIENTO = mef.IDMOVIMIENTO;
                                        if (cmbRazonAnulacion.SelectedValue=="7")
                                        {

                                          aef.CARGA = "1";
                                        }
                                        else
                                        {
                                           aef.CARGA = "0";
                                        }
                                       
                                        aef.commitLine();
                                        aef.actualizar();

                                        //cambiar a estado eliminado el rango anterior
                                        mef.CODESTADO = "100";
                                        mef.actualizar();
                                    }
                                    registrarMensaje("Serie " + txtSerie.Text + " anulada exitosamente");
                                    limpiarControles();
                                    rgRangoEspeciesFiscales.Rebind();
                                }
                                else
                                {
                                    efi.loadSerieEFIEnUso(txtSerie.Text, cmbEspecieFiscal.SelectedValue);
                                    if (efi.totalRegistros > 0)
                                    {
                                        if (cmbEspecieFiscal.SelectedValue == "DVA" || cmbEspecieFiscal.SelectedValue == "F")
                                        {
                                            //verificar que hayan especies fiscales disponibles para asignar a la instrucción en mef
                                            mef.loadRangoInicialEFXAduanaClienteNew(cmbAduana.SelectedValue, cmbEspecieFiscal.SelectedValue, efi.TABLA.Rows[0]["IdCliente"].ToString());
                                            if (mef.totalRegistros > 0)
                                            {
                                                string item = mef.ITEM;
                                                if ((mef.CANTIDAD - 1) > 0)
                                                {
                                                    //guardar nuevo registro con las series que quedan
                                                    mefe.loadMovimientosEspeciesFiscales("-1");
                                                    mefe.newLine();
                                                    mefe.IDMOVIMIENTO = idMovimiento;
                                                    mefe.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                                                    mefe.CODPAIS = cmbPais.SelectedValue;
                                                    mefe.CODIGOADUANA = cmbAduana.SelectedValue;
                                                    mefe.RANGOINICIAL = (int.Parse(mef.RANGOINICIAL) + 1).ToString();
                                                    mefe.RANGOFINAL = mef.RANGOFINAL;
                                                    mefe.CANTIDAD = (int.Parse(mef.RANGOFINAL) - int.Parse(mef.RANGOINICIAL));
                                                    mefe.FECHA = DateTime.Now.ToString();
                                                    mefe.CODTIPOMOVIMIENTO = "2";   //Anulacion
                                                    mefe.CODESTADO = mef.CODESTADO;
                                                    mefe.IDUSUARIO = Session["IdUsuario"].ToString();
                                                    mefe.IDCOMPRA = mef.IDCOMPRA;
                                                    mefe.commitLine();
                                                    mefe.actualizar();
                                                }

                                                //cambiar a estado eliminado el rango anterior
                                                mef.loadMovimientosEspeciesFiscalesItem(item);
                                                mef.CODESTADO = "100";
                                                mef.actualizar();

                                                //guardar registro con la serie anulada
                                                aef.loadAnulacionesEspeciesFiscales("-1");
                                                aef.newLine();
                                                aef.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                                                aef.CODPAIS = cmbPais.SelectedValue;
                                                aef.CODIGOADUANA = cmbAduana.SelectedValue;
                                                aef.SERIE = txtSerie.Text;
                                                aef.FECHA = DateTime.Now.ToString();
                                                aef.CODRAZONANULACION = cmbRazonAnulacion.SelectedValue;
                                                aef.OBSERVACION = txtObservacion.Text;
                                                aef.IDUSUARIO = Session["IdUsuario"].ToString();
                                                aef.IDMOVIMIENTO = efi.IDMOVIMIENTO;
                                                aef.CARGA = "0";
                                                aef.commitLine();
                                                aef.actualizar();

                                                //registra la nueva serie a la instrucción
                                                efi2.loadEspeciesFiscalesInstruccionesXInstruccion("-1");
                                                efi2.newLine();
                                                efi2.IDINSTRUCCION = efi.IDINSTRUCCION;
                                                efi2.CODESPECIEFISCAL = efi.CODESPECIEFISCAL;
                                                efi2.NECESARIO = "X";
                                                efi2.SERIE = mef.RANGOINICIAL;
                                                efi2.OBSERVACION = txtObservacion.Text;
                                                efi2.FECHA = DateTime.Now.ToString();
                                                efi2.ELIMINADO = "0";
                                                efi2.IDUSUARIO = Session["IdUsuario"].ToString();
                                                efi2.IDMOVIMIENTO = mef.IDMOVIMIENTO;
                                                efi2.commitLine();
                                                efi2.actualizar();

                                                efi.loadEspeciesFiscalesInstruccionesXItem(efi.ITEM);
                                                efi.NECESARIO = "";
                                                efi.ELIMINADO = "1";
                                                efi.actualizar();

                                                try
                                                {
                                                    if (cmbEspecieFiscal.SelectedValue == "DVA" || cmbEspecieFiscal.SelectedValue == "F")
                                                    {
                                                        efc.loadEspeciesFiscalesClientesXIdMov(mef.IDMOVIMIENTO);
                                                        if (efc.totalRegistros > 0)
                                                        {
                                                            string idCliente = efc.IDCLIENTE;
                                                            //guardar nuevo registro con las series que se envian al cliente
                                                            efc.loadEspeciesFiscalesClientesXIdMov("-1");
                                                            efc.newLine();
                                                            efc.IDMOVIMIENTO = idMovimiento;
                                                            efc.IDCLIENTE = idCliente;
                                                            efc.OBSERVACION = "";
                                                            efc.commitLine();
                                                            efc.actualizar();
                                                        }
                                                    }
                                                }
                                                catch { }

                                                registrarMensaje("Serie " + txtSerie.Text + " anulada exitosamente de la instrucción " + efi.IDINSTRUCCION + ", se le asignó la seri No. " + mef.RANGOINICIAL);
                                                limpiarControles();
                                                rgRangoEspeciesFiscales.Rebind();
                                            }
                                            else
                                                registrarMensaje("Serie no se puede anular, no existe ninguna " + cmbEspecieFiscal.Text + " disponible para asignar a esta instrucción");
                                        }
                                        else
                                        {
                                            //verificar que hayan especies fiscales disponibles para asignar a la instrucción en mef
                                            mef.loadRangoInicialEFXAduana(cmbAduana.SelectedValue, cmbEspecieFiscal.SelectedValue);
                                            if (mef.totalRegistros > 0)
                                            {
                                                if ((mef.CANTIDAD - 1) > 0)
                                                {
                                                    //guardar nuevo registro con las series que quedan
                                                    mefe.loadMovimientosEspeciesFiscales("-1");
                                                    mefe.newLine();
                                                    mefe.IDMOVIMIENTO = idMovimiento;
                                                    mefe.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                                                    mefe.CODPAIS = cmbPais.SelectedValue;
                                                    mefe.CODIGOADUANA = cmbAduana.SelectedValue;
                                                    mefe.RANGOINICIAL = (int.Parse(mef.RANGOINICIAL) + 1).ToString();
                                                    mefe.RANGOFINAL = mef.RANGOFINAL;
                                                    mefe.CANTIDAD = (int.Parse(mef.RANGOFINAL) - int.Parse(mef.RANGOINICIAL));
                                                    mefe.FECHA = DateTime.Now.ToString();
                                                    mefe.CODTIPOMOVIMIENTO = "2";   //Anulacion
                                                    mefe.CODESTADO = mef.CODESTADO;
                                                    mefe.IDUSUARIO = Session["IdUsuario"].ToString();
                                                    mefe.IDCOMPRA = mef.IDCOMPRA;
                                                    mefe.commitLine();
                                                    mefe.actualizar();
                                                }

                                                //cambiar a estado eliminado el rango anterior
                                                mef.CODESTADO = "100";
                                                mef.actualizar();

                                                //guardar registro con la serie anulada
                                                aef.loadAnulacionesEspeciesFiscales("-1");
                                                aef.newLine();
                                                aef.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                                                aef.CODPAIS = cmbPais.SelectedValue;
                                                aef.CODIGOADUANA = cmbAduana.SelectedValue;
                                                aef.SERIE = txtSerie.Text;
                                                aef.FECHA = DateTime.Now.ToString();
                                                aef.CODRAZONANULACION = cmbRazonAnulacion.SelectedValue;
                                                aef.OBSERVACION = txtObservacion.Text;
                                                aef.IDUSUARIO = Session["IdUsuario"].ToString();
                                                aef.IDMOVIMIENTO = efi.IDMOVIMIENTO;
                                                aef.CARGA = "0";
                                                aef.commitLine();
                                                aef.actualizar();

                                                //registra la nueva serie a la instrucción
                                                efi2.loadEspeciesFiscalesInstruccionesXInstruccion("-1");
                                                efi2.newLine();
                                                efi2.IDINSTRUCCION = efi.IDINSTRUCCION;
                                                efi2.CODESPECIEFISCAL = efi.CODESPECIEFISCAL;
                                                efi2.NECESARIO = "X";
                                                efi2.SERIE = mef.RANGOINICIAL;
                                                efi2.OBSERVACION = txtObservacion.Text;
                                                efi2.FECHA = DateTime.Now.ToString();
                                                efi2.ELIMINADO = "0";
                                                efi2.IDUSUARIO = Session["IdUsuario"].ToString();
                                                efi2.IDMOVIMIENTO = mef.IDMOVIMIENTO;
                                                efi2.commitLine();
                                                efi2.actualizar();

                                                efi.loadEspeciesFiscalesInstruccionesXItem(efi.ITEM);
                                                efi.NECESARIO = "";
                                                efi.ELIMINADO = "1";
                                                efi.actualizar();

                                                registrarMensaje("Serie " + txtSerie.Text + " anulada exitosamente de la instrucción " + efi.IDINSTRUCCION + ", se le asignó la seri No. " + mef.RANGOINICIAL);
                                                limpiarControles();
                                                rgRangoEspeciesFiscales.Rebind();
                                            }
                                            else
                                                registrarMensaje("Serie no se puede anular, no existe ninguna " + cmbEspecieFiscal.Text + " disponible para asignar a esta instrucción");
                                        }
                                    }
                                    else
                                        registrarMensaje("Serie no se puede anular, no existe en ningún rango de " + cmbEspecieFiscal.Text + " para esta aduana ó no existe en ninguna instruccion en trámite");
                                }
                            }
                            else
                                registrarMensaje("Seleccione una razón de anulación de la serie");
                        }
                        else
                            registrarMensaje("La serie no puede estar vacía");
                    }
                    else
                        registrarMensaje("La especie fiscal no puede estar vacía");
                }
                else
                    registrarMensaje("La aduana origen no puede estar vacía");
            }
            else
                registrarMensaje("País no puede estar vacío");
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "EspeciesFiscales");
        }
        finally
        {
            desconectar();
        }
    }

    protected void btnLimpiar_Click(object sender, ImageClickEventArgs e)
    {
        limpiarControles();
    }

    private void limpiarControles()
    {
        try
        {
            cargarDatosInicioRangos();
            cmbAduana.ClearSelection();
            cmbEspecieFiscal.ClearSelection();
            txtSerie.Text = "";
            txtObservacion.Text = "";
            llenarGridRangoEspeciesFiscales();
        }
        catch { }
    }
    #endregion

    protected void cmbAduana_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        llenarGridRangoEspeciesFiscales();
    }

    protected void cmbEspecieFiscal_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        llenarGridRangoEspeciesFiscales();
    }


    protected void ImageButton1_Command(object sender, CommandEventArgs e)
    {

        RadButton btn = (RadButton)sender;
        String[] args = btn.CommandArgument.Split(',');
        String RangoInicial = args[0];
        String RangoFinal = args[1];
        String Cliente = args[2];
        conectar();
        DateTime fecha = DateTime.Now;
        string idMovimiento = fecha.Year.ToString() + fecha.Month.ToString("00") + fecha.Day.ToString("00") + "." + fecha.Hour.ToString("00") + fecha.Minute.ToString("00") + fecha.Second.ToString("00") + "." + DateTime.Now.Millisecond.ToString("000");
        MovimientosEspeciesFiscalesBO Movimiento = new MovimientosEspeciesFiscalesBO(logApp);
        MovimientosEspeciesFiscalesBO Movimiento_Liberar = new MovimientosEspeciesFiscalesBO(logApp);
        Movimiento.loadMovimientosEspeciesFiscalesVerificarLiberar(RangoInicial, RangoFinal, cmbAduana.SelectedValue, cmbEspecieFiscal.SelectedValue);

        //Elimina Movimiento anterio para desligar del cliente
        if (Movimiento.totalRegistros != 0) {
            Movimiento.CODESTADO = "100";
            Movimiento.CODTIPOMOVIMIENTO = "2"; 
            Movimiento.actualizar();
       //Crea Nuevo un movimeinto
            Movimiento_Liberar.loadMovimientosEspeciesFiscales("-1");
            Movimiento_Liberar.newLine();
            Movimiento_Liberar.IDMOVIMIENTO = idMovimiento;
            Movimiento_Liberar.IDESPECIEFISCAL = Movimiento.IDESPECIEFISCAL;
            Movimiento_Liberar.CODPAIS = Movimiento.CODPAIS;
            Movimiento_Liberar.CODIGOADUANA = Movimiento.CODIGOADUANA;
            Movimiento_Liberar.RANGOINICIAL = Movimiento.RANGOINICIAL;
            Movimiento_Liberar.RANGOFINAL =Movimiento.RANGOFINAL;
            Movimiento_Liberar.CANTIDAD = Movimiento.CANTIDAD;
            Movimiento_Liberar.FECHA = DateTime.Now.ToString();
            Movimiento_Liberar.CODTIPOMOVIMIENTO = "1";   //Usada
            Movimiento_Liberar.CODESTADO = "1";
            Movimiento_Liberar.IDUSUARIO = Session["IdUsuario"].ToString();
            Movimiento_Liberar.IDCOMPRA = Movimiento.IDCOMPRA;
            Movimiento_Liberar.commitLine();
            Movimiento_Liberar.actualizar();

            registrarMensaje("Rango " + RangoInicial + "-" + RangoFinal + "  Ha sido habilitado para asignar a clientes ");
               
        
        }

        desconectar();
        
        this.rgRangoEspeciesFiscales.Rebind();

    }

}