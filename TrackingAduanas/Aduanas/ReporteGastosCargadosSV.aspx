﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteGastosCargadosSV.aspx.cs" Inherits="ReporteGastosCargados" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style1 {
            width: 734px;
        }

        .style4 {
            width: 155px;
        }

        .style5 {
            width: 107px;
        }

        .style6 {
            width: 122px;
        }

        .style8 {
            width: 105px;
        }

        .style9 {
            width: 109px;
        }

        .auto-style1 {
            width: 72px;
        }

        .auto-style2 {
            width: 178px;
        }

        .auto-style3 {
            width: 66px;
        }

        .auto-style5 {
        }

        .auto-style6 {
            width: 178px;
            height: 42px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart" Style="text-align: left">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                //<![CDATA[

                function pnlRequestStarted(ajaxPanel, eventArgs) {
                    try {
                        if ((eventArgs.EventTarget == "<%= btnSalvar.UniqueID %>")) {
                            eventArgs.EnableAjax = false;
                        }
                    }
                    catch (err)
                    { }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                    }
                }
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }

            </script>
        </telerik:RadScriptBlock>
        <table border="1" style="width: 55%">
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="Label1" runat="server" Text="Fecha Inicial:"></asp:Label>
                </td>
                <td class="auto-style2">
                    <telerik:RadDatePicker ID="rdpFechaInicio" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td class="auto-style3">
                    <asp:Label ID="Label2" runat="server" Text="Fecha Final:"></asp:Label>
                </td>
                <td class="auto-style2">
                    <telerik:RadDatePicker ID="rdpFechaFinal" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td align="center" class="auto-style5" rowspan="2">
                    <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Images/24/view_24.png" OnClick="btnBuscar_Click" />
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="lblPais" runat="server" Text="Pais:"></asp:Label>
                </td>
                <td class="auto-style2">
                    <telerik:RadComboBox ID="cmbPais" runat="server" DataTextField="Descripcion" DataValueField="Codigo">
                    </telerik:RadComboBox>
                </td>
                <td class="auto-style3">&nbsp;</td>
                <td class="auto-style6">&nbsp;</td>
            </tr>
        </table>
        <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0">
            <telerik:RadPageView ID="Compra" runat="server" Style="margin-top: 0px" Width="100%">
                <telerik:RadGrid ID="rgGatos" runat="server" AllowFilteringByColumn="True" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" Culture="es-ES" GridLines="None" OnItemCommand="rgCompras_ItemCommand" OnNeedDataSource="rgCompras_NeedDataSource" PageSize="20" Width="100%">
                    <ClientSettings AllowColumnsReorder="True">
                        <Selecting AllowRowSelect="True" />
                        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                    </ClientSettings>
                    <ExportSettings FileName="" HideStructureColumns="true" OpenInNewWindow="True">
                        <Excel Format="Biff" />
                    </ExportSettings>
                    <AlternatingItemStyle BackColor="White" BorderColor="Black" BorderStyle="Solid" HorizontalAlign="Left" VerticalAlign="Middle" />
                    <MasterTableView CommandItemDisplay="Top">
                        <CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="false" ShowExportToExcelButton="true" />
                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridBoundColumn DataField="IdInstruccion" FilterControlAltText="Filter IdInstruccion column" HeaderText="HOJA DE RUTA" UniqueName="IdInstruccion" FilterControlWidth=" 80%">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="NumeroFactura" FilterControlAltText="Filter NumeroFactura column" HeaderText="CORRELATIVO" UniqueName="NumeroFactura" FilterControlWidth=" 80%">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Nombre" FilterControlAltText="Filter Nombre column" HeaderText="CLIENTE" UniqueName="Nombre" FilterControlWidth=" 80%">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Balance" FilterControlAltText="Filter Balance column" HeaderText="CUENTA" UniqueName="Balance" FilterControlWidth=" 80%">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Cuenta" FilterControlAltText="Filter Cuenta column" HeaderText="MATERIAL" UniqueName="Cuenta" FilterControlWidth=" 80%">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Monto" FilterControlAltText="Filter Monto column" HeaderText="COSTO" UniqueName="Monto" FilterControlWidth=" 80%">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="IVA" FilterControlAltText="Filter IVA column" HeaderText="IVA" UniqueName="IVA" FilterControlWidth=" 80%">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Total" FilterControlAltText="Filter Total column" HeaderText="TOTAL" UniqueName="Total" FilterControlWidth=" 80%">
                            </telerik:GridBoundColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                            </EditColumn>
                        </EditFormSettings>
                    </MasterTableView>
                    <HeaderStyle BackColor="Yellow" BorderStyle="Solid" Font-Bold="True" ForeColor="Black" HorizontalAlign="left" VerticalAlign="Middle" />
                    <ItemStyle BackColor="White" BorderColor="Black" BorderStyle="Solid" HorizontalAlign="Left" VerticalAlign="Middle" />
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                </telerik:RadGrid>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
        <table width="100%">
            <tr align="center">
                <td align="center">
                    <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="~/Images/gris.png" />
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
    <%--<GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>--%>
</asp:Content>
