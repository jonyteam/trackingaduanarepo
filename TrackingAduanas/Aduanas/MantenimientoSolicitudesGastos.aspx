﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MantenimientoSolicitudesGastos.aspx.cs"
    Inherits="MantenimientoUsuarios" MasterPageFile="~/Site.master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .RadGrid_Default {
            font: 12px/16px "segoe ui",arial,sans-serif;
        }

        .RadGrid_Default {
            border: 1px solid #828282;
            background: #fff;
            color: #333;
        }

            .RadGrid_Default .rgHeaderDiv {
                background: #eee 0 -7550px repeat-x url('mvwres://Telerik.Web.UI, Version=2012.3.1016.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif');
            }

            .RadGrid_Default .rgMasterTable {
                font: 12px/16px "segoe ui",arial,sans-serif;
            }

        .RadGrid .rgMasterTable {
            border-collapse: separate;
            border-spacing: 0;
        }

        .RadGrid .rgClipCells .rgHeader {
            overflow: hidden;
        }

        .RadGrid_Default .rgHeader {
            color: #333;
        }

        .RadGrid_Default .rgHeader {
            border: 0;
            border-bottom: 1px solid #828282;
            background: #eaeaea 0 -2300px repeat-x url('mvwres://Telerik.Web.UI, Version=2012.3.1016.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif');
        }

        .RadGrid .rgHeader {
            padding-top: 5px;
            padding-bottom: 4px;
            text-align: left;
            font-weight: normal;
        }

        .RadGrid .rgHeader {
            padding-left: 7px;
            padding-right: 7px;
        }

        .RadGrid .rgHeader {
            cursor: default;
        }

        .RadGrid_Default .rgFilterRow {
            background: #eee;
        }

        .RadGrid .rgFilterRow input {
            vertical-align: middle;
        }

        .RadGrid_Default .rgFilterBox {
            border-color: #8e8e8e #c9c9c9 #c9c9c9 #8e8e8e;
            font-family: "segoe ui",arial,sans-serif;
            color: #333;
        }

        .RadGrid .rgFilterBox {
            border-width: 1px;
            border-style: solid;
            margin: 0;
            height: 15px;
            padding: 2px 1px 3px;
            font-size: 12px;
            vertical-align: middle;
        }

        .RadGrid_Default .rgFilter {
            background-position: 0 -300px;
        }

        .RadGrid_Default .rgFilter {
            background-image: url('mvwres://Telerik.Web.UI, Version=2012.3.1016.35, Culture=neutral, PublicKeyToken=121fae78165ba3d4/Telerik.Web.UI.Skins.Default.Grid.sprite.gif');
        }

        .RadGrid .rgFilter {
            width: 22px;
            height: 22px;
            margin: 0 0 0 2px;
        }

        .RadGrid .rgFilter {
            width: 16px;
            height: 16px;
            border: 0;
            margin: 0;
            padding: 0;
            background-color: transparent;
            background-repeat: no-repeat;
            vertical-align: middle;
            font-size: 1px;
            cursor: pointer;
        }

        .RadGrid_Default .rgFooterDiv {
            background: #eee;
        }

        .RadGrid_Default .rgFooter {
            background: #eee;
        }

        .style1 {
            width: 400px;
        }

        .style2 {
            width: 100px;
        }

        .style3 {
            width: 115px;
            height: 20px;
        }

        .style4 {
            height: 20px;
        }

        .style5 {
            width: 115px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <br />
    <telerik:RadGrid ID="rgGastos" runat="server" AllowFilteringByColumn="True" AllowPaging="True"
        AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" GridLines="None"
        OnNeedDataSource="rgGastos_NeedDataSource" OnItemCommand="rgGastos_ItemCommand"
        OnUpdateCommand="rgGastos_UpdateCommand" OnItemDataBound ="rgGastos_ItemDataBound" PageSize="20" Culture="es-ES">
        <ClientSettings AllowColumnsReorder="True">
            <Selecting AllowRowSelect="True" />
            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
        </ClientSettings>
        <MasterTableView DataKeyNames="HojaRuta, Id,Clientes,Material,Proveedor,MediodePago,Monto,IVA,ValorNegociado">
            <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
            <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                <HeaderStyle Width="20px"></HeaderStyle>
            </RowIndicatorColumn>
            <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                <HeaderStyle Width="20px"></HeaderStyle>
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridEditCommandColumn FilterControlAltText="Filter EditCommandColumn column"
                    AutoPostBackOnFilter="True" ButtonType="ImageButton">
                </telerik:GridEditCommandColumn>
                <telerik:GridButtonColumn FilterControlAltText="Filter column column" UniqueName="Delete" ButtonType="ImageButton" CommandName="Delete" ConfirmDialogType="RadWindow" ConfirmText="¿Esta seguro que desea eliminar este Gasto?" ConfirmTitle="Eliminar Gasto" Text="Eliminar">
                </telerik:GridButtonColumn>
                <telerik:GridBoundColumn DataField="HojaRuta" FilterControlAltText="Filter HojaRuta column"
                    HeaderText="Hoja de Ruta" UniqueName="HojaRuta">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Clientes" FilterControlAltText="Filter Clientes column"
                    HeaderText="Clientes" UniqueName="Clientes">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Material" FilterControlAltText="Filter Material column"
                    HeaderText="Material" UniqueName="Material">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Proveedor" FilterControlAltText="Filter Proveedor column"
                    HeaderText="Proveedor" UniqueName="Proveedor">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MediodePago" FilterControlAltText="Filter MediodePago column"
                    HeaderText="Medio de Pago" UniqueName="MediodePago">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Monto" FilterControlAltText="Filter Monto column"
                    HeaderText="Monto" UniqueName="Monto">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="IVA" FilterControlAltText="Filter IVA column"
                    HeaderText="IVA" UniqueName="IVA">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ValorNegociado" FilterControlAltText="Filter ValorNegociado column"
                    HeaderText="Valor Negociado" UniqueName="ValorNegociado">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Id" FilterControlAltText="Filter Id column" HeaderText="Id"
                    UniqueName="Id">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="IdGasto" FilterControlAltText="Filter IdGasto column"
                    HeaderText="IdGasto" UniqueName="IdGasto">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Cuenta" FilterControlAltText="Filter IdProveedor column"
                    HeaderText="IdProveedor" UniqueName="IdProveedor">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="MedioPago" FilterControlAltText="Filter MedioPago column"
                    HeaderText="MedioPago" UniqueName="MedioPago">
                </telerik:GridBoundColumn>
            </Columns>
            <EditFormSettings EditFormType="Template">
                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                </EditColumn>
                <FormTemplate>
                    <asp:Label ID="Label7" runat="server" Text='<%# ((Container is GridEditFormInsertItem) ? "Agregar Nuevo Registro" : "Editar Gasto Existente") %>'
                        Font-Size="Medium" Font-Bold="true" />
                    <table style="width: 100%">
                        <table class="style1">
                            <tr>
                                <td class="style5"></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Material:"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="edMaterial" runat="server" Width="400px" DataTextField="Gasto"
                                        Filter="Contains" DataValueField="IdGasto" DataSource='<%# GetGasto() %>' SelectedValue='<%# Bind("IdGasto") %>'>
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Medio de Pago:"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="edMedioPago" runat="server" Width="400px" DataTextField="Descripcion"
                                        DataValueField="Codigo" DataSource='<%# GetMedioPago() %>' SelectedValue='<%# Bind("MedioPago") %>'>
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style3">
                                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="Proveedor:"></asp:Label>
                                </td>
                                <td class="style4">
                                    <telerik:RadComboBox ID="edProveedor" runat="server" Width="400px" DataTextField="Nombre"
                                        Filter="Contains" DataValueField="Cuenta" DataSource='<%# GetProveedor() %>'
                                        SelectedValue='<%# Bind("Cuenta") %>'>
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    <asp:Label ID="Label4" runat="server" Font-Bold="True" Text="Monto:"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadNumericTextBox ID="edMonto" runat="server" Width="400px" Text='<%# Bind("Monto") %>'>
                                    </telerik:RadNumericTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    <asp:Label ID="Label5" runat="server" Font-Bold="True" Text="IVA:"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadNumericTextBox ID="edIVA" runat="server" Width="400px" Text='<%# Bind("IVA") %>'>
                                    </telerik:RadNumericTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style5">
                                    <asp:Label ID="Label6" runat="server" Font-Bold="True" Text="Valor Negociado:"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadNumericTextBox ID="edValorNegociado" runat="server" Width="400px" Text='<%# Bind("ValorNegociado") %>'>
                                    </telerik:RadNumericTextBox>
                                </td>
                            </tr>
                        </table>
                        <asp:ImageButton ID="btnSave" runat="server"
                            AlternateText='<%# (Container is GridEditFormInsertItem) ? "Agregar Usuario" : "Actualizar Gasto" %>'
                            CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                            ImageUrl="Images/16/check2_16.png" />
                        &nbsp;
                        <asp:ImageButton ID="btnCancel" runat="server" AlternateText="Cancelar"
                            CausesValidation="false" CommandName="Cancel"
                            ImageUrl="Images/16/delete2_16.png" />
                </FormTemplate>
            </EditFormSettings>
        </MasterTableView>
        <FilterMenu EnableImageSprites="False">
        </FilterMenu>
    </telerik:RadGrid>
</asp:Content>
