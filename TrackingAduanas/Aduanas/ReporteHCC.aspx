﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteHCC.aspx.cs" Inherits="GenerarArchivoSAPDinant" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                    //<![CDATA[
                    
                function pnlRequestStarted(ajaxPanel, eventArgs) {
                    try {
                        if ((eventArgs.EventTarget == "<%= btnSalvar.UniqueID %>")) {
                            eventArgs.EnableAjax = false;
                        }
                    }
                    catch (err)
                { }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                    }
                }
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }
                   
            </script>
        </telerik:RadScriptBlock>
        <table border="1" style="width: 55%">
            <tr>
                <td style="width: 15%">
                    <asp:Label ID="lblFechaInicio" runat="server" Text="Fecha Inicio:" ForeColor="Black"></asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadDatePicker ID="dpFechaInicio" runat="server" EnableTyping="False" Width="95%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="dpFechaInicio"
                        ErrorMessage="Ingrese Fecha" ForeColor="Red" />
                </td>
                <td style="width: 15%">
                    <asp:Label ID="lblFechaFinal" runat="server" Text="Fecha Final:" ForeColor="Black"></asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadDatePicker ID="dpFechaFinal" runat="server" EnableTyping="False" Width="95%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator runat="server" ID="reqName" ControlToValidate="dpFechaFinal"
                        ErrorMessage="Ingrese Fecha" ForeColor="Red" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center" style="text-align: right">
                    <input id="iTipoFlete" runat="server" type="hidden" />
                    <telerik:RadComboBox ID="cmbCliente" Runat="server" style="text-align: left">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="Cargill-Alcon" 
                                
                                Value="8000423 and A.Division = 2 and A.CodRegimen in (4000,5100,5600,4600,6010)" />
                            <telerik:RadComboBoxItem runat="server" Text="Cargill-No Alcon" 
                                Value="8000423 and A.Division != 2 and A.CodRegimen in (4000,4600)" />
                            <telerik:RadComboBoxItem runat="server" Text="Perry" Value="1000102" />
                            <telerik:RadComboBoxItem runat="server" Text="Agribrands" Value="8400911" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
                <td align="center" colspan="2">
                    <asp:ImageButton ID="btnBuscar" runat="server" 
                        ImageUrl="~/Images/24/view_24.png" OnClick="btnBuscar_Click" />
                </td>
            </tr>
        </table>
        <br />
        <telerik:RadGrid ID="RadGrid1" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" 
            GridLines="None" Width="8000px" onitemcommand="RadGrid1_ItemCommand" 
            onneeddatasource="RadGrid1_NeedDataSource" PageSize="20" 
            BorderStyle="Solid" 
            onexcelexportcellformatting="RadGrid1_ExcelExportCellFormatting" 
            onitemdatabound="RadGrid1_ItemDataBound" >
            <ClientSettings AllowColumnsReorder="True">
                <Selecting AllowRowSelect="True" />
            </ClientSettings>
             <ExportSettings 
                FileName ="Reporte" OpenInNewWindow="True">  
                 <Excel 
                     AutoFitImages="True" />  </ExportSettings>
            <MasterTableView CommandItemDisplay="Top">
           <%-- <CommandItemTemplate>
            <asp:Image ID = "prueba" runat="server" ImageUrl ="~/Images/Prueba.jpg"
            AlternateText="Sushi Bar"
                    Width="100%"></asp:Image>
            </CommandItemTemplate>--%>
                <CommandItemSettings ShowExportToExcelButton="true" ExportToPdfText="Export to PDF" ShowAddNewRecordButton = "false" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                    Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                    Visible="True">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="Descripcion" 
                        FilterControlAltText="Filter IdInstruccion column" HeaderText="Tipo Importacion" 
                        UniqueName="IdInstruccion">
                        <FooterStyle BackColor="#0099FF" />
                        <HeaderStyle BackColor="#0099FF" BorderStyle="Groove" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ProductosCargill" 
                        FilterControlAltText="Filter ProductosCargill column" HeaderText="Productos" 
                        UniqueName="ProductosCargill">
                        <FooterStyle BackColor="#0099FF" />
                        <HeaderStyle BackColor="#0099FF" BorderStyle="Groove" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Nombre" 
                        FilterControlAltText="Filter Proveedor column" HeaderText="Planta" 
                        UniqueName="Proveedor">
                        <HeaderStyle BackColor="#0099FF" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IdInstruccion" 
                        FilterControlAltText="Filter Nombre column" HeaderText="Numero" 
                        UniqueName="Nombre">
                        <HeaderStyle BackColor="#0099FF" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FechaRegistro" 
                        FilterControlAltText="Filter Fecha column" HeaderText="Fecha Registro" 
                        UniqueName="Fecha">
                        <HeaderStyle BackColor="#0099FF" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FacturaVESTA" 
                        FilterControlAltText="Filter DocumentoNo column" HeaderText="# Factura VESTA" 
                        UniqueName="FacturaVESTA">
                        <HeaderStyle BackColor="#0099FF" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FechaEntregaCargill" 
                        FilterControlAltText="Filter DocumentoNo column" 
                        HeaderText="Fecha Entrega Cargill" UniqueName="DocumentoNo">
                        <HeaderStyle BackColor="#0099FF" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="#OrdenCompra" 
                        FilterControlAltText="Filter CantidadBultos column" 
                        HeaderText="# Orden Compra" UniqueName="CantidadBultos">
                        <HeaderStyle BackColor="#0099FF" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Proveedor" 
                        FilterControlAltText="Filter PesoKgs column" HeaderText="Proveedor" 
                        UniqueName="PesoKgs">
                        <HeaderStyle BackColor="#0099FF" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Producto" 
                        FilterControlAltText="Filter PesoMiami column" HeaderText="Producto" 
                        UniqueName="PesoMiami">
                        <HeaderStyle BackColor="#0099FF" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PosicionArancelaria" 
                        FilterControlAltText="Filter IngresoSwissport column" 
                        HeaderText="Arancel" UniqueName="IngresoSwissport">
                        <HeaderStyle BackColor="#0099FF" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="%Arancel" 
                        FilterControlAltText="Filter SalidaSwissport column" 
                        HeaderText="% Arancel" UniqueName="SalidaSwissport">
                        <HeaderStyle BackColor="#0099FF" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ValorImpuestoTotal" 
                        FilterControlAltText="Filter Producto column" HeaderText="Valor Impuesto Total" 
                        UniqueName="Producto">
                        <HeaderStyle BackColor="#0099FF" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ETAPLANTA" 
                        FilterControlAltText="Filter GC1.Fecha column" HeaderText="ETA Planta" 
                        UniqueName="GC1.Fecha">
                        <HeaderStyle BackColor="#0099FF" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NumeroFactura" 
                        FilterControlAltText="Filter ArribodeCarga column" HeaderText="No. Factura" 
                        UniqueName="ArribodeCarga">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Incoterm" 
                        FilterControlAltText="Filter ENVIODEPRELIQUIDACION column" 
                        HeaderText="Incoterm" UniqueName="ENVIODEPRELIQUIDACION">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ReferenciaCliente" 
                        FilterControlAltText="Filter APROBACIONDEPRELIQUIDACION column" 
                        HeaderText="# Referencia Proveedor" 
                        UniqueName="APROBACIONDEPRELIQUIDACION">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DUA" 
                        FilterControlAltText="Filter ENVIODEBOLETIN column" 
                        HeaderText="Numero DUA" UniqueName="ENVIODEBOLETIN">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Flete" 
                        FilterControlAltText="Filter PagodeImpuestos column" 
                        HeaderText="Flete" UniqueName="PagodeImpuestos">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Seguro" 
                        FilterControlAltText="Filter Color column" HeaderText="Seguro" 
                        UniqueName="Color">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ValorFactura" 
                        FilterControlAltText="Filter RevisionMercancia column" 
                        HeaderText="Valor Facturado" UniqueName="RevisionMercancia">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PAISORIGEN" 
                        FilterControlAltText="Filter EmisionPaseSalida column" 
                        HeaderText="Pais Origen" UniqueName="EmisionPaseSalida">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Transporte" 
                        FilterControlAltText="Filter EntregadeServicios column" 
                        HeaderText="Tipo Importacion" UniqueName="EntregadeServicios">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IdRegimen" 
                        FilterControlAltText="Filter CodRegimen column" HeaderText="Regimen" 
                        UniqueName="CodRegimen">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EstadoFlujo" 
                        FilterControlAltText="Filter EstadoFlujo column" HeaderText="Status" 
                        UniqueName="EstadoFlujo" SortedBackColor="Aqua">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ColorCargill" 
                        FilterControlAltText="Filter ColorCargill column" HeaderText="Color" Visible ="false" 
                        UniqueName="ColorCargill">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Courier" 
                        FilterControlAltText="Filter IngresoDeposito column" 
                        HeaderText="Naviera, Courier" UniqueName="IngresoDeposito">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DescripcionContenedores" 
                        FilterControlAltText="Filter EntregaCliente column" 
                        HeaderText="# de Contenedor" UniqueName="EntregaCliente">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DocumentoNo" 
                        FilterControlAltText="Filter Aforador column" HeaderText="BL, GUIA o Carta Porte" 
                        UniqueName="Aforador">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PesoLibras" 
                        FilterControlAltText="Filter TiempoProceso column" HeaderText="Peso en Lbs" 
                        UniqueName="TiempoProceso">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CantidadBultos" 
                        FilterControlAltText="Filter TiempoProceso2 column" 
                        HeaderText="Cantidad" UniqueName="TiempoProceso2">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column column" 
                        HeaderText="Presentacion" UniqueName="column" DataField="Presentacion">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column1 column" 
                        HeaderText="# Tracking Del Courier " UniqueName="column1" 
                        DataField="TackingCurrier">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column2 column" 
                        HeaderText="Fecha Recibo Copias" UniqueName="column2" 
                        DataField="Fechaderecibocopiasdocs">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column3 column" 
                        HeaderText="Fecha Recibo Originales" UniqueName="column3" 
                        DataField="Fecharecibooriginales">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column4 column" 
                        HeaderText="Fecha Inicio Tramite Importacion" UniqueName="column4" 
                        DataField="MAGA">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column5 column" 
                        HeaderText="Fecha Extencion Permiso" UniqueName="column5" 
                        DataField="Fechaextendidopermiso">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column6 column" 
                        HeaderText="Numero Permiso" UniqueName="column6" DataField="PermisoNo">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column7 column" 
                        HeaderText="Canal Selectividad" UniqueName="column7" DataField="Color">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column8 column" 
                        HeaderText="Fecha Real Arribo Aduana" UniqueName="column8" 
                        DataField="FechaArriboAduana">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column9 column" 
                        HeaderText="Fecha Arribo Planta Proceso" UniqueName="column9" 
                        DataField="FechaPlantaProceso">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column10 column" 
                        HeaderText="Tiempo Llegada Hasta Entrega" UniqueName="column10" 
                        DataField="TiempoLlegadaHastaEntrega">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column11 column" 
                        HeaderText="Dias Libres Almacenaje" UniqueName="column11" 
                        DataField="DiasLibresAlmacenaje">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column12 column" 
                        HeaderText="Tiempo  de Energia" UniqueName="column12" 
                        DataField="TiempoEnergia">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column13 column" 
                        HeaderText="Termina Tiempo Libre DCS" UniqueName="column13" 
                        DataField="TerminaTiempoLibre">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column14 column" 
                        HeaderText="Cargo Almacenaje DCS" UniqueName="column14" 
                        DataField="CargoAlmacenaje">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column15 column" 
                        HeaderText="Termina tiempo Libre Electricidad" UniqueName="column15" 
                        DataField="TerminaTiempoLibreElectricidad">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column16 column" 
                        HeaderText="Cargo Demora Electricidad" UniqueName="column16" 
                        DataField="CargoDemoraElectricidad">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column17 column" 
                        HeaderText="Fecha Retorno Contenedor" UniqueName="column17" 
                        DataField="FechaRetornoContenedor">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column18 column" 
                        HeaderText="Dias Demora" UniqueName="column18" DataField="DiasDemora">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column19 column" 
                        HeaderText="Demora" UniqueName="column19" DataField="Demora">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column20 column" 
                        HeaderText="Notes" UniqueName="column20" DataField="Notes">
                        <HeaderStyle BackColor="#FF9933" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column21 column" 
                        HeaderText="# Declaracion Seguro" UniqueName="column21" 
                        DataField="NumeroDeclaracionSeguro">
                        <HeaderStyle BackColor="Yellow" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column22 column" 
                        HeaderText="Fecha" UniqueName="column22" DataField="Fecha">
                        <HeaderStyle BackColor="Yellow" />
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FechaFactura2" 
                        FilterControlAltText="Filter FechaFactura2 column" 
                        HeaderText="Fechade Factura 2" UniqueName="FechaFactura2">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Color" 
                        FilterControlAltText="Filter Color column" 
                        HeaderText="Selectividad" UniqueName="Selectividad">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <HeaderStyle BorderStyle="Solid" />
            <ItemStyle BorderColor="Black" BorderStyle="Solid" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        <br />
        <br />
        <br />
        <br />
        <table width="100%">
            <tr align="center">
                <td colspan="6" align="center">
                    <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="~/Images/gris.png"/>
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
    <%--<GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>--%>
</asp:Content>
