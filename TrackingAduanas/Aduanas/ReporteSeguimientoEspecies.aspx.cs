﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.OleDb;

using Telerik.Web.UI;
using System.Reflection;
using System.Text;

public partial class GenerarArchivoSAPDinant : Utilidades.PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Seguimiento Especies", "Seguimiento Especies");
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            c.loadCodigosXCategoriaCodigos("ESPECIESFISCALES", "'DA','DVA','F','P','DTI','HN-MI','HN-MN','SS','ZL','SSB'");
            cmbEspecie.Dispose();
            cmbEspecie.ClearSelection();
            cmbEspecie.DataSource = c.TABLA;
            cmbEspecie.DataBind();
            AduanasBO a = new AduanasBO(logApp);
            a.loadAllCampoAduanasReportes("H");
            cmbAduana.Dispose();
            cmbAduana.ClearSelection();
            cmbAduana.DataSource = a.TABLA;
            cmbAduana.DataBind();
        }
        EsconderControles();
    }
    protected void EsconderControles()
    {
        if (TabSeguimientoEspecies.SelectedIndex == 0)
        {
            cmbAduana.Enabled = false;
            cmbAduana.Visible = false;
            Label4.Visible = false;
            cmbEspecie.Visible = true;
            cmbEspecie.Enabled = true;
            Label3.Visible = true;
            rdpFechaInicio.Visible = true;
            rdpFechaFinal.Visible = true;
            rdpFechaInicio.Enabled = true;
            rdpFechaFinal.Enabled = true;
            Label1.Visible = true;
            Label2.Visible = true;
            chbAnuladas.Visible = false;
        }
        else if (TabSeguimientoEspecies.SelectedIndex == 1)
        {
            cmbAduana.Enabled = true;
            cmbAduana.Visible = true;
            Label4.Visible = true;
            cmbEspecie.Visible = false;
            cmbEspecie.Enabled = false;
            Label3.Visible = false;
            Label1.Visible = true;
            Label2.Visible = true;
            rdpFechaInicio.Visible = true;
            rdpFechaFinal.Visible = true;
            rdpFechaInicio.Enabled = true;
            rdpFechaFinal.Enabled = true;
            chbAnuladas.Visible = false;
        }
        else if (TabSeguimientoEspecies.SelectedIndex == 2)
        {
            cmbAduana.Enabled = true;
            cmbAduana.Visible = true;
            Label4.Visible = true;
            cmbEspecie.Visible = false;
            cmbEspecie.Enabled = false;
            Label3.Visible = false;
            Label1.Visible = false;
            Label2.Visible = false;
            rdpFechaInicio.Visible = false;
            rdpFechaFinal.Visible = false;
            rdpFechaInicio.Enabled = false;
            rdpFechaFinal.Enabled = false;
            chbAnuladas.Visible = true;
        }
        else if (TabSeguimientoEspecies.SelectedIndex == 3)
        {
            cmbAduana.Enabled = false;
            cmbAduana.Visible = false;
            Label4.Visible = false;
            cmbEspecie.Visible = false;
            cmbEspecie.Enabled = false;
            Label3.Visible = false;
            Label1.Visible = true;
            Label2.Visible = true;
            rdpFechaInicio.Visible = true;
            rdpFechaFinal.Visible = true;
            rdpFechaInicio.Enabled = true;
            rdpFechaFinal.Enabled = true;
            chbAnuladas.Visible = false;
        }
    }
    protected bool Consultar { get { return tienePermiso("Consultar"); } }
    private void ConfigureExport()
    {
        if (TabSeguimientoEspecies.SelectedIndex == 0)
        {
            String filename = "Reporte_Compras_" + cmbEspecie.Text;
            rgCompras.ExportSettings.FileName = filename;
            rgCompras.ExportSettings.ExportOnlyData = true;
            rgCompras.ExportSettings.IgnorePaging = true;
            rgCompras.ExportSettings.OpenInNewWindow = true;
            rgCompras.MasterTableView.ExportToExcel();
        }
        else if (TabSeguimientoEspecies.SelectedIndex == 1)
        {
            String filenameEnvio = "Reporte_Envios_" + cmbAduana.Text;
            rgEnvio.ExportSettings.FileName = filenameEnvio;
            rgEnvio.ExportSettings.ExportOnlyData = true;
            rgEnvio.ExportSettings.IgnorePaging = true;
            rgEnvio.ExportSettings.OpenInNewWindow = true;
            rgEnvio.MasterTableView.ExportToExcel();
        }
        else if (TabSeguimientoEspecies.SelectedIndex == 2)
        {
            String filenameInventario = "Reporte_Inventario_" + cmbAduana.Text;
            rgInventario.ExportSettings.FileName = filenameInventario;
            rgInventario.ExportSettings.ExportOnlyData = true;
            rgInventario.ExportSettings.IgnorePaging = true;
            rgInventario.ExportSettings.OpenInNewWindow = true;
            rgInventario.MasterTableView.ExportToExcel();
        }
        else if (TabSeguimientoEspecies.SelectedIndex == 3)
        {
            String filenameAnulacion = "Reporte_Anulacion_del " + rdpFechaInicio.SelectedDate.ToString() + " al " + rdpFechaFinal.SelectedDate.ToString(); ;
            rgAnulacion.ExportSettings.FileName = filenameAnulacion;
            rgAnulacion.ExportSettings.ExportOnlyData = true;
            rgAnulacion.ExportSettings.IgnorePaging = true;
            rgAnulacion.ExportSettings.OpenInNewWindow = true;
            rgAnulacion.MasterTableView.ExportToExcel();
        }
    }
    public override bool CanGoBack { get { return false; } }
    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        if (TabSeguimientoEspecies.SelectedIndex == 0)
        {
            llenarGridCompras();
        }
        else if (TabSeguimientoEspecies.SelectedIndex == 1)
        {
            LlenarGridEnvios();
        }
        else if (TabSeguimientoEspecies.SelectedIndex == 2)
        {
            LlenarGridInventario();
        }
        else if (TabSeguimientoEspecies.SelectedIndex == 3)
        {
            LlenarGridAnulacion();
        }
    }

    #region Compra
    private void llenarGridCompras()
    {
        try
        {
                conectar();

                DataTable tabla = new DataTable();
                DataRow fila;

                tabla.Columns.Add("TipoEspecie", typeof(String));
                tabla.Columns.Add("Serie", typeof(String));
                tabla.Columns.Add("CodigoCompra", typeof(String));
                tabla.Columns.Add("Fecha", typeof(String));
                tabla.Columns.Add("Especie", typeof(String));
                tabla.Columns.Add("Nombre", typeof(String));

                ComprasEspeciesFiscalesBO CEF = new ComprasEspeciesFiscalesBO(logApp);
                CEF.ComprasXFecha(rdpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), rdpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"), cmbEspecie.SelectedValue);

                for (int j = 0; j < CEF.totalRegistros; j++)
                {

                    for (int i = int.Parse(CEF.TABLA.Rows[j]["RangoInicial"].ToString()); i <= int.Parse(CEF.TABLA.Rows[j]["RangoFinal"].ToString()); i++)
                    {
                        fila = tabla.NewRow();
                        fila["TipoEspecie"] = CEF.TABLA.Rows[j]["Descripcion"].ToString();
                        fila["Serie"] = i;
                        fila["CodigoCompra"] = CEF.TABLA.Rows[j]["IdCompra"].ToString();
                        fila["Fecha"] = CEF.TABLA.Rows[j]["Fecha"].ToString();
                        fila["Nombre"] = CEF.TABLA.Rows[j]["Nombre"].ToString();
                        tabla.Rows.Add(fila);
                    }
                    CEF.regSiguiente();
                }


                rgCompras.DataSource = tabla;
                rgCompras.DataBind();
        }
        catch
        { }
        finally { desconectar(); }

    }
    protected void rgCompras_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (TabSeguimientoEspecies.SelectedIndex == 0)
        {
            try
            {
                if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName)
                    ConfigureExport();
            }
            catch (Exception)
            { }
        }
    }
    protected void rgCompras_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        if (TabSeguimientoEspecies.SelectedIndex == 0)
        {
            llenarGridCompras();
        }
    }
    #endregion

    #region Envio
    protected void LlenarGridEnvios()
    {
        try
        {
            conectar();
            TrasladosEspeciesFiscalesBO TEF = new TrasladosEspeciesFiscalesBO(logApp);
            TEF.TrasladosXAduanaYFecha(rdpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), rdpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"), cmbAduana.SelectedValue);

            DataTable tabla = new DataTable();
            DataRow fila;

            tabla.Columns.Add("Descripcion", typeof(String));
            tabla.Columns.Add("Origen", typeof(String));
            tabla.Columns.Add("Destino", typeof(String));
            tabla.Columns.Add("Serie", typeof(String));
            tabla.Columns.Add("FechaEnvio", typeof(String));
            tabla.Columns.Add("Nombre", typeof(String));
            tabla.Columns.Add("FechaRecibido", typeof(String));
            tabla.Columns.Add("Observacion", typeof(String));
            tabla.Columns.Add("NombreRecibio", typeof(String));

            for (int j = 0; j < TEF.totalRegistros; j++)
            {

                for (int i = int.Parse(TEF.TABLA.Rows[j]["RangoInicial"].ToString()); i <= int.Parse(TEF.TABLA.Rows[j]["RangoFinal"].ToString()); i++)
                {
                    fila = tabla.NewRow();
                    fila["Descripcion"] = TEF.TABLA.Rows[j]["Descripcion"].ToString();
                    fila["Origen"] = TEF.TABLA.Rows[j]["Origen"].ToString();
                    fila["Destino"] = TEF.TABLA.Rows[j]["Destino"].ToString();
                    fila["Serie"] = i;
                    fila["FechaEnvio"] = TEF.TABLA.Rows[j]["FechaEnvio"].ToString();
                    fila["Nombre"] = TEF.TABLA.Rows[j]["Nombre"].ToString();
                    fila["FechaRecibido"] = TEF.TABLA.Rows[j]["FechaRecibido"].ToString();
                    fila["Observacion"] = TEF.TABLA.Rows[j]["Observacion"].ToString();
                    fila["NombreRecibio"] = TEF.TABLA.Rows[j]["NombreRecibio"].ToString();
                    tabla.Rows.Add(fila);
                }
                TEF.regSiguiente();
            }


            rgEnvio.DataSource = tabla;
            rgEnvio.DataBind();

        }
        catch (Exception)
        { }
        finally { desconectar(); }
    }
    protected void rgEnvio_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (TabSeguimientoEspecies.SelectedIndex == 1)
        {
            try
            {
                if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName)
                    ConfigureExport();
            }
            catch (Exception)
            { }
        }
    }
    protected void rgEnvio_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (TabSeguimientoEspecies.SelectedIndex == 1)
        {
            LlenarGridEnvios();
        }
    }
    #endregion

    #region Inventario
    protected void LlenarGridInventario()
    {
        try
        {
            conectar();
            MovimientosEspeciesFiscalesBO MEF = new MovimientosEspeciesFiscalesBO(logApp);
            MEF.CargarInventarioXAduana(cmbAduana.SelectedValue,chbAnuladas.Checked);

            DataTable tabla = new DataTable();
            DataRow fila;

            tabla.Columns.Add("Descripcion", typeof(String));
            tabla.Columns.Add("NombreAduana", typeof(String));
            tabla.Columns.Add("Serie", typeof(String));
            tabla.Columns.Add("Fecha", typeof(String));
            tabla.Columns.Add("Nombre", typeof(String));
            tabla.Columns.Add("Cliente", typeof(String));
            tabla.Columns.Add("IdCompra", typeof(String));
            tabla.Columns.Add("FechaReporte", typeof(String));

            for (int j = 0; j < MEF.totalRegistros; j++)
            {

                for (int i = int.Parse(MEF.TABLA.Rows[j]["RangoInicial"].ToString()); i <= int.Parse(MEF.TABLA.Rows[j]["RangoFinal"].ToString()); i++)
                {
                    fila = tabla.NewRow();
                    fila["Descripcion"] = MEF.TABLA.Rows[j]["Descripcion"].ToString();
                    fila["NombreAduana"] = MEF.TABLA.Rows[j]["NombreAduana"].ToString();
                    fila["Serie"] = i;
                    fila["Fecha"] = MEF.TABLA.Rows[j]["Fecha"].ToString();
                    fila["Nombre"] = MEF.TABLA.Rows[j]["Nombre"].ToString();
                    fila["Cliente"] = MEF.TABLA.Rows[j]["Cliente"].ToString();
                    fila["IdCompra"] = MEF.TABLA.Rows[j]["IdCompra"].ToString();
                    fila["FechaReporte"] = MEF.TABLA.Rows[j]["FechaReporte"].ToString();
                    tabla.Rows.Add(fila);
                }
                MEF.regSiguiente();
            }
            rgInventario.DataSource = tabla;
            rgInventario.DataBind();

        }
        catch (Exception)
        { }
        finally { desconectar(); }
    }
    protected void rgInventario_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (TabSeguimientoEspecies.SelectedIndex == 2)
        {
            try
            {
                if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName)
                    ConfigureExport();
            }
            catch (Exception)
            { }
        }
    }
    protected void rgInventario_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (TabSeguimientoEspecies.SelectedIndex == 2)
        {
            LlenarGridInventario();
        }
    }
    #endregion

    #region Anulacion
    protected void LlenarGridAnulacion()
    {
        try
        {
            conectar();
            AnulacionesEspeciesFiscalesBO AEF = new AnulacionesEspeciesFiscalesBO(logApp);
            AEF.LoadAnulacionesXFecha(rdpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), rdpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"));
            rgAnulacion.DataSource = AEF.TABLA;
            rgAnulacion.DataBind();

        }
        catch (Exception)
        { }
        finally { desconectar(); }
    }
    protected void rgAnulacion_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (TabSeguimientoEspecies.SelectedIndex == 3)
        {
            try
            {
                if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName)
                    ConfigureExport();
            }
            catch (Exception)
            { }
        }
    }
    protected void rgAnulacion_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (TabSeguimientoEspecies.SelectedIndex == 3)
        {
            LlenarGridAnulacion();
        }
    }
    #endregion

    
}

