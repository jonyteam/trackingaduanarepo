﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ConfirmarSolicitudPagos.aspx.cs" Inherits="GenerarArchivoSAPDinant" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style5
        {
            width: 400px;
        }
        .style9
        {
            width: 306px;
        }
        .style11
        {
            width: 136px;
        }
        .style12
        {
            width: 100%;
            height: 50px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                    //<![CDATA[

                function pnlRequestStarted(ajaxPanel, eventArgs) {
                    try {
                        if ((eventArgs.EventTarget == "<%= btnSalvar.UniqueID %>")) {
                            eventArgs.EnableAjax = false;
                        }
                    }
                    catch (err)
                { }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                    }
                }
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }
                   
            </script>
        </telerik:RadScriptBlock>
        <input id="cmbMaterial" runat="server" type="hidden" />
        <input id="cmbMedioPago" runat="server" type="hidden" />
        <input id="cmbPagarA" runat="server" type="hidden" />
        <input id="txtMonto" runat="server" type="hidden" />
        <input id="txtIVA" runat="server" type="hidden" />
        <input id="txtMontoPagar" runat="server" type="hidden" />
        <input id="cmbMoneda" runat="server" type="hidden" />
        <input id="edFecha" runat="server" type="hidden" />
        <input id="edHora" runat="server" type="hidden" />
        <br />
        <telerik:RadGrid ID="rgGastos" runat="server" AllowPaging="True" AllowSorting="True"
            AutoGenerateColumns="False" BorderStyle="Solid" CellSpacing="0" GridLines="None"
            OnItemCommand="RadGrid1_ItemCommand" OnNeedDataSource="RadGrid1_NeedDataSource"
            PageSize="20" Style="margin-right: 0px" Width="1500px" OnItemDataBound="RadGrid1_ItemDataBound"
            OnUpdateCommand="RadGrid1_UpdateCommand" AllowFilteringByColumn="True" OnDeleteCommand="rgGastos_DeleteCommand" Culture="es-ES" >
            <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                <Selecting AllowRowSelect="True" />
                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
            </ClientSettings>
            <ExportSettings FileName="Reporte" HideStructureColumns="true" OpenInNewWindow="True">
                <Excel AutoFitImages="True" Format="Biff" />
            </ExportSettings>
            <MasterTableView CommandItemDisplay="Top" DataKeyNames="HojaRuta,Id,IdGasto">
                <CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="false"
                    ShowExportToExcelButton="true" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridEditCommandColumn AutoPostBackOnFilter="True" CancelText="Cancelar"
                        EditText="Editar" InsertText="Insertar" UpdateText="Grabar" ButtonType="ImageButton"
                        HeaderText="Editar">
                        <HeaderStyle Width="30px" />
                        <ItemStyle Width="5%" />
                    </telerik:GridEditCommandColumn>
                    <telerik:GridButtonColumn ConfirmText="¿Esta seguro de que desea eliminar este gasto?"
                        ConfirmDialogType="RadWindow" ConfirmTitle="Eliminar Gasto" UniqueName="DeleteColumn"
                        ButtonType="ImageButton" CommandName="Delete" HeaderText="Eliminar">
                        <HeaderStyle Width="40px" Wrap="True" />
                        <ItemStyle Width="5%" />
                    </telerik:GridButtonColumn>
                    <telerik:GridBoundColumn DataField="HojaRuta" FilterControlAltText="Filter IdInstruccion column"
                        HeaderText="Hoja de Ruta" UniqueName="HojaRuta" FilterControlWidth="75px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Material" FilterControlAltText="Filter Material column"
                        HeaderText="Material" UniqueName="Material" FilterControlWidth="75px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IdGasto" FilterControlAltText="Filter IdGasto column"
                        HeaderText="IdGasto" UniqueName="IdGasto" Visible="False" AllowFiltering="False">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="MedioPago" FilterControlAltText="Filter MedioPago column"
                        HeaderText="Medio de Pago" UniqueName="MedioPago" FilterControlWidth="75px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Proveedor" FilterControlAltText="Filter Proveedor column"
                        HeaderText="Pagar A" UniqueName="Proveedor" FilterControlWidth="75px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Monto" FilterControlAltText="Filter Monto column"
                        HeaderText="Monto" DataFormatString="{0:#,###.00}" FooterAggregateFormatString="{0:#,###,###.00}"
                        UniqueName="Monto" AllowFiltering="False">
                        <HeaderStyle HorizontalAlign="Center" Width="40px" />
                        <ItemStyle HorizontalAlign="Center" Width="10%" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IVA" FilterControlAltText="Filter IVA column"
                        HeaderText="IVA" UniqueName="IVA" AllowFiltering="False"
                        DataFormatString="{0:#,###.00}" FooterAggregateFormatString="{0:#,###,###.00}">
                        <HeaderStyle Width="40px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Total" FilterControlAltText="Filter Total column"
                        HeaderText="Valor Negociado" UniqueName="Total" AllowFiltering="False"
                        DataFormatString="{0:#,###.00}" FooterAggregateFormatString="{0:#,###,###.00}">
                        <HeaderStyle Width="50px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Moneda" FilterControlAltText="Filter Moneda column"
                        HeaderText="Moneda" UniqueName="Moneda" AllowFiltering="False">
                        <HeaderStyle Width="50px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" UniqueName="TemplateColumn"
                        AllowFiltering="False" Display="False">
                        <ItemTemplate>
                            <telerik:RadDatePicker ID="dtFechaPago" runat="server" Culture="es-HN" EnableTyping="False"
                                Width="120px">
                                <Calendar ID="Calendar1" runat="server" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False"
                                    ViewSelectorText="x">
                                </Calendar>
                                <DateInput ID="DateInput1" runat="server" DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"
                                    ReadOnly="True">
                                </DateInput>
                                <DatePopupButton HoverImageUrl="" ImageUrl="" />
                            </telerik:RadDatePicker>
                            <telerik:RadTimePicker ID="dtHoraPago" runat="server" Culture="es-HN" EnableTyping="True"
                                Width="100px">
                                <TimeView ID="TimeView2" runat="server" CellSpacing="-1" Columns="4" Culture="es-HN"
                                    Interval="00:30:00" TimeFormat="T">
                                </TimeView>
                                <TimePopupButton HoverImageUrl="" ImageUrl="" />
                            </telerik:RadTimePicker>
                        </ItemTemplate>
                        <HeaderStyle Width="190px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn1 column"
                        UniqueName="TemplateColumn1" AllowFiltering="False">
                        <ItemTemplate>
                            <asp:CheckBox ID="edchbaceptar" runat="server" Text="Aprobada" />
                        </ItemTemplate>
                        <HeaderStyle Width="80px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="Id" FilterControlAltText="Filter Id column" UniqueName="Id"
                        HeaderText="Id Solicitud" AllowFiltering="False">
                        <HeaderStyle Width="60px" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings EditFormType="Template">
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                    <FormTemplate>
                        <table class="style5">
                            <tr>
                                <td colspan="2">
                                </td>
                            </tr>
                            <tr>
                                <td class="style11">
                                    Hoja de Ruta:
                                </td>
                                <td class="style9">
                                    <asp:Label ID="IdHojaRuta" runat="server" Text='<%# Bind("HojaRuta") %>'></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="style11">
                                    Material:
                                </td>
                                <td class="style9">
                                    <telerik:RadComboBox ID="edMaterial" DataSource='<%# GetMateriales() %>' DataValueField="IdGasto"
                                        DataTextField="Gasto" SelectedValue='<%# Bind("IdGasto") %>' runat="server" Culture="es-ES">
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style11">
                                    Medio de Pago:
                                </td>
                                <td class="style9">
                                    <telerik:RadComboBox ID="edMedioPago" DataSource='<%# GetMedioPago() %>' DataValueField="Codigo"
                                        DataTextField="Descripcion" SelectedValue='<%# Bind("Codigo") %>' runat="server">
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style11">
                                    Pagar A:
                                </td>
                                <td class="style9">
                                    <telerik:RadComboBox ID="edPagarA" DataSource='<%# GetProveedores() %>' DataValueField="Cuenta"
                                        DataTextField="Nombre" SelectedValue='<%# Bind("ProveedorCodigo") %>' runat="server">
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style11">
                                    Monto:
                                </td>
                                <td class="style9">
                                    <telerik:RadNumericTextBox ID="edMonto" Text='<%# Bind("Monto") %>' runat="server"
                                        EmptyMessage="Monto">
                                    </telerik:RadNumericTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style11">
                                    IVA:
                                </td>
                                <td class="style9">
                                    <telerik:RadNumericTextBox ID="edIVA" Text='<%# Bind("IVA") %>' runat="server" EmptyMessage="IVA">
                                    </telerik:RadNumericTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style11">
                                    Valor Negociado:
                                </td>
                                <td class="style9">
                                    <telerik:RadNumericTextBox ID="edTotal" Text='<%# Bind("Total") %>' runat="server"
                                        EmptyMessage="Total">
                                    </telerik:RadNumericTextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style11">
                                    Moneda:
                                </td>
                                <td class="style9">
                                    <telerik:RadComboBox ID="edMoneda" DataSource='<%# GetMoneda() %>' DataValueField="Codigo"
                                        DataTextField="Descripcion" runat="server">
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="style11">
                                </td>
                                <td class="style9">
                                </td>
                            </tr>
                        </table>
                        <asp:ImageButton ID="btnSave" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                            AlternateText='<%# (Container is GridEditFormInsertItem) ? "Agregar Gasto" : "Actualizar Gasto" %>'
                            ImageUrl='Images/16/check2_16.png' />
                        &nbsp
                        <asp:ImageButton ID="btnCancel" runat="server" CommandName="Cancel" AlternateText="Cancelar"
                            ImageUrl='Images/16/forbidden_16.png' CausesValidation="false" />
                    </FormTemplate>
                </EditFormSettings>
            </MasterTableView>
            <HeaderStyle Width="100px" Wrap="True" />
            <ItemStyle BorderColor="Black" BorderStyle="Solid" />
            <SelectedItemStyle BackColor="#666666" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        <table class="style12">
            <tr>
                <td style="text-align: center">
                    <telerik:RadButton ID="rbtnEnviar" runat="server" Height="28px" OnClick="rbtnEnviar_Click"
                        Text="Enviar" Width="94px">
                    </telerik:RadButton>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        </telerik:RadWindowManager>
        <br />
        <br />
        <table width="100%">
            <tr align="center">
                <td colspan="6" align="center">
                    <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="~/Images/gris.png" />
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
    <%--<GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>--%>
</asp:Content>
