﻿using GrupoLis.Login;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Telerik.Web.UI;

public partial class MantenimientoUsuarios : Utilidades.PaginaBase
{
    private GrupoLis.Login.Login logAppEsquema;
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Mantenimiento Usuarios";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgUsuarios.FilterMenu);
        SetGridFilterMenu(rgRoles.FilterMenu);
        rgUsuarios.Skin = Skin;
        rgRoles.Skin = Skin;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Mantenimiento Usuarios", "Usuarios");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void SetVisible(TableCellCollection cells, String commandName, bool visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is ImageButton)
                {
                    ImageButton btn = (ImageButton)control;

                    if (btn.CommandName == commandName)
                        btn.Visible = visible;
                }
            }
        }
    }

    #region Usuarios
    private void loadGridUsuarios()
    {
        try
        {
            conectar();
            UsuariosBO bo = new UsuariosBO(logApp);
            bo.loadUsuario(Session["IdUsuario"].ToString());
            if (User.IsInRole("Administradores") & bo.CODPAIS == "H")
                bo.loadUsuarios_Esquema();
            else
                bo.loadUsuariosXPais(bo.CODPAIS);
            rgUsuarios.DataSource = bo.TABLA;
            rgUsuarios.DataBind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected Object GetUsuarios()
    {
        try
        {
            conectar();

            UsuariosBO bo = new UsuariosBO(logApp);
            bo.loadUsuariosNombreCompleto();

            return bo.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }

        return null;
    }

    protected Object GetPaises()
    {
        try
        {
            conectar();
            CodigosBO cod = new CodigosBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (User.IsInRole("Administradores") & u.CODPAIS == "H")
                cod.loadPaisesHojaRuta();
            else
                cod.loadAllCampos("PAISES", u.CODPAIS);
            return cod.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }
        return null;
    }

    protected void rgUsuarios_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        loadGridUsuarios();
    }

    protected bool GetDatosEditados(UsuariosBO bo, UsuariosBO u, GridItem item, string oldUsuario)
    {
        bool resp = false;
        GridEditableItem editedItem = item as GridEditableItem;
        RadTextBox usuario = (RadTextBox)editedItem.FindControl("edUsuario");
        RadTextBox nombre = (RadTextBox)editedItem.FindControl("edNombre");
        RadTextBox apellido = (RadTextBox)editedItem.FindControl("edApellido");
        RadTextBox identidad = (RadTextBox)editedItem.FindControl("edIdentidad");
        RadTextBox email = (RadTextBox)editedItem.FindControl("edEmail");
        RadNumericTextBox celular = (RadNumericTextBox)editedItem.FindControl("edCelular");
        RadNumericTextBox telefono = (RadNumericTextBox)editedItem.FindControl("edTelefono");
        RadNumericTextBox extension = (RadNumericTextBox)editedItem.FindControl("edExtension");
        RadComboBox pais = (RadComboBox)editedItem.FindControl("edPais");
        RadComboBox aduana = (RadComboBox)editedItem.FindControl("edAduana");

        u.loadUsuarioLogin(usuario.Text.Trim());
        if (u.totalRegistros <= 0)
        {
            bo.USUARIO = usuario.Text.Trim();
            bo.NOMBRE = nombre.Text.Trim();
            bo.APELLIDO = apellido.Text.Trim();
            bo.IDENTIDAD = identidad.Text.Trim();
            bo.EMAIL = email.Text.Trim();
            bo.CELULAR = celular.Text.Trim();
            bo.TELEFONO = telefono.Text.Trim();
            bo.EXTENSION = extension.Text.Trim();
            bo.CODPAIS = pais.SelectedValue.Trim();
            bo.CODIGOADUANA = aduana.SelectedValue;
            resp = true;
        }
        else if (oldUsuario == usuario.Text.Trim())
        {
            bo.USUARIO = usuario.Text.Trim();
            bo.NOMBRE = nombre.Text.Trim();
            bo.APELLIDO = apellido.Text.Trim();
            bo.IDENTIDAD = identidad.Text.Trim();
            bo.EMAIL = email.Text.Trim();
            bo.CELULAR = celular.Text.Trim();
            bo.TELEFONO = telefono.Text.Trim();
            bo.EXTENSION = extension.Text.Trim();
            bo.CODPAIS = pais.SelectedValue.Trim();
            bo.CODIGOADUANA = aduana.SelectedValue;
            resp = true;
        }
        else
            registrarMensaje("Nombre de usuario ya existe, favor ingrese otro nombre de usuario");
        return resp;
    }

    protected void rgUsuarios_InsertCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            GridEditableItem editedItem = e.Item as GridEditableItem;
            RadTextBox usuario = (RadTextBox)editedItem.FindControl("edUsuario");
            string[] arreglo = new string[usuario.Text.Trim().Length];
            arreglo = usuario.Text.Trim().Split(" ".ToCharArray());
            if (arreglo.Length == 1)
            {
                UsuariosBO bo = new UsuariosBO(logApp);
                UsuariosBO u = new UsuariosBO(logApp);
                bo.loadUsuario("-1");
                bo.newLine();
                bool resp = GetDatosEditados(bo, u, e.Item, "");
                if (resp)
                {
                    bo.IDUSUARIOINGRESO = Session["IdUsuario"].ToString();
                    bo.PASSWORD = Utilidades.PaginaBase.hashMD5(String.Format("{0}1234$", bo.USUARIO));
                    bo.VENCIMIENTO = DateTime.Now.AddDays(-1.0);
                    bo.commitLine();
                    bo.actualizar();
                    registrarMensaje("Usuario ingresado exitosamente");
                    llenarBitacora("Se ingresó el usuario " + bo.USUARIO, Session["IdUsuario"].ToString(), "");
                    rgRoles.Rebind();
                }
            }
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoUsuarios");
        }
        finally
        {
            desconectar();
        }
    }

    protected void rgUsuarios_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();
            GridEditableItem editedItem = e.Item as GridEditableItem;
            String IdUsuario = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdUsuario"]);
            UsuariosBO bo = new UsuariosBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            bo.loadUsuario(IdUsuario);
            string oldUsuario = bo.USUARIO;
            bool resp = GetDatosEditados(bo, u, e.Item, oldUsuario);
            if (resp)
            {
                bo.actualizar();
                registrarMensaje("Usuario modificado exitósamente");
                llenarBitacora("Se modificó el usuario " + oldUsuario + " por " + bo.USUARIO, Session["IdUsuario"].ToString(), bo.IDUSUARIO);
                rgRoles.Rebind();
            }
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoUsuarios");
        }
        finally
        {
            desconectar();
        }
    }

    protected void rgUsuarios_DeleteCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridDataItem editedItem = e.Item as GridDataItem;

            String IdUsuario = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdUsuario"]);

            UsuariosBO bo = new UsuariosBO(logApp);
            bo.loadUsuario(IdUsuario);
            bo.ELIMINADO = "1";
            bo.actualizar();
            registrarMensaje("Usuario eliminado exitósamente");

            llenarBitacora("Se eliminó el usuario " + bo.USUARIO, Session["IdUsuario"].ToString(), bo.IDUSUARIO);
            rgRoles.Rebind();
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoUsuarios");
        }
        finally
        {
            desconectar();
        }

    }

    public RadComboBox edPais;
    public RadComboBox edAduana;
    protected void rgUsuarios_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if ((e.Item.ItemType == GridItemType.Item) || (e.Item.ItemType == GridItemType.AlternatingItem) || (e.Item.ItemType == GridItemType.SelectedItem))
        {
            SetVisible(e.Item.Cells, "Edit", Modificar);
            SetVisible(e.Item.Cells, "Delete", Anular);
        }

        if (e.Item is GridEditFormInsertItem)
        {
            conectar();
            AduanasBO a = new AduanasBO(logApp);
            GridEditFormItem editForm = e.Item as GridEditFormItem;
            edAduana = editForm.FindControl("edAduana") as RadComboBox;

            a.loadPaisAduanas(edPais.SelectedValue);
            edAduana.DataSource = a.TABLA;
            edAduana.DataBind();
        }
        else if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
        {
            conectar();
            AduanasBO a = new AduanasBO(logApp);
            GridEditFormItem editForm = e.Item as GridEditFormItem;
            edAduana = editForm.FindControl("edAduana") as RadComboBox;

            a.loadPaisAduanas(edPais.SelectedValue);
            edAduana.DataSource = a.TABLA;
            edAduana.DataBind();

            String codigoAduana = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["CodigoAduana"]);
            edAduana.SelectedValue = codigoAduana;
            Session["codigoAduana"] = codigoAduana;
        }
    }

    protected void rgUsuarios_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
        {
            try
            {
                conectar();
                GridEditFormItem editForm = e.Item as GridEditFormItem;
                edPais = editForm.FindControl("edPais") as RadComboBox;
                edPais.AutoPostBack = true;
                edPais.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(edPais_SelectedIndexChanged);
            }
            catch { }

        }
        //if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
        //{
        //    conectar();
        //    GridEditFormItem editForm = e.Item as GridEditFormItem;
        //    edMonitoreoFin = editForm.FindControl("edMonitoreoRemotoFin") as RadComboBox;
        //    edMonitoreoFin.AutoPostBack = true;
        //    edMonitoreoFin.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(edMonitoreoFin_SelectedIndexChanged);
        //}
    }

    void edPais_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            conectar();
            GridEditFormItem editForm = (sender as RadComboBox).NamingContainer as GridEditFormItem;
            edAduana = editForm.FindControl("edAduana") as RadComboBox;
            AduanasBO a = new AduanasBO(logApp);
            a.loadPaisAduanas(edPais.SelectedValue);
            edAduana.DataSource = a.TABLA;
            edAduana.DataBind();
            //edAduana.SelectedValue = Session["codigoAduana"].ToString();
        }
        catch (Exception)
        { }
    }

    protected void rgUsuarios_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            UsuariosBO u = new UsuariosBO(logApp);
            GridDataItem editedItem = e.Item as GridDataItem;
            if (e.CommandName == "Reset")
            {
                String usuario = e.Item.Cells[4].Text;
                u.loadUsuarioXUsuario(usuario.Trim());
                u.PASSWORD = Utilidades.PaginaBase.hashMD5(String.Format("{0}1234$", usuario));
                u.VENCIMIENTO = DateTime.Now.AddDays(-1.0);
                u.actualizar();
                registrarMensaje("El password del usuario " + usuario + " fue reseteado a: " + usuario + "1234$");
            }
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "Instrucciones");
        }
        finally
        {
            desconectar();
        }
    }
    #endregion

    #region Roles
    private void loadGridRoles()
    {
        try
        {
            conectar();
            UsuariosRolesBO bo = new UsuariosRolesBO(logApp);
            bo.loadRolesUsuarios();
            rgRoles.DataSource = bo.TABLA;
            rgRoles.DataBind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected Object GetRoles()
    {
        try
        {
            conectar();

            RolesBO bo = new RolesBO(logApp);
            bo.loadRoles();

            return bo.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }

        return null;
    }

    protected void rgRoles_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        loadGridRoles();
    }

    protected void GetDatosEditadosRoles(UsuariosRolesBO bo, GridItem item)
    {
        GridEditableItem editedItem = item as GridEditableItem;

        RadComboBox usuario = (RadComboBox)editedItem.FindControl("edUsuario");
        RadComboBox rol = (RadComboBox)editedItem.FindControl("edRol");

        bo.IDUSUARIO = usuario.SelectedValue;
        bo.IDROL = rol.SelectedValue;
    }

    protected void rgRoles_InsertCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridEditableItem editedItem = e.Item as GridEditableItem;

            UsuariosRolesBO bo = new UsuariosRolesBO(logApp);
            UsuariosRolesBO ur = new UsuariosRolesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            RolesBO r = new RolesBO(logApp);
            bo.loadUsuariosRoles("-1");
            bo.newLine();

            GetDatosEditadosRoles(bo, e.Item);

            ur.loadRolUsuario(bo.IDUSUARIO, bo.IDROL);
            if (ur.totalRegistros <= 0)
            {
                bo.commitLine();
                bo.actualizar();
                registrarMensaje("Rol asignado a usuario exitosamente");
            }
            else
                registrarMensaje("Usuario ya tiene asignado este rol");

            u.loadUsuario(bo.IDUSUARIO);
            r.loadRoles(bo.IDROL);
            llenarBitacora("Se ingresó el rol " + r.DESCRIPCION + " al usuario " + u.USUARIO, Session["IdUsuario"].ToString(), r.IDROL + " al " + u.IDUSUARIO);
            rgRoles.Rebind();
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoUsuarios");
        }
        finally
        {
            desconectar();
        }
    }

    protected void rgRoles_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridEditableItem editedItem = e.Item as GridEditableItem;

            String IdUsuario = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdUsuario"]);
            String IdRol = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdRol"]);

            UsuariosRolesBO bo = new UsuariosRolesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            RolesBO r = new RolesBO(logApp);
            r.loadRoles(IdRol);

            string oldRol = r.DESCRIPCION;

            bo.loadRolUsuario(IdUsuario, IdRol);

            GetDatosEditadosRoles(bo, e.Item);

            bo.actualizar();
            registrarMensaje("Asignación de rol a usuario modificada exitósamente");

            u.loadUsuario(bo.IDUSUARIO);
            r.loadRoles(bo.IDROL);
            llenarBitacora("Se modificó el rol " + oldRol + "por el rol " + r.DESCRIPCION + " al usuario " + u.USUARIO, Session["IdUsuario"].ToString(), u.IDUSUARIO);
            rgRoles.Rebind();
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoUsuarios");
        }
        finally
        {
            desconectar();
        }
    }

    protected void rgRoles_DeleteCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridDataItem editedItem = e.Item as GridDataItem;

            String IdUsuario = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdUsuario"]);
            String IdRol = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdRol"]);

            UsuariosRolesBO bo = new UsuariosRolesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            RolesBO r = new RolesBO(logApp);
            u.loadUsuario(bo.IDUSUARIO);
            r.loadRoles(bo.IDROL);

            bo.deleteRolUsuario(IdUsuario, IdRol);

            bo.actualizar();

            registrarMensaje("Asignación de rol a usuario eliminada exitósamente");

            llenarBitacora("Se eliminó el rol " + r.DESCRIPCION + " al usuario " + u.USUARIO, Session["IdUsuario"].ToString(), u.IDUSUARIO);
            rgRoles.Rebind();
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoUsuarios");
        }
        finally
        {
            desconectar();
        }
    }

    protected void btnActivo_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton btn = (ImageButton)sender;
        try
        {
            conectar();

            String[] args = btn.CommandArgument.Split(',');
            String IdUsuario = args[0];
            String IdRol = args[1];

            UsuariosRolesBO bo = new UsuariosRolesBO(logApp);
            bo.loadRolUsuario(IdUsuario, IdRol);

            if (btn.CommandName == "Activar")
            {
                bo.ELIMINADO = "0";
                btn.CommandName = "Desactivar";
                btn.ImageUrl = "~/Imagenes/16/check2_16.png";
            }
            else
            {
                bo.ELIMINADO = "1";
                btn.CommandName = "Activar";
                btn.ImageUrl = "~/Imagenes/16/delete2_16.png";
            }

            bo.actualizar();
            rgRoles.Rebind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }


    protected void btnActivo_Click2(object sender, ButtonClickEventArgs e)
    {


    }
    protected void rgRoles_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if ((e.Item.ItemType == GridItemType.Item) || (e.Item.ItemType == GridItemType.AlternatingItem) || (e.Item.ItemType == GridItemType.SelectedItem))
        {
            SetVisible(e.Item.Cells, "Edit", Modificar);
            SetVisible(e.Item.Cells, "Delete", Anular);
        }
    }
    #endregion
    protected void ImageButton1_Command(object sender, CommandEventArgs e)
    {

        RadButton btn =(RadButton)sender;
        String[] args = btn.CommandArgument.Split(',');
        String Usuario = args[0];
        String Estado = args[1];
      
        conectarEsquema();

        GridDataItem editForm = (btn).NamingContainer as GridDataItem;
        Empleados_EsquemaBo Em_ES = new Empleados_EsquemaBo(logAppEsquema);
        Em_ES.loadUsuarioLogin(Usuario);
        if ( Estado=="0")
         {



             if (Em_ES.totalRegistros == 0)
             {
                 conectar();
                 UsuariosBO Usuario_Aduana = new UsuariosBO(logApp);
                 Usuario_Aduana.loadUsuarioLogin(Usuario);

                 Em_ES.loadUsuarioLogin("-1");
                 Em_ES.newLine();
                 Em_ES.NOMBREEMPLEADO = Usuario_Aduana.NOMBRE + " " + Usuario_Aduana.APELLIDO;
                 Em_ES.USUARIO = Usuario_Aduana.USUARIO;
                 Em_ES.CODIGOADUANA = Usuario_Aduana.CODIGOADUANA;
                 Em_ES.PASSWORD = Usuario_Aduana.PASSWORD;
                 Em_ES.IDROLE = "10";
                 Em_ES.SEXO = "N";
                 Em_ES.CODDEPARTAMENTO = "1";
                 Em_ES.ESTADO = "0";
                 Em_ES.commitLine();
                 Em_ES.actualizar();
                 desconectar();
             }
             else
             {
                 Em_ES.ESTADO = "0";
                 Em_ES.commitLine();
                 Em_ES.actualizar();
             }
                   
       
         }
        else
        { 
            Em_ES.ESTADO = "1";
            Em_ES.commitLine();
            Em_ES.actualizar();
        
        }

        desconectarEsquema();
        this.rgUsuarios.Rebind();
        
    }

    protected void ImageButton1_Command2(object sender, CommandEventArgs e)
    {

    }


    #region Conexion Esquema 
    private void conectarEsquema()
    {
        if (logAppEsquema == null)
            logAppEsquema = new GrupoLis.Login.Login();
        if (!logAppEsquema.conectado)
        {
            logAppEsquema.tipoConexion = TipoConexion.SQL_SERVER;
            logAppEsquema.SERVIDOR = mParamethers.Get("Servidor");
            logAppEsquema.DATABASE = mParamethers.Get("DatabaseEsquemaTramites2");
            logAppEsquema.USER = mParamethers.Get("User");
            logAppEsquema.PASSWD = mParamethers.Get("Password");
            logAppEsquema.conectar();
        }
    }

    private void desconectarEsquema()
    {
        if (logAppEsquema.conectado)
            logAppEsquema.desconectar();
    }
    #endregion
}
