﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;

using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.SessionState;
using System.Collections.Specialized;
using GrupoLis.Login;
using System.Text.RegularExpressions;
using System.Security.Principal;
using Telerik.Web;
using System.Security.Cryptography;

public partial class EditarInstrucciones : Utilidades.PaginaBase
{
    private NameValueCollection mParamethers;
    private GrupoLis.Login.Login logAppAduanas;

    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Editar Instrucciones";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        mParamethers = ConfigurationManager.AppSettings;
        SetGridFilterMenu(rgInstrucciones.FilterMenu);
        rgInstrucciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgInstrucciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            if (!Modificar)
                redirectTo("Default.aspx");
            rgInstrucciones.ClientSettings.Scrolling.AllowScroll = true;
            rgInstrucciones.ClientSettings.Scrolling.UseStaticHeaders = true;
            rgInstrucciones.ShowHeader = true;
            rgInstrucciones.ShowFooter = true;
            ((SiteMaster)Master).SetTitulo("Editar Instrucciones", "Editar Instrucciones");
            if (User.IsInRole("Administradores") || User.IsInRole("Administrador Pais"))
                Table1.Visible = true;
            else
                Table1.Visible = false;
        }
    }

    protected void rgInstrucciones_Init(object sender, System.EventArgs e)
    {
        GridFilterMenu menu = rgInstrucciones.FilterMenu;
        menu.Items.RemoveAt(rgInstrucciones.FilterMenu.Items.Count - 2);
    }

    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void rgInstrucciones_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridInstrucciones();
    }

    private void llenarGridInstrucciones()
    {
        try
        {
            conectar();
            InstruccionesBO ins = new InstruccionesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            if (User.IsInRole("Administradores"))
                ins.loadInstruccionesAll();
            else if (User.IsInRole("Administrador Pais"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                ins.loadInstruccionesAllXPais(u.CODPAIS);
            }
            rgInstrucciones.DataSource = ins.TABLA;
        }
        catch (Exception) { }
        finally
        {
            desconectar();
        }
    }

    protected void rgInstrucciones_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            conectarAduanas();
            InstruccionesBO i = new InstruccionesBO(logApp);
            HojaRutaBO hr = new HojaRutaBO(logAppAduanas);
            ComplementoRecepcionBO cr = new ComplementoRecepcionBO(logAppAduanas);
            EncabezadoEsquemaBO ee = new EncabezadoEsquemaBO(logAppAduanas);
            GridEditableItem editedItem = e.Item as GridEditableItem;
            if (e.CommandName == "Guardar")
            {
                edIdInstruccion.Value = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdInstruccion"]);
                RadComboBox edCampo = (RadComboBox)editedItem.FindControl("edCampo");
                RadComboBox edCmbValor = (RadComboBox)editedItem.FindControl("edCmbValor");
                RadTextBox edTxtValor = (RadTextBox)editedItem.FindControl("edTxtValor");
                RadNumericTextBox edNumValor = (RadNumericTextBox)editedItem.FindControl("edNumValor");
                RadTextBox edObservacion = (RadTextBox)editedItem.FindControl("edObservacion");
                RadComboBox edDivision = (RadComboBox)editedItem.FindControl("edDivision");
                switch (edCampo.SelectedValue)
                {
                    case "Seleccione": registrarMensaje("Seleccione un campo"); break;
                    #region Cliente
                    case "IdCliente":
                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            string oldIdCliente = i.IDCLIENTE;
                            i.IDCLIENTE = edCmbValor.SelectedValue;
                            i.actualizar();
                            try
                            {
                                hr.loadHojaRuta(i.IDINSTRUCCION);
                                hr.CODIGOCLIENTE = i.IDCLIENTE;
                                hr.actualizar();
                            }
                            catch { }
                            registrarMensaje("Cliente modificado exitosamente");
                            llenarBitacora("Se modificó el cliente " + oldIdCliente + " por " + i.IDCLIENTE + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region Oficial Cuenta
                    case "OficialCuenta":
                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            string oldOficialCliente = i.OFICIALCUENTA;
                            i.OFICIALCUENTA = edTxtValor.Text;
                            i.actualizar();
                            registrarMensaje("Oficial Cuenta modificado exitosamente");
                            llenarBitacora("Se modificó el oficial de cuenta " + oldOficialCliente + " por " + i.OFICIALCUENTA + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region Referencia Cliente
                    case "ReferenciaCliente":
                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            string oldReferenciaCliente = i.REFERENCIACLIENTE;
                            i.REFERENCIACLIENTE = edTxtValor.Text;
                            i.actualizar();
                            try
                            {
                                hr.loadHojaRuta(i.IDINSTRUCCION);
                                hr.REFERENCIACLIENTE = i.REFERENCIACLIENTE;
                                hr.actualizar();
                            }
                            catch { }
                            registrarMensaje("Referencia Cliente modificada exitosamente");
                            llenarBitacora("Se modificó la referencia del cliente " + oldReferenciaCliente + " por " + i.REFERENCIACLIENTE + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region Proveedor
                    case "Proveedor":
                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            string oldProveedor = i.PROVEEDOR;
                            i.PROVEEDOR = edTxtValor.Text;
                            i.actualizar();
                            try
                            {
                                hr.loadHojaRuta(i.IDINSTRUCCION);
                                hr.PROVEEDOR = i.PROVEEDOR;
                                hr.actualizar();
                            }
                            catch { }
                            registrarMensaje("Proveedor modificado exitosamente");
                            llenarBitacora("Se modificó el proveedor " + oldProveedor + " por " + i.PROVEEDOR + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region ValorFOB
                    case "ValorFOB":
                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            double oldValorFOB = i.VALORFOB;
                            i.VALORFOB = double.Parse(edNumValor.Text);
                            i.VALORCIF = i.VALORCIF - oldValorFOB + i.VALORFOB;
                            i.actualizar();
                            try
                            {
                                cr.loadComplementoRecepcion(i.IDINSTRUCCION);
                                cr.VALORFOB = i.VALORFOB;
                                cr.VALORCIF = cr.VALORCIF - oldValorFOB + i.VALORFOB;
                                cr.actualizar();
                            }
                            catch { }
                            registrarMensaje("ValorFOB modificado exitosamente");
                            llenarBitacora("Se modificó el valorFOB " + oldValorFOB + " por " + i.VALORFOB + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region Flete
                    case "Flete":
                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            double oldFlete = i.FLETE;
                            i.FLETE = double.Parse(edNumValor.Text);
                            i.VALORCIF = i.VALORCIF - oldFlete + i.FLETE;
                            i.actualizar();
                            try
                            {
                                cr.loadComplementoRecepcion(i.IDINSTRUCCION);
                                cr.FLETE = i.FLETE;
                                cr.VALORCIF = cr.VALORCIF - oldFlete + i.FLETE;
                                cr.actualizar();
                            }
                            catch { }
                            registrarMensaje("Flete modificado exitosamente");
                            llenarBitacora("Se modificó el valorFOB " + oldFlete + " por " + i.FLETE + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region Seguro
                    case "Seguro":
                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            double oldSeguro = i.SEGURO;
                            i.SEGURO = double.Parse(edNumValor.Text);
                            i.VALORCIF = i.VALORCIF - oldSeguro + i.SEGURO;
                            i.actualizar();
                            try
                            {
                                cr.loadComplementoRecepcion(i.IDINSTRUCCION);
                                cr.SEGURO = i.SEGURO;
                                cr.VALORCIF = cr.VALORCIF - oldSeguro + i.SEGURO;
                                cr.actualizar();
                            }
                            catch { }
                            registrarMensaje("Seguro modificado exitosamente");
                            llenarBitacora("Se modificó el valorFOB " + oldSeguro + " por " + i.SEGURO + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region Otros
                    case "Otros":
                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            double oldOtros = i.OTROS;
                            i.OTROS = double.Parse(edNumValor.Text);
                            i.VALORCIF = i.VALORCIF - oldOtros + i.OTROS;
                            i.actualizar();
                            try
                            {
                                cr.loadComplementoRecepcion(i.IDINSTRUCCION);
                                cr.OTROS = i.OTROS;
                                cr.VALORCIF = cr.VALORCIF - oldOtros + i.OTROS;
                                cr.actualizar();
                            }
                            catch { }
                            registrarMensaje("Otros modificado exitosamente");
                            llenarBitacora("Se modificó el valorFOB " + oldOtros + " por " + i.OTROS + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region CantidadBultos
                    case "CantidadBultos":
                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            int oldCantidadBultos = i.CANTIDADBULTOS;
                            i.CANTIDADBULTOS = int.Parse(edNumValor.Text);
                            i.actualizar();
                            registrarMensaje("Cantidad de bultos modificada exitosamente");
                            llenarBitacora("Se modificó la cantidad de bultos " + oldCantidadBultos + " por " + i.CANTIDADBULTOS + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region PesoKgs
                    case "PesoKgs":
                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            double oldPesoKgs = i.PESOKGS;
                            i.PESOKGS = double.Parse(edNumValor.Text);
                            i.actualizar();
                            try
                            {
                                cr.loadComplementoRecepcion(i.IDINSTRUCCION);
                                cr.PESO = i.PESOKGS;
                                cr.actualizar();
                            }
                            catch { }
                            registrarMensaje("Peso en Kgs modificado exitosamente");
                            llenarBitacora("Se modificó el peso en kgs. " + oldPesoKgs + " por " + i.PESOKGS + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region Producto
                    case "Producto":
                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            string oldProducto = i.PRODUCTO;
                            i.PRODUCTO = edTxtValor.Text;
                            i.actualizar();
                            try
                            {
                                hr.loadHojaRuta(i.IDINSTRUCCION);
                                hr.DESCRIPCIONPRODUCTO = i.PRODUCTO;
                                hr.actualizar();
                            }
                            catch { }
                            registrarMensaje("Producto modificado exitosamente");
                            llenarBitacora("Se modificó el producto " + oldProducto + " por " + i.PRODUCTO, Session["IdUsuario"].ToString() + " " + edObservacion.Text.Trim(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region PaisProcedencia
                    case "PaisProcedencia":
                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            string oldPaisProcedencia = i.CODPAISPROCEDENCIA;
                            i.CODPAISPROCEDENCIA = edCmbValor.SelectedValue;
                            i.actualizar();
                            registrarMensaje("País de procedencia modificado exitosamente");
                            llenarBitacora("Se modificó el país de procedencia " + oldPaisProcedencia + " por " + i.CODPAISPROCEDENCIA + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region PaisOrigen
                    case "PaisOrigen":
                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            string oldPaisOrigen = i.CODPAISORIGEN;
                            i.CODPAISORIGEN = edCmbValor.SelectedValue;
                            i.actualizar();
                            try
                            {
                                cr.loadComplementoRecepcion(i.IDINSTRUCCION);
                                cr.CODIGOPAIS = i.CODPAISORIGEN;
                                cr.actualizar();
                            }
                            catch { }
                            registrarMensaje("País de origen modificado exitosamente");
                            llenarBitacora("Se modificó el país de origen " + oldPaisOrigen + " por " + i.CODPAISORIGEN + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region PaisDestino
                    case "PaisDestino":
                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            string oldPaisDestino = i.CODPAISDESTINO;
                            i.CODPAISDESTINO = edCmbValor.SelectedValue;
                            i.actualizar();
                            registrarMensaje("País de destino modificado exitosamente");
                            llenarBitacora("Se modificó el país de destino " + oldPaisDestino + " por " + i.CODPAISDESTINO + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region CiudadDestino
                    case "CiudadDestino":
                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            string oldCiudadDestino = i.CIUDADDESTINO;
                            i.CIUDADDESTINO = edTxtValor.Text;
                            i.actualizar();
                            try
                            {
                                cr.loadComplementoRecepcion(i.IDINSTRUCCION);
                                cr.DESTINO = i.CIUDADDESTINO;
                                cr.actualizar();
                            }
                            catch { }
                            registrarMensaje("Ciudad destino modificado exitosamente");
                            llenarBitacora("Se modificó la ciudad destino " + oldCiudadDestino + " por " + i.CIUDADDESTINO + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region TipoTransporte
                    case "TipoTransporte":

                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            string oldTipoTransporte = i.CODTIPOTRANSPORTE;
                            i.CODTIPOTRANSPORTE = edCmbValor.SelectedValue;
                            i.actualizar();
                            registrarMensaje("Tipo de transporte modificado exitosamente");
                            llenarBitacora("Se modificó el tipo de transporte " + oldTipoTransporte + " por " + i.CODTIPOTRANSPORTE + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region TipoCargamento
                    case "TipoCargamento":

                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            string oldTipoCargamento = i.CODTIPOCARGAMENTO;
                            i.CODTIPOCARGAMENTO = edCmbValor.SelectedValue;
                            i.actualizar();
                            try
                            {
                                cr.loadComplementoRecepcion(i.IDINSTRUCCION);
                                cr.TIPOCARGA = i.CODTIPOCARGAMENTO;
                                cr.actualizar();
                            }
                            catch { }
                            registrarMensaje("Tipo de cargamento modificado exitosamente");
                            llenarBitacora("Se modificó el tipo de cargamento " + oldTipoCargamento + " por " + i.CODTIPOCARGAMENTO + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region TipoCarga
                    case "TipoCarga":

                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            string oldTipoCarga = i.CODTIPOCARGA;
                            i.CODTIPOCARGA = edCmbValor.SelectedValue;
                            i.actualizar();
                            registrarMensaje("Tipo de carga modificado exitosamente");
                            llenarBitacora("Se modificó el tipo de carga " + oldTipoCarga + " por " + i.CODTIPOCARGA + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region NoCorrelativo
                    case "NoCorrelativo":
                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            string oldNoCorrelativo = i.NOCORRELATIVO;
                            i.NOCORRELATIVO = edTxtValor.Text;
                            i.actualizar();
                            registrarMensaje("No. Correlativo modificado exitosamente");
                            llenarBitacora("Se modificó el No. Correlativo " + oldNoCorrelativo + " por " + i.NOCORRELATIVO + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region Aforador
                    case "Aforador":

                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            string oldAforador = i.AFORADOR;
                            i.AFORADOR = edCmbValor.SelectedValue;
                            i.actualizar();
                            registrarMensaje("Aforador modificado exitosamente");
                            llenarBitacora("Se modificó el aforador " + oldAforador + " por " + i.AFORADOR + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region Color
                    case "Color":
                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            string oldColor = i.COLOR;
                            i.COLOR = edTxtValor.Text;
                            i.actualizar();
                            registrarMensaje("Color modificado exitosamente");
                            llenarBitacora("Se modificó el color " + oldColor + " por " + i.COLOR + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region Tramitador
                    case "Tramitador":

                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            string oldTramitador = i.TRAMITADOR;
                            i.TRAMITADOR = edCmbValor.SelectedValue;
                            i.actualizar();
                            try
                            {
                                ee.loadEncabezadoEsquema(edIdInstruccion.Value.Trim());
                                ee.Tramitador = i.TRAMITADOR;
                                ee.actualizar();
                            }
                            catch { }
                            registrarMensaje("Tramitador modificado exitosamente");
                            llenarBitacora("Se modificó el tramitador " + oldTramitador + " por " + i.TRAMITADOR + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                    #region Impuesto
                    case "Impuesto":
                        i.loadInstruccion(edIdInstruccion.Value);
                        if (i.totalRegistros > 0)
                        {
                            double oldImpuesto = i.IMPUESTO;
                            i.IMPUESTO = double.Parse(edNumValor.Text);
                            i.actualizar();
                            registrarMensaje("Impuesto modificado exitosamente");
                            llenarBitacora("Se modificó el impuesto " + oldImpuesto + " por " + i.IMPUESTO + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                            rgInstrucciones.Rebind();
                        }
                        break;
                    #endregion
                        #region Division
                    //case "Division":
                    //    i.loadInstruccion(edIdEstadoFlujo.Value);
                    //    if (i.totalRegistros > 0)
                    //    {

                    //    }
                    //    break;
                        #endregion
                    default: break;
                }
            }
        }
        catch (Exception)
        { }
    }

    private void ConfigureExport()
    {
        rgInstrucciones.Columns[4].Visible = false;
        rgInstrucciones.Columns[5].Visible = false;
        rgInstrucciones.Columns[6].Visible = false;
        rgInstrucciones.Columns[7].Visible = false;
        rgInstrucciones.Columns[8].Visible = false;
        rgInstrucciones.Columns[9].Visible = false;
        rgInstrucciones.Columns[10].Visible = false;
        rgInstrucciones.Columns[11].Visible = false;
        rgInstrucciones.Columns[12].Visible = false;
        String filename = "Instrucciones" + "_" + DateTime.Now.ToShortDateString();
        rgInstrucciones.GridLines = GridLines.Both;
        rgInstrucciones.ExportSettings.FileName = filename;
        rgInstrucciones.ExportSettings.IgnorePaging = true;
        rgInstrucciones.ExportSettings.OpenInNewWindow = false;
        rgInstrucciones.ExportSettings.ExportOnlyData = true;
    }

    private void registrarEstadosFlujoCarga()
    {
        try
        {
            conectar();
            GestionCargaBO gc = new GestionCargaBO(logApp);
            InstruccionesBO ins = new InstruccionesBO(logApp);
            FlujoCargaBO fc = new FlujoCargaBO(logApp);
            ClientesBO c = new ClientesBO(logApp);
            EventosBO e = new EventosBO(logApp);
            string fechaEstadoAnterior = "";
            bool registrar = false, noHayEventos = false;
            gc.getMaximaFechaXInstruccion(edIdInstruccion.Value);
            if (gc.totalRegistros > 0 & !String.IsNullOrEmpty(gc.FECHA))
            {
                fechaEstadoAnterior = gc.FECHA;
            }
            else
            {
                ins.loadInstruccion(edIdInstruccion.Value);
                fechaEstadoAnterior = ins.FECHARECEPCION;
            }

            if (Convert.ToDateTime(fechaEstadoAnterior) < Convert.ToDateTime(edFecha.Value + " " + edHora.Value))
                registrar = true;

            fc.loadEstadoFlujoCargaXPaisRegimen(edCodPaisHojaRuta.Value.Trim(), edCodRegimen.Value.Trim());
            if (fc.totalRegistros > 0)
            {
                string[] arreglo = new string[fc.totalRegistros];
                for (int i = 0; i < fc.totalRegistros; i++)
                {
                    arreglo[i] = fc.IDESTADOFLUJO;
                    fc.regSiguiente();
                }

                int pos = -1;
                string nuevoIdEstadoFlujo = "";
                string proxNuevoIdEstadoFlujo = "";
                for (int j = 0; j < arreglo.Length; j++)
                {
                    if (edIdEstadoFlujo.Value.Trim() == arreglo[j])
                    {
                        pos = j;
                        if (j < arreglo.Length - 1)
                        {
                            nuevoIdEstadoFlujo = arreglo[j + 1];
                            if (j + 1 < arreglo.Length - 1) //si el siguiente estado no es el ultimo estado
                                proxNuevoIdEstadoFlujo = arreglo[j + 2];
                            else
                                noHayEventos = true;
                        }
                        else
                            nuevoIdEstadoFlujo = "99"; //instruccion finalizada
                        break;
                    }
                }

                if (nuevoIdEstadoFlujo == "99")
                {
                    e.loadEventosXInstruccion(edIdInstruccion.Value.Trim());
                    if (e.totalRegistros <= 0)
                        noHayEventos = true;
                    else
                        noHayEventos = false;
                }
                else
                    noHayEventos = true;

                if (registrar == true)
                {
                    if (noHayEventos == true)
                    {
                        gc.loadGestionCargaXInstruccion("-1");
                        gc.newLine();
                        gc.IDINSTRUCCION = edIdInstruccion.Value;
                        gc.IDESTADOFLUJO = edIdEstadoFlujo.Value;
                        gc.FECHA = edFecha.Value + " " + edHora.Value;
                        gc.FECHAINGRESO = DateTime.Now.ToString();
                        gc.OBSERVACION = edObservacion.Value;
                        gc.ELIMINADO = "0";
                        gc.IDUSUARIO = Session["IdUsuario"].ToString();
                        gc.commitLine();
                        gc.actualizar();

                        ins.loadInstruccion(edIdInstruccion.Value.Trim());
                        if (ins.totalRegistros > 0 & !String.IsNullOrEmpty(nuevoIdEstadoFlujo))
                        {
                            if (edEstadoFlujo.Value.Contains("Validación Electrónica"))
                            {
                                ins.NOCORRELATIVO = edCorrelativo.Value;
                                ins.AFORADOR = edAforador.Value;
                            }
                            else if (edEstadoFlujo.Value.Contains("Pago de Impuestos"))
                            {
                                try
                                {
                                    ins.IMPUESTO = double.Parse(edImpuesto.Value);
                                }
                                catch { }
                            }
                            else if (edEstadoFlujo.Value.Contains("Asignación Canal de Selectividad"))
                            {
                                ins.COLOR = edColor.Value;
                            }
                            else if (edEstadoFlujo.Value.Contains("Emisión de Pase de Salida"))
                            {
                                ins.TRAMITADOR = edTramitador.Value;
                            }
                            if (edColor.Value.Trim() == "Verde")
                                ins.IDESTADOFLUJO = proxNuevoIdEstadoFlujo;
                            else
                                ins.IDESTADOFLUJO = nuevoIdEstadoFlujo;
                            ins.actualizar();
                        }

                        registrarMensaje(edEstadoFlujo.Value + " registrado exitosamente");
                        llenarBitacora("Se selló el estado del flujo de carga " + edEstadoFlujo.Value, Session["IdUsuario"].ToString(), edIdInstruccion.Value);

                        conectar();
                        c.loadCliente(ins.IDCLIENTE);
                        if (c.totalRegistros > 0)
                        {
                            if (edEstadoFlujo.Value.Contains("Canal de Selectividad"))
                            {
                                //enviar correo con notificación
                                sendMail(c.EMAIL, "Canal de Selectividad", "Se asignó el canal de selectividad a la instrucción No. " + ins.IDINSTRUCCION + ", con color " + edColor.Value.Trim());
                            }
                            else if (edEstadoFlujo.Value.Contains("Entrega de Servicio"))
                            {
                                //enviar correo con notificación
                                sendMail(c.EMAIL, "Entrega de Servicio", "Se registró la entrega de servicio de la instrucción No. " + ins.IDINSTRUCCION);
                            }
                        }
                        rgInstrucciones.Rebind();
                    }
                    else
                        registrarMensaje("No se puede registrar este estado, hay al menos algún evento sin finalizar");
                }
                else
                    registrarMensaje("No existe flujo de carga para este régimen");
            }
            else
                registrarMensaje("La fecha actual tiene que ser mayor que la fecha del estado anterior, la cual es " + fechaEstadoAnterior);
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "GestionCarga");
            if (ex.Message.Contains(""))
                rgInstrucciones.Rebind();
        }
        finally
        {
            desconectar();
        }
    }

    protected void btnLimpiar_Click(object sender, ImageClickEventArgs e)
    {
        limpiar();
    }

    private void limpiar()
    {
        try
        {
            foreach (GridDataItem grdItem in rgInstrucciones.Items)
            {
                RadComboBox edCampo = (RadComboBox)grdItem.FindControl("edCampo");
                RadTextBox edObservacion = (RadTextBox)grdItem.FindControl("edObservacion");
                RadTextBox edTxtValor = (RadTextBox)grdItem.FindControl("edTxtValor");
                RadComboBox edCmbValor = (RadComboBox)grdItem.FindControl("edCmbValor");
                RadNumericTextBox edNumValor = (RadNumericTextBox)grdItem.FindControl("edNumValor");
                edCampo.ClearSelection();
                edObservacion.Text = "";
                edTxtValor.Visible = false;
                edCmbValor.Visible = false;
                edNumValor.Visible = false;
            }
        }
        catch (Exception)
        { }
    }

    protected void rgInstrucciones_ItemDataBound(object sender, GridItemEventArgs e)
    {
        try
        {
            foreach (GridDataItem gridItem in rgInstrucciones.Items)
            {
                RadComboBox edCampo = (RadComboBox)gridItem.FindControl("edCampo");
                RadComboBox edCmbValor = (RadComboBox)gridItem.FindControl("edCmbValor");
                RadTextBox edTxtValor = (RadTextBox)gridItem.FindControl("edTxtValor");
                RadNumericTextBox edNumValor = (RadNumericTextBox)gridItem.FindControl("edNumValor");
                switch (edCampo.SelectedValue)
                {
                    case "Seleccione":
                        edCmbValor.Visible = false;
                        edTxtValor.Visible = false;
                        edNumValor.Visible = false;
                        break;
                    default: break;
                }
            }
        }
        catch { }
    }

    protected void SetVisible(TableCellCollection cells, String commandName, string visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is RadTextBox)
                {
                    RadTextBox txt = (RadTextBox)control;
                    if (txt.ID == commandName)
                        txt.Visible = true;
                    else
                        txt.Visible = false;
                }
            }
        }
    }

    protected void rgInstrucciones_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            try
            {
                conectar();
                GridDataItem editForm = e.Item as GridDataItem;
                RadComboBox edCampo = editForm.FindControl("edCampo") as RadComboBox;
                edCampo.AutoPostBack = true;
                edCampo.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(edCampo_SelectedIndexChanged);
            }
            catch { }
        }
    }

    void edCampo_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            //////////////////////
            ClientesBO c = new ClientesBO(logApp);
            CodigosBO co = new CodigosBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            //////////////////////
            GridDataItem editForm = (sender as RadComboBox).NamingContainer as GridDataItem;
            RadComboBox edCampo = editForm.FindControl("edCampo") as RadComboBox;
            RadComboBox edCmbValor = editForm.FindControl("edCmbValor") as RadComboBox;
            RadTextBox edTxtValor = (RadTextBox)editForm.FindControl("edTxtValor");
            RadNumericTextBox edNumValor = (RadNumericTextBox)editForm.FindControl("edNumValor");
            string idInstruccion = editForm.Cells[2].Text;
            string codPais = editForm.Cells[6].Text;
            switch (edCampo.SelectedValue)
            {
                case "Seleccione":
                    edCmbValor.Visible = false;
                    edTxtValor.Visible = false;
                    edNumValor.Visible = false;
                    break;
                #region IdCliente
                case "IdCliente":
                    edCmbValor.Visible = true;
                    edTxtValor.Visible = false;
                    edNumValor.Visible = false;
                    c.loadAllCamposClientesXPais(codPais);
                    edCmbValor.DataTextField = c.TABLA.Columns["Nombre"].ToString();
                    edCmbValor.DataValueField = c.TABLA.Columns["CodigoSAP"].ToString();
                    edCmbValor.DataSource = c.TABLA;
                    edCmbValor.DataBind();
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        edCmbValor.SelectedValue = i.IDCLIENTE;
                    }
                    catch { }
                    break;
                #endregion
                #region OficialCuenta
                case "OficialCuenta":
                    edCmbValor.Visible = false;
                    edTxtValor.Visible = true;
                    edNumValor.Visible = false;
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        edTxtValor.Text = i.OFICIALCUENTA;
                    }
                    catch { }

                    break;
                #endregion
                #region ReferenciaCliente
                case "ReferenciaCliente":
                    edCmbValor.Visible = false;
                    edTxtValor.Visible = true;
                    edNumValor.Visible = false;
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        edTxtValor.Text = i.REFERENCIACLIENTE;
                    }
                    catch { }

                    break;
                #endregion
                #region Proveedor
                case "Proveedor":
                    edCmbValor.Visible = false;
                    edTxtValor.Visible = true;
                    edNumValor.Visible = false;
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        edTxtValor.Text = i.PROVEEDOR;
                    }
                    catch { }

                    break;
                #endregion
                #region ValorFOB
                case "ValorFOB":
                    edCmbValor.Visible = false;
                    edTxtValor.Visible = false;
                    edNumValor.Visible = true;
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        edNumValor.Value = i.VALORFOB;
                    }
                    catch { }

                    break;
                #endregion
                #region Flete
                case "Flete":
                    edCmbValor.Visible = false;
                    edTxtValor.Visible = false;
                    edNumValor.Visible = true;
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        edNumValor.Value = i.FLETE;
                    }
                    catch { }

                    break;
                #endregion
                #region Seguro
                case "Seguro":
                    edCmbValor.Visible = false;
                    edTxtValor.Visible = false;
                    edNumValor.Visible = true;
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        edNumValor.Value = i.SEGURO;
                    }
                    catch { }

                    break;
                #endregion
                #region Otros
                case "Otros":
                    edCmbValor.Visible = false;
                    edTxtValor.Visible = false;
                    edNumValor.Visible = true;
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        edNumValor.Value = i.OTROS;
                    }
                    catch { }

                    break;
                #endregion
                #region CantidadBultos
                case "CantidadBultos":
                    edCmbValor.Visible = false;
                    edTxtValor.Visible = false;
                    edNumValor.Visible = true;
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        edNumValor.Value = i.CANTIDADBULTOS;
                    }
                    catch { }

                    break;
                #endregion
                #region PesoKgs
                case "PesoKgs":
                    edCmbValor.Visible = false;
                    edTxtValor.Visible = false;
                    edNumValor.Visible = true;
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        edNumValor.Value = i.PESOKGS;
                    }
                    catch { }

                    break;
                #endregion
                #region Producto
                case "Producto":
                    edCmbValor.Visible = false;
                    edTxtValor.Visible = true;
                    edNumValor.Visible = false;
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        edTxtValor.Text = i.PRODUCTO;
                    }
                    catch { }

                    break;
                #endregion
                #region PaisProcedencia
                case "PaisProcedencia":
                    edCmbValor.Visible = true;
                    edTxtValor.Visible = false;
                    edNumValor.Visible = false;
                    co.loadAllCampos("PAISES");
                    edCmbValor.DataTextField = co.TABLA.Columns["Descripcion"].ToString();
                    edCmbValor.DataValueField = co.TABLA.Columns["Codigo"].ToString();
                    edCmbValor.DataSource = co.TABLA;
                    edCmbValor.DataBind();
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        edCmbValor.SelectedValue = i.CODPAISPROCEDENCIA;
                    }
                    catch { }
                    break;
                #endregion
                #region PaisOrigen
                case "PaisOrigen":
                    edCmbValor.Visible = true;
                    edTxtValor.Visible = false;
                    edNumValor.Visible = false;
                    co.loadAllCampos("PAISES");
                    edCmbValor.DataTextField = co.TABLA.Columns["Descripcion"].ToString();
                    edCmbValor.DataValueField = co.TABLA.Columns["Codigo"].ToString();
                    edCmbValor.DataSource = co.TABLA;
                    edCmbValor.DataBind();
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        edCmbValor.SelectedValue = i.CODPAISORIGEN;
                    }
                    catch { }
                    break;
                #endregion
                #region PaisDestino
                case "PaisDestino":
                    edCmbValor.Visible = true;
                    edTxtValor.Visible = false;
                    edNumValor.Visible = false;
                    co.loadAllCampos("PAISES");
                    edCmbValor.DataTextField = co.TABLA.Columns["Descripcion"].ToString();
                    edCmbValor.DataValueField = co.TABLA.Columns["Codigo"].ToString();
                    edCmbValor.DataSource = co.TABLA;
                    edCmbValor.DataBind();
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        edCmbValor.SelectedValue = i.CODPAISDESTINO;
                    }
                    catch { }
                    break;
                #endregion
                #region CiudadDestino
                case "CiudadDestino":
                    edCmbValor.Visible = false;
                    edTxtValor.Visible = true;
                    edNumValor.Visible = false;
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        edTxtValor.Text = i.CIUDADDESTINO;
                    }
                    catch { }

                    break;
                #endregion
                #region TipoTransporte
                case "TipoTransporte":
                    edCmbValor.Visible = true;
                    edTxtValor.Visible = false;
                    edNumValor.Visible = false;
                    co.loadAllCampos("TIPOTRANSPORTE");
                    edCmbValor.DataTextField = co.TABLA.Columns["Descripcion"].ToString();
                    edCmbValor.DataValueField = co.TABLA.Columns["Codigo"].ToString();
                    edCmbValor.DataSource = co.TABLA;
                    edCmbValor.DataBind();
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        edCmbValor.SelectedValue = i.CODTIPOTRANSPORTE;
                    }
                    catch { }
                    break;
                #endregion
                #region TipoCargamento
                case "TipoCargamento":
                    edCmbValor.Visible = true;
                    edTxtValor.Visible = false;
                    edNumValor.Visible = false;
                    co.loadAllCampos("TIPOCARGAMENTO");
                    edCmbValor.DataTextField = co.TABLA.Columns["Descripcion"].ToString();
                    edCmbValor.DataValueField = co.TABLA.Columns["Codigo"].ToString();
                    edCmbValor.DataSource = co.TABLA;
                    edCmbValor.DataBind();
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        edCmbValor.SelectedValue = i.CODTIPOCARGAMENTO;
                    }
                    catch { }
                    break;
                #endregion
                #region TipoCarga
                case "TipoCarga":
                    edCmbValor.Visible = true;
                    edTxtValor.Visible = false;
                    edNumValor.Visible = false;
                    co.loadAllCampos("TIPOCARGA");
                    edCmbValor.DataTextField = co.TABLA.Columns["Descripcion"].ToString();
                    edCmbValor.DataValueField = co.TABLA.Columns["Codigo"].ToString();
                    edCmbValor.DataSource = co.TABLA;
                    edCmbValor.DataBind();
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        edCmbValor.SelectedValue = i.CODTIPOCARGA;
                    }
                    catch { }
                    break;
                #endregion
                #region NoCorrelativo
                case "NoCorrelativo":
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        if (!String.IsNullOrEmpty(i.NOCORRELATIVO.Trim()))
                        {
                            edTxtValor.Text = i.NOCORRELATIVO;
                            edCmbValor.Visible = false;
                            edTxtValor.Visible = true;
                            edNumValor.Visible = false;
                        }
                        else
                        {
                            edCmbValor.Visible = false;
                            edTxtValor.Visible = false;
                            edNumValor.Visible = false;
                            registrarMensaje("No. Correlativo todavia no ha sido ingresado");
                        }
                    }
                    catch { }

                    break;
                #endregion
                #region Aforador
                case "Aforador":
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        if (!String.IsNullOrEmpty(i.AFORADOR.Trim()))
                        {
                            u.loadUsuariosNombreCompletoXAduana(i.CODIGOADUANA, "Aforadores");
                            edCmbValor.DataTextField = u.TABLA.Columns["NombreCompleto"].ToString();
                            edCmbValor.DataValueField = u.TABLA.Columns["IdUsuario"].ToString();
                            edCmbValor.DataSource = u.TABLA;
                            edCmbValor.DataBind();
                            edCmbValor.SelectedValue = i.AFORADOR;
                            edCmbValor.Visible = true;
                            edTxtValor.Visible = false;
                            edNumValor.Visible = false;
                        }
                        else
                        {
                            edCmbValor.Visible = false;
                            edTxtValor.Visible = false;
                            edNumValor.Visible = false;
                            registrarMensaje("Aforador todavia no ha sido ingresado");
                        }
                    }
                    catch { }
                    break;
                #endregion
                #region Color
                case "Color":
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        if (!String.IsNullOrEmpty(i.COLOR.Trim()))
                        {
                            edTxtValor.Text = i.COLOR;
                            edCmbValor.Visible = false;
                            edTxtValor.Visible = true;
                            edNumValor.Visible = false;
                        }
                        else
                        {
                            edCmbValor.Visible = false;
                            edTxtValor.Visible = false;
                            edNumValor.Visible = false;
                            registrarMensaje("Color todavia no ha sido ingresado");
                        }
                    }
                    catch { }

                    break;
                #endregion
                #region Tramitador
                case "Tramitador":
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        if (!String.IsNullOrEmpty(i.TRAMITADOR.Trim()))
                        {
                            u.loadUsuariosNombreCompletoXAduana(i.CODIGOADUANA, "Tramitadores");
                            edCmbValor.DataTextField = u.TABLA.Columns["NombreCompleto"].ToString();
                            edCmbValor.DataValueField = u.TABLA.Columns["IdUsuario"].ToString();
                            edCmbValor.DataSource = u.TABLA;
                            edCmbValor.DataBind();
                            edCmbValor.SelectedValue = i.TRAMITADOR;
                            edCmbValor.Visible = true;
                            edTxtValor.Visible = false;
                            edNumValor.Visible = false;
                        }
                        else
                        {
                            edCmbValor.Visible = false;
                            edTxtValor.Visible = false;
                            edNumValor.Visible = false;
                            registrarMensaje("Tramitador todavia no ha sido ingresado");
                        }
                    }
                    catch { }
                    break;
                #endregion
                #region Impuesto
                case "Impuesto":
                    try
                    {
                        i.loadInstruccion(idInstruccion);
                        if (!String.IsNullOrEmpty(i.IMPUESTO.ToString()))
                        {
                            edNumValor.Value = i.IMPUESTO;
                            edCmbValor.Visible = false;
                            edTxtValor.Visible = false;
                            edNumValor.Visible = true;
                        }
                        else
                        {
                            edCmbValor.Visible = false;
                            edTxtValor.Visible = false;
                            edNumValor.Visible = false;
                            registrarMensaje("Impuesto todavia no ha sido ingresado");
                        }
                    }
                    catch { }

                    break;
                #endregion
                default:

                    break;
            }
        }
        catch (Exception)
        { }
    }

    #region Conexion EsquemaTramites2
    private void conectarAduanas()
    {
        if (logAppAduanas == null)
            logAppAduanas = new GrupoLis.Login.Login();
        if (!logAppAduanas.conectado)
        {
            logAppAduanas.tipoConexion = TipoConexion.SQL_SERVER;
            logAppAduanas.SERVIDOR = mParamethers.Get("Servidor");
            logAppAduanas.DATABASE = mParamethers.Get("DatabaseEsquemaTramites2");
            logAppAduanas.USER = mParamethers.Get("User");
            logAppAduanas.PASSWD = mParamethers.Get("Password");
            logAppAduanas.conectar();
        }
    }

    private void desconectarAduanas()
    {
        if (logAppAduanas.conectado)
            logAppAduanas.desconectar();
    }
    #endregion

    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            conectar();
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (User.IsInRole("Administradores"))
                llenarGridInstruccion(txtInstruccion.Text.Trim());
            else if (User.IsInRole("Administrador Pais") || User.IsInRole("Administrador Pais SVGT")& (txtInstruccion.Text.Trim().Substring(0, 1) == u.CODPAIS))
                llenarGridInstruccion(txtInstruccion.Text.Trim());
            else
            {
                rgInstrucciones.Rebind();
                registrarMensaje("No puede consultar instrucciones de otro país");
            }
        }
        catch { }
    }

    private void llenarGridInstruccion(string idInstruccion)
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            i.loadInstruccionesAllXInstruccion(idInstruccion);
            rgInstrucciones.DataSource = i.TABLA;
            rgInstrucciones.DataBind();
        }
        catch { }
    }
}