﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;

public partial class EnviosEspeciesFiscales : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Envios Especies Fiscales";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgRangoEspeciesFiscales.FilterMenu);
        rgRangoEspeciesFiscales.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgRangoEspeciesFiscales.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Envíos Especies Fiscales", "Envíos Especies Fiscales");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
            mpEspeciesFiscales.SelectedIndex = 0;
            limpiarControles();
            cargarDatosInicioRangos();
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    #region Envíos Especies Fiscales
    protected void rgRangoEspeciesFiscales_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenarGridRangoEspeciesFiscales();
    }

    private void llenarGridRangoEspeciesFiscales()
    {
        try
        {
            conectar();
            MovimientosEspeciesFiscalesBO mef = new MovimientosEspeciesFiscalesBO(logApp);
            mef.loadRangosEspeciesFiscalesEnvios(cmbAduanaOrigen.SelectedValue, cmbEspecieFiscal.SelectedValue);
            rgRangoEspeciesFiscales.DataSource = mef.TABLA;
            rgRangoEspeciesFiscales.DataBind();

            for (int i = 0; i < mef.totalRegistros; i++)
            {
                foreach (GridDataItem gridItem in rgRangoEspeciesFiscales.Items)
                {
                    string RangoInicial = gridItem.Cells[10].Text;
                    string RangoFinal = gridItem.Cells[11].Text;
                    if (mef.TABLA.Rows[i]["RangoFinal"].ToString() == RangoFinal && mef.TABLA.Rows[i]["RangoInicial"].ToString() == RangoInicial)
                    {
                        RadTextBox item = (RadTextBox)gridItem.FindControl("Item");
                        item.Text = mef.TABLA.Rows[i]["Item"].ToString();
                    }
                }
            }
        }
        catch { }
    }

    protected void rgRangoEspeciesFiscales_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            ComprasEspeciesFiscalesBO cef = new ComprasEspeciesFiscalesBO(logApp);
            MovimientosEspeciesFiscalesBO mef = new MovimientosEspeciesFiscalesBO(logApp);
            GridEditableItem editedItem = e.Item as GridEditableItem;

            if (rgRangoEspeciesFiscales.Items.Count >= 1 & (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName))
                ConfigureExportRangoEF();
            else if (rgRangoEspeciesFiscales.Items.Count < 1 & (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName))
                e.Canceled = true;
        }
        catch { }
    }

    private void ConfigureExportRangoEF()
    {
        String filename = "Compras Especies Fiscales " + DateTime.Now.ToShortDateString();
        rgRangoEspeciesFiscales.ExportSettings.FileName = filename;
        rgRangoEspeciesFiscales.ExportSettings.ExportOnlyData = true;
        rgRangoEspeciesFiscales.ExportSettings.IgnorePaging = true;
        rgRangoEspeciesFiscales.ExportSettings.OpenInNewWindow = true;
        rgRangoEspeciesFiscales.MasterTableView.ExportToExcel();
    }

    protected void rgRangoEspeciesFiscales_Init(object sender, EventArgs e)
    {
        GridFilterMenu menu = rgRangoEspeciesFiscales.FilterMenu;
        menu.Items.RemoveAt(rgRangoEspeciesFiscales.FilterMenu.Items.Count - 2);
    }

    private void cargarDatosInicioRangos()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (u.CODPAIS == "H")
                c.loadPaisesHojaRuta();
            else
                c.loadAllCampos("PAISES", u.CODPAIS);
            cmbPais.DataSource = c.TABLA;
            cmbPais.DataBind();
            loadAduanasOrigen();
            loadAduanasDestino();
        }
        catch { }
    }

    private void loadAduanasOrigen()
    {
        try
        {
            conectar();
            AduanasBO a = new AduanasBO(logApp);
            a.loadPaisAduanas(cmbPais.SelectedValue);
            cmbAduanaOrigen.DataSource = a.TABLA;
            cmbAduanaOrigen.DataBind();
            loadEspeciesFiscales();
        }
        catch { }
    }

    private void loadAduanasDestino()
    {
        try
        {
            conectar();
            AduanasBO a = new AduanasBO(logApp);
            a.loadAduanasXPaisSinOrigen(cmbPais.SelectedValue, cmbAduanaOrigen.SelectedValue);
            cmbAduanaDestino.DataSource = a.TABLA;
            cmbAduanaDestino.DataBind();
            loadEspeciesFiscales();
            llenarGridRangoEspeciesFiscales();
        }
        catch { }
    }

    private void loadEspeciesFiscales()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            if (cmbAduanaDestino.SelectedValue == "DEYSI" || cmbAduanaDestino.SelectedValue == "JOHNY" || cmbAduanaDestino.SelectedValue == "SAMAN")
                //c.loadAllCampos("ESPECIESFISCALES", "F");
                c.loadCodigosXCategoriaCodigos("ESPECIESFISCALES", "'DA','DVA','F','P','DTI','HN-MI','HN-MN','SS','ZL'");
            //else if (cmbAduanaDestino.SelectedValue == "023")
            //    c.loadCodigosXCategoriaCodigos("ESPECIESFISCALES", "'DTI','HN-MI','HN-MN'");
            else if (cmbAduanaDestino.SelectedValue == "ARMAN")
                c.loadAllCampos("ESPECIESFISCALES", "P");
            else if (cmbAduanaDestino.SelectedValue == "CARLO")
                c.loadCodigosXCategoriaCodigos("ESPECIESFISCALES", "'DTI','HN-MI'");

            else if (cmbAduanaOrigen.SelectedValue == "DEYSI" || cmbAduanaOrigen.SelectedValue == "JOHNY" || cmbAduanaOrigen.SelectedValue == "SAMAN")
                //c.loadAllCampos("ESPECIESFISCALES", "F");
                c.loadCodigosXCategoriaCodigos("ESPECIESFISCALES", "'DA','DVA','F','P','DTI','HN-MI','HN-MN','SS','ZL'");
            //else if (cmbAduanaOrigen.SelectedValue == "023")
                //c.loadCodigosXCategoriaCodigos("ESPECIESFISCALES", "'DTI','HN-MI','HN-MN'");
            else if (cmbAduanaOrigen.SelectedValue == "ARMAN")
                c.loadAllCampos("ESPECIESFISCALES", "P");
            else if (cmbAduanaOrigen.SelectedValue == "CARLO")
                c.loadCodigosXCategoriaCodigos("ESPECIESFISCALES", "'DTI','HN-MI'");
            else
                c.loadAllCampos("ESPECIESFISCALES");
            cmbEspecieFiscal.DataSource = c.TABLA;
            cmbEspecieFiscal.DataBind();
            cmbEspecieFiscal.Items.Remove(9);
            cmbEspecieFiscal.Items.Remove(9);
            cmbEspecieFiscal.Items.Remove(9);
            cmbEspecieFiscal.Items.Remove(9);
            cmbEspecieFiscal.Items.Remove(9);
        }
        catch { }
    }

    protected void cmbPais_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        loadAduanasOrigen();
        loadAduanasDestino();
    }

    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            conectar();
            MovimientosEspeciesFiscalesBO mef = new MovimientosEspeciesFiscalesBO(logApp);
            MovimientosEspeciesFiscalesBO mefe = new MovimientosEspeciesFiscalesBO(logApp);
            TrasladosEspeciesFiscalesBO tef = new TrasladosEspeciesFiscalesBO(logApp);
            EspeciesFiscalesClientesBO efc = new EspeciesFiscalesClientesBO(logApp);
            if (!cmbPais.IsEmpty)
            {
                if (!cmbAduanaOrigen.IsEmpty)
                {
                    if (!cmbAduanaDestino.IsEmpty)
                    {
                        if (!cmbEspecieFiscal.IsEmpty)
                        {
                            if (txtCantidad.Value.HasValue)
                            {
                                if (txtCantidad.Value > 0)
                                {   ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                    string rango = "0";
                                    foreach (GridDataItem grdItem in rgRangoEspeciesFiscales.Items)
                                    {

                                        CheckBox chkRango = (CheckBox)grdItem.FindControl("chkRango");
                                        RadTextBox item = (RadTextBox)grdItem.FindControl("Item");
                                        string hola = item.Text;
                                        if (chkRango.Checked)
                                        {
                                            rango = rango + "," + item.Text;
                                        }
                                    }
                                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                    mef.loadMovimientosEspeciesFiscales2(cmbEspecieFiscal.SelectedValue, cmbAduanaOrigen.SelectedValue,rango);
                                    if (mef.totalRegistros > 0)
                                    {
                                        int cantidad = 0, cantidadDisponible = 0, cantidadSolicitada = int.Parse(txtCantidad.Value.ToString());
                                        for (int i = 0; i < mef.totalRegistros; i++)
                                        {
                                            cantidad += mef.CANTIDAD;
                                            mef.regSiguiente();
                                        }
                                        if (cantidad >= cantidadSolicitada)
                                        {
                                            DateTime fecha = DateTime.Now;      //20120109.171835.761    
                                            string rangoInicial = "", rangoFinal = "";
                                            string idMovimiento = fecha.Year.ToString() + fecha.Month.ToString("00") + fecha.Day.ToString("00") + "." + fecha.Hour.ToString("00") + fecha.Minute.ToString("00") + fecha.Second.ToString("00") + "." + fecha.Millisecond.ToString("000");
                                           

                                            mef.loadMovimientosEspeciesFiscales2(cmbEspecieFiscal.SelectedValue, cmbAduanaOrigen.SelectedValue,rango);
                                            for (int i = 0; i < mef.totalRegistros; i++)
                                            {
                                                cantidadDisponible += mef.CANTIDAD;
                                                if (cantidadDisponible > cantidadSolicitada)  //txtCantidad.Value
                                                {
                                                    //guardar nuevo registro con las series que quedan
                                                    mefe.loadMovimientosEspeciesFiscales("-1");
                                                    mefe.newLine();
                                                    mefe.IDMOVIMIENTO = idMovimiento;
                                                    mefe.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                                                    mefe.CODPAIS = cmbPais.SelectedValue;
                                                    mefe.CODIGOADUANA = cmbAduanaOrigen.SelectedValue;
                                                    mefe.RANGOINICIAL = (int.Parse(mef.RANGOINICIAL) + cantidadSolicitada).ToString();
                                                    mefe.RANGOFINAL = mef.RANGOFINAL;
                                                    mefe.CANTIDAD = int.Parse((cantidadDisponible - cantidadSolicitada).ToString());
                                                    mefe.FECHA = DateTime.Now.ToString();
                                                    //Si son DVA o F busca si estaba asignadas a un cliente y de ser asi las vuelve a dejar asignadas 
                                                    //De lo contrario las deja libre.
                                                    if (cmbEspecieFiscal.SelectedValue == "DVA" || cmbEspecieFiscal.SelectedValue == "F")
                                                    {
                                                        efc.loadEspeciesFiscalesClientesXIdMov(mef.IDMOVIMIENTO);
                                                        if (efc.totalRegistros > 0)
                                                        {
                                                            string idCliente = efc.IDCLIENTE;
                                                            //guardar nuevo registro con las series que se envian al cliente
                                                            efc.loadEspeciesFiscalesClientesXIdMov("-1");
                                                            efc.newLine();
                                                            efc.IDMOVIMIENTO = idMovimiento;
                                                            efc.IDCLIENTE = idCliente;
                                                            efc.OBSERVACION = "";
                                                            efc.commitLine();
                                                            efc.actualizar();
                                                            mefe.CODTIPOMOVIMIENTO = "5";   //1 Envío
                                                            mefe.CODESTADO = "3";   //Diferencia
                                                        }
                                                        else
                                                        {
                                                            mefe.CODTIPOMOVIMIENTO = "1";   //1 Envío
                                                            mefe.CODESTADO = "2";   //Diferencia
                                                        }
                                                    }
                                                    else
                                                    {
                                                        mefe.CODTIPOMOVIMIENTO = "1";   //1 Envío
                                                        mefe.CODESTADO = "2";   //Diferencia
                                                    }
                                                    mefe.IDUSUARIO = Session["IdUsuario"].ToString();
                                                    mefe.IDCOMPRA = mef.IDCOMPRA;
                                                    mefe.commitLine();
                                                    mefe.actualizar();

                                                    
                                                }
                                                DateTime fecha1 = DateTime.Now;
                                                string idMovimientoEnvio = fecha1.Year.ToString() + fecha1.Month.ToString("00") + fecha1.Day.ToString("00") + "." + fecha1.Hour.ToString("00") + fecha1.Minute.ToString("00") + fecha1.Second.ToString("00") + "." + fecha1.Millisecond.ToString("000");
                                                if (cantidadDisponible >= cantidadSolicitada)
                                                {
                                                    //cambia el estado a eliminado
                                                    mef.CODESTADO = "100";
                                                    mef.actualizar();
                                                    //guardar nuevo registro con las series que se envian a la otra aduana en movimientos
                                                    mefe.loadMovimientosEspeciesFiscales("-1");
                                                    mefe.newLine();
                                                    mefe.IDMOVIMIENTO = idMovimientoEnvio;
                                                    mefe.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                                                    mefe.CODPAIS = cmbPais.SelectedValue;
                                                    mefe.CODIGOADUANA = cmbAduanaDestino.SelectedValue;
                                                    mefe.RANGOINICIAL = mef.RANGOINICIAL;
                                                    mefe.RANGOFINAL = (int.Parse(mef.RANGOINICIAL) + cantidadSolicitada - 1).ToString();
                                                    mefe.CANTIDAD = cantidadSolicitada;
                                                    mefe.FECHA = DateTime.Now.ToString();
                                                    mefe.CODTIPOMOVIMIENTO = "1";   //1 Envío, 0 Compra
                                                    mefe.CODESTADO = "0";   //Enviado
                                                    mefe.IDUSUARIO = Session["IdUsuario"].ToString();
                                                    mefe.IDCOMPRA = mef.IDCOMPRA;
                                                    mefe.commitLine();
                                                    mefe.actualizar();

                                                    //guardar nuevo registro con las series que se envian a la otra aduana en traslados
                                                    tef.loadTrasladosEspeciesFiscales("-1");
                                                    tef.newLine();
                                                    tef.IDENVIO = idMovimientoEnvio;
                                                    tef.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                                                    tef.CODPAIS = cmbPais.SelectedValue;
                                                    tef.CODIGOADUANAORIGEN = cmbAduanaOrigen.SelectedValue;
                                                    tef.CODIGOADUANADESTINO = cmbAduanaDestino.SelectedValue;
                                                    tef.RANGOINICIAL = mefe.RANGOINICIAL;
                                                    tef.RANGOFINAL = mefe.RANGOFINAL;
                                                    tef.CANTIDAD = mefe.CANTIDAD;
                                                    tef.FECHA = DateTime.Now.ToString();
                                                    tef.IDUSUARIO = mefe.IDUSUARIO;
                                                    tef.IDMOVIMIENTO = mef.IDMOVIMIENTO;
                                                    tef.commitLine();
                                                    tef.actualizar();

                                                    //nuevo
                                                    rangoInicial = tef.RANGOINICIAL;
                                                    rangoFinal = tef.RANGOFINAL;
                                                    //

                                                    if (cmbEspecieFiscal.SelectedValue == "DVA" || cmbEspecieFiscal.SelectedValue == "F")
                                                    {
                                                        //efc.loadEspeciesFiscalesClientesXIdMov(mef.IDMOVIMIENTO);
                                                        //if (efc.totalRegistros > 0)
                                                        //{
                                                        //    string idCliente = efc.IDCLIENTE;
                                                        //    //guardar nuevo registro con las series que se envian al cliente
                                                        //    efc.loadEspeciesFiscalesClientesXIdMov("-1");
                                                        //    efc.newLine();
                                                        //    efc.IDMOVIMIENTO = idMovimientoEnvio;
                                                        //    efc.IDCLIENTE = idCliente;
                                                        //    efc.OBSERVACION = "";
                                                        //    efc.commitLine();
                                                        //    efc.actualizar();
                                                        //}
                                                    }

                                                    break;
                                                }
                                                else
                                                {
                                                    //cambia el estado a eliminado
                                                    mef.CODESTADO = "100";
                                                    mef.actualizar();
                                                    //guardar nuevo registro con las series que se envian a la otra aduana
                                                    mefe.loadMovimientosEspeciesFiscales("-1");
                                                    mefe.newLine();
                                                    mefe.IDMOVIMIENTO = idMovimientoEnvio;
                                                    mefe.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                                                    mefe.CODPAIS = cmbPais.SelectedValue;
                                                    mefe.CODIGOADUANA = cmbAduanaDestino.SelectedValue;
                                                    mefe.RANGOINICIAL = mef.RANGOINICIAL;
                                                    mefe.RANGOFINAL = mef.RANGOFINAL;
                                                    mefe.CANTIDAD = mef.CANTIDAD;
                                                    mefe.FECHA = DateTime.Now.ToString();
                                                    mefe.CODTIPOMOVIMIENTO = "1";   //1 Envío, 0 Compra
                                                    mefe.CODESTADO = "0";   //Enviado
                                                    mefe.IDUSUARIO = Session["IdUsuario"].ToString();
                                                    mefe.IDCOMPRA = mef.IDCOMPRA;
                                                    mefe.commitLine();
                                                    mefe.actualizar();

                                                    //guardar nuevo registro con las series que se envian a la otra aduana en traslados
                                                    tef.loadTrasladosEspeciesFiscales("-1");
                                                    tef.newLine();
                                                    tef.IDENVIO = idMovimientoEnvio;
                                                    tef.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                                                    tef.CODPAIS = cmbPais.SelectedValue;
                                                    tef.CODIGOADUANAORIGEN = cmbAduanaOrigen.SelectedValue;
                                                    tef.CODIGOADUANADESTINO = cmbAduanaDestino.SelectedValue;
                                                    tef.RANGOINICIAL = mefe.RANGOINICIAL;
                                                    tef.RANGOFINAL = mefe.RANGOFINAL;
                                                    tef.CANTIDAD = mefe.CANTIDAD;
                                                    tef.FECHA = DateTime.Now.ToString();
                                                    tef.IDUSUARIO = mefe.IDUSUARIO;
                                                    tef.IDMOVIMIENTO = mef.IDMOVIMIENTO;
                                                    tef.commitLine();
                                                    tef.actualizar();

                                                    //nuevo
                                                    rangoInicial = tef.RANGOINICIAL;
                                                    rangoFinal = tef.RANGOFINAL;
                                                    //

                                                    if (cmbEspecieFiscal.SelectedValue == "DVA")
                                                    {
                                                        efc.loadEspeciesFiscalesClientesXIdMov(mef.IDMOVIMIENTO);
                                                        if (efc.totalRegistros > 0)
                                                        {
                                                            //string idCliente = efc.IDCLIENTE;
                                                            ////guardar nuevo registro con las series que se envian al cliente
                                                            //efc.loadEspeciesFiscalesClientesXIdMov("-1");
                                                            //efc.newLine();
                                                            //efc.IDMOVIMIENTO = idMovimientoEnvio;
                                                            //efc.IDCLIENTE = idCliente;
                                                            //efc.OBSERVACION = "";
                                                            //efc.commitLine();
                                                            //efc.actualizar();
                                                        }
                                                    }

                                                    cantidadSolicitada -= mef.CANTIDAD;
                                                    cantidadDisponible -= mef.CANTIDAD;
                                                    mef.regSiguiente();
                                                }
                                            }
                                            //registrarMensaje("Envío de " + cmbEspecieFiscal.Text + " con rango del " + cef.RANGOINICIAL + " al " + cef.RANGOFINAL + " realizado exitosamente");
                                            registrarMensaje("Envío de " + cmbEspecieFiscal.Text + " del " + rangoInicial + " al " + rangoFinal + " realizado exitosamente");
                                            limpiarControles();
                                            //mpEspeciesFiscales.SelectedIndex = 1;
                                            //llenarBitacora("Se ingresó la compra de " + cmbEspecieFiscal.Text + " con un rango del " + cef.RANGOINICIAL + " al " + cef.RANGOFINAL, cef.IDUSUARIO, nuevoId);
                                            //rgRangoEspeciesFiscales.Rebind();
                                            rgRangoEspeciesFiscales.Rebind();
                                        }
                                        else
                                            registrarMensaje("La cantidad solicitada no esta disponible, solo existen " + cantidad + " series de esta especie fiscal");
                                    }
                                    else
                                        registrarMensaje("No existen series de esta especie fiscal en la aduana origen");
                                }
                                else
                                    registrarMensaje("La cantidad debe ser mayor que cero");
                            }
                            else
                                registrarMensaje("La cantidad no puede estar vacía");
                        }
                        else
                            registrarMensaje("La especie fiscal no puede estar vacía");
                    }
                    else
                        registrarMensaje("La aduana destino no puede estar vacía");
                }
                else
                    registrarMensaje("La aduana origen no puede estar vacía");
            }
            else
                registrarMensaje("País no puede estar vacío");
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "EspeciesFiscales");
        }
        finally
        {
            desconectar();
        }
    }

    protected void btnLimpiar_Click(object sender, ImageClickEventArgs e)
    {
        limpiarControles();
    }

    private void limpiarControles()
    {
        try
        {
            cargarDatosInicioRangos();
            //loadAduanasOrigen();
            cmbAduanaOrigen.ClearSelection();
            cmbAduanaDestino.ClearSelection();
            cmbEspecieFiscal.ClearSelection();
            txtCantidad.Text = "";
            llenarGridRangoEspeciesFiscales();
        }
        catch { }
    }
    #endregion

    protected void cmbAduanaOrigen_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        loadAduanasDestino();
        loadEspeciesFiscales();
        llenarGridRangoEspeciesFiscales();
    }

    protected void cmbEspecieFiscal_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        llenarGridRangoEspeciesFiscales();
    }

    protected void cmbAduanaDestino_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        loadEspeciesFiscales();
        llenarGridRangoEspeciesFiscales();
    }
}