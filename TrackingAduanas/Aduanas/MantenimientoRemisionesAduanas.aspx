﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="MantenimientoRemisionesAduanas.aspx.cs" Inherits="FcJefeDeAduana" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    
                    <telerik:RadTabStrip ID="RadTabStrip1" runat="server" SelectedIndex="1" MultiPageID="RadMantenimientoRemisiones">
                    <Tabs>
                        <telerik:RadTab runat="server" Text="Remisiones" PageViewID="RadPageView1" />
                        <telerik:RadTab runat="server" Text="Clientes Permitidos" PageViewID="RadClientesAdmitidos" Selected="True" />
                    </Tabs>
                </telerik:RadTabStrip>
                    <telerik:RadMultiPage ID="RadMantenimientoRemisiones" Runat="server" SelectedIndex="1">
                       
                        <telerik:RadPageView ID="RadPageView1" runat="server">
                            <telerik:RadGrid ID="rgAsistenteOperaciones" runat="server" AllowFilteringByColumn="True" 
                    AllowSorting="True" GridLines="None" Width="100%"
                    Height="430px" OnNeedDataSource="rgAsistenteOperaciones_NeedDataSource" AllowPaging="True"
                    ShowFooter="True" ShowStatusBar="True" PageSize="20"
                    CellSpacing="0" Culture="es-ES" AutoGenerateEditColumn="True">
                    <HeaderContextMenu EnableTheming="True">
                        <CollapseAnimation Duration="200" Type="OutQuint" />
                    </HeaderContextMenu>
                    <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                        PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                        Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                    <MasterTableView CommandItemDisplay="Top" 
                        NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay instrucciones definidas."
                        GroupLoadMode="Client">
                        <CommandItemSettings ShowAddNewRecordButton="False" RefreshText=""
                            ShowExportToExcelButton="False" />
                        <RowIndicatorColumn>
                            <HeaderStyle Width="20px"></HeaderStyle>
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn>
                            <HeaderStyle Width="20px"></HeaderStyle>
                        </ExpandCollapseColumn>

                    <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
                    </EditFormSettings>
                    </MasterTableView>
                    <HeaderStyle Width="180px" />
                    <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                        <Selecting AllowRowSelect="True" />
                        <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                            DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                            DropHereToReorder="Suelte aquí para Re-Ordenar" />
                        <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
                    </ClientSettings>
                    <FilterMenu EnableTheming="True">
                        <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                    </FilterMenu>
                    <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                    <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
                </telerik:RadGrid>
                    </telerik:RadPageView>
                        <telerik:RadPageView  Id="RadClientesAdmitidos" runat="server">
                            <telerik:RadGrid ID="RadGClientes" runat="server" CellSpacing="0" Culture="es-ES" GridLines="None" AutoGenerateColumns="False" OnNeedDataSource="RadGClientes_NeedDataSource" AllowFilteringByColumn="True" AllowPaging="True">
                                <MasterTableView >
                                    <CommandItemSettings ShowExportToPdfButton="True" />
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="CodigoCliente" FilterControlAltText="Filter CodigoCliente column" HeaderText="Codigo Sap" UniqueName="CodigoCliente" Display="False">
                                        </telerik:GridBoundColumn>

                                         <telerik:GridBoundColumn DataField="PermitirRemisiones" FilterControlAltText="Filter PermitirRemisiones column" HeaderText="PermitirRemisiones" UniqueName="PermitirRemisiones" Visible="false">
                                        </telerik:GridBoundColumn>

                                            <telerik:GridBoundColumn DataField="Nombre" FilterControlAltText="Filter Nombre column" HeaderText="Nombre Cliente" UniqueName="Nombre">
                                        </telerik:GridBoundColumn>


                                        <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn2 column"   UniqueName="Esquema" AllowFiltering="False"   HeaderText="Permite Remsiones" >
                                            <ItemTemplate >
                                                <telerik:RadButton ID="ImageButton1" runat="server" CommandName="habilitar"                                             
                                                  
                                                    CommandArgument='<%#Eval("CodigoCliente").ToString() %>'
                                                    Text='<%# (Eval("PermitirRemisiones").ToString() == "0" ? "Permitir" : "Denegar") %>'  
                                                    Icon-PrimaryIconUrl='<%# (Eval("PermitirRemisiones").ToString() == "0" ? "~/Images/16/check2_16.png" : "~/Images/16/delete2_16.png") %>'
                                                        OnCommand="ImageButton3_Command"                                         
                                                        Width="100px"  >
                                                    <Icon />
                                                        </telerik:RadButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                              
                                      <%--           <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn2 column"
                                                                   UniqueName="Cambiar" AllowFiltering="False"   HeaderText="Esquema Tramite" >
                                                    <ItemTemplate >
                                                        <telerik:RadButton ID="ImageButton3" runat="server" CommandName="habilitar"                                             
                                         
                                                            CommandArgument='<%#( Eval("CodigoCliente").ToString())"%>'
                                                            ToolTip='<%# (Eval("Estado").ToString() == "1" ? "Desactivar" : "Habilitar") %>'    Text='<%# (Eval("Estado").ToString() == "1" ? "Desactivar" : "Habilitar") %>'  
                                                            Icon-PrimaryIconUrl='<%# (Eval("Estado").ToString() == "0" ? "~/Images/16/check2_16.png" : "~/Images/16/delete2_16.png") %>'
                                                             OnCommand="ImageButton3_Command"                                         
                                                             Width="100px"  >
                                                            <Icon />
                                                             </telerik:RadButton>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>--%>

                                        
                                    

                                     
                                    </Columns>
                                </MasterTableView>
                            </telerik:RadGrid>
                        </telerik:RadPageView>
                </telerik:RadMultiPage>
                    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <%-- ClientEvents-OnRequestStart="requestStart"--%>
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgAsistenteOperaciones">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgAsistenteOperaciones" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnReasignar">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="btnReasignar" LoadingPanelID="LoadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="Timer1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgAsistenteOperaciones" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>


        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>
