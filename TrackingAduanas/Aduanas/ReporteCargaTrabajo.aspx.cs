﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Toolkit.Core.Extensions;
using System.Data;
using Aduana.Dto;
using AduanasNuevo.Repository;



public partial class ReporteCargaTrabajo : Utilidades.PaginaBase
{
    public List<DetalleInstruccionesCargaTrabajo> instruccionesDetalle = new List<DetalleInstruccionesCargaTrabajo>();
    DateTime fechaInicio = new DateTime();
    DateTime fechaFinal = new DateTime();
    private GrupoLis.Login.Login logAppAduanas;
    private List<UsuarioCargaTrabajo> listaUsuarios = new List<UsuarioCargaTrabajo>();
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "ReporteHagamodaPromoda";
        base.OnLoad(e);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        DetailWindow.Windows.Clear();
        SetGridFilterMenu(this.rgReporteCargaTrabajo.FilterMenu);
        rgReporteCargaTrabajo.FilterItemStyle.Font.Size = FontUnit.XSmall;
        rgReporteCargaTrabajo.FilterMenu.Font.Size = FontUnit.XXSmall;

        if(!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Carga de Trabajo.", "Reporte Carga de Trabajo.");
        }
    }

    protected void btnGenerarReporte_OnClick(object sender, ImageClickEventArgs e)
    {
       
        if (rdMesInicio.SelectedDate != null && rdMesFin.SelectedDate != null)
        {
            fechaInicio = new DateTime(rdMesInicio.SelectedDate.Value.Year, rdMesInicio.SelectedDate.Value.Month, 1);
            var mesFin = new DateTime(rdMesFin.SelectedDate.Value.Year, rdMesFin.SelectedDate.Value.Month + 1, 1);
            fechaFinal = mesFin.AddDays(-1);
            llenarGrid(fechaInicio, fechaFinal);
        }         
    }

    public void llenarGrid(DateTime fechaInicio, DateTime fechaFinal)
    {
        var cargaTrabajo = new ReporteCargaDeTrabajoRepository();
        //var l = cargaTrabajo.CargarGridPrincipal(fechaInicio, fechaFinal);
        var l = cargaTrabajo.CargarGridPrincipal(fechaInicio, fechaFinal);
        this.rgReporteCargaTrabajo.DataSource = l;
        this.rgReporteCargaTrabajo.DataBind();
        ColorearFilas();
     }


    protected void ColorearFilas()
    {
        foreach (GridDataItem item in rgReporteCargaTrabajo.MasterTableView.Items)
        {
            GridDataItem dataitem = (GridDataItem)item;
            TableCell cell1 = dataitem["RolReal"];
            var RolReal = cell1.Text.ToString();
            TableCell cell2 = dataitem["Eliminado"];
            var Eliminado = cell2.Text.ToString();

            dataitem.BackColor = System.Drawing.Color.LawnGreen;

            if (RolReal == "3")
            {
                dataitem.BackColor = System.Drawing.Color.White;
            }
            if (RolReal == "8")
            {
                dataitem.BackColor = System.Drawing.Color.White;
            }
            if (RolReal == "9")
            {
                dataitem.BackColor = System.Drawing.Color.White;
            }
            if (RolReal == "22")
            {
                dataitem.BackColor = System.Drawing.Color.White;
            }
            if (RolReal == "23")
            {
                dataitem.BackColor = System.Drawing.Color.White;
            }
            if (RolReal == "24")
            {
                dataitem.BackColor = System.Drawing.Color.White;
            }
            if (Eliminado == "1")
            {
                dataitem.ForeColor = System.Drawing.Color.Red;
            }
        }
    }

    protected void rgReporteCargaTrabajo_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
    {
        GridDataItem parentItem = e.DetailTableView.ParentItem as GridDataItem;
        var IdUsuario = parentItem["IdUsuario"].Text;

        List<DetalleInstruccionesCargaTrabajo> instruccionesDeUsuario = new List<DetalleInstruccionesCargaTrabajo>();
        var context = new AduanasDataContext();

        var rolUsuario =  context.Usuarios.Join(context.UsuariosRoles,
            u => u.IdUsuario,
            ur => ur.IdUsuario,
            (u, ur) => new
            {
                user = u,
                roles = ur
            }
            )
            .Where(u => u.user.IdUsuario == IdUsuario.ToDecimal() 
                 && u.roles.Eliminado == '0').ToList();

        if (IdUsuario == "0")
        {
            instruccionesDeUsuario.AddRange(CargarDetalleOficialesCuenta(0));
        }
        else
        {
            for (int i = 0; i < rolUsuario.Count; i++)
            {
                var rol = rolUsuario[i].roles.IdRol;
                if (rol == 3 || rol == 22 || rol == 23 || rol == 24)
                {
                    var id = rolUsuario[i].user.IdUsuario;
                    instruccionesDeUsuario.AddRange(CargarDetalleTramitadores(id));
                }
                else 
                {
                    var id = rolUsuario[i].user.IdUsuario;
                    instruccionesDeUsuario.AddRange(CargarDetalleOficialesCuenta(id));
                }
            }
        }
        
     
        for (int i = 0; i < instruccionesDeUsuario.Count; i++)
        {
            var Dia = instruccionesDeUsuario[i].Dia.ToString();
            if (Dia == "1" || Dia == "Monday")
            {
                instruccionesDeUsuario[i].Dia = "Lunes";
            }
            else if (Dia == "2" || Dia == "Tuesday")
            {
                instruccionesDeUsuario[i].Dia = "Martes";
            }
            else if (Dia == "3" ||  Dia == "Wednesday" )
            {
                instruccionesDeUsuario[i].Dia = "Miercoles";
            }
            else if (Dia == "4" ||  Dia == "Thursday")
            {
                instruccionesDeUsuario[i].Dia = "Jueves";
            }
            else if (Dia == "5" ||  Dia == "Friday")
            {
                instruccionesDeUsuario[i].Dia = "Viernes";
            }
            else if (Dia == "6" ||  Dia == "Saturday")
            {
                instruccionesDeUsuario[i].Dia = "Sabado";
            }
            else if (Dia == "0" ||  Dia == "Sunday")
            {
                instruccionesDeUsuario[i].Dia = "Domingo";
            }
        }
        e.DetailTableView.DataSource = instruccionesDeUsuario;
    }
     public List<DetalleInstruccionesCargaTrabajo>  CargarDetalleTramitadores( decimal idUsuario)
     {
         var context = new AduanasDataContext();
         var fechaInicio = new DateTime(rdMesInicio.SelectedDate.Value.Year, rdMesInicio.SelectedDate.Value.Month, 1);
         var mesFin = new DateTime(rdMesFin.SelectedDate.Value.Year, rdMesFin.SelectedDate.Value.Month + 1, 1);
         var fechaFinal = mesFin.AddDays(-1);

         var instruccionesDetallePorUsuario1 = context.Instrucciones.Join(context.Clientes,
                             i => i.IdCliente,
                             c => c.CodigoSAP,
                             (i, c) => new
                             {
                                 ins = i,
                                 cliente = c
                             }
                             ).Where(w => w.ins.IdEstadoFlujo == "99" && w.ins.CodEstado != "100"
                                       && (w.ins.FechaFinalFlujo >= fechaInicio
                                       && w.ins.FechaFinalFlujo <= fechaFinal)
                                       && w.ins.CodRegimen != "9999"
                                       && w.ins.CodRegimen != "CB"
                                       && w.cliente.Eliminado == '0'
                                       && w.ins.Tramitador == idUsuario)
                           .Select(s => new DetalleInstruccionesCargaTrabajo
                           {
                               Cliente = s.cliente.Nombre,
                               Instruccion = s.ins.IdInstruccion,
                               Fecha = s.ins.FechaFinalFlujo.Value.ToShortDateString(),
                               Dia = s.ins.FechaFinalFlujo.Value.DayOfWeek.ToString(),
                               Regimen = s.ins.CodRegimen,
                               Producto = s.ins.Producto,
                               CanalSelectividad = s.ins.Color
                           }).ToList();

         var instruccionesDetallePorUsuario2 = context.TiemposFlujoCarga.Join(context.Instrucciones,
            tfc => tfc.IdInstruccion,
            i => i.IdInstruccion,
            (tfc, i) => new
            {
                tiempoFC = tfc,
                ins = i
            }
            ).Join(context.Clientes,
            i => i.ins.IdCliente,
            c => c.CodigoSAP,
            (i, c) => new
            {
                ins = i,
                cliente = c
            }).Where(tfc => tfc.ins.tiempoFC.IdEstado == "9" && tfc.ins.tiempoFC.Eliminado == false
                       && (tfc.ins.tiempoFC.Fecha >= fechaInicio
                       && tfc.ins.tiempoFC.Fecha <= fechaFinal)
                       && tfc.ins.ins.CodEstado != "100" && tfc.ins.ins.IdEstadoFlujo == "99"
                       && (tfc.ins.ins.FechaFinalFlujo >= fechaInicio
                       && tfc.ins.ins.FechaFinalFlujo <= fechaFinal)
                       && tfc.ins.ins.CodRegimen != "9999"
                       && tfc.ins.ins.CodRegimen != "CB"
                       && tfc.ins.tiempoFC.IdUsuario == idUsuario).Select(s => new DetalleInstruccionesCargaTrabajo
                       {
                           Cliente = s.cliente.Nombre,
                           Instruccion = s.ins.ins.IdInstruccion,
                           Fecha = s.ins.ins.FechaFinalFlujo.Value.ToShortDateString(),
                           Dia = s.ins.ins.FechaFinalFlujo.Value.DayOfWeek.ToString(),
                           Regimen = s.ins.ins.CodRegimen,
                           Producto = s.ins.ins.Producto,
                           CanalSelectividad = s.ins.ins.Color
                       }).ToList();

         List<DetalleInstruccionesCargaTrabajo> instruccionesDeUsuario = new List<DetalleInstruccionesCargaTrabajo>();
         instruccionesDeUsuario = instruccionesDetallePorUsuario1;
         instruccionesDeUsuario.AddRange(instruccionesDetallePorUsuario2);

         return instruccionesDeUsuario;
     }

     public List<DetalleInstruccionesCargaTrabajo> CargarDetalleOficialesCuenta(decimal idUsuario)
     {
         var context = new AduanasDataContext();
         List<DetalleInstruccionesCargaTrabajo> instruccionesDeUsuario = new List<DetalleInstruccionesCargaTrabajo>();
         List<Instrucciones> instrucciones = new List<Instrucciones>();

         var fechaInicio = new DateTime(rdMesInicio.SelectedDate.Value.Year, rdMesInicio.SelectedDate.Value.Month, 1);
         var mesFin = new DateTime(rdMesFin.SelectedDate.Value.Year, rdMesFin.SelectedDate.Value.Month + 1, 1);
         var fechaFinal = mesFin.AddDays(-1);
         
         var aduanas = context.Aduanas.Where(w => w.CodEstado == "0" && w.CodPais == "H").ToList();
         var clientes = context.Clientes.Where(w => w.Eliminado == '0').ToList();

         if(idUsuario == 0)
         {
             instrucciones = context.Instrucciones.Where(w => w.CodEstado != "100" && w.IdEstadoFlujo == "99"
                                                        && (w.FechaFinalFlujo >= fechaInicio
                                                        && w.FechaFinalFlujo <= fechaFinal)
                                                        && w.CodRegimen != "9999"
                                                         && w.CodRegimen != "CB"
                                                        && w.IdOficialCuenta == null).ToList();
         }
         else
         {
             instrucciones = context.Instrucciones.Where(w => w.CodEstado != "100" && w.IdEstadoFlujo == "99"
                                                        && (w.FechaFinalFlujo >= fechaInicio
                                                        && w.FechaFinalFlujo <= fechaFinal)
                                                        && w.CodRegimen != "9999"
                                                        && w.CodRegimen != "CB"
                                                        && w.IdOficialCuenta == idUsuario).ToList();
         }
          

         instruccionesDeUsuario = instrucciones.Join(clientes, 
             i => i.IdCliente, 
             c => c.CodigoSAP, 
             (i,c) => new 
             {
                 ins = i,
                 cliente = c
             }
             ).Join(aduanas, 
             i => i.ins.CodigoAduana, 
             a => a.CodigoAduana, 
             (i,a) => new 
             {
                 ins = i,
                 aduana = a
             }
             ).Select(s => new DetalleInstruccionesCargaTrabajo
                   {
                       Cliente = s.ins.cliente.Nombre,
                       Instruccion = s.ins.ins.IdInstruccion,
                       Fecha = s.ins.ins.FechaFinalFlujo.Value.ToShortDateString(),
                       Dia = s.ins.ins.FechaFinalFlujo.Value.DayOfWeek.ToString(),
                       Regimen = s.ins.ins.CodRegimen,
                       Producto = s.ins.ins.Producto,
                       CanalSelectividad = s.ins.ins.Color
                   }).ToList();

         return instruccionesDeUsuario;
     }

     protected void MostrarDiaMasCargado_Click(object sender, EventArgs e)
     {
          var context = new AduanasDataContext();
          List<DetalleInstruccionesCargaTrabajo> instruccionesDeUsuario = new List<DetalleInstruccionesCargaTrabajo>();
          string urlBase = "ReporteCargaTrabajoDetalle.aspx";

          LinkButton link = (LinkButton)sender;
          string idUsuario = link.CommandArgument.ToString();
          string FechaTemp = link.CommandName.ToString();
          string Fecha = FechaTemp.Remove(10).Trim();

          var rolUsuario = context.Usuarios.Join(context.UsuariosRoles,
             u => u.IdUsuario,
             ur => ur.IdUsuario,
             (u, ur) => new
             {
                 user = u,
                 roles = ur
             }
             )
             .Where(u => u.user.IdUsuario == idUsuario.ToDecimal()
                  && u.roles.Eliminado == '0').ToList();

          if (idUsuario == "0")
          {
              urlBase += "?Fecha=" + Fecha;
          }
          else
          {
              for (int i = 0; i < rolUsuario.Count; i++)
              {
                  var rol = rolUsuario[i].roles.IdRol;
                  if (rol == 3 || rol == 22 || rol == 23 || rol == 24)
                  {
                      var id = rolUsuario[i].user.IdUsuario;
                      urlBase += "?id=" + id + "&Fecha="+Fecha+"&Usuario="+"Tramitador";
                      break;
                  }
                  else
                  {
                      var ins = context.Instrucciones.Where(w => w.CodEstado != "100" && w.IdEstadoFlujo == "99"
                                                       && w.FechaFinalFlujo.Value.Date == Convert.ToDateTime(Fecha)
                                                       && w.CodRegimen != "9999"
                                                       && w.CodRegimen != "CB"
                                                       && w.IdOficialCuenta == idUsuario.ToDecimal()
                                                       && w.FechaFinalFlujo.Value.Date == Convert.ToDateTime(Fecha)).ToList();
                      if(ins.Count != 0)
                      {
                          var id = rolUsuario[i].user.IdUsuario;
                          urlBase += "?id=" + id + "&Fecha=" + Fecha;
                      }                    
                  }
              }
          }

          var window1 = new RadWindow
          {
              NavigateUrl = urlBase,
              VisibleOnPageLoad = true,
              ID = "Tramites",
              Width = 1300,
              Height = 500,
              Animation = WindowAnimation.FlyIn,
              DestroyOnClose = true,
              VisibleStatusbar = false,
              Behaviors = WindowBehaviors.Close,
              Modal = true,
              OnClientClose = "OnClientClose"
          };
          DetailWindow.Windows.Add(window1);
         }

     protected void rgReporteCargaTrabajo_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
     {
         if(!e.IsFromDetailTable)
         {
             if (rdMesInicio.SelectedDate != null || rdMesFin.SelectedDate != null)
             {
                 fechaInicio = new DateTime(rdMesInicio.SelectedDate.Value.Year, rdMesInicio.SelectedDate.Value.Month, 1);
                 var mesFin = new DateTime(rdMesFin.SelectedDate.Value.Year, rdMesFin.SelectedDate.Value.Month + 1, 1);
                 fechaFinal = mesFin.AddDays(-1);
                 var cargaTrabajo = new ReporteCargaDeTrabajoRepository();
                 var l = cargaTrabajo.CargarGridPrincipal(fechaInicio, fechaFinal);
                 this.rgReporteCargaTrabajo.DataSource = l;
                 ColorearFilas();
             }      
         }         
     }

     private void ConfigureExport(string nombre)
     {
         rgReporteCargaTrabajo.AllowFilteringByColumn = false;
         String filename = nombre + "_" + DateTime.Now.ToShortDateString();
         rgReporteCargaTrabajo.GridLines = GridLines.Both;
         rgReporteCargaTrabajo.ExportSettings.FileName = filename;
         rgReporteCargaTrabajo.ExportSettings.IgnorePaging = true;
         rgReporteCargaTrabajo.ExportSettings.OpenInNewWindow = false;
         rgReporteCargaTrabajo.ExportSettings.ExportOnlyData = true;
     }

     protected void rgReporteCargaTrabajo_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
     {
         try
         {
             conectar();
               if (rgReporteCargaTrabajo.Items.Count <= 0 & e.CommandName == RadGrid.ExportToExcelCommandName)
                 e.Canceled = true;
             else if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
             {
                 ConfigureExport("ReporteCargaDeTrabajo");
             }
         }
         catch (Exception)
         { }
     }

}
