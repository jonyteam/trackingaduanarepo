﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .mainDiv
        {
            height: 508px;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <div class="mainDiv">
        <table>
            <tr style="height: 40px">
                <td colspan="3">
                </td>
            </tr>
            <tr>
                <td style="width: 20%">
                </td>
                <td style="width: 60%">
                    <asp:Panel ID="RotatorWrapper" runat="server" CssClass="rotNoButtonsBack">
                        <telerik:RadRotator ID="RadRotator1" runat="server" Width="100%" ItemWidth="300"
                            Height="350px" ItemHeight="220" ScrollDuration="500" FrameDuration="1500" PauseOnMouseOver="false"
                            RotatorType="CarouselButtons">
                            <ItemTemplate>
                                <asp:Image ID="Image1" runat="server" ImageUrl='<%# Bind("ImageUrl") %>' AlternateText="Grupo Vesta" />
                            </ItemTemplate>
                        </telerik:RadRotator>
                    </asp:Panel>
                </td>
                <td style="width: 20%">
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
