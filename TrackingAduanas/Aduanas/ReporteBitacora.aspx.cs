﻿using ObjetosDto;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Telerik.Web.UI;

public partial class ReporteEventos : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Reporte Eventos";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        RadWindowManager1.Windows.Clear();
        SetGridFilterMenu(RadGrid1.FilterMenu);
        RadGrid1.Skin = Skin;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Mantenimiento Unidades", "Bitacora");
            
            //if (!(Anular || Ingresar || Modificar))
            //    redirectTo("Inicio.aspx");
        }
    }
    public override bool CanGoBack { get { return false; } }
    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void SetVisible(TableCellCollection cells, String commandName, bool visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is ImageButton)
                {
                    ImageButton btn = (ImageButton)control;

                    if (btn.CommandName == commandName)
                        btn.Visible = visible;
                }
            }
        }
    }


     protected void RadButton1_Click(object sender, EventArgs e)
    {
        loadGridEventos();
        loadGridEventos2();
        this.ImageButton1.Visible = true;
        this.ImageButton2.Visible = true;
    }
    #region Consulta
    protected Object loadGridEventos()
    {

        try
        {
            string hojaruta;
            hojaruta = this.RadTextBox1.Text;
            conectar();
            Eventos bo = new Eventos(logApp);
            bo.mostrardatos(hojaruta);
            RadGrid1.DataSource = bo.TABLA;
            RadGrid1.DataBind();
        }

        catch { }
        finally
        {
            desconectar();
        }

        return null;
    }
    protected Object loadGridEventos2()
    {

        try
        {
            string hojaruta;
            hojaruta = this.RadTextBox1.Text;
            conectar();
            Eventos bo = new Eventos(logApp);
            Eventos bo2 = new Eventos(logApp);
            bo.mostrardatos2(hojaruta);
            RadGrid2.DataSource = bo.TABLA;
            RadGrid2.DataBind();
            RadGrid2.Visible=true;

            bo.ImpuestosAduanas(hojaruta);
            //RadGrid3.DataSource = bo.TABLA;
            //RadGrid3.Visible = true;
            //RadGrid3.DataBind();

            bo2.ImpuestosEsquema(hojaruta);
            //RadGrid4.DataSource = bo2.TABLA;
            //RadGrid4.Visible = true;
           // RadGrid4.DataBind();
            //this.Label4.Text = registrarMensaje;
        }

        catch { }
        finally
        {
            desconectar();
        }

        return null;
    }

    #endregion

    #region exportgrid1
    protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName)
                ConfigureExport();
        }
        catch (Exception)
        { }
    }

    private void ConfigureExport()
    {
        String filename = "Reporte_" + DateTime.Now.ToShortDateString();
        RadGrid1.ExportSettings.FileName = filename;
        RadGrid1.ExportSettings.ExportOnlyData = true;
        RadGrid1.ExportSettings.IgnorePaging = true;
        RadGrid1.ExportSettings.OpenInNewWindow = true;
        RadGrid1.MasterTableView.ExportToExcel();
    }
#endregion

    #region exportgrid2
    private void ConfigureExport2()
    {
        String filename = "Reporte_" + DateTime.Now.ToShortDateString();
        RadGrid2.ExportSettings.FileName = filename;
        RadGrid2.ExportSettings.ExportOnlyData = true;
        RadGrid2.ExportSettings.IgnorePaging = true;
        RadGrid2.ExportSettings.OpenInNewWindow = true;
        RadGrid2.MasterTableView.ExportToExcel();
    }
    protected void RadGrid2_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName)
                ConfigureExport();
        }
        catch (Exception)
        { }

    }
    #endregion

    protected void ImageButton1_Click(object sender, EventArgs e)
    {

        SessionFileShare.CreateDownload(this.RadTextBox1.Text, page: this);
    }

    protected void ImageButton2_Click(object sender, EventArgs e)
    {
      //  AbrirVentana("http://sarah.dei.gob.hn:8082/sarah/QueryDua.do?documentId=160004101277H&validatorKey=c776094dade3f533b787fc3cb3c533d8");
        AbrirVentana("DeclaracionAduaneraSarah.aspx?IdHojaRuta=" + this.RadTextBox1.Text);
    }


    private void AbrirVentana(string UrlBoletin)
    {
        var window1 = new RadWindow
        {
            NavigateUrl = UrlBoletin,
            VisibleOnPageLoad = true,
            ID = "Estados",
            Width = 740,
            Height = 480,
            Animation = WindowAnimation.FlyIn,
            DestroyOnClose = true,
            VisibleStatusbar = false,
            Behaviors = WindowBehaviors.Close,
            Modal = true,
            OnClientClose = "OnClientClose"

        };
        RadWindowManager1.Windows.Add(window1);
    }

}
