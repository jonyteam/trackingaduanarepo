﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="MantenimientoAduanas.aspx.cs" Inherits="MantenimientoAduanas" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadGrid ID="rgAduanas" runat="server" AllowFilteringByColumn="True" AllowSorting="True"
        AutoGenerateColumns="False" GridLines="None" Width="100%" Height="420px" OnNeedDataSource="rgAduanas_NeedDataSource"
        OnUpdateCommand="rgAduanas_UpdateCommand" OnDeleteCommand="rgAduanas_DeleteCommand"
        OnInsertCommand="rgAduanas_InsertCommand" AllowPaging="True" ShowFooter="True"
        ShowStatusBar="True" PageSize="20" OnInit="rgAduanas_Init" CellSpacing="0" Culture="es-ES">
        <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
            PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
            Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
        <MasterTableView DataKeyNames="CodigoAduana,CodPais" CommandItemDisplay="TopAndBottom"
            NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay Aduanas definidas."
            GroupLoadMode="Client">
            <CommandItemSettings AddNewRecordText="Agregar Aduana" RefreshText="Volver a Cargar Datos" />
            <RowIndicatorColumn>
                <HeaderStyle Width="20px"></HeaderStyle>
            </RowIndicatorColumn>
            <ExpandCollapseColumn>
                <HeaderStyle Width="20px"></HeaderStyle>
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridEditCommandColumn AutoPostBackOnFilter="True" CancelText="Cancelar"
                    EditText="Editar" InsertText="Insertar" UpdateText="Grabar" ButtonType="ImageButton"
                    HeaderText="Editar">
                    <HeaderStyle Width="5%" />
                    <ItemStyle Width="5%" />
                </telerik:GridEditCommandColumn>
                <telerik:GridButtonColumn ConfirmText="¿Esta seguro que desea eliminar esta Aduana?"
                    ConfirmDialogType="RadWindow" ConfirmTitle="Eliminar Aduana" UniqueName="DeleteColumn"
                    ButtonType="ImageButton" CommandName="Delete" HeaderText="Eliminar">
                    <HeaderStyle Width="5%" />
                    <ItemStyle Width="5%" />
                </telerik:GridButtonColumn>
                <telerik:GridBoundColumn DataField="Pais" HeaderText="Pais" UniqueName="Pais" FilterControlWidth="80%">
                    <HeaderStyle Width="16%" />
                    <ItemStyle Width="16%" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CodigoAduana" HeaderText="CodAduana" UniqueName="CodAduana"
                    FilterControlWidth="80%">
                    <HeaderStyle Width="16%" />
                    <ItemStyle Width="16%" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Aduana" HeaderText="Aduana" UniqueName="Aduana"
                    FilterControlWidth="80%" Aggregate="Count" FooterText="Cantidad: ">
                    <HeaderStyle Width="64%" />
                    <ItemStyle Width="64%" />
                </telerik:GridBoundColumn>
            </Columns>
            <GroupByExpressions>
                <telerik:GridGroupByExpression>
                    <SelectFields>
                        <telerik:GridGroupByField FieldAlias="Pais" FieldName="Pais"></telerik:GridGroupByField>
                    </SelectFields>
                    <GroupByFields>
                        <telerik:GridGroupByField FieldAlias="Pais" FieldName="Pais"></telerik:GridGroupByField>
                    </GroupByFields>
                </telerik:GridGroupByExpression>
            </GroupByExpressions>
            <EditFormSettings EditFormType="Template" CaptionDataField="Centros" CaptionFormatString="">
                <EditColumn UniqueName="EditCommandColumn1">
                </EditColumn>
                <FormTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# ((Container is GridEditFormInsertItem) ? "Agregar Nueva Aduana" : "Editar Aduana Existente") %>'
                        Font-Size="Medium" Font-Bold="true" />
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 10%">
                            </td>
                            <td style="width: 90%">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 10%">
                                <asp:Label ID="lblCodAduana" runat="server" Text="Código Aduana" Font-Bold="true" />
                            </td>
                            <td style="width: 90%">
                                <telerik:RadTextBox ID="edCodAduana" runat="server" Text='<%# Bind("CodigoAduana") %>'
                                    EmptyMessage="Escriba el Código de Aduana aquí" Width="50%" Enabled='<%# (Container is GridEditFormInsertItem) ? true : false %>'
                                    Font-Bold='<%# (Container is GridEditFormInsertItem) ? false : true %>' />
                                <asp:RequiredFieldValidator ID="rfCodAduana" runat="server" EnableClientScript="true"
                                    Display="Dynamic" ErrorMessage="El Código de Aduana no puede estar vacío" ControlToValidate="edCodAduana" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 10%">
                                <asp:Label ID="lbDescripcion" runat="server" Text="Aduana" Font-Bold="true" />
                            </td>
                            <td style="width: 90%">
                                <telerik:RadTextBox ID="edAduana" runat="server" Text='<%# Bind("Aduana") %>' EmptyMessage="Escriba la Aduana aquí"
                                    Width="50%" />
                                <asp:RequiredFieldValidator ID="rfAduana" runat="server" EnableClientScript="true"
                                    Display="Dynamic" ErrorMessage="La Aduana no puede estar vacía" ControlToValidate="edAduana" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 10%">
                                <asp:Label ID="lblPais" runat="server" Text="Pais" Font-Bold="true" />
                            </td>
                            <td style="width: 90%">
                                <telerik:RadComboBox ID="edPais" runat="server" Skin='<%# Skin %>' DataTextField="Descripcion"
                                    Filter="Contains" DataValueField="Codigo" DataSource='<%# GetPaises() %>' SelectedValue='<%# Bind("CodPais") %>'
                                    EmptyMessage="Seleccione el Pais" Width="50%" CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                    <asp:ImageButton ID="btnSave" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                        AlternateText='<%# (Container is GridEditFormInsertItem) ? "Agregar Aduana" : "Actualizar Aduana" %>'
                        ImageUrl='Images/16/check2_16.png' />
                    &nbsp
                    <asp:ImageButton ID="btnCancel" runat="server" CommandName="Cancel" AlternateText="Cancelar"
                        ImageUrl='Images/16/forbidden_16.png' CausesValidation="false" />
                </FormTemplate>
            </EditFormSettings>
        </MasterTableView>
        <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
            <Selecting AllowRowSelect="True" />
            <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                DropHereToReorder="Suelte aquí para Re-Ordenar" />
            <Scrolling AllowScroll="True" UseStaticHeaders="True" />
        </ClientSettings>
        <FilterMenu EnableTheming="True">
            <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
        </FilterMenu>
        <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
        <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
    </telerik:RadGrid>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgAduanas">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgAduanas" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>
