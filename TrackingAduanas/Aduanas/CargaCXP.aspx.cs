﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.OleDb;

using Telerik.Web.UI;
using Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using GrupoLis.Ebase;
using GrupoLis.Login;
using System.Collections.Specialized;
using System.Configuration;

public partial class GenerarArchivoSAPDinant : Utilidades.PaginaBase
{
    private GrupoLis.Login.Login logAppAduanas;
    AduanasDataContext _aduana = new AduanasDataContext();
    //string HojaRuta = "";
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Cargar CXP";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Carga CXP", "Cargar CXP");
        }
    }

    protected bool Consultar { get { return tienePermiso("Consultar"); } }

    private void ConfigureExport()
    {
        String filename = "Reporte_" + DateTime.Now.ToShortDateString();
        rgGastos.ExportSettings.FileName = filename;
        rgGastos.ExportSettings.ExportOnlyData = true;
        rgGastos.ExportSettings.IgnorePaging = true;
        rgGastos.ExportSettings.OpenInNewWindow = true;
        rgGastos.MasterTableView.ExportToExcel();
    }

    public override bool CanGoBack { get { return false; } }


    private void llenarGrid()
    {
        try
        {
            conectar();
            UsuariosBO u = new UsuariosBO(logApp);
            DetalleGastosBO DG = new DetalleGastosBO(logApp);
            if (User.IsInRole("Administradores") && User.Identity.Name == "dabonilla" || User.IsInRole("Administradores") && User.Identity.Name == "aalvarado" || User.IsInRole("Administradores") && User.Identity.Name == "lramirez" || User.IsInRole("Administradores") && User.Identity.Name == "mnapky" || User.IsInRole("Administradores") && User.Identity.Name == "gcorrales" || User.IsInRole("Administradores") && User.Identity.Name == "ycaballero" || User.IsInRole("Administradores") && User.Identity.Name == "ycaballero")
                DG.loadInstruccionesSolicitudPagoCXP();
            else if (User.IsInRole("Contabilidad"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                DG.loadInstruccionesSolicitudPagoCXPXPAIS(u.CODPAIS);
            }
            rgGastos.DataSource = DG.TABLA;

        }
        catch (Exception)
        {

        }

        finally { desconectar(); }
    }
    protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenarGrid();
    }
    #region Conexion
    private void conectarAduanas(string Pais)
    {
        if (logAppAduanas == null)
            logAppAduanas = new GrupoLis.Login.Login();
        if (!logAppAduanas.conectado)
        {
            logAppAduanas.tipoConexion = TipoConexion.SQL_SERVER;
            logAppAduanas.SERVIDOR = mParamethers.Get("Servidor2");
            if (Pais.ToString() == "S")
            {
                logAppAduanas.DATABASE = mParamethers.Get("CajaChicaSV");
            }
            if (Pais.ToString() == "N")
            {
                logAppAduanas.DATABASE = mParamethers.Get("CajaChicaNI");
            }
            if (Pais.ToString() == "G")
            {
                logAppAduanas.DATABASE = mParamethers.Get("CajaChicaGT");
            }
            logAppAduanas.USER = mParamethers.Get("User2");
            logAppAduanas.PASSWD = mParamethers.Get("Password2");
            logAppAduanas.conectar();
        }
    }

    private void desconectarAduanas()
    {
        if (logAppAduanas.conectado)
            logAppAduanas.desconectar();
    }
    #endregion
    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        writeToExcel();

    }
    protected void writeToExcel()
    {
        if (rgGastos.Items.Count > 0)
        {
            btnGuardar.Visible = false;
            string nameFile = "CargaCXPTemp.xlsx";
            string nameDest = "CargaCXP.xlsx";
            bool hayRegistro = false;
            Application.Lock();
            if (File.Exists("J:\\Files1\\Aduanas\\" + nameDest))
            {
                File.Delete("J:\\Files1\\Aduanas\\" + nameDest);
                File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
            }
            else
            {
                File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
            }
            //DIRECCION DE LA PLANTILLA
            var mTemplateFileName = @"J:\Files1\Aduanas\" + nameDest;

            var xlApp = new ApplicationClass() { DisplayAlerts = false, AskToUpdateLinks = false };
            var workbooks = xlApp.Workbooks;
            var workbook = workbooks.Open(mTemplateFileName, 2, false);

            try
            {
                conectar();
                DetalleGastosBO DG = new DetalleGastosBO(logApp);
                CargaBO C = new CargaBO(logApp);
                TipoCambioBO TC = new TipoCambioBO(logApp);
                C.UltimoResgitros();
                #region Carga
                //HOJA DE LA PLANTILLA       
                const string mInputWorkSheetName = "Facturacion";   //Hoja1
                //POSICIONAR LA HOJA A TRABAJAR
                //var inputWorkSheet = workBook.Worksheets.Cast<Excel.Worksheet>().Where(w => w.Name == m_InputWorkSheetName).FirstOrDefault();

                var inputWorkSheet = workbook.Worksheets.Cast<Worksheet>().FirstOrDefault(w => w.Name == mInputWorkSheetName);

                string HojaRuta = "", Cliente = "", IdSolicitud = "", Material = "", CuentaSAP = "", Monto = "", IVA = "", Total = "", MedioPago = "", Proveedor = "", CeCo = "", Serie = "", CodigoProveedor = "";
                int pos = 0, pos2 = 0, pos3 = 0;
                int carga = C.IDCARGA + 1;
                bool revision = false;

                foreach (GridDataItem gridItem in rgGastos.Items)
                {  // Varibales para el calculo de Impuestos 
                    double P_IVAs = 0, P_ISR = 0, vGASTO = 0, Renta = 0, TISV, Total1 = 0, Total2 = 0, Total3 = 0, SUbT2, TotAL1, costo = 0, IVAS = 0;

                    pos = pos + 2;
                    pos2 = pos + 1;
                    pos3 = pos2 + 1;
                    hayRegistro = true;
                    HojaRuta = gridItem.Cells[2].Text;
                    Cliente = gridItem.Cells[3].Text;
                    IdSolicitud = gridItem.Cells[4].Text; //+= Convert.ToDouble(gridItem.Cells[8].Text.Trim());
                    Material = gridItem.Cells[5].Text;
                    CuentaSAP = gridItem.Cells[6].Text;
                    Monto = gridItem.Cells[7].Text;
                    IVA = gridItem.Cells[8].Text;
                    Total = gridItem.Cells[9].Text;
                    MedioPago = gridItem.Cells[10].Text;
                    CodigoProveedor = gridItem["CodigoProveedor"].Text.Trim();
                    Proveedor = gridItem.Cells[11].Text;
                    CeCo = gridItem.Cells[12].Text;
                    Serie = gridItem.Cells[13].Text;
                    // GatosTracking                 
                    if (Serie == "&nbsp;")
                    {
                        Serie = "";
                    }
                    DG.LlenarGatosXId(IdSolicitud);
                    if (HojaRuta.Substring(0, 1) != "S")
                    {
                        TC.loadtasacambio(HojaRuta.Substring(0, 1));
                    }
                    else
                    {
                        ProveedoresImpuestoSBO PIS = new ProveedoresImpuestoSBO(logApp);
                        PIS.VerificarImpuesto(CodigoProveedor, DG.IDGASTO);
                        if (PIS.totalRegistros > 0)
                        {
                            vGASTO = Convert.ToDouble(Monto);
                            P_ISR = Convert.ToDouble(PIS.P_ISR) / 100;
                            P_IVAs = Convert.ToDouble(PIS.P_ISV) / 100;
                            Renta = (vGASTO * P_ISR);
                            costo = vGASTO - Renta;
                            IVAS = (vGASTO * P_IVAs);
                            Total1 = costo + Renta + IVAS;
                            Total2 = costo + IVAS;
                            revision = true;
                        }
                        else
                        {
                            //Ni idea porque siempre deberia de entrar en el primero :P
                        }
                    }
                    DG.ESTADO = "2"; //Bajada la Carga
                    if (HojaRuta.Substring(0, 1) != "S")
                    {
                        DG.TASACAMBIOVARIABLE = TC.TASACAMBIO;
                    }
                    else
                    {
                        DG.TASACAMBIOVARIABLE = 1.00;
                    }
                    DG.NUMEROCARGA = Convert.ToString(carga);
                    DG.actualizar();

                    #region INGreso Proveedor

                    inputWorkSheet.Cells.Range["A" + pos].Value = System.DateTime.Now.ToString("dd.MM.yyyy");
                    inputWorkSheet.Cells.Range["B" + pos].Value = "SA";
                    if (HojaRuta.Substring(0, 1) == "G")
                    {
                        inputWorkSheet.Cells.Range["C" + pos].Value = "GT01";
                    }
                    if (HojaRuta.Substring(0, 1) == "S")
                    {
                        inputWorkSheet.Cells.Range["C" + pos].Value = "SV01";
                    }
                    if (HojaRuta.Substring(0, 1) == "N")
                    {
                        inputWorkSheet.Cells.Range["C" + pos].Value = "NI01";
                    }
                    inputWorkSheet.Cells.Range["D" + pos].Value = System.DateTime.Now.ToString("dd.MM.yyyy");
                    inputWorkSheet.Cells.Range["E" + pos].Value = System.DateTime.Now.ToString("MM");
                    inputWorkSheet.Cells.Range["F" + pos].Value = DG.TABLA.Rows[0]["Moneda"].ToString();
                    inputWorkSheet.Cells.Range["G" + pos].Value = "1000010";
                    inputWorkSheet.Cells.Range["H" + pos].Value = "COSTO HR";
                    inputWorkSheet.Cells.Range["I" + pos].Value = Proveedor;
                    inputWorkSheet.Cells.Range["J" + pos].Value = "9095";
                    inputWorkSheet.Cells.Range["K" + pos].Value = "40";
                    //Cuenta el salvador 
                    if (HojaRuta.Substring(0, 1) == "S")
                    {
                        GatosTracking GT = new GatosTracking(logApp);
                        GT.LoadGatosXNombre(Material, HojaRuta.Substring(0, 1));
                        inputWorkSheet.Cells.Range["L" + pos].Value = GT.BALANCE;
                        /*Anotadacion mia 21/04/2015 por la forma de ingresar los gastos simpre deberian de llevar
                         * el indicador de impuesto, puesto que el impuesto va incluido pero por mientras dejaremos
                         * que Joao lo ingresa, si lo ingresa lo marcamos, si no no*/
                        //Creo que casi todos los gastos llevaran el Impuesto//
                        if (DG.IVA > 0 && IVAS == DG.IVA)//Si es mayor que 0 y el igual al calculo de nosotros lo marcamos.
                        {
                            inputWorkSheet.Cells.Range["O" + pos].Value = "B1";
                            inputWorkSheet.Cells.Range["P" + pos].Value = "X";
                            inputWorkSheet.Cells.Range["N" + pos].Value = Total1;// Valor bruto 
                        }
                        // Si el gasto no lleva impuestos tipo cuadrilla y etc
                        else
                        {
                            if (revision == true)
                            {
                                inputWorkSheet.Cells.Range["O" + pos].Value = "B0";
                                inputWorkSheet.Cells.Range["N" + pos].Value = costo;
                            }
                            else
                            {
                                inputWorkSheet.Cells.Range["O" + pos].Value = "B0";
                                inputWorkSheet.Cells.Range["N" + pos].Value = Total;
                            }
                        }
                    }
                    else
                    {
                        inputWorkSheet.Cells.Range["L" + pos].Value = CuentaSAP;
                        inputWorkSheet.Cells.Range["N" + pos].Value = Total;
                    }
                    // IMPORTES 
                    if (DG.IVA != 0)
                    {
                        if (HojaRuta.Substring(0, 1) == "G")
                        {
                            inputWorkSheet.Cells.Range["O" + pos].Value = "G6";
                            inputWorkSheet.Cells.Range["P" + pos].Value = "X";
                        }

                        if (HojaRuta.Substring(0, 1) == "N")
                        {
                            inputWorkSheet.Cells.Range["O" + pos].Value = "N1";
                        }
                    }
                    inputWorkSheet.Cells.Range["Q" + pos].Value = "";
                    inputWorkSheet.Cells.Range["T" + pos].Value = HojaRuta;
                    inputWorkSheet.Cells.Range["U" + pos].Value = Cliente + "/" + Material;
                    inputWorkSheet.Cells.Range["V" + pos].Value = Total;
                    inputWorkSheet.Cells.Range["W" + pos].Value = System.DateTime.Now.ToString("dd.MM.yyyy");
                    inputWorkSheet.Cells.Range["Z" + pos].Value = HojaRuta;
                    #endregion

                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //////////////////////////////////// Costo mas Impuestos///////////////////////////////////////////////////////////////////////////////////////////////////////////////////



                    inputWorkSheet.Cells.Range["A" + pos2].Value = System.DateTime.Now.ToString("dd.MM.yyyy");
                    inputWorkSheet.Cells.Range["B" + pos2].Value = "SA";
                    if (HojaRuta.Substring(0, 1) == "G")
                    {
                        inputWorkSheet.Cells.Range["C" + pos2].Value = "GT01";
                    }
                    if (HojaRuta.Substring(0, 1) == "S")
                    {
                        inputWorkSheet.Cells.Range["C" + pos2].Value = "SV01";

                    }
                    if (HojaRuta.Substring(0, 1) == "N")
                    {
                        inputWorkSheet.Cells.Range["C" + pos2].Value = "NI01";
                    }
                    inputWorkSheet.Cells.Range["D" + pos2].Value = System.DateTime.Now.ToString("dd.MM.yyyy");
                    inputWorkSheet.Cells.Range["E" + pos2].Value = System.DateTime.Now.ToString("MM");
                    inputWorkSheet.Cells.Range["F" + pos2].Value = DG.TABLA.Rows[0]["Moneda"].ToString();
                    inputWorkSheet.Cells.Range["G" + pos2].Value = "1000010";
                    inputWorkSheet.Cells.Range["H" + pos2].Value = "COSTO HR";
                    inputWorkSheet.Cells.Range["I" + pos2].Value = Proveedor;
                    inputWorkSheet.Cells.Range["J" + pos2].Value = "9095";
                    inputWorkSheet.Cells.Range["K" + pos2].Value = "31";
                    inputWorkSheet.Cells.Range["L" + pos2].Value = CodigoProveedor;

                    //Costo mas Impuesto
                    if (HojaRuta.Substring(0, 1) == "S")
                    {
                        // inputWorkSheet.Cells.Range["N" + pos2].Value = Total2;
                        if (P_IVAs > 0)
                        {

                            inputWorkSheet.Cells.Range["N" + pos2].Value = Total2;// Valor mas impuesto
                        }
                        else
                        {
                            inputWorkSheet.Cells.Range["N" + pos2].Value = Total;// Valor bruto 
                        }

                        inputWorkSheet.Cells.Range["O" + pos2].Value = "B0";//Porque va para la 31 nunca debera llevar indicador de Impuesto
                    }

                    else
                    {
                        inputWorkSheet.Cells.Range["N" + pos2].Value = Total;

                    }


                    if (DG.IVA != 0)
                    {
                        //if (HojaRuta.Substring(0, 1) == "G")
                        //{
                        //    inputWorkSheet.Cells.Range["O" + pos].Value = "N1";
                        //    inputWorkSheet.Cells.Range["P" + pos].Value = "X";
                        //}
                        //if (HojaRuta.Substring(0, 1) == "S")
                        //{
                        //    inputWorkSheet.Cells.Range["O" + pos].Value = "N1";
                        //    inputWorkSheet.Cells.Range["P" + pos].Value = "X";
                        //}
                        if (HojaRuta.Substring(0, 1) == "N")
                        {
                            inputWorkSheet.Cells.Range["O" + pos].Value = "N1";
                        }
                    }
                    inputWorkSheet.Cells.Range["Q" + pos2].Value = "";
                    inputWorkSheet.Cells.Range["T" + pos2].Value = IdSolicitud;
                    inputWorkSheet.Cells.Range["U" + pos2].Value = "ID " + IdSolicitud + Cliente + "/" + Material;
                    inputWorkSheet.Cells.Range["V" + pos2].Value = Total;
                    inputWorkSheet.Cells.Range["W" + pos2].Value = System.DateTime.Now.ToString("dd.MM.yyyy");
                    inputWorkSheet.Cells.Range["Z" + pos2].Value = HojaRuta;
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ////////////////////////////////////////////////////////////////// Impuesto Sobre Renta el salvador /////////////////////////////////////////////////////////////////////////////////////                    

                    if (HojaRuta.Substring(0, 1) == "S")
                    {       // Agragar Tercera Fila de Impuestos

                        if (P_ISR > 0)
                        {



                            inputWorkSheet.Cells.Range["A" + pos3].Value = System.DateTime.Now.ToString("dd.MM.yyyy");
                            inputWorkSheet.Cells.Range["B" + pos3].Value = "SA";
                            if (HojaRuta.Substring(0, 1) == "G")
                            {
                                inputWorkSheet.Cells.Range["C" + pos3].Value = "GT01";
                            }
                            if (HojaRuta.Substring(0, 1) == "S")
                            {
                                inputWorkSheet.Cells.Range["C" + pos3].Value = "SV01";
                            }
                            if (HojaRuta.Substring(0, 1) == "N")
                            {
                                inputWorkSheet.Cells.Range["C" + pos3].Value = "NI01";
                            }
                            inputWorkSheet.Cells.Range["D" + pos3].Value = System.DateTime.Now.ToString("dd.MM.yyyy");
                            inputWorkSheet.Cells.Range["E" + pos3].Value = System.DateTime.Now.ToString("MM");
                            inputWorkSheet.Cells.Range["F" + pos3].Value = DG.TABLA.Rows[0]["Moneda"].ToString();
                            inputWorkSheet.Cells.Range["G" + pos3].Value = "1000010";
                            inputWorkSheet.Cells.Range["H" + pos3].Value = "COSTO HR";
                            inputWorkSheet.Cells.Range["I" + pos3].Value = Proveedor;
                            inputWorkSheet.Cells.Range["J" + pos3].Value = "9095";
                            inputWorkSheet.Cells.Range["K" + pos3].Value = "50";
                            inputWorkSheet.Cells.Range["L" + pos3].Value = "2132018";
                            inputWorkSheet.Cells.Range["N" + pos3].Value = Renta;
                            inputWorkSheet.Cells.Range["O" + pos3].Value = "B0";
                            inputWorkSheet.Cells.Range["Q" + pos3].Value = "";
                            inputWorkSheet.Cells.Range["T" + pos3].Value = IdSolicitud;
                            inputWorkSheet.Cells.Range["U" + pos3].Value = "ID " + IdSolicitud + Cliente + "/" + Material;
                            inputWorkSheet.Cells.Range["V" + pos3].Value = Total;
                            inputWorkSheet.Cells.Range["W" + pos3].Value = System.DateTime.Now.ToString("dd.MM.yyyy");
                            inputWorkSheet.Cells.Range["Z" + pos3].Value = HojaRuta;
                            pos = pos + 1;
                        }
                    }

                    llenarBitacora("Se generó la carga " + IdSolicitud, Session["IdUsuario"].ToString(), HojaRuta);
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                }
                C.UltimoResgitros("-1");
                C.newLine();
                C.FECHA = DateTime.Now.ToString();
                C.USUARIO = Session["IdUsuario"].ToString();
                C.commitLine();
                C.actualizar();
                #endregion
                workbook.Save();
            }
            catch (Exception ex)
            {
                //LogError(ex.Message, Session["IdUsuario"].ToString(), "GenerarArchivoSAPDinant");
            }
            finally
            {
                //CERRAR EL LIBRO DE TRABAJO y la aplicacion

                workbook.Close();
                xlApp.Quit();
                desconectar();
                Application.UnLock();
            }
            if (hayRegistro)
            {
                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + nameDest);
                Response.Charset = "";
                Response.TransmitFile("J:\\Files1\\Aduanas\\" + nameDest);
                Response.Flush();
                Response.End();

            }
        }
    }
}
