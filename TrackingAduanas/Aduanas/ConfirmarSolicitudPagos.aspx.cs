﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;
using System.Data.OleDb;
using System.Reflection;
using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;

public partial class GenerarArchivoSAPDinant : Utilidades.PaginaBase
{
    private GrupoLis.Login.Login logAppAduanas;
    string HojaRuta = "";
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Confirmar Pagos";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Hagamoda/Promoda", "Confirmar Solicitud de Pagos");
        }
    }

    protected bool Consultar { get { return tienePermiso("Consultar"); } }

    private void ConfigureExport()
    {
        String filename = "Reporte_" + DateTime.Now.ToShortDateString();
        rgGastos.ExportSettings.FileName = filename;
        rgGastos.ExportSettings.ExportOnlyData = true;
        rgGastos.ExportSettings.IgnorePaging = true;
        rgGastos.ExportSettings.OpenInNewWindow = true;
        rgGastos.MasterTableView.ExportToExcel();
    }

    public override bool CanGoBack { get { return false; } }


    private void llenarGrid()
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            DetalleGastosBO DG = new DetalleGastosBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            if (User.IsInRole("Administradores") && User.Identity.Name == "dabonilla" || User.IsInRole("Administradores") && User.Identity.Name == "aalvarado" || User.IsInRole("Administradores") && User.Identity.Name == "lramirez" || User.IsInRole("Administradores") && User.Identity.Name == "mnapky" || User.IsInRole("Administradores") && User.Identity.Name == "gcorrales" || User.IsInRole("Administradores") && User.Identity.Name == "ycaballero")
                DG.loadInstruccionesSolicitudConfirmar();
            else if (User.IsInRole("Administrador Pais") || User.IsInRole("Administrador Pais SVGT"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                DG.loadInstruccionesSolicitudConfirmarXPais(u.CODPAIS);
            }
            rgGastos.DataSource = DG.TABLA;

        }
        catch (Exception)
        {

        }

        finally { desconectar(); }

    }

    protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {

            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName)
                ConfigureExport();

            foreach (GridDataItem gridItem in rgGastos.Items)
            {
                HojaRuta = gridItem.Cells[4].Text;
            }
        }
        catch (Exception)
        { }
    }

    protected void llenarinforhojaxgasto(string X, string Y)
    {
        try
        {
            conectar();
            DetalleGastosBO DG = new DetalleGastosBO(logApp);
            DG.LlenarGatosXHojaYGasto(X, Y);

        }
        catch (Exception)
        {
        }

        finally { desconectar(); }
    }

    protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenarGrid();
    }
    protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
    {
        try
        {
            conectar();
            GatosTracking GT = new GatosTracking(logApp);

            foreach (GridDataItem gridItem in rgGastos.Items)
            {
                string HojaRuta = gridItem.Cells[4].Text;
                GT.LlenarGatos(HojaRuta.Substring(0, 1));
                RadComboBox Material = (RadComboBox)gridItem.FindControl("cmbMaterial");
                RadComboBox MedioPago = (RadComboBox)gridItem.FindControl("cmbMedioPago");
                Material.DataSource = GT.TABLA;
                Material.DataBind();
                Material.Items.Insert(0, new RadComboBoxItem("Seleccione...", "Seleccione"));
                switch (Material.SelectedValue)
                {
                    case "Seleccione":
                        MedioPago.Visible = false;
                        break;
                    default: break;
                }

            }

        }
        catch (Exception)
        {

        }
    }
    protected Object GetMateriales()
    {
        try
        {
            conectar();

            GatosTracking bo = new GatosTracking(logApp);
            bo.LlenarGatos(HojaRuta.Substring(0, 1));
            return bo.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }
        return null;
    }

    protected Object GetMedioPago()
    {
        try
        {
            conectar();
            CodigosBO C = new CodigosBO(logApp);
            C.loadDivision("MEDIOPAGO");
            return C.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }
        return null;
    }
    protected Object GetProveedores()
    {
        try
        {
            conectar();
            ProveedoresSolicitudBO PS = new ProveedoresSolicitudBO(logApp);
            PS.loadProveedoresPais(HojaRuta.Substring(0, 1));
            return PS.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }
        return null;
    }
    protected Object GetMoneda()
    {
        try
        {
            conectar();
            CodigosBO C = new CodigosBO(logApp);
            C.loadAllCampos("MONEDA" + HojaRuta.Substring(0, 1));
            return C.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }
        return null;
    }
    #region Conexion
    private void conectarAduanas(string Pais)
    {
        if (logAppAduanas == null)
            logAppAduanas = new GrupoLis.Login.Login();
        if (!logAppAduanas.conectado)
        {
            logAppAduanas.tipoConexion = TipoConexion.SQL_SERVER;
            logAppAduanas.SERVIDOR = mParamethers.Get("Servidor2");
            if (Pais.ToString() == "S")
            {
                logAppAduanas.DATABASE = mParamethers.Get("CajaChicaSV");
            }
            if (Pais.ToString() == "N")
            {
                logAppAduanas.DATABASE = mParamethers.Get("CajaChicaNI");
            }
            if (Pais.ToString() == "G")
            {
                logAppAduanas.DATABASE = mParamethers.Get("CajaChicaGT");
            }
            logAppAduanas.USER = mParamethers.Get("User2");
            logAppAduanas.PASSWD = mParamethers.Get("Password2");
            logAppAduanas.conectar();
        }
    }

    private void desconectarAduanas()
    {
        if (logAppAduanas.conectado)
            logAppAduanas.desconectar();
    }
    #endregion

    protected void RadGrid1_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridEditableItem editedItem = e.Item as GridEditableItem;
            String HojaRuta = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["HojaRuta"]);
            String IdGasto = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdGasto"]);
            DetalleGastosBO DG = new DetalleGastosBO(logApp);
            DetalleGastosBO DG1 = new DetalleGastosBO(logApp);
            DG.LlenarGatosXHojaYGasto(HojaRuta, IdGasto);
            string gasto = "";
            gasto = DG.IDGASTO;

            if (DG.totalRegistros == 1)
            {
                GetDatosEditadosDetalleGastos(DG, e.Item);

                DG1.LlenarGatosXHojaYGasto(HojaRuta, DG.IDGASTO);
                if (DG1.totalRegistros == 0)
                {
                    DG.actualizar();
                    registrarMensaje("Gasto Actualizado Exitosamente");
                }
                if (DG1.totalRegistros == 1)
                {
                    if (gasto == DG1.IDGASTO)
                    {
                        DG.actualizar();
                    }
                    else
                    {
                        registrarMensaje("Gasto ya Existe no se Puede Duplicar");
                    }
                }
                if (DG1.totalRegistros > 1)
                {
                    registrarMensaje("Favor Comuniquese con el Administrador del Sistema");
                }
            }
            else
            {
                registrarMensaje("Favor Comuniquese con el Administrador del Sistema");
            }

        }
        catch (Exception)
        {

        }
        finally { desconectar(); }
    }

    protected void GetDatosEditadosDetalleGastos(DetalleGastosBO DG, GridItem item)
    {
        GridEditableItem editedItem = item as GridEditableItem;
        RadNumericTextBox IVA = (RadNumericTextBox)editedItem.FindControl("edIVA");
        RadNumericTextBox Monto = (RadNumericTextBox)editedItem.FindControl("edMonto");
        RadNumericTextBox TotalVenta = (RadNumericTextBox)editedItem.FindControl("edTotal");
        RadComboBox Material = (RadComboBox)editedItem.FindControl("edMaterial");
        RadComboBox MedioPago = (RadComboBox)editedItem.FindControl("edMedioPago");
        RadComboBox PagarA = (RadComboBox)editedItem.FindControl("edPagarA");
        RadComboBox Moneda = (RadComboBox)editedItem.FindControl("edMoneda");
        DG.IDGASTO = Material.SelectedValue;
        DG.MEDIOPAGO = MedioPago.SelectedValue;
        DG.PROVEEDOR = PagarA.SelectedValue;
        DG.MONTO = Convert.ToDouble(Monto.Value);
        DG.IVA = Convert.ToDouble(IVA.Value);
        DG.MONEDA = Moneda.SelectedValue;
        DG.TOTALVENTA = Convert.ToDouble(TotalVenta.Value);
    }
    protected void rbtnEnviar_Click(object sender, EventArgs e)
    {
        foreach (GridDataItem grdItem in rgGastos.Items)
        {
            CheckBox chkRango = (CheckBox)grdItem.FindControl("edchbaceptar");
            RadDatePicker Fecha = (RadDatePicker)grdItem.FindControl("dtFechaPago");
            RadTimePicker Hora = (RadTimePicker)grdItem.FindControl("dtHoraPago");
            //if (chkRango.Checked && !Fecha.IsEmpty && !Hora.IsEmpty)
            if (chkRango.Checked)
            {
                try
                {
                    conectar();
                    DetalleGastosBO DG = new DetalleGastosBO(logApp);
                    DG.LlenarGatosXHojaYGasto(grdItem.Cells[4].Text, grdItem.Cells[6].Text);
                    if (DG.totalRegistros >= 1)
                    {
                        // edFecha.Value = Fecha.SelectedDate.Value.ToShortDateString();
                        // edHora.Value = Hora.SelectedDate.Value.ToShortTimeString();
                        DG.FECHAPAGOSISTEMA = DateTime.Now.ToString();
                       // DG.FECHAPAGO = edFecha.Value + " " + edHora.Value;
                        DG.USUARIOPAGO = Session["IdUsuario"].ToString();
                        if (grdItem.Cells[9].Text == ".00")
                        {
                            DG.ESTADO = "10";
                        }
                        else
                        {
                            GatosTracking GT = new GatosTracking(logApp);
                            GT.LoadGatos(grdItem.Cells[6].Text);
                            if (GT.IMPUESTO == "1")
                            {
                                DG.ESTADO = "2";
                            }
                            else if (GT.IMPUESTO == "0")
                            {
                                DG.ESTADO = "1";
                            }
                            else if (grdItem.Cells[6].Text == "Caja Chica")
                            {
                                DG.ESTADO = "7";
                            }
                        }
                        DG.actualizar();
                      
                    }
                }
                catch (Exception)
                {

                }

                finally {
                    registrarMensaje("El Gasto Fue Aprobado Exitosamente!");
                    desconectar(); }

            }
        }
        rgGastos.Rebind();
    }
    protected void rgGastos_DeleteCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridDataItem editedItem = e.Item as GridDataItem;

            String codCliente = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"]);

            DetalleGastosBO DG = new DetalleGastosBO(logApp);
            DG.CargarGastoXID(codCliente);
            DG.ESTADO = "99";
            DG.actualizar();
            registrarMensaje("Gasto eliminado exitosamente");

            rgGastos.Rebind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }
}