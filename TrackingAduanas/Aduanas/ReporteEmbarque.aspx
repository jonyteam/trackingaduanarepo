﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteEmbarque.aspx.cs" Inherits="ReporteGestionCarga" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:content id="Content1" contentplaceholderid="HeadContent" runat="Server">
</asp:content>
<asp:content id="Content2" contentplaceholderid="MainContent" runat="Server">
    <table border="1" style="width: 45%">
        <tr>
            <td style="width: 15%">
                <asp:label id="lblFechaInicio" runat="server" text="Fecha Inicio:" forecolor="Black">
                </asp:label>
            </td>
            <td style="width: 22%">
                <telerik:RadDatePicker ID="dpFechaInicio" runat="server" EnableTyping="False" Width="95%">
                </telerik:RadDatePicker>
                <asp:requiredfieldvalidator runat="server" id="RequiredFieldValidator1" controltovalidate="dpFechaInicio"
                    errormessage="Ingrese Fecha" forecolor="Red" />
            </td>
            <td style="width: 15%">
                <asp:label id="lblFechaFinal" runat="server" text="Fecha Final:" forecolor="Black"></asp:label>
            </td>
            <td style="width: 22%">
                <telerik:RadDatePicker ID="dpFechaFinal" runat="server" EnableTyping="False" Width="95%">
                </telerik:RadDatePicker>
                <asp:requiredfieldvalidator runat="server" id="reqName" controltovalidate="dpFechaFinal"
                    errormessage="Ingrese Fecha" forecolor="Red" />
            </td>
            <td style="width: 10%">
                <asp:label id="lblPais" runat="server" text="País:" forecolor="Black"></asp:label>
            </td>
            <td style="width: 16%">
                <telerik:RadComboBox ID="cmbPais" runat="server" DataTextField="Descripcion" DataValueField="Codigo"
                    AutoPostBack="true" OnSelectedIndexChanged="cmbPais_SelectedIndexChanged">
                </telerik:RadComboBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:label id="Label2" runat="server" text="Cliente:" forecolor="Black"></asp:label>
            </td>
            <td colspan="5">
                <telerik:RadComboBox ID="cmbCliente" runat="server" DataTextField="Nombre" DataValueField="CodigoSAP"
                    Width="100%" Filter="StartsWith">
                </telerik:RadComboBox>
            </td>
        </tr>
        <tr>
            <td colspan="6" align="center">
                <asp:imagebutton id="btnBuscar" runat="server" imageurl="~/Images/24/view_24.png"
                    onclick="btnBuscar_Click" />
            </td>
        </tr>
    </table>
    <telerik:RadGrid ID="rgInstrucciones" runat="server" AllowFilteringByColumn="True"
        Visible="false" AllowSorting="True" AutoGenerateColumns="False" GridLines="None"
        Height="410px" OnNeedDataSource="rgInstrucciones_NeedDataSource" AllowPaging="True"
        ShowFooter="True" ShowStatusBar="True" PageSize="20" OnInit="rgInstrucciones_Init"
        OnItemCommand="rgInstrucciones_ItemCommand">
        <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
            PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
            Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
        <MasterTableView DataKeyNames="" CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros."
            NoMasterRecordsText="No hay instrucciones." GroupLoadMode="Client" ShowGroupFooter="true">
            <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" RefreshText="Volver a Cargar Datos"
                ShowExportToExcelButton="true" />
            <RowIndicatorColumn>
                <HeaderStyle Width="20px"></HeaderStyle>
            </RowIndicatorColumn>
            <ExpandCollapseColumn>
                <HeaderStyle Width="20px"></HeaderStyle>
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn DataField="Nombre" HeaderText="Cliente" UniqueName="Nombre"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="100px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="NombreAduana" HeaderText="Aduana" UniqueName="NombreAduana"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="100px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="IdInstruccion" HeaderText="IdInstruccion" UniqueName="IdInstruccion"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="120px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ReferenciaCliente" HeaderText="ReferenciaCliente"
                    UniqueName="ReferenciaCliente" FilterControlWidth="60%">
                    <HeaderStyle Width="200px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Correlativo" HeaderText="Correlativo" UniqueName="Correlativo"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="120px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Producto" HeaderText="Producto" UniqueName="Producto"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="140px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CodRegimen" HeaderText="Código Regimen" UniqueName="CodRegimen"
                    FilterControlWidth="50%">
                    <HeaderStyle Width="60px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Regimen" HeaderText="Regimen" UniqueName="Regimen"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="140px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="NumFactura" HeaderText="NumFactura" UniqueName="NumFactura"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="140px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="FechaEntrada" HeaderText="FechaEntrada" UniqueName="FechaEntrada"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="100px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="HoraEntrada" HeaderText="HoraEntrada" UniqueName="HoraEntrada"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="140px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Procedencia" HeaderText="Procedencia" UniqueName="Procedencia"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="140px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Proveedor" HeaderText="Proveedor" UniqueName="Proveedor"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="140px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CiudadDestino" HeaderText="CiudadDestino" UniqueName="CiudadDestino"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="140px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Motorista" HeaderText="Motorista" UniqueName="Motorista"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="140px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Impuesto" HeaderText="Impuesto" UniqueName="Impuesto"
                    FilterControlWidth="60%" DataFormatString="{0:C2}" FooterText="Total" Aggregate="Sum"
                    FooterAggregateFormatString="{0:C2}">
                    <HeaderStyle Width="140px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Observaciones" HeaderText="Observaciones" UniqueName="Observaciones"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="100px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="HoraSalida" HeaderText="HoraSalida" UniqueName="HoraSalida"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="140px" />
                </telerik:GridBoundColumn>
            </Columns>
            <GroupByExpressions>
                <telerik:GridGroupByExpression>
                    <GroupByFields>
                        <telerik:GridGroupByField FieldAlias="Aduana" FieldName="NombreAduana" SortOrder="Ascending" />
                    </GroupByFields>
                    <SelectFields>
                        <telerik:GridGroupByField FieldAlias="Aduana" FieldName="NombreAduana" />
                    </SelectFields>
                </telerik:GridGroupByExpression>
            </GroupByExpressions>
        </MasterTableView>
        <HeaderStyle Width="180px" />
        <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
            <Selecting AllowRowSelect="True" />
            <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                DropHereToReorder="Suelte aquí para Re-Ordenar" />
            <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
        </ClientSettings>
        <FilterMenu EnableTheming="True">
            <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
        </FilterMenu>
        <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
        <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
    </telerik:RadGrid>
</asp:content>
