﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;

public partial class EspeciesFiscalesClientes : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Especies Fiscales Clientes";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgRangoEspeciesFiscales.FilterMenu);
        rgRangoEspeciesFiscales.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgRangoEspeciesFiscales.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Especies Fiscales Clientes", "Especies Fiscales Clientes");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
            mpEspeciesFiscales.SelectedIndex = 0;
            limpiarControles();
            cargarDatosInicioRangos();
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    #region Especies Fiscales Clientes
    protected void rgRangoEspeciesFiscales_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenarGridRangoEspeciesFiscales();
    }

    private void llenarGridRangoEspeciesFiscales()

    {
        try
        {
            conectar();
            MovimientosEspeciesFiscalesBO mef = new MovimientosEspeciesFiscalesBO(logApp);
            mef.loadRangosEspeciesFiscalesEnviosCliente2(cmbAduanaOrigen.SelectedValue, cmbEspecieFiscal.SelectedValue);
            rgRangoEspeciesFiscales.DataSource = mef.TABLA;
            rgRangoEspeciesFiscales.DataBind();
        }
        catch { }
    }

    protected void rgRangoEspeciesFiscales_Init(object sender, EventArgs e)
    {
        GridFilterMenu menu = rgRangoEspeciesFiscales.FilterMenu;
        menu.Items.RemoveAt(rgRangoEspeciesFiscales.FilterMenu.Items.Count - 2);
    }

    private void cargarDatosInicioRangos()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (u.CODPAIS == "H")
                c.loadPaisesHojaRuta();
            else
                c.loadAllCampos("PAISES", u.CODPAIS);
            cmbPais.DataSource = c.TABLA;
            cmbPais.DataBind();
            loadAduanasOrigen();
            loadClientes();
        }
        catch { }
    }

    private void loadAduanasOrigen()
    {
        try
        {
            conectar();
            AduanasBO a = new AduanasBO(logApp);
            a.loadPaisAduanas(cmbPais.SelectedValue);
            cmbAduanaOrigen.DataSource = a.TABLA;
            cmbAduanaOrigen.DataBind();
            loadEspeciesFiscales();
            llenarGridRangoEspeciesFiscales();
            //cmbAduanaOrigen.Items.Remove(9);
            //cmbAduanaOrigen.Items.Remove(9);
            //cmbAduanaOrigen.Items.Remove(10);
            //cmbAduanaOrigen.Items.Remove(10);
        }
        catch { }
    }

    private void loadClientes()
    {
        try
        {
            conectar();
            ClientesBO c = new ClientesBO(logApp);
            c.loadAllCamposClientesXPais(cmbPais.SelectedValue);
            cmbCliente.DataSource = c.TABLA;
            cmbCliente.DataBind();
        }
        catch { }
    }

    private void loadEspeciesFiscales()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            c.loadCodigosXCategoriaCodigos("ESPECIESFISCALES", "'DVA','F'");
            //c.loadAllCampos("ESPECIESFISCALES", "DVA");
            cmbEspecieFiscal.DataSource = c.TABLA;
            cmbEspecieFiscal.DataBind();
        }
        catch { }
    }

    protected void cmbPais_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        loadAduanasOrigen();
        loadClientes();
    }



protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            conectar();
            MovimientosEspeciesFiscalesBO mef = new MovimientosEspeciesFiscalesBO(logApp);
            MovimientosEspeciesFiscalesBO mefe = new MovimientosEspeciesFiscalesBO(logApp);
     
            
            if (this.Validar_Entradas()) { 
      
                   mef.loadMovimientosEspeciesFiscalesClientes2(cmbEspecieFiscal.SelectedValue, cmbAduanaOrigen.SelectedValue,this.Buscar_Item());
           
                   if (mef.totalRegistros > 0)
                    {
                        int cantidad = 0, cantidadDisponible = 0, cantidadSolicitada = int.Parse(txtCantidad.Value.ToString());
                       
                       for (int i = 0; i < mef.totalRegistros; i++)
                        {
                            cantidad += mef.CANTIDAD;
                            mef.regSiguiente();
                        }

                        if (cantidad >= cantidadSolicitada)
                        {
                            string idMovimiento = Generar_IdMovimiento();
                            mef.loadMovimientosEspeciesFiscalesClientes2(cmbEspecieFiscal.SelectedValue, cmbAduanaOrigen.SelectedValue,this.Buscar_Item());
                            
                            for (int i = 0; i < mef.totalRegistros; i++)
                            {
                                cantidadDisponible += mef.CANTIDAD;

                                if (cantidadDisponible > cantidadSolicitada)
                                {

                                    this.nuevo_movimiento_EspecieFiscal( mef, idMovimiento, "1", "2", (int.Parse(mef.RANGOINICIAL) + cantidadSolicitada).ToString(),mef.RANGOFINAL.ToString(), int.Parse((cantidadDisponible - cantidadSolicitada).ToString()));
                                    
                                }
                               
                                string idMovimientoEnvio = this.Generar_IdMovimiento();
                                if (cantidadDisponible >= cantidadSolicitada)
                                {
                                    mef.CODESTADO = "100";
                                    mef.actualizar();

                                    this.nuevo_movimiento_EspecieFiscal ( mef, idMovimientoEnvio, "5", "3", mef.RANGOINICIAL, (int.Parse(mef.RANGOINICIAL) + cantidadSolicitada - 1).ToString(), cantidadSolicitada);
                                    this.Nuevo_movimientoClientes(idMovimientoEnvio);                                                      

                                    break;
                                }
                                else
                                {
                                    mef.CODESTADO = "100";
                                    mef.actualizar();
                                  
                                    this.nuevo_movimiento_EspecieFiscal(mef, idMovimientoEnvio, "5", "3",mef.RANGOINICIAL,mef.RANGOFINAL,mef.CANTIDAD);
                                    this.Nuevo_movimientoClientes(idMovimientoEnvio);    

                                }
                            }

                          
                            limpiarControles();
                         
                        }
                        else
                            registrarMensaje("La cantidad solicitada no esta disponible, solo existen " + cantidad + " series de esta especie fiscal");
                    }
                    else
                        registrarMensaje("No existen series de esta especie fiscal en la aduana origen");
            }
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "EspeciesFiscales");
        }
        finally
        {
            desconectar();
            rgRangoEspeciesFiscales.Rebind();
        }
    }



#region Funciones


public bool Validar_Entradas()
{
    bool VAL = false;
    
    if (!cmbPais.IsEmpty)
            {
                if (!cmbAduanaOrigen.IsEmpty)
                {
                    if (!cmbEspecieFiscal.IsEmpty)
                    {
                        if (txtCantidad.Value.HasValue)
                        {
                            if (txtCantidad.Value > 0)
                            {
                                VAL=true;
                            }
                        
                            else
                                registrarMensaje("La cantidad debe ser mayor que cero");
                        }
                        else
                            registrarMensaje("La cantidad no puede estar vacía");
                    }
                    else
                        registrarMensaje("La especie fiscal no puede estar vacía");
                }
                else
                    registrarMensaje("La aduana origen no puede estar vacía");
            }
            else
                registrarMensaje("País no puede estar vacío");
        
    return VAL;
}

public void nuevo_movimiento_EspecieFiscal(MovimientosEspeciesFiscalesBO mef,String Idmovimiento, String CODTIPOMOVIMIENTOs, String CODESTADO, String RANGOINICIAL, String RANGOFINAL, int CANTIDAD)
{
    MovimientosEspeciesFiscalesBO mefe = new MovimientosEspeciesFiscalesBO(logApp);
    mefe.loadMovimientosEspeciesFiscales("-1");
    mefe.newLine();
    mefe.IDMOVIMIENTO = Idmovimiento;
    mefe.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
    mefe.CODPAIS = cmbPais.SelectedValue;
    mefe.CODIGOADUANA = cmbAduanaOrigen.SelectedValue;
    mefe.RANGOINICIAL =RANGOINICIAL;
    mefe.RANGOFINAL =RANGOFINAL;
    mefe.CANTIDAD =CANTIDAD;
    mefe.FECHA = DateTime.Now.ToString();
    mefe.CODTIPOMOVIMIENTO = CODTIPOMOVIMIENTOs;   //1 Envío a cliente
    mefe.CODESTADO = CODESTADO;   //Enviado a cliente
    mefe.IDUSUARIO = Session["IdUsuario"].ToString();
    mefe.IDCOMPRA = mef.IDCOMPRA;
    mefe.commitLine();
    mefe.actualizar();

    llenarBitacora("Rango asignado por", Session["IdUsuario"].ToString(), (mefe.RANGOINICIAL.ToString() + " " + mefe.RANGOFINAL.ToString()));

    if (CODTIPOMOVIMIENTOs.ToString() == '5'.ToString())
    {
    registrarMensaje("Asignación de " + cmbEspecieFiscal.Text + " del " + mefe.RANGOINICIAL + " al " + mefe.RANGOFINAL + " realizado exitosamente");
       
    }
}


public string Generar_IdMovimiento()
{
    DateTime fecha1 = DateTime.Now;
    string idMovimientoEnvio = fecha1.Year.ToString() + fecha1.Month.ToString("00") + fecha1.Day.ToString("00") + "." + fecha1.Hour.ToString("00") + fecha1.Minute.ToString("00") + fecha1.Second.ToString("00") + "." + fecha1.Millisecond.ToString("000");
    return idMovimientoEnvio;                
}

public void Nuevo_movimientoClientes( String idMovimientoEnvio)
{
    EspeciesFiscalesClientesBO efc = new EspeciesFiscalesClientesBO(logApp);
    efc.loadEspeciesFiscalesClientesXIdMov("-1");
    efc.newLine();
    efc.IDMOVIMIENTO = idMovimientoEnvio;
    efc.IDCLIENTE = cmbCliente.SelectedValue;
    efc.OBSERVACION = "";
    efc.commitLine();
    efc.actualizar();
}

public string Buscar_Item()
{
    string rango="0";

    foreach (GridDataItem grdItem in rgRangoEspeciesFiscales.Items)
    {
        CheckBox chkSeleccion = (CheckBox)grdItem.FindControl("ckbSeleccionar");


        if (chkSeleccion.Checked)
        {
            rango = rango + "," + grdItem["Item"].Text;
            break;
        }
    }
    return rango;
}

#endregion























//boton de asignar especies fisclaes a los clientes antiror





    protected void btnSalvar_Click0(object sender, ImageClickEventArgs e)
{
    try
    {
        conectar();
        MovimientosEspeciesFiscalesBO mef = new MovimientosEspeciesFiscalesBO(logApp);
        MovimientosEspeciesFiscalesBO mefe = new MovimientosEspeciesFiscalesBO(logApp);
        EspeciesFiscalesClientesBO efc = new EspeciesFiscalesClientesBO(logApp);

        //TrasladosEspeciesFiscalesBO tef = new TrasladosEspeciesFiscalesBO(logApp);
        if (!cmbPais.IsEmpty)
        {
            if (!cmbAduanaOrigen.IsEmpty)
            {
                if (!cmbEspecieFiscal.IsEmpty)
                {
                    if (txtCantidad.Value.HasValue)
                    {
                        if (txtCantidad.Value > 0)
                        {
                            string rango = "0";

                            foreach (GridDataItem grdItem in rgRangoEspeciesFiscales.Items)
                            {

                                CheckBox chkRango = (CheckBox)grdItem.FindControl("chkRango");
                                RadTextBox item = (RadTextBox)grdItem.FindControl("Item");
                                string hola = item.Text;
                                if (chkRango.Checked)
                                {
                                    rango = rango + "," + item.Text;
                                }
                            }

                            mef.loadMovimientosEspeciesFiscalesClientes(cmbEspecieFiscal.SelectedValue, cmbAduanaOrigen.SelectedValue);
                            if (mef.totalRegistros > 0)
                            {
                                int cantidad = 0, cantidadDisponible = 0, cantidadSolicitada = int.Parse(txtCantidad.Value.ToString());
                                for (int i = 0; i < mef.totalRegistros; i++)
                                {
                                    cantidad += mef.CANTIDAD;
                                    mef.regSiguiente();
                                }
                                if (cantidad >= cantidadSolicitada)
                                {
                                    DateTime fecha = DateTime.Now;      //20120109.171835.761    
                                    string rangoInicial = "", rangoFinal = "";
                                    string idMovimiento = fecha.Year.ToString() + fecha.Month.ToString("00") + fecha.Day.ToString("00") + "." + fecha.Hour.ToString("00") + fecha.Minute.ToString("00") + fecha.Second.ToString("00") + "." + fecha.Millisecond.ToString("000");
                                    mef.loadMovimientosEspeciesFiscalesClientes(cmbEspecieFiscal.SelectedValue, cmbAduanaOrigen.SelectedValue);
                                    for (int i = 0; i < mef.totalRegistros; i++)
                                    {
                                        cantidadDisponible += mef.CANTIDAD;
                                        if (cantidadDisponible > cantidadSolicitada)
                                        {
                                            //guardar nuevo registro con las series que quedan disponibles para asignar a clientes
                                            mefe.loadMovimientosEspeciesFiscales("-1");
                                            mefe.newLine();
                                            mefe.IDMOVIMIENTO = idMovimiento;
                                            mefe.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                                            mefe.CODPAIS = cmbPais.SelectedValue;
                                            mefe.CODIGOADUANA = cmbAduanaOrigen.SelectedValue;
                                            mefe.RANGOINICIAL = (int.Parse(mef.RANGOINICIAL) + cantidadSolicitada).ToString();
                                            mefe.RANGOFINAL = mef.RANGOFINAL;
                                            mefe.CANTIDAD = int.Parse((cantidadDisponible - cantidadSolicitada).ToString());
                                            mefe.FECHA = DateTime.Now.ToString();
                                            mefe.CODTIPOMOVIMIENTO = "1";   //1 Envío
                                            mefe.CODESTADO = "2";   //Diferencia
                                            mefe.IDUSUARIO = Session["IdUsuario"].ToString();
                                            mefe.IDCOMPRA = mef.IDCOMPRA;
                                            mefe.commitLine();
                                            mefe.actualizar();
                                        }
                                        DateTime fecha1 = DateTime.Now;
                                        string idMovimientoEnvio = fecha1.Year.ToString() + fecha1.Month.ToString("00") + fecha1.Day.ToString("00") + "." + fecha1.Hour.ToString("00") + fecha1.Minute.ToString("00") + fecha1.Second.ToString("00") + "." + fecha1.Millisecond.ToString("000");
                                        if (cantidadDisponible >= cantidadSolicitada)
                                        {
                                            //cambia el estado a eliminado
                                            mef.CODESTADO = "100";
                                            mef.actualizar();
                                            //guardar nuevo registro en movimientos con las series que se envian al cliente
                                            mefe.loadMovimientosEspeciesFiscales("-1");
                                            mefe.newLine();
                                            mefe.IDMOVIMIENTO = idMovimientoEnvio;
                                            mefe.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                                            mefe.CODPAIS = cmbPais.SelectedValue;
                                            mefe.CODIGOADUANA = cmbAduanaOrigen.SelectedValue;
                                            mefe.RANGOINICIAL = mef.RANGOINICIAL;
                                            mefe.RANGOFINAL = (int.Parse(mef.RANGOINICIAL) + cantidadSolicitada - 1).ToString();
                                            mefe.CANTIDAD = cantidadSolicitada;
                                            mefe.FECHA = DateTime.Now.ToString();
                                            mefe.CODTIPOMOVIMIENTO = "5";   //1 Envío a cliente
                                            mefe.CODESTADO = "3";   //Enviado a cliente
                                            mefe.IDUSUARIO = Session["IdUsuario"].ToString();
                                            mefe.IDCOMPRA = mef.IDCOMPRA;
                                            mefe.commitLine();
                                            mefe.actualizar();

                                            //nuevo
                                            rangoInicial = mefe.RANGOINICIAL;
                                            rangoFinal = mefe.RANGOFINAL;
                                            //

                                            //guardar nuevo registro con las series que se envian al cliente
                                            efc.loadEspeciesFiscalesClientesXIdMov("-1");
                                            efc.newLine();
                                            efc.IDMOVIMIENTO = idMovimientoEnvio;
                                            efc.IDCLIENTE = cmbCliente.SelectedValue;
                                            efc.OBSERVACION = "";
                                            efc.commitLine();
                                            efc.actualizar();

                                            llenarBitacora("Rango asignado por", Session["IdUsuario"].ToString(), (rangoInicial + " " + rangoFinal));

                                            break;
                                        }
                                        else
                                        {
                                            //cambia el estado a eliminado
                                            mef.CODESTADO = "100";
                                            mef.actualizar();
                                            //guardar nuevo registro con las series que se envian a la otra aduana
                                            mefe.loadMovimientosEspeciesFiscales("-1");
                                            mefe.newLine();
                                            mefe.IDMOVIMIENTO = idMovimientoEnvio;
                                            mefe.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                                            mefe.CODPAIS = cmbPais.SelectedValue;
                                            mefe.CODIGOADUANA = cmbAduanaOrigen.SelectedValue;
                                            mefe.RANGOINICIAL = mef.RANGOINICIAL;
                                            mefe.RANGOFINAL = mef.RANGOFINAL;
                                            mefe.CANTIDAD = mef.CANTIDAD;
                                            mefe.FECHA = DateTime.Now.ToString();
                                            mefe.CODTIPOMOVIMIENTO = "5";   //1 Envío a cliente
                                            mefe.CODESTADO = "3";   //Enviado a cliente
                                            mefe.IDUSUARIO = Session["IdUsuario"].ToString();
                                            mefe.IDCOMPRA = mef.IDCOMPRA;
                                            mefe.commitLine();
                                            mefe.actualizar();

                                            //nuevo
                                            rangoInicial = mefe.RANGOINICIAL;
                                            rangoFinal = mefe.RANGOFINAL;
                                            //

                                            //guardar nuevo registro con las series que se envian al cliente
                                            efc.loadEspeciesFiscalesClientesXIdMov("-1");
                                            efc.newLine();
                                            efc.IDMOVIMIENTO = idMovimientoEnvio;
                                            efc.IDCLIENTE = cmbCliente.SelectedValue;
                                            efc.OBSERVACION = "";
                                            efc.commitLine();
                                            efc.actualizar();

                                            cantidadSolicitada -= mef.CANTIDAD;
                                            cantidadDisponible -= mef.CANTIDAD;
                                            mef.regSiguiente();

                                            llenarBitacora("Rango asignado por", Session["IdUsuario"].ToString(), (rangoInicial + " " + rangoFinal));
                                        }
                                    }
                                    //registrarMensaje("Asignación de " + cmbEspecieFiscal.Text + " al cliente " + cmbCliente.Text + " realizado exitosamente");
                                    registrarMensaje("Asignación de " + cmbEspecieFiscal.Text + " del " + rangoInicial + " al " + rangoFinal + " realizado exitosamente");
                                    limpiarControles();
                                    //mpEspeciesFiscales.SelectedIndex = 1;
                                    //llenarBitacora("Se ingresó la compra de " + cmbEspecieFiscal.Text + " con un rango del " + cef.RANGOINICIAL + " al " + cef.RANGOFINAL, cef.IDUSUARIO, nuevoId);
                                    //rgRangoEspeciesFiscales.Rebind();
                                    rgRangoEspeciesFiscales.Rebind();
                                }
                                else
                                    registrarMensaje("La cantidad solicitada no esta disponible, solo existen " + cantidad + " series de esta especie fiscal");
                            }
                            else
                                registrarMensaje("No existen series de esta especie fiscal en la aduana origen");
                        }
                        else
                            registrarMensaje("La cantidad debe ser mayor que cero");
                    }
                    else
                        registrarMensaje("La cantidad no puede estar vacía");
                }
                else
                    registrarMensaje("La especie fiscal no puede estar vacía");
            }
            else
                registrarMensaje("La aduana origen no puede estar vacía");
        }
        else
            registrarMensaje("País no puede estar vacío");
    }
    catch (Exception ex)
    {
        logError(ex.Message, Session["IdUsuario"].ToString(), "EspeciesFiscales");
    }
    finally
    {
        desconectar();
    }
}

    protected void btnLimpiar_Click(object sender, ImageClickEventArgs e)
    {
        limpiarControles();
    }

    private void limpiarControles()
    {
        try
        {
            cargarDatosInicioRangos();
            cmbAduanaOrigen.ClearSelection();
            cmbCliente.ClearSelection();
            cmbEspecieFiscal.ClearSelection();
            txtCantidad.Text = "";
            llenarGridRangoEspeciesFiscales();
        }
        catch { }
    }

    protected void cmbAduanaOrigen_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        //loadEspeciesFiscales();
        llenarGridRangoEspeciesFiscales();
    }

    protected void cmbEspecieFiscal_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        llenarGridRangoEspeciesFiscales();
    }
    #endregion
}