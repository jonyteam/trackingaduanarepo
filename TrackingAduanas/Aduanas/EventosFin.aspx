﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="EventosFin.aspx.cs" Inherits="EventosFin" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanelFlujoGuias" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 | args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }
                   
            </script>
        </telerik:RadScriptBlock>
        <input id="edInstruccion" runat="server" type="hidden" />
        <input id="edEvento" runat="server" type="hidden" />
        <input id="edFecha" runat="server" type="hidden" />
        <input id="edHora" runat="server" type="hidden" />
        <input id="edObservacion" runat="server" type="hidden" />
        <telerik:RadGrid ID="rgEventos" runat="server" AllowFilteringByColumn="True" AllowSorting="True"
            AutoGenerateColumns="False" GridLines="None" Height="410px" OnNeedDataSource="rgEventos_NeedDataSource"
            AllowPaging="True" ShowFooter="True" ShowStatusBar="True" PageSize="20" OnInit="rgEventos_Init"
            OnItemCommand="rgEventos_ItemCommand">
            <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
            <MasterTableView DataKeyNames="IdInstruccion" CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros."
                NoMasterRecordsText="No hay eventos." GroupLoadMode="Client">
                <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                    ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px"></HeaderStyle>
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px"></HeaderStyle>
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="IdInstruccion" HeaderText="Instruccion No." UniqueName="IdInstruccion"
                        FilterControlWidth="60%">
                        <ItemStyle Width="10%" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn UniqueName="Codigo" SortExpression="Orden" HeaderText="Eventos"
                        DataField="Codigo" AllowFiltering="false" ShowFilterIcon="false">
                        <%--<HeaderStyle HorizontalAlign="Center" />--%>
                        <ItemTemplate>
                            <telerik:RadComboBox ID="edEventos" runat="server" DataValueField="Codigo" DataTextField="Descripcion"
                                DataSource='<%# GetEventos() %>' SelectedValue='<%# Bind("IdEvento") %>' Width="80%"
                                CausesValidation="false" Enabled="false" ForeColor="Black" />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="FechaInicio" HeaderText="Fecha Inicial" UniqueName="FechaInicio"
                        FilterControlWidth="60%">
                        <ItemStyle Width="10%" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Fecha Final" AllowFiltering="false" ShowFilterIcon="false">
                        <HeaderStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <telerik:RadDatePicker ID="dtFechaSellado" runat="server" Width="55%" Culture="es-HN"
                                EnableTyping="False">
                                <Calendar ID="Calendar1" runat="server" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"
                                    UseRowHeadersAsSelectors="False">
                                </Calendar>
                                <DateInput ID="DateInput1" runat="server" DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"
                                    ReadOnly="True">
                                </DateInput><DatePopupButton HoverImageUrl="" ImageUrl="" />
                            </telerik:RadDatePicker>
                            <telerik:RadTimePicker ID="dtHoraSellado" runat="server" Width="37%" Culture="es-HN"
                                EnableTyping="True">
                                <TimeView ID="TimeView2" runat="server" CellSpacing="-1" Columns="4" Culture="es-HN"
                                    Interval="00:30:00" TimeFormat="T">
                                </TimeView>
                                <TimePopupButton HoverImageUrl="" ImageUrl="" />
                            </telerik:RadTimePicker>
                        </ItemTemplate>
                        <ItemStyle Width="16%" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Observación" AllowFiltering="false" ShowFilterIcon="false">
                        <ItemTemplate>
                            <telerik:RadTextBox ID="edObservacion" runat="server" Width="100%">
                            </telerik:RadTextBox>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridButtonColumn ButtonType="ImageButton" HeaderText="Sellar Estado" CommandName="Sellar"
                        ImageUrl="~/Images/24/lock2_24.png" UniqueName="btnSellar">
                        <ItemStyle Width="75px" />
                        <HeaderStyle Width="75px" />
                    </telerik:GridButtonColumn>
                </Columns>
            </MasterTableView>
            <HeaderStyle Width="180px" />
            <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                <Selecting AllowRowSelect="True" />
                <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                    DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                    DropHereToReorder="Suelte aquí para Re-Ordenar" />
                <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
            </ClientSettings>
            <FilterMenu EnableTheming="True">
                <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
            </FilterMenu>
            <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
            <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
        </telerik:RadGrid>
        <table width="100%">
            <tr>
                <td align="center">
                    <asp:ImageButton ID="btnLimpiar" runat="server" ImageUrl="~/Images/24/document_plain_24.png"
                        OnClick="btnLimpiar_Click" ToolTip="Limpiar Pantalla" />
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
</asp:Content>
