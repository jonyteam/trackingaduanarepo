﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="CargaUso.aspx.cs" Inherits="GenerarArchivoSAPDinant" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 734px;
        }
        .style4
        {
            width: 155px;
        }
        .style5
        {
            width: 107px;
        }
        .style6
        {
            width: 122px;
        }
        .style8
        {
            width: 105px;
        }
        .style9
        {
            width: 109px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart" style="text-align: left">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                    //<![CDATA[
                    
                function pnlRequestStarted(ajaxPanel, eventArgs) {
                    try {
                        if ((eventArgs.EventTarget == "<%= btnSalvar.UniqueID %>")) {
                            eventArgs.EnableAjax = false;
                        }
                    }
                    catch (err)
                { }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                    }
                }
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }
                   
            </script>
        </telerik:RadScriptBlock>
        <table border="1" style="width: 55%">
            <tr>
                <td align="center">
                    <input id="iTipoFlete" runat="server" type="hidden" />
                    <asp:ImageButton ID="btnBuscar" runat="server" 
                        ImageUrl="~/Images/24/view_24.png" OnClick="btnBuscar_Click" />
                </td>
            </tr>
        </table>
        <br />
        <table class="style1">
            <tr>
                <td class="style6">
                    <asp:CheckBox ID="chbUso" runat="server" Text="Uso de Especies" />
                </td>
                <td class="style8">
                    <asp:CheckBox ID="chbCompras" runat="server" Text="Compras" />
                </td>
                <td class="style9">
                    <asp:CheckBox ID="chbTraslados" runat="server" Text="Traslados"/>
                </td>
                <td class="style5">
                    <asp:CheckBox ID="chbAnulacion" runat="server" Text="Anulaciones" />
                </td>
                <td class="style4">
                    <asp:CheckBox ID="chbLiberacion" runat="server" Text="Liberacion" />
                </td>
            </tr>
        </table>
        <br />
        <telerik:RadGrid ID="rgCarga" runat="server" 
            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" 
            GridLines="None" Width="2302px" onitemcommand="RadGrid1_ItemCommand" 
            onneeddatasource="RadGrid1_NeedDataSource">
            <ClientSettings AllowColumnsReorder="True">
                <Selecting AllowRowSelect="True" />
            </ClientSettings>
             <ExportSettings HideStructureColumns="true" 
                FileName ="Reporte Promoda/Hagamoda" OpenInNewWindow="True">  <Excel Format ="Biff" />  </ExportSettings>
            <MasterTableView CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" ExportToPdfText="Export to PDF" ShowAddNewRecordButton = "false" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                    Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                    Visible="True">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="Fec.docu." 
                        FilterControlAltText="Filter IdInstruccion column" HeaderText="Fec.docu." 
                        UniqueName="IdInstruccion">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Clase Dto." 
                        FilterControlAltText="Filter Proveedor column" HeaderText="Clase Dto." 
                        UniqueName="Proveedor">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Sociedad" 
                        FilterControlAltText="Filter Nombre column" HeaderText="Sociedad" 
                        UniqueName="Nombre">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Fec.Con." 
                        FilterControlAltText="Filter DocumentoNo column" HeaderText="Fec.Con." 
                        UniqueName="DocumentoNo">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Periodo" 
                        FilterControlAltText="Filter DocumentoNo column" 
                        HeaderText="Periodo" UniqueName="DocumentoNo">
                     <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Moneda" 
                        FilterControlAltText="Filter CantidadBultos column" 
                        HeaderText="Moneda" UniqueName="CantidadBultos">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="No.Dto." 
                        FilterControlAltText="Filter PesoKgs column" HeaderText="No.Dto." 
                        UniqueName="PesoKgs">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Referencia" 
                        FilterControlAltText="Filter PesoMiami column" HeaderText="Referencia" 
                        UniqueName="PesoMiami">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TxtCabDto" 
                        FilterControlAltText="Filter IngresoSwissport column" 
                        HeaderText="TxtCabDto" UniqueName="IngresoSwissport">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Divi." 
                        FilterControlAltText="Filter SalidaSwissport column" 
                        HeaderText="Divi." UniqueName="SalidaSwissport">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CLAVE" 
                        FilterControlAltText="Filter Producto column" HeaderText="CLAVE" 
                        UniqueName="Producto">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Codigo" 
                        FilterControlAltText="Filter GC1.Fecha column" HeaderText="Cuenta" 
                        UniqueName="GC1.Fecha">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="&quot;&quot;" 
                        FilterControlAltText="Filter CME column" HeaderText="CME" UniqueName="CME">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ImporteMD" 
                        FilterControlAltText="Filter ArribodeCarga column" HeaderText="ImporteMD" 
                        UniqueName="ArribodeCarga">
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="&quot;&quot;" 
                        FilterControlAltText="Filter IndImpto column" HeaderText="Ind.Impto." 
                        UniqueName="IndImpto">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="&quot;&quot;" 
                        FilterControlAltText="Filter IndicadorCalculaimpuesto column" 
                        HeaderText="Indicador Calcula impuesto" UniqueName="IndicadorCalculaimpuesto">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CeCo" 
                        FilterControlAltText="Filter CeCo column" HeaderText="CeCo" UniqueName="CeCo">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="&quot;&quot;" 
                        FilterControlAltText="Filter Orden column" HeaderText="Orden" 
                        UniqueName="Orden">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="&quot;&quot;" 
                        FilterControlAltText="Filter CeBe column" HeaderText="CeBe" UniqueName="CeBe">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Asignacion" 
                        FilterControlAltText="Filter ENVIODEPRELIQUIDACION column" 
                        HeaderText="Asignacion" UniqueName="ENVIODEPRELIQUIDACION">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Texto" 
                        FilterControlAltText="Filter APROBACIONDEPRELIQUIDACION column" 
                        HeaderText="Texto" 
                        UniqueName="APROBACIONDEPRELIQUIDACION">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ImporteML" 
                        FilterControlAltText="Filter ENVIODEBOLETIN column" 
                        HeaderText="ImporteML" DataFormatString="{0:###.##}" 
                        UniqueName="ENVIODEBOLETIN">
                        <ItemStyle HorizontalAlign="Left"/>
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="&quot;&quot;" 
                        FilterControlAltText="Filter FechaValor column" HeaderText="Fecha Valor" 
                        UniqueName="FechaValor">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="&quot;&quot;" 
                        FilterControlAltText="Filter Referencia1 column" HeaderText="Referencia 1" 
                        UniqueName="Referencia1">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="&quot;&quot;" 
                        FilterControlAltText="Filter Referencia2 column" HeaderText="Referencia 2" 
                        UniqueName="Referencia2">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Referencia 3" 
                        FilterControlAltText="Filter PagodeImpuestos column" 
                        HeaderText="Referencia 3" UniqueName="PagodeImpuestos">
                    <ItemStyle HorizontalAlign="Left"/>
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        <br />
        <br />
        <table width="100%">
            <tr align="center">
                <td colspan="6" align="center">
                    <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="~/Images/gris.png"/>
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
    <%--<GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>--%>
</asp:Content>
