﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;
using System.Text;
using Telerik.Web.UI.GridExcelBuilder;

public partial class ReporteEventos : Utilidades.PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgInstrucciones.FilterMenu);
        rgInstrucciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgInstrucciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Eventos", "Reporte Eventos");
            cargarDatosInicio();
        }
    }

    protected void cargarDatosInicio()
    {
        try
        {

        }
        catch { }
    }

    protected void rgInstrucciones_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (!e.IsFromDetailTable)
        {
            llenarGridInstrucciones();
        }
    }

    private void llenarGridInstrucciones()
    {
        try
        {
            conectar();
          
            InstruccionesBO ev = new InstruccionesBO(logApp);

            ev.Consulta_Gastos(dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"));
            rgInstrucciones.DataSource = ev.TABLA;
            rgInstrucciones.DataBind();
        }
        catch (Exception)
        { }
        finally
        {
            desconectar();
        }
    }

    protected void rgInstrucciones_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
    {
        try
        {
            conectar();
            EventosBO ev = new EventosBO(logApp);
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            switch (e.DetailTableView.DataMember)
            {
                case "DetalleEventos":
                    {
                        string idInstruccion = dataItem["IdInstruccion"].Text;
                        ev.loadEventosAllXInstruccion(idInstruccion);
                        e.DetailTableView.DataSource = ev.TABLA;
                        break;
                    }
            }
        }
        catch (Exception)
        { }
        finally
        {
            desconectar();
        }
    }

    protected void rgInstrucciones_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == RadGrid.ExportToExcelCommandName)
                ConfigureExport();
        }
        catch (Exception)
        { }
    }

    private void ConfigureExport()
    {
        String filename = "Reporte Gastos Masivos " + DateTime.Now.ToShortDateString();
        rgInstrucciones.GridLines = GridLines.Both;
        rgInstrucciones.ExportSettings.FileName = filename;
        rgInstrucciones.ExportSettings.IgnorePaging = true;

        rgInstrucciones.ExportSettings.OpenInNewWindow = false;
        rgInstrucciones.ExportSettings.ExportOnlyData = true;
       
    }

    public override bool CanGoBack { get { return false; } }

    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {  
        
             llenarGridInstrucciones();
            rgInstrucciones.Visible = true;
      
    }

    private void registrarMensaje2(string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("alert('" + mensaje + "');");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "mensaje", sb.ToString(), false);
    }

    protected void rgInstrucciones_ExcelExportCellFormatting(object sender, ExcelExportCellFormattingEventArgs e)
    {


    }


    //protected void RadGrid1_ExcelMLExportRowCreated(object source, ExcelExportCellFormattingEventArgs e)
    //{

    //    if (e.RowType == ExcelExportCellFormattingEventArgs.HeaderRow)
    //    {

    //        //Add custom styles to the desired cells
    //        CellElement cell = e.Row.Cells.GetCellByName("Number");
    //        cell.StyleValue = "normalHeaderStyle";

    //        cell = e.Row.Cells.GetCellByName("Name");
    //        cell.StyleValue = "normalHeaderStyle";

    //        cell = e.Row.Cells.GetCellByName("Weight in Grams");
    //        cell.StyleValue = "normalHeaderStyle";

    //        cell = e.Row.Cells.GetCellByName("Energie");
    //        cell.StyleValue = "perTotalHeaderStyle";

    //        cell = e.Row.Cells.GetCellByName("Eiweiss");
    //        cell.StyleValue = "perTotalHeaderStyle";

    //        cell = e.Row.Cells.GetCellByName("Fett");
    //        cell.StyleValue = "perTotalHeaderStyle";

    //        cell = e.Row.Cells.GetCellByName("KH");
    //        cell.StyleValue = "perTotalHeaderStyle";

    //        cell = e.Row.Cells.GetCellByName("Nahrungsfasern, total");
    //        cell.StyleValue = "perTotalHeaderStyle";



    //        cell = e.Row.Cells.GetCellByName("Energie1");
    //        cell.StyleValue = "per100HeaderStyle";

    //        cell = e.Row.Cells.GetCellByName("Eiweiss1");
    //        cell.StyleValue = "per100HeaderStyle";

    //        cell = e.Row.Cells.GetCellByName("Fett1");
    //        cell.StyleValue = "per100HeaderStyle";

    //        cell = e.Row.Cells.GetCellByName("KH1");
    //        cell.StyleValue = "per100HeaderStyle";

    //        cell = e.Row.Cells.GetCellByName("Nahrungsfasern, total1");
    //        cell.StyleValue = "per100HeaderStyle";



    //        cell = e.Row.Cells.GetCellByName("Energie CAL.");
    //        cell.StyleValue = "perTotalHeaderStyle";
    //    }



    //    if (e.RowType == ExcelExportCellFormattingEventArgs.DataRow)
    //    {


    //        //Add custom styles to the desired cells
    //        CellElement cell = e.Row.Cells.GetCellByName("Number");
    //        cell.StyleValue = cell.StyleValue == "itemStyle" ? "itemNormalStyle" : "itemNormalStyle";

    //        cell = e.Row.Cells.GetCellByName("Name");
    //        cell.StyleValue = cell.StyleValue == "itemStyle" ? "itemNormalStyle" : "itemNormalStyle";

    //        cell = e.Row.Cells.GetCellByName("Weight in Grams");
    //        cell.StyleValue = cell.StyleValue == "itemStyle" ? "itemNormalStyle" : "itemNormalStyle";

    //        cell = e.Row.Cells.GetCellByName("Energie");
    //        cell.StyleValue = cell.StyleValue == "itemStyle" ? "itemNutrientStyle" : "itemNutrientStyle";

    //        cell = e.Row.Cells.GetCellByName("Eiweiss");
    //        cell.StyleValue = cell.StyleValue == "itemStyle" ? "itemNutrientStyle" : "itemNutrientStyle";

    //        cell = e.Row.Cells.GetCellByName("Fett");
    //        cell.StyleValue = cell.StyleValue == "itemStyle" ? "itemNutrientStyle" : "itemNutrientStyle";

    //        cell = e.Row.Cells.GetCellByName("KH");
    //        cell.StyleValue = cell.StyleValue == "itemStyle" ? "itemNutrientStyle" : "itemNutrientStyle";

    //        cell = e.Row.Cells.GetCellByName("Nahrungsfasern, total");
    //        cell.StyleValue = cell.StyleValue == "itemStyle" ? "itemNutrientStyle" : "itemNutrientStyle";

    //        cell = e.Row.Cells.GetCellByName("Energie1");
    //        cell.StyleValue = cell.StyleValue == "itemStyle" ? "itemNutrientStyle" : "itemNutrientStyle";

    //        cell = e.Row.Cells.GetCellByName("Eiweiss1");
    //        cell.StyleValue = cell.StyleValue == "itemStyle" ? "itemNutrientStyle" : "itemNutrientStyle";

    //        cell = e.Row.Cells.GetCellByName("Fett1");
    //        cell.StyleValue = cell.StyleValue == "itemStyle" ? "itemNutrientStyle" : "itemNutrientStyle";

    //        cell = e.Row.Cells.GetCellByName("KH1");
    //        cell.StyleValue = cell.StyleValue == "itemStyle" ? "itemNutrientStyle" : "itemNutrientStyle";

    //        cell = e.Row.Cells.GetCellByName("Nahrungsfasern, total1");
    //        cell.StyleValue = cell.StyleValue == "itemStyle" ? "itemNutrientStyle" : "itemNutrientStyle";

    //        cell = e.Row.Cells.GetCellByName("Energie CAL.");
    //        cell.StyleValue = cell.StyleValue == "itemStyle" ? "itemNutrientStyle" : "itemNutrientStyle";

    //        if (!isConfigured)
    //        {
    //            //Set Worksheet name
    //            e.Worksheet.Name = "Nährwerte";

    //            //Set Column widths
    //            foreach (ColumnElement column in e.Worksheet.Table.Columns)
    //            {
    //                if (e.Worksheet.Table.Columns.IndexOf(column) == 1)
    //                    column.Width = Unit.Point(180); //set width 180 to ProductName column
    //                else
    //                    column.Width = Unit.Point(80); //set width 80 to the rest of the columns
    //            }

    //            //Set Page options
    //            PageSetupElement pageSetup = e.Worksheet.WorksheetOptions.PageSetup;
    //            pageSetup.PageLayoutElement.IsCenteredVertical = true;
    //            pageSetup.PageLayoutElement.IsCenteredHorizontal = true;
    //            pageSetup.PageMarginsElement.Left = 0.5;
    //            pageSetup.PageMarginsElement.Top = 0.5;
    //            pageSetup.PageMarginsElement.Right = 0.5;
    //            pageSetup.PageMarginsElement.Bottom = 0.5;
    //            pageSetup.PageLayoutElement.PageOrientation = PageOrientationType.Landscape;

    //            //Freeze panes
    //            //e.Worksheet.WorksheetOptions.AllowFreezePanes = true;
    //            //e.Worksheet.WorksheetOptions.LeftColumnRightPaneNumber = 3;
    //            //e.Worksheet.WorksheetOptions.TopRowBottomPaneNumber = 1;
    //            //e.Worksheet.WorksheetOptions.SplitHorizontalOffset = 1;
    //            //e.Worksheet.WorksheetOptions.SplitVerticalOffest = 1;

    //            //e.Worksheet.WorksheetOptions.ActivePane = 4;
    //            isConfigured = true;
    //        }
    //    }
    //}

    //protected void RadGrid1_ExcelMLExportStylesCreated(object source, GridExportExcelMLStyleCreatedArgs e)
    //{
    //    //Add currency and percent styles
    //    StyleElement pertotalHeaderStyle = new StyleElement("perTotalHeaderStyle");
    //    pertotalHeaderStyle.FontStyle.Color = System.Drawing.Color.Black;
    //    pertotalHeaderStyle.FontStyle.Bold = true;
    //    pertotalHeaderStyle.FontStyle.FontName = "Calibri";
    //    pertotalHeaderStyle.FontStyle.Size = 10.0;
    //    e.Styles.Add(pertotalHeaderStyle);

    //    StyleElement per100HeaderStyle = new StyleElement("per100HeaderStyle");
    //    per100HeaderStyle.FontStyle.Color = System.Drawing.Color.Black;
    //    per100HeaderStyle.FontStyle.Bold = true;
    //    per100HeaderStyle.FontStyle.FontName = "Calibri";
    //    per100HeaderStyle.FontStyle.Size = 10.0;
    //    e.Styles.Add(per100HeaderStyle);

    //    StyleElement normalHeaderStyle = new StyleElement("normalHeaderStyle");
    //    normalHeaderStyle.FontStyle.Color = System.Drawing.Color.Black;
    //    normalHeaderStyle.FontStyle.Bold = true;
    //    normalHeaderStyle.FontStyle.FontName = "Calibri";
    //    normalHeaderStyle.FontStyle.Size = 10.0;
    //    e.Styles.Add(normalHeaderStyle);

    //    StyleElement itemStyle = new StyleElement("itemNormalStyle");
    //    itemStyle.FontStyle.Color = System.Drawing.Color.Black;
    //    itemStyle.FontStyle.Bold = false;
    //    itemStyle.FontStyle.FontName = "Calibri";
    //    itemStyle.FontStyle.Size = 10.0;
    //    e.Styles.Add(itemStyle);

    //    StyleElement itemNutrientStyle = new StyleElement("itemNutrientStyle");
    //    itemNutrientStyle.FontStyle.Color = System.Drawing.Color.Black;
    //    itemNutrientStyle.NumberFormat.FormatType = NumberFormatType.Standard;
    //    itemNutrientStyle.FontStyle.Bold = false;
    //    itemNutrientStyle.FontStyle.FontName = "Calibri";
    //    itemNutrientStyle.FontStyle.Size = 10.0;
    //    e.Styles.Add(itemNutrientStyle);


    //    //Apply background colors
    //    foreach (StyleElement style in e.Styles)
    //    {
    //        if (style.Id == "perTotalHeaderStyle")
    //        {
    //            style.InteriorStyle.Pattern = InteriorPatternType.Solid;
    //            style.InteriorStyle.Color = System.Drawing.Color.Yellow;

    //        }

    //        if (style.Id == "per100HeaderStyle")
    //        {
    //            style.InteriorStyle.Pattern = InteriorPatternType.Solid;
    //            style.InteriorStyle.Color = System.Drawing.Color.YellowGreen;
    //        }

    //        if (style.Id == "normalHeaderStyle")
    //        {
    //            style.InteriorStyle.Pattern = InteriorPatternType.Solid;
    //            style.InteriorStyle.Color = System.Drawing.Color.Gray;
    //        }

    //        if (style.Id == "itemNormalStyle")
    //        {
    //            style.InteriorStyle.Pattern = InteriorPatternType.None;
    //        }

    //        if (style.Id == "itemNutrientStyle")
    //        {
    //            style.InteriorStyle.Pattern = InteriorPatternType.None;
    //        }
    //    }

    //}

    protected void rgInstrucciones_ExcelMLExportStylesCreated(object sender, Telerik.Web.UI.GridExcelBuilder.GridExportExcelMLStyleCreatedArgs e)
    {

        StyleElement pertotalHeaderStyle = new StyleElement("FacturaDinnant");
        pertotalHeaderStyle.FontStyle.Color = System.Drawing.Color.Black;
        pertotalHeaderStyle.FontStyle.Bold = true;
        pertotalHeaderStyle.FontStyle.FontName = "Calibri";
        pertotalHeaderStyle.FontStyle.Size = 10.0;
        e.Styles.Add(pertotalHeaderStyle);

    }
    protected void rgInstrucciones_ExportCellFormatting(object sender, ExportCellFormattingEventArgs e)
    {

      
    }
    protected void rgInstrucciones_ExcelMLExportRowCreated(object sender, Telerik.Web.UI.GridExcelBuilder.GridExportExcelMLRowCreatedArgs e)
    {
       //if (e.RowType == ExcelExportCellFormattingEventArgs.HeaderRow)
        {

            //Add custom styles to the desired cells
            CellElement cell = e.Row.Cells.GetCellByName("Factura");
            cell.StyleValue = "normalHeaderStyle";
        }
    }   
    protected void rgInstrucciones_ExcelMLWorkBookCreated(object sender, Telerik.Web.UI.GridExcelBuilder.GridExcelMLWorkBookCreatedEventArgs e)
    {

    }
}