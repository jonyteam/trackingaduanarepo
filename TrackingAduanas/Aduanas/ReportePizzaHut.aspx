﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReportePizzaHut.aspx.cs"
    Inherits="ReporteEventos" MasterPageFile="~/Site.master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style3
        {
            width: 586px;
            height: 91px;
        }
        .style5
        {
            width: 86px;
        }
        .style7
        {
            width: 63px;
        }
        .style8
        {}
        .style9
        {
            width: 185px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <table class="style3">
        <tr>
            <td class="style5">
                <asp:Label ID="Label3" runat="server" Text="Fecha de Inicio"></asp:Label>
            </td>
            <td class="style9">
                <telerik:RadDatePicker ID="fechainicio" Runat="server">
                </telerik:RadDatePicker>
                <asp:requiredfieldvalidator runat="server" id="RequiredFieldValidator1" controltovalidate="fechainicio"
                    errormessage="Ingrese Fecha" forecolor="Red" />
            </td>
            <td class="style7">
                <asp:Label ID="Label4" runat="server" Text="Fecha Final"></asp:Label>
            </td>
            <td>
                <telerik:RadDatePicker ID="fechafin" Runat="server">
                </telerik:RadDatePicker>
                                <asp:requiredfieldvalidator runat="server" id="RequiredFieldValidator2" controltovalidate="fechafin"
                    errormessage="Ingrese Fecha" forecolor="Red" />
            </td>
        </tr>
        <tr>
            <td class="style5">
                <asp:Label ID="Label5" runat="server" Text="Cliente" Visible="False"></asp:Label>
            </td>
            <td class="style8" colspan="3">
                <telerik:RadComboBox ID="RadComboBox1" Runat="server" Width="500px" 
                    Visible="False">
                </telerik:RadComboBox>
            </td>
        </tr>
        <tr>
            <td class="style5">
                &nbsp;</td>
            <td class="style9">
                &nbsp;</td>
            <td class="style7">
                <telerik:RadButton ID="RadButton2" runat="server" Text="Buscar" 
                    onclick="RadButton2_Click">
                </telerik:RadButton>
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <br />
            <br />
    <telerik:RadGrid ID="Reporte" runat="server" AllowFilteringByColumn="True" 
        AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
        CellSpacing="0" GridLines="None">
        <ClientSettings AllowColumnsReorder="True">
            <Selecting AllowRowSelect="True" />
        </ClientSettings>
<MasterTableView CommandItemDisplay ="Top">
<CommandItemSettings ShowExportToExcelButton = "true" ShowAddNewRecordButton ="false" ></CommandItemSettings>

<RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
<HeaderStyle Width="20px"></HeaderStyle>
</RowIndicatorColumn>

<ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
<HeaderStyle Width="20px"></HeaderStyle>
</ExpandCollapseColumn>

    <Columns>
        <telerik:GridBoundColumn DataField="HojaRuta" 
            FilterControlAltText="Filter HojaRuta column" FilterControlWidth="60px" 
            HeaderText="Hoja Ruta" UniqueName="HojaRuta">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="REFOperacion" HeaderText="REF Operecion" 
            UniqueName="operacion" FilterControlWidth="80px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Empresa" 
            FilterControlAltText="Filter Empresa column" HeaderText="Empresa" 
            UniqueName="Empresa" FilterControlWidth="120px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="[Fauca/Dua]" 
            FilterControlAltText="Filter FaucaDUA column" HeaderText="Fauca/DUA" 
            UniqueName="FaucaDUA" FilterControlWidth="50px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Producto" 
            FilterControlAltText="Filter Producto column" HeaderText="Producto" 
            UniqueName="Producto" FilterControlWidth="100px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="HBL/HAWB" 
            FilterControlAltText="Filter HBLHAWB column" HeaderText="HBL/HAWB" 
            UniqueName="HBLHAWB" FilterControlWidth="45px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="DescripcionContenedores" 
            FilterControlAltText="Filter DescripcionContenedores column" 
            HeaderText="Descripcion Contenedores" UniqueName="DescripcionContenedores" 
            FilterControlWidth="60px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Origen" 
            FilterControlAltText="Filter Origen column" HeaderText="Origen" 
            UniqueName="Origen" FilterControlWidth="60px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Aduana" 
            FilterControlAltText="Filter Aduana column" HeaderText="Aduana" 
            UniqueName="Aduana" FilterControlWidth="50px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Regimen" 
            FilterControlAltText="Filter Regimen column" HeaderText="Regimen" 
            UniqueName="Regimen" FilterControlWidth="30px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="ETA" 
            FilterControlAltText="Filter ETA column" HeaderText="ETA" UniqueName="ETA" 
            FilterControlWidth="100px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="CopiasDOCSREC" 
            FilterControlAltText="Filter CopiasDOCSREC column" HeaderText="Copias DOCS REC" 
            UniqueName="CopiasDOCSREC" FilterControlWidth="100px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="PermisoIngresadoInicio" 
            FilterControlAltText="Filter PermisoIngresadoInicio column" 
            HeaderText="Permiso Ingresado Inicio" UniqueName="PermisoIngresadoInicio">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="PermisoIngresadoFin" 
            FilterControlAltText="Filter PermisoIngresadoFin column" 
            HeaderText="Permiso Ingresado Fin" UniqueName="PermisoIngresadoFin" 
            FilterControlWidth="100px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="PermisoRecibido" 
            FilterControlAltText="Filter PermisoRecibido column" 
            HeaderText="Permiso Recibido" UniqueName="PermisoRecibido" 
            FilterControlWidth="100px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="EnviodePrepoliza" 
            FilterControlAltText="Filter EnviodePrepoliza column" 
            HeaderText="Envio de Prepoliza" UniqueName="EnviodePrepoliza" 
            FilterControlWidth="100px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="AutorizacionPrepoliza" 
            FilterControlAltText="Filter AutorizacionPrepoliza column" 
            HeaderText="AutorizacionPrepoliza" UniqueName="AutorizacionPrepoliza" 
            FilterControlWidth="100px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="EnvioPoliza" 
            FilterControlAltText="Filter EnvioPoliza column" HeaderText="Envio Poliza" 
            UniqueName="EnvioPoliza" FilterControlWidth="100px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="PolizaIngresada" 
            FilterControlAltText="Filter PolizaIngresada column" 
            HeaderText="Poliza Ingresada" UniqueName="PolizaIngresada" 
            FilterControlWidth="100px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="PagoImpuesto" 
            FilterControlAltText="Filter PagoImpuesto column" HeaderText="Pago Impuesto" 
            UniqueName="PagoImpuesto" FilterControlWidth="100px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="ValidacionElectronicaVerde" 
            FilterControlAltText="Filter ValidacionElectronicaVerde column" 
            HeaderText="Validacion Electronica Verde" 
            UniqueName="ValidacionElectronicaVerde" FilterControlWidth="100px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="ValidacionElectronicaRojo" 
            FilterControlAltText="Filter ValidacionElectronicaRojo column" 
            HeaderText="Validacion Electronica Rojo" 
            UniqueName="ValidacionElectronicaRojo" FilterControlWidth="100px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Liberacion" 
            FilterControlAltText="Filter Liberacion column" HeaderText="Liberacion" 
            UniqueName="Liberacion" FilterControlWidth="100px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="SalidadePuerto" 
            FilterControlAltText="Filter SalidadePuerto column" 
            HeaderText="Salida de Puerto" UniqueName="SalidadePuerto" 
            FilterControlWidth="100px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="EntregaBodega" 
            FilterControlAltText="Filter EntregaBodega column" HeaderText="EntregaBodega" 
            UniqueName="EntregaBodega" FilterControlWidth="100px">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Observaciones" 
            FilterControlAltText="Filter Observaciones column" HeaderText="Observaciones" 
            UniqueName="Observaciones" FilterControlWidth="120px">
        </telerik:GridBoundColumn>
    </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
</MasterTableView>
<ExportSettings>
<Excel Format =Biff />
</ExportSettings>
<FilterMenu EnableImageSprites="False"></FilterMenu>
    </telerik:RadGrid>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgUsuarios">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgUsuarios" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rgRoles">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgRoles" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>
