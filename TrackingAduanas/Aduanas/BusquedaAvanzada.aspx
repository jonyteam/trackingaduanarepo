﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="BusquedaAvanzada.aspx.cs" Inherits="BusquedaAvanzada" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart" Style="text-align: left">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                //<![CDATA[
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }

                function pnlRequestStarted(ajaxPanel, eventArgs) {
                    try {
                        if ((eventArgs.EventTarget == "<%= btnExport.UniqueID %>") || (eventArgs.EventTarget == "<%= btnExport2.UniqueID %>")) {
                            eventArgs.EnableAjax = false;
                        }
                    }
                    catch (err)
                    { }
                }
            </script>
        </telerik:RadScriptBlock>
        <telerik:RadMultiPage ID="mpBusquedaAvanzada" runat="server">
            <telerik:RadPageView ID="RadPageView1" runat="server" Width="100%">
                <table style="width: 50%">
                    <tr>
                        <td style="width: 10%">
                            <asp:Label ID="LabelSimbolo" runat="server" Text="Símbolo"></asp:Label>
                        </td>
                        <td style="width: 30%">
                            <asp:Label ID="LabelCampo" runat="server" Text="Campo"></asp:Label>
                        </td>
                        <td style="width: 20%">
                            <asp:Label ID="LabelOperador" runat="server" Text="Operador"></asp:Label>
                        </td>
                        <td style="width: 20%">
                            <asp:Label ID="LabelValor" runat="server" Text="Valor"></asp:Label>
                        </td>
                        <td style="width: 10%">
                            <asp:Label ID="LabelCriterio" runat="server" Text="Criterio"></asp:Label>
                        </td>
                        <td style="width: 10%"></td>
                    </tr>
                    <tr>
                        <td>
                            <input id="edCampos" runat="server" type="hidden" />
                            <input id="edCriterio" runat="server" type="hidden" />
                            <telerik:RadComboBox ID="cmbSimbolo" runat="server" Width="80%">
                                <Items>
                                    <telerik:RadComboBoxItem runat="server" />
                                    <telerik:RadComboBoxItem runat="server" Text="(" Value="(" />
                                    <telerik:RadComboBoxItem runat="server" Text=")" Value=")" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <telerik:RadComboBox ID="cmbCampo" runat="server" Width="90%" AutoPostBack="True"
                                OnSelectedIndexChanged="cmbCampo_SelectedIndexChanged" Culture="es-ES">
                                <Items>
                                    <telerik:RadComboBoxItem />
                                    <telerik:RadComboBoxItem Value="I.IdInstruccion" Text="Instrucción" />
                                    <telerik:RadComboBoxItem Value="I.FechaRecepcion" Text="Fecha Recepción" />
                                    <telerik:RadComboBoxItem Value="I.Fecha" Text="Fecha Registro" />
                                    <telerik:RadComboBoxItem Value="A.NombreAduana" Text="Aduana" />
                                    <telerik:RadComboBoxItem Value="CL.Nombre" Text="Cliente" />
                                    <telerik:RadComboBoxItem Text="I.Proveedor" />
                                    <%--<telerik:RadComboBoxItem Text="Transporte" />--%>
                                    <%--<telerik:RadComboBoxItem Text="Gestor" />--%>
                                    <telerik:RadComboBoxItem Value="DI1.DocumentoNo" Text="Número Factura" />
                                    <%--<telerik:RadComboBoxItem Text="Regimen" />--%>
                                    <telerik:RadComboBoxItem Value="EFI.Serie" Text="Serie" />
                                    <telerik:RadComboBoxItem Value="I.NoCorrelativo" Text="Numero Correlativo" />
                                    <telerik:RadComboBoxItem Value="U1.Nombre" Text="Nombre Gestor" />
                                    <telerik:RadComboBoxItem Value="U1.Apellido" Text="Apellido Gestor" />
                                    <telerik:RadComboBoxItem Value="U2.Nombre" Text="Nombre Aforador" />
                                    <telerik:RadComboBoxItem Value="U2.Apellido" Text="Apellido Aforador" />
                                    <telerik:RadComboBoxItem Value="U3.Nombre" Text="Nombre Tramitador" />
                                    <telerik:RadComboBoxItem Value="U3.Apellido" Text="Apellido Tramitador" />
                                    <telerik:RadComboBoxItem Value="I.CodRegimen" Text="Codigo Regimen" />
                                    <telerik:RadComboBoxItem runat="server" Text="Pais" Value="I.CodPaisHojaRuta" />
                                </Items>
                            </telerik:RadComboBox>
                            <%--<asp:DropDownList ID="DropDownListCampo1" runat="server" Width="90%">
                                <asp:ListItem></asp:ListItem>
                                <asp:ListItem Value="r.IdHojaRuta">Hoja Ruta</asp:ListItem>
                                <asp:ListItem Value="FechaRecepcion">Fecha Recepcion</asp:ListItem>
                                <asp:ListItem Value="t.FechaProceso">Fecha Registro</asp:ListItem>
                                <asp:ListItem Value="NombreAduana">Aduana</asp:ListItem>
                                <asp:ListItem Value="NombreCliente">Cliente</asp:ListItem>
                                <asp:ListItem Value="Proveedor">Proveedor</asp:ListItem>
                                <asp:ListItem Value="r.Descripcion">Transporte</asp:ListItem>
                                <asp:ListItem Value="e.NombreEmpleado">Gestor</asp:ListItem>
                                <asp:ListItem Value="NumFactura">Numero Factura</asp:ListItem>
                                <asp:ListItem Value="CodigoRegimen">Regimen</asp:ListItem>
                                <asp:ListItem Value="ValorSerie">Serie</asp:ListItem>
                                <asp:ListItem Value="Correlativo">Correlativo</asp:ListItem>
                                <asp:ListItem Value="r.IdFlujo">CodigoFlujo</asp:ListItem>
                                <asp:ListItem>Impuesto</asp:ListItem>
                                <asp:ListItem>Anticipo</asp:ListItem>
                                <asp:ListItem Value="ValorMulta">Multa</asp:ListItem>
                                <asp:ListItem Value="ValorDemora">Demora</asp:ListItem>
                                <asp:ListItem Value="ValorLiquidacion">Liquidacion</asp:ListItem>
                                <asp:ListItem Value="ValorProvisionamientoEst">ProvisionamientoEst</asp:ListItem>
                                <asp:ListItem Value="OtrosFondos">OtrosFondos</asp:ListItem>
                                <asp:ListItem Value="IdResponsable">CodigoResponsable</asp:ListItem>
                                <asp:ListItem Value="NombreResponsable">NombreResponsable</asp:ListItem>
                                <asp:ListItem Value="a.CodigoPais">CodigoPais</asp:ListItem>
                                <asp:ListItem Value="paises.descripcion">Origen</asp:ListItem>
                                <asp:ListItem>Destino</asp:ListItem>
                                <asp:ListItem Value="cast(pa.Cod as numeric)">CodigoEtapa</asp:ListItem>
                                <asp:ListItem Value="FechaVencimiento">Fecha Vencimiento TL</asp:ListItem>
                                <asp:ListItem Value="FechaVencimientoENP">Fecha Vencimiento ENP</asp:ListItem>
                                <asp:ListItem Value="af.NombreEmpleado">Aforador</asp:ListItem>
                                <asp:ListItem Value="tr.NombreEmpleado">Tramitador</asp:ListItem>
                            </asp:DropDownList>--%>
                        </td>
                        <td>
                            <telerik:RadComboBox ID="cmbOperador" runat="server" Width="90%">
                                <Items>
                                    <telerik:RadComboBoxItem />
                                    <telerik:RadComboBoxItem Value=">" Text=">" />
                                    <telerik:RadComboBoxItem Value="<" Text="<" />
                                    <telerik:RadComboBoxItem Value="=" Text="=" />
                                    <telerik:RadComboBoxItem Value="&gt;=" Text="&gt;=" />
                                    <telerik:RadComboBoxItem Value="&lt;=" Text="&lt;=" />
                                    <telerik:RadComboBoxItem Value="!=" Text="!=" />
                                    <telerik:RadComboBoxItem Value="like" Text="Contiene" />
                                </Items>
                            </telerik:RadComboBox>
                            <%--<asp:DropDownList ID="DropDownOperador" runat="server" Width="80%">
                                <asp:ListItem Selected="True"></asp:ListItem>
                                <asp:ListItem>></asp:ListItem>
                                <asp:ListItem Value="<"><</asp:ListItem>
                                <asp:ListItem Value="=">=</asp:ListItem>
                                <asp:ListItem Value="&gt;="></asp:ListItem>
                                <asp:ListItem Value="&lt;="></asp:ListItem>
                                <asp:ListItem Value="!="></asp:ListItem>
                                <asp:ListItem Value="like">Contiene</asp:ListItem>
                            </asp:DropDownList>--%>
                        </td>
                        <td>
                            <telerik:RadTextBox ID="txtValor" runat="server" Width="80%">
                            </telerik:RadTextBox>
                            <telerik:RadDatePicker ID="edFecha" runat="server" Visible="false" Width="90%">
                            </telerik:RadDatePicker>
                            <% if (!IsPostBack)
                               {
                                   if (User.IsInRole("Jefes Aduana") && cmbCampo.SelectedValue == "Aduana")
                                   {
                                       //txtValor.Text = nombreAduana;
                                       txtValor.Enabled = false;
                                       edFecha.Visible = false;
                                   }
                                   else
                                   {
                                       txtValor.Enabled = true;
                                       txtValor.Text = "";
                                       edFecha.Visible = false;
                                   }
                                   if (cmbCampo.SelectedValue.Contains("Fecha"))
                                   {
                                       txtValor.Visible = false;
                                       edFecha.Visible = true;
                                   }
                                   else
                                       txtValor.Visible = true;
                               }%>
                        </td>
                        <td>
                            <telerik:RadComboBox ID="cmbCriterio" runat="server" Width="80%">
                                <Items>
                                    <telerik:RadComboBoxItem />
                                    <telerik:RadComboBoxItem Value="AND" Text="AND" />
                                    <telerik:RadComboBoxItem Value="OR" Text="OR" />
                                </Items>
                            </telerik:RadComboBox>
                        </td>
                        <td>
                            <asp:Button ID="btnAgregar" runat="server" Text="Agregar" OnClick="btnAgregar_Click" />
                        </td>
                    </tr>
                </table>
                <telerik:RadListBox ID="lstCriterios" runat="server" Width="50%" Height="75px">
                </telerik:RadListBox>
                <table style="width: 50%">
                    <tr>
                        <td colspan="3">
                            <asp:Button ID="btnLimpiar" runat="server" Text="Limpiar" OnClick="btnLimpiar_Click" />&nbsp;
                            <input id="CodigoAduana" name="CodigoAduana" type="hidden" value="<%=codigoAduana%>" />
                            <input id="NombreAduana" name="NombreAduana" type="hidden" value="<%=nombreAduana%>" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%; text-align: center">Campos Disponibles
                        </td>
                        <td style="width: 50%; text-align: center">Campos a Mostrar
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadListBox ID="lstCamposDisponibles" runat="server" Width="95%" Height="250px"
                                SelectionMode="Multiple" AllowTransfer="True" TransferToID="lstCamposMostrar"
                                AutoPostBackOnTransfer="True" AllowReorder="True" AutoPostBackOnReorder="True"
                                EnableDragAndDrop="True" Style="top: 0px; left: 0px" Culture="es-ES">
                                <Localization AllToLeft="Todo a la izquierda" AllToRight="Todo a la derecha" MoveDown="Bajar"
                                    MoveUp="Subir" ToLeft="Para la izquierda" ToRight="Para la derecha" />
                                <ButtonSettings TransferButtons="All" />
                                <Items>

                                    <telerik:RadListBoxItem Value="I.Fecha AS FechaRegistro" Text="Fecha Registro" />
                                    <telerik:RadListBoxItem Value="DI1.DocumentoNo AS Factura" Text="Factura" />
                                    <telerik:RadListBoxItem Value="CL.Nombre as Cliente" Text="Cliente" />
                                    <telerik:RadListBoxItem Value="I.Proveedor" Text="Proveedor" />
                                    <telerik:RadListBoxItem Value="I.Producto" Text="Producto" />
                                    <telerik:RadListBoxItem Value="A.NombreAduana AS Aduana" Text="Aduana" />
                                    <telerik:RadListBoxItem Value="I.CodRegimen" Text="Codigo Regimen" />
                                    <telerik:RadListBoxItem Value="UPPER(C1.Descripcion) AS Regimen" Text="Regimen" />
                                    <telerik:RadListBoxItem Value="EFI.Serie" Text="Serie" />
                                    <telerik:RadListBoxItem Value="I.NoCorrelativo" Text="Correlativo" />
                                    <telerik:RadListBoxItem Value="C2.Descripcion AS DocumentoEmbarque"
                                        Text="Documento Embarque" />
                                    <telerik:RadListBoxItem Value="DI2.DocumentoNo AS ValorDocumentoEmbarque"
                                        Text="Valor Documento Embarque" />
                                    <telerik:RadListBoxItem Value="I.ValorFOB" Text="ValorFOB" />
                                    <telerik:RadListBoxItem Value="I.Flete" Text="Flete" />
                                    <telerik:RadListBoxItem Value="I.Seguro" Text="Seguro" />
                                    <telerik:RadListBoxItem Value="I.Otros" Text="Otros" />
                                    <telerik:RadListBoxItem Value="I.ValorCIF" Text="ValorCIF" />
                                    <telerik:RadListBoxItem Value="I.Impuesto" Text="Impuesto" />
                                    <telerik:RadListBoxItem Value="I.CantidadBultos" Text="Cantidad Bultos" />
                                    <telerik:RadListBoxItem Value="I.PesoKgs" Text="PesoKgs" />
                                    <telerik:RadListBoxItem Value="C3.Descripcion AS Origen" Text="Origen" />
                                    <telerik:RadListBoxItem Value="I.CiudadDestino" Text="Ciudad Destino" />
                                    <telerik:RadListBoxItem Value="I.ReferenciaCliente" Text="ReferenciaCliente" />
                                    <telerik:RadListBoxItem Value="ISNULL(E.FechaInicio ,GC88.Fecha )AS ArribodeCarga"
                                        Text="Arribo de Carga" />
                                    <telerik:RadListBoxItem Value="ISNULL(GC1.Fecha ,GC1.Fecha )AS ComienzoFlujo"
                                        Text="Comienzo Flujo" />
                                    <telerik:RadListBoxItem Value="ISNULL(GC3.Fecha , GC22.Fecha )AS ValidacionElectonica"
                                        Text="Validación Electónica" />
                                    <telerik:RadListBoxItem Value="ISNULL(GC4.Fecha,GC33.Fecha ) AS PagodeImpuestos "
                                        Text="Pago Impuestos" />
                                    <telerik:RadListBoxItem Value="ISNULL(GC5.Fecha,GC44.Fecha) AS AsignacionCanalSelectividad"
                                        Text="Asignación Canal Selectividad" />
                                    <telerik:RadListBoxItem Value="ISNULL(GC6.Fecha,GC55.Fecha )AS RevisionMercancia"
                                        Text="Revisión Mercancía" />
                                    <telerik:RadListBoxItem Value="ISNULL(GC7.Fecha,GC66.Fecha) AS EmisionPaseSalida"
                                        Text="Emisión Pase Salida" />
                                    <telerik:RadListBoxItem Value="ISNULL(GC.Fecha ,GC77.Fecha )AS EntregadeServicio"
                                        Text="Entrega de Servicio" />
                                    <telerik:RadListBoxItem Value="'Dias: ' + CAST(DATEDIFF(SECOND, GC1.Fecha, GC.Fecha) / 86400 AS varchar(50)) +
' Horas: ' + CAST((DATEDIFF(SECOND, GC1.Fecha, GC.Fecha) % 86400) / 3600 AS varchar(50)) +
' Minutos: ' + CAST((DATEDIFF(SECOND, GC1.Fecha, GC.Fecha) % 3600) / 60 AS varchar(50)) AS TiempoProceso"
                                        Text="Tiempo Proceso" />
                                    <telerik:RadListBoxItem Value="U1.Nombre + ' ' + U1.Apellido" Text="Gestor" />
                                    <telerik:RadListBoxItem Value="U2.Nombre + ' ' + U2.Apellido" Text="Aforador" />
                                    <telerik:RadListBoxItem Value="U3.Nombre + ' ' + U3.Apellido" Text="Tramitador" />
                                    <telerik:RadListBoxItem runat="server" Text="Pais Destino"
                                        Value="C4.Descripcion AS PaisDestino" />
                                    <telerik:RadListBoxItem runat="server" Text="Empresa"
                                        Value="CASE WHEN DGC.Empresa = '' THEN 'NO HAY DATOS' WHEN DGC.Empresa IS NULL THEN 'NO HAY DATOS' ELSE DGC.Empresa END AS Empresa" />
                                    <telerik:RadListBoxItem runat="server" Text="Observacion"
                                        Value="I.Observacion" />
                                    <telerik:RadListBoxItem runat="server" Text="Creacion Usuario"
                                        Value="(U4.Nombre+' '+U4.Apellido) AS Nombre" />
                                    <telerik:RadListBoxItem runat="server" Text="Fecha Permiso Inicio Ente Gubernamental"
                                        Value="E5.FechaInicio" />
                                    <telerik:RadListBoxItem runat="server" Text="Fecha Permiso Fin Ente Gubernamental"
                                        Value="E5.FechaFin" />
                                    <telerik:RadListBoxItem runat="server" Text="Observacion Permiso Fin Ente Gubernamental"
                                        Value="E5.ObservacionFin" />
                                    <telerik:RadListBoxItem runat="server" Text="Observacion Permiso Inicio Ente Gubernamental"
                                        Value="E5.ObservacionInicio" />
                                    <telerik:RadListBoxItem runat="server" Text="Envio/Recepcion Documentos para Tramite de Permiso Inicio"
                                        Value="E7.FechaInicio" />
                                    <telerik:RadListBoxItem runat="server" Text="Envio/Recepcion Documentos para Tramite de Permiso Fin"
                                        Value="E7.FechaFin" />
                                    <telerik:RadListBoxItem runat="server" Text="Envio/Recepcion Documentos para Tramite de Permiso Fin"
                                        Value="E7.ObservacionFin" />
                                    <telerik:RadListBoxItem runat="server" Text="Envio/Recepcion Documentos para Tramite de Permiso Inicio"
                                        Value="E7.ObservacionInicio" />
                                    <telerik:RadListBoxItem runat="server" Text="Ingreso de Solicitud de Cheque Inicio"
                                        Value="E8.FechaInicio" />
                                    <telerik:RadListBoxItem runat="server" Text="Ingreso de Solicitud de Cheque Fin"
                                        Value="E8.FechaFin" />
                                    <telerik:RadListBoxItem runat="server" Text="Ingreso de Solicitud de Cheque Fin"
                                        Value="E8.ObservacionFin" />
                                    <telerik:RadListBoxItem runat="server" Text="Ingreso de Solicitud de Cheque Inicio"
                                        Value="E8.ObservacionInicio" />
                                    <telerik:RadListBoxItem runat="server" Text="Factura Comercial Recibida"
                                        Value="E12.FechaInicio as FacturaComercialRecibida'" />
                                    <telerik:RadListBoxItem runat="server" Text="Documentos de Exportacion Entregados"
                                        Value="E13.FechaInicio as DocumentosDeExportacionEntregados" />
                                    <telerik:RadListBoxItem runat="server" Text="Color" Value="ISNULL(I.Color,PF.CanalSelectividad) as Color" />
                                    <telerik:RadListBoxItem runat="server" ListBox="lstCamposDisponibles"
                                        Text="Numero de Permiso" Value="PermisoNo" />
                                    <telerik:RadListBoxItem runat="server" Text="Tiempo Medición Proceso Aduanero"
                                        Value="'Dias: ' + CAST(DATEDIFF(SECOND, E.FechaInicio, GC.Fecha) / 86400 AS varchar(50)) + ' Horas: ' + CAST((DATEDIFF(SECOND, E.FechaInicio, GC.Fecha) % 86400) / 3600 AS varchar(50)) +' Minutos: ' + CAST((DATEDIFF(SECOND, E.FechaInicio, GC.Fecha) % 3600) / 60 AS varchar(50)) AS TiempoProceso2" />
                                    <telerik:RadListBoxItem runat="server" Text="CodigoSAP"
                                        Value="I.IdCliente as CodigoSAP" />
                                    <telerik:RadListBoxItem runat="server" ListBox="lstCamposDisponibles"
                                        Text="Tiempo de Proceso Horas"
                                        Value="CONVERT(money,(DATEDIFF(MINUTE ,GC1.Fecha,GC.Fecha)))/60 as TiempoProcesoHoras" />
                                    <telerik:RadListBoxItem runat="server" ListBox="lstCamposDisponibles"
                                        Text="ETA" Value="I.ETA as ETA" />
                                    <telerik:RadListBoxItem runat="server" Text="Observacion General"
                                        Value="DGC.ObservacionGeneral" />
                                    <telerik:RadListBoxItem runat="server" Text="Descripcion Contenedores"
                                        Value="I.DescripcionContenedores" />
                                    <telerik:RadListBoxItem runat="server" Text="Cantidad Contenedores"
                                        Value="I.Contenedores" />
                                    <telerik:RadListBoxItem runat="server" ListBox="lstCamposDisponibles"
                                        Text="Gestion Aforo"
                                        Value="CONVERT(money,(DATEDIFF(MINUTE,GC1.Fecha,GC5.Fecha)))/60 as GestionAforo" />
                                    <telerik:RadListBoxItem runat="server" ListBox="lstCamposDisponibles"
                                        Text="Gestion Aduanera"
                                        Value="CONVERT(money,(DATEDIFF(MINUTE,GC5.Fecha,GC7.Fecha)))/60 as GestionAduanera" />
                                    <telerik:RadListBoxItem runat="server" ListBox="lstCamposDisponibles"
                                        Text="Gestion de salida"
                                        Value="CONVERT(money,(DATEDIFF(MINUTE,GC7.Fecha,GC.Fecha)))/60 as Gestionsalida" />
                                    <telerik:RadListBoxItem runat="server" Text="Division"
                                        Value="C7.Descripcion as Division" />
                                    <telerik:RadListBoxItem runat="server" Text="Gate Pass Fecha Fin"
                                        Value="E9.FechaFin as GatePassFin" />
                                    <telerik:RadListBoxItem runat="server" Text="Solicitud Cita OPC Fecha Fin"
                                        Value="E10.FechaFin as CitaOPCFin" />
                                    <telerik:RadListBoxItem runat="server" Text="Creacion de Manifiesto Fecha Fin"
                                        Value="E11.FechaFin as ManifiestoFin" />
                                    <telerik:RadListBoxItem Value="CASE WHEN I.IdEstadoFlujo = '99' THEN 'Flujo Finalizado' When I.IdEstadoFlujo ='98'Then 'Habilitada Temporalmente' ELSE ISNULL(FC2.EstadoFlujo,'Comienzo Flujo') END as FlujoActual" Text="Flujo Actual" />
                                    <telerik:RadListBoxItem runat="server" Text="Especies Fiscales" Value="EF.Serie+' '+C5.Descripcion as Especies" />

                                    <%--Removidas por Darwin Bonilla 17/09/2015--%>
                                    <%--<telerik:RadListBoxItem runat="server" Text="Observacion Tiempo"
                                     Value="Case When CONVERT(money,(DATEDIFF(MINUTE ,GC1.Fecha,GC.Fecha)))/60 &gt;= 6.00 and I.CodigoAduana NOT IN ('016','017','014') 
                                        Then 'Fuera de Tiempo' When CONVERT(money,(DATEDIFF(MINUTE ,GC1.Fecha,GC.Fecha)))/60 &gt;= 36.00 and I.CodigoAduana IN ('017','014')Then 
                                        'Fuera de Tiempo' When CONVERT(money,(DATEDIFF(MINUTE ,GC1.Fecha,GC.Fecha)))/60 &gt;= 72.00 and I.CodigoAduana IN ('016')Then 'Fuera de Tiempo' 
                                        Else 'A Tiempo' END AS TIEMPO" />--%>
                                    <%--<telerik:RadListBoxItem runat="server" ListBox="lstCamposDisponibles" Text="Recepcion Copias" Value="DI2.FechaCopia as Copia" />--%>
                                    <%--<telerik:RadListBoxItem runat="server" Text="Recepcion Originales" Value="DI2.FechaOriginal as Original" />--%>
                                    <%--<telerik:RadListBoxItem runat="server" Text="Especies Fiscales" Value="EF.Serie+' '+C5.Descripcion as Especies" />--%>
                                    <%--<telerik:RadListBoxItem runat="server" Text="Recepcion de Documentos" Value="E6.FechaInicioIngreso" />--%>
                                    <%--<telerik:RadListBoxItem runat="server" ListBox="lstCamposDisponibles"
                                        Text="Tipo Cargamento" Value="C6.Descripcion as TipoCargamento" />--%>
                                    <%--<telerik:RadListBoxItem runat="server" Text="No. Correlativo RIT"
                                        Value="CASE  WHEN E4.ObservacionInicio = '' THEN 'NO HAY DATOS' WHEN E4.ObservacionInicio IS NULL THEN 'NO HAY DATOS' ELSE E4.ObservacionInicio END AS NoCorrelativoRIT" />--%>
                                    <%--<telerik:RadListBoxItem runat="server" Text="Saldo Disponible Sarah"
                                        Value="CASE  WHEN E3.ObservacionInicio = '' THEN 'NO HAY DATOS' WHEN E3.ObservacionInicio IS NULL THEN 'NO HAY DATOS' ELSE E3.ObservacionInicio END AS SaldoDisponibleSarah" />--%>
                                    <%--<telerik:RadListBoxItem runat="server" Text="Saldo Cancelado"
                                        Value="CASE  WHEN E2.ObservacionInicio = '' THEN 'NO HAY DATOS' WHEN E2.ObservacionInicio IS NULL THEN 'NO HAY DATOS' ELSE E2.ObservacionInicio END AS SaldoCancelado" />--%>
                                    <%--<telerik:RadListBoxItem Value="I.FechaRecepcion" Text="FechaRecepcion" />--%>
                                    <%--<telerik:RadListBoxItem Text="TiempoEstimadoEntrega" />--%>
                                    <%--<telerik:RadListBoxItem Value="I.Aforador" Text="Aforador" />--%>
                                    <%--<telerik:RadListBoxItem Value="I.Tramitador" Text="Tramitador" />--%>
                                    <%--<telerik:RadListBoxItem Value="Convert(varchar,cast(Anticipo as Money),1) as ValorAnticipo "Text="Anticipo" />--%>
                                    <%--<telerik:RadListBoxItem Value="Convert(varchar,cast(Impuesto as Money),1) as ValorImpuesto" Text="Impuesto" />--%>
                                    <%--<telerik:RadListBoxItem Text="Multa" />--%>
                                    <%--<telerik:RadListBoxItem Text="Demora" />--%>
                                    <%--<telerik:RadListBoxItem Text="Liquidacion" />--%>
                                    <%--<telerik:RadListBoxItem Text="CodigoResponsable" />--%>
                                    <%--<telerik:RadListBoxItem Text="NombreResponsable" />--%>
                                    <%--<telerik:RadListBoxItem Text="Eta" />--%>
                                    <%--<telerik:RadListBoxItem Value="e.NombreEmpleado as Gestor" Text="Gestor" />--%>
                                    <%--Removidas por Darwin Bonilla 17/09/2015--%>
                                    <telerik:RadListBoxItem runat="server" Text="Liquidacion" Value="bp.Liquidacion" />
                                </Items>
                            </telerik:RadListBox>
                        </td>
                        <td>
                            <telerik:RadListBox ID="lstCamposMostrar" runat="server" Width="95%" Height="250px"
                                SelectionMode="Multiple" AllowReorder="true" AutoPostBackOnReorder="true" EnableDragAndDrop="true">
                                <Localization MoveDown="Bajar" MoveUp="Subir" />
                            </telerik:RadListBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Button ID="btnProcesar" runat="server" Text="Procesar" OnClick="btnProcesar_Click" />
                        </td>
                    </tr>
                </table>
                <%--  <table style="width: 100%">
                    <tr>
                        <td>--%>
                <input id="edBag" runat="server" type="hidden" value="true" />
                <telerik:RadGrid ID="rgInstrucciones" runat="server" AllowFilteringByColumn="True"
                    Width="3000px" Visible="false" AllowSorting="True" AutoGenerateColumns="true"
                    GridLines="None" OnNeedDataSource="rgInstrucciones_NeedDataSource" AllowPaging="True"
                    ShowFooter="True" ShowStatusBar="True" PageSize="20" OnInit="rgInstrucciones_Init"
                    OnItemCommand="rgInstrucciones_ItemCommand">
                    <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                        PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                        Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                    <MasterTableView DataKeyNames="IdInstruccion" CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros."
                        NoMasterRecordsText="No hay instrucciones." GroupLoadMode="Client">
                        <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                            ShowExportToExcelButton="true" ShowExportToCsvButton="false" />
                        <RowIndicatorColumn>
                            <HeaderStyle Width="20px"></HeaderStyle>
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn>
                            <HeaderStyle Width="20px"></HeaderStyle>
                        </ExpandCollapseColumn>
                        <%--<GroupByExpressions>
                                        <telerik:GridGroupByExpression>
                                            <SelectFields>
                                                <telerik:GridGroupByField FieldAlias="PaisOrigen" FieldName="PaisOrigen"></telerik:GridGroupByField>
                                            </SelectFields>
                                            <GroupByFields>
                                                <telerik:GridGroupByField FieldAlias="PaisOrigen" FieldName="PaisOrigen"></telerik:GridGroupByField>
                                            </GroupByFields>
                                        </telerik:GridGroupByExpression>
                                    </GroupByExpressions>--%>
                    </MasterTableView>
                    <HeaderStyle Width="180px" />
                    <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                        <Selecting AllowRowSelect="True" />
                        <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                            DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                            DropHereToReorder="Suelte aquí para Re-Ordenar" />
                        <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
                    </ClientSettings>
                    <FilterMenu EnableTheming="True">
                        <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                    </FilterMenu>
                    <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                    <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
                </telerik:RadGrid>
                <table>
                    <tr>
                        <td colspan="4" align="center">
                            <asp:ImageButton ID="btnExport" ToolTip="Exportar a Excel" ImageUrl="~/Images/24/arrow_down_green_24.png"
                                runat="server" CausesValidation="false" Visible="false" OnClick="btnExport_Click" />
                            <asp:ImageButton ID="btnExport2" ToolTip="Exportar a Excel" ImageUrl="~/Images/24/arrow_down_green_24.png"
                                runat="server" CausesValidation="false" Visible="false" OnClick="btnExport_Click" />
                        </td>
                    </tr>
                </table>
                <%--</td>
                    </tr>
                </table>--%>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </telerik:RadAjaxPanel>
</asp:Content>
