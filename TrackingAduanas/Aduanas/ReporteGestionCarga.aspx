﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteGestionCarga.aspx.cs" Inherits="ReporteGestionCarga" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <table border="1" style="width: 45%">
        <tr>
            <td style="width: 15%">
                <asp:Label ID="lblFechaInicio" runat="server" Text="Fecha Inicio:" ForeColor="Black"></asp:Label>
            </td>
            <td style="width: 22%">
                <telerik:RadDatePicker ID="dpFechaInicio" runat="server" EnableTyping="False" Width="95%">
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="dpFechaInicio"
                    ErrorMessage="Ingrese Fecha" ForeColor="Red" />
            </td>
            <td style="width: 15%">
                <asp:Label ID="lblFechaFinal" runat="server" Text="Fecha Final:" ForeColor="Black"></asp:Label>
            </td>
            <td style="width: 22%">
                <telerik:RadDatePicker ID="dpFechaFinal" runat="server" EnableTyping="False" Width="95%">
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator runat="server" ID="reqName" ControlToValidate="dpFechaFinal"
                    ErrorMessage="Ingrese Fecha" ForeColor="Red" />
            </td>
            <td style="width: 10%">
                <asp:Label ID="lblPais" runat="server" Text="País:" ForeColor="Black"></asp:Label>
            </td>
            <td style="width: 16%">
                <telerik:RadComboBox ID="cmbPais" runat="server" DataTextField="Descripcion" DataValueField="Codigo"
                    AutoPostBack="true" OnSelectedIndexChanged="cmbPais_SelectedIndexChanged">
                </telerik:RadComboBox>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="Label2" runat="server" Text="Cliente:" ForeColor="Black"></asp:Label>
            </td>
            <td colspan="5">
                <telerik:RadComboBox ID="cmbCliente" runat="server" DataTextField="Nombre" DataValueField="CodigoSAP"
                    Width="100%" Filter="StartsWith">
                </telerik:RadComboBox>
            </td>
        </tr>
        <tr>
            <td colspan="6" align="center">
                <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Images/24/view_24.png"
                    OnClick="btnBuscar_Click" />
            </td>
        </tr>
    </table>
    <telerik:RadGrid ID="rgInstrucciones" runat="server" AllowFilteringByColumn="True"
        Visible="False" AllowSorting="True" AutoGenerateColumns="False" GridLines="None"
        Height="410px" OnNeedDataSource="rgInstrucciones_NeedDataSource" AllowPaging="True"
        ShowFooter="True" ShowStatusBar="True" PageSize="20" OnInit="rgInstrucciones_Init"
        OnItemCommand="rgInstrucciones_ItemCommand" CellSpacing="0">
        <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
            PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
            Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
        <MasterTableView DataKeyNames="" CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros."
            NoMasterRecordsText="No hay instrucciones." GroupLoadMode="Client">
            <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                ShowExportToExcelButton="true" />
<CommandItemSettings RefreshText="Volver a Cargar Datos" ExportToPdfText="Export to PDF" ShowAddNewRecordButton="False" ShowExportToExcelButton="True"></CommandItemSettings>

            <RowIndicatorColumn>
                <HeaderStyle Width="20px"></HeaderStyle>
            </RowIndicatorColumn>
            <ExpandCollapseColumn>
                <HeaderStyle Width="20px"></HeaderStyle>
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn DataField="IdInstruccion" HeaderText="Instruccion No." UniqueName="IdInstruccion"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="100px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Nombre" HeaderText="Cliente" UniqueName="Nombre"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="120px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Producto" HeaderText="Producto" UniqueName="Producto"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="200px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="DocumentoNo" HeaderText="Factura" UniqueName="DocumentoNo"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="120px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ArriboCarga" HeaderText="Arribo de Carga" UniqueName="ArriboCarga"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="140px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ComienzoFlujo" HeaderText="Comienzo Flujo" UniqueName="ComienzoFlujo"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="140px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ValidaciónElectrónica" HeaderText="Validación Electrónica"
                    UniqueName="ValidaciónElectrónica" FilterControlWidth="60%">
                    <HeaderStyle Width="140px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="PagoImpuestos" HeaderText="Pago de Impuestos"
                    UniqueName="PagoImpuestos" FilterControlWidth="60%">
                    <HeaderStyle Width="140px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="AsignaciónCanalSelectividad" HeaderText="Asignación de Canal de Selectividad"
                    UniqueName="AsignaciónCanalSelectividad" FilterControlWidth="60%">
                    <HeaderStyle Width="140px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Color" HeaderText="Color" UniqueName="Color"
                    FilterControlWidth="60%">
                    <HeaderStyle Width="100px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="RevisiónMercancía" HeaderText="Revisión de Mercancía"
                    UniqueName="RevisiónMercancía" FilterControlWidth="60%">
                    <HeaderStyle Width="140px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="EmisiónPaseSalida" HeaderText="Emisión de Pase deSalida"
                    UniqueName="EmisiónPaseSalida" FilterControlWidth="60%">
                    <HeaderStyle Width="140px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="EntregaServicio" HeaderText="Entrega de Servicio"
                    UniqueName="EntregaServicio" FilterControlWidth="60%">
                    <HeaderStyle Width="140px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Regime" 
                    FilterControlAltText="Filter Regime column" HeaderText="Regime" 
                    UniqueName="Regime">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Serie" 
                    FilterControlAltText="Filter Serie column" HeaderText="Serie" 
                    UniqueName="Serie">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="BL" FilterControlAltText="Filter BL column" 
                    HeaderText="BL" UniqueName="BL">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="ReferenciaCliente" 
                    FilterControlAltText="Filter ReferenciaCliente column" 
                    HeaderText="Referncia Cliente" UniqueName="ReferenciaCliente">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="CiudadDestino" 
                    FilterControlAltText="Filter CiudadDestino column" HeaderText="Ciudad Destino" 
                    UniqueName="CiudadDestino">
                </telerik:GridBoundColumn>
            </Columns>

<EditFormSettings>
<EditColumn FilterControlAltText="Filter EditCommandColumn column"></EditColumn>
</EditFormSettings>
        </MasterTableView>
        <HeaderStyle Width="180px" />
<SortingSettings SortToolTip="Presione aqu&#237; para ordenar" 
            SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente"></SortingSettings>

        <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
            <Selecting AllowRowSelect="True" />
            <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                DropHereToReorder="Suelte aquí para Re-Ordenar" />
            <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
<Selecting AllowRowSelect="True"></Selecting>

<ClientMessages DropHereToReorder="Suelte aqu&#237; para Re-Ordenar" DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tama&#241;o" PagerTooltipFormatString="P&#225;gina &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"></ClientMessages>

<Scrolling AllowScroll="True" UseStaticHeaders="True"></Scrolling>
        </ClientSettings>

<StatusBarSettings ReadyText="Listo" LoadingText="Cargando, por favor espere..."></StatusBarSettings>

        <FilterMenu EnableTheming="True">
            <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
        </FilterMenu>
        <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
        <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
    </telerik:RadGrid>
</asp:Content>
