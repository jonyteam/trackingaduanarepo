﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteHCCExcell.aspx.cs" Inherits="GenerarArchivoSAPDinant" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style12 {
            width: 100%;
            height: 50px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="pnlRequestStarted">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">

                function pnlRequestStarted(ajaxPanel, eventArgs) {
                    try {
                        if ((eventArgs.EventTarget == "<%= btnSalvar.UniqueID %>")) {
                            eventArgs.EnableAjax = false;
                        }
                    }
                    catch (err)
                    { }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                    }
                }
            </script>
        </telerik:RadScriptBlock>
        <table border="1" style="width: 55%">
            <tr>
                <td style="width: 15%">
                    <asp:Label ID="lblFechaInicio" runat="server" Text="Fecha Inicio:" ForeColor="Black"></asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadDatePicker ID="dpFechaInicio" runat="server" EnableTyping="False" Width="95%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="dpFechaInicio"
                        ErrorMessage="Ingrese Fecha" ForeColor="Red" />
                </td>
                <td style="width: 15%">
                    <asp:Label ID="lblFechaFinal" runat="server" Text="Fecha Final:" ForeColor="Black"></asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadDatePicker ID="dpFechaFinal" runat="server" EnableTyping="False" Width="95%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator runat="server" ID="reqName" ControlToValidate="dpFechaFinal"
                        ErrorMessage="Ingrese Fecha" ForeColor="Red" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center" style="text-align: right">
                    <input id="iTipoFlete" runat="server" type="hidden" />
                    <telerik:RadComboBox ID="cmbCliente" runat="server" Style="text-align: left">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="Cargill-Alcon" Value="8000423 and A.Division = 2 and A.CodRegimen in ('4000','5100','5600','4600','6010','4051')" />
                            <telerik:RadComboBoxItem runat="server" Text="Cargill-No Alcon" Value="8000423 and A.Division != 2 and A.CodRegimen in ('4000','4600','4051')" />
                            <telerik:RadComboBoxItem runat="server" Text="Perry" Value="1000102" />
                            <telerik:RadComboBoxItem runat="server" Text="Agribrands" Value="8400911" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
                <td align="center" colspan="2">
                    <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Images/24/view_24.png"
                        OnClick="btnBuscar_Click" />
                </td>
            </tr>
        </table>
        <br />
        <telerik:RadGrid ID="grReporte" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            CellSpacing="0" GridLines="None" Width="8000px" OnItemCommand="RadGrid1_ItemCommand"
            OnNeedDataSource="RadGrid1_NeedDataSource" PageSize="20" BorderStyle="Solid"
            OnExcelExportCellFormatting="RadGrid1_ExcelExportCellFormatting" OnItemDataBound="RadGrid1_ItemDataBound" Culture="es-ES">
            <ClientSettings AllowColumnsReorder="True">
                <Selecting AllowRowSelect="True" />
            </ClientSettings>
            <ExportSettings FileName="Reporte" OpenInNewWindow="True">
                <Excel AutoFitImages="True" />
            </ExportSettings>
            <MasterTableView CommandItemDisplay="Top">
                <%-- <CommandItemTemplate>
            <asp:Image ID = "prueba" runat="server" ImageUrl ="~/Images/Prueba.jpg"
            AlternateText="Sushi Bar"
                    Width="100%"></asp:Image>
            </CommandItemTemplate>--%>
                <CommandItemSettings ShowExportToExcelButton="true" ExportToPdfText="Export to PDF"
                    ShowAddNewRecordButton="false" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="Descripcion" FilterControlAltText="Filter IdInstruccion column"
                        HeaderText="Tipo Importacion" UniqueName="IdInstruccion">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ProductosCargill" FilterControlAltText="Filter ProductosCargill column"
                        HeaderText="Productos" UniqueName="ProductosCargill">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Nombre" FilterControlAltText="Filter Proveedor column"
                        HeaderText="Planta" UniqueName="Proveedor">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NombreAduana" FilterControlAltText="Filter Aduana column"
                        HeaderText="Aduana" UniqueName="Aduana">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IdInstruccion" FilterControlAltText="Filter Nombre column"
                        HeaderText="Numero" UniqueName="Nombre">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FechaRegistro" FilterControlAltText="Filter Fecha column"
                        HeaderText="Fecha Registro" UniqueName="Fecha">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FacturaVESTA" FilterControlAltText="Filter DocumentoNo column"
                        HeaderText="# Factura VESTA" UniqueName="FacturaVESTA">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FechaEntregaCargill" FilterControlAltText="Filter DocumentoNo column"
                        HeaderText="Fecha Entrega Cargill" UniqueName="DocumentoNo">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="#OrdenCompra" FilterControlAltText="Filter CantidadBultos column"
                        HeaderText="# Orden Compra" UniqueName="CantidadBultos">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Proveedor" FilterControlAltText="Filter PesoKgs column"
                        HeaderText="Proveedor" UniqueName="PesoKgs">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Producto" FilterControlAltText="Filter PesoMiami column"
                        HeaderText="Producto" UniqueName="PesoMiami">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PosicionArancelaria" FilterControlAltText="Filter IngresoSwissport column"
                        HeaderText="Arancel" UniqueName="IngresoSwissport">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="%Arancel" FilterControlAltText="Filter SalidaSwissport column"
                        HeaderText="% Arancel" UniqueName="SalidaSwissport">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ValorImpuestoTotal" FilterControlAltText="Filter Producto column"
                        HeaderText="Valor Impuesto Total" UniqueName="Producto">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ETAPLANTA" FilterControlAltText="Filter GC1.Fecha column"
                        HeaderText="ETA Planta" UniqueName="GC1.Fecha">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NumeroFactura" FilterControlAltText="Filter ArribodeCarga column"
                        HeaderText="No. Factura" UniqueName="ArribodeCarga">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Incoterm" FilterControlAltText="Filter ENVIODEPRELIQUIDACION column"
                        HeaderText="Incoterm" UniqueName="ENVIODEPRELIQUIDACION">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ReferenciaCliente" FilterControlAltText="Filter APROBACIONDEPRELIQUIDACION column"
                        HeaderText="# Referencia Proveedor" UniqueName="APROBACIONDEPRELIQUIDACION">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DUA" FilterControlAltText="Filter ENVIODEBOLETIN column"
                        HeaderText="Numero DUA" UniqueName="ENVIODEBOLETIN">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Flete" FilterControlAltText="Filter PagodeImpuestos column"
                        HeaderText="Flete" UniqueName="PagodeImpuestos">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Seguro" FilterControlAltText="Filter Color column"
                        HeaderText="Seguro" UniqueName="Color">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ValorFactura" FilterControlAltText="Filter RevisionMercancia column"
                        HeaderText="Valor Facturado" UniqueName="RevisionMercancia">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PAISORIGEN" FilterControlAltText="Filter EmisionPaseSalida column"
                        HeaderText="Pais Origen" UniqueName="EmisionPaseSalida">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Transporte" FilterControlAltText="Filter EntregadeServicios column"
                        HeaderText="Tipo Importacion" UniqueName="EntregadeServicios">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IdRegimen" FilterControlAltText="Filter CodRegimen column"
                        HeaderText="Regimen" UniqueName="CodRegimen">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EstadoFlujo" FilterControlAltText="Filter EstadoFlujo column"
                        HeaderText="Status" UniqueName="EstadoFlujo" SortedBackColor="Aqua">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Courier" FilterControlAltText="Filter IngresoDeposito column"
                        HeaderText="Naviera, Courier" UniqueName="IngresoDeposito">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Contenedores" FilterControlAltText="Filter EntregaCliente column"
                        HeaderText="# de Contenedor" UniqueName="Contenedores">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DescripcionContenedores" FilterControlAltText="Filter DescripcionContenedores column"
                        HeaderText="Descripcion Contenedores" UniqueName="DescripcionContenedores">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DocumentoNo" FilterControlAltText="Filter Aforador column"
                        HeaderText="BL, GUIA o Carta Porte" UniqueName="Aforador">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PesoLibras" FilterControlAltText="Filter TiempoProceso column"
                        HeaderText="Peso en Lbs" UniqueName="TiempoProceso">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CantidadBultos" FilterControlAltText="Filter TiempoProceso2 column"
                        HeaderText="Cantidad" UniqueName="TiempoProceso2">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Presentacion" FilterControlAltText="Filter column column"
                        HeaderText="Presentacion" UniqueName="column">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TackingCurrier" FilterControlAltText="Filter column1 column"
                        HeaderText="# Tracking Del Courier " UniqueName="column1">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Fechaderecibocopiasdocs" FilterControlAltText="Filter column2 column"
                        HeaderText="Fecha Recibo Copias" UniqueName="column2">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Fecharecibooriginales" FilterControlAltText="Filter column3 column"
                        HeaderText="Fecha Recibo Originales" UniqueName="column3">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="MAGA" FilterControlAltText="Filter column4 column"
                        HeaderText="Fecha Inicio Tramite Importacion" UniqueName="column4">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Fechaextendidopermiso" FilterControlAltText="Filter column5 column"
                        HeaderText="Fecha Extencion Permiso" UniqueName="column5">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PermisoNo" FilterControlAltText="Filter column6 column"
                        HeaderText="Numero Permiso" UniqueName="column6">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Color" FilterControlAltText="Filter column7 column"
                        HeaderText="Canal Selectividad" UniqueName="column7">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FechaArriboAduana" FilterControlAltText="Filter column8 column"
                        HeaderText="Fecha Real Arribo Aduana" UniqueName="column8">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FechaPlantaProceso" FilterControlAltText="Filter column9 column"
                        HeaderText="Fecha Arribo Planta Proceso" UniqueName="column9">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TiempoLlegadaHastaEntrega" FilterControlAltText="Filter column10 column"
                        HeaderText="Tiempo Llegada Hasta Entrega" UniqueName="column10">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DiasLibresAlmacenaje" FilterControlAltText="Filter column11 column"
                        HeaderText="Dias Libres Almacenaje" UniqueName="column11">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TiempoEnergia" FilterControlAltText="Filter column12 column"
                        HeaderText="Tiempo  de Energia" UniqueName="column12">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TerminaTiempoLibre" FilterControlAltText="Filter column13 column"
                        HeaderText="Termina Tiempo Libre DCS" UniqueName="column13">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CargoAlmacenaje" FilterControlAltText="Filter column14 column"
                        HeaderText="Cargo Almacenaje DCS" UniqueName="column14">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TerminaTiempoLibreElectricidad" FilterControlAltText="Filter column15 column"
                        HeaderText="Termina tiempo Libre Electricidad" UniqueName="column15">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CargoDemoraElectricidad" FilterControlAltText="Filter column16 column"
                        HeaderText="Cargo Demora Electricidad" UniqueName="column16">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FechaRetornoContenedor" FilterControlAltText="Filter column17 column"
                        HeaderText="Fecha Retorno Contenedor" UniqueName="column17">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DiasDemora" FilterControlAltText="Filter column18 column"
                        HeaderText="Dias Demora" UniqueName="column18">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Demora" FilterControlAltText="Filter column19 column"
                        HeaderText="Demora" UniqueName="column19">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Notes" FilterControlAltText="Filter column20 column"
                        HeaderText="Notes" UniqueName="column20">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NumeroDeclaracionSeguro" FilterControlAltText="Filter column21 column"
                        HeaderText="# Declaracion Seguro" UniqueName="column21">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Fecha" FilterControlAltText="Filter column22 column"
                        HeaderText="Fecha" UniqueName="column22">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FechaFactura2" FilterControlAltText="Filter FechaFactura2 column"
                        HeaderText="Fechade Factura 2" UniqueName="FechaFactura2">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ColorCargill" FilterControlAltText="Filter ColorCargill column"
                        HeaderText="Color" UniqueName="ColorCargill">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PuertoOrigen" FilterControlAltText="Filter PuertoOrigen column"
                        HeaderText="Puerto de Origen" UniqueName="PuertoOrigen">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Color" 
                        FilterControlAltText="Filter Color column" 
                        HeaderText="Selectividad" UniqueName="Selectividad">
                        <ItemStyle BorderColor="Black" BorderStyle="Solid" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <HeaderStyle BorderStyle="Solid" ForeColor="Black" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        <table class="style12">
            <tr>
                <td style="text-align: center">
                    <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Images/24/disk_blue_ok_24.png"
                        OnClientClick="radconfirm('Esta seguro que Desea Generar el Reporte?',confirmCallBackSalvar, 300, 100); return false;"
                        ToolTip="Salvar" Visible="False" OnClick="btnGuardar_Click" style="height: 24px" />
                    <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="~/Images/gris.png" OnClick="btnSalvar_Click" />
                    <asp:Button ID="btnCargar" runat="server" Text="Cargar Archivo" Visible="false" />
                </td>
            </tr>
        </table>
        <br />
        <br />
        <br />
        <br />
        <table width="100%">
        </table>
    </telerik:RadAjaxPanel>
  
</asp:Content>
