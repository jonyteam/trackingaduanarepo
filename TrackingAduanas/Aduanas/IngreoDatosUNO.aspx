﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="IngreoDatosUNO.aspx.cs" Inherits="EditarInstrucciones" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanelFlujoGuias" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 | args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }                   
            </script>
        </telerik:RadScriptBlock>
        <input id="edIdInstruccion" runat="server" type="hidden" />
        <input id="edIdEstadoFlujo" runat="server" type="hidden" />
        <input id="edEstadoFlujo" runat="server" type="hidden" />
        <input id="edCodPaisHojaRuta" runat="server" type="hidden" />
        <input id="edCodRegimen" runat="server" type="hidden" />
        <input id="edRegimen" runat="server" type="hidden" />
        <input id="edFecha" runat="server" type="hidden" />
        <input id="edHora" runat="server" type="hidden" />
        <input id="edObservacion" runat="server" type="hidden" />
        <input id="edCorrelativo" runat="server" type="hidden" />
        <input id="edImpuesto" runat="server" type="hidden" />
        <input id="edColor" runat="server" type="hidden" />
        <input id="edTramitador" runat="server" type="hidden" />
        <input id="edAforador" runat="server" type="hidden" />
        <telerik:RadGrid ID="rgInstrucciones" runat="server" AllowFilteringByColumn="True"
            AllowSorting="True" AutoGenerateColumns="False" GridLines="None" Height="700px"
            OnNeedDataSource="rgInstrucciones_NeedDataSource" AllowPaging="True" ShowFooter="True"
            ShowStatusBar="True" PageSize="40" OnInit="rgInstrucciones_Init" OnItemCommand="rgInstrucciones_ItemCommand"
            OnItemDataBound="rgInstrucciones_ItemDataBound" 
            OnItemCreated="rgInstrucciones_ItemCreated" CellSpacing="0" Width="100%">
            <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
            <MasterTableView CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay instrucciones."
                GroupLoadMode="Client">
                <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                    ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px"></HeaderStyle>
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px"></HeaderStyle>
                </ExpandCollapseColumn>
                <Columns>
                    <%--<telerik:GridTemplateColumn HeaderText="Fecha" AllowFiltering="false" ShowFilterIcon="false">
                        <ItemTemplate>
                            <telerik:RadDatePicker ID="dtFechaSellado" runat="server" Width="50%" Culture="es-HN"
                                EnableTyping="False">
                                <Calendar ID="Calendar1" runat="server" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"
                                    UseRowHeadersAsSelectors="False">
                                </Calendar>
                                <DateInput ID="DateInput1" runat="server" DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"
                                    ReadOnly="True">
                                </DateInput><DatePopupButton HoverImageUrl="" ImageUrl="" />
                            </telerik:RadDatePicker>
                            <telerik:RadTimePicker ID="dtHoraSellado" runat="server" Width="45%" Culture="es-HN"
                                EnableTyping="True">
                                <TimeView ID="TimeView2" runat="server" CellSpacing="-1" Columns="6" Culture="es-HN"
                                    Interval="00:30:00" TimeFormat="t">
                                </TimeView>
                                <TimePopupButton HoverImageUrl="" ImageUrl="" />
                            </telerik:RadTimePicker>
                        </ItemTemplate>
                        <HeaderStyle Width="200px" />
                    </telerik:GridTemplateColumn>--%>
                    <telerik:GridBoundColumn DataField="tFecha" 
                        FilterControlAltText="Filter Fecha column" HeaderText="Fecha" DataFormatString="{0:dd-MM-yyyy}"
                        UniqueName="Fecha" FilterControlWidth="70%">
                        <HeaderStyle Width="75px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="tHoja" 
                        FilterControlAltText="Filter Hoja column" FilterControlWidth="80%" 
                        HeaderText="Hoja de Ruta" UniqueName="Hoja">
                        <HeaderStyle Width="75px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="tCliente" HeaderText="Cliente" UniqueName="Cliente"
                        FilterControlWidth="60%" FilterControlAltText="Filter Cliente column" 
                        AllowFiltering="False">
                        <HeaderStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="tCodigoRegimen" HeaderText="Codigo Regimen" UniqueName="CodigoRegimen"
                        FilterControlWidth="60%" 
                        FilterControlAltText="Filter CodigoRegimen column">
                        <HeaderStyle Width="50px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="tSerie" HeaderText="Serie" UniqueName="Serie"
                        FilterControlWidth="60%" FilterControlAltText="Filter Serie column">
                        <HeaderStyle Width="70px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="tCorrelativo" HeaderText="Correlativo"
                        UniqueName="Correlativo" FilterControlWidth="60%" 
                        FilterControlAltText="Filter Correlativo column" Visible="false">
                        <HeaderStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="tProducto" HeaderText="Producto" UniqueName="Producto"
                        FilterControlWidth="80%" FilterControlAltText="Filter Producto column">
                        <HeaderStyle Width="200px" />
                        <HeaderStyle Width="140px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" 
                        HeaderText="ITEM APV" UniqueName="TemplateColumn" AllowFiltering="False">
                        <ItemTemplate>
                            <telerik:RadNumericTextBox ID="edAPV" Runat="server" Culture="es-HN" 
                                DbValueFactor="1" EmptyMessage="INGRESE VALOR APV" LabelWidth="64px" 
                                Type="Currency" Width="90%">
                                <NumberFormat ZeroPattern="L. n" />
                            </telerik:RadNumericTextBox>
                        </ItemTemplate>
                        <HeaderStyle Width="95px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn1 column" 
                        HeaderText="Galones" UniqueName="TemplateColumn1" AllowFiltering="False">
                        <ItemTemplate>
                            <telerik:RadNumericTextBox ID="edGalones" Runat="server" 
                                EmptyMessage="Cantidad de Galones" Width="90%">
                            </telerik:RadNumericTextBox>
                        </ItemTemplate>
                        <HeaderStyle Width="95px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn3 column" 
                        HeaderText="Facturas" UniqueName="TemplateColumn3" AllowFiltering="False">
                        <ItemTemplate>
                            <telerik:RadTextBox ID="edFactura" Runat="server" 
                                EmptyMessage="INGRESE FACTURA" LabelWidth="64px" Width="90%">
                            </telerik:RadTextBox>
                        </ItemTemplate>
                        <HeaderStyle Width="95px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn2 column" 
                        UniqueName="TemplateColumn2" AllowFiltering="False">
                        <ItemTemplate>
                            <telerik:RadButton ID="btnAceptar" runat="server" onclick="btnAceptar_Click" 
                                Text="Ingresar" Width="100%">
                            </telerik:RadButton>
                        </ItemTemplate>
                        <HeaderStyle Width="50px" />
                    </telerik:GridTemplateColumn>
                </Columns>
                <EditFormSettings>

                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>

                </EditFormSettings>
            </MasterTableView>
            <HeaderStyle Width="180px" />
            <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                <Selecting AllowRowSelect="True" />
                <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                    DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                    DropHereToReorder="Suelte aquí para Re-Ordenar" />
                <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
            </ClientSettings>
            <FilterMenu EnableTheming="True">
                <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
            </FilterMenu>
            <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
            <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
        </telerik:RadGrid>
        <table width="100%">
            <tr>
                <td align="center">
                    &nbsp;</td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
</asp:Content>
