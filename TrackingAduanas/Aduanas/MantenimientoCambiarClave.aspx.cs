﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using GrupoLis.Login;
using System.Collections.Specialized;

public partial class MantenimientoCambiarClave : Utilidades.PaginaBase
{
    private NameValueCollection mParamethers;
    private GrupoLis.Login.Login logAppAduanas;
    private GrupoLis.Login.Login logAppEsquema;
    protected void Page_Load(object sender, EventArgs e)
    {
        mParamethers = ConfigurationManager.AppSettings;
        if (!IsPostBack)
        {
            String titulo = parameterQueryString("titulo", "Cambiar Clave");
            ((SiteMaster)Master).SetTitulo("Cambiar Clave", "Cambiar Clave");
        }
    }

    public override String QueryFormat { get { return @"^[\w-=., ]{1,200}$"; } }

    protected void btnCambiar_Click(object sender, EventArgs e)
    {
        try
        {
            conectar();
            UsuariosBO usuarios = new UsuariosBO(logApp);
            usuarios.loadUsuario(Session["IdUsuario"].ToString());
            if (Utilidades.PaginaBase.hashMD5(edOldPass.Text) == usuarios.PASSWORD)
            {
                usuarios.PASSWORD = Utilidades.PaginaBase.hashMD5(edNewPass.Text);
                usuarios.VENCIMIENTO = DateTime.Now.AddMonths(1);
                usuarios.actualizar();

                ///////////////////////////nuevo
                try
                {
                    conectarAduanas();
                    EmpleadosBO em = new EmpleadosBO(logAppAduanas);
                    em.loadUsuario(usuarios.USUARIO);
                    if (em.totalRegistros > 0)
                    {
                        em.PASSWORD = usuarios.PASSWORD;
                        em.actualizar();
                    }
                }
                catch (Exception ex)
                {
                    logError(ex.Message, Session["IdUsuario"].ToString(), "Cambiar Clave");
                }
                finally
                {
                    desconectarAduanas();
                }
                ///////////////////////////
                redirectTo("Default.aspx");
            }
            else
                registrarMensaje("La clave anterior no es valida");
        }
        catch
        {
        }
        finally
        {
            desconectar();
        }
    }

    public override bool CanGoBack { get { return false; } }

    #region Conexion
    private void conectarAduanas()
    {
        if (logAppAduanas == null)
            logAppAduanas = new GrupoLis.Login.Login();
        if (!logAppAduanas.conectado)
        {
            logAppAduanas.tipoConexion = TipoConexion.SQL_SERVER;
            logAppAduanas.SERVIDOR = mParamethers.Get("Servidor");
            logAppAduanas.DATABASE = mParamethers.Get("DatabaseEsquemaTramites2");
            logAppAduanas.USER = mParamethers.Get("User");
            logAppAduanas.PASSWD = mParamethers.Get("Password");
            logAppAduanas.conectar();
        }
    }

    private void desconectarAduanas()
    {
        if (logAppAduanas.conectado)
            logAppAduanas.desconectar();
    }
    #endregion

      private void conectarEsquema()
    {
        if (logAppEsquema == null)
            logAppEsquema = new GrupoLis.Login.Login();
        if (!logAppEsquema.conectado)
        {
            logAppEsquema.tipoConexion = TipoConexion.SQL_SERVER;
            logAppEsquema.SERVIDOR = mParamethers.Get("Servidor");
            logAppEsquema.DATABASE = mParamethers.Get("DatabaseEsquemaTramites2");
            logAppEsquema.USER = mParamethers.Get("User");
            logAppEsquema.PASSWD = mParamethers.Get("Password");
            logAppEsquema.conectar();
        }
    }

    private void desconectarEsquema()
    {
        if (logAppEsquema.conectado)
            logAppEsquema.desconectar();
    }
 
}