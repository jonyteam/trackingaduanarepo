﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReporteBitacora.aspx.cs"
    Inherits="ReporteEventos" MasterPageFile="~/Site.master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            text-align: center;
        }
        .auto-style1 {
            width: 639px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
   
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            ////<![CDATA[
            //function requestStart(sender, args) {
            //    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
            //        args.set_enableAjax(false);
            //    }
            //}

            function OnClientClose() {
                $find("<%= RadAjaxManager1.ClientID%>").ajaxRequest("Rebind");
            }
        </script>
    </telerik:RadScriptBlock>
    
     <telerik:RadTextBox ID="RadTextBox1" Runat="server">
    </telerik:RadTextBox>
    <br />
    <telerik:RadButton ID="RadButton1" runat="server" Text="Buscar" 
        onclick="RadButton1_Click">
    </telerik:RadButton>
    <br />
    
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
                <Windows>
                    <telerik:RadWindow ID="RadWindow1" runat="server" style="display:none;">
                    </telerik:RadWindow>
       </Windows>
            </telerik:RadWindowManager>
    <table class="style1">
        <tr>
            <td class="style2">
                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Eventos"></asp:Label>
                    </td>
        </tr>
    </table>
        <telerik:RadGrid ID="RadGrid1" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" 
            GridLines="None" Width="1293px" onitemcommand="RadGrid1_ItemCommand" 
             PageSize="20">
            <ClientSettings AllowColumnsReorder="True">
                <Selecting AllowRowSelect="True" />
            </ClientSettings>
             <ExportSettings HideStructureColumns="true" 
                FileName ="Reporte Eventos" OpenInNewWindow="True">  
                 <Excel Format ="Biff" />  </ExportSettings>
            <MasterTableView CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" 
                    ExportToPdfText="Export to PDF" ShowAddNewRecordButton = "false" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                    Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                    Visible="True">
                </ExpandCollapseColumn>
                <Columns>
        <telerik:GridBoundColumn DataField="Descripcion" HeaderText="Descripcion" 
            UniqueName="Descripcion" FilterControlAltText="Filter Preferencia column">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="FechaInicio" HeaderText="Fecha Inicio" 
            UniqueName="FechaInicio" FilterControlAltText="Filter FDCNumber column">
        </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ObservacionInicio" 
                        FilterControlAltText="Filter ObservacionInicio column" 
                        HeaderText="Observacion del Inicio" UniqueName="ObservacionInicio">
                    </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="FechaFin" HeaderText="Fecha Fin" 
            UniqueName="FechaFin" FilterControlAltText="Filter BUname column">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="ObservacionFin" 
            HeaderText="Observacion de Fin" UniqueName="ObservacionFin" 
                        FilterControlAltText="Filter ObservacionFin column">
        </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Nombre" 
                        FilterControlAltText="Filter Nombre column" HeaderText="Cliente" 
                        UniqueName="Nombre">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NumeroFactura" 
                        FilterControlAltText="Filter NumeroFactura column" HeaderText="Numero Factura" 
                        UniqueName="NumeroFactura">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ReferenciaCliente" 
                        FilterControlAltText="Filter ReferenciaCliente column" 
                        HeaderText="Referencia de Cliente" UniqueName="ReferenciaCliente">
                    </telerik:GridBoundColumn>
    </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        <br />
            <table class="style1">
                <tr>
                    <td class="style2">
                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Text="Comienzo Flujo"></asp:Label>
                    </td>
                </tr>
    </table>
        <telerik:RadGrid ID="RadGrid2" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" 
            GridLines="None" Width="1293px" onitemcommand="RadGrid2_ItemCommand" 
             PageSize="20">
            <ClientSettings AllowColumnsReorder="True">
                <Selecting AllowRowSelect="True" />
            </ClientSettings>
             <ExportSettings HideStructureColumns="true" 
                FileName ="Reporte Eventos" OpenInNewWindow="True">  
                 <Excel Format ="Biff" />  </ExportSettings>
            <MasterTableView CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" 
                    ExportToPdfText="Export to PDF" ShowAddNewRecordButton = "false" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                    Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                    Visible="True">
                </ExpandCollapseColumn>
                <Columns>
        <telerik:GridBoundColumn DataField="GestionCarga" HeaderText="Gestion Carga" 
            UniqueName="GestionCarga" FilterControlAltText="Filter Preferencia column">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="FechaGestor" HeaderText="Fecha Gestor" 
            UniqueName="FechaGestor" FilterControlAltText="Filter FDCNumber column">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Color" HeaderText="Color" 
            UniqueName="Color" FilterControlAltText="Filter BUname column">
        </telerik:GridBoundColumn>
        <telerik:GridBoundColumn DataField="Observacion" 
            HeaderText="Observacion" UniqueName="Observacion" 
                        FilterControlAltText="Filter ObservacionFin column">
        </telerik:GridBoundColumn>
    </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
   <%-- <table>
        <tr>
            <td class="auto-style1">
                <br />
                
        <telerik:RadGrid ID="RadGrid3" runat="server" AllowPaging="True" 
            AllowSorting="True" CellSpacing="0" 
            GridLines="None" Width="1293px" onitemcommand="RadGrid2_ItemCommand" 
             PageSize="20" Culture="es-ES">
            <ClientSettings AllowColumnsReorder="True">
                <Selecting AllowRowSelect="True" />
            </ClientSettings>
             <ExportSettings HideStructureColumns="true" 
                FileName ="Reporte Eventos" OpenInNewWindow="True">  
                 <Excel Format ="Biff" />  </ExportSettings>
            <MasterTableView CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" 
                    ExportToPdfText="Export to PDF" ShowAddNewRecordButton = "false" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                    Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                    Visible="True">
                </ExpandCollapseColumn>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
                <br />
            <telerik:RadGrid ID="RadGrid4" runat="server" AllowPaging="True" 
            AllowSorting="True" CellSpacing="0" 
            GridLines="None" Width="1293px" onitemcommand="RadGrid2_ItemCommand" 
             PageSize="20" Culture="es-ES">
            <ClientSettings AllowColumnsReorder="True">
                <Selecting AllowRowSelect="True" />
            </ClientSettings>
             <ExportSettings HideStructureColumns="true" 
                FileName ="Reporte Eventos" OpenInNewWindow="True">  
                 <Excel Format ="Biff" />  </ExportSettings>
            <MasterTableView CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" 
                    ExportToPdfText="Export to PDF" ShowAddNewRecordButton = "false" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                    Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                    Visible="True">
                </ExpandCollapseColumn>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
                <br />
                <br />
                <br />
                &nbsp; 
               
                <br />
                <br />
               
        </tr>
    </table>--%>
    <telerik:RadButton ID="ImageButton1" runat="server" CommandName="descarga"  ImageUrl="Images/16/index_down_16.png"  
                                            OnClientClick="radconfirm('Desea Bajar el Documento de Respaldo?',confirmCallBackSalvar, 300, 100); return false;"
                                            ToolTip="Salvar"  Text="Ver Documentos " OnClick="ImageButton1_Click" Visible="False"  >
                                        </telerik:RadButton>
               
                <telerik:RadButton ID="ImageButton2" runat="server" CommandName="descarga"  ImageUrl="Images/16/index_down_16.png"  
                                            OnClientClick="radconfirm('Desea Bajar el Documento de Respaldo?',confirmCallBackSalvar, 300, 100); return false;"
                                            ToolTip="Salvar"  Text="Ver Declaracion" OnClick="ImageButton2_Click" Visible="False"  >
                                        </telerik:RadButton>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgUsuarios">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgUsuarios" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rgRoles">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgRoles" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>



               
            </telerik:AjaxSetting>


            
        </AjaxSettings>

            
     
    </telerik:RadAjaxManager>

      
</asp:Content>
