﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.OleDb;

using Telerik.Web.UI;
using Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using GrupoLis.Ebase;
using GrupoLis.Login;
using System.Collections.Specialized;
using System.Configuration;

public partial class GenerarArchivoSAPDinant : Utilidades.PaginaBase
{
    private GrupoLis.Login.Login logAppAduanas;
    //string HojaRuta = "";
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "ReporteHagamodaPromoda";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Hagamoda/Promoda", "Reporte de Inventarios UNO");
        }
    }

    protected bool Consultar { get { return tienePermiso("Consultar"); } }

    private void ConfigureExport()
    {
        String filename = "Reporte_" + DateTime.Now.ToShortDateString();
        rgReporte.ExportSettings.FileName = filename;
        rgReporte.ExportSettings.ExportOnlyData = true;
        rgReporte.ExportSettings.IgnorePaging = true;
        rgReporte.ExportSettings.OpenInNewWindow = true;
        rgReporte.MasterTableView.ExportToExcel();
    }

    public override bool CanGoBack { get { return false; } }


    private void llenarGrid()
    {
        try
        {
            conectar();
            InstruccionesBO bo = new InstruccionesBO(logApp);
            bo.ReporteUNO(dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"));
            rgReporte.DataSource = bo.TABLA;
            rgReporte.DataBind();
            btnGuardar.Visible = true;
        }
        catch (Exception)
        {

        }

        finally { desconectar(); }
    }
    protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenarGrid();
    }
    #region Conexion
    private void conectarAduanas( string Pais)
    {
        if (logAppAduanas == null)
            logAppAduanas = new GrupoLis.Login.Login();
        if (!logAppAduanas.conectado)
        {
            logAppAduanas.tipoConexion = TipoConexion.SQL_SERVER;
            logAppAduanas.SERVIDOR = mParamethers.Get("Servidor2");
            if (Pais.ToString() == "S")
            {
                logAppAduanas.DATABASE = mParamethers.Get("CajaChicaSV");
            }
            if (Pais.ToString() == "N")
            {
                logAppAduanas.DATABASE = mParamethers.Get("CajaChicaNI");
            }
            if (Pais.ToString() == "G")
            {
                logAppAduanas.DATABASE = mParamethers.Get("CajaChicaGT");
            }
            logAppAduanas.USER = mParamethers.Get("User2");
            logAppAduanas.PASSWD = mParamethers.Get("Password2");
            logAppAduanas.conectar();
        }
    }

    private void desconectarAduanas()
    {
        if (logAppAduanas.conectado)
            logAppAduanas.desconectar();
    }
    #endregion
    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        writeToExcel();
        
    }
    protected void writeToExcel()
    {
        
            btnGuardar.Visible = false;
            string nameFile = "ReporteUNOT.xlsx";
            string nameDest = "ReporteUNO.xlsx";
            bool hayRegistro = false;
            Application.Lock();
            if (File.Exists("J:\\Files1\\Aduanas\\" + nameDest))
            {
                File.Delete("J:\\Files1\\Aduanas\\" + nameDest);
                File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
            }
            else
            {
                File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
            }

            //DIRECCION DE LA PLANTILLA
            var mTemplateFileName = @"J:\Files1\Aduanas\" + nameDest;

            var xlApp = new ApplicationClass() { DisplayAlerts = false, AskToUpdateLinks = false };
            var workbooks = xlApp.Workbooks;
            var workbook = workbooks.Open(mTemplateFileName, 2, false);

            try
            {
                conectar();
                InstruccionesBO I = new InstruccionesBO(logApp);
                TasadeCambioUNOBO TCU = new TasadeCambioUNOBO(logApp);
                I.ReporteUNO(dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"));
                TCU.LoadFecha2(dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"));
                #region Factura comisión
                //HOJA DE LA PLANTILLA       
                const string mInputWorkSheetName = "LIMPIOS OCT";   //Hoja1
                //POSICIONAR LA HOJA A TRABAJAR
                //var inputWorkSheet = workBook.Worksheets.Cast<Excel.Worksheet>().Where(w => w.Name == m_InputWorkSheetName).FirstOrDefault();

                var inputWorkSheet = workbook.Worksheets.Cast<Worksheet>().FirstOrDefault(w => w.Name == mInputWorkSheetName);
                int pos = 15, pos2 = 2;

                int contador = 0, contador2 = 0, contador3 = 1, contador4 = 1;
                string HojaRuta = "", HojadeRuta2 = "";
                int mimo = 0;
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                for (int m = 0; m < TCU.totalRegistros; m++)
                {
                    inputWorkSheet.Cells.Range["K" + pos2].Value = TCU.TABLA.Rows[m]["FechaInicio"].ToString();//Fecha Inicial
                    inputWorkSheet.Cells.Range["L" + pos2].Value = TCU.TABLA.Rows[m]["FechaFinal"].ToString();//Fecha Final
                    inputWorkSheet.Cells.Range["M" + pos2].Value = TCU.TABLA.Rows[m]["GASOLINASUPER"].ToString();//Gasolina Premion
                    inputWorkSheet.Cells.Range["N" + pos2].Value = TCU.TABLA.Rows[m]["GASOLINAREGULAR"].ToString();//Gasolina Regular
                    inputWorkSheet.Cells.Range["O" + pos2].Value = TCU.TABLA.Rows[m]["DIESEL"].ToString();//Diesel
                    inputWorkSheet.Cells.Range["P" + pos2].Value = TCU.TABLA.Rows[m]["AVJET"].ToString();//AV JET
                    inputWorkSheet.Cells.Range["Q" + pos2].Value = TCU.TABLA.Rows[m]["BUNKER"].ToString();//Bunker 
                    inputWorkSheet.Cells.Range["R" + pos2].Value = TCU.TABLA.Rows[m]["KEROSENO"].ToString();//Kerosene
                    pos2 = pos2 + 1;
                }
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                for (int j = 0; j < I.totalRegistros; j++)
                {
                    if (contador == 0)
                    {
                        HojaRuta = I.TABLA.Rows[j]["HojaRuta"].ToString();
                        HojadeRuta2 = HojaRuta;
                    }
                    if (HojadeRuta2 != HojaRuta)
                    {
                        HojadeRuta2 = HojaRuta;
                        contador2 = 0;
                    }
                    if (HojadeRuta2 == HojaRuta && contador2 == 0)
                    {
                        inputWorkSheet.Cells.Range["A" + pos].Value = contador4;
                        inputWorkSheet.Cells.Range["B" + pos].Value = I.TABLA.Rows[j]["Fecha"].ToString();//Fecha
                        inputWorkSheet.Cells.Range["C" + pos].Value = HojaRuta;//Hoja
                        inputWorkSheet.Cells.Range["D" + pos].Value = I.TABLA.Rows[j]["Cliente"].ToString();//Cliente
                        inputWorkSheet.Cells.Range["E" + pos].Value = I.TABLA.Rows[j]["Regimen"].ToString();//Regimen
                        inputWorkSheet.Cells.Range["F" + pos].Value = I.TABLA.Rows[j]["Serie"].ToString();//Serie
                        inputWorkSheet.Cells.Range["G" + pos].Value = I.TABLA.Rows[j]["Correlativo"].ToString();//No Correlativo
                        inputWorkSheet.Cells.Range["H" + pos].Value = I.TABLA.Rows[j]["ItemAPV"].ToString();//ItemAPV
                        contador2 = contador2 + 1;
                        contador4 = contador4 + 1;
                    }
                    if (contador2 > 0)
                    {

                        inputWorkSheet.Cells.Range["I" + pos].Value = "ITEM " + contador3;// # de Factura Vesta
                        //string producto = I.TABLA.Rows[j]["Producto"].ToString().TrimStart();//ItemAPV
                        inputWorkSheet.Cells.Range["J" + pos].Value = I.TABLA.Rows[j]["Producto"].ToString().TrimStart();//ItemAPV
                        inputWorkSheet.Cells.Range["K" + pos].Value = I.TABLA.Rows[j]["APV"].ToString();// # Orden de Compra//
                        inputWorkSheet.Cells.Range["L" + pos].Value = I.TABLA.Rows[j]["Galones"].ToString();// Proveedor/Galones
                        //inputWorkSheet.Cells.Range["P" + pos].Value = I.TABLA.Rows[j]["Kilos"].ToString();//Kilos
                        inputWorkSheet.Cells.Range["Q" + pos].Value = I.TABLA.Rows[j]["BL"].ToString();//BL o Guia Aerea o Carta Porte
                        inputWorkSheet.Cells.Range["R" + pos].Value = I.TABLA.Rows[j]["Factura"].ToString();//Factura
                        inputWorkSheet.Cells.Range["S" + pos].Value = I.TABLA.Rows[j]["Vapor"].ToString();//Vapor
                        pos = pos + 1;
                        mimo = mimo + 1;
                        contador3 = contador3 + 1;
                    }
                    if (mimo == Convert.ToInt16(I.TABLA.Rows[j]["Cuenta"].ToString()))
                    {
                        inputWorkSheet.Cells.Range["J" + pos].Value = "TOTAL";// Numero(HojaRuta)
                        inputWorkSheet.Cells.Range["K" + pos].Value = I.TABLA.Rows[j]["ItemAPV"].ToString();//ItemAPV
                        mimo = 0;
                        pos = pos + 2;
                        contador = 0;
                        contador2 = 0;
                        contador3 = 1;
                    }

                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                }
                rgReporte.Rebind();
                #endregion
                workbook.Save();

                //llenarBitacora("Se generó la carga " + idCarga, Session["IdUsuario"].ToString());
            }
            catch (Exception ex)
            {
                //LogError(ex.Message, Session["IdUsuario"].ToString(), "GenerarArchivoSAPDinant");
            }
            finally
            {
                //CERRAR EL LIBRO DE TRABAJO y la aplicacion

                workbook.Close();
                xlApp.Quit();
                desconectar();
                Application.UnLock();
            }
                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + nameDest);
                Response.Charset = "";
                Response.TransmitFile("J:\\Files1\\Aduanas\\" + nameDest);
                Response.Flush();
                Response.End();
            //llenarGrid();
        
    }

    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        if (dpFechaInicio.SelectedDate <= dpFechaFinal.SelectedDate)
        {

            llenarGrid();
            rgReporte.Visible = true;
        }
        else
            registrarMensaje("Fecha inicio no puede ser mayor que fecha final");
    }
}
