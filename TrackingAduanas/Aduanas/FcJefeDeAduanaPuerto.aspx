﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="FcJefeDeAduanaPuerto.aspx.cs" Inherits="FcJefeDeAduanaPuerto" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            ////<![CDATA[
            //function requestStart(sender, args) {
            //    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
            //        args.set_enableAjax(false);
            //    }
            //}

            function OnClientClose() {
                $find("<%= RadAjaxManager1.ClientID%>").ajaxRequest("Rebind");
            }

            <%-- function confirmCallBackSalvar(arg) {
                if (arg == true) {
                    var btn = document.getElementById('<%= FindControl("btnSalvar").ClientID %>');
                    if (btn != null)
                        btn.click();
                }
            }--%>
        </script>
    </telerik:RadScriptBlock>
    <div id="miDiv" runat="server" class="panelCentrado2">
        <table width="100%">
            <tr>
                <td>
                    <telerik:RadGrid ID="rgAsistenteOperaciones" runat="server" AllowFilteringByColumn="True"
                        AllowSorting="True" AutoGenerateColumns="False" GridLines="None" Width="100%"
                        Height="420px" OnNeedDataSource="rgAsistenteOperaciones_NeedDataSource" AllowPaging="True"
                        ShowFooter="True" ShowStatusBar="True" PageSize="20" OnInit="rgAsistenteOperaciones_Init"
                        OnItemCommand="rgAsistenteOperaciones_ItemCommand" OnItemDataBound="rgAsistenteOperaciones_ItemDataBound">
                        <HeaderContextMenu EnableTheming="True">
                            <CollapseAnimation Duration="200" Type="OutQuint" />
                        </HeaderContextMenu>
                        <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                            PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                            Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                        <MasterTableView DataKeyNames="Id,Correlativo,IdInstruccion" CommandItemDisplay="Top"
                            NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay instrucciones definidas."
                            GroupLoadMode="Client">
                            <CommandItemSettings ShowAddNewRecordButton="False" RefreshText=""
                                ShowExportToExcelButton="False" />
                            <RowIndicatorColumn>
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn>
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridBoundColumn DataField="IdInstruccion" HeaderText="IdInstruccion" UniqueName="Instruccion"
                                    FilterControlWidth="80%">
                                    <HeaderStyle Width="90px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Cliente" HeaderText="Cliente" UniqueName="Cliente"
                                    FilterControlWidth="80%">
                                    <HeaderStyle Width="130px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Producto" HeaderText="Producto" UniqueName="Producto"
                                    FilterControlWidth="80%">
                                    <HeaderStyle Width="130px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Correlativo" HeaderText="Correlativo" UniqueName="Correlativo"
                                    FilterControlWidth="70%">
                                    <HeaderStyle Width="80px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Proveedor" HeaderText="Proveedor" UniqueName="Proveedor" FilterControlWidth="70%">
                                    <HeaderStyle Width="130px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CodRegimen" HeaderText="Regimen" UniqueName="CodRegimen"
                                    FilterControlWidth="50%">
                                    <HeaderStyle Width="70px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="NoFactura" HeaderText="Factura" UniqueName="NoFactura"
                                    FilterControlWidth="60%">
                                    <HeaderStyle Width="80px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CanalSelectividad" HeaderText="Canal de Selectividad" UniqueName="CanalSelectividad"
                                    FilterControlWidth="70%">
                                    <HeaderStyle Width="100px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridButtonColumn ButtonType="ImageButton" CommandName="Descargar" HeaderText="Descargar"
                                    ImageUrl="Images/16/index_down_16.png" UniqueName="btnDescargar">
                                    <HeaderStyle HorizontalAlign="Center" Width="60px" />
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                </telerik:GridButtonColumn>
                                <telerik:GridTemplateColumn HeaderText="Revision Cita" AllowFiltering="false" ShowFilterIcon="false">
                                    <HeaderStyle Width="90px" />
                                    <ItemTemplate>
                                        <telerik:RadComboBox ID="edRevision" runat="server" Width="80%"
                                            EmptyMessage="Seleccione...">
                                            <Items>
                                                <telerik:RadComboBoxItem Value="True" Text="Si" runat="server" />
                                                <telerik:RadComboBoxItem Value="False" Text="No" runat="server" />
                                            </Items>
                                        </telerik:RadComboBox>
                                        <%-- <asp:RequiredFieldValidator ID="rfvRevision" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="16pt" Font-Bold="True"
                                            ControlToValidate="edRevision">
                                        </asp:RequiredFieldValidator>--%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <%--<telerik:GridTemplateColumn HeaderText="" AllowFiltering="false" ShowFilterIcon="false">
                                    <HeaderStyle HorizontalAlign="Center" Width="60px" />
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <telerik:RadButton ID="btnGuardar" runat="server" Text="Guardar" CommandName="Guardar">
                                        </telerik:RadButton>
                                       </ItemTemplate>
                                </telerik:GridTemplateColumn>--%>
                                <telerik:GridButtonColumn ConfirmText="¿Esta seguro que desea guardar la decisión de la revisión?"
                                    ConfirmDialogType="RadWindow" ConfirmTitle="Decisión revisión" UniqueName="DeleteColumn"
                                    ButtonType="ImageButton" CommandName="Guardar" ImageUrl="~/Images/24/disk_blue_ok_24.png">
                                    <HeaderStyle Width="5%" />
                                    <ItemStyle Width="5%" />
                                </telerik:GridButtonColumn>
                            </Columns>
                        </MasterTableView>
                        <HeaderStyle Width="180px" />
                        <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                            <Selecting AllowRowSelect="True" />
                            <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                                DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                                DropHereToReorder="Suelte aquí para Re-Ordenar" />
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
                        </ClientSettings>
                        <FilterMenu EnableTheming="True">
                            <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                        </FilterMenu>
                        <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                        <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
                    </telerik:RadGrid>
                </td>
            </tr>
            <tr style="text-align: center">
                <td>
                    <telerik:RadButton ID="btnReasignar" runat="server" Text="Reasignar" Width="100px" Height="30px"
                        BackColor="#E4E4E4" Font-Bold="True" Font-Size="12pt" OnClick="btnReasignar_Click"
                        CausesValidation="False">
                    </telerik:RadButton>
                </td>
            </tr>
        </table>
        <asp:Panel ID="Panel1" runat="server">
            <asp:Timer ID="Timer1" runat="server" Interval="300000" OnTick="Timer1_Tick" ClientIDMode="AutoID" />
        </asp:Panel>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server"></telerik:RadWindowManager>
    </div>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgAsistenteOperaciones">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgAsistenteOperaciones" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnReasignar">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="btnReasignar" LoadingPanelID="LoadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="Timer1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgAsistenteOperaciones" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>
