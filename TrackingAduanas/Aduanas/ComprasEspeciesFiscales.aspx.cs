﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;

public partial class ComprasEspeciesFiscales : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Compras Especies Fiscales";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgRangoEspeciesFiscales.FilterMenu);
        rgRangoEspeciesFiscales.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgRangoEspeciesFiscales.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Compras Especies Fiscales", "Compras Especies Fiscales");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
            mpEspeciesFiscales.SelectedIndex = 0;
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    #region Compras Especies Fiscales
    protected void rgRangoEspeciesFiscales_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenarGridRangoEspeciesFiscales();
    }

    private void llenarGridRangoEspeciesFiscales()
    {
        try
        {
            conectar();
            ComprasEspeciesFiscalesBO cef = new ComprasEspeciesFiscalesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (u.CODPAIS == "H")
                cef.loadRangosEspeciesFiscalesAll();
            else
                cef.loadRangosEspeciesFiscalesXPais(u.CODPAIS);
            rgRangoEspeciesFiscales.DataSource = cef.TABLA;
            rgRangoEspeciesFiscales.DataBind();
        }
        catch { }
    }

    protected void rgRangoEspeciesFiscales_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            ComprasEspeciesFiscalesBO cef = new ComprasEspeciesFiscalesBO(logApp);
            MovimientosEspeciesFiscalesBO mef = new MovimientosEspeciesFiscalesBO(logApp);
            GridEditableItem editedItem = e.Item as GridEditableItem;
            if (e.CommandName == "Delete")
            {
                String idCompra = editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdCompra"].ToString();
                cef.loadComprasEspeciesFiscalesXId(idCompra);
                mef.loadMovimientosEspeciesFiscalesCompras(idCompra);
                if (cef.totalRegistros > 0 & mef.totalRegistros > 0)
                {
                    //if (cef.CANTIDAD == mef.CANTIDAD)
                    //{
                    cef.CODESTADO = "100";
                    cef.actualizar();
                    registrarMensaje("Compra de especies fiscales eliminada exitosamente");
                    //}
                    //else
                    //    registrarMensaje("Compra no se puede eliminar, ya tiene documentos utilizados");
                }
                else
                    registrarMensaje("Comprano se puede eliminar, ya se utilizaron algunos documentos");
            }
            if (rgRangoEspeciesFiscales.Items.Count >= 1 & (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName))
                ConfigureExportRangoEF();
            else if (rgRangoEspeciesFiscales.Items.Count < 1 & (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName))
                e.Canceled = true;
        }
        catch { }
    }

    private void ConfigureExportRangoEF()
    {
        String filename = "Compras Especies Fiscales " + DateTime.Now.ToShortDateString();
        rgRangoEspeciesFiscales.ExportSettings.FileName = filename;
        rgRangoEspeciesFiscales.ExportSettings.ExportOnlyData = true;
        rgRangoEspeciesFiscales.ExportSettings.IgnorePaging = true;
        rgRangoEspeciesFiscales.ExportSettings.OpenInNewWindow = true;
        rgRangoEspeciesFiscales.MasterTableView.ExportToExcel();
    }

    protected void rgRangoEspeciesFiscales_Init(object sender, EventArgs e)
    {
        GridFilterMenu menu = rgRangoEspeciesFiscales.FilterMenu;
        menu.Items.RemoveAt(rgRangoEspeciesFiscales.FilterMenu.Items.Count - 2);
    }

    protected void btnCrear_Click(object sender, ImageClickEventArgs e)
    {
        limpiarControles();
        cargarDatosInicioRangos();
        mpEspeciesFiscales.SelectedIndex = 1;

    }

    private void cargarDatosInicioRangos()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (u.CODPAIS == "H")
                c.loadPaisesHojaRuta();
            else
                c.loadAllCampos("PAISES", u.CODPAIS);
            cmbPais.DataSource = c.TABLA;
            cmbPais.DataBind();
            loadAduanas();
            loadProveedor();
        }
        catch { }
    }

    private void loadAduanas()
    {
        try
        {
            conectar();
            AduanasBO a = new AduanasBO(logApp);
            if (cmbPais.SelectedValue == "G")
                a.loadNombreAduanas("ALMACEN GUATEMALA", "G");
            else
                a.loadPaisAduanas(cmbPais.SelectedValue);
            cmbAduana.DataSource = a.TABLA;
            cmbAduana.DataBind();
            loadEspeciesFiscales();
        }
        catch { }
    }

    private void loadEspeciesFiscales()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            if (cmbAduana.SelectedValue == "DEYSI" || cmbAduana.SelectedValue == "JOHNY" || cmbAduana.SelectedValue == "SAMAN")
                c.loadAllCampos("ESPECIESFISCALES", "F");
            else if (cmbAduana.SelectedValue == "023")
                c.loadCodigosXCategoriaCodigos("ESPECIESFISCALES", "'DTI','HN-MI','HN-MN'");
            else
                c.loadEspeciesnoAdmin();
            cmbEspecieFiscal.DataSource = c.TABLA;
            cmbEspecieFiscal.DataBind();
            cmbEspecieFiscal.Items.Remove(9);
        }
        catch { }
    }

        private void loadProveedor()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            c.loadAllCamposCodigos("PROVEEDORESESPECIES");
            cmbProveedor.DataSource = c.TABLA;
            cmbProveedor.DataBind();
            //cmbEspecieFiscal.Items.Remove(9);
        }
        catch { }
    }

    protected void cmbPais_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        loadAduanas();
    }

    protected void cmbAduana_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        loadEspeciesFiscales();
    }

    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            conectar();
            ComprasEspeciesFiscalesBO cef = new ComprasEspeciesFiscalesBO(logApp);
            MovimientosEspeciesFiscalesBO mef = new MovimientosEspeciesFiscalesBO(logApp);
            string nuevoId = "";
            if (!cmbAduana.IsEmpty)
            {
                if (!cmbEspecieFiscal.IsEmpty)
                {
                    if (!String.IsNullOrEmpty(txtNumeroFactura.Text.Trim()))
                    {
                        if (txtRangoInicial.Value.HasValue)
                        {
                            if (txtRangoFinal.Value.HasValue)
                            {
                                if (txtRangoFinal.Value >= txtRangoInicial.Value)
                                {
                                    //////////////////////////////////////////////
                                    ////Se creo esta parte para las cargas///////
                                    /////////////////////////////////////////////
                                    if (!cmbProveedor.IsEmpty)
                                    {
                                        if (txtValorCompra.Value.HasValue)
                                        {
                                            cef.loadComprasEspeciesFiscalesVerificar(txtRangoInicial.Text, txtRangoFinal.Text, cmbAduana.SelectedValue, cmbEspecieFiscal.SelectedValue);
                                            if (cef.totalRegistros <= 0)
                                            {
                                                cef.loadNuevoId("CEF-");
                                                nuevoId = cef.TABLA.Rows[0]["NuevoId"].ToString();
                                                if (nuevoId == "")
                                                {
                                                    nuevoId = "CEF-1";
                                                }

                                                cef.loadComprasEspeciesFiscales("-1", "-1");
                                                cef.newLine();
                                                cef.IDCOMPRA = nuevoId;
                                                if (cmbEspecieFiscal.SelectedValue != "SSB")
                                                {
                                                    cef.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                                                }
                                                else
                                                {
                                                    cef.IDESPECIEFISCAL = "SS";
                                                }
                                               
                                                cef.CODPAIS = cmbPais.SelectedValue;
                                                cef.CODIGOADUANA = cmbAduana.SelectedValue;
                                                cef.RANGOINICIAL = txtRangoInicial.Text.Trim();
                                                cef.RANGOFINAL = txtRangoFinal.Text.Trim();
                                                int cantidad = int.Parse((txtRangoFinal.Value - txtRangoInicial.Value + 1).ToString());
                                                cef.CANTIDAD = cantidad;
                                                cef.FECHA = DateTime.Now.ToString();
                                                cef.NUMEROFACTURA = txtNumeroFactura.Text.Trim();
                                                cef.CODESTADO = "0";
                                                cef.IDUSUARIO = Session["IdUsuario"].ToString();
                                                cef.PROVEEDOR = cmbProveedor.SelectedValue;
                                                cef.CARGA = "0";
                                                cef.TOTALCOMPRA = txtValorCompra.Text.Trim();
                                                cef.commitLine();
                                                cef.actualizar();

                                                DateTime fecha = DateTime.Now;
                                                //20120109.171835.761                                            
                                                string idMovimiento = fecha.Year.ToString() + fecha.Month.ToString("00") + fecha.Day.ToString("00") + "." + fecha.Hour.ToString("00") + fecha.Minute.ToString("00") + fecha.Second.ToString("00") + "." + fecha.Millisecond.ToString("000");

                                                mef.loadMovimientosEspeciesFiscales("-1", "-1");
                                                mef.newLine();
                                                mef.IDMOVIMIENTO = idMovimiento;
                                                mef.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                                                mef.CODPAIS = cmbPais.SelectedValue;
                                                mef.CODIGOADUANA = cmbAduana.SelectedValue;
                                                mef.RANGOINICIAL = txtRangoInicial.Text.Trim();
                                                mef.RANGOFINAL = txtRangoFinal.Text.Trim();
                                                mef.CANTIDAD = cantidad;
                                                mef.FECHA = DateTime.Now.ToString();
                                                mef.CODTIPOMOVIMIENTO = "0";    //compra
                                                mef.CODESTADO = "1";    //recibido
                                                mef.IDUSUARIO = Session["IdUsuario"].ToString();
                                                mef.IDCOMPRA = nuevoId;
                                                mef.commitLine();
                                                mef.actualizar();

                                                registrarMensaje("Compra de " + cmbEspecieFiscal.Text + " con rango del " + cef.RANGOINICIAL + " al " + cef.RANGOFINAL + " ingresada exitosamente");
                                                limpiarControles();
                                                mpEspeciesFiscales.SelectedIndex = 1;
                                                llenarBitacora("Se ingresó la compra de " + cmbEspecieFiscal.Text + " con un rango del " + cef.RANGOINICIAL + " al " + cef.RANGOFINAL, cef.IDUSUARIO, nuevoId);
                                                rgRangoEspeciesFiscales.Rebind();
                                            }
                                            else
                                                registrarMensaje("No puede crear este rango, algunos documentos están relacionados con el rango del " + cef.RANGOINICIAL + " al " + cef.RANGOFINAL);
                                        }
                                        else
                                            registrarMensaje("El Valor de Compra no puede estar vacio");
                                    }
                                    else
                                    registrarMensaje("El Proveedor no puede estar vacio");
                                }
                                else
                                    registrarMensaje("El rango final no puede ser menor que el rango inicial");
                            }
                            else
                                registrarMensaje("El rango final no puede estar vacío");
                        }
                        else
                            registrarMensaje("El rango inicial no puede estar vacío");
                    }
                    else
                        registrarMensaje("El número de factura no puede estar vacío");
                }
                else
                    registrarMensaje("La especie fiscal no puede estar vacía");
            }
            else
                registrarMensaje("La aduana no puede estar vacía");
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "EspeciesFiscales");
        }
        finally
        {
            desconectar();
        }
    }

    protected void btnLimpiar_Click(object sender, ImageClickEventArgs e)
    {
        limpiarControles();
    }

    protected void btnBack_Click(object sender, ImageClickEventArgs e)
    {
        mpEspeciesFiscales.SelectedIndex = 0;
    }

    private void limpiarControles()
    {
        try
        {
            cargarDatosInicioRangos();
            loadAduanas();
            cmbAduana.ClearSelection();
            cmbEspecieFiscal.ClearSelection();
            txtNumeroFactura.Text = "";
            txtRangoInicial.Text = "";
            txtRangoFinal.Text = "";
            txtValorCompra.Text = "";
            cmbProveedor.ClearSelection();
        }
        catch { }
    }
    #endregion
}