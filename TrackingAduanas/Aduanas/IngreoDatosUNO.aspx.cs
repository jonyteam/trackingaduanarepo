﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;

using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.SessionState;
using System.Collections.Specialized;
using GrupoLis.Login;
using System.Text.RegularExpressions;
using System.Security.Principal;
using Telerik.Web;
using System.Security.Cryptography;

public partial class EditarInstrucciones : Utilidades.PaginaBase
{
    private NameValueCollection mParamethers;
    private GrupoLis.Login.Login logAppAduanas;

    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Ingreso UNO";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        mParamethers = ConfigurationManager.AppSettings;
        SetGridFilterMenu(rgInstrucciones.FilterMenu);
        rgInstrucciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgInstrucciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            //if (!Modificar)
            //rgInstrucciones.ClientSettings.Scrolling.AllowScroll = true;
            //rgInstrucciones.ClientSettings.Scrolling.UseStaticHeaders = true;
            //rgInstrucciones.ShowHeader = true;
            //rgInstrucciones.ShowFooter = true;
            ((SiteMaster)Master).SetTitulo("Ingreso de Datos UNO", "Ingreso de Datos UNO");
        }
    }

    protected void rgInstrucciones_Init(object sender, System.EventArgs e)
    {
        GridFilterMenu menu = rgInstrucciones.FilterMenu;
        menu.Items.RemoveAt(rgInstrucciones.FilterMenu.Items.Count - 2);
    }

    //protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void rgInstrucciones_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridInstrucciones();
    }

    private void llenarGridInstrucciones()
    {
        try
        {
            conectar();
            InstruccionesBO ins = new InstruccionesBO(logApp);
            UNOBO UNO = new UNOBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            int edCantidad;
            if (User.IsInRole("Administradores"))
                ins.loadInstruccionesUNO();
            else if (User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                ins.loadInstruccionesUNOXPais(u.CODPAIS);
                
            }
            DataTable tabla = new DataTable();
            DataRow fila;
            tabla.Columns.Add("tFecha", typeof(String));
            tabla.Columns.Add("tHoja", typeof(String));
            tabla.Columns.Add("tCliente", typeof(String));
            tabla.Columns.Add("tCodigoRegimen", typeof(String));
            tabla.Columns.Add("tSerie", typeof(String));
            tabla.Columns.Add("tCorrelativo", typeof(String));
            tabla.Columns.Add("tProducto", typeof(String));
            for (int j = 0; j < ins.totalRegistros; j++)
            {
                string codEspecieFiscal = ins.TABLA.Rows[j]["Producto"].ToString();
                string[] arreglo = codEspecieFiscal.Trim().Split(",".ToCharArray());
                edCantidad = arreglo.Length;
                string producto = "";
                for (int i = 0; i < edCantidad; i++)
                {
                    producto = arreglo[i];
                    UNO.VerificarProducto(ins.TABLA.Rows[j]["Hoja"].ToString(), producto.TrimStart());
                   if (UNO.totalRegistros == 0)
                   {
                        fila = tabla.NewRow();
                        fila["tFecha"] = ins.TABLA.Rows[j]["Fecha"].ToString();
                        fila["tHoja"] = ins.TABLA.Rows[j]["Hoja"].ToString();
                        fila["tCliente"] = ins.TABLA.Rows[j]["Cliente"].ToString();
                        fila["tCodigoRegimen"] = ins.TABLA.Rows[j]["CodigoRegimen"].ToString();
                        fila["tSerie"] = ins.TABLA.Rows[j]["Serie"].ToString();
                        fila["tCorrelativo"] = ins.TABLA.Rows[j]["Correlativo"].ToString();
                        fila["tProducto"] = producto;
                        tabla.Rows.Add(fila);
                   }
                }
                ins.regSiguiente();
            }
            
            rgInstrucciones.DataSource = tabla;
            
        }
        catch (Exception) { }
        finally
        {
            desconectar();
        }
    }

    protected void rgInstrucciones_ItemCommand(object sender, GridCommandEventArgs e)
    {

    }

    private void ConfigureExport()
    {
        rgInstrucciones.Columns[4].Visible = false;
        rgInstrucciones.Columns[5].Visible = false;
        rgInstrucciones.Columns[6].Visible = false;
        rgInstrucciones.Columns[7].Visible = false;
        rgInstrucciones.Columns[8].Visible = false;
        rgInstrucciones.Columns[9].Visible = false;
        rgInstrucciones.Columns[10].Visible = false;
        rgInstrucciones.Columns[11].Visible = false;
        rgInstrucciones.Columns[12].Visible = false;
        String filename = "Instrucciones" + "_" + DateTime.Now.ToShortDateString();
        rgInstrucciones.GridLines = GridLines.Both;
        rgInstrucciones.ExportSettings.FileName = filename;
        rgInstrucciones.ExportSettings.IgnorePaging = true;
        rgInstrucciones.ExportSettings.OpenInNewWindow = false;
        rgInstrucciones.ExportSettings.ExportOnlyData = true;
    }
    protected void rgInstrucciones_ItemDataBound(object sender, GridItemEventArgs e)
    {
        try
        {
            foreach (GridDataItem gridItem in rgInstrucciones.Items)
            {
                RadComboBox edCampo = (RadComboBox)gridItem.FindControl("edCampo");
                RadComboBox edCmbValor = (RadComboBox)gridItem.FindControl("edCmbValor");
                RadTextBox edTxtValor = (RadTextBox)gridItem.FindControl("edTxtValor");
                RadNumericTextBox edNumValor = (RadNumericTextBox)gridItem.FindControl("edNumValor");
                switch (edCampo.SelectedValue)
                {
                    case "Seleccione":
                        edCmbValor.Visible = false;
                        edTxtValor.Visible = false;
                        edNumValor.Visible = false;
                        break;
                    default: break;
                }
            }
        }
        catch { }
    }

    protected void SetVisible(TableCellCollection cells, String commandName, string visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is RadTextBox)
                {
                    RadTextBox txt = (RadTextBox)control;
                    if (txt.ID == commandName)
                        txt.Visible = true;
                    else
                        txt.Visible = false;
                }
            }
        }
    }

    protected void rgInstrucciones_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            try
            {
               
            }
            catch { }
        }
    }
 
    private void llenarGridInstruccion(string idInstruccion)
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            i.loadInstruccionesAllXInstruccion(idInstruccion);
            rgInstrucciones.DataSource = i.TABLA;
            rgInstrucciones.DataBind();
        }
        catch { }
    }
    protected void btnAceptar_Click(object sender, EventArgs e)
    {
        try
        {
            
            GridDataItem editForm = (sender as RadButton).NamingContainer as GridDataItem;
            string HojaRuta = editForm.Cells[3].Text;
            string Cliente = editForm.Cells[4].Text;
            string Producto = editForm.Cells[8].Text;
            RadNumericTextBox APV = editForm.FindControl("edAPV") as RadNumericTextBox;
            RadNumericTextBox Galones = editForm.FindControl("edGalones") as RadNumericTextBox;
            RadTextBox Factura = editForm.FindControl("edFactura") as RadTextBox;
            conectar();
            UNOBO U = new UNOBO(logApp);
            U.VerificarProducto(HojaRuta, Producto);
            if (U.totalRegistros <= 0)
            {
                U.newLine();
                U.IDINSTRUCCION = HojaRuta;
                U.PRODUCTO = Producto.TrimStart();
                U.ITEMAPV = Convert.ToDouble(APV.Text);
                U.GALONES = Convert.ToInt32(Galones.Text);
                U.FACTURA = Factura.Text;
                U.FECHA = DateTime.Now.ToString();
                U.IDUSUARIO = Session["IdUsuario"].ToString();
                U.CODESTADO = "0";
                U.commitLine();
                U.actualizar();
                registrarMensaje("Datos ingresados correctamente");
                rgInstrucciones.Rebind();   
            }
            else
            {
                registrarMensaje("Ya se ingreso el APV para este producto y para esta Hoja de Ruta");
            }

        }
        catch (Exception)
        {

            throw;
        }
        finally { desconectar(); }

    }
}