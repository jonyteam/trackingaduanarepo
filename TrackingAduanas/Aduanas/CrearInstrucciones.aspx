﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="CrearInstrucciones.aspx.cs" Inherits="CrearInstrucciones" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                    //<![CDATA[
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0
                    || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }                
            </script>
        </telerik:RadScriptBlock>
        <table width="100%">
            <tr>
                <td colspan="2">
                    <telerik:RadGrid ID="rgInstrucciones" runat="server" AllowFilteringByColumn="True"
                        AllowSorting="True" AutoGenerateColumns="False" GridLines="None" Width="100%"
                        Height="400px" OnNeedDataSource="rgInstrucciones_NeedDataSource" AllowPaging="True"
                        ShowFooter="True" ShowStatusBar="True" PageSize="20" OnInit="rgInstrucciones_Init"
                        OnItemCommand="rgInstrucciones_ItemCommand" CellSpacing="0">
                        <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                            PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                            Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                        <MasterTableView DataKeyNames="Instruccion,Estado" CommandItemDisplay="TopAndBottom"
                            NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay instrucciones."
                            GroupLoadMode="Client">
                            <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                                ShowExportToExcelButton="true" ShowExportToCsvButton="true" />
                            <RowIndicatorColumn>
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn>
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </ExpandCollapseColumn>
                            <Columns>
                                <telerik:GridButtonColumn ConfirmText="¿Está seguro que desea crear ó modificar una instrucción?"
                                    ConfirmDialogType="RadWindow" ConfirmTitle="Crear Instrucción" ButtonType="ImageButton"
                                    HeaderText="Crear Instrucción" CommandName="Crear" ImageUrl="Images/16/note_add_16.png"
                                    UniqueName="btnCrear">
                                    <ItemStyle Width="100px" />
                                    <HeaderStyle Width="100px" />
                                </telerik:GridButtonColumn>
                                <telerik:GridButtonColumn ConfirmText="¿Está seguro de que desea duplicar esta instrucción?"
                                    ConfirmDialogType="RadWindow" ConfirmTitle="Duplicar Instrucción" UniqueName="DuplicateColumn"
                                    ButtonType="ImageButton" ImageUrl="~/Images/24/cd_new_24.png" CommandName="Duplicar"
                                    HeaderText="Duplicar">
                                    <ItemStyle Width="35px" />
                                    <HeaderStyle Width="35px" />
                                </telerik:GridButtonColumn>
                                <telerik:GridBoundColumn DataField="Instruccion" HeaderText="Instruccion No." UniqueName="Instruccion"
                                    FilterControlWidth="80%">
                                    <ItemStyle Width="30%" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Nombre" HeaderText="Cliente" UniqueName="Nombre"
                                    FilterControlWidth="80%">
                                    <ItemStyle Width="30%" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="NumeroFactura" 
                                    FilterControlAltText="Filter column column" HeaderText="Numero de Factura" 
                                    UniqueName="column">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="ReferenciaCliente" HeaderText="Referencia Cliente"
                                    UniqueName="ReferenciaCliente" FilterControlWidth="80%">
                                    <ItemStyle Width="30%" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Estado" HeaderText="Estado" UniqueName="Estado"
                                    FilterControlWidth="80%">
                                    <ItemStyle Width="30%" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="NoManifiesto" HeaderText="No. Manifiesto" UniqueName="NoManifiesto"
                                    FilterControlWidth="80%">
                                    <ItemStyle Width="30%" />
                                </telerik:GridBoundColumn>
                            </Columns>
                            <EditFormSettings>
                                <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                </EditColumn>
                            </EditFormSettings>
                        </MasterTableView>
                        <HeaderStyle Width="180px" />
                        <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                            <Selecting AllowRowSelect="True" />
                            <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                                DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                                DropHereToReorder="Suelte aquí para Re-Ordenar" />
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
                        </ClientSettings>
                        <FilterMenu EnableTheming="True">
                            <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                        </FilterMenu>
                        <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                        <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
        <table width="100%">
            <tr>
                <td align="center">
                    <asp:ImageButton ID="btnCrear" ToolTip="Crear Instrucción" ImageUrl="Images/32/note_add_32.png"
                        runat="server" OnClick="btnCrear_Click" />
                    <asp:ImageButton ID="btnVerInstruccion" ToolTip="Ver Instrucción" ImageUrl="Images/32/table_sql_view_32.png"
                        runat="server" OnClick="btnVerInstruccion_Click" />
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
</asp:Content>
