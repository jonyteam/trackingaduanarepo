﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.OleDb;

using Telerik.Web.UI;
using Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using System.Collections;

public partial class GenerarArchivoSAPDinant : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "ReporteHagamodaPromoda";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //   ((SiteMaster)Master).SetTitulo("Reporte Hagamoda/Promoda","");
        }
    }

    protected bool Consultar { get { return tienePermiso("Consultar"); } }




    private void ConfigureExport()
    {
        String filename = "Reporte_" + DateTime.Now.ToShortDateString();
        grReporte.ExportSettings.FileName = filename;
        grReporte.ExportSettings.ExportOnlyData = true;
        grReporte.ExportSettings.IgnorePaging = true;
        grReporte.ExportSettings.OpenInNewWindow = true;
        grReporte.MasterTableView.ExportToExcel();
    }

    public override bool CanGoBack { get { return false; } }

    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {

    }
    private void llenarGrid()
    {

        try
        {
            conectar();
            InstruccionesBO bo = new InstruccionesBO(logApp);
            //bo.ReporteHagamodaPromoda();
            bo.ReporteHCC(dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"), cmbCliente.SelectedValue);
            grReporte.DataSource = bo.TABLA;
            Session["TotalRegistro"] = bo.totalRegistros;
            //RadGrid1.DataBind();
            btnGuardar.Visible = true;

        }
        catch { }

        finally { desconectar(); }

    }



    protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName)
                ConfigureExport();
        }
        catch (Exception)
        { }
    }
    protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenarGrid();
    }
    protected void RadGrid1_ExcelExportCellFormatting(object sender, ExcelExportCellFormattingEventArgs e)
    {
       
    }
    protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
    {
        {
            try
            {
                
            }
            catch { }
        }
    }

    protected void writeToExcel()
    {
        if (grReporte.Items.Count >= 0)
        {
            btnGuardar.Visible = false;
            string nameFile = "HCCTemp.xlsx";
            string nameDest = "HCC.xlsx";
            bool hayRegistro = false;
            Application.Lock();
            if (File.Exists("J:\\Files1\\Aduanas\\" + nameDest))
            {
                File.Delete("J:\\Files1\\Aduanas\\" + nameDest);
                File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
            }
            else
            {
                File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
            }

            //DIRECCION DE LA PLANTILLA
            var mTemplateFileName = @"J:\Files1\Aduanas\" + nameDest;

            var xlApp = new ApplicationClass() { DisplayAlerts = false, AskToUpdateLinks = false };
            var workbooks = xlApp.Workbooks;
            var workbook = workbooks.Open(mTemplateFileName, 2, false);

            try
            {
                conectar();
                #region Escribir Excell
                //HOJA DE LA PLANTILLA       
                const string mInputWorkSheetName = "Datos";   //Hoja1
                //POSICIONAR LA HOJA A TRABAJAR
                //var inputWorkSheet = workBook.Worksheets.Cast<Excel.Worksheet>().Where(w => w.Name == m_InputWorkSheetName).FirstOrDefault();

                var inputWorkSheet = workbook.Worksheets.Cast<Worksheet>().FirstOrDefault(w => w.Name == mInputWorkSheetName);
                int pos = 13;
                foreach (GridDataItem gridItem in grReporte.Items)
                {
                    pos = pos + 1;

                    hayRegistro = true;

                    if (gridItem.Cells[19].Text == "&nbsp;")
                    {
                        gridItem.Cells[19].Text = "";
                    }
                    if (gridItem.Cells[28].Text == "&nbsp;")
                    {
                        gridItem.Cells[28].Text = "";
                    }
                    if (gridItem.Cells[55].Text == "&nbsp;")
                    {
                        gridItem.Cells[55].Text = "";
                    }
                    if (gridItem.Cells[59].Text == "&nbsp;")
                    {
                        gridItem.Cells[59].Text = "";
                    }
                    inputWorkSheet.Cells.Range["A" + pos].Value = gridItem.Cells[2].Text;// Tipo Importacion
                    inputWorkSheet.Cells.Range["B" + pos].Value = gridItem.Cells[3].Text;// Producto
                    inputWorkSheet.Cells.Range["C" + pos].Value = gridItem.Cells[4].Text;// Planta
                    inputWorkSheet.Cells.Range["D" + pos].Value = gridItem.Cells[5].Text;// Aduana
                    inputWorkSheet.Cells.Range["E" + pos].Value = gridItem.Cells[6].Text;// Numero(HojaRuta)
                    inputWorkSheet.Cells.Range["F" + pos].Value = gridItem.Cells[7].Text;// Fecha Registro
                    inputWorkSheet.Cells.Range["G" + pos].Value = gridItem.Cells[8].Text;// # de Factura Vesta
                    inputWorkSheet.Cells.Range["H" + pos].Value = gridItem.Cells[9].Text;// Fecha Entrega Cargill
                    inputWorkSheet.Cells.Range["I" + pos].Value = gridItem.Cells[10].Text;// # Orden de Compra
                    inputWorkSheet.Cells.Range["J" + pos].Value = gridItem.Cells[11].Text;// Proveedor
                    inputWorkSheet.Cells.Range["K" + pos].Value = gridItem.Cells[12].Text;// Producto
                    inputWorkSheet.Cells.Range["M" + pos].Value = gridItem.Cells[13].Text;// Arancel
                    inputWorkSheet.Cells.Range["N" + pos].Value = gridItem.Cells[14].Text;// % Arancel
                    inputWorkSheet.Cells.Range["O" + pos].Value = gridItem.Cells[15].Text;// Valor Total Impuesto
                    inputWorkSheet.Cells.Range["P" + pos].Value = gridItem.Cells[16].Text;// ETA Planta
                    inputWorkSheet.Cells.Range["Q" + pos].Value = gridItem.Cells[17].Text;// No. Factura
                    inputWorkSheet.Cells.Range["R" + pos].Value = gridItem.Cells[18].Text;// Incoterm
                    inputWorkSheet.Cells.Range["S" + pos].Value = gridItem.Cells[19].Text;// # Referencia Proveedor
                    inputWorkSheet.Cells.Range["T" + pos].Value = gridItem.Cells[20].Text;// Numero de Dua
                    inputWorkSheet.Cells.Range["U" + pos].Value = gridItem.Cells[21].Text;// Flete
                    inputWorkSheet.Cells.Range["V" + pos].Value = gridItem.Cells[22].Text;// Seguro
                    inputWorkSheet.Cells.Range["W" + pos].Value = gridItem.Cells[23].Text;// Valor Facturado
                    inputWorkSheet.Cells.Range["X" + pos].Value = gridItem.Cells[24].Text;// Pais Origen
                    inputWorkSheet.Cells.Range["Y" + pos].Value = gridItem.Cells[59].Text;// Puerto Origen
                    inputWorkSheet.Cells.Range["Z" + pos].Value = gridItem.Cells[25].Text;// Tipo Importacion
                    inputWorkSheet.Cells.Range["AA" + pos].Value = gridItem.Cells[26].Text;// Regimen
                    inputWorkSheet.Cells.Range["AB" + pos].Value = gridItem.Cells[27].Text;// Status
                    inputWorkSheet.Cells.Range["AC" + pos].Value = gridItem.Cells[28].Text;// Naviera/Currier
                    inputWorkSheet.Cells.Range["AD" + pos].Value = gridItem.Cells[29].Text;// # de Contenedor
                    inputWorkSheet.Cells.Range["AE" + pos].Value = gridItem.Cells[30].Text;// Descripcion de Contenedor
                    inputWorkSheet.Cells.Range["AF" + pos].Value = gridItem.Cells[31].Text;// BL/Guia/Carta Porte
                    inputWorkSheet.Cells.Range["AG" + pos].Value = gridItem.Cells[32].Text;// Peso en Libra
                    inputWorkSheet.Cells.Range["AH" + pos].Value = gridItem.Cells[33].Text;// Cantidad
                    inputWorkSheet.Cells.Range["AI" + pos].Value = gridItem.Cells[34].Text;// Presentacion
                    inputWorkSheet.Cells.Range["AJ" + pos].Value = gridItem.Cells[35].Text;// # Tracking del Courier
                    inputWorkSheet.Cells.Range["AK" + pos].Value = gridItem.Cells[36].Text;// Fecha Recibo Copias
                    inputWorkSheet.Cells.Range["AL" + pos].Value = gridItem.Cells[37].Text;// Fecha Recibo Originales
                    inputWorkSheet.Cells.Range["AM" + pos].Value = gridItem.Cells[38].Text;// Fecha Inicio Tramite Importacion
                    inputWorkSheet.Cells.Range["AN" + pos].Value = gridItem.Cells[39].Text;// Fecha Extencion Permiso
                    inputWorkSheet.Cells.Range["AO" + pos].Value = gridItem.Cells[40].Text;// Numero Permiso
                    inputWorkSheet.Cells.Range["AP" + pos].Value = gridItem.Cells[41].Text;// Canal Selectividad
                    inputWorkSheet.Cells.Range["AQ" + pos].Value = gridItem.Cells[42].Text;// Fecha Real Arribo Aduana
                    inputWorkSheet.Cells.Range["AR" + pos].Value = gridItem.Cells[43].Text;// Fecha Arribo Planta Proceso
                    inputWorkSheet.Cells.Range["AS" + pos].Value = gridItem.Cells[44].Text;// Tiempo Llegada Hasta Entrega
                    inputWorkSheet.Cells.Range["AT" + pos].Value = gridItem.Cells[45].Text;// Dias Demora
                    inputWorkSheet.Cells.Range["AU" + pos].Value = gridItem.Cells[46].Text;// Tiempo de Energia
                    inputWorkSheet.Cells.Range["AV" + pos].Value = gridItem.Cells[47].Text;// Dias Libres Demora
                    inputWorkSheet.Cells.Range["AW" + pos].Value = gridItem.Cells[48].Text;// Cargo Demoras DCS
                    inputWorkSheet.Cells.Range["AX" + pos].Value = gridItem.Cells[49].Text;// Termina tiempo Libre Electricidad
                    inputWorkSheet.Cells.Range["AY" + pos].Value = gridItem.Cells[50].Text;// Cargo Demora Electricidad
                    inputWorkSheet.Cells.Range["AZ" + pos].Value = gridItem.Cells[51].Text;// Fecha Retorno Contenedor
                    inputWorkSheet.Cells.Range["BB" + pos].Value = gridItem.Cells[52].Text;// Dias Demora
                    inputWorkSheet.Cells.Range["BC" + pos].Value = gridItem.Cells[53].Text;// Demora
                    inputWorkSheet.Cells.Range["BD" + pos].Value = gridItem.Cells[54].Text;// Notes
                    //inputWorkSheet.Cells.Range["BC" + pos].Value = gridItem.Cells[54].Text;// # Declaracion Seguro
                    inputWorkSheet.Cells.Range["BE" + pos].Value = gridItem.Cells[55].Text;// Fecha
                    //inputWorkSheet.Cells.Range["BE" + pos].Value = gridItem.Cells[56].Text;// Fechade Factura 2
                    inputWorkSheet.Cells.Range["BN" + pos].Value = gridItem.Cells[58].Text;// Color
                    inputWorkSheet.Cells.Range["BF" + pos].Value = gridItem.Cells[60].Text;// Color
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                }
                grReporte.Rebind();
                #endregion
                workbook.Save();

                //llenarBitacora("Se generó la carga " + idCarga, Session["IdUsuario"].ToString());
            }
            catch (Exception ex)
            {
                //LogError(ex.Message, Session["IdUsuario"].ToString(), "GenerarArchivoSAPDinant");
            }
            finally
            {
                //CERRAR EL LIBRO DE TRABAJO y la aplicacion

                workbook.Close();
                xlApp.Quit();
                desconectar();
                Application.UnLock();
            }
            if (hayRegistro)
            {
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + nameDest);
            Response.Charset = "";
            Response.TransmitFile("J:\\Files1\\Aduanas\\" + nameDest);
            Response.Flush();
            Response.End();

            }
            //llenarGrid();
        }
    }
    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        writeToExcel();
    }
    protected void btnGuardar_Click(object sender, ImageClickEventArgs e)
    {

    }
}
