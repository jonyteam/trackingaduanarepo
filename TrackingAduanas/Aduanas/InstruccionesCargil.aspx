﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="InstruccionesCargil.aspx.cs" Inherits="InstruccionesCargil" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
   
    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
                    //<![CDATA[
            function requestStart(sender, args) {
//                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
//                    args.set_enableAjax(false);
//                }
            }

            function mensaje(msg) {
                var retorno = confirm(msg);
                return retorno;
            }

            var message = "Grupo Vesta, Derechos Reservados";
            function habilitarButtonCargar() { 
                
            }

            function click(e) {
                if (document.all) {
                    if (event.button == 2) {
                        alert(message);
                        return false;
                    }
                }
                if (document.layers) {
                    if (e.which == 3) {
                        alert(message);
                        return false;
                    }
                }
            }
            if (document.layers) {
                document.captureEvents(Event.MOUSEDOWN);
            }
            document.onmousedown = click;
        </script>
    </telerik:RadScriptBlock>
    <div style="width: 100%; height: 100%; padding-left: 20px;">
    <br />
    <br />
        <asp:Panel ID="PanelSubirArchivo" runat="server" BorderStyle="Inset" 
            Width="600px">
        <table width="600px" style="border: thin solid #0066FF">
            <tr>
                <td colspan="2" style="padding-left: 60px">
                    <br />                   
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Buscar Archivo (.xls - Excel 2007)" 
                        Font-Bold="True"></asp:Label>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <telerik:RadUpload ID="fileUpload" runat="server" AllowedFileExtensions=".xls,xlsx"
                        ControlObjectsVisibility="None" EnableFileInputSkinning="true" FocusOnLoad="true"
                        InitialFileInputsCount="1" MaxFileInputsCount="1" MaxFileSize="5242880" OverwriteExistingFiles="true"
                        TargetPhysicalFolder="J:\Files1\Aduanas\" Width="100%" Font-Bold="True">
                    </telerik:RadUpload>
                    <telerik:RadProgressManager ID="RadProgressManager1" runat="server" />
                    <telerik:RadProgressArea ID="RadProgressArea1" runat="server" 
                        HeaderText="Subiendo el archivo, espere por favor.">
                    </telerik:RadProgressArea>                 
                    <br />
                </td>
                <td>
                    <div class="smallModule">
                        <asp:Label ID="labelNoResults" runat="server">Aun No hay Archivos subidos...</asp:Label>
                        <asp:Repeater ID="repeaterResults" runat="server" Visible="False">
                            <HeaderTemplate>
                                <div>
                                    <asp:Label ID="lblTitulo" runat="server" Text=""></asp:Label></div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#DataBinder.Eval(Container.DataItem, "FileName")%>
                                <%#DataBinder.Eval(Container.DataItem, "ContentLength").ToString() + " bytes"%>
                                <br />
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 20px">
                    <asp:Button ID="btnCargar" runat="server" Text="Cargar Archivo" 
                        OnClick="btnCargar_Click" Font-Bold="True" />
                    <telerik:RadCodeBlock ID="RadCodeBlock2" runat="server">
                        <input id="filename" name="filename" type="hidden" value="<%=filename%>" />
                    </telerik:RadCodeBlock>
                    <br />
                    <br />
                </td>
                <td>
                    <asp:Label ID="LabelMensajes" runat="server" 
                        Text="Archivo procesado exitosamente..." Font-Bold="True" ForeColor="#339933" 
                        Font-Size="14pt" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="padding-left: 20px">
                    <%--<asp:Button ID="btnProcesar" runat="server" Text="Procesar Archivo Excel" Enabled="False"
                        OnClick="btnProcesar_Click" Font-Bold="True" />--%>
                    <telerik:RadButton ID="RadButtonProcesar" runat="server" 
                        Text="Procesar Archivo Excel" Enabled="False" Font-Bold="True" 
                        onclick="RadButtonProcesar_Click" >
                    </telerik:RadButton>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding-left: 20px">
                <br />               
                </td>
            </tr>
        </table>
        </asp:Panel>
    </div>
        
   
    <%--<%#DataBinder.Eval(Container.DataItem, "ContentLength").ToString() + " bytes"%>--%>
    <telerik:RadAjaxManager ID="RadAjaxManagerCarcarSol" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadButtonProcesar">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadButtonProcesar" LoadingPanelID="LoadingPanel"/>
                    <telerik:AjaxUpdatedControl ControlID="PanelResultado" LoadingPanelID=""/>                                   
                </UpdatedControls>
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="LabelMensajes" LoadingPanelID="" />
                </UpdatedControls>                                            
            </telerik:AjaxSetting>                  
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>
