﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ObjetosDto;
using Telerik.Web.UI;
using Utilities.Web;
using System.Configuration;
using System.Collections.Specialized;

public partial class FcJefeDeAduanaPuertoEstados : Utilidades.PaginaBase
{
    readonly AduanasDataContext _aduanasDc = new AduanasDataContext();
    private NameValueCollection mParamethers;
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Reporte Eventos";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        mParamethers = ConfigurationManager.AppSettings;
        string idHojaRuta = parameterQueryString("IdHojaRuta", "").Trim();
        Llenar_Boletin(idHojaRuta);
        if (!IsPostBack)
        {
           ((SiteMaster)Master).SetTitulo("Declaracion", "");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
        }

   
    }

    private bool Anular { get { return tienePermiso("ANULAR"); } }
    private bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    private bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    public override bool CanGoBack { get { return false; } }




    public void Llenar_Boletin(string IdInstrucccion)
    {


        var queryDeclaracion = _aduanasDc.DeclaracionAduaneras.Where(p => p.IdInstruccion == IdInstrucccion).SingleOrDefault();
        if (queryDeclaracion != null)
        {
            
       
            var queryItmes = _aduanasDc.DeclaracionItems.Where(p => p.IdInstruccion == IdInstrucccion).ToList();
            var queryConceptos = _aduanasDc.DeclaracionConceptos.Where(p => p.Correlativo == queryDeclaracion.Correlativo).ToList();
            var queryBultos = _aduanasDc.DeclaracionBultos.Where(p => p.Correlativo == queryDeclaracion.Correlativo).ToList();


            this.Correlativo.Text = queryDeclaracion.Correlativo;
            this.Preimpreso.Text = queryDeclaracion.Preimpreso;
            this.RegimenAduanero.Text = queryDeclaracion.RegimenAduanero;
            this.ImportadorExportador.Text = queryDeclaracion.ImportadorExportador;
            this.FechaOficializacion.Text = queryDeclaracion.FechaOficializacion.ToString();
            this.EstadoDeclaracion.Text = queryDeclaracion.EstadoDeclaracion;
            this.AgenteAduanero.Text = queryDeclaracion.AgenteAduanero;
            this.AgenteAduanero2.Text = queryDeclaracion.AgenteAduanero;
            this.AduanaIngreso.Text = queryDeclaracion.AduanaIngreso;
            this.PaisOrigen.Text = queryDeclaracion.PaisOrigen;
            this.PaisProcedencia.Text = queryDeclaracion.PaisProcedencia;
            this.CondicionEntrega.Text = queryDeclaracion.CondicionEntrega;
            this.ValorFacturaFleteSeg.Text = queryDeclaracion.ValorFacturaFleteSeg;
            this.Correlativo2.Text = queryDeclaracion.TituloTransporte;
            this.CantidadComercial2.Text = queryDeclaracion.CantidadItems.ToString();
           // string specifier = "#,#.00#;(#,#.00#)";
          //  Decimal? Valor = queryConceptos.Sum(x => x.ImporteLps);
        //    this.ImporteLps.Text = string.Format("{0:0.00}", Valor);;
            this.ImporteLps.Text = queryDeclaracion.TotalPagar.ToString();
     
            this.totalgarantia.Text = queryDeclaracion.TotalGarantizar.ToString();
            this.RadGridBultos.DataSource = queryBultos;
            this.RadGridBultos.DataBind();

            this.RadGridItems.DataSource = queryItmes;
            this.RadGridItems.DataBind();

            this.RadGridConceptos.DataSource = queryConceptos;
            this.RadGridConceptos.DataBind();
        }
        else
        {
            registrarMensaje("La hoja de ruta aun no tiene una Declaracion asociada.");
            //redirectTo("");
        }
    }


    private void RegistrarMensaje2(string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("alert('" + mensaje + "');");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "mensaje", sb.ToString(), false);
    }
   

    protected void RadGridBultos_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {

    }
    protected void RadGridItems_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {

    }
    protected void RadGridConceptos_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {

    }
}