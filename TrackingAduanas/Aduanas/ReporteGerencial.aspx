﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteGerencial.aspx.cs" Inherits="ReporteGerencial" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <table style="width: 45%">
        <tr>
            <td rowspan="6">
                <input id="codigoUno" runat="server" type="hidden" />
                <input id="codigoDos" runat="server" type="hidden" />
                <input id="agruparUno" runat="server" type="hidden" />
                <input id="agruparDos" runat="server" type="hidden" />
                <asp:RadioButtonList ID="rbFiltrar" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbFiltrar_SelectedIndexChanged">
                    <asp:ListItem Value="A.NombreAduana AS Descripcion">Por Aduana</asp:ListItem>
                    <asp:ListItem Value="C.Nombre AS Descripcion">Por Cliente</asp:ListItem>
                    <asp:ListItem Value="C2.Descripcion AS Descripcion">Por Regimen</asp:ListItem>
                    <asp:ListItem Value="C3.Descripcion AS Descripcion">Por Origen</asp:ListItem>
                    <asp:ListItem Value="5">Por Aduana, Cliente y Regimen</asp:ListItem>
                </asp:RadioButtonList>
                <telerik:RadComboBox ID="cmbDescripcion1" runat="server" DataTextField="Descripcion"
                    DataValueField="Codigo" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="cmbDescripcion1_SelectedIndexChanged">
                </telerik:RadComboBox>
                <telerik:RadComboBox ID="cmbDescripcion2" runat="server" DataTextField="Descripcion"
                    DataValueField="Codigo" AutoPostBack="true" Visible="false" OnSelectedIndexChanged="cmbDescripcion2_SelectedIndexChanged">
                </telerik:RadComboBox>
                <telerik:RadComboBox ID="cmbDescripcion3" runat="server" DataTextField="Descripcion"
                    DataValueField="Codigo" AutoPostBack="true" Visible="false">
                </telerik:RadComboBox>
            </td>
            <td rowspan="6">
            </td>
            <td style="width: 15%">
                <asp:Label ID="lblFechaInicio" runat="server" Text="Fecha Inicio:" ForeColor="Black"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 22%">
                <telerik:RadDatePicker ID="dpFechaInicio" runat="server" EnableTyping="False" Width="95%">
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="dpFechaInicio"
                    ErrorMessage="Ingrese Fecha" ForeColor="Red" />
            </td>
        </tr>
        <tr>
            <td style="width: 15%">
                <asp:Label ID="lblFechaFinal" runat="server" Text="Fecha Final:" ForeColor="Black"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 22%">
                <telerik:RadDatePicker ID="dpFechaFinal" runat="server" EnableTyping="False" Width="95%">
                </telerik:RadDatePicker>
                <asp:RequiredFieldValidator runat="server" ID="reqName" ControlToValidate="dpFechaFinal"
                    ErrorMessage="Ingrese Fecha" ForeColor="Red" />
            </td>
        </tr>
        <tr>
            <td style="width: 10%">
                <asp:Label ID="lblPais" runat="server" Text="País:" ForeColor="Black"></asp:Label>
            </td>
        </tr>
        <tr>
            <td style="width: 16%">
                <telerik:RadComboBox ID="cmbPais" runat="server" DataTextField="Descripcion" DataValueField="Codigo">
                </telerik:RadComboBox>
            </td>
        </tr>
        <tr>
            <td colspan="6" align="center">
                <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Images/24/view_24.png"
                    OnClick="btnBuscar_Click" />
            </td>
        </tr>
    </table>
    <telerik:RadGrid ID="rgInstrucciones" Skin="Default" runat="server" Visible="false"
        AllowSorting="True" AllowFilteringByColumn="True" AutoGenerateColumns="False"
        GridLines="None" Height="410px" OnNeedDataSource="rgInstrucciones_NeedDataSource"
        AllowPaging="True" ShowFooter="True" ShowStatusBar="True" PageSize="20" OnInit="rgInstrucciones_Init"
        OnItemCommand="rgInstrucciones_ItemCommand">
        <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
            PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
            Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
        <MasterTableView DataKeyNames="" CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros."
            NoMasterRecordsText="No hay instrucciones." GroupLoadMode="Client" ShowGroupFooter="true">
            <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                ShowExportToExcelButton="true" />
            <RowIndicatorColumn>
                <HeaderStyle Width="20px"></HeaderStyle>
            </RowIndicatorColumn>
            <ExpandCollapseColumn>
                <HeaderStyle Width="20px"></HeaderStyle>
            </ExpandCollapseColumn>
            <Columns>
                <telerik:GridBoundColumn DataField="País" HeaderText="País" UniqueName="País" FilterControlWidth="60%">
                    <HeaderStyle Width="100px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Descripcion" HeaderText="Descripción" UniqueName="Descripcion"
                    FilterControlWidth="60%" FooterText="Total" FooterStyle-HorizontalAlign="Right">
                    <HeaderStyle Width="100px" />
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Cantidad" HeaderText="Cantidad" UniqueName="Cantidad"
                    FilterControlWidth="60%" Aggregate="Sum" DataFormatString="{0:D}">
                    <HeaderStyle Width="120px" />
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <HeaderStyle Width="180px" />
        <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
            <Selecting AllowRowSelect="True" />
            <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                DropHereToReorder="Suelte aquí para Re-Ordenar" />
            <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
        </ClientSettings>
        <FilterMenu EnableTheming="True">
            <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
        </FilterMenu>
        <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
        <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
    </telerik:RadGrid>
    <telerik:RadGrid ID="rgInstrucciones2" Skin="Default" enableajax="True" Visible="false"
        ShowStatusBar="true" runat="server" AllowFilteringByColumn="False" Width="100%"
        Height="410px" AutoGenerateColumns="False" PageSize="20" AllowSorting="True"
        AllowMultiRowSelection="False" AllowPaging="True" GridLines="None" OnDetailTableDataBind="rgInstrucciones2_DetailTableDataBind"
        OnNeedDataSource="rgInstrucciones2_NeedDataSource" OnItemCommand="rgInstrucciones2_ItemCommand">
        <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
            PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
            Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
        <PagerStyle Mode="NumericPages"></PagerStyle>
        <MasterTableView Width="100%" DataKeyNames="Codigo1" AllowMultiColumnSorting="True"
            CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay registros."
            GroupLoadMode="Client">
            <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                ShowExportToExcelButton="true" ShowExportToCsvButton="false" />
            <DetailTables>
                <telerik:GridTableView DataKeyNames="Codigo1,Codigo2" DataMember="ClientesOC" Width="100%"
                    GridLines="Horizontal" Style="border-color: #d5b96a" CssClass="RadGrid2">
                    <DetailTables>
                        <telerik:GridTableView DataKeyNames="Codigo2" DataMember="Detalle2" Width="100%"
                            GridLines="Horizontal" Style="border-color: #d5b96a" CssClass="RadGrid3">
                            <Columns>
                                <telerik:GridBoundColumn SortExpression="Descripcion3" HeaderText="Descripcion3"
                                    HeaderButtonType="TextButton" DataField="Descripcion3">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn SortExpression="Cantidad" HeaderText="Cantidad" HeaderButtonType="TextButton"
                                    DataField="Cantidad" UniqueName="Cantidad">
                                </telerik:GridBoundColumn>
                            </Columns>
                        </telerik:GridTableView>
                    </DetailTables>
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="Codigo2" HeaderText="Codigo2" HeaderButtonType="TextButton"
                            DataField="Codigo2" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="Codigo1" HeaderText="Codigo1" HeaderButtonType="TextButton"
                            DataField="Codigo1" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="Descripcion2" HeaderText="Descripcion2"
                            HeaderButtonType="TextButton" DataField="Descripcion2">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="Cantidad" HeaderText="Cantidad" HeaderButtonType="TextButton"
                            DataField="Cantidad">
                        </telerik:GridBoundColumn>
                    </Columns>
                </telerik:GridTableView>
            </DetailTables>
            <Columns>
                <telerik:GridBoundColumn SortExpression="País" HeaderText="País" HeaderButtonType="TextButton"
                    DataField="País">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="Codigo1" HeaderText="Codigo1" HeaderButtonType="TextButton"
                    DataField="Codigo1" Visible="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="Descripcion1" HeaderText="Descripcion1"
                    HeaderButtonType="TextButton" DataField="Descripcion1">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="Cantidad" HeaderText="Cantidad" HeaderButtonType="TextButton"
                    DataField="Cantidad">
                </telerik:GridBoundColumn>
            </Columns>
        </MasterTableView>
        <HeaderStyle Width="180px" />
        <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
            <Selecting AllowRowSelect="True" />
            <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                DropHereToReorder="Suelte aquí para Re-Ordenar" />
            <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
        </ClientSettings>
        <FilterMenu EnableTheming="True">
            <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
        </FilterMenu>
        <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
        <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
    </telerik:RadGrid>
</asp:Content>
