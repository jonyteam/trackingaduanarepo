﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="FcCrearInstruccionFaucaCliente.aspx.cs" Inherits="FcCrearInstruccionFaucaCliente" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="cmbEspecieFiscal">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="cmbRegimen" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cmbCliente">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="cmbProducto" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cmbProducto">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="cmbProducto" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="txtValorFOB">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="txtValorCIF" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="txtFlete">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="txtValorCIF" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="txtSeguro">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="txtValorCIF" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="txtOtros">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="txtValorCIF" />
                </UpdatedControls>
            </telerik:AjaxSetting>

            <telerik:AjaxSetting AjaxControlID="btnGuardar">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="btnGuardar" LoadingPanelID="LoadingPanel"
                        UpdatePanelRenderMode="Inline" />
                    <%--<telerik:AjaxUpdatedControl ControlID="cmbPais" />
                    <telerik:AjaxUpdatedControl ControlID="cmbAduana" />
                    <telerik:AjaxUpdatedControl ControlID="cmbEspecieFiscal" />
                    <telerik:AjaxUpdatedControl ControlID="dtpFechaRecepcion" />
                    <telerik:AjaxUpdatedControl ControlID="btnCancelar" />--%>
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="btnCancelar">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="btnCancelar" LoadingPanelID="LoadingPanel"
                        UpdatePanelRenderMode="Inline" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadScriptBlock ID="scripts" runat="server">
        <script type="text/javascript">
            //<![CDATA[
            function cerrar() {
                window.close();
            }

            function GetRadWindow() {
                var oWindow = null;
                if (window.radWindow) oWindow = window.radWindow;
                else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
                return oWindow;
            }

            function Close() {
                var oWindow = GetRadWindow();
                oWindow.argument = null;
                oWindow.close();
            }
        </script>
    </telerik:RadScriptBlock>
    <div id="miDiv" runat="server" class="panelCentrado2">
        <table width="100%">
            <tr>
                <td colspan="2" style="width: 35%">
                    <div id="div2" class="Encabezado" runat="server" style="padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 17%">
                                    <asp:Label ID="lblPais" runat="server" Text="País" Font-Bold="true" />
                                </td>
                                <td style="width: 30%">
                                    <telerik:RadComboBox ID="cmbPais" runat="server" CausesValidation="false"
                                        Enabled="False" Width="99%" DataTextField="Descripcion" DataValueField="Codigo1">
                                    </telerik:RadComboBox>
                                </td>
                                <td style="width: 3%">
                                    <asp:RequiredFieldValidator ID="rfvPais" runat="server" EnableClientScript="true"
                                        Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="16pt" Font-Bold="True"
                                        ControlToValidate="cmbPais">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 17%">
                                    <asp:Label ID="lblAduana" runat="server" Text="Aduana" Font-Bold="true" />
                                </td>
                                <td style="width: 30%">
                                    <telerik:RadComboBox ID="cmbAduana" runat="server" CausesValidation="false"
                                        Enabled="False" Width="99%" DataTextField="NombreAduana" DataValueField="CodigoAduana">
                                    </telerik:RadComboBox>
                                </td>
                                <td style="width: 3%">
                                    <asp:RequiredFieldValidator ID="rfvAduana" runat="server" EnableClientScript="true"
                                        Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="16pt" Font-Bold="True"
                                        ControlToValidate="cmbAduana">
                                    </asp:RequiredFieldValidator></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblEspecieFiscal" runat="server" Text="Especie Fiscal" Font-Bold="true" />
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="cmbEspecieFiscal" runat="server" CausesValidation="false"
                                        Width="99%" DataTextField="Descripcion" DataValueField="Codigo1"
                                        Filter="None" EmptyMessage="Seleccione..." AutoPostBack="True"
                                        OnSelectedIndexChanged="cmbEspecieFiscal_SelectedIndexChanged">
                                    </telerik:RadComboBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvEspecieFiscal" runat="server" EnableClientScript="true"
                                        Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="16pt" Font-Bold="True"
                                        ControlToValidate="cmbEspecieFiscal">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <asp:Label ID="lblFechaRecepcion" runat="server" Text="Fecha de Recepción" Font-Bold="true" />
                                </td>
                                <td>
                                    <telerik:RadDateTimePicker ID="dtpFechaRecepcion" runat="server" Width="99%"
                                        MinDate="2016-01-01" MaxDate="2040-12-31">
                                        <TimeView ID="TimeView2" Columns="6" Culture="es-HN" TimeFormat="T" runat="server">
                                        </TimeView>
                                    </telerik:RadDateTimePicker>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvFechaRecepcion" runat="server" EnableClientScript="true"
                                        Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="16pt" Font-Bold="True"
                                        ControlToValidate="dtpFechaRecepcion">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblCliente" runat="server" Text="Cliente" Font-Bold="true" />
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="cmbCliente" runat="server" CausesValidation="false"
                                        Width="99%" DataTextField="Nombre" DataValueField="CodigoSAP"
                                        Filter="Contains" EmptyMessage="Seleccione..." AutoPostBack="True" OnSelectedIndexChanged="cmbCliente_SelectedIndexChanged">
                                    </telerik:RadComboBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvCliente" runat="server" EnableClientScript="true"
                                        Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="16pt" Font-Bold="True"
                                        ControlToValidate="cmbCliente">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <asp:Label ID="lblProveedor" runat="server" Text="Proveedor" Font-Bold="true" />
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="cmbProveedor" runat="server" CausesValidation="false"
                                        Width="99%" DataTextField="Descripcion" DataValueField="Codigo"
                                        Filter="Contains" EmptyMessage="Seleccione...">
                                    </telerik:RadComboBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvProveedor" runat="server" EnableClientScript="true"
                                        Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="16pt" Font-Bold="True"
                                        ControlToValidate="cmbProveedor">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblProducto" runat="server" Font-Bold="true" Text="Producto" />
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="cmbProducto" runat="server" CausesValidation="False"
                                        Width="99%" DataTextField="Descripcion" Filter="Contains"
                                        EmptyMessage="Seleccione..." CheckBoxes="True" AutoPostBack="True"
                                        OnItemChecked="cmbProducto_ItemChecked">
                                    </telerik:RadComboBox>
                                    <%--<telerik:RadComboBox ID="cmbProducto" runat="server" EmptyMessage="Seleccione..."
                                        DataTextField="Descripcion" Width="99%" CausesValidation="false"
                                        Filter="Contains" CheckBoxes="True" AutoPostBack="True">
                                    </telerik:RadComboBox>--%>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvProducto" runat="server" EnableClientScript="true"
                                        Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="16pt" Font-Bold="True"
                                        ControlToValidate="cmbProducto">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td>
                                    <asp:Label ID="lblNumeroFactura" runat="server" Text="Número de Factura" Font-Bold="true" />
                                </td>
                                <td>
                                    <telerik:RadTextBox ID="txtNumeroFactura" runat="server" Width="99%" MaxLength="50">
                                    </telerik:RadTextBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="rfvNumeroFactura" runat="server" EnableClientScript="true"
                                        Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="16pt" Font-Bold="True"
                                        ControlToValidate="txtNumeroFactura">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblValorFOB" runat="server" Font-Bold="true" Text="Valor FOB" />
                                </td>
                                <td>
                                    <telerik:RadNumericTextBox ID="txtValorFOB" runat="server" MinValue="0" Value="0"
                                        NumberFormat-DecimalDigits="2" NumberFormat-DecimalSeparator="." NumberFormat-GroupSeparator=","
                                        OnTextChanged="txtValorFOB_TextChanged" AutoPostBack="true" Width="99%">
                                    </telerik:RadNumericTextBox>
                                </td>
                                <td></td>
                                <td>
                                    <asp:Label ID="lblFlete" runat="server" Font-Bold="true" Text="Flete" />
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadNumericTextBox ID="txtFlete" runat="server" MinValue="0" Value="0" NumberFormat-DecimalDigits="2"
                                        NumberFormat-DecimalSeparator="." NumberFormat-GroupSeparator="," OnTextChanged="txtFlete_TextChanged"
                                        AutoPostBack="true" Width="99%">
                                    </telerik:RadNumericTextBox>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblSeguro" runat="server" Font-Bold="true" Text="Seguro" />
                                </td>
                                <td>
                                    <telerik:RadNumericTextBox ID="txtSeguro" runat="server" MinValue="0" Value="0" NumberFormat-DecimalDigits="2"
                                        NumberFormat-DecimalSeparator="." NumberFormat-GroupSeparator="," OnTextChanged="txtSeguro_TextChanged"
                                        AutoPostBack="true" Width="99%">
                                    </telerik:RadNumericTextBox>
                                </td>
                                <td></td>
                                <td>
                                    <asp:Label ID="lblOtros" runat="server" Font-Bold="true" Text="Otros" />
                                </td>
                                <td>
                                    <telerik:RadNumericTextBox ID="txtOtros" runat="server" MinValue="0" Value="0" NumberFormat-DecimalDigits="2"
                                        NumberFormat-DecimalSeparator="." NumberFormat-GroupSeparator="," OnTextChanged="txtOtros_TextChanged"
                                        AutoPostBack="true" Width="99%">
                                    </telerik:RadNumericTextBox>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblValorCIF" runat="server" Font-Bold="true" Text="Valor CIF" />
                                </td>
                                <td>
                                    <telerik:RadNumericTextBox ID="txtValorCIF" runat="server" Enabled="false" MinValue="0"
                                        NumberFormat-DecimalDigits="2" NumberFormat-DecimalSeparator="." NumberFormat-GroupSeparator=","
                                        Value="0" Width="99%">
                                    </telerik:RadNumericTextBox>
                                </td>
                                <td></td>
                                <td>
                                    <asp:Label ID="lblRegimen" runat="server" Text="Régimen" Font-Bold="true" />
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="cmbRegimen" runat="server" CausesValidation="false"
                                        Width="99%" DataTextField="Descripcion" DataValueField="Codigo1" Enabled="False">
                                    </telerik:RadComboBox>
                                </td>
                                <td>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" EnableClientScript="true"
                                        Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="16pt" Font-Bold="True"
                                        ControlToValidate="cmbCliente">
                                    </asp:RequiredFieldValidator>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="4" align="center">
                    <telerik:RadButton ID="btnCancelar" Text="Cancelar" runat="server" Width="130px"
                        CausesValidation="false" OnClick="btnCancelar_Click">
                        <Icon PrimaryIconUrl="Images/16/cancelar_16.png" />
                    </telerik:RadButton>
                    <telerik:RadButton ID="btnGuardar" Text="Guardar" runat="server" Width="130px"
                        OnClick="btnGuardar_Click">
                        <Icon PrimaryIconUrl="Images/16/Ribbon_Save_16x16.png" />
                    </telerik:RadButton>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
