﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using GrupoLis.Login;
using Telerik.Web.UI;

public partial class MantenimientoUsuarios : Utilidades.PaginaBase
{
    private GrupoLis.Login.Login logAppCajaChica;
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Mantenimiento Solicitud";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgGastos.FilterMenu);
        SetGridFilterMenu(rgProveedores.FilterMenu);
        rgGastos.Skin = Skin;
        rgProveedores.Skin = Skin;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Mantenimiento Proveedores", "Gastos/Proveedores");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
            LoadPaises();

        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void SetVisible(TableCellCollection cells, String commandName, bool visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is ImageButton)
                {
                    ImageButton btn = (ImageButton)control;

                    if (btn.CommandName == commandName)
                        btn.Visible = visible;
                }
            }
        }
    }

    #region Gastos
    private void loadGridUsuarios()
    {
        try
        {
            conectar();
            GatosTracking GT = new GatosTracking(logApp);
            if (User.IsInRole("Administradores"))
            {
                GT.LlenarGatosXAdministrador();
                rgGastos.DataSource = GT.TABLA;
                rgGastos.DataBind();
            }
        }
        catch { }
        finally
        {
            desconectar();
        }
    }
    protected Object GetPaises()
    {
        try
        {
            conectar();
            CodigosBO cod = new CodigosBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (User.IsInRole("Administradores") & u.CODPAIS == "H")
                cod.loadPaisesHojaRuta();
            else
                cod.loadAllCampos("PAISES", u.CODPAIS);
            return cod.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }
        return null;
    }
    protected void rgUsuarios_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        loadGridUsuarios();
    }
    protected bool GetDatosEditados(GatosTracking GT, GridItem item, string IdGasto)
    {
        bool resp = false;
        GridEditableItem editedItem = item as GridEditableItem;
        RadTextBox Gasto = (RadTextBox)editedItem.FindControl("edGasto");
        RadNumericTextBox Material = (RadNumericTextBox)editedItem.FindControl("edMaterial");
        RadNumericTextBox Balance = (RadNumericTextBox)editedItem.FindControl("edBalance");
        RadComboBox pais = (RadComboBox)editedItem.FindControl("edPais");
        RadComboBox Impuesto = (RadComboBox)editedItem.FindControl("edImpuesto");
        GT.LoadGatos(IdGasto);
        if (GT.totalRegistros == 1)
        {
            GT.GASTO = Gasto.Text.Trim();
            GT.CUENTA = Material.Text.Trim();
            GT.BALANCE = Balance.Text.Trim();
            GT.CODPAIS = pais.SelectedValue.Trim();
            GT.IMPUESTO = Impuesto.SelectedValue.Trim();
            resp = true;
        }
        else
        {
            resp = false;
        }
        return resp;
    }
    protected void rgUsuarios_InsertCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            GridEditableItem editedItem = e.Item as GridEditableItem;
            RadTextBox Gasto = (RadTextBox)editedItem.FindControl("edGasto");
            RadNumericTextBox Material = (RadNumericTextBox)editedItem.FindControl("edMaterial");
            RadNumericTextBox Balance = (RadNumericTextBox)editedItem.FindControl("edBalance");
            RadComboBox pais = (RadComboBox)editedItem.FindControl("edPais");
            RadComboBox Impuesto = (RadComboBox)editedItem.FindControl("edImpuesto");
            GatosTracking GT = new GatosTracking(logApp);
            GT.VerificarXGuardar(pais.SelectedValue, Material.Text, Balance.Text, Gasto.Text);
            if (GT.totalRegistros == 0)
            {
                GT.newLine();
                GT.GASTO = Gasto.Text.Trim();
                GT.CUENTA = Material.Text.Trim();
                GT.BALANCE = Balance.Text.Trim();
                GT.CODPAIS = pais.SelectedValue.Trim();
                GT.ESTADO = "0";
                GT.IMPUESTO = Impuesto.SelectedValue.Trim();
                GT.BALANCE2 = Balance.Text.Trim();
                GT.commitLine();
                GT.actualizar();
                rgGastos.Rebind();
            }
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoUsuarios");
        }
        finally
        {
            desconectar();
        }
    }
    protected void rgUsuarios_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();
            GridEditableItem editedItem = e.Item as GridEditableItem;
            String IdGasto = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdGasto"]);
            String Gasto = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Gasto"]);
            GatosTracking GT = new GatosTracking(logApp);
            bool resp = GetDatosEditados(GT, e.Item, IdGasto);
            if (resp)
            {
                GT.actualizar();
                registrarMensaje("Gasto modificado exitósamente");
                llenarBitacora("Se modificó el Gasto " + Gasto + " por " + GT.GASTO.ToString(), Session["IdUsuario"].ToString(), "");
                rgProveedores.Rebind();
            }
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoUsuarios");
        }
        finally
        {
            desconectar();
        }
    }
    protected void rgUsuarios_DeleteCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridDataItem editedItem = e.Item as GridDataItem;

            String IdGasto = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdGasto"]);

            GatosTracking GT = new GatosTracking(logApp);
            GT.LoadTodosGatos(IdGasto);
            if (GT.ESTADO == "0")
            {
                GT.ESTADO = "1";
                registrarMensaje("Gasto eliminado exitósamente");
            }
            else if (GT.ESTADO == "1")
            {
                GT.ESTADO = "0";
                registrarMensaje("Gasto Habilitado exitósamente");
            }
            else
            {
                registrarMensaje1("Favor notifique al administrador");
            }
            GT.actualizar();

            llenarBitacora("Se eliminó el Gasto " + IdGasto, Session["IdUsuario"].ToString(), "");
            rgProveedores.Rebind();
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoUsuarios");
        }
        finally
        {
            desconectar();
        }

    }
    public RadComboBox edPais;
    protected void rgUsuarios_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if ((e.Item.ItemType == GridItemType.Item) || (e.Item.ItemType == GridItemType.AlternatingItem) || (e.Item.ItemType == GridItemType.SelectedItem))
        {
            SetVisible(e.Item.Cells, "Edit", Modificar);
            SetVisible(e.Item.Cells, "Delete", Anular);
        }
    }
    protected void rgUsuarios_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridEditFormItem && e.Item.IsInEditMode)
        {
            try
            {
                conectar();
                GridEditFormItem editForm = e.Item as GridEditFormItem;
            }
            catch { }

        }
    }
    protected void rgUsuarios_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            UsuariosBO u = new UsuariosBO(logApp);
            GridDataItem editedItem = e.Item as GridDataItem;
            if (e.CommandName == "Reset")
            {
                String usuario = e.Item.Cells[4].Text;
                u.loadUsuarioXUsuario(usuario.Trim());
                u.PASSWORD = Utilidades.PaginaBase.hashMD5(String.Format("{0}1234$", usuario));
                u.VENCIMIENTO = DateTime.Now.AddDays(-1.0);
                u.actualizar();
                registrarMensaje("El password del usuario " + usuario + " fue reseteado a: " + usuario + "1234$");
            }
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "Instrucciones");
        }
        finally
        {
            desconectar();
        }
    }
    #endregion

    #region Proveedores

    private void loadGridProveedor()
    {
        try
        {
            conectar();
            ProveedoresSolicitudBO PS = new ProveedoresSolicitudBO(logApp);
            PS.loadProveedoresTodos();
            rgProveedores.DataSource = PS.TABLA;
            rgProveedores.DataBind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }
    protected void rgRoles_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        loadGridProveedor();
    }
    protected void GetDatosEditadosRoles(ProveedoresSolicitudBO PS, GridItem item)
    {
        GridEditableItem editedItem = item as GridEditableItem;

        RadTextBox proveedor = (RadTextBox)editedItem.FindControl("edProveedor");
        RadNumericTextBox Cuenta = (RadNumericTextBox)editedItem.FindControl("edCuenta");
        RadComboBox Pais = (RadComboBox)editedItem.FindControl("cmbPais");

        PS.NOMBRE = proveedor.Text.Trim();
        PS.CUENTA = Cuenta.Text.Trim();
        PS.CODPAIS = Pais.SelectedValue;
        PS.ESTADO = "0";
    }
    protected void rgRoles_InsertCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            GridEditableItem editedItem = e.Item as GridEditableItem;
            ProveedoresSolicitudBO PS = new ProveedoresSolicitudBO(logApp);
            RadNumericTextBox Cuenta = (RadNumericTextBox)editedItem.FindControl("edCuenta");
            RadComboBox Pais = (RadComboBox)editedItem.FindControl("cmbPais");
            PS.VerificarProveedor(Cuenta.Text, Pais.SelectedValue);
            if (PS.totalRegistros == 0)
            {
                PS.newLine();

                GetDatosEditadosRoles(PS, e.Item);
                PS.commitLine();
                PS.actualizar();
                registrarMensaje("Proveedor creado exitosamente");
            }
            else
            {
                registrarMensaje("La cuenta ya se utiliza para el proveedor " + PS.NOMBRE + " ,Favor revise");
            }
            llenarBitacora("Se ingresó el Proveedor " + PS.NOMBRE, Session["IdUsuario"].ToString(), "");
            rgProveedores.Rebind();
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoUsuarios");
        }
        finally
        {
            desconectar();
        }
    }
    protected void rgRoles_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridEditableItem editedItem = e.Item as GridEditableItem;
            ProveedoresSolicitudBO PS = new ProveedoresSolicitudBO(logApp);
            ProveedoresSolicitudBO PS1 = new ProveedoresSolicitudBO(logApp);
            String IdProveedor = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdProveedor"]);
            RadNumericTextBox Cuenta = (RadNumericTextBox)editedItem.FindControl("edCuenta");
            PS1.VerificarProveedorXcuenta(Cuenta.Text);
            if (PS1.totalRegistros == 0)
            {
                PS.VerificarProveedorXId(IdProveedor);
                GetDatosEditadosRoles(PS, e.Item);
                PS.actualizar();
                registrarMensaje("Proveedor modificado exitósamente");

                llenarBitacora("Se modificó el Proveedor " + PS.NOMBRE, Session["IdUsuario"].ToString(), "");
                rgProveedores.Rebind();
            }
            else
            {
                registrarMensaje("Esta cuenta ya esta creadad para el proveedor " + PS1.NOMBRE + " Favor verifique");
            }
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoUsuarios");
        }
        finally
        {
            desconectar();
        }
    }
    protected void rgRoles_DeleteCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();

            GridDataItem editedItem = e.Item as GridDataItem;

            ProveedoresSolicitudBO PS = new ProveedoresSolicitudBO(logApp);
            String IdProveedor = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdProveedor"]);

            PS.VerificarProveedorXId(IdProveedor);
            PS.ESTADO = "1";
            PS.actualizar();

            registrarMensaje("Proveedor eliminado exitósamente");

            llenarBitacora("Se eliminó el Proveedor" + PS.NOMBRE, Session["IdUsuario"].ToString(), "");
            rgProveedores.Rebind();
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "MantenimientoUsuarios");
        }
        finally
        {
            desconectar();
        }
    }
    protected void rgRoles_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if ((e.Item.ItemType == GridItemType.Item) || (e.Item.ItemType == GridItemType.AlternatingItem) || (e.Item.ItemType == GridItemType.SelectedItem))
        {
            SetVisible(e.Item.Cells, "Edit", Modificar);
            SetVisible(e.Item.Cells, "Delete", Anular);
        }
    }
    #endregion


    private void LoadPaises()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (u.CODPAIS == "H")
                c.loadPaisesHojaRuta();
            else
                c.loadAllCampos("PAISES", u.CODPAIS);
            cmbPaises.DataSource = c.TABLA;
            cmbPaises.DataBind();
            cmbPaises.Items.Insert(0, new RadComboBoxItem("Seleccione un país...", "0"));

        }
        catch { }
        finally { desconectar(); }
    }
    protected void cmbPaises_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            conectarCajaChica(cmbPaises.SelectedValue);
            conectar();
            Descripcion_PagosBO DP = new Descripcion_PagosBO(logAppCajaChica);
            GatosTracking GT = new GatosTracking(logApp);
            DP.LoadGastosDisponibles();
            GT.LlenarGatosMapeo(cmbPaises.SelectedValue);
            rgSolicitud.DataSource = GT.TABLA;
            rgCajaChica.DataSource = DP.TABLA;
            rgSolicitud.DataBind();
            rgCajaChica.DataBind();

        }
        catch (Exception)
        {
        }
        finally { desconectar(); desconectarCajaChica(); }
    }

    #region Conexion
    private void conectarCajaChica(string Pais)
    {
        if (logAppCajaChica == null)
            logAppCajaChica = new GrupoLis.Login.Login();
        if (!logAppCajaChica.conectado)
        {
            logAppCajaChica.tipoConexion = TipoConexion.SQL_SERVER;
            logAppCajaChica.SERVIDOR = mParamethers.Get("Servidor2");
            if (Pais == "H")
            {
                logAppCajaChica.DATABASE = "ControlAduanasVCustoms";
            }
            else if (Pais == "G")
            {
                logAppCajaChica.DATABASE = "ControlAduanasGT";
            }
            else if (Pais == "S")
            {
                logAppCajaChica.DATABASE = "ControlAduanasSV";
            }
            else
            {
                logAppCajaChica.DATABASE = "ControlAduanasNI";
            }
            logAppCajaChica.USER = mParamethers.Get("User2");
            logAppCajaChica.PASSWD = mParamethers.Get("Password2");
            logAppCajaChica.conectar();
        }
    }
    private void desconectarCajaChica()
    {
        if (logAppCajaChica.conectado)
            logAppCajaChica.desconectar();
    }

    #endregion
    protected void rgSolicitud_SelectedIndexChanged(object sender, EventArgs e)
    {
        var dataItem = rgSolicitud.SelectedItems[0] as GridDataItem;
        if (dataItem != null)
        {
            var Gasto = dataItem["Gasto"].Text;
            txtGastoAduanas.Text = String.Format("{0}", Gasto);
            var IdGasto = dataItem["IdGasto"].Text;
            txtIdGastoAduana.Text = String.Format("{0}", IdGasto);


        }
    }
    protected void rgCajaChica_SelectedIndexChanged(object sender, EventArgs e)
    {
        var dataItem = rgCajaChica.SelectedItems[0] as GridDataItem;
        if (dataItem != null)
        {
            var Gasto = dataItem["Gasto"].Text;
            txtGastoCajaChica.Text = String.Format("{0}", Gasto);
            var IdGasto = dataItem["Id"].Text;
            txtIdGastoCajaChica.Text = String.Format("{0}", IdGasto);

        }
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            conectarCajaChica(cmbPaises.SelectedValue);
            MapeoCodGastosBO MC = new MapeoCodGastosBO(logAppCajaChica);
            MC.BuscarGastos(txtIdGastoAduana.Text, txtIdGastoCajaChica.Text);
            if (MC.totalRegistros == 0)
            {
                MC.newLine();
                MC.CodCajaChica = int.Parse(txtIdGastoCajaChica.Text);
                MC.CodEsquema = int.Parse(txtIdGastoAduana.Text);
                MC.commitLine();
                MC.actualizar();
                registrarMensaje("Gasto Relacionado Exitosamente");
            }
            else if (MC.totalRegistros == 1)
            {
                registrarMensaje("El gasto ya esta mapeado");
            }
        }
        catch (Exception)
        {
        }
        finally { desconectarCajaChica(); }
    }
}