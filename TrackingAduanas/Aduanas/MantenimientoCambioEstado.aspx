﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="MantenimientoCambioEstado.aspx.cs" Inherits="MantenimientoCodigos" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="Server">
    


    <table>
        <tr>
            <td>
                <asp:Label ID="Label1" runat="server" Text="Hoja de Ruta: "></asp:Label>            
            </td>
            <td>
                <asp:TextBox ID="txtHojaRuta" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="btnBuscar" runat="server" Text="Buscar Flujo" OnClick="btnBuscar_OnClick" Width="108px" />
            </td>
            <td>
                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" OnClick="btnCancelar_OnClick" Visible="False" Width="108px"/>
                </td>
        </tr>
        
        <tr>
            <td>
                <asp:Label runat="server" ID="lblEstado" Text="Estado" Visible="False"></asp:Label>
            </td>
            <td>
                <telerik:RadComboBox ID="rcbEstado" runat="server" EmptyMessage="Seleccione un estado..." Visible="False"></telerik:RadComboBox>
            </td>
            <td>
                <asp:Button ID="btnCrear" runat="server" Text="  Crear Flujo" OnClick="btnCrear_OnClick" Visible="False" Width="108px"/>
            </td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <asp:Button runat="server" ID="btnUpdate" Visible="False" Text="Actualizar" OnClick="btnUpdate_OnClick" style="width: 100%"/>
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>

    
    
    
   
  
    <br />

    
    
    
</asp:Content>


