﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;

using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;
using System.Net;

public partial class Instrucciones : Utilidades.PaginaBase
{
    private GrupoLis.Login.Login logAppAduanas;

    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Instrucciones";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgInstrucciones.FilterMenu);
        rgInstrucciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgInstrucciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Instrucciones", "Instrucciones");
            if (Anular || Ingresar || Modificar)
                redirectTo("Default.aspx");
            else
            {
                string estado = "", opcion = "";
                try
                {
                    edInstruccion.Value = Request.QueryString.Get("IdInstruccion").ToString();
                    estado = Request.QueryString.Get("Estado").ToString();
                    opcion = Request.QueryString.Get("Opcion").ToString();
                }
                catch
                { }

                if (!String.IsNullOrEmpty(edInstruccion.Value.Trim()))
                {
                    if (edInstruccion.Value.Trim() == "VER")
                        mpInstrucciones.SelectedIndex = 0;
                    else if (edInstruccion.Value.Trim() == "CREAR")
                    {
                        mpInstrucciones.SelectedIndex = 1;
                        cargarDatosInicio();
                    }
                    else if (!String.IsNullOrEmpty(edInstruccion.Value.Trim()) & (estado == "1" || estado == "2") & opcion == "modificar")
                    {
                        //mpInstrucciones.SelectedIndex = 1;
                        cargarDatosInicio();
                        llenarDatosInstruccion(estado);
                        btnGuardar.CommandName = "Modificar";
                        btnBack.CommandName = "Crear";
                        cmbPaisHojaRuta.Enabled = false;
                        cmbAduanas.Enabled = false;
                        //habilitarControles(true);
                        mpInstrucciones.SelectedIndex = 1;
                    }
                    else if (!String.IsNullOrEmpty(edInstruccion.Value.Trim()) & estado == "1" & opcion == "duplicar")
                    {
                        try
                        {
                            conectar();
                            InstruccionesBO i = new InstruccionesBO(logApp);
                            i.loadInstruccionGD(edInstruccion.Value.Trim());
                            if (i.totalRegistros > 0)
                            {
                                edDuplicar.Value = "true";
                                cargarDatosInicio();
                                llenarDatosInstruccionDuplicar("1");
                                btnGuardar.CommandName = "Duplicar";
                                btnBack.CommandName = "Crear";
                                //habilitarControles(false);
                                mpInstrucciones.SelectedIndex = 1;
                                txtIntruccion.Text = "";
                                cmbPaisHojaRuta.Enabled = true;
                                cmbAduanas.Enabled = true;
                            }
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                    }
                }
            }
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    #region Instrucciones
    protected void rgInstrucciones_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridInstrucciones();
    }
    private void llenarGridInstrucciones()
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            if (User.IsInRole("Administradores"))
                i.loadInstruccionesClienteInstr();
            else if (User.IsInRole("Administrador Pais") || User.IsInRole("Jefe Centralización"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                i.loadInstruccionesXPaisIns(u.CODPAIS);
            }
            else if (User.IsInRole("Operaciones") || User.IsInRole("Inplant") || User.IsInRole("Oficial de Cuenta"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.totalRegistros > 0)
                {
                    string idU = Session["IdUsuario"].ToString();
                    if (User.Identity.Name == "davila" || User.Identity.Name == "spolanco"
                        || User.Identity.Name == "jgarcia" || User.Identity.Name == "ncontreras" ||
                        User.Identity.Name == "afonseca" || User.Identity.Name == "cmorales")
                        i.loadInstruccionesXPaisUsuario(u.CODPAIS, idU);
                    else
                        i.loadInstruccionesXPaisIns(u.CODPAIS);
                }
            }
            else if (User.IsInRole("Jefes Aduana") || User.IsInRole("Aforadores") || User.IsInRole("Analista de Documentos"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.totalRegistros > 0)
                    i.loadInstruccionXAduana(u.CODIGOADUANA);
            }
            else if (User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT"))
            {
                string idU = Session["IdUsuario"].ToString();
                u.loadUsuarioLogin(User.Identity.Name);
                if (u.totalRegistros > 0)
                    if (u.CODPAIS == "H")
                        if (User.Identity.Name == "eizaguirre")
                        { i.loadInstruccionXAduanaUsuario("016", idU); }
                        else
                        {
                            i.loadInstruccionXAduanaUsuario(u.CODIGOADUANA, idU);
                        }
                    else
                        //i.loadInstruccionXUsuario(idU);
                        i.loadInstruccionesXPais(u.CODPAIS);
            }
            rgInstrucciones.DataSource = i.TABLA;
        }
        catch { }

    }
    protected void rgInstrucciones_Init(object sender, System.EventArgs e)
    {
        GridFilterMenu menu = rgInstrucciones.FilterMenu;
        menu.Items.RemoveAt(rgInstrucciones.FilterMenu.Items.Count - 2);
    }
    protected void btnBack_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (btnBack.CommandName == "Modificar")
                mpInstrucciones.SelectedIndex = 0;
            else
                Response.Redirect("CrearInstrucciones.aspx");
            btnBack.CommandName = "";
            rgInstrucciones.Rebind();
        }
        catch (Exception)
        { }
    }
    private void limpiarComboBox(RadComboBox ComboBox)
    {
        try
        {
            int tamano = ComboBox.Items.Count;
            for (int i = 0; i < tamano; i++)
            {
                ComboBox.Items.Remove(0);
            }
        }
        catch (Exception)
        { }
    }
    protected void rgInstrucciones_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            GridEditableItem editedItem = e.Item as GridEditableItem;

            if (e.CommandName == "Editar")
            {
                edInstruccion.Value = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdInstruccion"]);
                i.loadInstruccion(edInstruccion.Value.Trim());
                if (i.totalRegistros > 0)
                {
                    if (i.IDESTADOFLUJO == "0")
                    {
                        edDuplicar.Value = "false";
                        cargarDatosInicio();
                        llenarDatosInstruccion("0");
                        btnGuardar.CommandName = "Modificar";
                        btnBack.CommandName = "Modificar";
                        cmbPaisHojaRuta.Enabled = false;
                        cmbAduanas.Enabled = false;
                        //habilitarControles(true);
                        mpInstrucciones.SelectedIndex = 1;
                    }
                    else
                        registrarMensaje("Instrucción no se puede modificar, el flujo ya se comenzó");
                }
                else
                    registrarMensaje("Instrucción no existe, consulte con el administrador del sistema");
            }
            else if (e.CommandName == "Duplicar")
            {
                edInstruccion.Value = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdInstruccion"]);
                i.loadInstruccion(edInstruccion.Value.Trim());
                if (i.totalRegistros > 0)
                {
                    //i.loadInstruccionXTramite(i.IDTRAMITE);
                    //if (i.totalRegistros < 2)
                    //{
                    edDuplicar.Value = "true";
                    cargarDatosInicio();
                    llenarDatosInstruccion("0");
                    btnGuardar.CommandName = "Duplicar";
                    btnBack.CommandName = "Modificar";
                    //habilitarControles(false);
                    mpInstrucciones.SelectedIndex = 1;
                    txtIntruccion.Text = "";
                    cmbPaisHojaRuta.Enabled = true;
                    cmbAduanas.Enabled = true;
                    //}
                    //else
                    //    registrarMensaje("No se puede duplicar instrucción, el trámite " + edTramite.Value.Trim() + " ya contiene " + i.totalRegistros + " instrucciones");
                }
            }
            else if (e.CommandName == "Delete")
            {
                edInstruccion.Value = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdInstruccion"]);
                i.loadInstruccion(edInstruccion.Value.Trim());
                if (i.totalRegistros > 0)
                {
                    i.CODESTADO = "100";     //cambia estado a eliminado
                    i.actualizar();
                    registrarMensaje("Instrucción " + edInstruccion.Value + " eliminada esitosamente");
                    llenarBitacora("Instrucción Eliminada", Session["IdUsuario"].ToString(), edInstruccion.Value);
                    rgInstrucciones.Rebind();
                    CorreosBO CO = new CorreosBO(logApp);
                    UsuariosBO U = new UsuariosBO(logApp);
                    U.loadUsuario(Session["IdUsuario"].ToString());
                    CO.LoadCorreosXPais(edInstruccion.Value.Substring(0, 1), "Aduanas");
                    sendMailConParametrosSinArchivo(CO.CORREOS, "Se Elimino la Hoja de Ruta " + edInstruccion.Value, "La Hoja de Ruta " + edInstruccion.Value + " Fue Eliminada por " + U.NOMBRE + " " + U.APELLIDO, CO.COPIAS, "Tracking Aduanas", "trackingaduanas", "nMgtdA$7PaRjQphEYZBD");
                }
                else
                    registrarMensaje("Instrucción no existe");
            }
            else if (rgInstrucciones.Items.Count <= 0 & e.CommandName == RadGrid.ExportToExcelCommandName)
                e.Canceled = true;
            else if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                ConfigureExport();
            }
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "Instrucciones");
        }
        finally
        {
            desconectar();
        }
    }
    protected void btnComenzar_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton btn = (ImageButton)sender;
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            FlujoCargaBO fc = new FlujoCargaBO(logApp);
            DocumentosRegimenBO dr = new DocumentosRegimenBO(logApp);
            DocumentosInstruccionesBO di = new DocumentosInstruccionesBO(logApp);
            GestionCargaBO gc = new GestionCargaBO(logApp);
            string idInstruccion = btn.CommandArgument;
            i.loadInstruccion(idInstruccion);
            if (i.totalRegistros > 0)
            {
                //verificar que se hallan ingresado los documentos mínimos
                dr.loadDocumentosXRegimenPais(i.CODREGIMEN, i.CODPAISHOJARUTA);
                di.loadDocumentosMinimosXInstruccion(idInstruccion.Trim());
                if ((dr.totalRegistros + 1) == di.totalRegistros)
                {
                    fc.loadEstadoFlujoCargaXPaisRegimen(i.CODPAISHOJARUTA, i.CODREGIMEN);
                    if (fc.totalRegistros > 0)
                    {
                        i.IDESTADOFLUJO = fc.IDESTADOFLUJO;  //cambia estado al primer estado del flujo de la carga por país
                        btn.CommandName = "Nada";
                        btn.Visible = false;
                        btn.ImageUrl = "~/Images/gris.png";
                        i.actualizar();

                        try
                        {
                            gc.loadGestionCargaXInstruccionEvento(idInstruccion, "0");
                            if (gc.totalRegistros <= 0)
                            {
                                gc.newLine();
                                gc.IDINSTRUCCION = idInstruccion;
                                gc.IDESTADOFLUJO = "0";
                                gc.ESTADO = "0";
                                gc.FECHA = DateTime.Now.ToString();
                                gc.FECHAINGRESO = DateTime.Now.ToString();
                                gc.OBSERVACION = "Se agregó de comienzo flujo";
                                gc.ELIMINADO = "0";
                                gc.IDUSUARIO = Session["IdUsuario"].ToString();
                                gc.commitLine();
                                gc.actualizar();
                            }
                        }
                        catch { }

                        registrarMensaje("Flujo de la instrucción comenzado exitosamente");
                        llenarBitacora("Flujo de la instrucción " + idInstruccion + " comenzado exitosamente", Session["IdUsuario"].ToString(), idInstruccion);
                        rgInstrucciones.Rebind();
                    }
                    else
                        registrarMensaje("No existe flujo de carga para el régimen de esta instrucción");
                }
                else
                    registrarMensaje("Todavía no ha ingresado todos los documentos mínimos");
            }
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "Instrucciones");
        }
        finally
        {
            desconectar();
        }
    }
    private void ConfigureExport()
    {
        rgInstrucciones.Columns[0].Visible = false;
        rgInstrucciones.Columns[1].Visible = false;
        rgInstrucciones.Columns[2].Visible = false;
        rgInstrucciones.Columns[3].Visible = false;
        String filename = "Instrucciones" + "_" + DateTime.Now.ToShortDateString();
        rgInstrucciones.GridLines = GridLines.Both;
        rgInstrucciones.ExportSettings.FileName = filename;
        rgInstrucciones.ExportSettings.IgnorePaging = true;
        rgInstrucciones.ExportSettings.OpenInNewWindow = false;
        rgInstrucciones.ExportSettings.ExportOnlyData = true;
    }
    #endregion

    #region Crear Instruccion
    private void cargarDatosInicio()
    {
        try
        {
            conectar();
            ClientesBO c = new ClientesBO(logApp);
            CodigosBO co = new CodigosBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            if (User.IsInRole("Administradores") || edDuplicar.Value == "true" || User.IsInRole("Inplant"))
                co.loadPaisesHojaRuta();
            else if (User.IsInRole("Jefes Aduana") || User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT") || User.IsInRole("Operaciones") || User.IsInRole("Inplant") 
                        || User.IsInRole("Oficial de Cuenta") || User.IsInRole("Aforadores") || User.IsInRole("Analista de Documentos") || User.IsInRole("Jefe Centralización"))
            {
                u.loadUsuario(Session["IdUsuario"].ToString());
                co.loadAllCampos("PAISES", u.CODPAIS);
            }
            cmbPaisHojaRuta.DataSource = co.TABLA;
            cmbPaisHojaRuta.DataBind();
            llenarClientes();
            llenarOficialCuentaYRTN();
            co.loadAllCampos("DOCUMENTOEMBARQUE");
            cmbDocumentoEmbarque.DataSource = co.TABLA;
            cmbDocumentoEmbarque.DataBind();
            co.loadAllCampos("PAISES");
            cmbPaisProcedencia.DataSource = co.TABLA;
            cmbPaisProcedencia.DataBind();
            cmbPaisOrigen.DataSource = co.TABLA;
            cmbPaisOrigen.DataBind();
            cmbPaisDestino.DataSource = co.TABLA;
            cmbPaisDestino.DataBind();
            ///////////////
            cmbPaisProcedencia.Items.Insert(0, new RadComboBoxItem("Seleccione un país...", "0"));
            cmbPaisOrigen.Items.Insert(0, new RadComboBoxItem("Seleccione un país...", "0"));
            cmbPaisDestino.Items.Insert(0, new RadComboBoxItem("Seleccione un país...", "0"));
            ///////////////
            co.loadAllCampos("TIPOTRANSPORTE");
            cmbTipoTransporte.DataSource = co.TABLA;
            cmbTipoTransporte.DataBind();
            co.loadAllCampos("TIPOCARGAMENTO");
            cmbTipoCargamento.DataSource = co.TABLA;
            cmbTipoCargamento.DataBind();
            co.loadAllCampos("TIPOCARGA");
            cmbTipoCarga.DataSource = co.TABLA;
            cmbTipoCarga.DataBind();
            ///////////////
            //cmbPaisProcedencia.Items.Insert(0, new RadComboBoxItem("Seleccione un país...", "0"));
            cmbTipoCargamento.Items.Insert(0, new RadComboBoxItem("Seleccione tipo de carga...", "0"));
            cmbTipoCarga.Items.Insert(0, new RadComboBoxItem("Seleccione tipo de embalaje...", "0"));
            ///////////////
            llenarRegimenesYAduanas();
            llenarUsuariosAduana();
            llenarIncoterm();
            DateTime fecha = Convert.ToDateTime("01-01-1900 00:00:00");
            dtpETA.SelectedDate = DateTime.Now;
            dtpFechaVencTiempoLibreNaviera.SelectedDate = fecha;
            dtpFechaVencTiempoLibrePuerto.SelectedDate = fecha;
            dtpTiempoEstimadoEntrega.SelectedDate = fecha;
            dtpFechaRecepcion.SelectedDate = DateTime.Now;
        }
        catch { }
    }
    private void llenarClientes()
    {
        try
        {
            conectar();
            ClientesBO c = new ClientesBO(logApp);
            c.loadAllCamposClientesXPais(cmbPaisHojaRuta.SelectedValue);
            cmbClientes.DataSource = c.TABLA;
            cmbClientes.DataBind();
        }
        catch { }
    }
    private void llenarDivision()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            c.loadDivision(cmbClientes.SelectedValue);
            if (c.totalRegistros > 0)
            {
                cmbDivision.Enabled = true;
                cmbProductoCargill.Enabled = true;
            }
            else
            {
                cmbDivision.Enabled = false;
                cmbProductoCargill.Enabled = false;
            }
            cmbDivision.DataSource = c.TABLA;
            cmbDivision.DataBind();
            if (cmbClientes.Text.ToString() == "CORPORACION DINANT S.A." || cmbClientes.Text.ToString() == "EXPORTADORA DEL ATLANTICO")
            {
                llenarProductoCargill("DINANT");
            }
            else
            {
                llenarProductoCargill("OTROS");
            }
        }
        catch { }
    }
    private void llenarDivision(string X)
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            c.loadDivision(cmbClientes.SelectedValue);
            if (c.totalRegistros > 0)
            {
                cmbDivision.Enabled = true;
                cmbProductoCargill.Enabled = true;
            }
            else
            {
                cmbDivision.Enabled = false;
                cmbProductoCargill.Enabled = false;
            }
            cmbDivision.DataSource = c.TABLA;
            cmbDivision.DataBind();
            if (cmbClientes.Text.ToString() == "CORPORACION DINANT S.A." || cmbClientes.Text.ToString() == "EXPORTADORA DEL ATLANTICO")
            {
                llenarProductoCargill("DINANT");
            }
            else if (X == "2")
            {
                llenarProductoCargill("ALCON");
            }
            else
            {
                llenarProductoCargill("OTROS");
            }
        }
        catch { }
    }
    private void llenarIncoterm()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            c.loadDivision("INCOTERM");
            cmbIncoterm.DataSource = c.TABLA;
            cmbIncoterm.DataBind();

        }
        catch { }
    }
    private void llenarOficialCuentaYRTN()
    {
        try
        {
            conectar();
            ClientesBO c = new ClientesBO(logApp);
            c.loadCliente(cmbClientes.SelectedValue);
            if (c.totalRegistros > 0)
            {
                txtOficialCuenta.Text = c.OFICIALCUENTA;
                txtRTN.Text = c.RTN;
            }
            else
            {
                txtOficialCuenta.Text = "";
                txtRTN.Text = "";
            }
        }
        catch { }
    }
    private void llenarRegimenesYAduanas()
    {
        try
        {
            conectar();
            AduanasBO a = new AduanasBO(logApp);
            CodigosBO co = new CodigosBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (User.IsInRole("Administradores") || User.IsInRole("Operaciones") || User.IsInRole("Inplant") || edDuplicar.Value == "true" || u.CODPAIS != "H" || User.IsInRole("Oficial de Cuenta") || User.IsInRole("Administrador Pais") || User.IsInRole("Jefe Centralización"))
            {
                if (cmbPaisHojaRuta.SelectedValue == "H")
                {
                    a.loadAduanasParaInstrucciones(cmbPaisHojaRuta.SelectedValue);
                }
                else
                {
                    a.loadPaisAduanas(cmbPaisHojaRuta.SelectedValue);
                }
            }
            else if (User.IsInRole("Jefes Aduana") || User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT") || User.IsInRole("Aforadores") || User.IsInRole("Analista de Documentos"))
                if (User.Identity.Name == "eizaguirre")
                { a.loadAduanas("016"); }
                else
                {
                    a.loadAduanas(u.CODIGOADUANA);
                }
            cmbAduanas.DataSource = a.TABLA;
            cmbAduanas.DataBind();
            //if (cmbPaisHojaRuta.SelectedValue == "H")
            //    try
            //    {
            //        cmbAduanas.Items.Remove(14);
            //        cmbAduanas.Items.Remove(14);
            //        cmbAduanas.Items.Remove(14);
            //        cmbAduanas.Items.Remove(14);
            //        cmbAduanas.Items.Remove(14);
            //        cmbAduanas.Items.Remove(14);
            //        cmbAduanas.Items.Remove(14);
            //        cmbAduanas.Items.Remove(14);
            //    }
            //    catch { }
            co.loadAllCampos("REGIMEN" + cmbPaisHojaRuta.SelectedItem.Text.Trim());
            cmbRegimen.DataSource = co.TABLA;
            cmbRegimen.DataBind();
        }
        catch { }
    }
    private void llenarUsuariosAduana()
    {
        try
        {
            conectar();
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (User.IsInRole("Inplant"))
            {
                if (cmbPaisHojaRuta.SelectedValue != "H")
                    u.loadUsuariosNombreCompletoXPais(cmbPaisHojaRuta.SelectedValue, "Gestores");
                else
                    u.loadUsuariosNombreCompletoXAduana(cmbAduanas.SelectedValue, "Gestores");
                cmbUsuarioAduana.DataSource = u.TABLA;
                cmbUsuarioAduana.DataBind();
                cmbUsuarioAduana.SelectedValue = Session["IdUsuario"].ToString();
            }
            else
            {
                if (u.CODPAIS != "H" || cmbPaisHojaRuta.SelectedValue != "H")
                    u.loadUsuariosNombreCompletoXPais(cmbPaisHojaRuta.SelectedValue, "Gestores");
                else
                    u.loadUsuariosNombreCompletoXAduana(cmbAduanas.SelectedValue, "Gestores");
                cmbUsuarioAduana.DataSource = u.TABLA;
                cmbUsuarioAduana.DataBind();
                cmbUsuarioAduana.SelectedValue = Session["IdUsuario"].ToString();
            }
        }
        catch { }
    }
    private void llenarDatosInstruccion(string estado)
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            CodigosBO c = new CodigosBO(logApp);
            i.loadInstruccionLlenar(edInstruccion.Value.Trim(), estado);
            if (i.totalRegistros > 0)
            {
                txtTramite.Text = i.IDTRAMITE;
                txtIntruccion.Text = edInstruccion.Value.Trim();
                cmbPaisHojaRuta.SelectedValue = i.CODPAISHOJARUTA;
                llenarRegimenesYAduanas();
                cmbAduanas.SelectedValue = i.CODIGOADUANA;
                llenarUsuariosAduana();
                try { dtpFechaRecepcion.SelectedDate = Convert.ToDateTime(i.FECHARECEPCION); }
                catch { }
                llenarClientes();
                cmbClientes.SelectedValue = i.IDCLIENTE;
                //llenarDivision();
                c.CargarDivision(i.IDCLIENTE, i.DIVISION);
                if (c.totalRegistros > 0)
                {
                    cmbDivision.SelectedValue = c.CODIGO;
                }
                llenarDivision(cmbDivision.SelectedValue);
                c.CargarDivision("INCOTERM", i.INCOTERM);
                if (c.totalRegistros > 0)
                {
                    cmbIncoterm.SelectedValue = c.CODIGO;
                }
                cmbProductoCargill.SelectedValue = i.PRODUCTOSCARGILL;
                txtNumFactura.Text = i.NUMEROFACTURA;
                txtOficialCuenta.Text = i.OFICIALCUENTA;
                txtReferenciaCliente.Text = i.REFERENCIACLIENTE;
                txtProveedores.Text = i.PROVEEDOR;
                //txtNoFactura.Text = i.NOFACTURA;
                txtProducto.Text = i.PRODUCTO;
                //edDocumentoOld.Value = i.IDDOCUMENTOESPECIEFISCAL;
                try { txtValorFOB.Value = i.VALORFOB; }
                catch { }
                try { txtFlete.Value = i.FLETE; }
                catch { }
                try { txtSeguro.Value = i.SEGURO; }
                catch { }
                try { txtOtros.Value = i.OTROS; }
                catch { }
                try { txtValorCIF.Value = i.VALORCIF; }
                catch { }
                cmbDocumentoEmbarque.SelectedValue = i.CODDOCUMENTOEMBARQUE;
                //txtNoDocumentoEmbarque.Text = i.NODOCUMENTOEMBARQUE;
                try { txtCantidadBultos.Text = i.CANTIDADBULTOS.ToString(); }
                catch { }
                try { txtPeso.Value = i.PESOKGS; }
                catch { }
                cmbPaisProcedencia.SelectedValue = i.CODPAISPROCEDENCIA;
                cmbPaisOrigen.SelectedValue = i.CODPAISORIGEN;
                cmbPaisDestino.SelectedValue = i.CODPAISDESTINO;
                txtCiudadDestino.Text = i.CIUDADDESTINO;
                txtContenedores.Text = i.CONTENEDORES;
                txtDescripcionContenedores.Text = i.DESCRIPCIONCONTENEDORES;
                cmbRegimen.SelectedValue = i.CODREGIMEN;
                cmbUsuarioAduana.SelectedValue = i.IDUSUARIOADUANA;
                cmbTipoTransporte.SelectedValue = i.CODTIPOTRANSPORTE;
                cmbTipoCargamento.SelectedValue = i.CODTIPOCARGAMENTO;
                cmbTipoCarga.SelectedValue = i.CODTIPOCARGA;
                dtpFechaVencTiempoLibreNaviera.SelectedDate = Convert.ToDateTime(i.FECHAVENCTIEMPOLIBRENAVIERA);
                dtpFechaVencTiempoLibrePuerto.SelectedDate = Convert.ToDateTime(i.FECHAVENCTIEMPOLIBREPUERTO);
                dtpETA.SelectedDate = Convert.ToDateTime(i.ETA);
                dtpTiempoEstimadoEntrega.SelectedDate = Convert.ToDateTime(i.TIEMPOESTIMADOENTREGA);
                txtInstruccionesEntrega.Text = i.INSTRUCCIONESENTREGA;
                txtObservacion.Text = i.OBSERVACION;
            }
        }
        catch (Exception)
        { }
    }
    private void llenarDatosInstruccionDuplicar(string estado)
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            i.loadInstruccionLlenar(edInstruccion.Value.Trim(), estado);
            if (i.totalRegistros > 0)
            {
                txtTramite.Text = i.IDTRAMITE;
                txtIntruccion.Text = edInstruccion.Value.Trim();
                cmbPaisHojaRuta.SelectedValue = i.CODPAISHOJARUTA;
                llenarRegimenesYAduanas();
                cmbAduanas.SelectedValue = i.CODIGOADUANA;
                llenarUsuariosAduana();
                dtpFechaRecepcion.SelectedDate = Convert.ToDateTime(i.FECHARECEPCION);
                llenarClientes();
                cmbClientes.SelectedValue = i.IDCLIENTE;
                txtOficialCuenta.Text = i.OFICIALCUENTA;
                txtReferenciaCliente.Text = i.REFERENCIACLIENTE;
                txtProveedores.Text = i.PROVEEDOR;
                //txtNoFactura.Text = i.NOFACTURA;
                txtProducto.Text = i.PRODUCTO;
                //edDocumentoOld.Value = i.IDDOCUMENTOESPECIEFISCAL;
                txtValorFOB.Value = i.VALORFOB;
                txtFlete.Value = i.FLETE;
                txtSeguro.Value = i.SEGURO;
                txtOtros.Value = i.OTROS;
                txtValorCIF.Value = i.VALORCIF;
                cmbDocumentoEmbarque.SelectedValue = i.CODDOCUMENTOEMBARQUE;
                //txtNoDocumentoEmbarque.Text = i.NODOCUMENTOEMBARQUE;
                txtCantidadBultos.Text = i.CANTIDADBULTOS.ToString();
                txtPeso.Value = i.PESOKGS;
                cmbPaisProcedencia.SelectedValue = i.CODPAISPROCEDENCIA;
                cmbPaisOrigen.SelectedValue = i.CODPAISORIGEN;
                cmbPaisDestino.SelectedValue = i.CODPAISDESTINO;
                txtCiudadDestino.Text = i.CIUDADDESTINO;
                txtContenedores.Text = i.CONTENEDORES;
                txtDescripcionContenedores.Text = i.DESCRIPCIONCONTENEDORES;
                cmbRegimen.SelectedValue = i.CODREGIMEN;
                cmbUsuarioAduana.SelectedValue = i.IDUSUARIOADUANA;
                cmbTipoTransporte.SelectedValue = i.CODTIPOTRANSPORTE;
                cmbTipoCargamento.SelectedValue = i.CODTIPOCARGAMENTO;
                cmbTipoCarga.SelectedValue = i.CODTIPOCARGA;
                dtpFechaVencTiempoLibreNaviera.SelectedDate = Convert.ToDateTime(i.FECHAVENCTIEMPOLIBRENAVIERA);
                dtpFechaVencTiempoLibrePuerto.SelectedDate = Convert.ToDateTime(i.FECHAVENCTIEMPOLIBREPUERTO);
                dtpETA.SelectedDate = Convert.ToDateTime(i.ETA);
                dtpTiempoEstimadoEntrega.SelectedDate = Convert.ToDateTime(i.TIEMPOESTIMADOENTREGA);
                txtInstruccionesEntrega.Text = i.INSTRUCCIONESENTREGA;
                txtObservacion.Text = i.OBSERVACION;
            }
        }
        catch (Exception)
        { }
    }
    private void habilitarControles(bool habilitar)
    {
        try
        {
            cmbClientes.Enabled = habilitar;
            txtValorFOB.Enabled = habilitar;
            txtFlete.Enabled = habilitar;
            txtSeguro.Enabled = habilitar;
            txtOtros.Enabled = habilitar;
            txtValorCIF.Enabled = habilitar;
            cmbPaisProcedencia.Enabled = habilitar;
            cmbPaisOrigen.Enabled = habilitar;
            cmbPaisDestino.Enabled = habilitar;
            txtContenedores.Enabled = habilitar;
            txtPeso.Enabled = habilitar;
            txtProducto.Enabled = habilitar;
            cmbTipoCargamento.Enabled = habilitar;
            cmbTipoCarga.Enabled = habilitar;
            dtpFechaVencTiempoLibreNaviera.Enabled = habilitar;
            dtpFechaVencTiempoLibrePuerto.Enabled = habilitar;
            dtpETA.Enabled = habilitar;
            dtpTiempoEstimadoEntrega.Enabled = habilitar;
            txtInstruccionesEntrega.Enabled = habilitar;
            txtObservacion.Enabled = habilitar;
        }
        catch { }
    }
    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        #region salvar click
        try
        {
            #region Objetos de Bases de Datos
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            AduanasBO a = new AduanasBO(logApp);
            InstruccionesBO ins = new InstruccionesBO(logApp);
            ClientesBO c = new ClientesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            DocumentosInstruccionesBO di = new DocumentosInstruccionesBO(logApp);
            string idTramite = "", idInstruccion = "";
            bool permitir = false, permitirGA = false;
            #endregion

            #region Validacion de controles
            if (User.Identity.Name == "davila" || User.Identity.Name == "spolanco" || User.IsInRole("Operaciones"))//|| User.IsInRole("Inplant")  || User.Identity.Name == "afonseca" || User.Identity.Name == "jgarcia" || User.Identity.Name == "ncontreras" 
                permitir = true;
            if (cmbRegimen.SelectedValue == "9999")
                permitirGA = true;
            if (!cmbPaisHojaRuta.IsEmpty)
            {
                if (!cmbAduanas.IsEmpty)
                {
                    if (!cmbClientes.IsEmpty)
                    {
                        if (!String.IsNullOrEmpty(txtProveedores.Text.Trim()) || permitir || permitirGA)
                        {
                            if (!String.IsNullOrEmpty(txtProducto.Text.Trim()) || permitir || permitirGA)
                            {
                                if ((!String.IsNullOrEmpty(txtDescripcionContenedores.Text.Trim()) & !txtDescripcionContenedores.Text.Trim().StartsWith(",")) ||
                                    (String.IsNullOrEmpty(txtDescripcionContenedores.Text.Trim()) & txtContenedores.Value == 0))
                                {
                                    if (txtPeso.Value > 0 || permitir || permitirGA)
                                    {
                                        if ((!cmbPaisProcedencia.IsEmpty & cmbPaisProcedencia.SelectedValue != "0") || permitir || permitirGA)
                                        {
                                            if ((!cmbPaisOrigen.IsEmpty & cmbPaisOrigen.SelectedValue != "0") || permitir || permitirGA)
                                            {
                                                if ((!cmbPaisDestino.IsEmpty & cmbPaisDestino.SelectedValue != "0") || permitir || permitirGA)
                                                {
                                                    if ((!cmbTipoCargamento.IsEmpty & cmbTipoCargamento.SelectedValue != "0") || permitir || permitirGA)
                                                    {
                                                        if ((!cmbTipoCarga.IsEmpty & cmbTipoCarga.SelectedValue != "0") || permitir || permitirGA)
                                                        {
                                                            if (txtValorCIF.Value > 0 || permitir || permitirGA)
                                                            {
                                                                if (!String.IsNullOrEmpty(txtNumFactura.Text.Trim()))
                                                                {
                                                                    if (!String.IsNullOrEmpty(txtProveedores.Text.Trim()))
                                                                    {
            #endregion 

                                                                    #region codigo
                                                                    string mensaje = "", mensaje2 = "";

                                                                    #region Modificar
                                                                    if (btnGuardar.CommandName == "Modificar")
                                                                    {
                                                                        //guarda datos en la tabla instrucciones
                                                                        ins.loadInstruccionModificar(edInstruccion.Value.Trim());
                                                                        if (ins.totalRegistros > 0)
                                                                        {
                                                                            ins.FECHARECEPCION = dtpFechaRecepcion.SelectedDate.ToString();
                                                                            ins.IDCLIENTE = cmbClientes.SelectedValue;
                                                                            ins.OFICIALCUENTA = txtOficialCuenta.Text;
                                                                            ins.REFERENCIACLIENTE = txtReferenciaCliente.Text;
                                                                            ins.PROVEEDOR = txtProveedores.Text;
                                                                            ins.VALORFOB = Convert.ToDouble(txtValorFOB.Value);
                                                                            ins.FLETE = Convert.ToDouble(txtFlete.Value);
                                                                            ins.SEGURO = Convert.ToDouble(txtSeguro.Value);
                                                                            ins.OTROS = Convert.ToDouble(txtOtros.Value);
                                                                            ins.VALORCIF = Convert.ToDouble(txtValorCIF.Value);
                                                                            ins.CODDOCUMENTOEMBARQUE = cmbDocumentoEmbarque.SelectedValue;
                                                                            ins.CANTIDADBULTOS = Convert.ToInt32(txtCantidadBultos.Value);
                                                                            ins.PESOKGS = Convert.ToDouble(txtPeso.Value);
                                                                            ins.CONTENEDORES = txtContenedores.Text;
                                                                            ins.DESCRIPCIONCONTENEDORES = txtDescripcionContenedores.Text.Trim();
                                                                            ins.PRODUCTO = txtProducto.Text;
                                                                            ins.CODREGIMEN = cmbRegimen.SelectedValue;
                                                                            ins.CODPAISPROCEDENCIA = cmbPaisProcedencia.SelectedValue;
                                                                            ins.CODPAISORIGEN = cmbPaisOrigen.SelectedValue;
                                                                            ins.CODPAISDESTINO = cmbPaisDestino.SelectedValue;
                                                                            ins.CIUDADDESTINO = txtCiudadDestino.Text;
                                                                            ins.CODTIPOTRANSPORTE = cmbTipoTransporte.SelectedValue;
                                                                            ins.CODTIPOCARGAMENTO = cmbTipoCargamento.SelectedValue;
                                                                            ins.CODTIPOCARGA = cmbTipoCarga.SelectedValue;
                                                                            ins.FECHAVENCTIEMPOLIBRENAVIERA = dtpFechaVencTiempoLibreNaviera.SelectedDate.ToString();
                                                                            ins.FECHAVENCTIEMPOLIBREPUERTO = dtpFechaVencTiempoLibrePuerto.SelectedDate.ToString();
                                                                            ins.ETA = dtpETA.SelectedDate.ToString();
                                                                            ins.TIEMPOESTIMADOENTREGA = dtpTiempoEstimadoEntrega.SelectedDate.ToString();
                                                                            ins.INSTRUCCIONESENTREGA = Server.HtmlDecode(txtInstruccionesEntrega.Text);
                                                                            ins.OBSERVACION = Server.HtmlDecode(txtObservacion.Text);
                                                                            ins.IDUSUARIOADUANA = cmbUsuarioAduana.SelectedValue;
                                                                            ins.IDOFICIALCUENTA = Recuperar_Oficial_Cuenta(cmbClientes.SelectedValue);
                                                                            if (cmbDivision.Enabled == false)
                                                                            {
                                                                                ins.DIVISION = "";
                                                                                ins.PRODUCTOSCARGILL = "";
                                                                            }
                                                                            else
                                                                            {
                                                                                ins.DIVISION = cmbDivision.SelectedValue;
                                                                                ins.PRODUCTOSCARGILL = cmbProductoCargill.SelectedValue;
                                                                            }
                                                                            if (User.IsInRole("Inplant") || permitir)
                                                                                ins.CODESTADO = "1";
                                                                            else
                                                                                ins.CODESTADO = "0";
                                                                            ins.INCOTERM = cmbIncoterm.SelectedValue;
                                                                            ins.NUMEROFACTURA = txtNumFactura.Text;
                                                                            if (cmbClientes.Text.Contains("CARGILL"))
                                                                            {
                                                                                CargaCargill(ins.IDINSTRUCCION);
                                                                            }

                                                                            //if (this.CBDua.Checked)
                                                                            //{
                                                                            //    ProcesoRequiereDua(idInstruccion, cmbRegimen.SelectedValue);
                                                                            //}

                                                                            ins.actualizar();
                                                                            txtTramite.Text = "";
                                                                            txtIntruccion.Text = "";

                                                                            try
                                                                            {
                                                                                di.eliminaDocumentosEmbarques(edInstruccion.Value.Trim(), cmbDocumentoEmbarque.SelectedValue);
                                                                            }
                                                                            catch { }

                                                                            try
                                                                            {
                                                                                conectarAduanas();
                                                                                HojaRutaBO hr = new HojaRutaBO(logAppAduanas);
                                                                                ComplementoRecepcionBO cr = new ComplementoRecepcionBO(logAppAduanas);
                                                                                EncabezadoEsquemaBO ee = new EncabezadoEsquemaBO(logAppAduanas);
                                                                                HojaRutaFlujoPortuarioBO hrfp = new HojaRutaFlujoPortuarioBO(logAppAduanas);
                                                                                hr.loadHojaRuta(edInstruccion.Value.Trim());
                                                                                if (hr.totalRegistros > 0)
                                                                                {
                                                                                    hr.FECHARECEPCION = dtpFechaRecepcion.SelectedDate.ToString();
                                                                                    hr.CODIGOADUANA = cmbAduanas.SelectedValue;
                                                                                    //c.loadCliente(cmbClientes.SelectedValue);
                                                                                    //hr.CODIGOCLIENTE = c.CODIGOCLIENTEADUANAS;
                                                                                    hr.CODIGOCLIENTE = cmbClientes.SelectedValue;
                                                                                    hr.REFERENCIACLIENTE = txtReferenciaCliente.Text;
                                                                                    hr.PROVEEDOR = txtProveedores.Text;
                                                                                    hr.DESCRIPCIONPRODUCTO = txtProducto.Text;
                                                                                    //u.loadUsuario(cmbUsuarioAduana.SelectedValue);
                                                                                    //hr.IDRECEPTOR = u.IDUSUARIOADUANAS;
                                                                                    hr.IDRECEPTOR = cmbUsuarioAduana.SelectedValue;
                                                                                    //revisar si la aduana es terrestre o no?
                                                                                    //hr.IDFLUJO = "0";
                                                                                    hr.ESTADO = "0";
                                                                                    hr.actualizar();
                                                                                }
                                                                                cr.loadComplementoRecepcion(edInstruccion.Value.Trim());
                                                                                if (cr.totalRegistros > 0)
                                                                                {
                                                                                    cr.VALORCIF = Convert.ToDouble(txtValorCIF.Value);
                                                                                    cr.VALORFOB = Convert.ToDouble(txtValorFOB.Value);
                                                                                    cr.FLETE = Convert.ToDouble(txtFlete.Value);
                                                                                    cr.SEGURO = Convert.ToDouble(txtSeguro.Value);
                                                                                    string codPais = "";
                                                                                    if (cmbPaisOrigen.SelectedValue == "H")
                                                                                        codPais = "HN";
                                                                                    else if (cmbPaisOrigen.SelectedValue == "G")
                                                                                        codPais = "GT";
                                                                                    else if (cmbPaisOrigen.SelectedValue == "S")
                                                                                        codPais = "SV";
                                                                                    else if (cmbPaisOrigen.SelectedValue == "N")
                                                                                        codPais = "NI";
                                                                                    else
                                                                                        codPais = cmbPaisOrigen.SelectedValue;
                                                                                    cr.CODIGOPAIS = codPais;
                                                                                    cr.CODIGOREGIMEN = cmbRegimen.SelectedValue;
                                                                                    cr.DESTINO = txtCiudadDestino.Text;
                                                                                    cr.PESO = Convert.ToDouble(txtPeso.Value);
                                                                                    cr.FECHAVENCIMIENTO = dtpFechaVencTiempoLibreNaviera.SelectedDate.ToString();
                                                                                    cr.TIPOCARGA = cmbTipoCargamento.SelectedValue;
                                                                                    cr.ESTADO = "0";
                                                                                    cr.actualizar();
                                                                                }

                                                                                ee.loadEncabezadoEsquema(edInstruccion.Value.Trim());
                                                                                if (ee.totalRegistros > 0)
                                                                                {
                                                                                    ee.CodigoCliente = cmbClientes.SelectedValue;
                                                                                    ee.ValorCIF = Convert.ToDouble(txtValorCIF.Value);
                                                                                    ee.Peso = Convert.ToDouble(txtPeso.Value) / 1000;
                                                                                    ee.DescripcionProducto = txtProducto.Text;
                                                                                    ee.TipoCarga = cmbTipoCargamento.SelectedValue;
                                                                                    ee.Contenedores = Int16.Parse(txtContenedores.Text);
                                                                                    ee.TipoCargamento = cmbTipoCarga.SelectedValue;
                                                                                    c.loadCliente(cmbClientes.SelectedValue);
                                                                                    ee.Importador = c.NOMBRE;
                                                                                    ee.VecimientoNaviera = Convert.ToDateTime(dtpFechaVencTiempoLibreNaviera.SelectedDate);
                                                                                    ee.actualizar();
                                                                                }
                                                                            }
                                                                            catch (Exception ex)
                                                                            {
                                                                                logError(ex.Message, Session["IdUsuario"].ToString(), "Instrucciones, modificar, HojaRuta y ComplementoRecepción");
                                                                            }
                                                                            finally
                                                                            {
                                                                                desconectarAduanas();
                                                                            }
                                                                            ///////////////////////////////////////////////////////Darwin Noviembre 2014///////////////////////////////////////////////////////
                                                                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                            DetalleGastosBO DG = new DetalleGastosBO(logApp);
                                                                            DG.loadHojaxFactura(ins.NUMEROFACTURA, ins.IDINSTRUCCION.Substring(0, 1));
                                                                            if (DG.totalRegistros > 0)
                                                                            {
                                                                                DG.ActualizarHojaxFactura(ins.NUMEROFACTURA, ins.IDINSTRUCCION.Substring(0, 1), ins.IDINSTRUCCION);
                                                                            }
                                                                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                            registrarMensaje("Instrucción " + edInstruccion.Value + " modificada exitosamente, en tramite " + txtTramite.Text.Trim());
                                                                            llenarBitacora("Se modificó la instrucción " + edInstruccion.Value + ", en trámite " + txtTramite.Text.Trim(), Session["IdUsuario"].ToString(), edInstruccion.Value);
                                                                            //if (User.IsInRole("Inplant") || permitir)
                                                                            //    Response.Redirect("CrearInstrucciones.aspx");
                                                                            //else
                                                                            //    mpInstrucciones.SelectedIndex = 0;
                                                                            rgInstrucciones.Rebind();
                                                                            limpiarControles();

                                                                        }
                                                                        else
                                                                            registrarMensaje("Instrucción no se puede modificar");
                                                                    }
                                                                    #endregion

                                                                    #region Duplicar
                                                                    else if (btnGuardar.CommandName == "Duplicar")
                                                                    {
                                                                        a.loadAduanas(cmbAduanas.SelectedValue);
                                                                        ins.loadMaxInstruccionXAduanaCompleta(cmbAduanas.SelectedValue);
                                                                        if (ins.totalRegistros <= 0)
                                                                             //H-014-1
                                                                          
                                                                        
                                                                            if(cmbTipoInstruccion.SelectedValue=="0")
                                                                                {
                                                                                    idInstruccion = a.CODPAIS + "-" + a.CODIGOADUANA + "-" + "1";
                                                                                }
                                                                            else
                                                                            {
                                                                                idInstruccion = a.CODPAIS + "r" + a.CODIGOADUANA + "-" + "1";
                                                                            }
                                                                        else
                                                                            if (cmbTipoInstruccion.SelectedValue == "0")
                                                                            {
                                                                                idInstruccion = a.CODPAIS + "-" + a.CODIGOADUANA + "-" + ins.TABLA.Rows[0]["MaxIdInstruccion"].ToString();
                                                                                
                                                                            }
                                                                            else
                                                                            {
                                                                                idInstruccion = a.CODPAIS + "r" + a.CODIGOADUANA + "-" + ins.TABLA.Rows[0]["MaxIdInstruccion"].ToString();
                                                                            }
                                                                        

                                                                        idTramite = txtTramite.Text.Trim();

                                                                        //guarda datos en la tabla instrucciones
                                                                        ins.loadInstruccion("-1");
                                                                        ins.newLine();
                                                                        ins.IDTRAMITE = idTramite;
                                                                        ins.IDINSTRUCCION = idInstruccion;
                                                                        ins.CODPAISHOJARUTA = cmbPaisHojaRuta.SelectedValue;
                                                                        ins.CODIGOADUANA = cmbAduanas.SelectedValue;
                                                                        ins.FECHARECEPCION = dtpFechaRecepcion.SelectedDate.ToString();
                                                                        ins.IDCLIENTE = cmbClientes.SelectedValue;
                                                                        ins.OFICIALCUENTA = txtOficialCuenta.Text;
                                                                        ins.REFERENCIACLIENTE = txtReferenciaCliente.Text;
                                                                        ins.PROVEEDOR = txtProveedores.Text;
                                                                        ins.VALORFOB = Convert.ToDouble(txtValorFOB.Value);
                                                                        ins.FLETE = Convert.ToDouble(txtFlete.Value);
                                                                        ins.SEGURO = Convert.ToDouble(txtSeguro.Value);
                                                                        ins.OTROS = Convert.ToDouble(txtOtros.Value);
                                                                        ins.VALORCIF = Convert.ToDouble(txtValorCIF.Value);
                                                                        ins.CODDOCUMENTOEMBARQUE = cmbDocumentoEmbarque.SelectedValue;
                                                                        ins.CANTIDADBULTOS = Convert.ToInt32(txtCantidadBultos.Value);
                                                                        ins.PESOKGS = Convert.ToDouble(txtPeso.Value);
                                                                        ins.CONTENEDORES = txtContenedores.Text;
                                                                        ins.DESCRIPCIONCONTENEDORES = txtDescripcionContenedores.Text.Trim();
                                                                        ins.PRODUCTO = txtProducto.Text;
                                                                        ins.CODREGIMEN = cmbRegimen.SelectedValue;
                                                                        ins.CODPAISPROCEDENCIA = cmbPaisProcedencia.SelectedValue;
                                                                        ins.CODPAISORIGEN = cmbPaisOrigen.SelectedValue;
                                                                        ins.CODPAISDESTINO = cmbPaisDestino.SelectedValue;
                                                                        ins.CIUDADDESTINO = txtCiudadDestino.Text;
                                                                        ins.CODTIPOTRANSPORTE = cmbTipoTransporte.SelectedValue;
                                                                        ins.CODTIPOCARGAMENTO = cmbTipoCargamento.SelectedValue;
                                                                        ins.CODTIPOCARGA = cmbTipoCarga.SelectedValue;
                                                                        ins.FECHAVENCTIEMPOLIBRENAVIERA = dtpFechaVencTiempoLibreNaviera.SelectedDate.ToString();
                                                                        ins.FECHAVENCTIEMPOLIBREPUERTO = dtpFechaVencTiempoLibrePuerto.SelectedDate.ToString();
                                                                        ins.ETA = dtpETA.SelectedDate.ToString();
                                                                        ins.TIEMPOESTIMADOENTREGA = dtpTiempoEstimadoEntrega.SelectedDate.ToString();
                                                                        ins.INSTRUCCIONESENTREGA = Server.HtmlDecode(txtInstruccionesEntrega.Text);
                                                                        ins.OBSERVACION = Server.HtmlDecode(txtObservacion.Text);
                                                                        ins.FECHA = DateTime.Now.ToString();
                                                                        ins.IDOFICIALCUENTA = Recuperar_Oficial_Cuenta(cmbClientes.SelectedValue);


                                                                        if (CBDua.Checked)
                                                                        {

                                                                            ins.RDUA = "1";
                                                                        }
                                                                        
                                                                        if (cmbDivision.Enabled == false)
                                                                        {
                                                                            ins.DIVISION = "";
                                                                            ins.PRODUCTOSCARGILL = "";
                                                                        }
                                                                        else
                                                                        {
                                                                            ins.DIVISION = cmbDivision.SelectedValue;
                                                                            ins.PRODUCTOSCARGILL = cmbProductoCargill.SelectedValue;
                                                                        }
                                                                        if (User.IsInRole("Inplant") || permitir)
                                                                            ins.CODESTADO = "1";
                                                                        else
                                                                            ins.CODESTADO = "0";
                                                                        if (permitirGA)
                                                                        {
                                                                            try
                                                                            {
                                                                                FlujoCargaBO fc = new FlujoCargaBO(logApp);
                                                                                fc.loadEstadoFlujoCargaXPaisRegimen(cmbPaisHojaRuta.SelectedValue, cmbRegimen.SelectedValue);
                                                                                if (fc.totalRegistros > 0)
                                                                                    ins.IDESTADOFLUJO = fc.IDESTADOFLUJO;
                                                                            }
                                                                            catch { }
                                                                        }
                                                                        else

                                                                        ins.IDESTADOFLUJO = "0";
                                                                        ins.IDUSUARIO = Session["IdUsuario"].ToString();
                                                                        ins.IDUSUARIOADUANA = cmbUsuarioAduana.SelectedValue;
                                                                        ins.INCOTERM = cmbIncoterm.SelectedValue;
                                                                        ins.NUMEROFACTURA = txtNumFactura.Text;



                                                                        Gastos_Masivos(this.cmbClientes.SelectedValue, idInstruccion, this.cmbClientes.Text, cmbAduanas.SelectedValue);
                                                                     //   if (this.CBDua.Checked )
                                                                       // {
                                                                            ProcesoRequiereDua(idInstruccion, cmbRegimen.SelectedValue);
                                                                        //}
                                                                       
                                                                        ins.commitLine();
                                                                        ins.actualizar();

                                                                        if (cmbClientes.Text.Contains("CARGILL"))
                                                                        {
                                                                            CargaCargill(ins.IDINSTRUCCION);
                                                                        }
                                                                        //txtTramite.Text = idTramite;
                                                                        //txtIntruccion.Text = idInstruccion;
                                                                        try
                                                                        {
                                                                            conectarAduanas();
                                                                            HojaRutaBO hr = new HojaRutaBO(logAppAduanas);
                                                                            ComplementoRecepcionBO cr = new ComplementoRecepcionBO(logAppAduanas);
                                                                            EncabezadoEsquemaBO ee = new EncabezadoEsquemaBO(logAppAduanas);
                                                                            HojaRutaFlujoPortuarioBO hrfp = new HojaRutaFlujoPortuarioBO(logAppAduanas);
                                                                            HojaRutaEsquemaTramitesBO hret = new HojaRutaEsquemaTramitesBO(logAppAduanas);
                                                                            EsquemaEnvioDoctosBO eed = new EsquemaEnvioDoctosBO(logAppAduanas);
                                                                            hr.loadHojaRuta("-1");
                                                                            hr.newLine();
                                                                            hr.IDHOJARUTA = idInstruccion;
                                                                            hr.FECHARECEPCION = DateTime.Now.ToString();
                                                                            hr.CODIGOADUANA = cmbAduanas.SelectedValue;
                                                                            //c.loadCliente(cmbClientes.SelectedValue);
                                                                            //hr.CODIGOCLIENTE = c.CODIGOCLIENTEADUANAS;
                                                                            hr.CODIGOCLIENTE = cmbClientes.SelectedValue;
                                                                            hr.REFERENCIACLIENTE = txtReferenciaCliente.Text;
                                                                            hr.PROVEEDOR = txtProveedores.Text;
                                                                            hr.DESCRIPCIONPRODUCTO = txtProducto.Text;
                                                                            //u.loadUsuario(cmbUsuarioAduana.SelectedValue);
                                                                            //hr.IDRECEPTOR = u.IDUSUARIOADUANAS;
                                                                            hr.IDRECEPTOR = cmbUsuarioAduana.SelectedValue;
                                                                            if (cmbAduanas.SelectedValue == "015" || cmbAduanas.SelectedValue == "018" || cmbAduanas.SelectedValue == "019" || cmbAduanas.SelectedValue == "020" || cmbAduanas.SelectedValue == "021" || cmbAduanas.SelectedValue == "022" || cmbAduanas.SelectedValue == "027")
                                                                                hr.IDFLUJO = "6";
                                                                            else
                                                                                hr.IDFLUJO = "0";
                                                                            hr.ESTADO = "0";
                                                                            hr.commitLine();
                                                                            hr.actualizar();

                                                                            cr.loadComplementoRecepcion("-1");
                                                                            cr.newLine();
                                                                            cr.IDHOJARUTA = idInstruccion;
                                                                            cr.VALORCIF = Convert.ToDouble(txtValorCIF.Value);
                                                                            cr.VALORFOB = Convert.ToDouble(txtValorFOB.Value);
                                                                            cr.FLETE = Convert.ToDouble(txtFlete.Value);
                                                                            cr.SEGURO = Convert.ToDouble(txtSeguro.Value);
                                                                            string codPais = "";
                                                                            if (cmbPaisOrigen.SelectedValue == "H")
                                                                                codPais = "HN";
                                                                            else if (cmbPaisOrigen.SelectedValue == "G")
                                                                                codPais = "GT";
                                                                            else if (cmbPaisOrigen.SelectedValue == "S")
                                                                                codPais = "SV";
                                                                            else if (cmbPaisOrigen.SelectedValue == "N")
                                                                                codPais = "NI";
                                                                            else
                                                                                codPais = cmbPaisOrigen.SelectedValue;
                                                                            cr.CODIGOPAIS = codPais;
                                                                            cr.CODIGOREGIMEN = cmbRegimen.SelectedValue;
                                                                            cr.DESTINO = txtCiudadDestino.Text;
                                                                            cr.PESO = Convert.ToDouble(txtPeso.Value);
                                                                            cr.FECHAVENCIMIENTO = dtpFechaVencTiempoLibreNaviera.SelectedDate.ToString();
                                                                            cr.TIPOCARGA = cmbTipoCargamento.SelectedValue;
                                                                            cr.ESTADO = "0";
                                                                            cr.commitLine();
                                                                            cr.actualizar();

                                                                            ee.loadEncabezadoEsquema("-1");
                                                                            ee.newLine();
                                                                            ee.IdHojaRuta = idInstruccion;
                                                                            ee.CodigoCliente = cmbClientes.SelectedValue;
                                                                            ee.ValorCIF = Convert.ToDouble(txtValorCIF.Value);
                                                                            ee.Peso = Convert.ToDouble(txtPeso.Value) / 1000;
                                                                            ee.DescripcionProducto = txtProducto.Text;
                                                                            ee.TipoCarga = cmbTipoCargamento.SelectedValue;
                                                                            ee.Contenedores = Int16.Parse(txtContenedores.Text);
                                                                            ee.TipoCargamento = cmbTipoCarga.SelectedValue;
                                                                            c.loadCliente(cmbClientes.SelectedValue);
                                                                            ee.Importador = c.NOMBRE;
                                                                            ee.VecimientoNaviera = Convert.ToDateTime(dtpFechaVencTiempoLibreNaviera.SelectedDate);
                                                                            if (cmbAduanas.SelectedValue == "015" || cmbAduanas.SelectedValue == "018" || cmbAduanas.SelectedValue == "019" || cmbAduanas.SelectedValue == "020" || cmbAduanas.SelectedValue == "021" || cmbAduanas.SelectedValue == "022" || cmbAduanas.SelectedValue == "027")
                                                                            {
                                                                                //if (cmbPaisHojaRuta.SelectedValue == "H" && (cmbRegimen.SelectedValue == "1000" || cmbRegimen.SelectedValue == "1050" || cmbRegimen.SelectedValue == "1051" || cmbRegimen.SelectedValue == "1052" || cmbRegimen.SelectedValue == "1100" || cmbRegimen.SelectedValue == "2000" || cmbRegimen.SelectedValue == "2100" || cmbRegimen.SelectedValue == "2200" || cmbRegimen.SelectedValue == "3040" || cmbRegimen.SelectedValue == "3051" || cmbRegimen.SelectedValue == "3052" || cmbRegimen.SelectedValue == "3053" || cmbRegimen.SelectedValue == "3054" || cmbRegimen.SelectedValue == "3059" || cmbRegimen.SelectedValue == "3070" || cmbRegimen.SelectedValue == "3154" || cmbRegimen.SelectedValue == "3155" || cmbRegimen.SelectedValue == "5110" || cmbRegimen.SelectedValue == "5300" || cmbRegimen.SelectedValue == "3051" && cmbClientes.SelectedValue.ToString() == "8000017") || cmbRegimen.SelectedValue == "CB")
                                                                                //{
                                                                                    ee.Status = "2";
                                                                                    ee.Flujo = 9;
                                                                                //}
                                                                                //else
                                                                                //{
                                                                                    //ee.Status = "1";
                                                                                    //ee.Flujo = 2;
                                                                                //}
                                                                            }
                                                                            ee.commitLine();
                                                                            ee.actualizar();

                                                                            //nueva modificación 09/10/2012
                                                                            hrfp.loadHojaRutaFlujo(-1);
                                                                            hrfp.newLine();
                                                                            hrfp.IDHOJARUTA = idInstruccion;
                                                                            hrfp.IDFLUJO = 1;
                                                                            hrfp.OBS = "Sistema Aduanas";
                                                                            hrfp.IDUSUARIO = "121";
                                                                            hrfp.commitLine();
                                                                            hrfp.actualizar();

                                                                            hret.loadAllCamposHojaRuta("-1");
                                                                            hret.newLine();
                                                                            hret.IdHojaRuta = idInstruccion;
                                                                            hret.IdConceptoSAP = 139;
                                                                            hret.Monto = 0;
                                                                            hret.ValorReal = 0;
                                                                            hret.Solicitar = "1";
                                                                            hret.Aprobar = "1";
                                                                            hret.NumRecibo = idInstruccion;
                                                                            hret.commitLine();
                                                                            hret.actualizar();

                                                                            eed.newLine();
                                                                            eed.loadAllDoctos("-1");
                                                                            eed.IdHojaRuta = idInstruccion;
                                                                            eed.IdConceptoSAP = 139;
                                                                            eed.IdEmpleado = "121";
                                                                            eed.commitLine();
                                                                            eed.actualizar();
                                                                        }
                                                                        catch (Exception ex)
                                                                        {
                                                                            logError(ex.Message, Session["IdUsuario"].ToString(), "Instrucciones, duplicar, HojaRuta y ComplementoRecepción");
                                                                        }
                                                                        finally
                                                                        {
                                                                            desconectarAduanas();
                                                                        }

                                                                        registrarMensaje("Instrucción " + idInstruccion + " duplicada exitosamente, en tramite " + idTramite);
                                                                        llenarBitacora("Se duplicó la instrucción " + idInstruccion + ", en trámite " + idTramite, Session["IdUsuario"].ToString(), idInstruccion);
                                                                        //if (User.IsInRole("Inplant") || permitir)                                                                
                                                                        //        Response.Redirect("CrearInstrucciones.aspx");                                                               
                                                                        //else
                                                                        //    mpInstrucciones.SelectedIndex = 0;
                                                                        limpiarControles();
                                                                        ///////////////////////////////////////////////////////Darwin Noviembre 2014///////////////////////////////////////////////////////
                                                                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                        DetalleGastosBO DG = new DetalleGastosBO(logApp);
                                                                        DG.loadHojaxFactura(ins.NUMEROFACTURA, ins.IDINSTRUCCION.Substring(0, 1));
                                                                        if (DG.totalRegistros > 0)
                                                                        {
                                                                            DG.ActualizarHojaxFactura(ins.NUMEROFACTURA, ins.IDINSTRUCCION.Substring(0, 1), ins.IDINSTRUCCION);
                                                                        }
                                                                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                        rgInstrucciones.Rebind();
                                                                        //mpInstrucciones.SelectedIndex = 0;
                                                                    }
                                                                    #endregion

                                                                    #region Crear
                                                                    else
                                                                    {
                                                                        #region Generar Codigo Hoja de Ruta
                                                                        a.loadAduanas(cmbAduanas.SelectedValue);
                                                                        ins.loadMaxInstruccionXAduanaCompleta(cmbAduanas.SelectedValue);
                                                                        if (ins.totalRegistros <= 0)
                                                                        //    idInstruccion = a.CODPAIS + "-" + a.CODIGOADUANA + "-" + "1";   //HN-014-1

                                                                        //else
                                                                        //    idInstruccion = a.CODPAIS + "-" + a.CODIGOADUANA + "-" + ins.TABLA.Rows[0][0].ToString();
                                                                        
                                                                        if(cmbTipoInstruccion.SelectedValue=="0")
                                                                        {
                                                                            idInstruccion = a.CODPAIS + "-" + a.CODIGOADUANA + "-" + "1";
                                                                        }   
                                                                        else
                                                                        {
                                                                            idInstruccion = a.CODPAIS + "r" + a.CODIGOADUANA + "-" + "1";
                                                                        }
                                                                            
                                                                        else if (cmbTipoInstruccion.SelectedValue == "0")
                                                                        {
                                                                            idInstruccion = a.CODPAIS + "-" + a.CODIGOADUANA + "-" + ins.TABLA.Rows[0]["MaxIdInstruccion"].ToString();
                                                                        }
                                                                        else
                                                                        {
                                                                            idInstruccion = a.CODPAIS + "r" + a.CODIGOADUANA + "-" + ins.TABLA.Rows[0]["MaxIdInstruccion"].ToString();
                                                                        }
                                                                        
                                                                        #endregion

                                                                        DateTime fecha = DateTime.Now;
                                                                        idTramite = "TR" + fecha.Year.ToString().Substring(2) + fecha.Month.ToString("00") + fecha.Day.ToString("00") + "." + fecha.Hour.ToString("00") + fecha.Minute.ToString("00") + fecha.Second.ToString("00") + "." + fecha.Millisecond.ToString("000");

                                                                        #region Guardar Nueva Instruccion 
                                                                        ins.loadInstruccion("-1");
                                                                        ins.newLine();
                                                                        ins.IDTRAMITE = idTramite;
                                                                        ins.IDINSTRUCCION = idInstruccion;
                                                                        ins.CODPAISHOJARUTA = cmbPaisHojaRuta.SelectedValue;
                                                                        ins.CODIGOADUANA = cmbAduanas.SelectedValue;
                                                                        ins.FECHARECEPCION = dtpFechaRecepcion.SelectedDate.ToString();
                                                                        ins.IDCLIENTE = cmbClientes.SelectedValue;
                                                                        ins.OFICIALCUENTA = txtOficialCuenta.Text;
                                                                        ins.REFERENCIACLIENTE = txtReferenciaCliente.Text;
                                                                        ins.PROVEEDOR = txtProveedores.Text;
                                                                        ins.VALORFOB = Convert.ToDouble(txtValorFOB.Value);
                                                                        ins.FLETE = Convert.ToDouble(txtFlete.Value);
                                                                        ins.SEGURO = Convert.ToDouble(txtSeguro.Value);
                                                                        ins.OTROS = Convert.ToDouble(txtOtros.Value);
                                                                        ins.VALORCIF = Convert.ToDouble(txtValorCIF.Value);
                                                                        ins.CODDOCUMENTOEMBARQUE = cmbDocumentoEmbarque.SelectedValue;
                                                                        ins.CANTIDADBULTOS = Convert.ToInt32(txtCantidadBultos.Value);
                                                                        ins.PESOKGS = Convert.ToDouble(txtPeso.Value);
                                                                        ins.CONTENEDORES = txtContenedores.Text;
                                                                        ins.DESCRIPCIONCONTENEDORES = txtDescripcionContenedores.Text.Trim();
                                                                        ins.PRODUCTO = txtProducto.Text;
                                                                        ins.CODREGIMEN = cmbRegimen.SelectedValue;
                                                                        ins.CODPAISPROCEDENCIA = cmbPaisProcedencia.SelectedValue;
                                                                        ins.CODPAISORIGEN = cmbPaisOrigen.SelectedValue;
                                                                        ins.CODPAISDESTINO = cmbPaisDestino.SelectedValue;
                                                                        ins.CIUDADDESTINO = txtCiudadDestino.Text;
                                                                        ins.CODTIPOTRANSPORTE = cmbTipoTransporte.SelectedValue;
                                                                        ins.CODTIPOCARGAMENTO = cmbTipoCargamento.SelectedValue;
                                                                        ins.CODTIPOCARGA = cmbTipoCarga.SelectedValue;
                                                                        ins.FECHAVENCTIEMPOLIBRENAVIERA = dtpFechaVencTiempoLibreNaviera.SelectedDate.ToString();
                                                                        ins.FECHAVENCTIEMPOLIBREPUERTO = dtpFechaVencTiempoLibrePuerto.SelectedDate.ToString();
                                                                        ins.ETA = dtpETA.SelectedDate.ToString();
                                                                        ins.TIEMPOESTIMADOENTREGA = dtpTiempoEstimadoEntrega.SelectedDate.ToString();
                                                                        ins.INSTRUCCIONESENTREGA = Server.HtmlDecode(txtInstruccionesEntrega.Text);
                                                                        ins.OBSERVACION = Server.HtmlDecode(txtObservacion.Text);
                                                                        ins.FECHA = DateTime.Now.ToString();
                                                                        ins.IDOFICIALCUENTA=this.Recuperar_Oficial_Cuenta(cmbClientes.SelectedValue);


                                                                        if (CBDua.Checked)
                                                                        {

                                                                            ins.RDUA = "1";
                                                                        }
                                                                        
                                                                        if (cmbDivision.Enabled == false)
                                                                        {
                                                                        }
                                                                        else
                                                                        {
                                                                            ins.DIVISION = cmbDivision.SelectedValue;
                                                                            ins.PRODUCTOSCARGILL = cmbProductoCargill.SelectedValue;
                                                                        }
                                                                        if (User.IsInRole("Inplant") || permitir)
                                                                            ins.CODESTADO = "1";
                                                                        else
                                                                            ins.CODESTADO = "0";
                                                                        if (permitirGA)
                                                                        {
                                                                            try
                                                                            {
                                                                                FlujoCargaBO fc = new FlujoCargaBO(logApp);
                                                                                fc.loadEstadoFlujoCargaXPaisRegimen(cmbPaisHojaRuta.SelectedValue, cmbRegimen.SelectedValue);
                                                                                if (fc.totalRegistros > 0)
                                                                                    ins.IDESTADOFLUJO = fc.IDESTADOFLUJO;
                                                                            }
                                                                            catch { }
                                                                        }
                                                                        else
                                                                        ins.IDESTADOFLUJO = "0";
                                                                        ins.IDUSUARIO = Session["IdUsuario"].ToString();
                                                                        ins.IDUSUARIOADUANA = cmbUsuarioAduana.SelectedValue;
                                                                        ins.INCOTERM = cmbIncoterm.SelectedValue;
                                                                        ins.NUMEROFACTURA = txtNumFactura.Text;
                                                                        ins.commitLine();
                                                                        ins.actualizar();

                                                                        #endregion 
                                                                        #region Gastos Masivos
                                                                        Gastos_Masivos(this.cmbClientes.SelectedValue, idInstruccion, this.cmbClientes.Text,cmbAduanas.SelectedValue);
                                                                        
                                                                       
                                                                      //  if (this.CBDua.Checked)
                                                                      //  {
                                                                            ProcesoRequiereDua(idInstruccion, cmbRegimen.SelectedValue);
                                                                       // }
                                                                        #endregion
                                                                        #region Dodumentos Clientes CARGILL
                                                                        if (cmbClientes.Text.Contains("CARGILL"))
                                                                        {
                                                                            CargaCargill(ins.IDINSTRUCCION);
                                                                        }
                                                                        //txtTramite.Text = idTramite;
                                                                        //txtIntruccion.Text = idInstruccion;
                                                                       
                                                                        ClientesBO clbo = new ClientesBO(logApp);
                                                                        clbo.loadClienteMensaje(cmbClientes.SelectedValue);
                                                                        if (clbo.totalRegistros > 0)
                                                                        {
                                                                            mensaje = "X";
                                                                            mensaje2 = clbo.MENSAJE;
                                                                        }
                                                                        #endregion 

                                                                        try
                                                                        {
                                                                            #region Ingreso al EsquemaTramites
                                                                            conectarAduanas();
                                                                            HojaRutaBO hr = new HojaRutaBO(logAppAduanas);
                                                                            ComplementoRecepcionBO cr = new ComplementoRecepcionBO(logAppAduanas);
                                                                            EncabezadoEsquemaBO ee = new EncabezadoEsquemaBO(logAppAduanas);
                                                                            HojaRutaFlujoPortuarioBO hrfp = new HojaRutaFlujoPortuarioBO(logAppAduanas);
                                                                            HojaRutaEsquemaTramitesBO hret = new HojaRutaEsquemaTramitesBO(logAppAduanas);
                                                                            EsquemaEnvioDoctosBO eed = new EsquemaEnvioDoctosBO(logAppAduanas);
                                                                            hr.loadHojaRuta("-1");
                                                                            hr.newLine();
                                                                            hr.IDHOJARUTA = idInstruccion;
                                                                            hr.FECHARECEPCION = dtpFechaRecepcion.SelectedDate.ToString();
                                                                            hr.CODIGOADUANA = cmbAduanas.SelectedValue;
                                                                            //c.loadCliente(cmbClientes.SelectedValue);
                                                                            //hr.CODIGOCLIENTE = c.CODIGOCLIENTEADUANAS;
                                                                            hr.CODIGOCLIENTE = cmbClientes.SelectedValue;
                                                                            hr.REFERENCIACLIENTE = txtReferenciaCliente.Text;
                                                                            hr.PROVEEDOR = txtProveedores.Text;
                                                                            hr.DESCRIPCIONPRODUCTO = txtProducto.Text;
                                                                            //u.loadUsuario(cmbUsuarioAduana.SelectedValue);
                                                                            //hr.IDRECEPTOR = u.IDUSUARIOADUANAS;
                                                                            hr.IDRECEPTOR = cmbUsuarioAduana.SelectedValue;
                                                                            if (cmbAduanas.SelectedValue == "015" || cmbAduanas.SelectedValue == "018" || cmbAduanas.SelectedValue == "019" || cmbAduanas.SelectedValue == "020" || cmbAduanas.SelectedValue == "021" || cmbAduanas.SelectedValue == "022" || cmbAduanas.SelectedValue == "027")
                                                                                hr.IDFLUJO = "6";
                                                                            else
                                                                                hr.IDFLUJO = "0";
                                                                            hr.ESTADO = "0";
                                                                            hr.commitLine();
                                                                            hr.actualizar();

                                                                            cr.loadComplementoRecepcion("-1");
                                                                            cr.newLine();
                                                                            cr.IDHOJARUTA = idInstruccion;
                                                                            cr.VALORCIF = Convert.ToDouble(txtValorCIF.Value);
                                                                            cr.VALORFOB = Convert.ToDouble(txtValorFOB.Value);
                                                                            cr.FLETE = Convert.ToDouble(txtFlete.Value);
                                                                            cr.SEGURO = Convert.ToDouble(txtSeguro.Value);
                                                                            string codPais = "";
                                                                            if (cmbPaisOrigen.SelectedValue == "H")
                                                                                codPais = "HN";
                                                                            else if (cmbPaisOrigen.SelectedValue == "G")
                                                                                codPais = "GT";
                                                                            else if (cmbPaisOrigen.SelectedValue == "S")
                                                                                codPais = "SV";
                                                                            else if (cmbPaisOrigen.SelectedValue == "N")
                                                                                codPais = "NI";
                                                                            else
                                                                                codPais = cmbPaisOrigen.SelectedValue;
                                                                            cr.CODIGOPAIS = codPais;
                                                                            cr.CODIGOREGIMEN = cmbRegimen.SelectedValue;
                                                                            cr.DESTINO = txtCiudadDestino.Text;

                                                                            cr.PESO = Convert.ToDouble(txtPeso.Value);
                                                                            cr.FECHAVENCIMIENTO = dtpFechaVencTiempoLibreNaviera.SelectedDate.ToString();
                                                                            cr.TIPOCARGA = cmbTipoCargamento.SelectedValue;
                                                                            cr.ESTADO = "0";
                                                                            cr.commitLine();
                                                                            cr.actualizar();

                                                                            ee.loadEncabezadoEsquema("-1");
                                                                            ee.newLine();
                                                                            ee.IdHojaRuta = idInstruccion;
                                                                            ee.CodigoCliente = cmbClientes.SelectedValue;
                                                                            ee.ValorCIF = Convert.ToDouble(txtValorCIF.Value);
                                                                            ee.Peso = Convert.ToDouble(txtPeso.Value) / 1000;
                                                                            ee.DescripcionProducto = txtProducto.Text;
                                                                            ee.TipoCarga = cmbTipoCargamento.SelectedValue;
                                                                            ee.Contenedores = Int16.Parse(txtContenedores.Text);
                                                                            ee.TipoCargamento = cmbTipoCarga.SelectedValue;
                                                                            c.loadCliente(cmbClientes.SelectedValue);
                                                                            ee.Importador = c.NOMBRE;
                                                                            ee.VecimientoNaviera = Convert.ToDateTime(dtpFechaVencTiempoLibreNaviera.SelectedDate);
                                                                            if (cmbAduanas.SelectedValue == "015" || cmbAduanas.SelectedValue == "018" || cmbAduanas.SelectedValue == "019" || cmbAduanas.SelectedValue == "020" || cmbAduanas.SelectedValue == "021" || cmbAduanas.SelectedValue == "022" || cmbAduanas.SelectedValue == "027")
                                                                            {
                                                                                //if (cmbPaisHojaRuta.SelectedValue == "H" && (cmbRegimen.SelectedValue == "1000" || cmbRegimen.SelectedValue == "1050" || cmbRegimen.SelectedValue == "1051" || cmbRegimen.SelectedValue == "1052" || cmbRegimen.SelectedValue == "1100" || cmbRegimen.SelectedValue == "2000" || cmbRegimen.SelectedValue == "2100" || cmbRegimen.SelectedValue == "2200" || cmbRegimen.SelectedValue == "3040" || cmbRegimen.SelectedValue == "3051" || cmbRegimen.SelectedValue == "3052" || cmbRegimen.SelectedValue == "3053" || cmbRegimen.SelectedValue == "3054" || cmbRegimen.SelectedValue == "3059" || cmbRegimen.SelectedValue == "3070" || cmbRegimen.SelectedValue == "3154" || cmbRegimen.SelectedValue == "3155" || cmbRegimen.SelectedValue == "5110" || cmbRegimen.SelectedValue == "5300" || cmbRegimen.SelectedValue == "3051" && cmbClientes.SelectedValue.ToString() == "8000017") || cmbRegimen.SelectedValue == "CB")
                                                                                //{
                                                                                    ee.Status = "2";
                                                                                    ee.Flujo = 9;
                                                                                //}
                                                                                //else
                                                                                //{
                                                                                    //ee.Status = "1";
                                                                                    //ee.Flujo = 2;
                                                                                //}
                                                                            }
                                                                            ee.commitLine();
                                                                            ee.actualizar();

                                                                            //nueva modificación 09/10/2012
                                                                            hrfp.loadHojaRutaFlujo(-1);
                                                                            hrfp.newLine();
                                                                            hrfp.IDHOJARUTA = idInstruccion;
                                                                            hrfp.IDFLUJO = 1;
                                                                            hrfp.OBS = "Sistema Aduanas";
                                                                            hrfp.IDUSUARIO = "121";
                                                                            hrfp.commitLine();
                                                                            hrfp.actualizar();

                                                                            hret.loadAllCamposHojaRuta("-1");
                                                                            hret.newLine();
                                                                            hret.IdHojaRuta = idInstruccion;
                                                                            hret.IdConceptoSAP = 139;
                                                                            hret.Monto = 0;
                                                                            hret.ValorReal = 0;
                                                                            hret.Solicitar = "1";
                                                                            hret.Aprobar = "1";
                                                                            hret.NumRecibo = idInstruccion;
                                                                            hret.commitLine();
                                                                            hret.actualizar();

                                                                            //////////////////////////////////Darwin 17/07/2014///////////////////////////////////
                                                                            eed.newLine();
                                                                            eed.loadAllDoctos("-1");
                                                                            eed.IdHojaRuta = idInstruccion;
                                                                            eed.IdConceptoSAP = 139;
                                                                            eed.IdEmpleado = "121";
                                                                            eed.commitLine();
                                                                            eed.actualizar();
                                                                           
                                                                            //////////////////////////////////Darwin 17/07/2014///////////////////////////////////
                                                                        }
                                                                        catch (Exception ex)
                                                                        {
                                                                            logError(ex.Message, Session["IdUsuario"].ToString(), "Instrucciones, crear HojaRuta y ComplementoRecepción");
                                                                        }
                                                                        finally
                                                                        {
                                                                            desconectarAduanas();
                                                                        }
                                                                            #endregion

                                                                            #region mensajes de ingreso Exitso
                                                                        if (mensaje == "X")
                                                                        {
                                                                            registrarMensaje("Instrucción " + idInstruccion + " ingresada exitosamente, en tramite " + idTramite + " " + mensaje2);
                                                                        }
                                                                        else
                                                                        {
                                                                            registrarMensaje("Instrucción " + idInstruccion + " ingresada exitosamente, en tramite " + idTramite);
                                                                        }
                                                                        llenarBitacora("Se ingresó la instrucción " + idInstruccion + ", en trámite " + idTramite, Session["IdUsuario"].ToString(), idInstruccion);
                                                                        //if (User.IsInRole("Inplant") || permitir)
                                                                        //    Response.Redirect("CrearInstrucciones.aspx");
                                                                        //else
                                                                        //    mpInstrucciones.SelectedIndex = 0;
                                                                        ///////////////////////////////////////////////////////Darwin Noviembre 2014///////////////////////////////////////////////////////
                                                                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endregion 
                                                                        #region  Detalles Gastos
                                                                        DetalleGastosBO DG = new DetalleGastosBO(logApp);
                                                                        DG.loadHojaxFactura(ins.NUMEROFACTURA, ins.IDINSTRUCCION.Substring(0, 1));
                                                                        if (DG.totalRegistros > 0)
                                                                        {
                                                                            DG.ActualizarHojaxFactura(ins.NUMEROFACTURA, ins.IDINSTRUCCION.Substring(0, 1), ins.IDINSTRUCCION);
                                                                        }

                                                                        
                                                                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                                                                        rgInstrucciones.Rebind();
                                                                        limpiarControles();
                                                                        #endregion
                                                                    }
                                                                    #endregion

                                                                    #endregion
                                                                  
            # region  Mensajes de Error
                                                                    }
                                                                    else
                                                                        registrarMensaje("Favor Seleccione un Proveedor");
                                                                }
                                                                else
                                                                    registrarMensaje("El Numero de Factura no puede esta Vacio");
                                                            }
                                                            else
                                                                registrarMensaje("ValorCIF debe ser mayor que cero");
                                                        }
                                                        else
                                                            registrarMensaje("Seleccione un tipo de carga");
                                                    }
                                                    else
                                                        registrarMensaje("Seleccione un tipo de cargamento");
                                                }
                                                else
                                                    registrarMensaje("Seleccione un país de destino");
                                            }
                                            else
                                                registrarMensaje("Seleccione un país de origen");
                                        }
                                        else
                                            registrarMensaje("Seleccione un país de procedencia");
                                    }
                                    else
                                        registrarMensaje("El peso debe ser mayor que cero");
                                }
                                else
                                    registrarMensaje("La descripción de los conetenedores no es valida o esta vacía");
                            }
                            else
                                registrarMensaje("El producto no puede estar vacío");
                        }
                        else
                            registrarMensaje("El proveedor no puede estar vacío");
                    }
                    else
                        registrarMensaje("El nombre del cliente no puede estar vacío");
                }
                else
                    registrarMensaje("La aduana no puede estar vacía");
            }
            else
                registrarMensaje("El País de la hoja de ruta no puede estar vacío");
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "Instrucciones");
        }
        finally
        {
            desconectar();
        }
#endregion 
        #endregion
    }
    protected void btnLimpiar_Click(object sender, ImageClickEventArgs e)
    {
        limpiarControles();
    }
    private void limpiarControles()
    {
        cargarDatosInicio();
        cmbPaisHojaRuta.SelectedIndex = 0;
        dtpFechaRecepcion.SelectedDate = DateTime.Now;
        txtOficialCuenta.Text = "";
        txtReferenciaCliente.Text = "";
        txtProveedores.Text = "";
        txtProducto.Text = "";
        txtValorFOB.Value = 0;
        txtFlete.Value = 0;
        txtSeguro.Value = 0;
        txtOtros.Value = 0;
        txtValorCIF.Value = 0;
        txtContenedores.Value = 1;
        txtDescripcionContenedores.Text = "";
        txtPeso.Value = 0;
        txtCantidadBultos.Text = "";
        dtpFechaVencTiempoLibreNaviera.SelectedDate = DateTime.Now;
        dtpFechaVencTiempoLibrePuerto.SelectedDate = DateTime.Now;
        dtpETA.SelectedDate = DateTime.Now;
        dtpTiempoEstimadoEntrega.SelectedDate = DateTime.Now;
        txtInstruccionesEntrega.Text = "";
        txtObservacion.Text = "";
    }
    protected void cmbPaisHojaRuta_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        llenarClientes();
        llenarRegimenesYAduanas();
        llenarOficialCuentaYRTN();
        llenarUsuariosAduana();
        txtProveedores.Text = "";
    }
    protected void cmbAduanas_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        llenarUsuariosAduana();
    }
    #endregion

    #region productosClientes
    public string descripciones = "";
    protected void btnEditProdDescs_Click(object sender, ImageClickEventArgs e)
    {
        descripciones = txtProducto.Text.Trim();
        lbDescripcionesRes.Items.Clear();
        llenarDescs();
        llenarDescsRes();
        mpInstrucciones.SelectedIndex = 2;
    }

    protected void llenarDescs()
    {
        try
        {
            conectar();
            ProductosClientesBO prodcli = new ProductosClientesBO(logApp);
            prodcli.loadBusquedaProductosClientes(cmbClientes.SelectedValue.Trim());

            lbDescripciones.DataSource = prodcli.TABLA;
            lbDescripciones.DataBind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void llenarDescsRes()
    {
        if (cmbClientes.IsEmpty) return;
        try
        {
            string[] descs = descripciones.Split(',', ';');
            foreach (String desc in descs)
            {
                if (desc.Trim().Length > 0)
                    lbDescripcionesRes.Items.Add(new RadListBoxItem(desc.Trim().ToUpper()));
            }
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void btnAgregar_Click(object sender, EventArgs e)
    {
        if (edDescripcion.Text.Trim().Length < 1) return;

        try
        {
            conectar();
            ProductosClientesBO prodcli = new ProductosClientesBO(logApp);

            prodcli.insertProductoCliente(cmbClientes.SelectedValue.Trim(), edDescripcion.Text.Trim());
            edDescripcion.Text = "";

            prodcli.loadBusquedaProductosClientes(cmbClientes.SelectedValue.Trim());

            lbDescripciones.DataSource = prodcli.TABLA;
            lbDescripciones.DataBind();
        }
        catch
        {

        }
        finally
        {
            desconectar();
        }
    }

    protected void btnEliminar_Click(object sender, EventArgs e)
    {
        if (lbDescripciones.SelectedValue.Trim().Length < 1) return;

        try
        {
            conectar();
            ProductosClientesBO prodcli = new ProductosClientesBO(logApp);

            prodcli.deleteProductoCliente(lbDescripciones.SelectedValue, cmbClientes.SelectedValue.Trim());

            prodcli.loadBusquedaProductosClientes(cmbClientes.SelectedValue.Trim());

            lbDescripciones.DataSource = prodcli.TABLA;
            lbDescripciones.DataBind();
        }
        catch
        {

        }
        finally
        {
            desconectar();
        }
    }

    //protected void btnIns_Click(object sender, EventArgs e)
    //{
    //    if (lbDescripciones.SelectedIndex > -1)
    //    {
    //        String valor = lbDescripciones.SelectedItem.Text.Trim().ToUpper();

    //        ListItem li = lbDescripcionesRes.Items.FindByText(valor);
    //        if (li != null)
    //            lbDescripcionesRes.Items.Remove(li);

    //        lbDescripcionesRes.Items.Add(new ListItem(valor, lbDescripciones.SelectedItem.Value));
    //    }
    //}

    //protected void btnRem_Click(object sender, EventArgs e)
    //{
    //    if (lbDescripcionesRes.SelectedIndex > -1)
    //        lbDescripcionesRes.Items.RemoveAt(lbDescripcionesRes.SelectedIndex);
    //}

    protected void btnRegresar_Click(object sender, ImageClickEventArgs e)
    {
        string producto = "";
        foreach (RadListBoxItem li in lbDescripcionesRes.Items)
        {
            producto += li.Text.Trim().ToUpper() + ", ";
        }
        txtProducto.Text = producto.Substring(0, producto.Trim().Length > 0 ? producto.Trim().Length - 1 : producto.Trim().Length);
        mpInstrucciones.SelectedIndex = 1;
    }
    #endregion

    #region proveedoresPais
    protected void btnAgregarProveedor_Click(object sender, ImageClickEventArgs e)
    {
        llenarDescProv();
        mpInstrucciones.SelectedIndex = 3;
    }

    protected void llenarDescProv()
    {
        try
        {
            conectar();
            ProveedoresPaisBO pp = new ProveedoresPaisBO(logApp);
            pp.loadProveedoresPais(cmbPaisHojaRuta.SelectedValue.Trim());
            lbProveedores.DataSource = pp.TABLA;
            lbProveedores.DataBind();
            pp.loadProveedoresPais(cmbPaisHojaRuta.SelectedValue, txtProveedores.Text);
            if (pp.totalRegistros > 0)
                lbProveedores.SelectedValue = pp.CODIGO;
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void btnAddProv_Click(object sender, EventArgs e)
    {
        if (edDescripcionProveedor.Text.Trim().Length < 1) return;
        try
        {
            conectar();
            ProveedoresPaisBO pp = new ProveedoresPaisBO(logApp);
            pp.insertProveedoresPais(edDescripcionProveedor.Text.Trim().ToUpper(), cmbPaisHojaRuta.SelectedValue);
            edDescripcionProveedor.Text = "";
            pp.loadProveedoresPais(cmbPaisHojaRuta.SelectedValue);
            lbProveedores.DataSource = pp.TABLA;
            lbProveedores.DataBind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void btnDelProv_Click(object sender, EventArgs e)
    {
        if (lbProveedores.SelectedValue.Trim().Length < 1) return;
        try
        {
            conectar();
            ProveedoresPaisBO pp = new ProveedoresPaisBO(logApp);
            pp.deleteProveedoresPais(lbProveedores.SelectedValue, cmbPaisHojaRuta.SelectedValue);

            pp.loadProveedoresPais(cmbPaisHojaRuta.SelectedValue);
            lbProveedores.DataSource = pp.TABLA;
            lbProveedores.DataBind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void btnRegresarProv_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (lbProveedores.Items.Count > 0)
            {
                if (lbProveedores.SelectedIndex >= 0)
                {
                    txtProveedores.Text = lbProveedores.SelectedItem.Text;
                    mpInstrucciones.SelectedIndex = 1;
                }
                else
                    registrarMensaje("Elija un proveedor");
            }
            else
                mpInstrucciones.SelectedIndex = 1;
        }
        catch { }
    }
    #endregion
    protected void cmbClientes_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        txtProducto.Text = "";
        llenarOficialCuentaYRTN();
        llenarDivision();
        if (cmbClientes.Text.Contains("CARGILL"))
        {
            lblCargill.Visible = true;
            cmbCargaCargill.Visible = true;
        }
        else
        {
            cmbCargaCargill.Dispose();
            cmbCargaCargill.ClearSelection();
            lblCargill.Visible = false;
            cmbCargaCargill.Visible = false;
        }
    }
    protected void txtValorFOB_TextChanged(object sender, EventArgs e)
    {
        if (txtValorFOB.Text == "")
            txtValorFOB.Value = 0;
        sumaCostos();
    }
    protected void txtFlete_TextChanged(object sender, EventArgs e)
    {
        if (txtFlete.Text == "")
            txtFlete.Value = 0;
        sumaCostos();
    }
    protected void txtSeguro_TextChanged(object sender, EventArgs e)
    {
        if (txtSeguro.Text == "")
            txtSeguro.Value = 0;
        sumaCostos();
    }
    protected void txtOtros_TextChanged(object sender, EventArgs e)
    {
        if (txtOtros.Text == "")
            txtOtros.Value = 0;
        sumaCostos();
    }
    private void sumaCostos()
    {
        try
        {
            txtValorCIF.Value = txtValorFOB.Value + txtFlete.Value + txtSeguro.Value + txtOtros.Value;
        }
        catch { }
    }
    public string[] descCont = null;
    protected void btnAgregarContenedores_Click(object sender, ImageClickEventArgs e)
    {
        descCont = descCont = txtDescripcionContenedores.Text.Trim().Split(",".ToCharArray());
        mpInstrucciones.SelectedIndex = 4;
    }
    protected void btnRegresarContenedor_Click(object sender, ImageClickEventArgs e)
    {
        txtDescripcionContenedores.Text = parameterForm("txtDescContenedores", "");
        mpInstrucciones.SelectedIndex = 1;
    }

    #region Agregar Ciudades
    protected void btnAgregarCiudad_Click(object sender, ImageClickEventArgs e)
    {
        llenarDescripcionCiudades();
        mpInstrucciones.SelectedIndex = 5;
    }

    protected void llenarDescripcionCiudades()
    {
        try
        {
            conectar();
            CiudadesPaisBO cp = new CiudadesPaisBO(logApp);
            cp.loadCiudadesPais(cmbPaisDestino.SelectedValue.Trim());
            lbCiudades.DataSource = cp.TABLA;
            lbCiudades.DataBind();
            cp.loadCiudadesPais(cmbPaisDestino.SelectedValue, txtCiudadDestino.Text);
            if (cp.totalRegistros > 0)
                lbCiudades.SelectedValue = cp.CODIGO;
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void btnAddCiudad_Click(object sender, EventArgs e)
    {
        if (edDescripcionCiudad.Text.Trim().Length < 1) return;
        try
        {
            conectar();
            CiudadesPaisBO cp = new CiudadesPaisBO(logApp);
            cp.insertCiudadesPais(edDescripcionCiudad.Text.Trim().ToUpper(), cmbPaisDestino.SelectedValue);
            edDescripcionCiudad.Text = "";
            cp.loadCiudadesPais(cmbPaisDestino.SelectedValue);
            lbCiudades.DataSource = cp.TABLA;
            lbCiudades.DataBind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void btnDelCiudad_Click(object sender, EventArgs e)
    {
        if (lbCiudades.SelectedValue.Trim().Length < 1) return;
        try
        {
            conectar();
            CiudadesPaisBO cp = new CiudadesPaisBO(logApp);
            cp.deleteCiudadesPais(lbCiudades.SelectedValue, cmbPaisDestino.SelectedValue);
            cp.loadCiudadesPais(cmbPaisDestino.SelectedValue);
            lbCiudades.DataSource = cp.TABLA;
            lbCiudades.DataBind();
        }
        catch { }
        finally
        {
            desconectar();
        }
    }

    protected void btnBackCiudad_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (lbCiudades.Items.Count > 0)
            {
                if (lbCiudades.SelectedIndex >= 0)
                {
                    txtCiudadDestino.Text = lbCiudades.SelectedItem.Text;
                    mpInstrucciones.SelectedIndex = 1;
                }
                else
                    registrarMensaje("Elija una ciudad");
            }
            else
                mpInstrucciones.SelectedIndex = 1;
        }
        catch { }
    }
    #endregion

    protected void rgInstrucciones_ItemDataBound(object sender, GridItemEventArgs e)
    {
        foreach (GridDataItem gridItem in rgInstrucciones.Items)
        {
            if (User.IsInRole("Administradores"))
            {
                if (gridItem.Cells[10].Text != "0")
                {
                    gridItem.Cells[2].Enabled = false;
                    //gridItem.Cells[3].Enabled = false;
                    gridItem.Cells[2].Text = "";
                    //gridItem.Cells[3].Text = "";
                }
            }
            else
            {
                if (gridItem.Cells[10].Text != "0")
                {
                    gridItem.Cells[2].Enabled = false;
                    gridItem.Cells[3].Enabled = false;
                    gridItem.Cells[2].Text = "";
                    gridItem.Cells[3].Text = "";
                }
            }
        }
    }
    protected void txtContenedores_TextChanged(object sender, EventArgs e)
    {
        //descCont = descCont = txtDescripcionContenedores.Text.Trim().Split(",".ToCharArray());
        if (int.Parse(txtContenedores.Text) > 0)
        {
            descCont = txtDescripcionContenedores.Text.Trim().Split(",".ToCharArray());
            mpInstrucciones.SelectedIndex = 4;
        }
    }
    #region Conexion
    private void conectarAduanas()
    {
        if (logAppAduanas == null)
            logAppAduanas = new GrupoLis.Login.Login();
        if (!logAppAduanas.conectado)
        {
            logAppAduanas.tipoConexion = TipoConexion.SQL_SERVER;
            logAppAduanas.SERVIDOR = mParamethers.Get("Servidor");
            logAppAduanas.DATABASE = mParamethers.Get("DatabaseEsquemaTramites2");
            logAppAduanas.USER = mParamethers.Get("User");
            logAppAduanas.PASSWD = mParamethers.Get("Password");
            logAppAduanas.conectar();
        }
    }

    private void desconectarAduanas()
    {
        if (logAppAduanas.conectado)
            logAppAduanas.desconectar();
    }
    #endregion
    protected void cmbDivision_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {//Aca se llenan los producos para cada Division, Se separan en las de Alcon, las de Dinant/Exportadora del Atlantico y otros que son el resto de
        //Divisiones de Cargill
        if (cmbDivision.Text.ToString() == "ALCON")
        {
            llenarProductoCargill("ALCON");
        }
        else if (cmbClientes.Text.ToString() == "CORPORACION DINANT S.A." || cmbClientes.Text.ToString() == "EXPORTADORA DEL ATLANTICO")
        {
            llenarProductoCargill("DINANT");
        }
        else
        {
            llenarProductoCargill("OTROS");
        }


    }
    private void llenarProductoCargill(string X)
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            c.loadDivision(X);
            cmbProductoCargill.DataSource = c.TABLA;
            cmbProductoCargill.DataBind();
            c.loadDivision("CARGACARGILL");
            cmbCargaCargill.Dispose();
            cmbCargaCargill.ClearSelection();
            cmbCargaCargill.DataSource = c.TABLA;
            cmbCargaCargill.DataBind();
            if (txtIntruccion.Text != "")
            {
                InstruccionesCargillCargaBO IC = new InstruccionesCargillCargaBO(logApp);
                IC.VerificarRegistro(txtIntruccion.Text);
                c.CODIGO = IC.TIPOCARGACARGILL;
                cmbCargaCargill.SelectedValue = c.CODIGO;
                lblCargill.Visible = true;
                cmbCargaCargill.Visible = true;
            }
        }
        catch { }
    }
    protected void CargaCargill(string IdInstruccion)
    {
        try
        {
            InstruccionesCargillCargaBO IC = new InstruccionesCargillCargaBO(logApp);
            IC.VerificarRegistro(IdInstruccion);
            if (IC.totalRegistros == 0)
            {
                IC.newLine();
            }
            IC.IDINSTRUCCION = IdInstruccion;
            IC.TIPOCARGACARGILL = cmbCargaCargill.SelectedValue;
            IC.IDUSUARIO = int.Parse(Session["IdUsuario"].ToString());
            IC.commitLine();
            IC.actualizar();
        }
        catch (Exception)
        {
        }
    }

    #region Gastos Masivos

    public Boolean Gastos_Masivos(String Id_Cliente,  String HojaRuta , String Nombre, String CodAduana )
    {

        Boolean Crea = false;
        PlantillaGastosMBO GM = new PlantillaGastosMBO(logApp);

        try
        {
            if (HojaRuta.Substring(0, 1) != "S")
            {

                #region Gastos Masivos Distintos del Salvador
                GM.loadGastos_masivos(Id_Cliente);

                if (GM.totalRegistros > 0)
                {


                    int verificar = 0;
                    double tasacambio = 1.00;
                    if (HojaRuta.Substring(0, 1) == "S")
                    {
                        verificar = 1;
                    }
                    else
                    { // verifica la tasa de cambio actual de un pais
                        TipoCambioBO TC = new TipoCambioBO(logApp);
                        TC.loadtasacambio(HojaRuta.Substring(0, 1));
                        if (TC.totalRegistros >= 1)
                        {
                            verificar = TC.totalRegistros;
                            tasacambio = TC.TASACAMBIO;
                        }
                    }

                    if (verificar == 1)
                    {
                        for (int i = 0; i < GM.totalRegistros; i++)
                        {
                            DetalleGastosBO DG1 = new DetalleGastosBO(logApp);
                            DetalleGastosBO DG = new DetalleGastosBO(logApp);
                            DG.BuscarHoja("1");
                            DG.newLine();
                            DG.IDINSTRUCCION = HojaRuta;
                            DG.IDGASTO = GM.IDGASTO;
                            DG.MEDIOPAGO = GM.MEDIOPAGO;
                            DG.PROVEEDOR = GM.IDPROVEEDOR;
                            DG.MONTO = GM.COSTO * tasacambio;
                            DG.IVA = GM.IVA;
                            DG.TOTALVENTA = (GM.VENTA * tasacambio);
                            DG.TASACAMBIOFIJA = tasacambio;
                            DG.TASACAMBIOVARIABLE = tasacambio;
                            CodigosBO C = new CodigosBO(logApp);
                            C.loadAllCampos("MONEDA" + HojaRuta.Substring(0, 1));
                            DG.MONEDA = C.CODIGO;
                            if (DG.MONTO > 0)
                            {
                                DG.ESTADO = "0";
                            }
                            else
                            {

                                DG.ESTADO = "10";
                            }

                            DG.ESTADO = "0";
                            DG.FECHACREACION = DateTime.Now.ToString();
                            DG.USUARIO = Session["IdUsuario"].ToString();

                            if (GM.MEDIOPAGO == "CC" && DG.MONTO > 0)
                            {
                                DG.ESTADO = "7";
                            }
                            DG.OBSERVACION = GM.OBSERVACION;
                            DG.CODESTADOFACTURACION = "0";
                            DG.REVERSARPARTIDA = "2";
                            DG.commitLine();
                            DG.actualizar();
                            GM.regSiguiente();

                        }

                        //  registrarMensaje("Gastos predetrminados  ingresados  exitosamente!");
                    }
                    else
                    {
                        // Eviar correo electronicoa a la nueva categoria indicando que no se crearon los gastor porque la tasa esta descatualizaza y quedebn ser creados automaticamente 
                        EnviarCorreo(HojaRuta, Nombre);

                    }

                }
                #endregion
            }

            else  if (HojaRuta.Substring(0, 1) == "S"){

                #region Gastos Masivos  del Salvador
                GM.loadGastos_masivos_Salvador(Id_Cliente, CodAduana);

                if (GM.totalRegistros > 0)
                {


                    int verificar = 0;
                    double tasacambio = 1.00;
                    if (HojaRuta.Substring(0, 1) == "S")
                    {
                        verificar = 1;
                    }
                    else
                    { // verifica la tasa de cambio actual de un pais
                        TipoCambioBO TC = new TipoCambioBO(logApp);
                        TC.loadtasacambio(HojaRuta.Substring(0, 1));
                        if (TC.totalRegistros >= 1)
                        {
                            verificar = TC.totalRegistros;
                            tasacambio = TC.TASACAMBIO;
                        }
                    }

                    if (verificar == 1)
                    {
                        for (int i = 0; i < GM.totalRegistros; i++)
                        {
                            DetalleGastosBO DG1 = new DetalleGastosBO(logApp);
                            DetalleGastosBO DG = new DetalleGastosBO(logApp);
                            DG.BuscarHoja("1");
                            DG.newLine();
                            DG.IDINSTRUCCION = HojaRuta;
                            DG.IDGASTO = GM.IDGASTO;
                            DG.MEDIOPAGO = GM.MEDIOPAGO;
                            DG.PROVEEDOR = GM.IDPROVEEDOR;
                            DG.MONTO = GM.COSTO * tasacambio;
                            DG.IVA = GM.IVA;
                            DG.TOTALVENTA = (GM.VENTA * tasacambio);
                            DG.TASACAMBIOFIJA = tasacambio;
                            DG.TASACAMBIOVARIABLE = tasacambio;
                            CodigosBO C = new CodigosBO(logApp);
                            C.loadAllCampos("MONEDA" + HojaRuta.Substring(0, 1));
                            DG.MONEDA = C.CODIGO;
                            if (DG.MONTO > 0)
                            {
                                DG.ESTADO = "0";
                            }
                            else
                            {

                                DG.ESTADO = "10";
                            }

                            DG.ESTADO = "0";
                            DG.FECHACREACION = DateTime.Now.ToString();
                            DG.USUARIO = Session["IdUsuario"].ToString();

                            if (GM.MEDIOPAGO == "CC" && DG.MONTO > 0)
                            {
                                DG.ESTADO = "7";
                            }
                            DG.OBSERVACION = GM.OBSERVACION;
                            DG.CODESTADOFACTURACION = "0";
                            DG.REVERSARPARTIDA = "2";
                            DG.commitLine();
                            DG.actualizar();
                            GM.regSiguiente();

                        }

                        //  registrarMensaje("Gastos predetrminados  ingresados  exitosamente!");
                    }
                    else
                    {
                        // Eviar correo electronicoa a la nueva categoria indicando que no se crearon los gastor porque la tasa esta descatualizaza y quedebn ser creados automaticamente 
                        //EnviarCorreo(HojaRuta, Nombre);

                    }

                }
                #endregion

            }
        }

        catch
        {

        }
           
            return Crea;
    }



    #region Enviar correo


    protected void EnviarCorreo(string Idinstruccion, string Cliente)
    {
        try
        {

            CorreosBO C = new CorreosBO(logApp);


            C.LoadCorreosXPais("N", "Gastos_Masivos");



            string Asunto = " Notificacion de Gastos masivos  en Instrucción " + Idinstruccion + " para el cliente " + Cliente;
            string Cuerpo = " Los [ Gastos  Masivos ] No fueron generados  automáticamente  en la Instrucción <b> " + Idinstruccion + " …</b> para el cliente <font color=\"red\">" + Cliente + "</font> debido a que" +
                            " la tasa de cambio no estaba actualizada, por lo tanto  <b> los gastos deben ser ingresados manualmente </b>. Favor tomar nota;" +
                            "<br><br><br> Este mensaje es generado  automáticamente , favor no responderlo";
            string Copias = C.COPIAS;
            string NombreUsuario = "Tracking Aduanas";
            string NombredelArchivo = "Remision";
            string usuario = "trackingaduanas";
            string pass = "nMgtdA$7PaRjQphEYZBD";
            string para = C.CORREOS;
            sendMailConParametros(para, Asunto, Cuerpo, Copias, NombreUsuario, NombredelArchivo, usuario, pass, ".xlsx");
        }
        catch (Exception)
        {

        }


    }
    protected void sendMailConParametros(string para, string subject, String body, string Copias, string nombreUsuario, string fileName, string user, string pass, string extencion)
    {
        try
        {
            // SmtpClient SmtpServer = new SmtpClient("mail.grupovesta.com");
            ServicePointManager.ServerCertificateValidationCallback += (o, c, ch, er) => true;
            StringBuilder bodytmp = new StringBuilder();
            //string bodytmp = @"<html><body><img src=""cid:FirmaJuandeDios""></body></html>";
            //string firma = @"<img src="+ "cid:" + to + ">";
            MailMessage mailmsg = new MailMessage();
            mailmsg.From = new MailAddress("trackingaduanas@grupovesta.com", user);
            mailmsg.To.Add(new MailAddress(para));
            mailmsg.CC.Add(Copias);
            //mailmsg.Bcc.Add(new MailAddress(mailFrom, nombreUsuario));                
            mailmsg.Subject = subject;
            //   mailmsg.Attachments.Add(new Attachment(@"J:\Files1\Aduanas\" + fileName + ".xlsx"));

            //bodytmp.Append("<html xmlns=\"http://www.w3.org/1999/xhtml\"> <head> <title>" + subject + "</title> </head> ");
            bodytmp.Append("<html> <head> <title>" + subject + "</title> </head> ");
            bodytmp.Append("<body> ");
            bodytmp.Append("<table>");
            bodytmp.Append("<tr>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("</tr>");
            bodytmp.Append("<tr>");
            bodytmp.Append("<td>" + body + "</td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("</tr>");
            bodytmp.Append("<tr>");
            //  bodytmp.Append("<td> " + "<img src=J:\\Files1\\Aduanas\\FirmaVesta.jpg</td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("</tr>");
            bodytmp.Append("</table>");
            bodytmp.Append("</body>");
            bodytmp.Append("</html>");

            AlternateView avHTML = AlternateView.CreateAlternateViewFromString(bodytmp.ToString(), null, MediaTypeNames.Text.Html);

            //CODIGO PARA AGREGAR IMAGEN AL CUERPO DEL CORREO
            //LinkedResource yourPictureRes = new LinkedResource(@"J:\Files1\Aduanas\FirmaVesta.jpg", MediaTypeNames.Image.Jpeg);
            //yourPictureRes.ContentId = mailFrom;
            //avHTML.LinkedResources.Add(yourPictureRes);
            //CODIGO PARA AGREGAR IMAGEN AL CUERPO DEL CORREO

            mailmsg.AlternateViews.Add(avHTML);

            mailmsg.IsBodyHtml = true;

            StreamWriter sw = new StreamWriter(new MemoryStream(), Encoding.UTF8);
            sw.Write(body.ToString());
            sw.Flush();

            sw.BaseStream.Position = 0;
            //mailmsg.Attachments.Add(new Attachment(sw.BaseStream, "Template.html", MediaTypeNames.Text.Html));

            SmtpClient smtpClient = new SmtpClient("190.4.28.53");
            smtpClient.Port = 587;
            smtpClient.EnableSsl = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(user, pass);

            smtpClient.Send(mailmsg);

            sw.Close();
        }
        catch (Exception ex)
        {
            //LogError(ex.InnerException + ex.Message, "sendMailConParametros", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString(), Session["IdUsuario"].ToString(), "Utilidades.cs");
        }
    }
    #endregion

    #endregion

    public void ProcesoRequiereDua(string idInstruccion, string Regimen)
    {   
        
        
        Boolean estado =false;
         string EstadoC = "-1";

       
        
            
             switch (Regimen)
             {  
                 
                 case "1000" :

                     if (cmbRegimen.SelectedValue == "1000" && cmbAduanas.SelectedValue == "016")
                     {
                         estado = true;
                         EstadoC = "3";
                        
                     }

                     else
                     {

                             if (this.CBDua.Checked)
                              {
                                 estado = true;
                                 EstadoC = "-2";
                             }
                             //else
                             //{
                             //    estado = true;
                             //    EstadoC = "-1";
                             //}
                            
                     }
                      
                 break;
                 
                 case "5100":
                 case "6020":
                 case "2000":
                 case "6010":
                     estado = true;
                     EstadoC = "-1";
                     break;
                 case "5300":
                  var res=  cmbClientes.SelectedItem.Value.Contains("rowley");
                  var res2 = cmbClientes.SelectedItem.Value.Contains("aersk");
                  if (res!=true&&res2!=true)
                     {
                         estado = true;
                         EstadoC = "-1"; 
                     }
                  break; 
                  
                 case "5170":
                     estado = true;
                     EstadoC = "-1";
                     break;
                 case "5070":
                     estado = true;
                     EstadoC = "-1";
                     break;
                 case "5700":
                     estado = true;
                     EstadoC = "-1";
                     break;
                 case "8000":
                     estado = true;
                     EstadoC = "-1";
                     break;
                 case "8100":
                     estado = true;
                     EstadoC = "-1";
                     break;
                 case "3070":
                 case "3000":
                 case "3053":
                     estado = true;
                     EstadoC = "-1";
                     break;
                 //case "3053":
                 //    estado = true;
                 //    EstadoC = "-1";
                 //    break;
                 case "7000":
                     estado = true;
                     EstadoC = "-1";
                     break;

             }

             if (cmbTipoInstruccion.SelectedValue == "1")
             {
                 estado = true;
                 EstadoC = "2";

             } 
             if (estado)
             {
                 this.Tiempo_flujo(idInstruccion, this.cmbClientes.SelectedItem.Text, this.txtProveedores.Text, EstadoC);
             }
         
        

      
        //if (estado)
        //{
           
        //}
        //else
        //{
        //    //this.Tiempo_flujo(idInstruccion, this.cmbClientes.SelectedValue, this.txtProveedores.Text);
        //}

    }
    
    protected void Tiempo_flujo(string IdHojaRuta, string IdCliente, String Proveedor, String Estado)
    {
        try
        {


            ProcesoFlujoCargaBO NUevo_proceso = new ProcesoFlujoCargaBO(logApp);
            
            NUevo_proceso.loadflujo(IdHojaRuta);

            if (NUevo_proceso.totalRegistros == 0)
            {

                NUevo_proceso.loadflujo("-1");
                NUevo_proceso.newLine();
                NUevo_proceso.IDINSTRUCCION = IdHojaRuta;
                NUevo_proceso.CODIGOADUANA = IdHojaRuta.Substring(2, 3);
                NUevo_proceso.CLIENTE = IdCliente;
                NUevo_proceso.PROVEEDOR = Proveedor;
                NUevo_proceso.ELIMINADO = "false";
                NUevo_proceso.PERMISO = "false";
                NUevo_proceso.IDUSUARIO = Session["IdUsuario"].ToString();
                NUevo_proceso.FECHA = DateTime.Now;

                if (IdHojaRuta.Substring(2, 3)=="016" &&  cmbRegimen.SelectedValue=="1000")
                {
                    try
                    {
                        NUevo_proceso.GATEPASS = "true";
                    }
                    catch (Exception)
                    {
                        
                    }
                  
                }

                if (cmbTipoInstruccion.SelectedValue=="1")
                {
                    NUevo_proceso.CODESTADO = "2";
                    
                }
                if (Estado=="3")
                {
                    NUevo_proceso.GATEPASS = "true";
                    NUevo_proceso.REVISION_ = "0";
                    
                }
                //OBTENER EL CORRELATIVO
                NUevo_proceso.CODESTADO = Estado;
                NUevo_proceso.PAGA = "Cliente";
                NUevo_proceso.commitLine();
                NUevo_proceso.actualizar();
            }
            else
            { 
              //  NUevo_proceso=
            }
        }
        catch (Exception)
        {
        }
        finally
        {
           
        }

    }

    public int Recuperar_Oficial_Cuenta(string Sap_Cliente)
    {
        Int16 IdOficial = 2;

        try
        {
            conectar();

            ClientesBO N_cliente = new ClientesBO(logApp);
            N_cliente.loadCliente(Sap_Cliente);
            IdOficial = Convert.ToInt16(N_cliente.IDOFICIALCUENTA);
           

        }
        catch (Exception)
        {

            IdOficial= 2;
        }
       
         return IdOficial;
        
           

    

      

    }
    protected void cmbUsuarioAduana_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
       // decile a la gente de amatilloq ue cree instruciones porfa
    }

    protected void btnGuardar_Click(object sender, ImageClickEventArgs e)
    {

    }
}