﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReciboEspeciesFiscales.aspx.cs" Inherits="ReciboEspeciesFiscales" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                    //<![CDATA[
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0
                     || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }

                function openWin() {
                    var oWnd = radopen("BuscarTramite.aspx", "RadWindow1");
                    oWnd.add_close(OnClientClose);
                }           
                
            </script>
        </telerik:RadScriptBlock>
        <telerik:RadMultiPage ID="mpEspeciesFiscales" runat="server">
            <%--<input id="edCodEspecieFiscal" runat="server" type="hidden" />
            <input id="edEspecieFiscal" runat="server" type="hidden" />
            <input id="edIdRangoEspecieFiscal" runat="server" type="hidden" />--%>
            <telerik:RadPageView ID="pvEnvioEspeciesFiscales" runat="server" Width="100%">
                <div id="miDiv" runat="server" class="panelCentrado">
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="rgRangoEspeciesFiscales" runat="server" AllowFilteringByColumn="True"
                                    AllowSorting="True" AutoGenerateColumns="False" GridLines="None" Width="100%"
                                    Height="420px" OnNeedDataSource="rgRangoEspeciesFiscales_NeedDataSource" AllowPaging="True"
                                    ShowFooter="True" ShowStatusBar="True" PageSize="20" OnInit="rgRangoEspeciesFiscales_Init"
                                    OnItemCommand="rgRangoEspeciesFiscales_ItemCommand">
                                    <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                                        PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                                        Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                                    <MasterTableView DataKeyNames="IdCompra,IdMovimiento" CommandItemDisplay="TopAndBottom"
                                        NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay rangos de especies fiscales."
                                        GroupLoadMode="Client">
                                        <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                                            ShowExportToExcelButton="true" ShowExportToCsvButton="true" />
                                        <RowIndicatorColumn>
                                            <HeaderStyle Width="20px"></HeaderStyle>
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn>
                                            <HeaderStyle Width="20px"></HeaderStyle>
                                        </ExpandCollapseColumn>
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="IdMovimiento" HeaderText="Código" UniqueName="IdMovimiento"
                                                FilterControlWidth="80%">
                                                <HeaderStyle Width="120px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="IdCompra" HeaderText="Compra No." UniqueName="IdCompra"
                                                FilterControlWidth="60%">
                                                <HeaderStyle Width="80px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Pais" HeaderText="Pais" UniqueName="Pais" FilterControlWidth="70%">
                                                <HeaderStyle Width="100px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="AduanaOrigen" HeaderText="Aduana Origen" UniqueName="AduanaOrigen"
                                                FilterControlWidth="80%">
                                                <HeaderStyle Width="120px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="NombreAduana" HeaderText="Aduana" UniqueName="NombreAduana"
                                                FilterControlWidth="80%">
                                                <HeaderStyle Width="120px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EspecieFiscal" HeaderText="Especie Fiscal" UniqueName="EspecieFiscal"
                                                FilterControlWidth="80%">
                                                <HeaderStyle Width="120px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="RangoInicial" HeaderText="Rango Inicial" UniqueName="RangoInicial"
                                                FilterControlWidth="60%">
                                                <HeaderStyle Width="80px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="RangoFinal" HeaderText="Rango Final" UniqueName="RangoFinal"
                                                FilterControlWidth="60%">
                                                <HeaderStyle Width="80px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Cantidad" HeaderText="Cantidad" UniqueName="Cantidad"
                                                FilterControlWidth="50%">
                                                <HeaderStyle Width="60px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Fecha" HeaderText="Fecha Envío" UniqueName="Fecha"
                                                FilterControlWidth="70%">
                                                <HeaderStyle Width="100px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Usuario" HeaderText="Usuario" UniqueName="Usuario"
                                                FilterControlWidth="80%">
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle Width="10%" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn HeaderText="Fecha" AllowFiltering="false" ShowFilterIcon="false">
                                                <ItemTemplate>
                                                    <telerik:RadDatePicker ID="dtFechaSellado" runat="server" Width="55%" Culture="es-HN"
                                                        EnableTyping="False">
                                                        <Calendar ID="Calendar1" runat="server" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"
                                                            UseRowHeadersAsSelectors="False">
                                                        </Calendar>
                                                        <DateInput ID="DateInput1" runat="server" DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"
                                                            ReadOnly="True">
                                                        </DateInput><DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                    </telerik:RadDatePicker>
                                                    <telerik:RadTimePicker ID="dtHoraSellado" runat="server" Width="37%" Culture="es-HN"
                                                        EnableTyping="True">
                                                        <TimeView ID="TimeView2" runat="server" CellSpacing="-1" Columns="4" Culture="es-HN"
                                                            Interval="00:30:00" TimeFormat="T">
                                                        </TimeView>
                                                        <TimePopupButton HoverImageUrl="" ImageUrl="" />
                                                    </telerik:RadTimePicker>
                                                </ItemTemplate>
                                                <ItemStyle Width="16%" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn HeaderText="Observación" AllowFiltering="false" ShowFilterIcon="false">
                                                <ItemTemplate>
                                                    <telerik:RadTextBox ID="edObservacion" runat="server">
                                                    </telerik:RadTextBox>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridButtonColumn ButtonType="ImageButton" HeaderText="Sellar Estado" CommandName="Sellar"
                                                ImageUrl="~/Images/24/lock2_24.png" UniqueName="btnSellar">
                                                <ItemStyle Width="75px" />
                                                <HeaderStyle Width="75px" />
                                            </telerik:GridButtonColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <HeaderStyle Width="180px" />
                                    <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                                        <Selecting AllowRowSelect="True" />
                                        <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                                            DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                                            DropHereToReorder="Suelte aquí para Re-Ordenar" />
                                        <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
                                    </ClientSettings>
                                    <FilterMenu EnableTheming="True">
                                        <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                                    </FilterMenu>
                                    <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                                    <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </div>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </telerik:RadAjaxPanel>
</asp:Content>
