﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FileShare.aspx.cs" Inherits="Aduanas.FileShare" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="Scripts/CloseWindow.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server"></telerik:RadWindowManager>
    </form>
    <input type="hidden" id="respHidden" runat="server"/>
</body>
</html>
