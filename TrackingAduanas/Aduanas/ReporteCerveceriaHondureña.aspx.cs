﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.OleDb;
using System.Text;
using System.Configuration;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;

using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.SessionState;
using System.Collections.Specialized;
using GrupoLis.Login;
using System.Text.RegularExpressions;
using System.Security.Principal;
using Telerik.Web;
using System.Security.Cryptography;
public partial class ReporteGestionCarga : Utilidades.PaginaBase
{

    private readonly AduanasDataContext _aduanasdc = new AduanasDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        //SetGridFilterMenu(rgInstrucciones.FilterMenu);
        //rgInstrucciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        //rgInstrucciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Cerveceria Hondureña", "Reporte Cervecería Hondureña");
            cargarDatosInicio();
        }
    }

    protected void cargarDatosInicio()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            ClientesBO cl = new ClientesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (u.CODPAIS == "H")
                c.loadPaisesHojaRuta();
            else
                c.loadAllCampos("PAISES", u.CODPAIS);
            cmbPais.DataSource = c.TABLA;
            cmbPais.DataBind();
            cl.loadAllCamposClientesXPais(cmbPais.SelectedValue);
            cmbCliente.DataSource = cl.TABLA;
            cmbCliente.DataBind();
        }
        catch { }
    }

    public override bool CanGoBack { get { return false; } }

    private void Salir()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("top.location.href = LoginTracking.aspx;");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Salir", sb.ToString(), false);
    }

    protected void cmbPais_SelectedIndexChanged(object sender, Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            conectar();
            ClientesBO cl = new ClientesBO(logApp);
            cl.loadAllCamposClientesXPais(cmbPais.SelectedValue);
            cmbCliente.DataSource = cl.TABLA;
            cmbCliente.DataBind();
        }
        catch { }
    }

    #region GestionCarga
    protected void rgInstrucciones_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridInstrucciones();
    }

    private void llenarGridInstrucciones()
    {
        string fecha = dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd");
        string fechaFin = dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd");
        string pais = cmbPais.SelectedValue.ToString();
        string cliente = cmbCliente.SelectedValue.ToString();
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            //i.loadInstruccionGestioonCargaAutomaticas(dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"), cmbCliente.SelectedValue,cmbPais.SelectedValue);
            i.ReporteCerveceriaHondureña(fecha, fechaFin, pais, cliente);
            rgInstrucciones.DataSource = i.TABLA;
            rgInstrucciones.DataBind();
            
        }
        catch (Exception ex) { }
        finally
        {
            desconectar();
        }
    }

    protected void rgInstrucciones_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();
            if (rgInstrucciones.Items.Count <= 0 & e.CommandName == RadGrid.ExportToExcelCommandName)
                e.Canceled = true;
            else if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                ConfigureExport();
            }
        }
        catch (Exception)
        { }
    }

    private void ConfigureExport()
    {
        rgInstrucciones.AllowFilteringByColumn = false;
        String filename = "Reporte Cervecería" + "_" + DateTime.Now.ToShortDateString();
        rgInstrucciones.GridLines = GridLines.Both;
        rgInstrucciones.ExportSettings.FileName = filename;
        rgInstrucciones.ExportSettings.IgnorePaging = true;
        rgInstrucciones.ExportSettings.OpenInNewWindow = false;
        rgInstrucciones.ExportSettings.ExportOnlyData = true;
    }
    #endregion

    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        if (dpFechaInicio.SelectedDate < dpFechaFinal.SelectedDate)
        {
            llenarGridInstrucciones();
            rgInstrucciones.Visible = true;
        }
        else
        {
            registrarMensaje2("Fecha inicio no puede ser mayor que fecha final");
            rgInstrucciones.Visible = false;
        }
    }

    private void registrarMensaje2(string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("alert('" + mensaje + "');");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "mensaje", sb.ToString(), false);
    }

    protected void rgInstrucciones_Init(object sender, System.EventArgs e)
    {
        //GridFilterMenu menu = rgInstrucciones.FilterMenu;
        //menu.Items.RemoveAt(rgInstrucciones.FilterMenu.Items.Count - 2);
    }
}

//var query = _aduanasdc.Instrucciones
//    .Join(_aduanasdc.Clientes, x => x.IdCliente, y => y.CodigoSAP, (x, y) => new {x = x, y = y})
//    .Join(_aduanasdc.Aduanas, x => x.x.CodigoAduana, a => a.CodigoAduana, (x, a) => new {x = x, a = a})
//    .Join(_aduanasdc.RegimenesHonduras, x => x.x.x.CodRegimen, r => r.Codigo, (x, r) => new {x = x, r = r})
//    .Join(_aduanasdc.TiemposFlujoCarga.Where(w => w.IdEstado == "12"), x => x.x.x.x.IdInstruccion,
//        t => t.IdInstruccion, (x, t) => new {x = x, t = t})
//    .GroupJoin(_aduanasdc.DocumentosInstrucciones, x => x.x.x.x.x.IdInstruccion, di => di.IdInstruccion)
//.Join(_aduanasdc.Codigos.Where(w => w.Categoria == "PAISES"), x => x.x.x.x.x.x.CodPaisOrigen,
//    c1 => c1.Codigo1, (x, c1) => new {x = x, c1 = c1})
//.Join(
//    _aduanasdc.DocumentosInstrucciones.Where(
//        w => w.CodDocumento == "50" || w.CodDocumento == "52" || w.CodDocumento == "51"),
//    x => x.x.x.x.x.IdInstruccion, di => di.IdInstruccion, (x, di) => new {x = x, di = di})

//.Where(w => w.x.x.x.x.x.x.Fecha >= fecha && w.x.x.x.x.x.x.Fecha < fechaFin && w.x.x.x.x.x.x.CodPaisHojaRuta == pais
//           && w.x.x.x.x.x.x.IdCliente == cliente  && w.x.x.x.x.x.x.CodEstado != "100" /*&& w.x.x.t.IdEstado == "12"*/)
//.Select(s => new
//{
//    IdInstruccion = s.x.x.x.x.x.x.IdInstruccion,
//    Aduana = s.x.x.x.x.a.NombreAduana,
//    Cliente = s.x.x.x.x.x.y.Nombre,
//    Proveedor = s.x.x.x.x.x.x.Proveedor,
//    Producto = s.x.x.x.x.x.x.Producto,
//    Regimen = s.x.x.x.r.Descripcion,
//    Guia_BL = s.x.di.DocumentoNo,
//    Origen = s.c1.Descripcion,
//    Fecha = s.x.x.x.x.x.x.Fecha,
//    FechaLiquidacion = s.x.x.t.Fecha,
//    Factura = s.x.x.x.x.x.x.NumeroFactura,
//    NoCorrelativo = s.x.x.x.x.x.x.NoCorrelativo,
//    PosArancelaria = "",
//    Impuesto = s.x.x.x.x.x.x.ISV,
//    Flete = s.x.x.x.x.x.x.Flete,
//    ValFob = s.x.x.x.x.x.x.ValorFOB,
//    ValCif = s.x.x.x.x.x.x.ValorCIF,
//    Seguro = s.x.x.x.x.x.x.Seguro,
//    Otros = s.x.x.x.x.x.x.Otros
//}).ToList();