﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="EnvioFacturacion.aspx.cs" Inherits="CargarArchivo" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .wrap
        {
            white-space: normal;
            width: 98px;
        }
        
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server" OnAjaxRequest="RadAjaxManager1_AjaxRequest"
        ClientEvents-OnRequestStart="requestStart">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgIngresos">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgIngresos" LoadingPanelID="LoadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="RadWindowManager1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadAjaxManager1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgIngresos" LoadingPanelID="RadAjaxLoadingPanel1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" EnableSkinTransparency="true">
        <div class="loading">
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Ajax/Img/loading1.gif" AlternateText="loading">
            </asp:Image>
        </div>
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
        <script type="text/javascript">
            //<![CDATA[
            function requestStart(sender, args) {
                if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                    args.set_enableAjax(false);
                }
            }

            function OnClientClose() {
                $find("<%= RadAjaxManager1.ClientID%>").ajaxRequest("Rebind");
            }
        </script>
    </telerik:RadScriptBlock>
    <telerik:RadMultiPage ID="mpRequerimiento" runat="server" SelectedIndex="0">
        <telerik:RadPageView ID="RadPageView1" runat="server" Width="99.9%">
            <table width="100%">
                <tr>
                    <td style="width: 100%">
                        <asp:Label ID="lblIngresos" runat="server" Text="Cargar" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input id="IdTransaccion" runat="server" type="hidden" />
                        <telerik:RadGrid ID="rgFacturacion" runat="server" 
                            AllowFilteringByColumn="True" AllowSorting="True"
                            AutoGenerateColumns="False" GridLines="None" Width="99.9%" Height="420px" OnNeedDataSource="rgIngresos_NeedDataSource"
                            AllowPaging="True" ShowFooter="True" ShowStatusBar="True" PageSize="1000" CellSpacing="0" Culture="es-ES">
                            <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                                PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                                Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                            <MasterTableView DataKeyNames="" CommandItemDisplay="Top" NoDetailRecordsText="No hay registros."
                                NoMasterRecordsText="No hay ingresos." GroupLoadMode="Client">
                                <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                                    ShowExportToExcelButton="true" ShowExportToCsvButton="false" />
                                <RowIndicatorColumn>
                                    <HeaderStyle Width="20px"></HeaderStyle>
                                </RowIndicatorColumn>
                                <ExpandCollapseColumn>
                                    <HeaderStyle Width="20px"></HeaderStyle>
                                </ExpandCollapseColumn>
                                <Columns>
                                    <telerik:GridTemplateColumn AllowFiltering="False" FilterControlAltText="Filter TemplateColumn1 column"
                                        FilterControlWidth="75px" HeaderText="Enviar" ShowFilterIcon="False" UniqueName="TemplateColumn1">
                                        <ItemTemplate>
                                            <telerik:RadButton ID="btnEnviar" runat="server" OnClick="btnEnviar_Click" Text="Enviar">
                                            </telerik:RadButton>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn AllowFiltering="False" FilterControlAltText="Filter TemplateColumn column"
                                        ShowFilterIcon="False" UniqueName="TemplateColumn">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chbAgrupar" runat="server" Text="Seleccionar"  
                                                Enabled='<%# (Convert.ToDecimal(Eval("Diferencia").ToString()) >= 1 ?  false:true) %>'
                                                   
                                                 />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn DataField="HojaRuta"
                                         FilterControlAltText="Filter FechaPago column" FilterControlWidth="75px"
                                         HeaderText="Hoja de Ruta" UniqueName="HojaRuta" 
                                        >

                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn AllowFiltering="False" DataField="Cliente" FilterControlAltText="Filter Cliente column" HeaderText="Cliente" UniqueName="Cliente">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn AllowFiltering="False" DataField="Aduana" FilterControlAltText="Filter Aduana column" HeaderText="Aduana" UniqueName="Aduana">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn AllowFiltering="False" DataField="TotalGastos" FilterControlAltText="Filter MedioPago column" FilterControlWidth="75px" HeaderText="Total Costo" ShowFilterIcon="False" UniqueName="TotalGastos">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn AllowFiltering="False" DataField="TotalComprobado" FilterControlAltText="Filter HojaRuta column" FilterControlWidth="75px" HeaderText="Total Doc Recuperado" ShowFilterIcon="False" UniqueName="TotalComprobado">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn AllowFiltering="False" DataField="Diferencia" FilterControlAltText="Filter Cliente column" FilterControlWidth="75px" HeaderText="Diferencia" ShowFilterIcon="False" UniqueName="Diferencia">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn AllowFiltering="False" DataField="Pendiente" FilterControlAltText="Filter Id column" FilterControlWidth="75px" HeaderText="Gastos Pendientes" ShowFilterIcon="False" UniqueName="Pendiente">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn AllowFiltering="False" DataField="TotalGastosDolares" FilterControlAltText="Filter Material column" FilterControlWidth="75px" HeaderText="Total Costos Dolares" ShowFilterIcon="False" UniqueName="TotalGastosDolares">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn AllowFiltering="False" DataField="TotalDolaresComprobadoDolares" FilterControlAltText="Filter CuentaSAP column" FilterControlWidth="75px" HeaderText="Total Docs Comprobado Dolares" ShowFilterIcon="False" UniqueName="TotalDolaresComprobadoDolares">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn AllowFiltering="False" DataField="DiferenciaDolares" FilterControlAltText="Filter Monto column" FilterControlWidth="75px" HeaderText="Diferencia Dolares" ShowFilterIcon="False" UniqueName="DiferenciaDolares">
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <EditFormSettings>
                                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                    </EditColumn>
                                </EditFormSettings>
                            </MasterTableView>
                            <HeaderStyle Width="180px" />
                            <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                                <Selecting AllowRowSelect="True" />
                                <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                                    DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                                    DropHereToReorder="Suelte aquí para Re-Ordenar" />
                                <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
                            </ClientSettings>
                            <FilterMenu EnableTheming="True">
                                <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                            </FilterMenu>
                            <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                            <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
                        </telerik:RadGrid>
                        <br />
                        <br />
                        <br />
                        <br />
                        <table width="100%">
                            <tr align="center">
                                <td align="center">
                                    <asp:ImageButton ID="btnGuardar" runat="server" 
                                        ImageUrl="~/Images/24/disk_blue_ok_24.png" 
                                        OnClientClick="radconfirm('Esta seguro que desea enviar a facturar?',confirmCallBackSalvar, 300, 100); return false;" 
                                        ToolTip="Salvar" onclick="btnGuardar_Click1"/>
                                    
                                </td>
                            </tr>
                        </table>
                        <br />
                        <br />
                    </td>
                </tr>
            </table>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <script type="text/javascript">
        function onRequestStart(sender, args) {
            if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 ||
                            args.get_eventTarget().indexOf("ExportToWordButton") >= 0 ||
                            args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                args.set_enableAjax(false);
            }
        }
        var message = "Grupo Vesta, Derechos Reservados";

        function click(e) {
            if (document.all) {
                if (event.button == 2) {
                    alert(message);
                    return false;
                }
            }
            if (document.layers) {
                if (e.which == 3) {
                    alert(message);
                    return false;
                }
            }
        }
        if (document.layers) {
            document.captureEvents(Event.MOUSEDOWN);
        }
        document.onmousedown = click;

        function GetRadWindow() {
            var oWindow = null;
            if (window.radWindow) oWindow = window.radWindow;
            else if (window.frameElement.radWindow) oWindow = window.frameElement.radWindow;
            return oWindow;
        }

        function Close() {
            var oWindow = GetRadWindow();
            oWindow.argument = null;
            oWindow.close();
        }
    </script>
</asp:Content>
