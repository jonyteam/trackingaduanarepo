﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Telerik.Web.UI;
using System.Text.RegularExpressions;
using System.Threading;
using Excel2 = Microsoft.Office.Interop.Excel;
using System.Web.UI;
//using Excel;

public partial class FcCargarArchivoSelloTiempos : Utilidades.PaginaBase
{
    AduanasDataContext _aduana = new AduanasDataContext();
    protected string Filename = "";

    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Cargar Archivo Sello Tiempos";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Filename = ParameterForm1("filename", "");
        RadProgressArea1.Localization.Uploaded = "Total Progress";
        RadProgressArea1.Localization.UploadedFiles = "Progress";
        if (IsPostBack) return;
        var paginaMasterBase = (SiteMaster)Master;
        if (paginaMasterBase != null) paginaMasterBase.SetTitulo("Cargar Archivo Sello Tiempos", "Cargar Archivo Sello Tiempos");
        if (!Ingresar)
            redirectTo("Default.aspx");
    }

    private bool Ingresar { get { return tienePermiso("Ingresar"); } }

    private string ParameterForm1(string name, string startup)
    {
        if (Page.Request.Form.Get(name) != null && Page.Request.Form.Get(name).Length > 0)
            if (!Regex.IsMatch(Page.Request.Form.Get(name), @"^[\w-=,. ]{1,150}$"))
                throw new ArgumentException("Invalid name parameter");
        return Page.Request.Form.Get(name) ?? startup;
    }

    protected void btnCargar_Click(object sender, EventArgs e)
    {
        try
        {
            LabelMensajes.Visible = false;
            if (FileUpload.UploadedFiles.Count > 0)
            {
                var file = FileUpload.UploadedFiles[0];
                if (file.ContentLength <= FileUpload.MaxFileSize)
                {
                    if (!Equals(file, null))
                    {
                        LooongMethodWhichUpdatesTheProgressContext(file);
                    }

                    repeaterResults.DataSource = FileUpload.UploadedFiles;
                    repeaterResults.DataBind();
                    repeaterResults.Visible = true;
                    Filename = file.GetName();

                    labelNoResults.Text = "";
                    ReadExcel(file.InputStream);
                }
            }
            else
            {
                if (FileUpload.InvalidFiles.Count > 0)
                {
                    repeaterResults.Visible = true;
                    repeaterResults.DataSource = FileUpload.InvalidFiles;
                    repeaterResults.DataBind();
                    var lbl = (Label)repeaterResults.Controls[0].FindControl("lblTitulo");
                    lbl.Text = "Archivo Invalido:";
                }
                else
                {
                    repeaterResults.Visible = false;
                    registrarMensaje("Debe Seleccionar el archivo a cargar");
                }
                Filename = "";
            }
        }
        catch (Exception ex)
        {
            //LogErrorOrden(ex.Message, Session["IdUsuario"].ToString(), "Cargar un Archivo de Excel");
            registrarMensaje(ex.Message);
        }
    }

    //private void ReadExcel(Stream fileStream)
    //{
    //    var fecha = DateTime.Now;
    //    var lst = DocumentApi.Spreadsheet.ReadExcel.Document<ArchivoSelloTiemposVm>(fileStream);
    //    foreach (var ast in lst)
    //    {
    //        var archivoSelloTiempo = new ArchivoSelloTiempos
    //        {
    //            IdInstruccion = ast.IdInstruccion,
    //            FacturaNo = ast.FacturaNo,
    //            Referencia = ast.Referencia,
    //            Cliente = ast.Cliente,
    //            Aprobado = ast.Aprobado,
    //            Correlativo = ast.Correlativo,
    //            IdTransaccion = fecha.ToString(),
    //            Fecha = fecha,
    //            Eliminado = false,
    //            IdUsuario = Convert.ToDecimal(Session["IdUsuario"].ToString()),
    //        };
    //        _aduana.ArchivoSelloTiempos.InsertOnSubmit(archivoSelloTiempo);

    //        //sellar Validación Electrónica (12) y Canal de Selectividad (2)

    //        //var idInstruccion = _aduana.TiemposFlujoCarga.FirstOrDefault(w => w.IdInstruccion == ast.IdInstruccion);
    //        var estados = new List<string> { "12", "2" };
    //        var idInstruccion = _aduana.TiemposFlujoCarga.FirstOrDefault(w => w.IdInstruccion == ast.IdInstruccion && estados.Contains(w.IdEstado));
    //        if (idInstruccion != null) continue;
    //        var tfc = new TiemposFlujoCarga
    //        {
    //            IdInstruccion = ast.IdInstruccion,
    //            IdEstado = "12",
    //            Fecha = ast.Aprobado,
    //            FechaIngreso = fecha,
    //            Observacion = "Archivo Sello Tiempos",
    //            Eliminado = false,
    //            IdUsuario = Convert.ToDecimal(Session["IdUsuario"].ToString()),
    //        };
    //        _aduana.TiemposFlujoCarga.InsertOnSubmit(tfc);
    //        var tfc2 = new TiemposFlujoCarga
    //        {
    //            IdInstruccion = ast.IdInstruccion,
    //            IdEstado = "2",
    //            Fecha = ast.Aprobado,
    //            FechaIngreso = fecha,
    //            Observacion = "Archivo Sello Tiempos",
    //            Eliminado = false,
    //            IdUsuario = Convert.ToDecimal(Session["IdUsuario"].ToString()),
    //        };
    //        _aduana.TiemposFlujoCarga.InsertOnSubmit(tfc2);

    //        //var pfc = new ProcesoFlujoCarga
    //        //{
    //        //    IdInstruccion = ast.IdInstruccion,
    //        //    CodigoAduana = ast.IdInstruccion.Substring(2, 3),
    //        //    Cliente = ast.Cliente,
    //        //    Correlativo = ast.Correlativo,
    //        //    Paga = "Vesta",
    //        //    CodEstado = "2",
    //        //    Fecha = DateTime.Now,
    //        //    Eliminado = false,
    //        //    IdUsuario = Convert.ToDecimal(Session["IdUsuario"].ToString())
    //        //};

    //        var proceso = _aduana.ProcesoFlujoCarga.FirstOrDefault(w => w.IdInstruccion == ast.IdInstruccion);
    //        if (proceso == null)
    //        {
    //            var pfc = new ProcesoFlujoCarga
    //            {
    //                IdInstruccion = ast.IdInstruccion,
    //                CodigoAduana = ast.IdInstruccion.Substring(2, 3),
    //                Cliente = ast.Cliente,
    //                Correlativo = ast.Correlativo,
    //                Paga = "Vesta",
    //                CodEstado = "2",
    //                Fecha = DateTime.Now,
    //                Eliminado = false,
    //                IdUsuario = Convert.ToDecimal(Session["IdUsuario"].ToString())
    //            };
    //            _aduana.ProcesoFlujoCarga.InsertOnSubmit(pfc);
    //        }
    //        else
    //        {
    //            proceso.Cliente = ast.Cliente;
    //            proceso.Correlativo = ast.Correlativo;
    //            //var ast1 = ast;
    //            //_aduana.ProcesoFlujoCargas.Where(w => w.IdInstruccion == ast1.IdInstruccion)
    //            //    .Update(w => new ProcesoFlujoCarga
    //            //    {
    //            //        Cliente = ast1.Cliente,
    //            //        Correlativo = ast1.Correlativo,
    //            //    });
    //        }




    //       // _aduana.ProcesoFlujoCarga.InsertOnSubmit(pfc);
    //    }

    //    _aduana.SubmitChanges();
    //    RegistrarMensaje2("Tiempos registrados exitosamente");
    //}/*Sello de Tempos Viojo*/
    private void ReadExcel(Stream fileStream)
    {
        var fecha = DateTime.Now;
        var lst = DocumentApi.Spreadsheet.ReadExcel.Document<ArchivoSelloTiemposVm>(fileStream);

        foreach (var ast in lst)
        {
            var archivoSelloTiempo = new ArchivoSelloTiempo
            {
                IdInstruccion = ast.IdInstruccion,
                FacturaNo = ast.FacturaNo,
                Referencia = ast.Referencia,
                Cliente = ast.Cliente,
                Aprobado = ast.Aprobado,
                Correlativo = ast.Correlativo,
                IdTransaccion = fecha.ToString(),
                Fecha = fecha,
                Eliminado = false,
                IdUsuario = Convert.ToDecimal(Session["IdUsuario"].ToString()),
                CanalSelectividad = ast.CanalSelectividad,
            };
            _aduana.ArchivoSelloTiempo.InsertOnSubmit(archivoSelloTiempo);

            //sellar Validación Electrónica (12) y Canal de Selectividad (2)
            var estados = new List<string> { "12", "2" };
            var idInstruccion = _aduana.TiemposFlujoCarga.FirstOrDefault(w => w.IdInstruccion == ast.IdInstruccion && estados.Contains(w.IdEstado));
            if (idInstruccion != null) continue;
            var tfc = new TiemposFlujoCarga
            {
                IdInstruccion = ast.IdInstruccion,
                IdEstado = "12",
                Fecha = ast.Aprobado,
                FechaIngreso = fecha,
                Observacion = "Archivo Sello Tiempos",
                Eliminado = false,
                IdUsuario = Convert.ToDecimal(Session["IdUsuario"].ToString()),
            };
            _aduana.TiemposFlujoCarga.InsertOnSubmit(tfc);
            var tfc2 = new TiemposFlujoCarga
            {
                IdInstruccion = ast.IdInstruccion,
                IdEstado = "2",
                Fecha = ast.Aprobado,
                FechaIngreso = fecha,
                Observacion = "Archivo Sello Tiempos",
                Eliminado = false,
                IdUsuario = Convert.ToDecimal(Session["IdUsuario"].ToString()),
            };
            _aduana.TiemposFlujoCarga.InsertOnSubmit(tfc2);

            var proceso = _aduana.ProcesoFlujoCarga.FirstOrDefault(w => w.IdInstruccion == ast.IdInstruccion);
            if (proceso == null)
            {
                var pfc = new ProcesoFlujoCarga
                {
                    IdInstruccion = ast.IdInstruccion,
                    CodigoAduana = ast.IdInstruccion.Substring(2, 3),
                    Cliente = ast.Cliente,
                    Correlativo = ast.Correlativo,
                    Paga = "Vesta",
                    CodEstado = "2",
                    Fecha = DateTime.Now,
                    Eliminado = false,
                    IdUsuario = Convert.ToDecimal(Session["IdUsuario"].ToString()),
                    CanalSelectividad = ast.CanalSelectividad,
                };
                _aduana.ProcesoFlujoCarga.InsertOnSubmit(pfc);
            }
            else
            {
                proceso.Cliente = ast.Cliente;
                proceso.Correlativo = ast.Correlativo;
                proceso.CanalSelectividad = ast.CanalSelectividad;
            }
        }

        _aduana.SubmitChanges();
        RegistrarMensaje2("Tiempos registrados exitosamente");
    }
    private void LooongMethodWhichUpdatesTheProgressContext(UploadedFile file)
    {
        try
        {
            const int total = 100;
            var progress = RadProgressContext.Current;
            for (int i = 0; i < total; i++)
            {
                progress.PrimaryTotal = 1;
                progress.PrimaryValue = 1;
                progress.PrimaryPercent = 100;

                progress.SecondaryTotal = total;
                progress.SecondaryValue = i;
                progress.SecondaryPercent = i;

                progress.CurrentOperationText = file.GetName() + " se está procesando...";

                if (!Response.IsClientConnected)
                {
                    //Cancel button was clicked or the browser was closed, so stop processing
                    break;
                }
                //Stall the current thread for 0.1 seconds
                Thread.Sleep(100);
            }
        }
        catch (Exception ex)
        {
            //LogErrores(ex.Message, Session["IdUsuario"].ToString(), "Cargar Ordenes Compra");
            registrarMensaje(ex.Message);
        }
    }

    private void RegistrarMensaje2(string mensaje)
    {
        var sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("alert('" + mensaje + "');");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "mensaje", sb.ToString(), false);
    }
}
