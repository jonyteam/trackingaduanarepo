﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.OleDb;

using Telerik.Web.UI;
using Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using GrupoLis.Ebase;
using GrupoLis.Login;
using System.Collections.Specialized;
using System.Configuration;

public partial class GenerarArchivoSAPDinant : Utilidades.PaginaBase
{
    private GrupoLis.Login.Login logAppAduanas;
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "ReporteHagamodaPromoda";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Hagamoda/Promoda", "Reporte Dinant");
            DateTime baseDate = DateTime.Today;
            var thisMonthStart = baseDate.AddDays(1 - baseDate.Day);
            var thisMonthEnd = thisMonthStart.AddMonths(1).AddSeconds(-1);
            dpFechaInicio.SelectedDate = thisMonthStart;
            dpFechaFinal.SelectedDate = thisMonthEnd;
            cargarDatosInicio();
        }
    }
    protected bool Consultar { get { return tienePermiso("Consultar"); } }

    private void ConfigureExport()
    {
        String filename = "Reporte_" + DateTime.Now.ToShortDateString();
        rgReporte.ExportSettings.FileName = filename;
        rgReporte.ExportSettings.ExportOnlyData = true;
        rgReporte.ExportSettings.IgnorePaging = true;
        rgReporte.ExportSettings.OpenInNewWindow = true;
        rgReporte.MasterTableView.ExportToExcel();
    }

    public override bool CanGoBack { get { return false; } }

    protected void cargarDatosInicio()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            ClientesBO cl = new ClientesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (u.CODPAIS == "H")
                c.loadPaisesHojaRuta();
            else
                c.loadAllCampos("PAISES", u.CODPAIS);
            cmbPais.DataSource = c.TABLA;
            cmbPais.DataBind();
            cl.loadAllCamposClientesXPais(cmbPais.SelectedValue);
            cmbCliente.DataSource = cl.TABLA;
            cmbCliente.DataBind();
        }
        catch { }
    }
    private void llenarGrid()
    {
           try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            i.ReporteDinant(dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"), cmbCliente.SelectedValue, cmbPais.SelectedValue.Substring(0, 1));
            rgReporte.DataSource = i.TABLA;
            rgReporte.DataBind();
            btnGuardar.Visible = true;
        }
        catch (Exception) { }
        finally { desconectar(); }
    }
    #region Conexion
    private void conectarAduanas( string Pais)
    {
        if (logAppAduanas == null)
            logAppAduanas = new GrupoLis.Login.Login();
        if (!logAppAduanas.conectado)
        {
            logAppAduanas.tipoConexion = TipoConexion.SQL_SERVER;
            logAppAduanas.SERVIDOR = mParamethers.Get("Servidor2");
            if (Pais.ToString() == "S")
            {
                logAppAduanas.DATABASE = mParamethers.Get("CajaChicaSV");
            }
            if (Pais.ToString() == "N")
            {
                logAppAduanas.DATABASE = mParamethers.Get("CajaChicaNI");
            }
            if (Pais.ToString() == "G")
            {
                logAppAduanas.DATABASE = mParamethers.Get("CajaChicaGT");
            }
            logAppAduanas.USER = mParamethers.Get("User2");
            logAppAduanas.PASSWD = mParamethers.Get("Password2");
            logAppAduanas.conectar();
        }
    }

    private void desconectarAduanas()
    {
        if (logAppAduanas.conectado)
            logAppAduanas.desconectar();
    }
    #endregion
    protected void cmbPais_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            conectar();
            ClientesBO cl = new ClientesBO(logApp);
            cl.loadAllCamposClientesXPais(cmbPais.SelectedValue);
            cmbCliente.DataSource = cl.TABLA;
            cmbCliente.DataBind();
        }
        catch { }
    }
    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        writeToExcel();
    }
    protected void writeToExcel()
    {
        if (rgReporte.Items.Count >= 0)
        {
            btnGuardar.Visible = false;
            string nameFile = "DINANTTemp.xlsx";
            string nameDest = "DINANT.xlsx";
            bool hayRegistro = false;
            Application.Lock();
            if (File.Exists("J:\\Files1\\Aduanas\\" + nameDest))
            {
                File.Delete("J:\\Files1\\Aduanas\\" + nameDest);
                File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
            }
            else
            {
                File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
            }

            //DIRECCION DE LA PLANTILLA
            var mTemplateFileName = @"J:\Files1\Aduanas\" + nameDest;

            var xlApp = new ApplicationClass() { DisplayAlerts = false, AskToUpdateLinks = false };
            var workbooks = xlApp.Workbooks;
            var workbook = workbooks.Open(mTemplateFileName, 2, false);

            try
            {
                conectar();
                #region Factura comisión
                //HOJA DE LA PLANTILLA       
                const string mInputWorkSheetName = "Datos";   //Hoja1
                //POSICIONAR LA HOJA A TRABAJAR
                //var inputWorkSheet = workBook.Worksheets.Cast<Excel.Worksheet>().Where(w => w.Name == m_InputWorkSheetName).FirstOrDefault();
                
                var inputWorkSheet = workbook.Worksheets.Cast<Worksheet>().FirstOrDefault(w => w.Name == mInputWorkSheetName);

                
                int pos = 11;
                foreach (GridDataItem gridItem in rgReporte.Items)
                {
                    pos = pos + 1;
                    
                    hayRegistro = true;

                    if (gridItem.Cells[6].Text == "&nbsp;")
                    {
                        gridItem.Cells[6].Text = "";
                    }
                    if (gridItem.Cells[10].Text == "&nbsp;")
                    {
                        gridItem.Cells[10].Text = "";
                    }
                    if (gridItem.Cells[14].Text == "&nbsp;")
                    {
                        gridItem.Cells[14].Text = "";
                    }
                    if (gridItem.Cells[16].Text == "&nbsp;")
                    {
                        gridItem.Cells[16].Text = "";
                    }
                    inputWorkSheet.Cells.Range["A" + pos].Value = gridItem.Cells[2].Text;//Aduana
                    inputWorkSheet.Cells.Range["B" + pos].Value = gridItem.Cells[3].Text;//Tipo Importacion
                    inputWorkSheet.Cells.Range["C" + pos].Value = gridItem.Cells[4].Text;//Division
                    inputWorkSheet.Cells.Range["D" + pos].Value = gridItem.Cells[5].Text;//Hoja de Ruta
                    inputWorkSheet.Cells.Range["E" + pos].Value = gridItem.Cells[6].Text;//Ordenes
                    inputWorkSheet.Cells.Range["F" + pos].Value = gridItem.Cells[7].Text;//Proveedor
                    inputWorkSheet.Cells.Range["G" + pos].Value = gridItem.Cells[8].Text;//Producto
                    inputWorkSheet.Cells.Range["H" + pos].Value = gridItem.Cells[9].Text;//ETA Aduana
                    inputWorkSheet.Cells.Range["I" + pos].Value = gridItem.Cells[10].Text;//Arribo de la Carga
                    inputWorkSheet.Cells.Range["J" + pos].Value = gridItem.Cells[11].Text;//ETA Planta
                    inputWorkSheet.Cells.Range["K" + pos].Value = gridItem.Cells[12].Text;//No. Factura
                    inputWorkSheet.Cells.Range["L" + pos].Value = gridItem.Cells[13].Text;//Pais Origen
                    inputWorkSheet.Cells.Range["M" + pos].Value = gridItem.Cells[14].Text;//Naviera/Curier
                    inputWorkSheet.Cells.Range["N" + pos].Value = gridItem.Cells[15].Text;//# Contenedor
                    inputWorkSheet.Cells.Range["O" + pos].Value = gridItem.Cells[16].Text;//BL/Guia/Carta Porte
                    inputWorkSheet.Cells.Range["P" + pos].Value = gridItem.Cells[17].Text;//Cantidad Bultos
                    inputWorkSheet.Cells.Range["Q" + pos].Value = gridItem.Cells[18].Text;//Peso en Kilos
                    inputWorkSheet.Cells.Range["R" + pos].Value = gridItem.Cells[19].Text;//Fecha Recibo Copias Email
                    inputWorkSheet.Cells.Range["S" + pos].Value = gridItem.Cells[20].Text;//Fecha Recibo Originales
                    inputWorkSheet.Cells.Range["T" + pos].Value = gridItem.Cells[21].Text;//Status
                    inputWorkSheet.Cells.Range["U" + pos].Value = gridItem.Cells[22].Text;//Observaciones
                    inputWorkSheet.Cells.Range["V" + pos].Value = gridItem.Cells[23].Text;//Comentarios
                    inputWorkSheet.Cells.Range["Z" + pos].Value = gridItem.Cells[24].Text;//Color 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                }
                rgReporte.Rebind();
                #endregion
                workbook.Save();

                //llenarBitacora("Se generó la carga " + idCarga, Session["IdUsuario"].ToString());
            }
            catch (Exception ex)
            {
                //LogError(ex.Message, Session["IdUsuario"].ToString(), "GenerarArchivoSAPDinant");
            }
            finally
            {
                //CERRAR EL LIBRO DE TRABAJO y la aplicacion

                workbook.Close();
                xlApp.Quit();
                desconectar();
                Application.UnLock();
            }
            if (hayRegistro)
            {
                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + nameDest);
                Response.Charset = "";
                Response.TransmitFile("J:\\Files1\\Aduanas\\" + nameDest);
                Response.Flush();
                Response.End();
                
            }
            llenarGrid();   
        }
    }   
    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        if (dpFechaInicio.SelectedDate <= dpFechaFinal.SelectedDate)
        {

            llenarGrid();
            rgReporte.Visible = true;
        }
        else
            registrarMensaje("Fecha inicio no puede ser mayor que fecha final");
    }
}
