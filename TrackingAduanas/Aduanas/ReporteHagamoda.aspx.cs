﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.OleDb;

using Telerik.Web.UI;
using Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using GrupoLis.Ebase;
using GrupoLis.Login;
using System.Collections.Specialized;
using System.Configuration;


public partial class GenerarArchivoSAPDinant : Utilidades.PaginaBase
{
    private GrupoLis.Login.Login logAppAduanas;
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "ReporteHagamodaPromoda";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Hagamoda/Promoda", "Reporte Hagamoda/Promoda");
            cargarDatosInicio();
        }
    }
    protected void cargarDatosInicio()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            ClientesBO cl = new ClientesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (u.CODPAIS == "H")
                c.loadPaisesHojaRuta();
            else
                c.loadAllCampos("PAISES", u.CODPAIS);
            cmbPais.DataSource = c.TABLA;
            cmbPais.DataBind();
            cl.loadAllCamposClientesXPais(cmbPais.SelectedValue);
            cmbCliente.DataSource = cl.TABLA;
            cmbCliente.DataBind();
        }
        catch { }
    }

    protected bool Consultar { get { return tienePermiso("Consultar"); } }

    private void ConfigureExport()
    {
        String filename = "Reporte_" + DateTime.Now.ToShortDateString();
        rgReporte.ExportSettings.FileName = filename;
        rgReporte.ExportSettings.ExportOnlyData = true;
        rgReporte.ExportSettings.IgnorePaging = true;
        rgReporte.ExportSettings.OpenInNewWindow = true;
        rgReporte.MasterTableView.ExportToExcel();
    }

    public override bool CanGoBack { get { return false; } }


    private void llenarGrid()
    {
           try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            i.ReporteHagamoda2Pagina1(dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"), cmbCliente.SelectedValue, cmbPais.SelectedValue);
            rgReporte.DataSource = i.TABLA;
            rgReporte.DataBind();
            btnGuardar.Visible = true;
        }
        catch (Exception) { }
        finally { desconectar(); }
    }

    protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
    {
    }
    protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        //llenarGrid();
    }
    protected void RadGrid1_ItemDataBound(object sender, GridItemEventArgs e)
    {
    }
    #region Conexion
    private void conectarAduanas( string Pais)
    {
        if (logAppAduanas == null)
            logAppAduanas = new GrupoLis.Login.Login();
        if (!logAppAduanas.conectado)
        {
            logAppAduanas.tipoConexion = TipoConexion.SQL_SERVER;
            logAppAduanas.SERVIDOR = mParamethers.Get("Servidor2");
            if (Pais.ToString() == "S")
            {
                logAppAduanas.DATABASE = mParamethers.Get("CajaChicaSV");
            }
            if (Pais.ToString() == "N")
            {
                logAppAduanas.DATABASE = mParamethers.Get("CajaChicaNI");
            }
            if (Pais.ToString() == "G")
            {
                logAppAduanas.DATABASE = mParamethers.Get("CajaChicaGT");
            }
            logAppAduanas.USER = mParamethers.Get("User2");
            logAppAduanas.PASSWD = mParamethers.Get("Password2");
            logAppAduanas.conectar();
        }
    }

    private void desconectarAduanas()
    {
        if (logAppAduanas.conectado)
            logAppAduanas.desconectar();
    }
    #endregion

    protected void RadGrid1_UpdateCommand(object source, Telerik.Web.UI.GridCommandEventArgs e)
    {
    }
    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        writeToExcel();
    }
    protected void writeToExcel()
    {
        if (rgReporte.Items.Count >= 0)
        {
            btnGuardar.Visible = false;
            string nameFile = "SeguimientoDiarioHagamodaTemp.xlsx";
            string nameDest = "SeguimientoDiarioHagamoda.xlsx";
            bool hayRegistro = false;
            Application.Lock();
            if (File.Exists("J:\\Files1\\Aduanas\\" + nameDest))
            {
                File.Delete("J:\\Files1\\Aduanas\\" + nameDest);
                File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
            }
            else
            {
                File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
            }

            //DIRECCION DE LA PLANTILLA
            var mTemplateFileName = @"J:\Files1\Aduanas\" + nameDest;

            var xlApp = new ApplicationClass() { DisplayAlerts = false, AskToUpdateLinks = false };
            var workbooks = xlApp.Workbooks;
            var workbook = workbooks.Open(mTemplateFileName, 2, false);

            try
            {
                conectar();
                #region Plantilla
                //HOJA DE LA PLANTILLA       
                const string mInputWorkSheetName = "Envios en Proceso";   //Hoja1
                //POSICIONAR LA HOJA A TRABAJAR
                //var inputWorkSheet = workBook.Worksheets.Cast<Excel.Worksheet>().Where(w => w.Name == m_InputWorkSheetName).FirstOrDefault();
                
                var inputWorkSheet = workbook.Worksheets.Cast<Worksheet>().FirstOrDefault(w => w.Name == mInputWorkSheetName);

                string HojaRuta = "", Proveedor = "", Cliente = "", GuiaAerea = "", FacturaComercial = "", Bultos = "", KGS = "", PesoMiami = "", PesoISwissport = "", PesoSSwissport = "";
                string Producto = "", ETA = "", ATA = "", Preliquidacion = "", AprovacionPreLiquidacion = "", Liquidacion = "", PagoLiquidacion = "", Canal = "", FechaRenvision = "", FechaLiberacion = "";
                string FechaDespacho = "", Comentarios = "", EstadoActual = "", IngresoDeposito="", Aforador ="",TiempoProceso1= "", TiempoProceso2="";
                int pos = 4;
                foreach (GridDataItem gridItem in rgReporte.Items)
                {
                    pos = pos + 1;
                    
                    hayRegistro = true;
                    HojaRuta = gridItem.Cells[2].Text;
                    Proveedor = gridItem.Cells[3].Text;
                    Cliente = gridItem.Cells[4].Text;
                    GuiaAerea = gridItem.Cells[5].Text;
                    if (GuiaAerea == "&nbsp;")
                    { GuiaAerea = ""; }
                    FacturaComercial = gridItem.Cells[6].Text;
                    Bultos = gridItem.Cells[7].Text;
                    KGS = gridItem.Cells[8].Text;
                    PesoMiami = gridItem.Cells[9].Text;
                    if (PesoMiami == "&nbsp;")
                    { PesoMiami = ""; }
                    PesoISwissport = gridItem.Cells[10].Text;
                    if (PesoISwissport == "&nbsp;")
                    { PesoISwissport = ""; }
                    PesoSSwissport = gridItem.Cells[11].Text;
                    if (PesoSSwissport == "&nbsp;")
                    { PesoSSwissport = ""; }
                    Producto = gridItem.Cells[12].Text;
                    ETA = gridItem.Cells[13].Text;
                    ATA = gridItem.Cells[14].Text;
                    if (ATA == "&nbsp;")
                    { ATA = ""; }
                    IngresoDeposito = gridItem.Cells[15].Text;
                    if (IngresoDeposito == "&nbsp;")
                    { IngresoDeposito = ""; }
                    Preliquidacion = gridItem.Cells[16].Text;
                    if (Preliquidacion == "&nbsp;")
                    { Preliquidacion = ""; }
                    AprovacionPreLiquidacion = gridItem.Cells[17].Text;
                    if (AprovacionPreLiquidacion == "&nbsp;")
                    { AprovacionPreLiquidacion = ""; }
                    Liquidacion = gridItem.Cells[18].Text;
                    if (Liquidacion == "&nbsp;")
                    { Liquidacion = ""; }
                    PagoLiquidacion = gridItem.Cells[19].Text;
                    if (PagoLiquidacion == "&nbsp;")
                    { PagoLiquidacion = ""; }
                    Canal = gridItem.Cells[20].Text;
                    if (Canal == "&nbsp;")
                    { Canal = ""; }
                    FechaRenvision = gridItem.Cells[21].Text;
                    if (FechaRenvision == "&nbsp;")
                    { FechaRenvision = ""; }
                    FechaLiberacion = gridItem.Cells[22].Text;
                    if (FechaLiberacion == "&nbsp;")
                    { FechaLiberacion = ""; }
                    FechaDespacho = gridItem.Cells[23].Text;
                    if (FechaDespacho == "&nbsp;")
                    { FechaDespacho = ""; }
                    Comentarios = gridItem.Cells[24].Text;
                    if (Comentarios == "&nbsp;")
                    { Comentarios = ""; }
                    EstadoActual = gridItem.Cells[25].Text;
                    Aforador = gridItem.Cells[26].Text;
                    if (Aforador == "&nbsp;")
                    { Aforador = ""; }
                    TiempoProceso1 = gridItem.Cells[27].Text;
                    if (TiempoProceso1 == "&nbsp;")
                    { TiempoProceso1 = ""; }
                    TiempoProceso2 = gridItem.Cells[28].Text;
                    if (TiempoProceso2 == "&nbsp;")
                    { TiempoProceso2 = ""; }

                    inputWorkSheet.Cells.Range["A" + pos].Value = HojaRuta;
                    inputWorkSheet.Cells.Range["B" + pos].Value = Proveedor;
                    inputWorkSheet.Cells.Range["C" + pos].Value = Cliente;
                    inputWorkSheet.Cells.Range["D" + pos].Value = GuiaAerea;
                    inputWorkSheet.Cells.Range["E" + pos].Value = FacturaComercial;
                    inputWorkSheet.Cells.Range["F" + pos].Value = Bultos;
                    inputWorkSheet.Cells.Range["G" + pos].Value = KGS;
                    inputWorkSheet.Cells.Range["H" + pos].Value = PesoMiami;
                    inputWorkSheet.Cells.Range["I" + pos].Value = PesoISwissport;
                    inputWorkSheet.Cells.Range["J" + pos].Value = PesoSSwissport;
                    inputWorkSheet.Cells.Range["K" + pos].Value = Producto;
                    inputWorkSheet.Cells.Range["M" + pos].Value = ETA;
                    inputWorkSheet.Cells.Range["N" + pos].Value = ATA;
                    inputWorkSheet.Cells.Range["P" + pos].Value = IngresoDeposito;
                    inputWorkSheet.Cells.Range["Q" + pos].Value = Preliquidacion;
                    inputWorkSheet.Cells.Range["R" + pos].Value = AprovacionPreLiquidacion;
                    inputWorkSheet.Cells.Range["S" + pos].Value = Liquidacion;
                    inputWorkSheet.Cells.Range["T" + pos].Value = PagoLiquidacion;
                    inputWorkSheet.Cells.Range["U" + pos].Value = Canal;
                    inputWorkSheet.Cells.Range["V" + pos].Value = FechaRenvision;
                    inputWorkSheet.Cells.Range["W" + pos].Value = FechaLiberacion;
                    inputWorkSheet.Cells.Range["X" + pos].Value = FechaDespacho;
                    inputWorkSheet.Cells.Range["Z" + pos].Value = Comentarios;
                    inputWorkSheet.Cells.Range["AA" + pos].Value = EstadoActual;
                    inputWorkSheet.Cells.Range["AB" + pos].Value = Aforador;
                    inputWorkSheet.Cells.Range["AC" + pos].Value = TiempoProceso1;
                    inputWorkSheet.Cells.Range["AD" + pos].Value = TiempoProceso2;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                }
                conectar();
                InstruccionesBO I = new InstruccionesBO (logApp);
                var m_InputWorkSheetName2 = "Envios Despachados";
                var inputWorkSheet2 = workbook.Worksheets.Cast<Worksheet>().FirstOrDefault(w => w.Name == m_InputWorkSheetName2);
                I.ReporteHagamoda2Pagina2(dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"), cmbCliente.SelectedValue, cmbPais.SelectedValue);
                int pos2 = 4;
                for (int i = 0; i < I.totalRegistros; i++)
                {
                    pos2 = pos2 + 1;
                    string hijo = I.TABLA.Rows[i]["HojaRuta"].ToString();
                    inputWorkSheet2.Cells.Range["A" + pos2].Value = I.TABLA.Rows[i]["HojaRuta"].ToString();
                    inputWorkSheet2.Cells.Range["B" + pos2].Value = I.TABLA.Rows[i]["Proveedor"].ToString();
                    inputWorkSheet2.Cells.Range["C" + pos2].Value = I.TABLA.Rows[i]["Cliente"].ToString();
                    inputWorkSheet2.Cells.Range["D" + pos2].Value = I.TABLA.Rows[i]["GuiaAerea"].ToString();
                    inputWorkSheet2.Cells.Range["E" + pos2].Value = I.TABLA.Rows[i]["FacturaComercial"].ToString();
                    inputWorkSheet2.Cells.Range["F" + pos2].Value = I.TABLA.Rows[i]["Bultos"].ToString();
                    inputWorkSheet2.Cells.Range["G" + pos2].Value = I.TABLA.Rows[i]["KGSOrigen"].ToString();
                    inputWorkSheet2.Cells.Range["H" + pos2].Value = I.TABLA.Rows[i]["PesoMiami"].ToString();
                    inputWorkSheet2.Cells.Range["I" + pos2].Value = I.TABLA.Rows[i]["PesoIngresoSwissport"].ToString();
                    inputWorkSheet2.Cells.Range["J" + pos2].Value = I.TABLA.Rows[i]["PesoSalidaSwissport"].ToString();
                    inputWorkSheet2.Cells.Range["K" + pos2].Value = I.TABLA.Rows[i]["Producto"].ToString();
                    inputWorkSheet2.Cells.Range["L" + pos2].Value = I.TABLA.Rows[i]["ETS"].ToString();
                    inputWorkSheet2.Cells.Range["M" + pos2].Value = I.TABLA.Rows[i]["ETA"].ToString();
                    inputWorkSheet2.Cells.Range["N" + pos2].Value = I.TABLA.Rows[i]["ATA"].ToString();
                    inputWorkSheet2.Cells.Range["P" + pos2].Value = I.TABLA.Rows[i]["IngresoDeposito"].ToString();
                    inputWorkSheet2.Cells.Range["Q" + pos2].Value = I.TABLA.Rows[i]["PreLiquidacion"].ToString();
                    inputWorkSheet2.Cells.Range["R" + pos2].Value = I.TABLA.Rows[i]["AprovacionPreLiquidacion"].ToString();
                    inputWorkSheet2.Cells.Range["S" + pos2].Value = I.TABLA.Rows[i]["Liquidacion"].ToString();
                    inputWorkSheet2.Cells.Range["T" + pos2].Value = I.TABLA.Rows[i]["PagoLiquidacion"].ToString();
                    inputWorkSheet2.Cells.Range["U" + pos2].Value = I.TABLA.Rows[i]["Canal"].ToString();
                    inputWorkSheet2.Cells.Range["V" + pos2].Value = I.TABLA.Rows[i]["FechaLiberacion"].ToString();
                    inputWorkSheet2.Cells.Range["W" + pos2].Value = I.TABLA.Rows[i]["FechaDespacho"].ToString();
                    inputWorkSheet2.Cells.Range["X" + pos2].Value = I.TABLA.Rows[i]["FechaEntrega"].ToString();
                    inputWorkSheet2.Cells.Range["Y" + pos2].Value = I.TABLA.Rows[i]["EntregaEnTienda"].ToString();
                    inputWorkSheet2.Cells.Range["Z" + pos2].Value = I.TABLA.Rows[i]["Comentarios"].ToString();
                    inputWorkSheet2.Cells.Range["AA" + pos2].Value = I.TABLA.Rows[i]["EstadoActual"].ToString();
                    inputWorkSheet2.Cells.Range["AB" + pos2].Value = I.TABLA.Rows[i]["Aforador"].ToString();
                    inputWorkSheet2.Cells.Range["AC" + pos2].Value = I.TABLA.Rows[i]["TiempoProceso"].ToString();
                    inputWorkSheet2.Cells.Range["AD" + pos2].Value = I.TABLA.Rows[i]["TiempoProceso2"].ToString();
                    inputWorkSheet2.Cells.Range["AF" + pos2].Value = I.TABLA.Rows[i]["FacturaVEsta"].ToString();
                }
                rgReporte.Rebind();
                #endregion
                workbook.Save();

                //llenarBitacora("Se generó la carga " + idCarga, Session["IdUsuario"].ToString());
            }
            catch (Exception ex)
            {
                //LogError(ex.Message, Session["IdUsuario"].ToString(), "GenerarArchivoSAPDinant");
            }
            finally
            {
                //CERRAR EL LIBRO DE TRABAJO y la aplicacion

                workbook.Close();
                xlApp.Quit();
                desconectar();
                Application.UnLock();
            }
            //if (hayRegistro)
            //{
                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + nameDest);
                Response.Charset = "";
                Response.TransmitFile("J:\\Files1\\Aduanas\\" + nameDest);
                Response.Flush();
                Response.End();
                
            //}
            llenarGrid();   
        }
    }
    protected void cmbPais_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            conectar();
            ClientesBO cl = new ClientesBO(logApp);
            cl.loadAllCamposClientesXPais(cmbPais.SelectedValue);
            cmbCliente.DataSource = cl.TABLA;
            cmbCliente.DataBind();
        }
        catch { }
    }
    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        if (dpFechaInicio.SelectedDate <= dpFechaFinal.SelectedDate)
        {

            llenarGrid();
            rgReporte.Visible = true;
        }
        else
            registrarMensaje("Fecha inicio no puede ser mayor que fecha final");
    }
}
