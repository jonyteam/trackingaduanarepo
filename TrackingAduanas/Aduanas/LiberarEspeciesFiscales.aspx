﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="LiberarEspeciesFiscales.aspx.cs" Inherits="EnviosEspeciesFiscales" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style1 {
            width: 14%;
            height: 33px;
            text-align: center;
        }

        .style2 {
            width: 7%;
            height: 35px;
            text-align: left;
        }

        .style3 {
            width: 14%;
            height: 35px;
            text-align: center;
        }

        .style4 {
            width: 7%;
            height: 33px;
            text-align: left;
        }

        .style5 {
            width: 100%;
        }

        .style6 {
            width: 11%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Height="200px"
        Width="100%" Style="text-align: left"  LoadingPanelID="LoadingPanel" >
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                //<![CDATA[
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0
                    || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }

            </script>
        </telerik:RadScriptBlock>
        <table id="Table1" runat="server" style="width: 100%">
            <tr>
                <td class="style2">
                    <asp:Label ID="lblInstruccion" Font-Bold="true" runat="server"
                        Text="Instruccion:"></asp:Label>
                </td>
                <td class="style3">
                    <telerik:RadTextBox ID="txtInstruccion" runat="server" MaxLength="15" Width="140px"
                        EmptyMessage="Hoja Ruta" LabelWidth="40%">
                    </telerik:RadTextBox>
                </td>
                <td style="width: 85%; text-align: left;" colspan="2">
                    <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Images/24/view_24.png"
                        OnClick="btnBuscar_Click" />
                    <asp:Label ID="lblIdCorreccion" Font-Bold="True" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style4">
                    <asp:Label ID="lblEspecie" Font-Bold="True" runat="server" Text="Especie:"></asp:Label>
                </td>
                <td class="style1">
                    <telerik:RadComboBox ID="cmbEspecieFiscal" runat="server" DataTextField="Descripcion"
                        DataValueField="Codigo" Width="140px" AutoPostBack="True" OnSelectedIndexChanged="cmbEspecieFiscal_SelectedIndexChanged">
                    </telerik:RadComboBox>
                </td>
                <td style="text-align: left;" class="style6">
                    <asp:Label ID="lblRazon" Font-Bold="True" runat="server" Text="Razon de Anulacion:"
                        Visible="False"></asp:Label>
                </td>
                <td style="width: 85%; text-align: left;">
                    <telerik:RadComboBox ID="cmbAnulacion" runat="server" DataTextField="Descripcion"
                        DataValueField="Codigo" Visible="False" Width="190px">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td class="style4">
                    <asp:Label ID="lblAccion" Font-Bold="True" runat="server" Text="Accion:"></asp:Label>
                </td>
                <td class="style1">
                    <telerik:RadComboBox ID="cmbAccion" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cmbAccion_SelectedIndexChanged"
                        Width="140px">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="Seleccione la Acción:" Value="2"
                                Owner="cmbAccion" />
                            <telerik:RadComboBoxItem runat="server" Selected="True" Text="Liberar" Value="1"
                                Owner="cmbAccion" />
                            <telerik:RadComboBoxItem runat="server" Text="Anular" Value="0"
                                Owner="cmbAccion" />                       
                        </Items>
                    </telerik:RadComboBox>
                </td>
                <td style="text-align: left;" class="style6">
                    <asp:Label ID="lblObservacion" Font-Bold="True" runat="server" Text="Observacion:"
                        Visible="False"></asp:Label>
                </td>
                <td style="width: 85%; text-align: left;">
                    <telerik:RadTextBox ID="txtObservacion" runat="server" EmptyMessage="Observacion"
                        LabelWidth="64px" Visible="False" Width="190px">
                    </telerik:RadTextBox>
                    <asp:Label ID="UsuarioAsignoEspecie" runat="server" Font-Bold="True" Visible="False"></asp:Label>
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="rgEspecies" runat="server" AllowSorting="True"
            AutoGenerateColumns="False" CellSpacing="0" GridLines="None"
            OnNeedDataSource="rgEspecies_NeedDataSource" PageSize="100" Visible="False"
            Width="100%">
            <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                <Selecting AllowRowSelect="True" />
                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
            </ClientSettings>
            <MasterTableView>
                <CommandItemSettings ExportToPdfText="Export to PDF" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column"
                    Visible="True">
                    <HeaderStyle Width="20px" />
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column"
                    Visible="True">
                    <HeaderStyle Width="20px" />
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column"
                        UniqueName="TemplateColumn">
                        <ItemTemplate>
                            <asp:CheckBox ID="chbEspecie" runat="server" />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="Serie"
                        FilterControlAltText="Filter column1 column" HeaderText="Especie"
                        UniqueName="column1">
                        <HeaderStyle Width="40%" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="HojaRuta"
                        FilterControlAltText="Filter column column" HeaderText="Hoja Ruta"
                        UniqueName="column">
                        <HeaderStyle Width="40%" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
                <PagerStyle PageButtonCount="100" />
            </MasterTableView>
            <PagerStyle PageButtonCount="100" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>


                    
        </telerik:RadGrid>

        <table class="style5">
            <tr>
                <td>        
                    <asp:ImageButton ID="btnGuardar" runat="server"
                        ImageUrl="~/Images/24/disk_blue_ok_24.png"
                          ConfirmText="¿Está seguro que desea generar reporte de esta instrucción?"
                          ConfirmDialogType="RadWindow" 
                          ConfirmTitle="return confirm('¿Está seguro que desea Liberar o Anular Especie Fiscal?')"
                         
                          onclientclick="return confirm('¿Está seguro que desea Liberar o Anular Especie Fiscal?')"
                          OnClick="btnGuardar_Click" 
                        ToolTip="Salvar" Visible="False" />
                </td>
                    <%--onclientclick="return confirm('¿Está seguro que desea Liberar o Anular instrucción?')"--%>
            </tr>
        </table>


        <script type="text/javascript">
            function Clicking(sender, args) {
                args.set_cancel(!window.confirm("¿Está seguro que desea Liberar o Anular instrucción?"));
            }
        </script>

    </telerik:RadAjaxPanel>


</asp:Content>
