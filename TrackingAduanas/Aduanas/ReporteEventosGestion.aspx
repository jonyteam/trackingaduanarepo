﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteEventosGestion.aspx.cs" Inherits="ReporteEventos" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 16%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                //<![CDATA[
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }

                function openWin() {
                    var oWnd = radopen("BuscarTramite.aspx", "RadWindow1");
                    oWnd.add_close(OnClientClose);
                }

            </script>
        </telerik:RadScriptBlock>
        <table border="1" style="width: 60%">
            <tr>
                <td style="width: 15%">
                    <asp:Label ID="lblFechaInicio" runat="server" Text="Fecha Inicio:" ForeColor="Black"></asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadDatePicker ID="dpFechaInicio" runat="server" EnableTyping="False" Width="95%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="dpFechaInicio"
                        ErrorMessage="Ingrese Fecha" ForeColor="Red" Display="Dynamic" />
                </td>
                <td style="width: 15%">
                    <asp:Label ID="lblFechaFinal" runat="server" Text="Fecha Final:" ForeColor="Black"></asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadDatePicker ID="dpFechaFinal" runat="server" EnableTyping="False" Width="95%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator runat="server" ID="reqName" ControlToValidate="dpFechaFinal"
                        ErrorMessage="Ingrese Fecha" ForeColor="Red" Display="Dynamic" />
                </td>
                <td style="width: 10%">
                    <asp:Label ID="lblPais" runat="server" Text="País:" ForeColor="Black"></asp:Label>
                </td>
                <td class="auto-style1">
                    <telerik:RadComboBox ID="cmbPais" runat="server" DataTextField="Descripcion" DataValueField="Codigo" AutoPostBack="True" OnSelectedIndexChanged="cmbPais_SelectedIndexChanged">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td style="width: 15%">
                    <asp:Label ID="Label3" runat="server" ForeColor="Black" Text="Aduana:"></asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadComboBox ID="cmbAduanas" runat="server" DataTextField="NombreAduana" DataValueField="CodigoAduana">
                    </telerik:RadComboBox>
                </td>
                <td style="width: 15%">
                    <asp:Label ID="Label2" runat="server" ForeColor="Black" Text="Cliente:" Visible="False"></asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadComboBox ID="cmbCliente" runat="server" DataTextField="Nombre" DataValueField="CodigoSAP" Filter="StartsWith" Visible="False" Width="100%">
                    </telerik:RadComboBox>
                </td>
                <td style="width: 10%">&nbsp;</td>
                <td class="auto-style1">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="6" align="center">
                    <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Images/24/view_24.png"
                        OnClick="btnBuscar_Click" />
                </td>
            </tr>
        </table>
        <telerik:RadGrid ID="rgInstrucciones" enableajax="True" ShowStatusBar="True" runat="server"
            Visible="False" AllowFilteringByColumn="True" Width="100%"
            Height="410px" AutoGenerateColumns="False" PageSize="100" AllowSorting="True" AllowPaging="True" GridLines="None" OnDetailTableDataBind="rgInstrucciones_DetailTableDataBind"
            OnNeedDataSource="rgInstrucciones_NeedDataSource" OnItemCommand="rgInstrucciones_ItemCommand" CellSpacing="0" Culture="es-ES">
            <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
            <PagerStyle Mode="NumericPages"></PagerStyle>
            <MasterTableView Width="100%" DataKeyNames="IdInstruccion" AllowMultiColumnSorting="True"
                HierarchyDefaultExpanded="true" CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros."
                NoMasterRecordsText="No hay eventos." GroupLoadMode="Client">
                <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                    ShowExportToExcelButton="true" ShowExportToCsvButton="false" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="IdInstruccion" FilterControlAltText="Filter IdInstruccion column" HeaderText="Hoja Ruta" UniqueName="IdInstruccion" FilterControlWidth="70%">
                        <HeaderStyle Width="115px" />
                        <ItemStyle Width="115px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NombreAduana" FilterControlAltText="Filter NombreAduana column" HeaderText="Aduana" UniqueName="NombreAduana" FilterControlWidth="75%">
                        <HeaderStyle Width="120px" />
                        <ItemStyle Width="120px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Cliente" FilterControlAltText="Filter Cliente column" HeaderText="Cliente" UniqueName="Cliente" FilterControlWidth="85%">
                        <HeaderStyle Width="200px" />
                        <ItemStyle Width="200px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Nombre" FilterControlAltText="Filter Nombre column" HeaderText="Nombre" UniqueName="Nombre" FilterControlWidth="70%">
                        <HeaderStyle Width="115px" />
                        <ItemStyle Width="115px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Flujo/Evento" FilterControlAltText="Filter Flujo/Evento column" HeaderText="Flujo/Evento" UniqueName="Flujo/Evento" FilterControlWidth="83%">
                        <HeaderStyle Width="160px" />
                        <ItemStyle Width="160px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Fecha" FilterControlAltText="Filter Fecha column" HeaderText="Fecha" UniqueName="Fecha" FilterControlWidth="83%">
                        <HeaderStyle Width="160px" />
                        <ItemStyle Width="160px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FechaIngreso" FilterControlAltText="Filter FechaIngreso column" HeaderText="Fecha Ingreso" UniqueName="FechaIngreso" FilterControlWidth="83%">
                        <HeaderStyle Width="160px" />
                        <ItemStyle Width="160px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Diferencia" DataFormatString="{0:#,###.00}" FilterControlAltText="Filter Diferencia column" HeaderText="Diferencia" UniqueName="Diferencia" FilterControlWidth="63%">
                        <HeaderStyle Width="78px" />
                        <ItemStyle Width="78px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FechaFin" FilterControlAltText="Filter FechaFin column" HeaderText="Fecha Fin" UniqueName="FechaFin" FilterControlWidth="83%">
                        <HeaderStyle Width="160px" />
                        <ItemStyle Width="160px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FechaFinIngreso" FilterControlAltText="Filter FechaFinIngreso column" HeaderText="Fecha Fin Ingreso" UniqueName="FechaFinIngreso" FilterControlWidth="83%">
                        <HeaderStyle Width="160px" />
                        <ItemStyle Width="160px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Diferencia2" DataFormatString="{0:#,###.00}" FilterControlAltText="Filter Diferencia2 column" HeaderText="Diferencia" UniqueName="Diferencia2" FilterControlWidth="63%">
                        <HeaderStyle Width="78px" />
                        <ItemStyle Width="78px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Informacion" FilterControlAltText="Filter Informacion column" HeaderText="Informacion" UniqueName="Informacion" FilterControlWidth="63%">
                        <HeaderStyle Width="78px" />
                        <ItemStyle Width="78px" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <HeaderStyle Width="180px" />
            <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                <Selecting AllowRowSelect="True" />
                <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                    DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                    DropHereToReorder="Suelte aquí para Re-Ordenar" />
                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
            </ClientSettings>
            <FilterMenu EnableTheming="True">
                <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
            </FilterMenu>
            <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
            <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
        </telerik:RadGrid>
    </telerik:RadAjaxPanel>
</asp:Content>
