﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteSeguimientoEspecies.aspx.cs" Inherits="GenerarArchivoSAPDinant" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style1 {
            width: 734px;
        }

        .style4 {
            width: 155px;
        }

        .style5 {
            width: 107px;
        }

        .style6 {
            width: 122px;
        }

        .style8 {
            width: 105px;
        }

        .style9 {
            width: 109px;
        }

        .auto-style1 {
            width: 72px;
        }

        .auto-style2 {
            width: 178px;
        }

        .auto-style3 {
            width: 66px;
        }

        .auto-style5 {
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart" Style="text-align: left">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                //<![CDATA[

                function pnlRequestStarted(ajaxPanel, eventArgs) {
                    try {
                        if ((eventArgs.EventTarget == "<%= btnSalvar.UniqueID %>")) {
                            eventArgs.EnableAjax = false;
                        }
                    }
                    catch (err)
                    { }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                    }
                }
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }

            </script>
        </telerik:RadScriptBlock>
        <telerik:RadTabStrip ID="TabSeguimientoEspecies" runat="server" SelectedIndex="0" MultiPageID="RadMultiPage1" AutoPostBack="True">
            <Tabs>
                <telerik:RadTab runat="server" Text="Compra" PageViewID="Compra" SelectedIndex="0" Selected="True" />
                <telerik:RadTab runat="server" Text="Envio" PageViewID="Envio" SelectedIndex="1" />
                <telerik:RadTab runat="server" Text="Inventario" PageViewID="Inventario" SelectedIndex="2" />

                <telerik:RadTab runat="server" PageViewID="PVAnulaciones" SelectedIndex="3" Text="Anulacion">
                </telerik:RadTab>

            </Tabs>
        </telerik:RadTabStrip>
        <table border="1" style="width: 55%">
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="Label1" runat="server" Text="Fecha Inicial:"></asp:Label>
                </td>
                <td class="auto-style2">
                    <telerik:RadDatePicker ID="rdpFechaInicio" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td class="auto-style3">
                    <asp:Label ID="Label2" runat="server" Text="Fecha Final:"></asp:Label>
                </td>
                <td class="auto-style2">
                    <telerik:RadDatePicker ID="rdpFechaFinal" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td align="center" class="auto-style5" rowspan="2">
                    <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Images/24/view_24.png" OnClick="btnBuscar_Click" />
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    <asp:Label ID="Label4" runat="server" Text="Aduana:"></asp:Label>
                </td>
                <td class="auto-style2">
                    <telerik:RadComboBox ID="cmbAduana" runat="server" DataTextField="NombreAduana" DataValueField="CodigoAduana">
                    </telerik:RadComboBox>
                </td>
                <td class="auto-style3">
                    <asp:Label ID="Label3" runat="server" Text="Seleccione Especie:"></asp:Label>
                </td>
                <td class="auto-style2">
                    <asp:CheckBox ID="chbAnuladas" runat="server" Text="Mostrar Deshabilitadas" />
                    <telerik:RadComboBox ID="cmbEspecie" runat="server" DataTextField="Descripcion" DataValueField="Codigo">
                    </telerik:RadComboBox>
                </td>
            </tr>
        </table>
        <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex ="0">
            <telerik:RadPageView ID="Compra" runat="server" Style="margin-top: 0px" Width="100%">
                <telerik:RadGrid ID="rgCompras" runat="server" AllowFilteringByColumn="True" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" Culture="es-ES" GridLines="None" OnItemCommand="rgCompras_ItemCommand" OnNeedDataSource="rgCompras_NeedDataSource" PageSize="20" Width="100%">
                    <ClientSettings AllowColumnsReorder="True">
                        <Selecting AllowRowSelect="True" />
                        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                    </ClientSettings>
                    <ExportSettings FileName="" HideStructureColumns="true" OpenInNewWindow="True">
                        <Excel Format="Biff" />
                    </ExportSettings>
                    <AlternatingItemStyle BackColor="#999999" BorderColor="Black" BorderStyle="Solid" HorizontalAlign="Left" VerticalAlign="Middle" />
                    <MasterTableView CommandItemDisplay="Top">
                        <CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="false" ShowExportToExcelButton="true" />
                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridBoundColumn DataField="TipoEspecie" FilterControlAltText="Filter Descripcion column" FilterControlWidth="80%" HeaderText="Tipo Especie" UniqueName="Descripcion">
                                <ItemStyle Width="200px" />
                                <HeaderStyle Width="200px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Serie" FilterControlAltText="Filter Serie column" FilterControlWidth="80%" HeaderText="Serie" UniqueName="Serie">
                                <ItemStyle Width="200px" />
                                <HeaderStyle Width="200px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="CodigoCompra" FilterControlAltText="Filter IdCompra column" FilterControlWidth="80%" HeaderText="Codigo Compra" UniqueName="IdCompra">
                                <ItemStyle Width="200px" />
                                <HeaderStyle Width="200px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Fecha" FilterControlAltText="Filter Fecha column" FilterControlWidth="80%" HeaderText="Fecha Compra" UniqueName="Fecha" DataFormatString="{0:d}">
                                <ItemStyle Width="200px" />
                                <HeaderStyle Width="200px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Nombre" FilterControlAltText="Filter Nombre column" FilterControlWidth="80%" HeaderText="Usuario" UniqueName="Nombre">
                                <ItemStyle Width="200px" />
                                <HeaderStyle Width="200px" />
                            </telerik:GridBoundColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                            </EditColumn>
                        </EditFormSettings>
                    </MasterTableView>
                    <HeaderStyle BackColor="Red" BorderStyle="Solid" Font-Bold="True" ForeColor="Black" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <ItemStyle BackColor="#999999" BorderColor="Black" BorderStyle="Solid" HorizontalAlign="Left" VerticalAlign="Middle" />
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                </telerik:RadGrid>
            </telerik:RadPageView>
            <telerik:RadPageView ID="Envio" runat="server">
                <telerik:RadGrid ID="rgEnvio" runat="server" AllowFilteringByColumn="True" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" Culture="es-ES" GridLines="None" OnItemCommand="rgEnvio_ItemCommand" OnNeedDataSource="rgEnvio_NeedDataSource" PageSize="20" Width="100%">
                    <ClientSettings AllowColumnsReorder="True">
                        <Selecting AllowRowSelect="True" />
                        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                    </ClientSettings>
                    <ExportSettings FileName="" HideStructureColumns="true" OpenInNewWindow="True">
                        <Excel Format="Biff" />
                    </ExportSettings>
                    <AlternatingItemStyle BackColor="#999999" BorderColor="Black" BorderStyle="Solid" HorizontalAlign="Left" VerticalAlign="Middle" />
                    <MasterTableView CommandItemDisplay="Top">
                        <CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="false" ShowExportToExcelButton="true" />
                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridBoundColumn DataField="Descripcion" FilterControlAltText="Filter Descripcion column" HeaderText="Tipo Especie" UniqueName="Descripcion" FilterControlWidth="80%">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Serie" FilterControlAltText="Filter Serie column" HeaderText="Serie" UniqueName="Serie" FilterControlWidth="70%">
                                <HeaderStyle Width="100px" />
                                <ItemStyle Width="100px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Origen" FilterControlAltText="Filter Origen column" HeaderText="Aduana Origen" UniqueName="Origen" FilterControlWidth="80%">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Destino" FilterControlAltText="Filter Destino column" HeaderText="Aduana Destino" UniqueName="Destino" FilterControlWidth="80%">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="FechaEnvio" FilterControlAltText="Filter FechaEnvio column" HeaderText="Fecha Envio" UniqueName="FechaEnvio" DataFormatString="{0:d}" FilterControlWidth="80%">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Nombre" FilterControlAltText="Filter Nombre column" HeaderText="Usuario Envio" UniqueName="Nombre" FilterControlWidth="80%">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="FechaRecibido" FilterControlAltText="Filter FechaRecibido column" HeaderText="Fecha Recibido" UniqueName="FechaRecibido" DataFormatString="{0:d}" FilterControlWidth="80%">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="NombreRecibio" FilterControlAltText="Filter NombreRecibio column" HeaderText="Usuario Recibido" UniqueName="NombreRecibio" FilterControlWidth="80%">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Observacion" FilterControlAltText="Filter Observacion column" HeaderText="Observacion" UniqueName="Observacion" FilterControlWidth="80%">
                            </telerik:GridBoundColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                            </EditColumn>
                        </EditFormSettings>
                    </MasterTableView>
                    <HeaderStyle BackColor="Red" BorderStyle="Solid" Font-Bold="True" ForeColor="Black" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <ItemStyle BackColor="#999999" BorderColor="Black" BorderStyle="Solid" HorizontalAlign="Left" VerticalAlign="Middle" />
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                </telerik:RadGrid>
            </telerik:RadPageView>
            <telerik:RadPageView ID="Inventario" runat="server">
                <telerik:RadGrid ID="rgInventario" runat="server" AllowFilteringByColumn="True" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" Culture="es-ES" GridLines="None" OnItemCommand="rgInventario_ItemCommand" OnNeedDataSource="rgInventario_NeedDataSource" PageSize="20" Width="100%">
                    <ClientSettings AllowColumnsReorder="True">
                        <Selecting AllowRowSelect="True" />
                        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                    </ClientSettings>
                    <ExportSettings FileName="" HideStructureColumns="true" OpenInNewWindow="True">
                        <Excel Format="Biff" />
                    </ExportSettings>
                    <AlternatingItemStyle BackColor="#999999" BorderColor="Black" BorderStyle="Solid" HorizontalAlign="Left" VerticalAlign="Middle" />
                    <MasterTableView CommandItemDisplay="Top">
                        <CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="false" ShowExportToExcelButton="true" />
                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridBoundColumn DataField="Descripcion" FilterControlAltText="Filter Descripcion column" HeaderText="Tipo Especie" UniqueName="Descripcion" FilterControlWidth="80%">
                                <HeaderStyle Width="150px" />
                                <ItemStyle Width="150px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Serie" FilterControlAltText="Filter Serie column" HeaderText="Serie" UniqueName="Serie" FilterControlWidth="70%">
                                <HeaderStyle Width="100px" />
                                <ItemStyle Width="100px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="NombreAduana" FilterControlAltText="Filter NombreAduana column" HeaderText="Aduana" UniqueName="NombreAduana" FilterControlWidth="75%">
                                <HeaderStyle Width="120px" />
                                <ItemStyle Width="120px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Fecha" FilterControlAltText="Filter Fecha column" HeaderText="Fecha Ultimo Movimiento" UniqueName="Fecha" FilterControlWidth="80%">
                                <HeaderStyle Width="150px" />
                                <ItemStyle Width="150px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Nombre" FilterControlAltText="Filter Nombre column" HeaderText="Ultimo Movimiento" UniqueName="Nombre" FilterControlWidth="80%">
                                <HeaderStyle Width="140px" />
                                <ItemStyle Width="140px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Cliente" FilterControlAltText="Filter Cliente column" HeaderText="Cliente" UniqueName="Cliente" FilterControlWidth="90%">
                                <HeaderStyle Width="240px" />
                                <ItemStyle Width="240px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="IdCompra" FilterControlAltText="Filter IdCompra column" HeaderText="Codigo Compra" UniqueName="IdCompra" FilterControlWidth="80%">
                                <HeaderStyle Width="100px" />
                                <ItemStyle Width="100px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="FechaReporte" FilterControlAltText="Filter Date column" HeaderText="Fecha Reporte" UniqueName="FechaReporte" FilterControlWidth="80%">
                                <HeaderStyle Width="150px" />
                                <ItemStyle Width="150px" />
                            </telerik:GridBoundColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                            </EditColumn>
                        </EditFormSettings>
                    </MasterTableView>
                    <HeaderStyle BackColor="Red" BorderStyle="Solid" Font-Bold="True" ForeColor="Black" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <ItemStyle BackColor="#999999" BorderColor="Black" BorderStyle="Solid" HorizontalAlign="Left" VerticalAlign="Middle" />
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                </telerik:RadGrid>
            </telerik:RadPageView>
            <telerik:RadPageView ID="PVAnulaciones" runat="server">
                <telerik:RadGrid ID="rgAnulacion" runat="server" AllowFilteringByColumn="True" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" Culture="es-ES" GridLines="None" OnItemCommand="rgAnulacion_ItemCommand" OnNeedDataSource="rgAnulacion_NeedDataSource" PageSize="20" Width="100%">
                    <ClientSettings AllowColumnsReorder="True">
                        <Selecting AllowRowSelect="True" />
                        <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                    </ClientSettings>
                    <ExportSettings FileName="" HideStructureColumns="true" OpenInNewWindow="True">
                        <Excel Format="Biff" />
                    </ExportSettings>
                    <AlternatingItemStyle BackColor="#999999" BorderColor="Black" BorderStyle="Solid" HorizontalAlign="Left" VerticalAlign="Middle" />
                    <MasterTableView CommandItemDisplay="Top">
                        <CommandItemSettings ExportToPdfText="Export to PDF" ShowAddNewRecordButton="false" ShowExportToExcelButton="true" />
                        <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                        </RowIndicatorColumn>
                        <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                        </ExpandCollapseColumn>
                        <Columns>
                            <telerik:GridBoundColumn DataField="Serie" FilterControlAltText="Filter Serie column" HeaderText="Serie" UniqueName="Serie">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="NombreAduana" FilterControlAltText="Filter Aduana column" HeaderText="Aduana" UniqueName="Aduana">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="IdEspecieFiscal" FilterControlAltText="Filter column column" UniqueName="column">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="RazonAnulacion" FilterControlAltText="Filter RazonAnulacion column" HeaderText="Razon Anulacion" UniqueName="RazonAnulacion">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Nombre" FilterControlAltText="Filter UsuarioAnulo column" HeaderText="Usuario Anulo" UniqueName="UsuarioAnulo">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Fecha" FilterControlAltText="Filter FechaAnulacion column" HeaderText="Fecha de Anulacion" UniqueName="FechaAnulacion">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="Observacion" FilterControlAltText="Filter Observacion column" HeaderText="Observacion" UniqueName="Observacion">
                            </telerik:GridBoundColumn>
                        </Columns>
                        <EditFormSettings>
                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                            </EditColumn>
                        </EditFormSettings>
                    </MasterTableView>
                    <HeaderStyle BackColor="Red" BorderStyle="Solid" Font-Bold="True" ForeColor="Black" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <ItemStyle BackColor="#999999" BorderColor="Black" BorderStyle="Solid" HorizontalAlign="Left" VerticalAlign="Middle" />
                    <FilterMenu EnableImageSprites="False">
                    </FilterMenu>
                </telerik:RadGrid>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
        <table width="100%">
            <tr align="center">
                <td align="center">
                    <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="~/Images/gris.png" />
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
    <%--<GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>--%>
</asp:Content>
