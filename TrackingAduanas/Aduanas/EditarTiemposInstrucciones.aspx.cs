﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using System.Data;

using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.SessionState;
using System.Collections.Specialized;
using GrupoLis.Login;
using System.Text.RegularExpressions;
using System.Security.Principal;
using Telerik.Web;
using System.Security.Cryptography;

public partial class EditarTiemposInstrucciones : Utilidades.PaginaBase
{
    private NameValueCollection mParamethers;
    private GrupoLis.Login.Login logAppAduanas;

    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Editar Instrucciones";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        mParamethers = ConfigurationManager.AppSettings;
        SetGridFilterMenu(rgInstrucciones.FilterMenu);
        rgInstrucciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgInstrucciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            if (!Modificar)
                redirectTo("Default.aspx");
            rgInstrucciones.ClientSettings.Scrolling.AllowScroll = true;
            rgInstrucciones.ClientSettings.Scrolling.UseStaticHeaders = true;
            rgInstrucciones.ShowHeader = true;
            rgInstrucciones.ShowFooter = true;
            ((SiteMaster)Master).SetTitulo("Editar Tiempos Instrucciones", "Editar Tiempos Instrucciones");
            if (User.IsInRole("Administradores") || User.IsInRole("Administrador Pais"))
                Table1.Visible = true;
            else
                Table1.Visible = false;
        }
    }

    protected void rgInstrucciones_Init(object sender, System.EventArgs e)
    {
        GridFilterMenu menu = rgInstrucciones.FilterMenu;
        menu.Items.RemoveAt(rgInstrucciones.FilterMenu.Items.Count - 2);
    }

    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void rgInstrucciones_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridInstrucciones();
    }

    private void llenarGridInstrucciones()
    {
        try
        {
            conectar();
            InstruccionesBO ins = new InstruccionesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            if (User.IsInRole("Administradores"))
                ins.loadInstruccionesAll();
            else if (User.IsInRole("Administrador Pais"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                ins.loadInstruccionesAllXPais(u.CODPAIS);
            }
            rgInstrucciones.DataSource = ins.TABLA;
        }
        catch (Exception) { }
        finally
        {
            desconectar();
        }
    }

    protected void rgInstrucciones_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            conectarAduanas();
            InstruccionesBO i = new InstruccionesBO(logApp);
            GestionCargaBO gc = new GestionCargaBO(logApp);
            EventosBO ev = new EventosBO(logApp);
            FlujoCargaBO fc = new FlujoCargaBO(logApp);
            //HojaRutaBO hr = new HojaRutaBO(logAppAduanas);
            //ComplementoRecepcionBO cr = new ComplementoRecepcionBO(logAppAduanas);
            //EncabezadoEsquemaBO ee = new EncabezadoEsquemaBO(logAppAduanas);
            GridEditableItem editedItem = e.Item as GridEditableItem;
            if (e.CommandName == "Guardar")
            {
                edIdInstruccion.Value = Convert.ToString(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdInstruccion"]);
                RadComboBox edCampo = (RadComboBox)editedItem.FindControl("edCampo");
                RadDatePicker fecha = (RadDatePicker)editedItem.FindControl("dtFechaSellado");
                RadTimePicker hora = (RadTimePicker)editedItem.FindControl("dtHoraSellado");
                RadTextBox edObservacion = (RadTextBox)editedItem.FindControl("edObservacion");

                if (edCampo.SelectedValue == "Seleccione")
                    registrarMensaje("Seleccione un campo");
                else if (edCampo.SelectedValue == "FR")
                {
                    try
                    {
                        if (fecha.SelectedDate.HasValue)
                        {
                            if (hora.SelectedDate.HasValue)
                            {
                                i.loadInstruccion(edIdInstruccion.Value);
                                if (i.totalRegistros > 0)
                                {
                                    string oldFechaRecepcion = i.FECHARECEPCION;
                                    i.FECHARECEPCION = fecha.SelectedDate.Value.ToShortDateString() + " " + hora.SelectedDate.Value.ToShortTimeString();
                                    i.actualizar();
                                    registrarMensaje("Fecha recepción modificada exitosamente");
                                    llenarBitacora("Se modificó la fecha recepción " + oldFechaRecepcion + " por " + i.FECHARECEPCION + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), i.IDINSTRUCCION);
                                    //rgInstrucciones.Rebind();
                                    limpiar();
                                }
                            }
                        }
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        bool gestionCarga = false, evento = false;
                        gc.loadGestionCargaXInstruccionEvento(edIdInstruccion.Value, edCampo.SelectedValue);
                        if (gc.totalRegistros > 0)
                        {
                            string oldFechaGC = gc.FECHA;
                            gc.FECHA = fecha.SelectedDate.Value.ToShortDateString() + " " + hora.SelectedDate.Value.ToShortTimeString();
                            if (edObservacion.Text != "")
                            {
                                gc.OBSERVACION = edObservacion.Text.Trim();
                            }
                            fc.loadFlujoCargaUnico(edCampo.SelectedValue);
                            if (fc.totalRegistros > 0)
                            {
                                if (fc.CODESTADOFLUJOCARGA == "6")
                                {
                                    i.loadInstruccion(edIdInstruccion.Value);
                                    i.FECHAFINALFLUJO = fecha.SelectedDate.Value.ToShortDateString() + " " + hora.SelectedDate.Value.ToShortTimeString();
                                    i.actualizar();
                                }
                            }
                            gc.actualizar();
                            registrarMensaje("Fecha modificada exitosamente");
                            llenarBitacora("Se modificó la fecha de " + edCampo.SelectedValue + " " + oldFechaGC + " por " + gc.FECHA + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), edIdInstruccion.Value);
                            //rgInstrucciones.Rebind();
                            limpiar();
                            gestionCarga = true;
                        }
                        if (!gestionCarga)
                        {
                            ev.loadEventosXInstruccionYEvento(edIdInstruccion.Value, edCampo.SelectedValue.Substring(0, edCampo.SelectedValue.Trim().Length - 1));
                            if (edCampo.SelectedValue.Contains("A") & !String.IsNullOrEmpty(ev.FECHAINICIO))
                            {
                                string oldFechaEV = ev.FECHAINICIO;
                                ev.FECHAINICIO = fecha.SelectedDate.Value.ToShortDateString() + " " + hora.SelectedDate.Value.ToShortTimeString();
                                if (edObservacion.Text != "")
                                {
                                    ev.OBSERVACIONINICIO = edObservacion.Text.Trim();
                                }
                                ev.actualizar();
                                registrarMensaje("Fecha modificada exitosamente");
                                llenarBitacora("Se modificó la fecha de " + edCampo.SelectedValue + " " + oldFechaEV + " por " + ev.FECHAINICIO + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), edIdInstruccion.Value);
                                //rgInstrucciones.Rebind();
                                limpiar();
                                evento = true;
                            }
                            else if (edCampo.SelectedValue.Contains("B") & !String.IsNullOrEmpty(ev.FECHAFIN))
                            {
                                string oldFechaEV = ev.FECHAFIN;
                                ev.FECHAFIN = fecha.SelectedDate.Value.ToShortDateString() + " " + hora.SelectedDate.Value.ToShortTimeString();
                                if (edObservacion.Text != "")
                                {
                                    ev.OBSERVACIONFIN = edObservacion.Text.Trim();
                                }
                                ev.actualizar();
                                registrarMensaje("Fecha modificada exitosamente");
                                llenarBitacora("Se modificó la fecha de " + edCampo.SelectedValue + " " + oldFechaEV + " por " + ev.FECHAFIN + " " + edObservacion.Text.Trim(), Session["IdUsuario"].ToString(), edIdInstruccion.Value);
                                //rgInstrucciones.Rebind();
                                limpiar();
                                evento = true;
                            }
                        }
                        if (!gestionCarga || !evento)
                            registrarMensaje("Consulte al administrador del sistema");
                    }
                    catch { }
                }
            }
        }
        catch (Exception)
        { }
    }

    private void ConfigureExport()
    {
        rgInstrucciones.Columns[4].Visible = false;
        rgInstrucciones.Columns[5].Visible = false;
        rgInstrucciones.Columns[6].Visible = false;
        rgInstrucciones.Columns[7].Visible = false;
        rgInstrucciones.Columns[8].Visible = false;
        rgInstrucciones.Columns[9].Visible = false;
        rgInstrucciones.Columns[10].Visible = false;
        rgInstrucciones.Columns[11].Visible = false;
        rgInstrucciones.Columns[12].Visible = false;
        String filename = "Instrucciones" + "_" + DateTime.Now.ToShortDateString();
        rgInstrucciones.GridLines = GridLines.Both;
        rgInstrucciones.ExportSettings.FileName = filename;
        rgInstrucciones.ExportSettings.IgnorePaging = true;
        rgInstrucciones.ExportSettings.OpenInNewWindow = false;
        rgInstrucciones.ExportSettings.ExportOnlyData = true;
    }

    protected void btnLimpiar_Click(object sender, ImageClickEventArgs e)
    {
        limpiar();
    }

    private void limpiar()
    {
        try
        {
            foreach (GridDataItem grdItem in rgInstrucciones.Items)
            {
                RadComboBox edCampo = (RadComboBox)grdItem.FindControl("edCampo");
                RadDatePicker fecha = (RadDatePicker)grdItem.FindControl("dtFechaSellado");
                RadTimePicker hora = (RadTimePicker)grdItem.FindControl("dtHoraSellado");
                RadTextBox observacion = (RadTextBox)grdItem.FindControl("edObservacion");
                edCampo.ClearSelection();
                fecha.Visible = false;
                hora.Visible = false;
                observacion.Text = "";
            }
        }
        catch (Exception)
        { }
    }

    protected void rgInstrucciones_ItemDataBound(object sender, GridItemEventArgs e)
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            foreach (GridDataItem gridItem in rgInstrucciones.Items)
            {
                string idInstruccion = gridItem.Cells[2].Text;
                RadComboBox edCampo = (RadComboBox)gridItem.FindControl("edCampo");
                RadDatePicker fecha = (RadDatePicker)gridItem.FindControl("dtFechaSellado");
                RadTimePicker hora = (RadTimePicker)gridItem.FindControl("dtHoraSellado");
                RadTextBox observacion = (RadTextBox)gridItem.FindControl("edObservacion");

                i.loadDescripcionEstados(idInstruccion);
                edCampo.DataSource = i.TABLA;
                edCampo.DataBind();
                edCampo.Items.Insert(0, new RadComboBoxItem("Fecha Recepción", "FR"));
                edCampo.Items.Insert(0, new RadComboBoxItem("Seleccione...", "Seleccione"));
                switch (edCampo.SelectedValue)
                {
                    case "Seleccione":
                        fecha.Visible = false;
                        hora.Visible = false;
                        break;
                    default: break;
                }
            }
        }
        catch { }
    }

    protected void SetVisible(TableCellCollection cells, String commandName, string visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is RadTextBox)
                {
                    RadTextBox txt = (RadTextBox)control;
                    if (txt.ID == commandName)
                        txt.Visible = true;
                    else
                        txt.Visible = false;
                }
            }
        }
    }

    protected void rgInstrucciones_ItemCreated(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            try
            {
                conectar();
                GridDataItem editForm = e.Item as GridDataItem;
                RadComboBox edCampo = editForm.FindControl("edCampo") as RadComboBox;
                edCampo.AutoPostBack = true;
                edCampo.SelectedIndexChanged += new RadComboBoxSelectedIndexChangedEventHandler(edCampo_SelectedIndexChanged);
            }
            catch { }
        }
    }

    void edCampo_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            GestionCargaBO gc = new GestionCargaBO(logApp);
            EventosBO ev = new EventosBO(logApp);
            ////////////////////////
            //ClientesBO c = new ClientesBO(logApp);
            //CodigosBO co = new CodigosBO(logApp);
            //UsuariosBO u = new UsuariosBO(logApp);
            //////////////////////
            GridDataItem editForm = (sender as RadComboBox).NamingContainer as GridDataItem;
            RadComboBox edCampo = editForm.FindControl("edCampo") as RadComboBox;
            RadDatePicker fecha = (RadDatePicker)editForm.FindControl("dtFechaSellado");
            RadTimePicker hora = (RadTimePicker)editForm.FindControl("dtHoraSellado");
            string idInstruccion = editForm.Cells[2].Text;
            //string codPais = editForm.Cells[6].Text;

            if (edCampo.SelectedValue == "Seleccione")
            {
                fecha.Visible = false;
                hora.Visible = false;
            }
            else if (edCampo.SelectedValue == "FR")
            {
                try
                {
                    i.loadInstruccion(idInstruccion);
                    fecha.SelectedDate = Convert.ToDateTime(i.FECHARECEPCION);
                    hora.SelectedDate = Convert.ToDateTime(i.FECHARECEPCION);
                    fecha.Visible = true;
                    hora.Visible = true;
                }
                catch { }
            }
            else
            {
                try
                {
                    bool gestionCarga = false, evento = false;
                    gc.loadGestionCargaXInstruccionEvento(idInstruccion, edCampo.SelectedValue);
                    if (gc.totalRegistros > 0)
                    {
                        fecha.SelectedDate = Convert.ToDateTime(gc.FECHA);
                        hora.SelectedDate = Convert.ToDateTime(gc.FECHA);
                        gestionCarga = true;
                    }
                    if (!gestionCarga)
                    {
                        ev.loadEventosXInstruccionYEvento(idInstruccion, edCampo.SelectedValue.Substring(0, edCampo.SelectedValue.Trim().Length - 1));
                        if (edCampo.SelectedValue.Contains("A") & !String.IsNullOrEmpty(ev.FECHAINICIO))
                        {
                            fecha.SelectedDate = Convert.ToDateTime(ev.FECHAINICIO);
                            hora.SelectedDate = Convert.ToDateTime(ev.FECHAINICIO);
                            evento = true;
                        }
                        else if (edCampo.SelectedValue.Contains("B") & !String.IsNullOrEmpty(ev.FECHAFIN))
                        {
                            fecha.SelectedDate = Convert.ToDateTime(ev.FECHAFIN);
                            hora.SelectedDate = Convert.ToDateTime(ev.FECHAFIN);
                            evento = true;
                        }
                    }
                    if (gestionCarga || evento)
                    {
                        fecha.Visible = true;
                        hora.Visible = true;
                    }
                    else
                    {
                        fecha.Visible = false;
                        hora.Visible = false;
                    }
                }
                catch { }
            }

        }
        catch (Exception)
        { }
    }

    #region Conexion EsquemaTramites2
    private void conectarAduanas()
    {
        if (logAppAduanas == null)
            logAppAduanas = new GrupoLis.Login.Login();
        if (!logAppAduanas.conectado)
        {
            logAppAduanas.tipoConexion = TipoConexion.SQL_SERVER;
            logAppAduanas.SERVIDOR = mParamethers.Get("Servidor");
            logAppAduanas.DATABASE = mParamethers.Get("DatabaseEsquemaTramites2");
            logAppAduanas.USER = mParamethers.Get("User");
            logAppAduanas.PASSWD = mParamethers.Get("Password");
            logAppAduanas.conectar();
        }
    }

    private void desconectarAduanas()
    {
        if (logAppAduanas.conectado)
            logAppAduanas.desconectar();
    }
    #endregion

    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            conectar();
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (User.IsInRole("Administradores"))
                llenarGridInstruccion(txtInstruccion.Text.Trim());
            else if (User.IsInRole("Administrador Pais") || User.IsInRole("Administrador Pais SVGT") & (txtInstruccion.Text.Trim().Substring(0, 1) == u.CODPAIS))
                llenarGridInstruccion(txtInstruccion.Text.Trim());
            else
            {
                rgInstrucciones.Rebind();
                registrarMensaje("No puede consultar instrucciones de otro país");
            }
        }
        catch { }
    }

    private void llenarGridInstruccion(string idInstruccion)
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            i.loadInstruccionesAllXInstruccion(idInstruccion);
            rgInstrucciones.DataSource = i.TABLA;
            rgInstrucciones.DataBind();
        }
        catch { }
    }

    protected void btnModificarEstado_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton btn = (ImageButton)sender;
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            FlujoCargaBO fc = new FlujoCargaBO(logApp);
            DocumentosRegimenBO dr = new DocumentosRegimenBO(logApp);
            DocumentosInstruccionesBO di = new DocumentosInstruccionesBO(logApp);
            GestionCargaBO gc = new GestionCargaBO(logApp);
            string idInstruccion = btn.CommandArgument;
            i.loadInstruccion(idInstruccion);
            if (i.totalRegistros > 0)
            {
                if (btn.CommandName == "Habilitar")
                {
                    i.IDESTADOFLUJO = "98"; //habilita la instruccion finalizadas para aplicar algunos cambios
                    btn.CommandName = "Deshabilitar";
                    btn.ImageUrl = "~/Images/16/delete2_16.png";
                    i.actualizar();
                    registrarMensaje("Se habilitó la instrucción " + idInstruccion + ", favor recuerde deshabilitarla despues de los cambios");
                    llenarBitacora("Instrucción habilitada exitosamente", Session["IdUsuario"].ToString(), idInstruccion);
                }
                else if (btn.CommandName == "Deshabilitar")
                {
                    i.IDESTADOFLUJO = "99"; //deshabilita la instruccion y la pasa a finalizada
                    btn.CommandName = "Habilitar";
                    btn.ImageUrl = "~/Images/16/check2_16.png";
                    i.actualizar();
                    registrarMensaje("Se deshabilitó la instrucción " + idInstruccion);
                    llenarBitacora("Instrucción deshabilitada exitosamente", Session["IdUsuario"].ToString(), idInstruccion);
                }
            }
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "EditarTiemposInstrucciones");
        }
        finally
        {
            desconectar();
        }
    }
}




//switch ()
//{
//    case :

//        break;
//    case "FR":

//        break;
//    case "FR":
//        try
//        {
//            i.loadInstruccion(idInstruccion);
//            fecha.SelectedDate = Convert.ToDateTime(i.FECHARECEPCION);
//            hora.SelectedDate = Convert.ToDateTime(i.FECHARECEPCION);
//            fecha.Visible = true;
//            hora.Visible = true;
//        }
//        catch { }
//        break;
//#region IdCliente
//case "IdCliente":
//    edCmbValor.Visible = true;
//    edTxtValor.Visible = false;
//    edNumValor.Visible = false;
//    c.loadAllCamposClientesXPais(codPais);
//    edCmbValor.DataTextField = c.TABLA.Columns["Nombre"].ToString();
//    edCmbValor.DataValueField = c.TABLA.Columns["CodigoSAP"].ToString();
//    edCmbValor.DataSource = c.TABLA;
//    edCmbValor.DataBind();
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        edCmbValor.SelectedValue = i.IDCLIENTE;
//    }
//    catch { }
//    break;
//#endregion
//#region OficialCuenta
//case "OficialCuenta":
//    edCmbValor.Visible = false;
//    edTxtValor.Visible = true;
//    edNumValor.Visible = false;
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        edTxtValor.Text = i.OFICIALCUENTA;
//    }
//    catch { }

//    break;
//#endregion
//#region ReferenciaCliente
//case "ReferenciaCliente":
//    edCmbValor.Visible = false;
//    edTxtValor.Visible = true;
//    edNumValor.Visible = false;
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        edTxtValor.Text = i.REFERENCIACLIENTE;
//    }
//    catch { }

//    break;
//#endregion
//#region Proveedor
//case "Proveedor":
//    edCmbValor.Visible = false;
//    edTxtValor.Visible = true;
//    edNumValor.Visible = false;
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        edTxtValor.Text = i.PROVEEDOR;
//    }
//    catch { }

//    break;
//#endregion
//#region ValorFOB
//case "ValorFOB":
//    edCmbValor.Visible = false;
//    edTxtValor.Visible = false;
//    edNumValor.Visible = true;
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        edNumValor.Value = i.VALORFOB;
//    }
//    catch { }

//    break;
//#endregion
//#region Flete
//case "Flete":
//    edCmbValor.Visible = false;
//    edTxtValor.Visible = false;
//    edNumValor.Visible = true;
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        edNumValor.Value = i.FLETE;
//    }
//    catch { }

//    break;
//#endregion
//#region Seguro
//case "Seguro":
//    edCmbValor.Visible = false;
//    edTxtValor.Visible = false;
//    edNumValor.Visible = true;
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        edNumValor.Value = i.SEGURO;
//    }
//    catch { }

//    break;
//#endregion
//#region Otros
//case "Otros":
//    edCmbValor.Visible = false;
//    edTxtValor.Visible = false;
//    edNumValor.Visible = true;
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        edNumValor.Value = i.OTROS;
//    }
//    catch { }

//    break;
//#endregion
//#region CantidadBultos
//case "CantidadBultos":
//    edCmbValor.Visible = false;
//    edTxtValor.Visible = false;
//    edNumValor.Visible = true;
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        edNumValor.Value = i.CANTIDADBULTOS;
//    }
//    catch { }

//    break;
//#endregion
//#region PesoKgs
//case "PesoKgs":
//    edCmbValor.Visible = false;
//    edTxtValor.Visible = false;
//    edNumValor.Visible = true;
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        edNumValor.Value = i.PESOKGS;
//    }
//    catch { }

//    break;
//#endregion
//#region Producto
//case "Producto":
//    edCmbValor.Visible = false;
//    edTxtValor.Visible = true;
//    edNumValor.Visible = false;
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        edTxtValor.Text = i.PRODUCTO;
//    }
//    catch { }

//    break;
//#endregion
//#region PaisProcedencia
//case "PaisProcedencia":
//    edCmbValor.Visible = true;
//    edTxtValor.Visible = false;
//    edNumValor.Visible = false;
//    co.loadAllCampos("PAISES");
//    edCmbValor.DataTextField = co.TABLA.Columns["Descripcion"].ToString();
//    edCmbValor.DataValueField = co.TABLA.Columns["Codigo"].ToString();
//    edCmbValor.DataSource = co.TABLA;
//    edCmbValor.DataBind();
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        edCmbValor.SelectedValue = i.CODPAISPROCEDENCIA;
//    }
//    catch { }
//    break;
//#endregion
//#region PaisOrigen
//case "PaisOrigen":
//    edCmbValor.Visible = true;
//    edTxtValor.Visible = false;
//    edNumValor.Visible = false;
//    co.loadAllCampos("PAISES");
//    edCmbValor.DataTextField = co.TABLA.Columns["Descripcion"].ToString();
//    edCmbValor.DataValueField = co.TABLA.Columns["Codigo"].ToString();
//    edCmbValor.DataSource = co.TABLA;
//    edCmbValor.DataBind();
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        edCmbValor.SelectedValue = i.CODPAISORIGEN;
//    }
//    catch { }
//    break;
//#endregion
//#region PaisDestino
//case "PaisDestino":
//    edCmbValor.Visible = true;
//    edTxtValor.Visible = false;
//    edNumValor.Visible = false;
//    co.loadAllCampos("PAISES");
//    edCmbValor.DataTextField = co.TABLA.Columns["Descripcion"].ToString();
//    edCmbValor.DataValueField = co.TABLA.Columns["Codigo"].ToString();
//    edCmbValor.DataSource = co.TABLA;
//    edCmbValor.DataBind();
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        edCmbValor.SelectedValue = i.CODPAISDESTINO;
//    }
//    catch { }
//    break;
//#endregion
//#region CiudadDestino
//case "CiudadDestino":
//    edCmbValor.Visible = false;
//    edTxtValor.Visible = true;
//    edNumValor.Visible = false;
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        edTxtValor.Text = i.CIUDADDESTINO;
//    }
//    catch { }

//    break;
//#endregion
//#region TipoTransporte
//case "TipoTransporte":
//    edCmbValor.Visible = true;
//    edTxtValor.Visible = false;
//    edNumValor.Visible = false;
//    co.loadAllCampos("TIPOTRANSPORTE");
//    edCmbValor.DataTextField = co.TABLA.Columns["Descripcion"].ToString();
//    edCmbValor.DataValueField = co.TABLA.Columns["Codigo"].ToString();
//    edCmbValor.DataSource = co.TABLA;
//    edCmbValor.DataBind();
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        edCmbValor.SelectedValue = i.CODTIPOTRANSPORTE;
//    }
//    catch { }
//    break;
//#endregion
//#region TipoCargamento
//case "TipoCargamento":
//    edCmbValor.Visible = true;
//    edTxtValor.Visible = false;
//    edNumValor.Visible = false;
//    co.loadAllCampos("TIPOCARGAMENTO");
//    edCmbValor.DataTextField = co.TABLA.Columns["Descripcion"].ToString();
//    edCmbValor.DataValueField = co.TABLA.Columns["Codigo"].ToString();
//    edCmbValor.DataSource = co.TABLA;
//    edCmbValor.DataBind();
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        edCmbValor.SelectedValue = i.CODTIPOCARGAMENTO;
//    }
//    catch { }
//    break;
//#endregion
//#region TipoCarga
//case "TipoCarga":
//    edCmbValor.Visible = true;
//    edTxtValor.Visible = false;
//    edNumValor.Visible = false;
//    co.loadAllCampos("TIPOCARGA");
//    edCmbValor.DataTextField = co.TABLA.Columns["Descripcion"].ToString();
//    edCmbValor.DataValueField = co.TABLA.Columns["Codigo"].ToString();
//    edCmbValor.DataSource = co.TABLA;
//    edCmbValor.DataBind();
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        edCmbValor.SelectedValue = i.CODTIPOCARGA;
//    }
//    catch { }
//    break;
//#endregion
//#region NoCorrelativo
//case "NoCorrelativo":
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        if (!String.IsNullOrEmpty(i.NOCORRELATIVO.Trim()))
//        {
//            edTxtValor.Text = i.NOCORRELATIVO;
//            edCmbValor.Visible = false;
//            edTxtValor.Visible = true;
//            edNumValor.Visible = false;
//        }
//        else
//        {
//            edCmbValor.Visible = false;
//            edTxtValor.Visible = false;
//            edNumValor.Visible = false;
//            registrarMensaje("No. Correlativo todavia no ha sido ingresado");
//        }
//    }
//    catch { }

//    break;
//#endregion
//#region Aforador
//case "Aforador":
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        if (!String.IsNullOrEmpty(i.AFORADOR.Trim()))
//        {
//            u.loadUsuariosNombreCompletoXAduana(i.CODIGOADUANA, "Aforadores");
//            edCmbValor.DataTextField = u.TABLA.Columns["NombreCompleto"].ToString();
//            edCmbValor.DataValueField = u.TABLA.Columns["IdUsuario"].ToString();
//            edCmbValor.DataSource = u.TABLA;
//            edCmbValor.DataBind();
//            edCmbValor.SelectedValue = i.AFORADOR;
//            edCmbValor.Visible = true;
//            edTxtValor.Visible = false;
//            edNumValor.Visible = false;
//        }
//        else
//        {
//            edCmbValor.Visible = false;
//            edTxtValor.Visible = false;
//            edNumValor.Visible = false;
//            registrarMensaje("Aforador todavia no ha sido ingresado");
//        }
//    }
//    catch { }
//    break;
//#endregion
//#region Color
//case "Color":
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        if (!String.IsNullOrEmpty(i.COLOR.Trim()))
//        {
//            edTxtValor.Text = i.COLOR;
//            edCmbValor.Visible = false;
//            edTxtValor.Visible = true;
//            edNumValor.Visible = false;
//        }
//        else
//        {
//            edCmbValor.Visible = false;
//            edTxtValor.Visible = false;
//            edNumValor.Visible = false;
//            registrarMensaje("Color todavia no ha sido ingresado");
//        }
//    }
//    catch { }

//    break;
//#endregion
//#region Tramitador
//case "Tramitador":
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        if (!String.IsNullOrEmpty(i.TRAMITADOR.Trim()))
//        {
//            u.loadUsuariosNombreCompletoXAduana(i.CODIGOADUANA, "Tramitadores");
//            edCmbValor.DataTextField = u.TABLA.Columns["NombreCompleto"].ToString();
//            edCmbValor.DataValueField = u.TABLA.Columns["IdUsuario"].ToString();
//            edCmbValor.DataSource = u.TABLA;
//            edCmbValor.DataBind();
//            edCmbValor.SelectedValue = i.TRAMITADOR;
//            edCmbValor.Visible = true;
//            edTxtValor.Visible = false;
//            edNumValor.Visible = false;
//        }
//        else
//        {
//            edCmbValor.Visible = false;
//            edTxtValor.Visible = false;
//            edNumValor.Visible = false;
//            registrarMensaje("Tramitador todavia no ha sido ingresado");
//        }
//    }
//    catch { }
//    break;
//#endregion
//#region Impuesto
//case "Impuesto":
//    try
//    {
//        i.loadInstruccion(idInstruccion);
//        if (!String.IsNullOrEmpty(i.IMPUESTO.ToString()))
//        {
//            edNumValor.Value = i.IMPUESTO;
//            edCmbValor.Visible = false;
//            edTxtValor.Visible = false;
//            edNumValor.Visible = true;
//        }
//        else
//        {
//            edCmbValor.Visible = false;
//            edTxtValor.Visible = false;
//            edNumValor.Visible = false;
//            registrarMensaje("Impuesto todavia no ha sido ingresado");
//        }
//    }
//    catch { }

//    break;
//#endregion
//    default:

//        break;
//}