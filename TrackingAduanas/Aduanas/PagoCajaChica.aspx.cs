﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//using Microsoft.SharePoint.Client;
//using SP = Microsoft.SharePoint.Client;
using System.IO;
using System.Text;
//using System.Net.Http;
//using System.Net.Http.Headers;
using Telerik.Web.UI;
using System.Configuration;

public partial class SharePointUpload : System.Web.UI.Page
{
    //protected static string port = "";
    //protected static string libreria = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            try
            {
                if (rbtoriginal.Checked == true)
                {
                    Session["Radio"] = "O";
                }
                else
                {
                    Session["Radio"] = "C";
                }
                
                
            }
            catch (Exception)
            {
            }
        }
    }
    protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
    {
        this.rbtescaneado.Checked = false;
        if (rbtoriginal.Checked == true)
        {
            Session["Radio"] = "O";
        }
        else
        {
            Session["Radio"] = "C";
        }
    }
    protected void rbtescaneado_CheckedChanged(object sender, EventArgs e)
    {
        this.rbtoriginal.Checked = false;
        if (rbtescaneado.Checked == true)
        {
            Session["Radio"] = "C";
        }
        else
        {
            Session["Radio"] = "O";
        }
    }
    protected void RadButton1_Click(object sender, EventArgs e)
    {
        Session["Recibo"] = txtrecibo.Text;
        
        if (!ClientScript.IsStartupScriptRegistered("CloseWindow"))
        Page.ClientScript.RegisterStartupScript(GetType(), "CloseWindow", "Close();", true);
    }
}