﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.OleDb;

using Telerik.Web.UI;
using System.Reflection;
using System.Text;

public partial class GenerarArchivoSAPDinant : Utilidades.PaginaBase
{
    /*protected override void OnLoad(EventArgs e)
    {
        MODULO = "ReporteHagamodaPromoda";
        base.OnLoad(e);
    }*/

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Correcciones", "Reporte Correcciones");
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            c.loadCodigosXCategoriaCodigos("ESPECIESFISCALES", "'DA','DVA','F','P','DTI','HN-MI','HN-MN','SS','ZL'");
            cmbEspecie.Dispose();
            cmbEspecie.ClearSelection();
            cmbEspecie.DataSource = c.TABLA;
            cmbEspecie.DataBind();
        }

    }
    protected bool Consultar { get { return tienePermiso("Consultar"); } }

    protected void rgGuias_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGrid();
    }


    private void ConfigureExport()
    {
        String filename = "Carga_" + DateTime.Now.ToShortDateString();
        rgCorreccion.ExportSettings.FileName = filename;
        rgCorreccion.ExportSettings.ExportOnlyData = true;
        rgCorreccion.ExportSettings.IgnorePaging = true;
        rgCorreccion.ExportSettings.OpenInNewWindow = true;
        rgCorreccion.MasterTableView.ExportToExcel();
    }

    public override bool CanGoBack { get { return false; } }

    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {

        llenarGrid();
    }


    private void llenarGrid()
    {

        try
        {
            conectar();



            int edCantidad;

            DataTable tabla = new DataTable();
            DataRow fila;

            tabla.Columns.Add("HojadeRuta", typeof(String));
            tabla.Columns.Add("Cliente", typeof(String));
            tabla.Columns.Add("Aduana", typeof(String));
            tabla.Columns.Add("Serie", typeof(String));
            tabla.Columns.Add("Especie", typeof(String));
            tabla.Columns.Add("Fecha", typeof(String));

            #region Carga Uso
            if (chkUso.Checked == true)
            {
                EspeciesFiscalesInstruccionesBO EF = new EspeciesFiscalesInstruccionesBO(logApp);
                EF.CargarUsoNohelia(rdpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), rdpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"), cmbEspecie.SelectedValue);
                for (int j = 0; j < EF.totalRegistros; j++)
                {
                    string codEspecieFiscal = EF.TABLA.Rows[j]["Serie"].ToString();
                    string serie = "";
                    string[] arreglo = codEspecieFiscal.Trim().Split(",".ToCharArray());
                    edCantidad = arreglo.Length;
                    for (int i = 0; i < edCantidad; i++)
                    {
                        serie = arreglo[i];
                        fila = tabla.NewRow();
                        fila["HojadeRuta"] = EF.TABLA.Rows[j]["Hoja"].ToString();
                        fila["Cliente"] = EF.TABLA.Rows[j]["Cliente"].ToString();
                        fila["Aduana"] = EF.TABLA.Rows[j]["Aduana"].ToString();
                        fila["Serie"] = serie;
                        fila["Especie"] = EF.TABLA.Rows[j]["Especie"].ToString();
                        fila["Fecha"] = EF.TABLA.Rows[j]["Fecha"].ToString();
                        tabla.Rows.Add(fila);
                    }
                    EF.regSiguiente();
                }
            }
            else if (chkTodo.Checked == true)
            {
                EspeciesFiscalesInstruccionesBO EF = new EspeciesFiscalesInstruccionesBO(logApp);
                EF.CargarUsoNohelia(rdpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), rdpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"));
                for (int j = 0; j < EF.totalRegistros; j++)
                {
                    string codEspecieFiscal = EF.TABLA.Rows[j]["Serie"].ToString();
                    string serie = "";
                    string[] arreglo = codEspecieFiscal.Trim().Split(",".ToCharArray());
                    edCantidad = arreglo.Length;
                    for (int i = 0; i < edCantidad; i++)
                    {
                        serie = arreglo[i];
                        fila = tabla.NewRow();
                        fila["HojadeRuta"] = EF.TABLA.Rows[j]["Hoja"].ToString();
                        fila["Cliente"] = EF.TABLA.Rows[j]["Cliente"].ToString();
                        fila["Aduana"] = EF.TABLA.Rows[j]["Aduana"].ToString();
                        fila["Serie"] = serie;
                        fila["Especie"] = EF.TABLA.Rows[j]["Especie"].ToString();
                        fila["Fecha"] = EF.TABLA.Rows[j]["Fecha"].ToString();
                        tabla.Rows.Add(fila);
                    }
                    EF.regSiguiente();
                }
            }
            else
            {
                CorreccionesBO C = new CorreccionesBO(logApp);
                C.CargaCorrecciones(rdpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), rdpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"));

                for (int j = 0; j < C.totalRegistros; j++)
                {
                    string codEspecieFiscal = C.TABLA.Rows[j]["Serie"].ToString();
                    string serie = "";
                    string[] arreglo = codEspecieFiscal.Trim().Split(",".ToCharArray());
                    edCantidad = arreglo.Length;
                    for (int i = 0; i < edCantidad; i++)
                    {
                        serie = arreglo[i];
                        fila = tabla.NewRow();
                        fila["HojadeRuta"] = C.TABLA.Rows[j]["Hoja"].ToString();
                        fila["Cliente"] = C.TABLA.Rows[j]["Cliente"].ToString();
                        fila["Aduana"] = C.TABLA.Rows[j]["Aduana"].ToString();
                        fila["Serie"] = serie;
                        fila["Especie"] = C.TABLA.Rows[j]["Especie"].ToString();
                        tabla.Rows.Add(fila);
                    }
                    C.regSiguiente();
                }
            }



            #endregion

            rgCorreccion.DataSource = tabla;
            rgCorreccion.DataBind();

        }
        catch { }

        finally { desconectar(); }

    }



    protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName)
                ConfigureExport();
        }
        catch (Exception)
        { }
    }
    protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenarGrid();
    }
}

