﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.OleDb;

using Telerik.Web.UI;
//using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;

public partial class GenerarArchivoSAPDinant : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Reporte Cargill";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Hagamoda/Promoda", "Cargill");
        }
    }
    protected bool Consultar { get { return tienePermiso("Consultar"); } }
   
        private void ConfigureExport()
        {
            String filename = "Reporte_" + DateTime.Now.ToShortDateString();
            RadGrid1.ExportSettings.FileName = filename;
            RadGrid1.ExportSettings.ExportOnlyData = true;
            RadGrid1.ExportSettings.IgnorePaging = true;
            RadGrid1.ExportSettings.OpenInNewWindow = true;
            RadGrid1.MasterTableView.ExportToExcel();
        }

    public override bool CanGoBack { get { return false; } }
    
    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
                llenarGrid();
    }
    private void llenarGrid() 
    {

        try 
        {
            conectar();
            InstruccionesBO bo = new InstruccionesBO(logApp);
            bo.ReporteCargill(dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"));
            RadGrid1.DataSource = bo.TABLA;
            RadGrid1.DataBind();
        }
        catch { }

        finally { desconectar(); }

    }



    protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
    {
                try
        {
            if (e.CommandName == RadGrid.ExportToExcelCommandName || e.CommandName == RadGrid.ExportToCsvCommandName)
                ConfigureExport();
        }
        catch (Exception)
        { }
    }
    protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenarGrid();
    }
}

