﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.OleDb;
using System.Text;
using System.Configuration;
using Telerik.Web.UI;

public partial class ReporteGestionCarga : Utilidades.PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgRemisiones.FilterMenu);
        rgRemisiones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgRemisiones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte de Remisiones", "Reporte de Remisiones");
            cargarDatosInicio();
        }
    }

    protected void cargarDatosInicio()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (u.CODPAIS == "H")
                c.loadPaisesHojaRuta();
            else
                c.loadAllCampos("PAISES", u.CODPAIS);
            cmbPais.DataSource = c.TABLA;
            cmbPais.DataBind();
        }
        catch { }
    }

    public override bool CanGoBack { get { return false; } }

    private void Salir()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("top.location.href = LoginTracking.aspx;");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Salir", sb.ToString(), false);
    }

    #region Remisiones
    protected void rgRemisiones_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridInstrucciones();
    }

    private void llenarGridInstrucciones()
    {
        try
        {
            conectar();
            RemisionesBO R = new RemisionesBO(logApp);
            R.LoadRemisionXPais(dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"), cmbPais.SelectedValue+"-");
            rgRemisiones.DataSource = R.TABLA;
            rgRemisiones.DataBind();
        }
        catch (Exception) { }
        finally
        {
            desconectar();
        }
    }

    protected void rgRemisiones_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();
            if (rgRemisiones.Items.Count <= 0 & e.CommandName == RadGrid.ExportToExcelCommandName)
                e.Canceled = true;
            else if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                ConfigureExport();
            }
        }
        catch (Exception)
        { }
    }

    private void ConfigureExport()
    {
        String filename = "Remisiones" + "_" + DateTime.Now.ToShortDateString();
        rgRemisiones.GridLines = GridLines.Both;
        rgRemisiones.ExportSettings.FileName = filename;
        rgRemisiones.ExportSettings.IgnorePaging = true;
        rgRemisiones.ExportSettings.OpenInNewWindow = false;
        rgRemisiones.ExportSettings.ExportOnlyData = true;
    }
    #endregion

    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        if (dpFechaInicio.SelectedDate <= dpFechaFinal.SelectedDate)
        {
            llenarGridInstrucciones();
            rgRemisiones.Visible = true;
        }
        else
        {
            registrarMensaje2("Fecha inicio no puede ser mayor que fecha final");
            rgRemisiones.Visible = false;
        }
    }

    private void registrarMensaje2(string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("alert('" + mensaje + "');");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "mensaje", sb.ToString(), false);
    }

    protected void rgRemisiones_Init(object sender, System.EventArgs e)
    {
        GridFilterMenu menu = rgRemisiones.FilterMenu;
        menu.Items.RemoveAt(rgRemisiones.FilterMenu.Items.Count - 2);
    }
}