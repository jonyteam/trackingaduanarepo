﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Principal.aspx.cs" Inherits="Principal" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Menú Principal</title>
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
    <link href="~/Styles/StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="Form1" runat="server">
        <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnableScriptCombine="False">
        </telerik:RadScriptManager>
        <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
        </telerik:RadStyleSheetManager>
        <telerik:RadFormDecorator ID="RadFormDecorator1" runat="server" DecoratedControls="All"
            EnableAjaxSkinRendering="true" EnableEmbeddedScripts="true" />
        <telerik:RadWindowManager ID="RadWindowManager1" runat="server" Skin="">
        </telerik:RadWindowManager>
        <telerik:RadToolTipManager runat="server" ID="RadToolTipManager1" AutoTooltipify="true"
            Position="TopRight" Skin="Default" />
        <script type="text/javascript">

            function itemOpened(s, e) {
                if ($telerik.isIE8 || $telerik.isIE9) {
                    // Fix an IE 8 bug that causes the list bullets to disappear (standards mode only)
                    $telerik.$("li", e.get_item().get_element())
             .each(function () { this.style.cssText = this.style.cssText; });
                }
            }

        </script>
        <div class="page" style="width: auto">
            <div class="header">
                <div class="title">
                    <h1>Aduanas
                    </h1>
                </div>
                <div class="loginDisplay">
                    <asp:LoginView ID="HeadLoginView" runat="server" EnableViewState="false">
                        <AnonymousTemplate>
                            [ <a href="~/LoginTracking.aspx" id="HeadLoginStatus" runat="server">Log In</a>
                            ]
                        </AnonymousTemplate>
                        <LoggedInTemplate>
                            Bienvenido <span class="bold">
                                <asp:LoginName ID="HeadLoginName" runat="server" ForeColor="Black" />
                            </span>! [
                        <asp:LoginStatus ID="HeadLoginStatus" runat="server" LogoutAction="Redirect" LogoutText="Salir"
                            LogoutPageUrl="~/" />
                            ]
                        </LoggedInTemplate>
                    </asp:LoginView>
                </div>
                <div class="clear hideSkiplink">
                    <telerik:RadMenu runat="server" ID="RadMenuPrincipal" Skin="Office2010Blue" OnClientItemOpened="itemOpened"
                        ClickToOpen="false" Width="100%" Height="60px" EnableShadows="true" Style="top: 0px; left: 0px">
                        <Items>
                            <telerik:RadMenuItem Name="Mantenimiento" Text="Mantenimiento" PostBack="false">
                                <Items>
                                    <telerik:RadMenuItem Name="Mantenimiento" CssClass="Products" Width="900px">
                                        <ItemTemplate>
                                            <div id="CatWrapper" class="Wrapper" style="width: 700px;">
                                                <h3 style="color: White;">Categorias</h3>
                                                <telerik:RadSiteMap ID="RadSiteMap1" runat="server" Skin="Default">
                                                    <LevelSettings>
                                                        <telerik:SiteMapLevelSetting Level="0">
                                                            <ListLayout RepeatColumns="3" RepeatDirection="Vertical" />
                                                        </telerik:SiteMapLevelSetting>
                                                    </LevelSettings>
                                                    <Nodes>
                                                        <telerik:RadSiteMapNode NavigateUrl="#" Text="Comunes">
                                                            <Nodes>
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="MantenimientoCodigos.aspx"
                                                                    Text="Categorias y Codigos" Value="Mantenimiento Códigos" />
                                                            </Nodes>
                                                            <Nodes>
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="HabilitarEspecies.aspx"
                                                                    Text="Habilitar Especies Fiscales" Value="Habilitar Especies" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="CambioFecha.aspx"
                                                                    Text="Mantenimiento Cambio Fecha" Value="Mantenimiento Facturacion" />
                                                            </Nodes>
                                                        </telerik:RadSiteMapNode>
                                                        <telerik:RadSiteMapNode NavigateUrl="#" Text="Seguridad">
                                                            <Nodes>
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="MantenimientoUsuarios.aspx"
                                                                    Text="Usuarios" Value="Mantenimiento Usuarios" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="MantenimientoModPerRol.aspx"
                                                                    Text="Módulos Permisos y Perfiles" Value="Mantenimiento Módulos Permisos y Perfiles" />
                                                            </Nodes>
                                                        </telerik:RadSiteMapNode>
                                                        <telerik:RadSiteMapNode NavigateUrl="#" Text="Generales">
                                                            <Nodes>
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="MantenimientoCambiarClave.aspx"
                                                                    Text="Cambiar Clave" Value="Todos" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="MantenimientoClientes.aspx"
                                                                    Text="Clientes" Value="Mantenimiento Clientes" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="MantenimientoAduanas.aspx"
                                                                    Text="Aduanas" Value="Mantenimiento Aduanas" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="MantenimientoFlujoCarga.aspx"
                                                                    Text="Flujo Carga" Value="Mantenimiento Flujo Carga" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="MantenimientoDocumentosRegimen.aspx"
                                                                    Text="Documentos Regimen" Value="Mantenimiento Documentos Regimen" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="IngresoTarifas.aspx"
                                                                    Text="Ingreso Tarifas UNO" Value="Todos" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="TasaCambioPaises.aspx"
                                                                    Text="Tasa Cambio Paises" Value="Tasa Cambio" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="MantenimientoSolicitudes.aspx"
                                                                    Text="Solicitud de Pagos" Value="Mantenimiento Solicitud" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="MantenimientoSolicitudesGastos.aspx"
                                                                    Text="Mantenimientos Gastos" Value="Mantenimiento Solicitud" />
                                                              <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="MantenimientoRemisionesAduanas.aspx"
                                                                    Text="Mantenimiento Remisiones Aduanas" Value="Mantenimiento Remisiones Aduanas" />
                                                              <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="MantenimientoReferenciaFacturacion.aspx"
                                                                    Text="Mantenimiento Facturacion" Value="Mantenimiento Facturacion" />
                                                              <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="MantenimientoCambioEstado.aspx"
                                                                    Text="Mantenimiento Cambio de Estado" Value="Mantenimiento Facturacion" />
                                                              
                                                            
                                                            </Nodes>
                                                        </telerik:RadSiteMapNode>
                                                    </Nodes>
                                                </telerik:RadSiteMap>
                                            </div>
                                            <div id="FeatProduct">
                                                <h3>Vesta Logistic</h3>
                                                <img src="Images/Inversion.JPG" alt="Vesta Logistic" width="128px" height="150px" />
                                                <p>
                                                    Aduanas
                                                <br />
                                                    <%-- <span class="price">usuario:
                                                     <%= Session["Usuario"].ToString()%>
                                                </span>--%>
                                                </p>
                                            </div>
                                        </ItemTemplate>
                                    </telerik:RadMenuItem>
                                </Items>
                            </telerik:RadMenuItem>
                            <telerik:RadMenuItem Text="Operaciones" Name="Operaciones" PostBack="false" Owner="RadMenuPrincipal">
                                <Items>
                                    <telerik:RadMenuItem CssClass="Stores" Name="Operaciones" Width="750px">
                                        <ItemTemplate>
                                            <div id="StoreWrapper" class="Wrapper" style="width: 710px;">
                                                <h3 style="color: White;">Alrededor del Mundo</h3>
                                                <img name="world" src="Img/world.gif" width="355" height="210" border="0" id="world"
                                                    usemap="#m_world" alt="world" />
                                                <map name="m_world" id="m_world">
                                                    <area shape="circle" coords="309,171, 5" href="#" alt="New Zealand" />
                                                    <area shape="circle" coords="278,153, 5" href="#" alt="Australia" />
                                                    <area shape="circle" coords="272,116, 5" href="#" alt="Philippines" />
                                                    <area shape="circle" coords="255,128, 5" href="#" alt="Malaysia" />
                                                    <area shape="circle" coords="234,105, 5" href="#" alt="India" />
                                                    <area shape="circle" coords="200,98, 5" href="#" alt="Middle East" />
                                                    <area shape="circle" coords="173,80, 5" href="#" alt="Europe" />
                                                    <area shape="circle" coords="161,73, 5" href="#" alt="United Kingdom" />
                                                    <area shape="circle" coords="85,91, 5" href="#" alt="United States" />
                                                    <area shape="circle" coords="80,73, 5" href="#" alt="Canada" />
                                                </map>
                                                <telerik:RadSiteMap ID="RadSiteMap2" runat="server" Skin="Default" Style="width: 350px;">
                                                    <LevelSettings>
                                                        <telerik:SiteMapLevelSetting Level="0">
                                                            <ListLayout RepeatColumns="2" RepeatDirection="Vertical" />
                                                        </telerik:SiteMapLevelSetting>
                                                    </LevelSettings>
                                                    <Nodes>
                                                        <telerik:RadSiteMapNode>
                                                            <Nodes>
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="ComprasEspeciesFiscales.aspx"
                                                                    Text="Compras Especies Fiscales" Value="Compras Especies Fiscales" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="EnviosEspeciesFiscales.aspx"
                                                                    Text="Envios Especies Fiscales" Value="Envios Especies Fiscales" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="ReciboEspeciesFiscales.aspx"
                                                                    Text="Recibo Especies Fiscales" Value="Recibo Especies Fiscales" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="AnulacionEspeciesFiscales.aspx"
                                                                    Text="Anulacion Especies Fiscales" Value="Anulacion Especies Fiscales" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="EspeciesFiscalesClientes.aspx"
                                                                    Text="Especies Fiscales Clientes" Value="Especies Fiscales Clientes" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="InventarioUsuarios.aspx"
                                                                    Text="Inventario Aduana" Value="Inventario Usuarios" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="CrearInstrucciones.aspx"
                                                                    Text="Crear Instrucciones" Value="Crear Instrucciones" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="InstruccionesCargil.aspx"
                                                                    Text="Crear Instruccion Cargill" Value="Instrucciones Cargill" />

                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="EditarInstrucciones.aspx"
                                                                    Text="Editar Instrucciones" Value="Editar Instrucciones" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="EditarTiemposInstrucciones.aspx"
                                                                    Text="Editar Tiempos Instrucciones" Value="Editar Tiempos Instrucciones" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="EventosInicio.aspx"
                                                                    Text="Iniciar Eventos" Value="EventosInicio" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="EventosFin.aspx" Text="Finalizar Eventos"
                                                                    Value="EventosFin" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="SolicitudPagos.aspx"
                                                                    Text="Solicitud Pagos" Value="Solicitud Pago" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="SolicitudPagosGT-SV.aspx"
                                                                    Text="Solicitud Pagos" Value="Solicitud Pago2" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="SolicitudPagosSinFactura.aspx"
                                                                    Text="Solicitud Pagos Con Factura" Value="Solicitud Pago" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="ConfirmarSolicitudPagos.aspx"
                                                                    Text="Confirmar Pagos" Value="Confirmar Pagos" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="CargaCXP.aspx" Text="Cargar CXP"
                                                                    Value="Cargar CXP" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="PagoTesoreria.aspx"
                                                                    Text="Pago Tesoreria" Value="Pago Tesoreria" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="RecuperarDocumentos.aspx"
                                                                    Text="Recuperar Documentos" Value="Recuperar Documetos" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="EnvioFacturacion.aspx"
                                                                    Text="Envio Facturacion" Value="Envio Facturacion" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="InformeGastos.aspx"
                                                                    Text="Informe de Gastos" Value="Envio Facturacion" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="GestionDocumentos.aspx"
                                                                    Text="Gestion Documentos" Value="Gestion Documentos" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="GestionCarga.aspx"
                                                                    Text="Gestión Carga" Value="GestionCarga" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="CargarArchivo.aspx"
                                                                    Text="Cargar Documentos" Value="Cargar Documentos" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="DescargarArchivo.aspx"
                                                                    Text="Descargar Documentos" Value="Descargar Documentos" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="IngreoDatosUNO.aspx"
                                                                    Text="Ingreso Datos UNO" Value="Ingreso UNO" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="FacturasCargill.aspx"
                                                                    Text="Cargar Facturas Cargill" Value="Facturas Cargill" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="FacturasCerveceria.aspx"
                                                                    Text="Facturas CHSA" Value="Facturas Cerveceria Hondureña" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="EnviarRemision.aspx"
                                                                    Text="Envio de Remisiones" Value="Enviar Remision" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="ReenviarRemision.aspx"
                                                                    Text="Reenvio de Remisiones " Value="Reenvio Remision" />                                                       
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="LiberarEspeciesFiscales.aspx"
                                                                    Text="Liberar Especies" Value="Liberar Especies" />
                                                                 <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="GuiaRemision.aspx"
                                                                    Text="Guia  de Remision" Value="Guia  de Remision" />
                                                                  <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="FcAseguramientoDeServicio.aspx"
                                                                    Text="Aseguramiento De Servicio" Value="Aseguramiento De Servicio" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="FcAsistenteDeOperaciones.aspx"
                                                                    Text="Asistente de Operaciones" Value="Asistente de Operaciones" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="FcJefeDeAduana.aspx"
                                                                    Text="Jefe De Aduana" Value="Jefe De Aduana" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="FcJefeDeAduanaPuerto.aspx"
                                                                    Text="Jefe De Aduana Puerto" Value="Jefe De Aduana Puerto" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="FcCargarArchivoSelloTiempos.aspx"
                                                                    Text="Cargar Archivo Sello Tiempos" Value="Cargar Archivo Sello Tiempos" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="Notas.aspx"
                                                                    Text="Notas" Value="Notas" />
                                                                <telerik:RadSiteMapNode Target="iframePrincipal" NavigateUrl="InicioDeResponsabilidad.aspx"
                                                                    Text="Inicio de Responsabilidad" Value="Inicio Responsabilidad" />

                                                            </Nodes>
                                                        </telerik:RadSiteMapNode>
                                                        <telerik:RadSiteMapNode>
                                                            <Nodes>
                                                            </Nodes>
                                                        </telerik:RadSiteMapNode>
                                                    </Nodes>
                                                </telerik:RadSiteMap>
                                            </div>
                                        </ItemTemplate>
                                    </telerik:RadMenuItem>
                                </Items>
                            </telerik:RadMenuItem>
                            <telerik:RadMenuItem Text="Consultas" PostBack="false" Owner="RadMenuPrincipal">
                                <Items>
                                    <telerik:RadMenuItem CssClass="News" Width="750px">
                                        <ItemTemplate>
                                            <div id="NewsWrapper" class="Wrapper">
                                                <h3 style="color: White;">Tipos de Busqueda</h3>
                                                <div class="newsLeft">
                                                    <img src="Img/TrackingTerrestre.png" width="65%" height="30%" alt="Logistics" />
                                                    <h2>
                                                        <a href="#">Vesta Logistics - Aduanas</a></h2>
                                                    <span>Creado Diciembre/2011</span>
                                                    <%--<p>
                                                    Division especializada en brindar transporte terrestre de mercadería de origen a
                                                    destino final de una manera rápida, segura y confiable mediante controles e inspecciones
                                                    estrictas y monitoreo satelital permanente optimizando rutas a costos competitivos.</p>--%>
                                                </div>
                                                <table cellspacing="0" cellpadding="0" width="290px">
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="InstruccionesFinalizadas.aspx" target="iframePrincipal" class="newsLink">Instrucciones
                                                            Finalizadas</a><br />
                                                            Generar reporte de las instrucciones finalizadas.
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="BusquedaAvanzada.aspx" target="iframePrincipal" class="newsLink">Búsqueda Avanzada</a><br />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteEmbarque.aspx" target="iframePrincipal" class="newsLink">Reporte Embarque</a>
                                                            <br />
                                                            Generar un archivo en Excel con informacion del embarque.
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteGestionCarga.aspx" target="iframePrincipal" class="newsLink">Reporte
                                                            Gestión Carga</a>
                                                            <br />
                                                            Generar un archivo en Excel con la gestión de la carga.
                                                        </td>
                                                    </tr>
                                                          <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteGestionCargaAutomatica.aspx" target="iframePrincipal" class="newsLink">Reporte
                                                            Gestión Cargas Automáticas</a>
                                                            <br />
                                                            Generar un archivo en Excel con la gestión de las cargas automaticas.
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteGerencial.aspx" target="iframePrincipal" class="newsLink">Reporte Gerencial
                                                            </a>
                                                            <br />
                                                            Generar reporte de cantidad de embarques.
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteEventos.aspx" target="iframePrincipal" class="newsLink">Reporte Eventos
                                                            </a>
                                                            <br />
                                                            Generar reporte de eventos por instrucción
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteBitacora.aspx" target="iframePrincipal" class="newsLink">Reporte Bitacora
                                                            </a>
                                                            <br />
                                                            Generar reporte de bitacora por instruccón
                                                        </td>
                                                    </tr>

                                                       <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteDiarioAseguramiento.aspx" target="iframePrincipal" class="newsLink">Reporte Diario Aseguramiento
                                                            </a>
                                                            <br />
                                                            Generar Reporte Diario Aseguramiento
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteSelloTiempos.aspx" target="iframePrincipal" class="newsLink">Reporte Sello de Tiempos
                                                           </a>
                                                            <br />
                                                              Reporte Sello de Tiempos Validacion Electronica y Selectivo
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ConsultasVarias.aspx" target="iframePrincipal" class="newsLink">Consultas Varias
                                                            </a>
                                                            <br />
                                                            Consultas Varias
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteDinant.aspx" target="iframePrincipal" class="newsLink">Reporte Dinant
                                                            </a>
                                                            <br />
                                                            Reporte Dinant
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteMovimientosArancelarios.aspx" target="iframePrincipal" class="newsLink">Reporte Cargill </a>
                                                            <br />
                                                            Reporte Cargill
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteHCCExcell.aspx" target="iframePrincipal" class="newsLink">Reporte HCC
                                                            </a>
                                                            <br />
                                                            Reporte HCC
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteImpuestos.aspx" target="iframePrincipal" class="newsLink">Reporte de
                                                            Impuestos </a>
                                                            <br />
                                                            Reporte de Impuestos
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteHagamoda.aspx" target="iframePrincipal" class="newsLink">Reporte Hagamoda
                                                            </a>
                                                            <br />
                                                            Reporte Hagamoda
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="CargaUso.aspx" target="iframePrincipal" class="newsLink">Carga Uso </a>
                                                            <br />
                                                            Carga de Especies Usadas
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteUNO.aspx" target="iframePrincipal" class="newsLink">Reporte UNO
                                                            </a>
                                                            <br />
                                                            Rerpote de Inventarios
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteGastoAduana.aspx" target="iframePrincipal" class="newsLink">Reporte de Gastos
                                                            </a>
                                                            <br />
                                                            Reporte de Gastos
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="Correcciones.aspx" target="iframePrincipal" class="newsLink">Reporte de Correciones
                                                            </a>
                                                            <br />
                                                            Reporte de Correcciones
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteEventosGestion.aspx" target="iframePrincipal" class="newsLink">Reporte Gestion Carga y Eventos
                                                            </a>
                                                            <br />
                                                            Reporte de Gestion Carga y Eventos
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteRemisiones.aspx" target="iframePrincipal" class="newsLink">Reporte de Remisiones
                                                            </a>
                                                            <br />
                                                            Reporte de Remisiones
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteSeguimientosCalidad.aspx" target="iframePrincipal" class="newsLink">Reporte Seguimientos Calidad
                                                            </a>
                                                            <br />
                                                            Reporte Seguimientos Calidad
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteXGuiaJulio.aspx" target="iframePrincipal" class="newsLink">Reporte Cargill Terrestre
                                                            </a>
                                                            <br />
                                                            Reporte Cargill Terrestre
                                                        </td>
                                                    </tr>
                                                       <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteSeguimientoEspecies.aspx" target="iframePrincipal" class="newsLink">Reporte Seguimiento Especies
                                                            </a>
                                                            <br />
                                                            Reporte Seguimiento Especies
                                                        </td>
                                                    </tr>

                                                        <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteGastosMasivos.aspx" target="iframePrincipal" class="newsLink">Reporte Instruciones Gastos Masivos Nicaragua
                                                            </a>
                                                            <br />
                                                                Reporte de Gasto Generados Autómaticamente Para DINNAT NICARAGUA
                                                        </td>
                                                    </tr>
                                                         <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteGastosCargadosSV.aspx" target="iframePrincipal" class="newsLink">Reporte Gastos Cargados El Salvador
                                                            </a>
                                                            <br />
                                                                Reporte de Gastos Para El Salvador
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteCerveceriaHondureña.aspx" target="iframePrincipal" class="newsLink">Reporte Cervecería Hondureña S.A</a>
                                                            <br />
                                                                Reporte Cervecería Hondureña S.A
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteGeneral.aspx"  target="iframePrincipal" class="newsLink">Reporte Instrucciones Finalizadas.</a>
                                                            <br />
                                                               Reporte Instrucciones Finalizadas.
                                                        </td>
                                                    </tr>
                                                     <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                       <td>
                                                            <a href="ReporteCargilPostFilling.aspx"  target="iframePrincipal" class="newsLink">Reporte Cargill Post-Filing.</a>
                                                            <br />
                                                               Reporte Cargill Post-Filing.
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span>
                                                                <img src="Img/truck_red_16.png" alt="Logistics" /></span>
                                                        </td>
                                                        <td>
                                                            <a href="ReporteCargaTrabajo.aspx"  target="iframePrincipal" class="newsLink">Reporte Carga De Trabajo.</a>
                                                            <br />
                                                               Reporte Carga De Trabajo.
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </ItemTemplate>
                                    </telerik:RadMenuItem>
                                </Items>
                            </telerik:RadMenuItem>
                            <%--                        <telerik:RadMenuItem Text="Events" PostBack="false" Owner="RadMenuPrincipal">
                            <Items>
                                <telerik:RadMenuItem CssClass="Events" Width="640px">
                                    <ItemTemplate>
                                        <div id="EventWrapper" class="Wrapper">
                                            <h3>
                                                Industry Events</h3>
                                            <div class="newsLeft">
                                                <img src="Img/events.jpg" alt="events" />
                                                <h2>
                                                    <a href="#">International Furniture Fair Tokyo (IFFT)</a></h2>
                                                <span>10/02/2010</span>
                                                <p>
                                                    Furniture design event inaugurated in 1979 featuring furniture and interior design
                                                    exhibitors from all over the world.</p>
                                            </div>
                                            <table cellspacing="0" cellpadding="0" width="290px">
                                                <tr>
                                                    <td>
                                                        <span>01/02/2010</span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="eventLink">International Interior & Furniture Exhibition (IFEX)</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>02/02/2010</span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="eventLink">Australian International Furniture Fair </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>03/02/2010</span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="eventLink">Las Vegas Furniture Show </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>04/02/2010</span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="eventLink">Home Fashion & Design Shanghai </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>05/02/2010</span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="eventLink">imm cologne </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>06/02/2010</span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="eventLink">Malaysian International Furniture Fair </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>07/02/2010</span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="eventLink">Salon du Meuble de Paris </a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span>08/02/2010</span>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="eventLink">ICFF: International Contemporary Furniture Fair</a>
                                                    </td>
                                                </tr>
                                            </table>
                                            <a class="moreLink" href="#">View all &raquo;</a>
                                        </div>
                                    </ItemTemplate>
                                </telerik:RadMenuItem>
                            </Items>
                        </telerik:RadMenuItem>--%>
                        </Items>
                    </telerik:RadMenu>
                </div>
            </div>
            <div class="main">
                <iframe id="iframePrincipal" name="iframePrincipal" width="100%" height="510px" src="Default.aspx"></iframe>
            </div>
        </div>
    </form>
</body>
</html>
