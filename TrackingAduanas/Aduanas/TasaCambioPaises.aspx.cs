﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using Telerik.Web.UI;

public partial class MantenimientoUsuarios : Utilidades.PaginaBase
{
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Tasa Cambio";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgTasaCambio.FilterMenu);
        rgTasaCambio.Skin = Skin;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Ingreso Tarifas UNO", "Tasa Cambio Paises");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void SetVisible(TableCellCollection cells, String commandName, bool visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is ImageButton)
                {
                    ImageButton btn = (ImageButton)control;

                    if (btn.CommandName == commandName)
                        btn.Visible = visible;
                }
            }
        }
    }

    #region Tasa Cambio

    private void llenarGrid()
    {
        try
        {
            conectar();
            TipoCambioBO TC = new TipoCambioBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            if (User.IsInRole("Administradores") && User.Identity.Name == "dabonilla" || User.IsInRole("Administradores") && User.Identity.Name == "aalvarado" || User.IsInRole("Administradores") && User.Identity.Name == "lramirez" || User.IsInRole("Administradores") && User.Identity.Name == "mnapky" || User.IsInRole("Administradores") && User.Identity.Name == "gcorrales"|| User.IsInRole("Administradores") && User.Identity.Name == "ycaballero" || User.IsInRole("Administradores") && User.Identity.Name == "aalvarado" || User.IsInRole("Administradores") && User.Identity.Name == "lramirez" || User.IsInRole("Administradores") && User.Identity.Name == "mnapky" || User.IsInRole("Administradores") && User.Identity.Name == "gcorrales")
                TC.LoadTasasAdministrador();
            else if (User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT") || User.IsInRole("Administrador Pais") || User.IsInRole("Contabilidad"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                TC.LoadTasasAdministradorXPais(u.CODPAIS);
            }
            rgTasaCambio.DataSource = TC.TABLA;

        }
        catch (Exception)
        {

        }

        finally { desconectar(); }

    }
    protected Object GetPaises()
    {
        try
        {
            conectar();
            CodigosBO bo = new CodigosBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            if (User.IsInRole("Administradores") && User.Identity.Name == "dabonilla" || User.IsInRole("Administradores") && User.Identity.Name == "aalvarado" || User.IsInRole("Administradores") && User.Identity.Name == "lramirez" || User.IsInRole("Administradores") && User.Identity.Name == "mnapky" || User.IsInRole("Administradores") && User.Identity.Name == "gcorrales"|| User.IsInRole("Administradores") && User.Identity.Name == "ycaballero")
                bo.loadPaisesHojaRuta();
            else if (User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT") || User.IsInRole("Administrador Pais") || User.IsInRole("Contabilidad"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                bo.loadPaiseXHojaRuta(u.CODPAIS);
            }
            return bo.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }
        return null;
    }

    protected void rgUsuarios_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGrid();
    }

    protected bool GetDatosEditados(TipoCambioBO TC, GridItem item)
    {
        bool resp = false;
        GridEditableItem editedItem = item as GridEditableItem;
        RadNumericTextBox Tasa = (RadNumericTextBox)editedItem.FindControl("edTasa");
        RadComboBox Pais = (RadComboBox)editedItem.FindControl("edPais");
        RadDatePicker Fecha = (RadDatePicker)editedItem.FindControl("edFecha");
        TC.ValidarTasa(Fecha.SelectedDate.Value.ToString("yyyy/MM/dd"), Pais.SelectedValue);
        if (Pais.SelectedValue != "S")
        {
            if (TC.totalRegistros <= 0)
            {
                TC.TASACAMBIO = Convert.ToDouble(Tasa.Text);
                TC.FECHA = Fecha.SelectedDate.Value.ToString();
                TC.PAIS = Pais.SelectedValue;
                resp = true;
            }
            else
            {
                registrarMensaje("Para esta fecha " + Fecha.SelectedDate.Value.ToString("yyyy/MM/dd") + " ya se tiene una tasa de cambio, favor ingrese otra fecha");
            }
        }
        else
        {
            registrarMensaje("Para este país no se ha configurado el ingreso de la tasa de cambio favor comuníquese con el administrador del sistema");
        }
        return resp;
    }

    protected void rgUsuarios_InsertCommand(object source, GridCommandEventArgs e)
    {
        try
        {
            conectar();
            GridEditableItem editedItem = e.Item as GridEditableItem;
            TipoCambioBO TC = new TipoCambioBO(logApp);
            TC.loadtasa();
            TC.newLine();
            bool resp = GetDatosEditados(TC, e.Item);
            if (resp)
            {

                TC.IDUSUARIO = Session["IdUsuario"].ToString();
                TC.commitLine();
                TC.actualizar();
                registrarMensaje("Tasa de Cambio ingresada exitosamente");
                llenarBitacora("Se ingresaron las tasas de cambio de " + TC.FECHA + " al " + TC.TASACAMBIO, Session["IdUsuario"].ToString(), "");
            }
        }
        catch (Exception ex)
        {
            logError(ex.Message, Session["IdUsuario"].ToString(), "IngresoTarifas");
        }
        finally
        {
            desconectar();
        }
    }

    #endregion
}
