﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using Telerik.Web.UI;

public partial class MantenimientoUsuarios : Utilidades.PaginaBase
{
    String Prueba;
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Mantenimiento Solicitud";
        base.OnLoad(e);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgGastos.FilterMenu);
        rgGastos.Skin = Skin;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Mantenimiento Proveedores", "Gastos");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
        }
    }
    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }
    protected void SetVisible(TableCellCollection cells, String commandName, bool visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is ImageButton)
                {
                    ImageButton btn = (ImageButton)control;

                    if (btn.CommandName == commandName)
                        btn.Visible = visible;
                }
            }
        }
    }

    #region Gastos
    private void loadGridGastos()
    {
        try
        {
            conectar();
            GatosTracking GT = new GatosTracking(logApp);
            if (User.IsInRole("Administradores"))
            {
                GT.LlenarHojasConGastosMant();
                rgGastos.DataSource = GT.TABLA;
                rgGastos.DataBind();
            }
        }
        catch { }
        finally
        {
            desconectar();
        }
    }
    #endregion
    #region Grid
    protected void rgGastos_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        loadGridGastos();
    }
    protected Object GetGasto()
    {
        try
        {
            conectar();
            GatosTracking GT = new GatosTracking(logApp);
            GT.LlenarGatos(Prueba.Substring(0, 1));

            return GT.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }

        return null;
    }
    protected Object GetMedioPago()
    {
        try
        {
            conectar();
            CodigosBO C = new CodigosBO(logApp);
            C.loadAllCampos("MEDIOPAGO");
            return C.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }

        return null;
    }
    protected Object GetProveedor()
    {
        try
        {
            conectar();

            ProveedoresSolicitudBO PS = new ProveedoresSolicitudBO(logApp);
            PS.loadProveedoresPais(Prueba.Substring(0, 1));
            return PS.TABLA;
        }
        catch { }
        finally
        {
            desconectar();
        }

        return null;
    }
    protected void rgGastos_ItemCommand(object sender, GridCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            Prueba = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["HojaRuta"]);
        }
        else if (e.CommandName == "Delete")
        {
            string Id = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"]);
            string Hoja = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["HojaRuta"]);
            string Cliente = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Clientes"]);
            string MaterialSinModificar = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Material"]);
            string Proveedor = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Proveedor"]);
            string MediodePago = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["MediodePago"]);
            string MontoSinModificar = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Monto"]);
            string IVA = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IVA"]);
            string ValorNegociadoSinModificar = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ValorNegociado"]);
            EliminarGasto(Id, Hoja, Cliente, MaterialSinModificar, Proveedor, MediodePago, MontoSinModificar, IVA, ValorNegociadoSinModificar);
        }
    }
    protected void rgGastos_UpdateCommand(object sender, GridCommandEventArgs e)
    {
        try
        {

            GridEditableItem editedItem = e.Item as GridEditableItem;
            RadComboBox Material = (RadComboBox)editedItem.FindControl("edMaterial");
            RadComboBox MedioPago = (RadComboBox)editedItem.FindControl("edMedioPago");
            RadComboBox proveedor = (RadComboBox)editedItem.FindControl("edProveedor");
            RadNumericTextBox Monto = (RadNumericTextBox)editedItem.FindControl("edMonto");
            RadNumericTextBox Iva = (RadNumericTextBox)editedItem.FindControl("edIva");
            RadNumericTextBox ValorNegociado = (RadNumericTextBox)editedItem.FindControl("edValorNegociado");
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            string Id = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"]);
            string Hoja = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["HojaRuta"]);
            string Cliente = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Clientes"]);
            string MaterialSinModificar = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Material"]);
            string Proveedor = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Proveedor"]);
            string MediodePago = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["MediodePago"]);
            string MontoSinModificar = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Monto"]);
            string IVA = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IVA"]);
            string ValorNegociadoSinModificar = Convert.ToString(e.Item.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["ValorNegociado"]);
            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            conectar();
            DetalleGastosBO DG = new DetalleGastosBO(logApp);
            if (!String.IsNullOrEmpty(Monto.Text.Trim()))
            {
                DG.CargarGastoXID(Id);
                DG.IDGASTO = Material.SelectedValue;
                DG.MEDIOPAGO = MedioPago.SelectedValue;
                DG.PROVEEDOR = proveedor.SelectedValue;
                DG.MONTO = double.Parse(Monto.Text);
                DG.IVA = double.Parse(Iva.Text);
                if (Hoja.Substring(0, 1) == "N")
                {
                    DG.TOTALVENTA = double.Parse(ValorNegociado.Text);
                }
                //Que el medio de pago sea caja chica pero que se cambie el medio de pago y que todavia no se haya realizado el pago
                if (MediodePago == "Caja Chica" & MedioPago.SelectedValue != "CC" & DG.ESTADO == "7")
                {
                    DG.ESTADO = "0";
                }
                DG.actualizar();
                //Si el estado es mayor de 2 es porque ya se contabilizo y debe notificar de la modificacion de ese registro
                if (int.Parse(DG.ESTADO) >= 2)
                {
                    string SegundaLinea = "", TerceraLinea = "";
                    CorreosBO CO = new CorreosBO(logApp);
                    GatosTracking G = new GatosTracking(logApp);
                    G.LlenarHojasConGastosMantXId(Id);

                    CO.LoadCorreosXPais(Hoja.Substring(0, 1), "Modificaciones");
                    string PrimeraLinea = "Se modifico el gasto que se detalla a continuacion:";
                    //Solo Nicaragua ingresa el valor negocia o precio de venta por eso solo a ellos se les envia
                    if (Hoja.Substring(0, 1) == "N")
                    {
                        SegundaLinea = Hoja + "||" + Cliente + "||" + MaterialSinModificar + "||" + Proveedor + "||" + MediodePago + "||"
                            + MontoSinModificar + "||" + IVA + "||" + ValorNegociadoSinModificar;
                    }
                    else
                    {
                        SegundaLinea = Hoja + "||" + Cliente + "||" + MaterialSinModificar + "||" + Proveedor + "||" + MediodePago + "||"
                            + MontoSinModificar + "||" + IVA;
                    }
                    //Solo Nicaragua ingresa el valor negocia o precio de venta por eso solo a ellos se les envia
                    if (Hoja.Substring(0, 1) == "N")
                    {
                        TerceraLinea = Hoja + "||" + Cliente + "|" + G.TABLA.Rows[0]["Material"].ToString() + "||" + G.TABLA.Rows[0]["Proveedor"].ToString()
                            + "||" + G.TABLA.Rows[0]["MediodePago"].ToString() + "||"
                            + G.TABLA.Rows[0]["Monto"].ToString() + "||" + G.TABLA.Rows[0]["IVA"].ToString() + "||" + G.TABLA.Rows[0]["ValorNegociado"].ToString();
                    }
                    else
                    {
                        TerceraLinea = Hoja + "||" + Cliente + "||" + G.TABLA.Rows[0]["Material"].ToString() + "||" + G.TABLA.Rows[0]["Proveedor"].ToString()
                        + "|" + G.TABLA.Rows[0]["MediodePago"].ToString() + "||"
                        + G.TABLA.Rows[0]["Monto"].ToString() + "||" + G.TABLA.Rows[0]["IVA"].ToString();
                    }

                    string Asunto = "Modificacion de Gasto ";
                    string Copias = CO.COPIAS;
                    string NombreUsuario = "Tracking Aduanas";
                    string usuario = "trackingaduanas";
                    string pass = "nMgtdA$7PaRjQphEYZBD";
                    string para = CO.CORREOS;
                    UsuariosBO U = new UsuariosBO(logApp);
                    U.loadUsuario(Session["IdUsuario"].ToString());
                    CorreoTabla(CO.CORREOS, Asunto, PrimeraLinea, SegundaLinea, TerceraLinea, CO.COPIAS, NombreUsuario, usuario, pass);
                }
            }

        }
        catch (Exception)
        {
        }
        finally { desconectar(); }
    }
    #endregion
    protected void CorreoTabla(string para, string subject, string Linea1, string Linea2, string Linea3, string Copias, string nombreUsuario, string user, string pass)
    {
        try
        {

            StringBuilder bodytmp = new StringBuilder();
            MailMessage mailmsg = new MailMessage();
            mailmsg.From = new MailAddress("trackingaduanas@grupovesta.com", user);
            mailmsg.To.Add(new MailAddress(para));
            mailmsg.CC.Add(Copias);
            mailmsg.Subject = subject;

            //bodytmp.Append("<html xmlns=\"http://www.w3.org/1999/xhtml\"> <head> <title>" + subject + "</title> </head> ");
            bodytmp.Append("<html> <head> <title>" + subject + "</title> </head> ");
            bodytmp.Append("<body> ");
            bodytmp.Append("<table>");
            bodytmp.Append("<tr>");
            bodytmp.Append("<td>" + Linea1 + "</td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("</tr>");
            bodytmp.Append("<tr>");
            bodytmp.Append("<td>" + "Gasto Original" + "</td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("</tr>");
            bodytmp.Append("<tr>");
            bodytmp.Append("<td>" + Linea2 + "</td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("</tr>");
            //Si no se manda una segunda liena es porque fue eliminado y solo se tiene que que enviar la primera linea
            if (Linea3 != "")
            {
                bodytmp.Append("<tr>");
                bodytmp.Append("<td>" + "Gasto Modificado" + "</td>");
                bodytmp.Append("<td></td>");
                bodytmp.Append("<td></td>");
                bodytmp.Append("</tr>");
                bodytmp.Append("<tr>");
                bodytmp.Append("<td>" + Linea3 + "</td>");
                bodytmp.Append("<td></td>");
                bodytmp.Append("<td></td>");
                bodytmp.Append("</tr>");
            }
            bodytmp.Append("<tr>");
            bodytmp.Append("<td>" + "Correo Generado Automaticamente, Favor no responder" + "</td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("<td></td>");
            bodytmp.Append("</tr>");
            bodytmp.Append("</table>");
            bodytmp.Append("</body>");
            bodytmp.Append("</html>");
            string body = Linea1 + Linea2 + Linea3;
            AlternateView avHTML = AlternateView.CreateAlternateViewFromString(bodytmp.ToString(), null, MediaTypeNames.Text.Html);

            mailmsg.AlternateViews.Add(avHTML);

            mailmsg.IsBodyHtml = true;

            StreamWriter sw = new StreamWriter(new MemoryStream(), Encoding.UTF8);
            sw.Write(body.ToString());
            sw.Flush();

            sw.BaseStream.Position = 0;

            SmtpClient smtpClient = new SmtpClient("192.168.205.250");
            smtpClient.Port = 25;
            smtpClient.Credentials = new System.Net.NetworkCredential(user, pass);

            smtpClient.Send(mailmsg);

            sw.Close();
        }
        catch (Exception ex)
        {
        }
    }
    protected void rgGastos_ItemDataBound(object sender, GridItemEventArgs e)
    {

        if (!e.Item.IsInEditMode) return;
        var item = (GridEditableItem)e.Item;

        var ValorNegociado = (RadNumericTextBox)item.FindControl("edValorNegociado");
        var Label6 = (Label)item.FindControl("Label6");
        if (Prueba.Substring(0, 1) != "N")
        {
            ValorNegociado.Visible = false;
            Label6.Visible = false;

        }
        else
        {
            ValorNegociado.Visible = true;
            Label6.Visible = true;
        }
    }

    protected void EliminarGasto(string Id, string Hoja, string Cliente, string MaterialSinModificar, string Proveedor, string MediodePago, string MontoSinModificar, string IVA, string ValorNegociadoSinModificar)
    {
        try
        {
            conectar();
            string SegundaLinea = "", TerceraLinea = "";
            DetalleGastosBO DG = new DetalleGastosBO(logApp);
            DetalleGastosEliminadosBO DGE = new DetalleGastosEliminadosBO(logApp);
            DG.CargarGastoXID(Id);
            if (DG.totalRegistros == 1)
            {
                DGE.LlenarGatosXId("-1");
                DGE.newLine();
                DGE.IDINSTRUCCION = DG.IDINSTRUCCION;
                DGE.IDGASTO = DG.IDGASTO;
                DGE.PROVEEDOR = DG.PROVEEDOR;
                DGE.MEDIOPAGO = DG.MEDIOPAGO;
                DGE.MONTO = DG.MONTO;
                DGE.IVA = DG.IVA;
                DGE.TOTALVENTA = DG.TOTALVENTA;
                DGE.ESTADO = DG.ESTADO;
                DGE.IDUSUARIOCREO = DG.USUARIO;
                DGE.IDUSUARIOELIMINO = Session["IdUsuario"].ToString();
                DGE.commitLine();
                DGE.actualizar();
                DG.EliminarGastoXIDDocumento(Id);

                if (int.Parse(DGE.ESTADO) >= 2)
                {
                    CorreosBO CO = new CorreosBO(logApp);
                    GatosTracking G = new GatosTracking(logApp);
                    G.LlenarHojasConGastosMantXId(Id);

                    CO.LoadCorreosXPais(DGE.IDINSTRUCCION.Substring(0, 1), "Modificaciones");
                    string PrimeraLinea = "Se Elimino el gasto que se detalla a continuacion:";
                    //Solo Nicaragua ingresa el valor negocia o precio de venta por eso solo a ellos se les envia
                    if (DGE.IDINSTRUCCION.Substring(0, 1) == "N")
                    {
                        SegundaLinea = Hoja + "||" + Cliente + "||" + MaterialSinModificar + "||" + Proveedor + "||" + MediodePago + "||"
                            + MontoSinModificar + "||" + IVA + "||" + ValorNegociadoSinModificar;
                    }
                    else
                    {
                        SegundaLinea = Hoja + "||" + Cliente + "||" + MaterialSinModificar + "||" + Proveedor + "||" + MediodePago + "||"
                            + MontoSinModificar + "||" + IVA;
                    }
                    string Asunto = "Se Elimino el Gasto ";
                    string Copias = CO.COPIAS;
                    string NombreUsuario = "Tracking Aduanas";
                    string usuario = "trackingaduanas";
                    string pass = "nMgtdA$7PaRjQphEYZBD";
                    string para = CO.CORREOS;
                    UsuariosBO U = new UsuariosBO(logApp);
                    U.loadUsuario(Session["IdUsuario"].ToString());
                    CorreoTabla(CO.CORREOS, Asunto, PrimeraLinea, SegundaLinea, TerceraLinea, CO.COPIAS, NombreUsuario, usuario, pass);
                }

            }
            else
            {
                registrarMensaje("Favor Comunicarse con el Administrador del Sistema ya que no se Encuentra el Gasto");
            }
        }
        catch (Exception)
        {
        }
        finally { desconectar(); }
    }
}
