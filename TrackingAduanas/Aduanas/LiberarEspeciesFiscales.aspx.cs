﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;
using System.Data;
using System.Data.OleDb;

public partial class EnviosEspeciesFiscales : Utilidades.PaginaBase
{
    private GrupoLis.Login.Login logAppNaviero;
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Liberar Especies";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgEspecies.FilterMenu);
        rgEspecies.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgEspecies.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Envios Especies Fiscales", "Liberar Especies");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
            loadEspeciesFiscales();
            cmbAccion.SelectedValue = "2";
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    #region Llenar Especies Fiscales
    private void loadEspeciesFiscales()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            c.loadCodigosXCategoriaCodigos("ESPECIESFISCALES", "'DA','DVA','F','P','DTI','HN-MI','HN-MN','SS','ZL'");
            cmbEspecieFiscal.DataSource = c.TABLA;
            cmbEspecieFiscal.DataBind();
        }
        catch { }
    }
    private void limpiarControles()
    {
        try
        {
            rgEspecies.Visible = true;
            btnGuardar.Visible = false;
            cmbAnulacion.Visible = false;
            txtObservacion.Visible = false;
            cmbAnulacion.SelectedValue = "1";
            lblRazon.Visible = false;
            lblObservacion.Visible = false;
        }
        catch { }
    }
    #endregion

    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        this.btnGuardar.Enabled = true;
        if (!cmbEspecieFiscal.IsEmpty)
        {
            if (!String.IsNullOrEmpty(txtInstruccion.Text.Trim()))
            {
                cmbAccion.SelectedValue = "2";
                try
                {
                    conectar();
                    conectarNaviero();

                    EspeciesFiscalesInstruccionesBO EFI;
               
                    Boolean Naviero = false;

                    //BUscar especies fiscales

                    if (txtInstruccion.Text.Substring(0, 2) == "MS" || txtInstruccion.Text.Substring(0, 2) == "CR")
                    {  
                        Naviero = true;
                    }

                    if (Naviero)
                    {
                        EFI = new EspeciesFiscalesInstruccionesBO(logAppNaviero);
                        EFI.cargarespeciesxinstruccionNaviero(cmbEspecieFiscal.SelectedValue, txtInstruccion.Text.Trim());
                       
                    }
                    else {

                        EFI = new EspeciesFiscalesInstruccionesBO(logApp);
                        EFI.cargarespeciesxinstruccion(cmbEspecieFiscal.SelectedValue, txtInstruccion.Text.Trim());
                    }

                    DataTable tabla = new DataTable();
                    DataRow fila;
                    tabla.Columns.Add("Serie", typeof(String));
                    tabla.Columns.Add("HojaRuta", typeof(String));

                  

                    if (EFI.totalRegistros == 1)
                    {
                        string codEspecieFiscal = EFI.TABLA.Rows[0]["Serie"].ToString();
                        string serie = "";
                        string[] arreglo = codEspecieFiscal.Trim().Split(",".ToCharArray());
                        int edCantidad = arreglo.Length;
                        for (int i = 0; i < edCantidad; i++)
                        {
                            serie = arreglo[i];
                            fila = tabla.NewRow();
                            fila["Serie"] = serie.ToString();
                            fila["HojaRuta"] = EFI.IDINSTRUCCION.ToString();
                            tabla.Rows.Add(fila);
                        }
                        rgEspecies.DataSource = tabla;
                        rgEspecies.DataBind();
                        rgEspecies.Visible = true;
                        btnGuardar.Visible = true;
                        UsuarioAsignoEspecie.Text = EFI.TABLA.Rows[0]["IdUsuario"].ToString();
                    }
                    else if (EFI.totalRegistros == 0)
                    {
                        limpiarControles();
                        registrarMensaje("No se encuentra ninguna " + cmbEspecieFiscal.Text + " para la " + txtInstruccion.Text + " Favor Revise");
                        rgEspecies.Rebind();
                        rgEspecies.Visible = false;
                    }
                    else
                    {
                        limpiarControles();
                        registrarMensaje("Consulte al administrador ya que esta hoja de ruta tiene doble registro");
                    }
                }
                catch (Exception)
                {
                }
                finally { desconectar(); }
            }
            else
            {
                registrarMensaje("Digite una Hoja de Ruta para poder realizar la busqueda");
                rgEspecies.Rebind();
                rgEspecies.Visible = false;
            }
        }
        else
        {
            registrarMensaje("Seleccione una Especie Fiscal");
        }

    }
    protected void btnGuardar_Click(object sender, ImageClickEventArgs e)
    {
      
        string Anular = "", Asignar = "";
        bool Naviero = false;
        string prueba = txtInstruccion.Text.Substring(0, 2);
        if (txtInstruccion.Text.Substring(0, 2) == "MS" || txtInstruccion.Text.Substring(0, 2) == "CR")
        {
            Naviero = true;
        }
        #region Aduanas
     
        if (Naviero == false)
        {
            #region Seleccion de de Especies Fiscales
            foreach (GridDataItem grdItem in rgEspecies.Items)
            {
                CheckBox chkEspecie = (CheckBox)grdItem.FindControl("chbEspecie");
                //Verifico las que estan chekeadas y las no chekeadas, las chekeadas se anulan o se liberar, las no chekeadas se vuelven a asignar
                if (chkEspecie.Checked == true)
                {
                    //Guardo las Chekeadas para liberarlas o aunlarlas
                    Anular = Anular + grdItem.Cells[3].Text + ",";
                }
                else
                {
                    //Guardo las no chekeadas para volverlas a asignar
                    Asignar = Asignar + grdItem.Cells[3].Text + ",";
                }
            }
            #endregion
            #region Accion a Ejecutar 
            if (Anular != "")
            {
                #region Liberar
                if (cmbAccion.SelectedValue == "1")
                {
                    Anular = Anular.Remove(Anular.Length - 1);
                    if (Asignar != "")
                    {
                        Asignar = Asignar.Remove(Asignar.Length - 1);
                        VoverAsignar(Asignar, Naviero);
                    }
                    else
                    {
                        Eliminar(Anular, Naviero);
                    }

                    
                    RevisarLiberar(Anular, Naviero);
                }
                #endregion
                #region Anular Especies
                if (cmbAccion.SelectedValue == "0")
                
                {
                    Anular = Anular.Remove(Anular.Length - 1);
                    RevisarAnular(Anular, Naviero);
                    if (Asignar != "")
                    {
                        Asignar = Asignar.Remove(Asignar.Length - 1);
                        VoverAsignar(Asignar, Naviero);
                    }
                    else
                    {
                        Eliminar(Anular, Naviero);
                    }
                }
                #endregion
                #region Otros

                if (cmbAccion.SelectedValue == "2")

                {
                    registrarMensaje("Favor Seleccione la Acción liberar o anular");
                    return;
                }
                #endregion
            }
            else
            {
                limpiarControles();
                registrarMensaje("Favor Seleccione una serie que dese liberar o anular");

            }
#endregion
            rgEspecies.Rebind();
        }
        #endregion

        #region Naviero
        else
        {
            foreach (GridDataItem grdItem in rgEspecies.Items)
            {
                CheckBox chkEspecie = (CheckBox)grdItem.FindControl("chbEspecie");
                if (chkEspecie.Checked == true)
                {
                    //Guardo las Chekeadas para liberarlas o aunlarlas
                    Anular = Anular + grdItem.Cells[3].Text + ",";
                }

                else
                {
                    //Guardo las no chekeadas para volverlas a asignar
                    Asignar = Asignar + grdItem.Cells[3].Text + ",";
                }
            }
            if (Anular != "")
            {
                if (cmbAccion.SelectedValue == "1")
                {
                    Anular = Anular.Remove(Anular.Length - 1);
                  
                    if (Asignar != "")
                    {
                        Asignar = Asignar.Remove(Asignar.Length - 1);
                        VoverAsignar(Asignar, Naviero);
                    }
                    else
                    {
                        Eliminar(Anular, Naviero);
                    }
                    RevisarLiberar(Anular, Naviero);
                }
                if (cmbAccion.SelectedValue == "0")
                {
                    Anular = Anular.Remove(Anular.Length - 1);
                    RevisarAnular(Anular, Naviero);
                    if (Asignar != "")
                    {
                        Asignar = Asignar.Remove(Asignar.Length - 1);
                        VoverAsignar(Asignar, Naviero);
                    }
                    else
                    {
                        Eliminar(Anular, Naviero);
                    }
                }
                if (cmbAccion.SelectedValue == "2")
                {
                    registrarMensaje("Favor Seleccione la Acción liberar o anular");
                    return;
                }
            }
        }
        #endregion
        cmbAccion.SelectedValue = "2";
    }
    protected void Eliminar(string Anular, bool Naviero)
    {
        try
        {
            if (Naviero == false)
            {
                conectar();
                EspeciesFiscalesInstruccionesBO EFI = new EspeciesFiscalesInstruccionesBO(logApp);
                EFI.CargarEspeciesxInstruccionParaEliminar(cmbEspecieFiscal.SelectedValue, txtInstruccion.Text.Trim());
                if (EFI.totalRegistros == 1)
                {
                    EFI.ELIMINADO = "1";
                    EFI.actualizar();
                }
            }
            else
            {
                conectarNaviero();
                TramiteBO T = new TramiteBO(logAppNaviero);
                T.LoadTramite(txtInstruccion.Text);
                if (T.totalRegistros == 1)
                {
                    if (cmbEspecieFiscal.SelectedValue == "P" || cmbEspecieFiscal.SelectedValue == "ZL")
                    {
                        T.PREIMPRESO = "";
                    }
                    else if (cmbEspecieFiscal.SelectedValue == "DA")
                    {
                        T.EXTENSION = "";
                    }
                    else
                    {
                        registrarMensaje("Especie no se puede liberar porque no es de Naviero");
                    }
                    if (T.ESTADO == "3") {
                        T.ESTADO = "1";
                    }
                    T.commitLine();
                    T.actualizar();
                }
                else if (T.totalRegistros >= 2)
                {
                    registrarMensaje("Ocurrio algun error favor consulte al administrador");
                }
            }
        }
        catch (Exception)
        {
        }
    }

    #region Liberar
    protected void RevisarLiberar(string Anular, bool Naviero)
    {
        string[] arreglo = Anular.Trim().Split(",".ToCharArray());
        string serie = "";
        int edCantidad = arreglo.Length;
        int RI = 0, RF = 0, Temp = 0, cont = 0;

        for (int i = 0; i < edCantidad; i++)
        {
            serie = arreglo[i];//Asigno el vector que convertir antes
            if (i == 0)
            {
                //En la primera entrada todos los valores seran iguales y el contador seria igual a 1 para la cantidad
                RI = Convert.ToInt32(serie);
                RF = Convert.ToInt32(serie);
                Temp = Convert.ToInt32(serie);
                cont = cont + 1;
            }
            else
            {
                //Temp comienza con el mismo valor del RI, si le sumo 1 y no es igual al siguiente valor del arreglo entonces termina el rango
                //Si es igual le asigno a RF el valor del arreglo y el rango continua
                Temp = Temp + 1;
                if (Temp == Convert.ToInt32(serie))
                {
                    RF = Convert.ToInt32(serie);
                    cont = cont + 1;
                }
                //al finalizar el rango mando a liberar o anular todo el rango
                else
                {
                    //Para liberar
                    Liberar(RI, RF, cont, Naviero);
                    //Al romperse la continuidad de un rango a todas las variables les asigno el valor del arreglo y dejo el contador con 1 por si no vuelve a cargar
                    RI = Convert.ToInt32(serie);
                    RF = Convert.ToInt32(serie);
                    Temp = Convert.ToInt32(serie);
                    cont = 1;
                }
            }
        }
        //Para Liberar el Ultimo Rango o El primero en caso que solo sea uno
        Liberar(RI, RF, cont, Naviero);
        Correcciones(Anular, "1");// si es uno es una liberacion
    }
    protected void Liberar(int RI, int RF, int Cont, bool Naviero)
    {
        try
        {
            conectar();
            MovimientosEspeciesFiscalesBO mefe = new MovimientosEspeciesFiscalesBO(logApp);
            ComprasEspeciesFiscalesBO cef = new ComprasEspeciesFiscalesBO(logApp);
            string CodigoAduana = "";
            if (Naviero == true)
            {
                if (txtInstruccion.Text.Substring(0, 2) == "CR")
                {
                    CodigoAduana = "C";
                }
                else if (txtInstruccion.Text.Substring(0, 2) == "MS")
                {
                    CodigoAduana = "MAE";
                }
            }
            else
            {
                InstruccionesBO ins = new InstruccionesBO(logApp);
                ins.loadInstruccion(txtInstruccion.Text);
                CodigoAduana = ins.CODIGOADUANA;
            }
         
            cef.ConseguirIdCompra(RI);
            DateTime fecha = DateTime.Now;
            string idMovimiento = fecha.Year.ToString() + fecha.Month.ToString("00") + fecha.Day.ToString("00") + "." + fecha.Hour.ToString("00") + fecha.Minute.ToString("00") + fecha.Second.ToString("00") + "." + DateTime.Now.Millisecond.ToString("000");
            mefe.loadMovimientosEspeciesFiscales("-1");
            mefe.newLine();
            mefe.IDMOVIMIENTO = idMovimiento;
            mefe.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
            mefe.CODPAIS = "H";
            mefe.CODIGOADUANA = CodigoAduana;
            mefe.RANGOINICIAL = (RI).ToString();
            mefe.RANGOFINAL = (RF).ToString();
            mefe.CANTIDAD = Cont;
            mefe.FECHA = DateTime.Now.ToString();
            mefe.CODTIPOMOVIMIENTO = "1";   //Usada
            mefe.CODESTADO = "1";
            mefe.IDUSUARIO = Session["IdUsuario"].ToString();
            mefe.IDCOMPRA = cef.IDCOMPRA;
            mefe.commitLine();
            mefe.actualizar();
            if (Cont > 1)
            {
                registrarMensaje("Series Liberadas Correctamente");
            }
            else
            {
                registrarMensaje("Serie Liberada Correctamente");
            }
           
        }
        catch (Exception)
        {
        }
        finally { desconectar(); }
    }
    #endregion

    #region Anular
    protected void RevisarAnular(string Anular, bool Naviero)
    {
        try
        {
            conectar();
            AnulacionesEspeciesFiscalesBO aef = new AnulacionesEspeciesFiscalesBO(logApp);
            MovimientosEspeciesFiscalesBO mef = new MovimientosEspeciesFiscalesBO(logApp);
            string CodigoAduana = "";
            if (Naviero == true)
            {
                if (txtInstruccion.Text.Substring(1, 2) == "CR")
                {
                    CodigoAduana = "C";
                }
                else if (txtInstruccion.Text.Substring(1, 2) == "MS")
                {
                    CodigoAduana = "MAE";
                }
            }
            else
            {
                InstruccionesBO ins = new InstruccionesBO(logApp);
                ins.loadInstruccion(txtInstruccion.Text);
                CodigoAduana = ins.CODIGOADUANA;
            }
            string[] arreglo = Anular.Trim().Split(",".ToCharArray());
            string serie = "";
            int edCantidad = arreglo.Length;
            int contadorinterno = 0;
            for (int i = 0; i < edCantidad; i++)
            {
                serie = arreglo[i];//Asigno el vector que convertir antes
                mef.IdMovimientoAnular(serie, cmbEspecieFiscal.SelectedValue);
                aef.loadAnulacionesEspeciesFiscales("-1");
                aef.newLine();
                aef.IDESPECIEFISCAL = cmbEspecieFiscal.SelectedValue;
                aef.CODPAIS = "H";
                aef.CODIGOADUANA = CodigoAduana;
                aef.SERIE = serie;
                aef.FECHA = DateTime.Now.ToString();
                aef.CODRAZONANULACION = cmbAnulacion.SelectedValue;
                aef.OBSERVACION = txtObservacion.Text;
                aef.IDUSUARIO = Session["IdUsuario"].ToString();
                aef.IDMOVIMIENTO = mef.IDMOVIMIENTO;

                if (cmbAnulacion.SelectedValue == "7")
                {

                    aef.CARGA = "1";
                }
                else
                {
                    aef.CARGA = "0";
                }
            

                aef.commitLine();
                aef.actualizar();
                contadorinterno = contadorinterno + 1;
            }
            if (contadorinterno == edCantidad)
            {
                registrarMensaje("Todas las series se anularon exitosamente");
            }
            else
            {
                registrarMensaje("NO SE ANULARON TODAS LAS SERIES FAVOR PERDILE AL ADMINSTRADOR QUE REVISE");
            }
            Correcciones(Anular, "2");//2 si es dos es una anulacion

        }
        catch (Exception)
        {
        }
        finally { desconectar(); }
    }
    protected void cmbAccion_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            if (cmbAccion.SelectedValue == "1")
            {
                cmbAnulacion.Visible = false;
                cmbAnulacion.SelectedValue = "";
                lblRazon.Visible = false;
                txtObservacion.Visible = false;
                txtObservacion.Text = "";
                lblObservacion.Visible = false;
            }
            else
            {
                conectar();
                CodigosBO c = new CodigosBO(logApp);
                c.loadAllCampos("RAZONANULACION");
                cmbAnulacion.DataSource = c.TABLA;
                cmbAnulacion.DataBind();
                cmbAnulacion.Visible = true;
                lblRazon.Visible = true;
                txtObservacion.Visible = true;
                txtObservacion.Text = "";
                lblObservacion.Visible = true;
            }
        }
        catch (Exception)
        {
        }
        finally { desconectar(); }
    }
    #endregion

    #region Correciones
    protected void Correcciones(string Serie, string Partida)
    {
        try
        {
            conectar();
            string CodigoAduana = "";
            CorreccionesBO C = new CorreccionesBO(logApp);
            if (txtInstruccion.Text != "")
            {
                if (txtInstruccion.Text.Substring(0, 2) == "CR")
                {
                    CodigoAduana = "C";
                }
                else if (txtInstruccion.Text.Substring(0, 2) == "MS")
                {
                    CodigoAduana = "MAE";
                }
                else
                {
                    InstruccionesBO ins = new InstruccionesBO(logApp);
                    ins.loadInstruccion(txtInstruccion.Text);
                    CodigoAduana = ins.CODIGOADUANA;
                }
                C.LoadCorrecionesXId("-1");
                C.newLine();
                C.IDINSTRUCCION = txtInstruccion.Text;
                C.CODESPECIE = cmbEspecieFiscal.SelectedValue;
                C.SERIE = Serie.ToString();
                C.FECHASOLICITUD = DateTime.Now.ToString();
                C.USUARIOSOLICITO = Session["IdUsuario"].ToString();
                C.FECHARESOLVIO = DateTime.Now.ToString();
                C.USUARIORESOLVIO = Session["IdUsuario"].ToString();
                C.CODADUANA = CodigoAduana;
                C.ESTADO = "0";
                C.PARTIDA = Partida.ToString();
                C.USUARIOASIGNOESPECIE = UsuarioAsignoEspecie.Text;
                C.commitLine();
                C.actualizar();
            }
            else
            {
                C.LoadCorrecionesXId(lblIdCorreccion.Text);
                if (C.totalRegistros == 1)
                {
                    C.FECHARESOLVIO = DateTime.Now.ToString();
                    C.USUARIORESOLVIO = Session["IdUsuario"].ToString();
                    C.ESTADO = "0";
                    C.actualizar();
                }
                else
                {
                    registrarMensaje("Favor Comunicarse con el Administrador de Sitemas");
                }
            }
        }
        catch (Exception)
        {
        }
    }
    #endregion

    #region Volver Asignar
    protected void VoverAsignar(string Asignar, bool Naviero)
    {
        try
        {
            conectar();
            EspeciesFiscalesInstruccionesBO efi = new EspeciesFiscalesInstruccionesBO(logApp);
            MovimientosEspeciesFiscalesBO mef = new MovimientosEspeciesFiscalesBO(logApp);
            MovimientosEspeciesFiscalesBO mef2 = new MovimientosEspeciesFiscalesBO(logApp);
            MovimientosEspeciesFiscalesBO mef3 = new MovimientosEspeciesFiscalesBO(logApp);
            string[] arreglo = Asignar.Trim().Split(",".ToCharArray());
            string serie = "", idmovimiento = "";
            int edCantidad = arreglo.Length;
            int RI = 0, RF = 0, Temp = 0;
            for (int i = 0; i < edCantidad; i++)
            {
                serie = arreglo[i];//Asigno el vector que convertir antes
                if (i == 0)
                {
                    //En la primera entrada todos los valores seran iguales y el contador seria igual a 1 para la cantidad
                    RI = Convert.ToInt32(serie);
                    RF = Convert.ToInt32(serie);
                    Temp = Convert.ToInt32(serie);
                }
                else
                {
                    //Temp comienza con el mismo valor del RI, si le sumo 1 y no es igual al siguiente valor del arreglo entonces termina el rango
                    //Si es igual le asigno a RF el valor del arreglo y el rango continua
                    Temp = Temp + 1;
                    if (Temp == Convert.ToInt32(serie))
                    {
                        RF = Convert.ToInt32(serie);
                    }
                    //al finalizar el rango mando a liberar o anular todo el rango
                    else
                    {
                        //Para liberar
                        mef.IdMovimientoAsignar(RI, RF, cmbEspecieFiscal.SelectedValue);
                        //Al romperse la continuidad de un rango a todas las variables les asigno el valor del arreglo y dejo el contador con 1 por si no vuelve a cargar
                        if (mef.totalRegistros == 1)
                        {
                            idmovimiento = idmovimiento + mef.IDMOVIMIENTO + ",";
                        }
                        else
                        {
                            mef.IdMovimientoAnular((RI).ToString(), cmbEspecieFiscal.SelectedValue);
                        }
                        RI = Convert.ToInt32(serie);
                        RF = Convert.ToInt32(serie);
                        Temp = Convert.ToInt32(serie);
                    }
                }
            }
            mef2.IdMovimientoAsignar(RI, RF, cmbEspecieFiscal.SelectedValue);
            if (mef.totalRegistros == 1)
            {
                idmovimiento = idmovimiento + mef.IDMOVIMIENTO + ",";
            }
            else
            {
                mef3.IdMovimientoAnular((RI).ToString(), cmbEspecieFiscal.SelectedValue);
                if (mef3.totalRegistros == 1)
                {
                    idmovimiento = idmovimiento + mef3.IDMOVIMIENTO + ",";
                }
            }
            efi.CargarEspeciesxInstruccionParaEliminar(cmbEspecieFiscal.SelectedValue, txtInstruccion.Text.Trim());
            if (efi.totalRegistros == 1 && Naviero == false)
            {
                idmovimiento.Remove(idmovimiento.Length - 1);
                efi.SERIE = Asignar.ToString();
                efi.IDMOVIMIENTO = idmovimiento.ToString();
                efi.actualizar();
            }
            else
            {
                conectarNaviero();
                TramiteBO T = new TramiteBO(logAppNaviero);
                T.LoadTramite(txtInstruccion.Text);
                if (T.totalRegistros == 1)
                {
                    if (cmbEspecieFiscal.SelectedValue == "P" || cmbEspecieFiscal.SelectedValue == "ZL")
                    {
                        T.PREIMPRESO = Asignar.ToString();
                        T.commitLine();
                        T.actualizar();
                    }
                    else if (cmbEspecieFiscal.SelectedValue == "DA")
                    {
                        T.EXTENSION = Asignar.ToString();
                        T.commitLine();
                        T.actualizar();
                    }
                    else
                    {
                        registrarMensaje("Especie no se puede liberar porque no es de Naviero");
                    }
                }
                else if (T.totalRegistros >= 2)
                {
                    registrarMensaje("Ocurrio algun error favor consulte al administrador");
                }
            }
        }
        catch (Exception)
        {
        }
        finally
        {
            if (Naviero == true)
            { desconectarNaviero(); }
            desconectar();
        }
    }
    #endregion

    protected void rgEspecies_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenargird();
    }
    protected void cmbEspecieFiscal_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        llenargird();
    }
    protected void llenargird()
    {
        try
        {
            if (txtInstruccion.Text != "" && cmbEspecieFiscal.SelectedValue != "")
            {
                conectar();
                EspeciesFiscalesInstruccionesBO EFI = new EspeciesFiscalesInstruccionesBO(logApp);
                EFI.cargarespeciesxinstruccion(cmbEspecieFiscal.SelectedValue, txtInstruccion.Text.Trim());
                cmbAnulacion.SelectedValue = "1";
            }
            limpiarControles();
        }
        catch (Exception)
        {
        }
        finally { desconectar(); }
    }

    #region Conexion
    private void conectarNaviero()
    {
        if (logAppNaviero == null)
            logAppNaviero = new GrupoLis.Login.Login();
        if (!logAppNaviero.conectado)
        {
            logAppNaviero.tipoConexion = TipoConexion.SQL_SERVER;
            logAppNaviero.SERVIDOR = mParamethers.Get("ServidorGoogle");
            logAppNaviero.DATABASE = mParamethers.Get("DatabaseN");
            logAppNaviero.USER = mParamethers.Get("UserN");
            logAppNaviero.PASSWD = mParamethers.Get("PasswordN");
            logAppNaviero.conectar();
        }
    }

    private void desconectarNaviero()
    {
        if (logAppNaviero.conectado)
            logAppNaviero.desconectar();
    }
    #endregion
    protected void btnGuardar_Command(object sender, CommandEventArgs e)
    {

    }
}