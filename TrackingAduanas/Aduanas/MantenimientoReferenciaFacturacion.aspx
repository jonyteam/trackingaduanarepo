﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="MantenimientoReferenciaFacturacion.aspx.cs" Inherits="MantenimientoCodigos" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadTabStrip ID="RadTabStrip1" runat="server" SelectedIndex="0" 
        MultiPageID="RadMultiPage1">
        <Tabs>
            <telerik:RadTab Text="Referencia Facturacion" PageViewID="pvCategorias" Selected="True">
            </telerik:RadTab>
       
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" BorderStyle="Groove"
        Width="99%" Height="90%">
        <telerik:RadPageView ID="pvCategorias" runat="server" Width="100%">
            <telerik:RadGrid ID="rgCategorias" runat="server" AllowFilteringByColumn="True" AllowSorting="True"
                AutoGenerateColumns="False" GridLines="None" Width="100%" Height="420px" 
                OnUpdateCommand="rgCategorias_UpdateCommand" OnDeleteCommand="rgCategorias_DeleteCommand"
                OnInsertCommand="rgCategorias_InsertCommand" AllowPaging="True" ShowFooter="True"
                ShowStatusBar="True" PageSize="20" oninit="rgCategorias_Init" CellSpacing="0" Culture="es-ES" OnNeedDataSource="rgCategorias_NeedDataSource1">
                <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                <MasterTableView CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros."
                    NoMasterRecordsText="No hay Categorías definidas.">

                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </ExpandCollapseColumn>
                    <Columns>
                     
                        <telerik:GridBoundColumn HeaderText="ReferenciaFacturacion" UniqueName="ReferenciaFacturacion" FilterControlAltText="Filter referencia column" DataField="ReferenciaFacturacion">
                            <HeaderStyle Width="23%" />
                            <ItemStyle Width="23%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridDateTimeColumn DataField="FechaEnvioFactura" FilterControlAltText="Filter fecha column" HeaderText="Fecha de Envio" UniqueName="FechaEnvioFactura">
                        </telerik:GridDateTimeColumn>
                        <telerik:GridBoundColumn DataField="Nombre" FilterControlAltText="Filter Usuario column" HeaderText="Usuario de  Envio" UniqueName="Usuario">
                        </telerik:GridBoundColumn>
           

                         <telerik:GridButtonColumn ConfirmText="¿Esta Seguro que desea Revertir la Facturacion?"
                            ConfirmDialogType="RadWindow" ConfirmTitle="Revertir Facturacion" UniqueName="DeleteColumn"  Text="Revertir"
                            ButtonType="ImageButton" CommandName="Update" >
                            <HeaderStyle Width="5%" />
                            <ItemStyle Width="5%" />
                        </telerik:GridButtonColumn>
                    </Columns>
                    <EditFormSettings EditFormType="Template"  CaptionFormatString="">
                        <EditColumn UniqueName="EditCommandColumn1">
                        </EditColumn>
                        <FormTemplate>
                      
                           
                        </FormTemplate>
                    </EditFormSettings>
                </MasterTableView>
                <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                    <Selecting AllowRowSelect="True" />
                    <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                        DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                        DropHereToReorder="Suelte aquí para Re-Ordenar" />
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                </ClientSettings>
                <FilterMenu EnableTheming="True">
                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                </FilterMenu>
                <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
            </telerik:RadGrid>
        </telerik:RadPageView>
        <telerik:RadPageView ID="pvCodigos" runat="server" Width="100%">
          
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgCategorias">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgCategorias" LoadingPanelID="LoadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="cbCategorias" LoadingPanelID="LoadingPanel" />
                    <telerik:AjaxUpdatedControl ControlID="rgCodigos" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rgCodigos">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgCodigos" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="cbCategorias">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgCodigos" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="Revertir">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgCategorias" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>


