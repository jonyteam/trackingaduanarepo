﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="EventosInicio.aspx.cs" Inherits="EventosInicio" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>--%>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanelFlujoGuias" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
             //<![CDATA[
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 | args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }

                function handleScrolling(sender, args) {
                    //check if the items are scrolled to bottom and get additional items
                    if (args.get_isOnBottom()) {
                        var master = sender.get_masterTableView();
                        if (master.get_pageSize() < master.get_virtualItemCount()) {
                            //changing page size causes automatic rebind
                            master.set_pageSize(master.get_pageSize() + 20);
                        }
                    }
                }
                 //]]>                                 
            </script>
        </telerik:RadScriptBlock>
        <telerik:RadMultiPage ID="mpInstrucciones" runat="server" BackColor="AliceBlue">
            <telerik:RadPageView ID="pvGridInstrucciones" runat="server" Width="100%">
                <table width="100%">
                    <tr>
                        <td>
                            <input id="edInstruccion" runat="server" type="hidden" />
                            <input id="edEvento" runat="server" type="hidden" />
                            <input id="edEventoDesc" runat="server" type="hidden" />
                            <input id="edFecha" runat="server" type="hidden" />
                            <input id="edHora" runat="server" type="hidden" />
                            <input id="edObservacion" runat="server" type="hidden" />
                            <%--<telerik:RadGrid ID="RadGrid1" runat="server" AllowFilteringByColumn="True"
                                AllowSorting="True" AutoGenerateColumns="False" GridLines="None" Width="100%"
                                Height="400px" PageSize="50" OnNeedDataSource="rgInstrucciones_NeedDataSource"
                                AllowPaging="True" ShowFooter="True" ShowStatusBar="True" OnInit="rgInstrucciones_Init"
                                OnItemCommand="rgInstrucciones_ItemCommand" OnItemDataBound="rgInstrucciones_ItemDataBound">
                                <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                                <MasterTableView DataKeyNames="IdInstruccion,IdTramite,CodigoAduana,IdEstadoFlujo"
                                    CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay instrucciones."
                                    GroupLoadMode="Client">
                                    <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                                        ShowExportToExcelButton="true" ShowExportToCsvButton="true" />
                                    <RowIndicatorColumn>
                                        <HeaderStyle Width="20px"></HeaderStyle>
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn>
                                        <HeaderStyle Width="20px"></HeaderStyle>
                                    </ExpandCollapseColumn>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="IdInstruccion" HeaderText="Instruccion No." UniqueName="IdInstruccion"
                                            FilterControlWidth="60%" FooterText="Total registros: " Aggregate="Count">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn UniqueName="Codigo" SortExpression="Orden" HeaderText="Eventos"
                                            DataField="Codigo" AllowFiltering="false" ShowFilterIcon="false">
                                            <ItemTemplate>
                                                <telerik:RadComboBox ID="edEventos" runat="server" DataValueField="Codigo" DataTextField="Descripcion"
                                                    EmptyMessage="Seleccione un Evento" Width="80%" CausesValidation="false" />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Fecha" AllowFiltering="false" ShowFilterIcon="false">
                                            <ItemTemplate>
                                                <telerik:RadDatePicker ID="dtFechaSellado" runat="server" Width="55%" Culture="es-HN"
                                                    EnableTyping="False">
                                                    <Calendar ID="Calendar1" runat="server" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"
                                                        UseRowHeadersAsSelectors="False">
                                                    </Calendar>
                                                    <DateInput ID="DateInput1" runat="server" DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"
                                                        ReadOnly="True">
                                                    </DateInput><DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                </telerik:RadDatePicker>
                                                <telerik:RadTimePicker ID="dtHoraSellado" runat="server" Width="37%" Culture="es-HN"
                                                    EnableTyping="True">
                                                    <TimeView ID="TimeView2" runat="server" CellSpacing="-1" Columns="4" Culture="es-HN"
                                                        Interval="00:30:00" TimeFormat="T">
                                                    </TimeView>
                                                    <TimePopupButton HoverImageUrl="" ImageUrl="" />
                                                </telerik:RadTimePicker>
                                            </ItemTemplate>
                                            <ItemStyle Width="16%" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Observación" AllowFiltering="false" ShowFilterIcon="false">
                                            <ItemTemplate>
                                                <telerik:RadTextBox ID="edObservacion" runat="server" Width="100%">
                                                </telerik:RadTextBox>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridButtonColumn ButtonType="ImageButton" HeaderText="Sellar Estado" CommandName="Sellar"
                                            ImageUrl="~/Images/24/lock2_24.png" UniqueName="btnSellar">
                                            <ItemStyle Width="75px" />
                                            <HeaderStyle Width="75px" />
                                        </telerik:GridButtonColumn>
                                    </Columns>
                                </MasterTableView>
                                <HeaderStyle Width="180px" />
                                <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                                    <Selecting AllowRowSelect="True" />
                                    <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                                        DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                                        DropHereToReorder="Suelte aquí para Re-Ordenar" />
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" EnableVirtualScrollPaging="true" />
                                </ClientSettings>
                                <FilterMenu EnableTheming="True">
                                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                                </FilterMenu>
                                <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                                <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
                            </telerik:RadGrid>--%>
                            <%--<telerik:RadGrid ID="rgInstrucciones" runat="server" AllowPaging="True" PageSize="14"
                                AutoGenerateColumns="false" OnNeedDataSource="rgInstrucciones_NeedDataSource"
                                OnInit="rgInstrucciones_Init" OnItemCommand="rgInstrucciones_ItemCommand" OnItemDataBound="rgInstrucciones_ItemDataBound">
                                <PagerStyle Mode="NumericPages" />
                                <MasterTableView TableLayout="Fixed" DataKeyNames="IdInstruccion,IdTramite,CodigoAduana,IdEstadoFlujo"
                                    NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay instrucciones.">
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="IdInstruccion" HeaderText="Instruccion No." UniqueName="IdInstruccion"
                                            FilterControlWidth="60%" FooterText="Total registros: " Aggregate="Count">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn UniqueName="Codigo" SortExpression="Orden" HeaderText="Eventos"
                                            DataField="Codigo" AllowFiltering="false" ShowFilterIcon="false">
                                            <ItemTemplate>
                                                <telerik:RadComboBox ID="edEventos" runat="server" DataValueField="Codigo" DataTextField="Descripcion"
                                                    EmptyMessage="Seleccione un Evento" Width="80%" CausesValidation="false" />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Fecha" AllowFiltering="false" ShowFilterIcon="false">
                                            <ItemTemplate>
                                                <telerik:RadDatePicker ID="dtFechaSellado" runat="server" Width="55%" Culture="es-HN"
                                                    EnableTyping="False">
                                                    <Calendar ID="Calendar1" runat="server" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"
                                                        UseRowHeadersAsSelectors="False">
                                                    </Calendar>
                                                    <DateInput ID="DateInput1" runat="server" DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"
                                                        ReadOnly="True">
                                                    </DateInput><DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                </telerik:RadDatePicker>
                                                <telerik:RadTimePicker ID="dtHoraSellado" runat="server" Width="37%" Culture="es-HN"
                                                    EnableTyping="True">
                                                    <TimeView ID="TimeView2" runat="server" CellSpacing="-1" Columns="4" Culture="es-HN"
                                                        Interval="00:30:00" TimeFormat="T">
                                                    </TimeView>
                                                    <TimePopupButton HoverImageUrl="" ImageUrl="" />
                                                </telerik:RadTimePicker>
                                            </ItemTemplate>
                                            <ItemStyle Width="16%" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Observación" AllowFiltering="false" ShowFilterIcon="false">
                                            <ItemTemplate>
                                                <telerik:RadTextBox ID="edObservacion" runat="server" Width="100%">
                                                </telerik:RadTextBox>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridButtonColumn ButtonType="ImageButton" HeaderText="Sellar Estado" CommandName="Sellar"
                                            ImageUrl="~/Images/24/lock2_24.png" UniqueName="btnSellar">
                                            <ItemStyle Width="75px" />
                                            <HeaderStyle Width="75px" />
                                        </telerik:GridButtonColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings>
                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                                    <ClientEvents OnScroll="handleScrolling" />
                                </ClientSettings>
                            </telerik:RadGrid>--%>
                            <telerik:RadGrid ID="rgInstrucciones" runat="server" AllowFilteringByColumn="True"
                                AllowSorting="True" AutoGenerateColumns="False" GridLines="None" Height="410px"
                                OnNeedDataSource="rgInstrucciones_NeedDataSource" AllowPaging="True" ShowFooter="True"
                                ShowStatusBar="True" PageSize="20" OnInit="rgInstrucciones_Init" OnItemCommand="rgInstrucciones_ItemCommand"
                                OnItemDataBound="rgInstrucciones_ItemDataBound" CellSpacing="0">
                                <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                                <MasterTableView DataKeyNames="IdInstruccion" CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros."
                                    NoMasterRecordsText="No hay instrucciones." GroupLoadMode="Client">
                                    <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                                        ShowExportToExcelButton="true" />
                                    <RowIndicatorColumn>
                                        <HeaderStyle Width="20px"></HeaderStyle>
                                    </RowIndicatorColumn>
                                    <ExpandCollapseColumn>
                                        <HeaderStyle Width="20px"></HeaderStyle>
                                    </ExpandCollapseColumn>
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="IdInstruccion" HeaderText="Instruccion No." UniqueName="IdInstruccion"
                                            FilterControlWidth="60%">
                                            <ItemStyle Width="10%" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn UniqueName="Codigo" SortExpression="Orden" HeaderText="Eventos"
                                            DataField="Codigo" AllowFiltering="false" ShowFilterIcon="false">
                                            <ItemTemplate>
                                                <telerik:RadComboBox ID="edEventos" runat="server" DataValueField="Codigo" DataTextField="Descripcion"
                                                    Width="80%" CausesValidation="false" />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Fecha" AllowFiltering="false" ShowFilterIcon="false">
                                            <ItemTemplate>
                                                <telerik:RadDatePicker ID="dtFechaSellado" runat="server" Width="55%" Culture="es-HN"
                                                    EnableTyping="False">
                                                    <Calendar ID="Calendar1" runat="server" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"
                                                        UseRowHeadersAsSelectors="False">
                                                    </Calendar>
                                                    <DateInput ID="DateInput1" runat="server" DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"
                                                        ReadOnly="True">
                                                    </DateInput><DatePopupButton HoverImageUrl="" ImageUrl="" />
                                                </telerik:RadDatePicker>
                                                <telerik:RadTimePicker ID="dtHoraSellado" runat="server" Width="37%" Culture="es-HN"
                                                    EnableTyping="True">
                                                    <TimeView ID="TimeView2" runat="server" CellSpacing="-1" Columns="4" Culture="es-HN"
                                                        Interval="00:30:00" TimeFormat="T">
                                                    </TimeView>
                                                    <TimePopupButton HoverImageUrl="" ImageUrl="" />
                                                </telerik:RadTimePicker>
                                            </ItemTemplate>
                                            <ItemStyle Width="16%" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Observación" AllowFiltering="false" ShowFilterIcon="false">
                                            <ItemTemplate>
                                                <telerik:RadTextBox ID="edObservacion" runat="server" Width="100%">
                                                </telerik:RadTextBox>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridButtonColumn ButtonType="ImageButton" HeaderText="Sellar Estado" CommandName="Sellar"
                                            ImageUrl="~/Images/24/lock2_24.png" UniqueName="btnSellar">
                                            <ItemStyle Width="75px" />
                                            <HeaderStyle Width="75px" />
                                        </telerik:GridButtonColumn>
                                    </Columns>
                                </MasterTableView>
                                <HeaderStyle Width="180px" />
                                <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                                    <Selecting AllowRowSelect="True" />
                                    <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                                        DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                                        DropHereToReorder="Suelte aquí para Re-Ordenar" />
                                    <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
                                </ClientSettings>
                                <FilterMenu EnableTheming="True">
                                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                                </FilterMenu>
                                <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                                <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
                            </telerik:RadGrid>
                            <%-- <table width="100%">
                                <tr>
                                    <td align="center">
                                        <asp:ImageButton ID="btnLimpiar" runat="server" ImageUrl="~/Images/24/document_plain_24.png"
                                            OnClick="btnLimpiar_Click" ToolTip="Limpiar Pantalla" />
                                    </td>
                                </tr>
                            </table>--%>
                        </td>
                    </tr>
                </table>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </telerik:RadAjaxPanel>
</asp:Content>
