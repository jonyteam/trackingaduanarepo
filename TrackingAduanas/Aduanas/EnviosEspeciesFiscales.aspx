﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="EnviosEspeciesFiscales.aspx.cs" Inherits="EnviosEspeciesFiscales" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                    //<![CDATA[
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0
                     || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                    }
                }

                function openWin() {
                    var oWnd = radopen("BuscarTramite.aspx", "RadWindow1");
                    oWnd.add_close(OnClientClose);
                }           
                
            </script>
        </telerik:RadScriptBlock>
        <telerik:RadMultiPage ID="mpEspeciesFiscales" runat="server">
            <%--<input id="edCodEspecieFiscal" runat="server" type="hidden" />
            <input id="edEspecieFiscal" runat="server" type="hidden" />
            <input id="edIdRangoEspecieFiscal" runat="server" type="hidden" />--%>
            <telerik:RadPageView ID="pvEnvioEspeciesFiscales" runat="server" Width="100%">
                <div id="miDiv" runat="server" class="panelCentrado">
                    <asp:Panel ID="Panel1" runat="server" GroupingText="Rango de Especies Fiscales" Width="98%"
                        BorderColor="White">
                        <table width="100%">
                            <tr>
                                <td style="width: 10%">
                                    País:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadComboBox ID="cmbPais" runat="server" Width="90%" DataValueField="Codigo"
                                        DataTextField="Descripcion" AutoPostBack="true" OnSelectedIndexChanged="cmbPais_SelectedIndexChanged">
                                    </telerik:RadComboBox>
                                </td>
                                <td style="width: 10%">
                                    Aduana Origen:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadComboBox ID="cmbAduanaOrigen" runat="server" DataTextField="NombreAduana"
                                        DataValueField="CodigoAduana" Width="91%" AutoPostBack="true" OnSelectedIndexChanged="cmbAduanaOrigen_SelectedIndexChanged" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%">
                                    Aduana Destino:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadComboBox ID="cmbAduanaDestino" runat="server" DataTextField="NombreAduana"
                                        DataValueField="CodigoAduana" Width="91%" AutoPostBack="true" OnSelectedIndexChanged="cmbAduanaDestino_SelectedIndexChanged" />
                                </td>
                                <td style="width: 10%">
                                    Especie Fiscal:
                                </td>
                                <td style="width: 23%">
                                    <%--<telerik:RadComboBox ID="cmbEspecieFiscal" runat="server" Width="90%" DataTextField="Descripcion"
                                        DataValueField="Codigo" Enabled="false" Font-Bold="true">
                                    </telerik:RadComboBox>--%>
                                    <telerik:RadComboBox ID="cmbEspecieFiscal" runat="server" Width="90%" DataValueField="Codigo"
                                        DataTextField="Descripcion" AutoPostBack="true" OnSelectedIndexChanged="cmbEspecieFiscal_SelectedIndexChanged">
                                    </telerik:RadComboBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 10%">
                                    Cantidad:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadNumericTextBox ID="txtCantidad" MinValue="0" runat="server" MaxLength="15"
                                        NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator=",">
                                    </telerik:RadNumericTextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <table width="100%">
                        <tr align="center">
                            <td colspan="6" align="center" style="margin-left: 240px">
                                <asp:ImageButton ID="btnGuardar" ToolTip="Salvar" ImageUrl="~/Images/24/disk_blue_ok_24.png"
                                    OnClientClick="radconfirm('Esta seguro de Salvar?',confirmCallBackSalvar, 300, 100); return false;"
                                    runat="server" />&nbsp;
                                <asp:ImageButton ID="btnLimpiar" ToolTip="Limpiar Pantalla" ImageUrl="~/Images/24/document_plain_24.png"
                                    runat="server" OnClick="btnLimpiar_Click" />
                                <asp:ImageButton ID="btnSalvar" ImageUrl="~/Images/gris.png" runat="server" OnClick="btnSalvar_Click"
                                    Style="height: 1px" />
                            </td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="rgRangoEspeciesFiscales" runat="server" AllowFilteringByColumn="True"
                                    AllowSorting="True" AutoGenerateColumns="False" GridLines="None" Width="129%"
                                    Height="400px" OnNeedDataSource="rgRangoEspeciesFiscales_NeedDataSource"
                                    ShowFooter="True" ShowStatusBar="True" PageSize="70" OnInit="rgRangoEspeciesFiscales_Init"
                                    OnItemCommand="rgRangoEspeciesFiscales_ItemCommand" CellSpacing="0" 
                                    AllowMultiRowSelection ="True">
                                    <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                                        PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                                        Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                                    <MasterTableView DataKeyNames="IdCompra" CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros."
                                        NoMasterRecordsText="No hay rangos de especies fiscales." GroupLoadMode="Client">
                                        <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                                            ShowExportToExcelButton="true" ShowExportToCsvButton="true" />
                                        <RowIndicatorColumn>
                                            <HeaderStyle Width="20px"></HeaderStyle>
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn>
                                            <HeaderStyle Width="20px"></HeaderStyle>
                                        </ExpandCollapseColumn>
                                        <Columns>
                                            <telerik:GridTemplateColumn AllowFiltering="False" 
                                                FilterControlAltText="Filter Seleccione column" FilterControlWidth="30px" 
                                                UniqueName="Seleccione">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkRango" runat="server" />
                                                </ItemTemplate>
                                                <FooterStyle Width="30px" />
                                                <HeaderStyle Width="30px" />
                                                <ItemStyle Width="30px" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn DataField="Item" 
                                                FilterControlAltText="Filter TemplateColumn column" 
                                                UniqueName="TemplateColumn" Visible="False">
                                                <ItemTemplate>
                                                    <telerik:RadTextBox ID="Item" Runat="server">
                                                    </telerik:RadTextBox>
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridBoundColumn DataField="Item" 
                                                FilterControlAltText="Filter column column" HeaderText="Item" 
                                                UniqueName="column" Visible="False">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="IdCompra" HeaderText="Compra No." UniqueName="IdCompra"
                                                FilterControlWidth="80%">
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle Width="10%" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Pais" HeaderText="Pais" UniqueName="Pais" FilterControlWidth="80%">
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle Width="10%" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="NombreAduana" HeaderText="Aduana" UniqueName="NombreAduana"
                                                FilterControlWidth="80%">
                                                <ItemStyle Width="10%" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Nombre" 
                                                FilterControlAltText="Filter Nombre column" FilterControlWidth="80%" 
                                                HeaderText="Cliente" UniqueName="Nombre">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EspecieFiscal" HeaderText="Especie Fiscal" UniqueName="EspecieFiscal"
                                                FilterControlWidth="80%">
                                                <ItemStyle Width="10%" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="RangoInicial" HeaderText="Rango Inicial" UniqueName="RangoInicial"
                                                FilterControlWidth="80%">
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle Width="10%" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="RangoFinal" HeaderText="Rango Final" UniqueName="RangoFinal"
                                                FilterControlWidth="80%">
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle Width="10%" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Cantidad" HeaderText="Cantidad" UniqueName="Cantidad"
                                                FilterControlWidth="80%">
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle Width="10%" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Fecha" HeaderText="Fecha" UniqueName="Fecha"
                                                FilterControlWidth="80%">
                                                <ItemStyle Width="10%" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Usuario" HeaderText="Usuario" UniqueName="Usuario"
                                                FilterControlWidth="80%">
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle Width="10%" />
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                    </MasterTableView>
                                    <HeaderStyle Width="180px" />
                                    <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                                        <Selecting AllowRowSelect="True" />
                                        <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                                            DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                                            DropHereToReorder="Suelte aquí para Re-Ordenar" />
                                        <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
                                    </ClientSettings>
                                    <FilterMenu EnableTheming="True">
                                        <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                                    </FilterMenu>
                                    <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                                    <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </div>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </telerik:RadAjaxPanel>
</asp:Content>
