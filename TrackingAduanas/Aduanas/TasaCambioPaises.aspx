﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TasaCambioPaises.aspx.cs"
    Inherits="MantenimientoUsuarios" MasterPageFile="~/Site.master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style1
        {
            width: 20%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="0" Width="99%">
        <telerik:RadPageView ID="pvUsuarios" runat="server" Width="100%">
            <telerik:RadGrid ID="rgTasaCambio" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                GridLines="Horizontal" Width="100%" Height="550px" OnNeedDataSource="rgUsuarios_NeedDataSource"
                OnInsertCommand="rgUsuarios_InsertCommand" AllowPaging="True" ShowFooter="True"
                ShowStatusBar="True" PageSize="20" CellSpacing="0" style="text-align: left">
                <HeaderContextMenu EnableTheming="True">
                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                </HeaderContextMenu>
                <PagerStyle NextPagesToolTip="Páginas Siguientes" NextPageToolTip="Página Siguiente"
                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                <MasterTableView CommandItemDisplay="<%# Ingresar ? GridCommandItemDisplay.Top : GridCommandItemDisplay.None %>"
                    NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay Usuarios definidos.">
                    <CommandItemSettings AddNewRecordText="Agregar Tasas de Cambio" RefreshText="Volver a Cargar Datos" />
                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridEditCommandColumn AutoPostBackOnFilter="True" CancelText="Cancelar"
                            EditText="Editar" InsertText="Insertar" UpdateText="Grabar" ButtonType="ImageButton"
                            Visible="False">
                            <HeaderStyle Width="75px" />
                            <ItemStyle Width="5%" />
                        </telerik:GridEditCommandColumn>
                        <telerik:GridButtonColumn ConfirmText="¿Esta seguro de que desea eliminar este usuario?"
                            ConfirmDialogType="RadWindow" ConfirmTitle="Eliminar Usuario" UniqueName="DeleteColumn"
                            ButtonType="ImageButton" CommandName="Delete" Visible="False">
                            <HeaderStyle Width="75px" />
                            <ItemStyle Width="5%" />
                        </telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="Fecha" HeaderText="Fecha de Cambio" UniqueName="Fecha"
                            FilterControlAltText="Filter FechaInicio column" DataFormatString="{0:dd-MM-yyyy}">
                            <HeaderStyle Width="75px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="TasaCambio" HeaderText="TasaCambio" UniqueName="TasaCambio"
                            FilterControlAltText="Filter FechaFinal column">
                            <HeaderStyle Width="75px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Pais" HeaderText="Pais" UniqueName="Pais" FilterControlAltText="Filter GASOLINASUPER column">
                            <HeaderStyle Width="75px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Nombre" HeaderText="Nombre" UniqueName="Nombre"
                            FilterControlAltText="Filter GASOLINAREGULAR column">
                            <HeaderStyle Width="75px" />
                        </telerik:GridBoundColumn>
                    </Columns>
                    <EditFormSettings EditFormType="Template" CaptionDataField="Categoria" CaptionFormatString="">
                        <EditColumn FilterControlAltText="Filter EditCommandColumn1 column" UniqueName="EditCommandColumn1">
                        </EditColumn>
                        <FormTemplate>
                            <asp:Label ID="Label1" runat="server" Font-Bold="true" Font-Size="Medium" Text='<%# ((Container is GridEditFormInsertItem) ? "Agregar Nueva Tasa de Cambio" : "Editar Usuario Existente") %>' />
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 10%">
                                    </td>
                                    <td style="width: 90%">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="lbBunker" runat="server" Font-Bold="True" Text="Pais" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadComboBox ID="edPais" runat="server" Skin='<%# Skin %>' DataTextField="Descripcion"
                                            Filter="Contains" DataValueField="Codigo" DataSource='<%# GetPaises() %>' EmptyMessage="Seleccione el Pais"
                                            Width="50%" CausesValidation="false" />
                                        <asp:RequiredFieldValidator ID="rfedPais" runat="server" 
                                            ControlToValidate="edPais" Display="Dynamic" EnableClientScript="true" 
                                            ErrorMessage="Favor Seleccione un País" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="lbTasaCambio" runat="server" Font-Bold="True" Text="Tasa Cambio" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadNumericTextBox ID="edTasa" runat="server" EmptyMessage="Tasa de Cambio"
                                            Width="50%" Culture="es-HN" DbValueFactor="1" LabelWidth="40%" Type="Currency">
                                            <NumberFormat DecimalDigits="4" ZeroPattern="L. n" />
                                        </telerik:RadNumericTextBox>
                                        <asp:RequiredFieldValidator ID="rfedTasa" runat="server" ControlToValidate="edTasa"
                                            Display="Dynamic" EnableClientScript="true" ErrorMessage="Favor Ingresar una Tasa de Cambio" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        &nbsp;
                                    </td>
                                    <td class="style1">
                                        <asp:Label ID="lbFechaInicio" runat="server" Font-Bold="True" Text="Fecha:" />
                                        :
                                        <telerik:RadDatePicker ID="edFecha" runat="server" MinDate="2014-01-01">
                                        </telerik:RadDatePicker>
                                        <asp:RequiredFieldValidator ID="rfedFecha" runat="server" ControlToValidate="edFecha"
                                            Display="Dynamic" EnableClientScript="true" ErrorMessage="Favor Ingresar una fecha" />
                                    </td>
                                </tr>
                            </table>
                            <asp:ImageButton ID="btnSave" runat="server" AlternateText='<%# (Container is GridEditFormInsertItem) ? "Agregar Usuario" : "Actualizar Usuario" %>'
                                CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                ImageUrl="Images/16/check2_16.png" />
                            &nbsp;
                            <asp:ImageButton ID="btnCancel" runat="server" AlternateText="Cancelar" CausesValidation="false"
                                CommandName="Cancel" ImageUrl="Images/16/delete2_16.png" />
                        </FormTemplate>
                    </EditFormSettings>
                </MasterTableView>
                <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                    <Selecting AllowRowSelect="True" />
                    <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                        DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                        DropHereToReorder="Suelte aquí para Re-Ordenar" />
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                </ClientSettings>
                <FilterMenu EnableTheming="True">
                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                </FilterMenu>
                <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
            </telerik:RadGrid>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgUsuarios">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgUsuarios" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rgRoles">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgRoles" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>
