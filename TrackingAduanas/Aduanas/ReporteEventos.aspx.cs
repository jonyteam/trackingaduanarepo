﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;
using System.Text;

public partial class ReporteEventos : Utilidades.PaginaBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgInstrucciones.FilterMenu);
        rgInstrucciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgInstrucciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Eventos", "Reporte Eventos");
            cargarDatosInicio();
        }
    }

    protected void cargarDatosInicio()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            ClientesBO cl = new ClientesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (u.CODPAIS == "H")
                c.loadPaisesHojaRuta();
            else
                c.loadAllCampos("PAISES", u.CODPAIS);
            cmbPais.DataSource = c.TABLA;
            cmbPais.DataBind();
        }
        catch { }
    }

    protected void rgInstrucciones_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        if (!e.IsFromDetailTable)
        {
            llenarGridInstrucciones();
        }
    }

    private void llenarGridInstrucciones()
    {
        try
        {
            conectar();
            EventosBO ev = new EventosBO(logApp);
            ev.loadEventosAll(cmbPais.SelectedValue, dpFechaInicio.SelectedDate.Value.ToString("yyyy/MM/dd"), dpFechaFinal.SelectedDate.Value.ToString("yyyy/MM/dd"));
            rgInstrucciones.DataSource = ev.TABLA;
            rgInstrucciones.DataBind();
        }
        catch (Exception)
        { }
        finally
        {
            desconectar();
        }
    }

    protected void rgInstrucciones_DetailTableDataBind(object sender, GridDetailTableDataBindEventArgs e)
    {
        try
        {
            conectar();
            EventosBO ev = new EventosBO(logApp);
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            switch (e.DetailTableView.DataMember)
            {
                case "DetalleEventos":
                    {
                        string idInstruccion = dataItem["IdInstruccion"].Text;
                        ev.loadEventosAllXInstruccion(idInstruccion);
                        e.DetailTableView.DataSource = ev.TABLA;
                        break;
                    }
            }
        }
        catch (Exception)
        { }
        finally
        {
            desconectar();
        }
    }

    protected void rgInstrucciones_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == RadGrid.ExportToExcelCommandName)
                ConfigureExport();
        }
        catch (Exception)
        { }
    }

    private void ConfigureExport()
    {
        String filename = "Reporte Eventos " + DateTime.Now.ToShortDateString();
        rgInstrucciones.GridLines = GridLines.Both;
        rgInstrucciones.ExportSettings.FileName = filename;
        rgInstrucciones.ExportSettings.IgnorePaging = true;
        rgInstrucciones.ExportSettings.OpenInNewWindow = false;
        rgInstrucciones.ExportSettings.ExportOnlyData = true;
    }

    public override bool CanGoBack { get { return false; } }

    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        if (dpFechaInicio.SelectedDate <= dpFechaFinal.SelectedDate)
        {
            llenarGridInstrucciones();
            rgInstrucciones.Visible = true;
        }
        else
        {
            registrarMensaje2("Fecha inicio no puede ser mayor que fecha final");
            rgInstrucciones.Visible = false;
        }
    }

    private void registrarMensaje2(string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("alert('" + mensaje + "');");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "mensaje", sb.ToString(), false);
    }

}