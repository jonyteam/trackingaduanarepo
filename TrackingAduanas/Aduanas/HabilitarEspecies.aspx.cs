﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

using Telerik.Web.UI;
using Toolkit.Core.Extensions;

public partial class ReporteEventos : Utilidades.PaginaBase
{

    private readonly MovimientosEspeciesFiscalesRepository _mefr = new MovimientosEspeciesFiscalesRepository();
    private readonly AnulacionEspeciesFiscalesRepository _aefr = new AnulacionEspeciesFiscalesRepository();
    private readonly EspeciesFiscalesInstruccionesRepository _efir = new EspeciesFiscalesInstruccionesRepository();
    private readonly UsuarioRepository _ur = new UsuarioRepository();
    private readonly CorreoRepository _cr = new CorreoRepository();

    private bool llenar = false;

    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Hablilitar Especies";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {

        Utilidades.PaginaBase.SetGridFilterMenu(this.rgEspecies.FilterMenu);
        rgEspecies.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgEspecies.FilterMenu.Font.Size = FontUnit.XXSmall;
        btnGuardar.Visible = false;
        btnGuardar.Enabled = false;
        if (!IsPostBack)
        {
            ((SiteMaster) Master).SetTitulo("Evios Especies Fiscales", "Habilitar Especies Fiscales");

            //if (!(Anular || Ingresar || Modificar))
            //    redirectTo("Default.aspx");

            try
            {
                conectar();
                var codigosBo = new CodigosBO(base.logApp);
                codigosBo.loadCodigosXCategoriaCodigos("ESPECIESFISCALES", "'DA','DVA','F','P','DTI','HN-MI','HN-MN','SS','ZL'");
                cmbEspecieFiscal.DataSource = codigosBo.TABLA;
                cmbEspecieFiscal.DataBind();
            }

            catch
            {
            }
            finally
            {
                desconectar();
            }
        }
    }

    public override bool CanGoBack{get { return false; }}
    protected bool Anular{get { return tienePermiso("ANULAR"); }}
    protected bool Ingresar{get { return tienePermiso("INGRESAR"); }}
    protected bool Modificar{get { return tienePermiso("MODIFICAR"); }}

    private void LimpiarControles()
    {
        rgEspecies.Visible = true;
        btnGuardar.Enabled = false;
        txtSerie.Text = "";
        cmbEspecieFiscal.SelectedValue = "1";
    }

    private void LlegarGrid()
    {
        if (llenar)
        {
            rgEspecies.Visible = true;
            int? estado = null;
            try
            {
                var existeEspecie = _mefr.GetMovimientosEspeciesBySerie(txtSerie.Text.Trim(),
                    cmbEspecieFiscal.SelectedValue);
                if (existeEspecie.Any())
                {
                    foreach (GridEspeciesFiscales item in existeEspecie)
                    {
                        //if (existeEspecie[i].ToString() != "") continue;
                        if (Convert.ToInt32(item.Estado) <= 5)
                        {
                            item.Estado = "Disponible";
                        }
                        //if (item.Observacion == "")
                        //{
                        //    rgEspecies.MasterTableView.GetColumn("Obseracion").Display = false;
                        //}
                    }
                    rgEspecies.DataSource = existeEspecie;
                }
                else
                {

                    var existeInstruccion = _efir.GetEspeciesInstruccionesBySerie(txtSerie.Text.Trim(),
                        cmbEspecieFiscal.SelectedValue);
                    if (!existeInstruccion.Any())
                    {
                        var especieAnulada = _aefr.GetAnulacionEspeciesBySerie(txtSerie.Text.Trim(),
                            cmbEspecieFiscal.SelectedValue);
                        if (especieAnulada.Any())
                        {
                            btnGuardar.Visible = true;
                            btnGuardar.Enabled = true;

                            foreach (var item in especieAnulada)
                            {
                                estado = Convert.ToInt32(item.Estado);
                                item.Estado = "Anulada";
                            }
                            btnGuardar.CommandName = estado == 0 ? "nc" : "c";
                            rgEspecies.DataSource = especieAnulada;
                        }
                        else
                        {
                            var especiePerdida = _mefr.GetMovimientosEspeciePerdida(txtSerie.Text.Trim(),
                                cmbEspecieFiscal.SelectedValue);
                            if (especiePerdida.Any())
                            {
                                btnGuardar.CommandName = "r";
                                btnGuardar.Visible = true;
                                btnGuardar.Enabled = true;
                                rgEspecies.DataSource = especiePerdida;
                            }
                            else
                            {
                                registrarMensaje("La serie o el tipo de especie fiscal solicitada es incorrecta.Por Favor verificar");
                            }

                        }
                    }
                    else
                    {
                        rgEspecies.DataSource = existeInstruccion;
                        foreach (var item in existeInstruccion)
                        {
                            estado = Convert.ToInt32(item.Estado);
                            item.Estado = estado == 1 ? "Eliminada" : "Activa";
                        }
                    }
                }

                rgEspecies.DataBind();
              

            }
            catch (Exception)
            {
                throw;
            }
        }
        else
        {
            //rgEspecies.DataSource = new string[] { };
            //rgEspecies.DataBind();
        }
    }

    protected void rgEspecies_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        LlegarGrid();
    }

    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(txtSerie.Text) && !String.IsNullOrWhiteSpace(txtSerie.Text) && cmbEspecieFiscal.SelectedValue != null)
        {
            llenar = true;
            LlegarGrid();
        }       
    }

    protected void btnGuardar_Click(object sender, ImageClickEventArgs e)
    {
        string comandName = btnGuardar.CommandName;

        if (comandName == "nc")
        {
            ResucitarEspecie();
        }
        else
        {
            if (comandName == "c")
            {
                EnviarCorreo();
                ResucitarEspecie();
            }
            else
            {
                ResucitarEspecie();
            }
        }
        LimpiarControles();
        LlegarGrid();
    }

    private void EnviarCorreo()
    {
        try
        {
            int idUsuario = Convert.ToInt32(Session["IdUsuario"].ToString());
            var usuario = _ur.GetUsuarioById(idUsuario);

            var correoData = _cr.GetCorreoById(28);
            string subject = "";
            string copias = correoData.Copias;
            string from = correoData.Correos1;
            string msg = "Por favor descartar de SAP la DVA/Poliza/Extencion Zona Libre etc " + txtSerie.Text.Trim() +
                         "." + "La cual fue habilitada en la fecha " + DateTime.Now.Date + ", favor tomar nota" +
                         "Este mensaje es generado automaticamente favor no responderlo."
                         + "Att: " + usuario.Nombre + " " + usuario.Apellido;
            string user = "trackingaduanas";
            string pass = "nMgtdA$7PaRjQphEYZBD";

            sendMailConParametrosSinArchivo(from, subject, msg, copias ,"", user,pass);
        }
        catch (Exception)
        {
            registrarMensaje("No se pudo enviar el correo de notificacion.");
        }
    }

    private void ResucitarEspecie()
    {
        try
        {
            var movimientoEspecieFiscal = _mefr.ResucitarEspecie(txtSerie.Text.Trim(), cmbEspecieFiscal.SelectedValue);
            _mefr.InsertarRegistro(movimientoEspecieFiscal);
            registrarMensaje("La serie fue habilitada con exito");
        }
        catch (Exception)
        {
            registrarMensaje("Ocurrio un error al habilitar la serie solicitada");
        }
    }

    protected void cmbEspecieFiscal_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
            //LlegarGrid();
    }
    protected void rgEspecies_ItemDataBound(object sender, GridItemEventArgs e)
    {
       rgEspecies.MasterTableView.GetColumn("Item").Display = false;
    }
}
   