﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="Correcciones.aspx.cs" Inherits="GenerarArchivoSAPDinant" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style1 {
            width: 734px;
        }

        .style4 {
            width: 155px;
        }

        .style5 {
            width: 107px;
        }

        .style6 {
            width: 122px;
        }

        .style8 {
            width: 105px;
        }

        .style9 {
            width: 109px;
        }

        .auto-style1 {
            width: 72px;
        }

        .auto-style2 {
            width: 178px;
        }

        .auto-style3 {
            width: 66px;
        }

        .auto-style4 {
            width: 65px;
        }

        .auto-style5 {
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart" Style="text-align: left">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                //<![CDATA[

                function pnlRequestStarted(ajaxPanel, eventArgs) {
                    try {
                        if ((eventArgs.EventTarget == "<%= btnSalvar.UniqueID %>")) {
                            eventArgs.EnableAjax = false;
                        }
                    }
                    catch (err)
                    { }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                    }
                }
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }

            </script>
        </telerik:RadScriptBlock>
        <table border="1" style="width: 55%">
            <tr>
                <td class="auto-style1">
                    <input id="iTipoFlete" runat="server" type="hidden" />
                    <asp:Label ID="Label1" runat="server" Text="Fecha Inicial:"></asp:Label>
                </td>
                <td class="auto-style2">
                    <telerik:RadDatePicker ID="rdpFechaInicio" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td class="auto-style3">
                    <asp:Label ID="Label2" runat="server" Text="Fecha Final:"></asp:Label>
                </td>
                <td class="auto-style2">
                    <telerik:RadDatePicker ID="rdpFechaFinal" runat="server">
                    </telerik:RadDatePicker>
                </td>
                <td align="center" class="auto-style5">
                    <asp:ImageButton ID="btnBuscar0" runat="server" ImageUrl="~/Images/24/view_24.png" OnClick="btnBuscar_Click" />
                </td>
            </tr>
            <tr>
                <td class="auto-style1">&nbsp;</td>
                <td class="auto-style2">
                    <asp:CheckBox ID="chkUso" runat="server" Text="Generar Uso" />
                    <br />
                    <asp:CheckBox ID="chkTodo" runat="server" Text="Generar Todo" />
                </td>
                <td class="auto-style3">
                    <asp:Label ID="Label3" runat="server" Text="Seleccione Especie:"></asp:Label>
                </td>
                <td class="auto-style2">
                    <telerik:RadComboBox ID="cmbEspecie" Runat="server" DataTextField="Descripcion" DataValueField="Codigo">
                    </telerik:RadComboBox>
                </td>
                <td align="center" class="auto-style5">&nbsp;</td>
            </tr>
        </table>
        <br />
        <br />
        <telerik:RadGrid ID="rgCorreccion" runat="server" CellSpacing="0"
            GridLines="None" Width="100%" OnItemCommand="RadGrid1_ItemCommand"
            OnNeedDataSource="RadGrid1_NeedDataSource" Culture="es-ES" AllowFilteringByColumn="True" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" PageSize="20">
            <ClientSettings AllowColumnsReorder="True">
                <Selecting AllowRowSelect="True" />
                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
            </ClientSettings>
            <ExportSettings HideStructureColumns="true"
                FileName="" OpenInNewWindow="True">
                <Excel Format="Biff" />
            </ExportSettings>
            <AlternatingItemStyle BackColor="#999999" BorderColor="Black" BorderStyle="Solid" HorizontalAlign="Left" VerticalAlign="Middle" />
            <MasterTableView CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" ExportToPdfText="Export to PDF" ShowAddNewRecordButton="false" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column"
                    Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column"
                    Visible="True">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="HojadeRuta" FilterControlAltText="Filter HojaRuta column" HeaderText="Hoja de Ruta" UniqueName="HojaRuta" FilterControlWidth="80%">
                        <ItemStyle Width="200px" />
                        <HeaderStyle Width="200px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Cliente" FilterControlAltText="Filter Cliente column" HeaderText="Cliente" UniqueName="Cliente" FilterControlWidth="80%">
                        <ItemStyle Width="200px" />
                        <HeaderStyle Width="200px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Aduana" FilterControlAltText="Filter Aduana column" HeaderText="Aduana" UniqueName="Aduana" FilterControlWidth="80%">
                        <ItemStyle Width="200px" />
                        <HeaderStyle Width="200px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Serie" FilterControlAltText="Filter Serie column" HeaderText="Serie" UniqueName="Serie" FilterControlWidth="80%">
                        <ItemStyle Width="200px" />
                        <HeaderStyle Width="200px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Especie" FilterControlAltText="Filter Especie column" HeaderText="Especie" UniqueName="Especie" FilterControlWidth="80%">
                        <ItemStyle Width="200px" />
                        <HeaderStyle Width="200px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Fecha" FilterControlAltText="Filter Fecha column" HeaderText="Fecha Uso" UniqueName="Fecha">
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <HeaderStyle BackColor="Red" BorderStyle="Solid" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" VerticalAlign="Middle" />
            <ItemStyle BackColor="#999999" BorderColor="Black" BorderStyle="Solid" HorizontalAlign="Left" VerticalAlign="Middle" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        <br />
        <br />
        <table width="100%">
            <tr align="center">
                <td colspan="6" align="center">
                    <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="~/Images/gris.png" />
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
    <%--<GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>--%>
</asp:Content>
