﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="InventarioUsuarios.aspx.cs" Inherits="AnulacionEspeciesFiscales" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                    //<![CDATA[
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0
                     || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }

               

                function openWin() {
                    var oWnd = radopen("BuscarTramite.aspx", "RadWindow1");
                    oWnd.add_close(OnClientClose);
                }           
                
            </script>
        </telerik:RadScriptBlock>
        <telerik:RadMultiPage ID="mpEspeciesFiscales" runat="server">
            <%--<input id="edCodEspecieFiscal" runat="server" type="hidden" />
            <input id="edEspecieFiscal" runat="server" type="hidden" />
            <input id="edIdRangoEspecieFiscal" runat="server" type="hidden" />--%>
            <telerik:RadPageView ID="pvEnvioEspeciesFiscales" runat="server" Width="100%">
                <div id="miDiv" runat="server" class="panelCentrado">
                    <asp:Panel ID="Panel1" runat="server" GroupingText="Rango de Especies Fiscales" Width="98%"
                        BorderColor="White">
                        <table width="100%">
                            <tr>
                                <td style="width: 10%">
                                    Especie Fiscal:
                                </td>
                                <td style="width: 23%">
                                    <telerik:RadComboBox ID="cmbEspecieFiscal" runat="server" Width="90%" DataValueField="Codigo"
                                        DataTextField="Descripcion" AutoPostBack="true" OnSelectedIndexChanged="cmbEspecieFiscal_SelectedIndexChanged">
                                    </telerik:RadComboBox>
                                </td>
                                <td style="width: 10%">
                                    &nbsp;</td>
                                <td style="width: 23%">
                                    &nbsp;</td>
                            </tr>
                        </table>
                    </asp:Panel>
                   
                    <table width="100%">
                        <tr>
                            <td>
                                <telerik:RadGrid ID="rgRangoEspeciesFiscales" runat="server" AllowFilteringByColumn="True"
                                    AllowSorting="True" AutoGenerateColumns="False" GridLines="None" Width="100%"
                                    Height="400px" OnNeedDataSource="rgRangoEspeciesFiscales_NeedDataSource" AllowPaging="True"
                                    ShowFooter="True" ShowStatusBar="True" PageSize="20" 
                                    OnInit="rgRangoEspeciesFiscales_Init" CellSpacing="0">
                                    <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                                        PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                                        Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                                    <MasterTableView DataKeyNames="IdCompra" CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros."
                                        NoMasterRecordsText="No hay rangos de especies fiscales." GroupLoadMode="Client">
                                        <CommandItemSettings ShowAddNewRecordButton="false" ShowRefreshButton="false" ShowExportToExcelButton="false"
                                            ShowExportToCsvButton="false" />
                                        <RowIndicatorColumn>
                                            <HeaderStyle Width="20px"></HeaderStyle>
                                        </RowIndicatorColumn>
                                        <ExpandCollapseColumn>
                                            <HeaderStyle Width="20px"></HeaderStyle>
                                        </ExpandCollapseColumn>
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="IdCompra" HeaderText="Compra No." UniqueName="IdCompra"
                                                FilterControlWidth="80%">
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle Width="10%" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Pais" HeaderText="Pais" UniqueName="Pais" FilterControlWidth="80%">
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle Width="10%" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="NombreAduana" HeaderText="Aduana" UniqueName="NombreAduana"
                                                FilterControlWidth="80%">
                                                <ItemStyle Width="10%" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="EspecieFiscal" HeaderText="Especie Fiscal" UniqueName="EspecieFiscal"
                                                FilterControlWidth="80%">
                                                <ItemStyle Width="10%" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="RangoInicial" HeaderText="Rango Inicial" UniqueName="RangoInicial"
                                                FilterControlWidth="70%">
                                                <HeaderStyle Width="7%" />
                                                <ItemStyle Width="7%" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="RangoFinal" HeaderText="Rango Final" UniqueName="RangoFinal"
                                                FilterControlWidth="70%">
                                                <HeaderStyle Width="7%" />
                                                <ItemStyle Width="7%" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Cantidad" HeaderText="Cantidad" UniqueName="Cantidad"
                                                FilterControlWidth="60%">
                                                <HeaderStyle Width="5%" />
                                                <ItemStyle Width="5%" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Nombre" HeaderText="Cliente" UniqueName="Nombre"
                                                FilterControlWidth="80%">
                                                <HeaderStyle Width="15%" />
                                                <ItemStyle Width="15%" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Fecha" HeaderText="Fecha" UniqueName="Fecha"
                                                FilterControlWidth="80%">
                                                <HeaderStyle Width="9%" />
                                                <ItemStyle Width="9%" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Usuario" HeaderText="Usuario" UniqueName="Usuario"
                                                FilterControlWidth="70%">
                                                <HeaderStyle Width="8%" />
                                                <ItemStyle Width="8%" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Observacion" 
                                                FilterControlAltText="Filter Observacion column" HeaderText="Observacion" 
                                                UniqueName="Observacion">
                                            </telerik:GridBoundColumn>
                                        </Columns>
                                        <EditFormSettings>
                                            <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                                            </EditColumn>
                                        </EditFormSettings>
                                    </MasterTableView>
                                    <HeaderStyle Width="180px" />
                                    <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                                        <Selecting AllowRowSelect="True" />
                                        <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                                            DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                                            DropHereToReorder="Suelte aquí para Re-Ordenar" />
                                        <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
                                    </ClientSettings>
                                    <FilterMenu EnableTheming="True">
                                        <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                                    </FilterMenu>
                                    <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                                    <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </div>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </telerik:RadAjaxPanel>
</asp:Content>
