﻿using System;
using System.Linq;
using Telerik.Web.UI;

public partial class MantenimientoCodigos : Utilidades.PaginaBase
{
    readonly AduanasDataContext _db = new AduanasDataContext();
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Mantenimiento Facturacion";
        base.OnLoad(e);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Mantenimiento Proceso Flujo Carga", "Mantenimiento Proceso Flujo Carga");
            LlenarComboBox();
        }
    }
    private ProcesoFlujoCarga HojaProcesoFlujoCarga(AduanasDataContext db)
    {
        
        return db.ProcesoFlujoCarga.FirstOrDefault(w => w.IdInstruccion == txtHojaRuta.Text.Trim() && Convert.ToInt16(w.CodEstado) < 3);
    }
    private Instrucciones HojaRuta(AduanasDataContext db)
    {
        return db.Instrucciones.FirstOrDefault(w => w.IdInstruccion == txtHojaRuta.Text.Trim() && (Convert.ToInt16(w.CodEstado) < 3 && Convert.ToInt16(w.CodEstado) !=99));
    }
    protected void btnBuscar_OnClick(object sender, EventArgs e)
    {

        Validacion();


    }
    protected void btnUpdate_OnClick(object sender, EventArgs e)
    {
        Update();
    }
    protected void btnCrear_OnClick(object sender, EventArgs e)
    {  
        CrearFlujo(_db);
        Validacion();
    }
    protected void btnCancelar_OnClick(object sender, EventArgs e)
    {
        redirectTo("MantenimientoCambioEstado.aspx");
    }
    private void Update()
    {
        var res = HojaProcesoFlujoCarga(_db);
        res.CodEstado = rcbEstado.SelectedValue;
        _db.SubmitChanges();
        registrarMensaje1("Cambios realizados.");
        Validacion();
    }
    public void CrearFlujo(AduanasDataContext db)
    {
        try
        {
            var HOJA = db.Instrucciones.FirstOrDefault(w => w.IdInstruccion == txtHojaRuta.Text.Trim() && Convert.ToInt16(w.CodEstado) != 100);
            var Cliente = db.Clientes.FirstOrDefault(w => w.CodigoSAP == HOJA.IdCliente);
            var pfc = new ProcesoFlujoCarga
            {
                IdInstruccion = this.txtHojaRuta.Text.Trim(),
                CodigoAduana = this.txtHojaRuta.Text.Trim().Substring(2, 3),
                Cliente = Cliente.Nombre,
                Proveedor = HOJA.Proveedor,
                Paga = "Vesta",
                CodEstado = "1",
                Fecha = DateTime.Now,
                Eliminado = false,
                IdUsuario = Convert.ToDecimal(Session["IdUsuario"].ToString())
            };
            db.ProcesoFlujoCarga.InsertOnSubmit(pfc);

            db.SubmitChanges();
            registrarMensaje1("Flujo creado exitosamente.");
        }
        catch (Exception)
        {
            registrarMensaje1("Ocurrió un error.<br>Comuníquese con Arle.");
        }
      

    }
    private void LlenarComboBox()
    {
        rcbEstado.Items.Add(new RadComboBoxItem("Aseguramiento", "0"));
        rcbEstado.Items.Add(new RadComboBoxItem("Asistente", "1"));
        rcbEstado.Items.Add(new RadComboBoxItem("Jefe", "2"));
        rcbEstado.Items.Add(new RadComboBoxItem("Finalizar", "99"));
        rcbEstado.Items.Add(new RadComboBoxItem("Enviar a Esquema", "-1"));

        rcbEstado.DataBind();
    }
    private void Validacion()
    {
        var res = HojaProcesoFlujoCarga(_db);
        if (res == null)
        {

            ValidarControles(false);
            // registrarMensaje("No existe un Flujo Por favor Proceda a Crear ");

        }
        else
        {
            ValidarControles(true);
            rcbEstado.SelectedValue = res.CodEstado;
        }
    }
    private void ValidarControles(bool existe)
    {
        if (existe)
        {
            lblEstado.Visible = true;
            rcbEstado.Visible = true;
            btnUpdate.Visible = true;
            btnCrear.Visible = false;
            btnCancelar.Visible = true;
            txtHojaRuta.Enabled = false;
        }
        else
        {
            if (HojaRuta(_db) == null)
            {
                registrarMensaje1("El estado de la hoja de ruta está fuera del rango, finalizada o está mal escrita.");
                btnCrear.Visible = false;
                txtHojaRuta.Enabled = true;
                btnCancelar.Visible = false;
            }
            else
            {
                btnCrear.Visible = true;
                txtHojaRuta.Enabled = false;
                btnCancelar.Visible = true;
            }
            lblEstado.Visible = false;
            rcbEstado.Visible = false;
            btnUpdate.Visible = false;
            btnCancelar.Visible = true;
        }
    }
}
