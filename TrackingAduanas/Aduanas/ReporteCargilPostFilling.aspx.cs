﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data.OleDb;

using Telerik.Web.UI;
using Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using GrupoLis.Ebase;
using GrupoLis.Login;
using System.Collections.Specialized;
using System.Configuration;


public partial class ReporteCargillPostFiling : Utilidades.PaginaBase
{
    private GrupoLis.Login.Login logAppAduanas;
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "ReporteHagamodaPromoda";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Cargill Post-Filing", "Reporte Cargill Post-Filing");
            cargarDatosInicio();
        }
    }
    protected void cargarDatosInicio()
    {
        try
        {
            conectar();
            CodigosBO c = new CodigosBO(logApp);
            ClientesBO cl = new ClientesBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            u.loadUsuario(Session["IdUsuario"].ToString());
            if (u.CODPAIS == "H")
                c.loadPaisesHojaRuta();
            else
                c.loadAllCampos("PAISES", u.CODPAIS);
            //cmbPais.DataSource = c.TABLA;
            //cmbPais.DataBind();
            cl.loadAllCamposClientesXPais("H");
            cmbCliente.DataSource = cl.TABLA;
            cmbCliente.DataBind();
            cmbCliente.SelectedValue = "8000423";
        }
        catch { }
    }

    protected bool Consultar { get { return tienePermiso("Consultar"); } }

    private void ConfigureExport()
    {
        String filename = "Reporte_" + DateTime.Now.ToShortDateString();
        rgReporte.ExportSettings.FileName = filename;
        rgReporte.ExportSettings.ExportOnlyData = true;
        rgReporte.ExportSettings.IgnorePaging = true;
        rgReporte.ExportSettings.OpenInNewWindow = true;
        rgReporte.MasterTableView.ExportToExcel();
    }

    public override bool CanGoBack { get { return false; } }


    private void llenarGrid()
    {
           try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            i.ReporteCargillPostFiling(txtInstruccion.Text.Trim(), 1, cmbCliente.SelectedValue);
            rgReporte.DataSource = i.TABLA;
            rgReporte.DataBind();
            i.ReporteCargillPostFiling(txtInstruccion.Text.Trim(), 2, cmbCliente.SelectedValue);
            rgGrid2.DataSource = i.TABLA;
            rgGrid2.DataBind();
            btnGuardar.Visible = true;
        }
        catch (Exception ex) { registrarMensaje("Ocurrio un Error al cargar la informacion."); }
        finally { desconectar(); }
    }
    protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        //llenarGrid();
    }


    private void desconectarAduanas()
    {
        if (logAppAduanas.conectado)
            logAppAduanas.desconectar();
    }

    protected void btnSalvar_Click(object sender, ImageClickEventArgs e)
    {
        writeToExcel();
    }

    protected void writeToExcel()
    {
        if (rgReporte.Items.Count >= 0)
        {
            btnGuardar.Visible = false;
            var fecha = DateTime.Now;
            string nameFile = "Post-FilingTemp.xlsx";
            string nameDest = "Post-Filing.xlsx";
            bool hayRegistro = false;
            Application.Lock();
            if (File.Exists("J:\\Files1\\Aduanas\\" + nameDest))
            {
                File.Delete("J:\\Files1\\Aduanas\\" + nameDest);
                File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
            }
            else
            {
                File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
            }

            //DIRECCION DE LA PLANTILLA
            var mTemplateFileName = @"J:\Files1\Aduanas\" + nameDest;

            var xlApp = new ApplicationClass() { DisplayAlerts = false, AskToUpdateLinks = false };
            var workbooks = xlApp.Workbooks;
            var workbook = workbooks.Open(mTemplateFileName, 2, false);

            try
            {
                conectar();
                #region Plantilla
                //HOJA DE LA PLANTILLA       
                const string mInputWorkSheetName = "Header fields";   //Hoja1
                //POSICIONAR LA HOJA A TRABAJAR
                //var inputWorkSheet = workBook.Worksheets.Cast<Excel.Worksheet>().Where(w => w.Name == m_InputWorkSheetName).FirstOrDefault();
                
                var inputWorkSheet = workbook.Worksheets.Cast<Worksheet>().FirstOrDefault(w => w.Name == mInputWorkSheetName);

                string HojaRuta = "", RegistrationNumber = "", ExportingCountry = "", EntryType = "", RecordNumber = "", CargoControlNo = "", TotalValue = "", TotalValueCurrency = "", ExchangeRate = "", DateAcceptance = "";
                string EntryDate = "", Freight = "", FreightCurrency = "", Insurance = "", InsuranceCurrency = "", Charges = "", ChargesCurrency = "", Fees = "", FeesCurrency ="", otherTax = "", otherTaxCurrency = "";

                int pos = 3;
                foreach (GridDataItem gridItem in rgReporte.Items)
                {
                    pos = pos + 1;

                    #region variables
                    hayRegistro = true;
                    HojaRuta = gridItem["IdInstruccion"].Text;
                    RegistrationNumber = gridItem["RegistrationNumber"].Text;
                    ExportingCountry = gridItem["ExportingCountry"].Text;
                    EntryType = gridItem["EntryType"].Text;
                    if (EntryType == "&nbsp;")
                    { EntryType = ""; }
                    RecordNumber = gridItem["ImporterOfRecordNumber"].Text;
                    CargoControlNo = gridItem["ControlCarga"].Text;
                    if (CargoControlNo == "&nbsp;")
                    { CargoControlNo = ""; }
                    TotalValue = gridItem["TotalValue"].Text;
                    TotalValueCurrency = gridItem["TotalValueCurrency"].Text;
                    if (TotalValueCurrency == "&nbsp;")
                    { TotalValueCurrency = ""; }
                    ExchangeRate = gridItem["ExchangeRate"].Text;
                    if (ExchangeRate == "&nbsp;")
                    { ExchangeRate = ""; }
                    DateAcceptance = gridItem["DateOfAcceptance"].Text;
                    if (DateAcceptance == "&nbsp;")
                    { DateAcceptance = ""; }
                    EntryDate = gridItem["EntryDate"].Text;
                    Freight = gridItem["Freight"].Text;
                    if (Freight == "nbsp;")
                    {Freight = "";}
                    FreightCurrency = gridItem["FreightCurrency"].Text;
                    if (FreightCurrency == "&nbsp;")
                    { FreightCurrency = ""; }
                    Insurance = gridItem["Insurance"].Text;
                    if (Insurance == "&nbsp;")
                    { Insurance = ""; }
                    InsuranceCurrency = gridItem["InsuranceCurrency"].Text;
                    if (InsuranceCurrency == "&nbsp;")
                    { InsuranceCurrency = ""; }
                    Charges = gridItem["Charges"].Text;
                    if (Charges == "&nbsp;")
                    { Charges = ""; }
                    ChargesCurrency = gridItem["ChargesCurrency"].Text;
                    if (ChargesCurrency == "&nbsp;")
                    { ChargesCurrency = ""; }
                    Fees = gridItem["Fees"].Text;
                    if (Fees == "&nbsp;")
                    { Fees = ""; }
                    FeesCurrency = gridItem["FeesCurrency"].Text;
                    if (FeesCurrency == "&nbsp;")
                    { FeesCurrency = ""; }
                    otherTax = gridItem["OtherTaxes"].Text;
                    if (otherTax == "&nbsp;")
                    { otherTax = ""; } 
                    otherTaxCurrency = gridItem["OtherTaxesCurrency"].Text;
                    if (otherTaxCurrency == "&nbsp;")
                    { otherTaxCurrency = ""; }
                    /*Comentarios = gridItem.Cells[24].Text;
                    if (Comentarios == "&nbsp;")
                    { Comentarios = ""; }
                    EstadoActual = gridItem.Cells[25].Text;
                    Aforador = gridItem.Cells[26].Text;
                    if (Aforador == "&nbsp;")
                    { Aforador = ""; }
                    TiempoProceso1 = gridItem.Cells[27].Text;
                    if (TiempoProceso1 == "&nbsp;")
                    { TiempoProceso1 = ""; }
                    TiempoProceso2 = gridItem.Cells[28].Text;
                    if (TiempoProceso2 == "&nbsp;")
                    { TiempoProceso2 = ""; }*/
                    #endregion

                    //inputWorkSheet.Cells.Range["B" + pos].Value = HojaRuta;
                    inputWorkSheet.Cells.Range["B" + 4].Value = RegistrationNumber;
                    inputWorkSheet.Cells.Range["B" + 5].Value = ExportingCountry;
                    inputWorkSheet.Cells.Range["B" + 6].Value = EntryType;
                    inputWorkSheet.Cells.Range["B" + 7].Value = RecordNumber;
                    inputWorkSheet.Cells.Range["B" + 8].Value = CargoControlNo; //agregar numero de control de carga 
                    inputWorkSheet.Cells.Range["B" + 9].Value = TotalValue;
                    inputWorkSheet.Cells.Range["B" + 10].Value = TotalValueCurrency;
                    inputWorkSheet.Cells.Range["B" + 11].Value = ExchangeRate;
                    inputWorkSheet.Cells.Range["B" + 12].Value = Freight;
                    inputWorkSheet.Cells.Range["B" + 13].Value = FreightCurrency;
                    inputWorkSheet.Cells.Range["B" + 14].Value = Insurance;
                    inputWorkSheet.Cells.Range["B" + 15].Value = InsuranceCurrency;
                    inputWorkSheet.Cells.Range["B" + 16].Value = Charges;
                    inputWorkSheet.Cells.Range["B" + 17].Value = ChargesCurrency;
                    inputWorkSheet.Cells.Range["B" + 18].Value = Fees;
                    inputWorkSheet.Cells.Range["B" + 29].Value = FeesCurrency;
                    inputWorkSheet.Cells.Range["B" + 20].Value = otherTax;
                    inputWorkSheet.Cells.Range["B" + 21].Value = otherTaxCurrency;
                    inputWorkSheet.Cells.Range["B" + 22].Value = DateAcceptance;
                    inputWorkSheet.Cells.Range["B" + 23].Value = EntryDate;

                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                }
                conectar();
                InstruccionesBO I = new InstruccionesBO(logApp);
                var m_InputWorkSheetName2 = "Item fields";
                var inputWorkSheet2 = workbook.Worksheets.Cast<Worksheet>().FirstOrDefault(w => w.Name == m_InputWorkSheetName2);
                I.ReporteCargillPostFiling(txtInstruccion.Text.Trim(), 2, cmbCliente.SelectedValue);
                int pos2 = 4;

                for (int i = 0; i < I.totalRegistros; i++)
                {
                    pos2 = pos2 + 1;
                    string hijo = I.TABLA.Rows[i]["IdInstruccion"].ToString();
                    //inputWorkSheet2.Cells.Range["A" + pos2].Value = I.TABLA.Rows[i]["IdInstruccion"].ToString();
                    inputWorkSheet2.Cells.Range["A" + pos2].Value = I.TABLA.Rows[i]["GrossWeight"].ToString();
                    //inputWorkSheet2.Cells.Range["B" + pos2].Value = I.TABLA.Rows[i]["PesoBrutoUnidad"].ToString();
                    inputWorkSheet2.Cells.Range["B" + pos2].Value = I.TABLA.Rows[i]["NetWeight"].ToString();
                    inputWorkSheet2.Cells.Range["C" + pos2].Value = I.TABLA.Rows[i]["NetWeightUnitOfMeasure"].ToString();
                    inputWorkSheet2.Cells.Range["D" + pos2].Value = I.TABLA.Rows[i]["NetQuantity"].ToString();
                    inputWorkSheet2.Cells.Range["E" + pos2].Value = I.TABLA.Rows[i]["NetQuantityUnitOfMeasure"].ToString();
                    inputWorkSheet2.Cells.Range["F" + pos2].Value = I.TABLA.Rows[i]["CountryOfOrigin"].ToString();
                    inputWorkSheet2.Cells.Range["G" + pos2].Value = I.TABLA.Rows[i]["PartyRelationshipAffiliation"].ToString();
                    inputWorkSheet2.Cells.Range["H" + pos2].Value = I.TABLA.Rows[i]["CustomsStatusPlacement"].ToString();
                    inputWorkSheet2.Cells.Range["I" + pos2].Value = I.TABLA.Rows[i]["HTSNumber"].ToString();
                    inputWorkSheet2.Cells.Range["J" + pos2].Value = I.TABLA.Rows[i]["EnteredValue"].ToString();
                    inputWorkSheet2.Cells.Range["K" + pos2].Value = I.TABLA.Rows[i]["EnteredValueCurrency"].ToString();
                    inputWorkSheet2.Cells.Range["L" + pos2].Value = I.TABLA.Rows[i]["DutyAmount"].ToString();
                    inputWorkSheet2.Cells.Range["M" + pos2].Value = I.TABLA.Rows[i]["DutyAmountCurrency"].ToString();
                    inputWorkSheet2.Cells.Range["N" + pos2].Value = I.TABLA.Rows[i]["VatAmount"].ToString();
                    inputWorkSheet2.Cells.Range["O" + pos2].Value = I.TABLA.Rows[i]["VatAmountCurrency"].ToString();
                    inputWorkSheet2.Cells.Range["P" + pos2].Value = I.TABLA.Rows[i]["TotalAmountInvoiced"].ToString();
                    inputWorkSheet2.Cells.Range["Q" + pos2].Value = I.TABLA.Rows[i]["CurrencyOfTotalAmountInvoiced"].ToString();
                    
                    
                    /*inputWorkSheet2.Cells.Range["R" + pos2].Value = 
                    inputWorkSheet2.Cells.Range["S" + pos2].Value = 
                    inputWorkSheet2.Cells.Range["T" + pos2].Value = "N/A";
                    inputWorkSheet2.Cells.Range["U" + pos2].Value = "N/A";
                    inputWorkSheet2.Cells.Range["V" + pos2].Value = "N/A";
                    inputWorkSheet2.Cells.Range["W" + pos2].Value = "N/A";
                    inputWorkSheet2.Cells.Range["X" + pos2].Value = "N/A";
                    inputWorkSheet2.Cells.Range["Y" + pos2].Value = "N/A";
                    inputWorkSheet2.Cells.Range["Z" + pos2].Value = "N/A";
                    inputWorkSheet2.Cells.Range["AA" + pos2].Value = "N/A";
                    inputWorkSheet2.Cells.Range["AB" + pos2].Value = "N/A";
                    inputWorkSheet2.Cells.Range["AC" + pos2].Value = "N/A";

                    inputWorkSheet2.Cells.Range["T" + pos2].Value = I.TABLA.Rows[i]["PagoLiquidacion"].ToString();
                    inputWorkSheet2.Cells.Range["U" + pos2].Value = I.TABLA.Rows[i]["Canal"].ToString();
                    inputWorkSheet2.Cells.Range["V" + pos2].Value = I.TABLA.Rows[i]["FechaLiberacion"].ToString();
                    inputWorkSheet2.Cells.Range["W" + pos2].Value = I.TABLA.Rows[i]["FechaDespacho"].ToString();
                    inputWorkSheet2.Cells.Range["X" + pos2].Value = I.TABLA.Rows[i]["FechaEntrega"].ToString();
                    inputWorkSheet2.Cells.Range["Y" + pos2].Value = I.TABLA.Rows[i]["EntregaEnTienda"].ToString();
                    inputWorkSheet2.Cells.Range["Z" + pos2].Value = I.TABLA.Rows[i]["Comentarios"].ToString();
                    inputWorkSheet2.Cells.Range["AA" + pos2].Value = I.TABLA.Rows[i]["EstadoActual"].ToString();
                    inputWorkSheet2.Cells.Range["AB" + pos2].Value = I.TABLA.Rows[i]["Aforador"].ToString();
                    inputWorkSheet2.Cells.Range["AC" + pos2].Value = I.TABLA.Rows[i]["TiempoProceso"].ToString();
                    inputWorkSheet2.Cells.Range["AD" + pos2].Value = I.TABLA.Rows[i]["TiempoProceso2"].ToString();
                    inputWorkSheet2.Cells.Range["AF" + pos2].Value = I.TABLA.Rows[i]["FacturaVEsta"].ToString();*/
                }
                rgReporte.Rebind();
                #endregion
                workbook.Save();

                //llenarBitacora("Se generó la carga " + idCarga, Session["IdUsuario"].ToString());
            }
            catch (Exception ex){}
            finally
            {
                //CERRAR EL LIBRO DE TRABAJO y la aplicacion

                workbook.Close();
                xlApp.Quit();
                desconectar();
                Application.UnLock();
            }
            //if (hayRegistro)
            //{
                Response.Clear();
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + nameDest);
                Response.Charset = "";
                Response.TransmitFile("J:\\Files1\\Aduanas\\" + nameDest);
                Response.Flush();
                Response.End();
                
            //}
            llenarGrid();   
        }
    }
    protected void cmbPais_SelectedIndexChanged(object sender, RadComboBoxSelectedIndexChangedEventArgs e)
    {
        try
        {
            //conectar();
            //ClientesBO cl = new ClientesBO(logApp);
            //cl.loadAllCamposClientesXPais(cmbPais.SelectedValue);
            //cmbCliente.DataSource = cl.TABLA;
            //cmbCliente.DataBind();
        }
        catch { }
    }
    protected void btnBuscar_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(txtInstruccion.Text) && !String.IsNullOrWhiteSpace(txtInstruccion.Text))
        {

            llenarGrid();
            rgReporte.Visible = true;
        }
        else
            registrarMensaje("El campo Instruccion no debe estar vacio.");
    }

    protected void rgGrid2_OnNeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        
    }
}
