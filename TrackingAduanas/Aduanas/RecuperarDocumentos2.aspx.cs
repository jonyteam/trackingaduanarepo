﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;
using Utilities.Web;
using ObjetosDto;
using ObjetosDto.Data;
using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;

public partial class CargarArchivo : Utilidades.PaginaBase
{
    string Ids = "0";
    String Solicitudes = "";
    Boolean mes =false;
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Recuperar Documetos";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        RadWindowManager1.Windows.Clear();
        SetGridFilterMenu(rgIngresos.FilterMenu);
        rgIngresos.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgIngresos.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Cargar Documentos", "");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
        }


        if (SessionFileShare.SessionCreada(this))
        {
            List<DigitacionGasto> Digitaciones = (List<DigitacionGasto>)Session["Digitaciones"];

            var resp = SessionFileShare.GetRespuesta(this);
            int respuesta = 0;
            ///   String referencia = "";

            string Solicitud = Request.QueryString.Get("IdSolicitud");


            var listTemp = resp as List<ResponseFileShareVm>;
            listTemp.ForEach(f =>
            {
                var id = f.IdSistema;
                respuesta = f.Estado;
            })
            ;




            if (respuesta == 1)
            {


                foreach (DigitacionGasto GT in Digitaciones)
                {
                    /*listTemp.ForEach(f =>
                    {
                        GT.REFERENCIA = f.DocumentoResponseVms.First().NoReferencia;
                    });*/
                    var obj = listTemp.FirstOrDefault(a => GT.IDINSTRUCCION == a.IdSistema);
                    GT.REFERENCIA = obj.DocumentoResponseVms.FirstOrDefault().NoReferencia;
                }




                this.M_Digitalizar(Digitaciones);
                mes = true;
            }


            SessionFileShare.DestruirFileShare(this);

            if (mes == true)
            {

                rgIngresos.Rebind();
                registrarMensaje2("Documentos digitalizados correctamente con referencia <br> " + Digitaciones[1].REFERENCIAMAESTRA.ToString());



            }
            else
            {
                registrarMensaje2("Los Documentos no fueron digitalizados ");
            }
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void SetVisible(TableCellCollection cells, String commandName, bool visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is ImageButton)
                {
                    ImageButton btn = (ImageButton)control;

                    if (btn.CommandName == commandName)
                        btn.Visible = visible;
                }
            }
        }
    }

    protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
    {
        if (e.Argument == "Rebind")
        {
            rgIngresos.Rebind();
        }
    }

    #region Ingresos
    protected void rgIngresos_NeedDataSource(object source, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
    {
        llenarGridIngresos();
    }

    private void llenarGridIngresos()
    {
        try
        {
            conectar();
            DetalleGastosBO DG = new DetalleGastosBO(logApp);

            UsuariosBO u = new UsuariosBO(logApp);
            if (User.IsInRole("Administradores") && User.Identity.Name == "dabonilla" || User.IsInRole("Administradores") && User.Identity.Name == "aalvarado" || User.IsInRole("Administradores") && User.Identity.Name == "lramirez" || User.IsInRole("Administradores") && User.Identity.Name == "mnapky" || User.IsInRole("Administradores") && User.Identity.Name == "gcorrales" || User.IsInRole("Administradores") && User.Identity.Name == "ycaballero" || User.IsInRole("Administradores") && User.Identity.Name == "arandino")
                DG.loadInstruccionesSolicitudSubirArchivo();
            else if (User.IsInRole("Administrador Pais") || User.IsInRole("Administrador Pais SVGT") || User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                DG.loadInstruccionesSolicitudSubirArchivoXPais(u.CODPAIS);
            }
            rgIngresos.DataSource = DG.TABLA;
            rgIngresos.DataBind();
        }
        catch { }
        finally
        {
            desconectar();
            ProductsSelectionManager.RestoreSelection(this.rgIngresos);
        }
    }



    public void CambiarEstado(string solicitud)
    {
        conectar();

        DetalleGastosBO DG = new DetalleGastosBO(logApp);

        String Solicitudes = solicitud.Substring(0, (solicitud.Length - 1));

    }


    public void M_Digitalizar(List<DigitacionGasto> Lista)
    {
        conectar();

        DetalleGastosBO DG = new DetalleGastosBO(logApp);
        NumeracionesBO N = new NumeracionesBO(logApp);
        string codigo = "";

        //   NumeracionesBO NU = new NumeracionesBO(logApp);
        //  String Solicitudes = solicitud.Substring(0, (solicitud.Length - 1));
        foreach (DigitacionGasto Gasto in Lista)
        {
            DG.ActualizarGrupo(Gasto.ID, "1", Session["IdUsuario"].ToString(), Gasto.REFERENCIA, Gasto.REFERENCIAMAESTRA, Session["Fecha"].ToString());
            codigo = Gasto.REFERENCIAMAESTRA.Substring(0, 3);
        }
        N.LoadNumeraciones(codigo);
        N.NUMERACION = N.NUMERACION + 1;
        N.actualizar();
        this.desconectar();

        //  DG.ActualizarGrupoCajaChica(solicitud, "1", Session["IdUsuario"].ToString());
        //DG.ESTADO = "3";
        //DG.IDDIGITALIZACION = IdDigitacion;
        //DG.FECHADIGITALIZACION = DateTime.Now.ToString();
        //DG.USUARIODIGITALIZACION = Session["IdUsuario"].ToString();
        //DG.actualizar();
        //  NU.LoadNumeraciones("1".Substring(0, 2));
        //if (NU.totalRegistros > 0)
        //{
        //    NU.NUMERACION = NU.NUMERACION + 1;
        //    NU.actualizar();
        //}
    }


    protected void CerrarWindow()
    {
        RadWindowManager1.Windows.Clear();
        RadWindow window1 = new RadWindow();
        //window1.NavigateUrl = navigateURL;
        window1.VisibleOnPageLoad = false;
        //window1.Title = titleBar;
        window1.Animation = WindowAnimation.FlyIn;
        window1.Modal = true;
        window1.DestroyOnClose = true;
        window1.VisibleStatusbar = false;
        RadWindowManager1.Windows.Add(window1);
    }
    protected void rgIngresos_Init(object sender, System.EventArgs e)
    {
        //GridFilterMenu menu = rgIngresos.FilterMenu;
        //menu.Items.RemoveAt(rgIngresos.FilterMenu.Items.Count - 2);

        GridFilterMenu filterMenu = rgIngresos.FilterMenu;

        int currentItemIndex = 0;
        while (currentItemIndex < filterMenu.Items.Count)
        {
            RadMenuItem item = filterMenu.Items[currentItemIndex];
            if (item.Text.Contains("Empty") || item.Text.Contains("Null"))
            {
                filterMenu.Items.Remove(item);
            }
            else currentItemIndex++;
        }
    }

    protected void rgIngresos_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {

            if (this.RdFechaTesoreria.SelectedDate == null)
            {
                registrarMensaje("Por Favor Ingrese la Fecha para el pago de Factura");
                this.RdFechaTesoreria.Focus();
                return;
            }

            GridDataItem editedItem = e.Item as GridDataItem;


            List<String> Hojas_T = new List<String>();
            List<String> Proveedores = new List<String>();
            List<DigitacionGasto> Digitaciones = new List<DigitacionGasto>();

            string estados = "", errores = "", aduana = "", aduana2 = "", HR = "", Cliente = "", IdTramite = "", Material = "", N_Proveedor = "";
            Solicitudes = "";
            int contador = 0, contador2 = 0;
            conectar();
            DetalleGastosBO DG = new DetalleGastosBO(logApp);
            NumeracionesBO N = new NumeracionesBO(logApp);
            var list = new List<TramiteInfo>();

            aduana = editedItem["Aduana"].Text;
            HR = editedItem["IdInstruccion"].Text;
            Cliente = editedItem["Cliente"].Text;
            Solicitudes += editedItem["Id"].Text + ",";
            N_Proveedor = editedItem["Proveedor"].Text;
            IdTramite = editedItem["IdTramite"].Text;
            Material = editedItem["Material"].Text;


            N.DatosReferencia(HR.Substring(0, 1));
            DigitacionGasto NuevaDigitacion = new DigitacionGasto(editedItem["Id"].Text, editedItem["IdInstruccion"].Text, (N.CODIGO.ToString() + "-" + DateTime.Today.Year.ToString() + "-" + N.NUMERACION.ToString()));
            Digitaciones.Add(NuevaDigitacion);


            if (e.CommandName == "Tiempo")
            {
                //Creo estado para unicamente mostrar la pantalla de carga solo cuando todos los agrupados tengan recibos


               


                if (DG.totalRegistros == 0)
                {
                    foreach (GridDataItem grdItem in rgIngresos.Items)
                    {

                        CheckBox chkRango = (CheckBox)grdItem.FindControl("chbAgrupar");

                        if (chkRango.Checked)
                        {

                            aduana = grdItem["Aduana"].Text;
                            HR = grdItem["IdInstruccion"].Text;
                            Solicitudes += grdItem["Id"].Text + ",";
                            Cliente = grdItem["Cliente"].Text;
                            IdTramite = grdItem["IdTramite"].Text;
                            Material = grdItem["Material"].Text;
                            N_Proveedor = grdItem["Proveedor"].Text;





                            var tramite = new TramiteInfo(HR, IdTramite, Cliente, Material, " ", " ");
                            DigitacionGasto NuevaDigitacion2 = new DigitacionGasto(grdItem["Id"].Text, grdItem["IdInstruccion"].Text, (N.CODIGO.ToString() + "-" + DateTime.Today.Year.ToString() + "-" + N.NUMERACION.ToString()));
                            Digitaciones.Add(NuevaDigitacion2);
                            list.Add(tramite);




                            contador2 = contador2++;

                            estados = "1";

                            estados = "2";
                            errores = "";
                            contador = contador + 1;

                            foreach (String Hoja in Hojas_T)
                            {

                                if (Hoja == HR)
                                {
                                    contador = 1;

                                }
                            }

                            Proveedores.Add(N_Proveedor);

                            foreach (String Proveedor in Proveedores)
                            {

                                if (Proveedor != N_Proveedor)
                                {

                                    registrarMensaje("No se Pueden Agrupar Distintos Proveedores");
                                    return;
                                }
                            }


                            Hojas_T.Add(HR);



                            if (contador == 1)
                            {

                                aduana2 = aduana;
                            }
                            else
                            {
                                //Si en alguna de las vueltas un gasto no es de la misma aduana entonces no podra pasar
                                if (aduana != aduana2)
                                {
                                    //  estados = "1";
                                }
                            }

                            //-- }
                            //Si esta en 1 guarda un error y entonces no asignara el valor de la fila equivocada a la variable
                            if (estados == "1")
                            {
                                //Se guardan las hojas de ruta para mostrar las que no estan bien configuradas
                                errores = errores + "," + grdItem.Cells[3].Text;
                            }
                        }
                    }
                    //  Session["Solicitudes"] = Solicitudes;
                    Session["Digitaciones"] = Digitaciones;
                    Session["Fecha"] = this.RdFechaTesoreria.SelectedDate.Value.ToString("yyyy/MM/dd");
                    //Si no hay errores abrimos la ventana para cargar
                    if (errores == "")
                    {
                        if (contador > 1)
                        {
                            try
                            {
                               
                                SessionFileShare.Create(userId: (Session["IdUsuario"].ToString()), userName: (Session["Usuario"].ToString()), tramites: list, idDocumento: 1, documento: "Otros", page: this);

                                

                            }
                            catch
                            {

                            }


                        }
                        else
                        {
                            try
                            {
                                SessionFileShare.Create(userId: (Session["IdUsuario"].ToString()), userName: (Session["Usuario"].ToString()), idDocumento: 1, documento: "Otros", idSistema: HR, page: this, identificador: IdTramite);


                            }
                            catch
                            {

                            }

                        }
                    }
                    //Si hay algun error lo mando
                    else if (errores != "" && contador2 > 0)
                    {
                        registrarMensaje("Favor Verificar bien su seleccion ya que se encontro que las siguientes hojas de ruta no tienen recibo o no estan seleccionadas o no son de la misma aduana" + errores);
                    }
                }
                //Si ya esta ingresado
                else if (DG.totalRegistros == 1)
                {
                    registrarMensaje("Gastos ya ingresado, favor seleccione otro gasto");
                    rgIngresos.Rebind();
                }
                // Otros que no deberian de haber nunca pero ahi queda
                else
                {
                    registrarMensaje("Se ha Encotrado Una Anomalia en el Sistema al Momento Subir, Favor Comunicarse con el Administrador y Pedir que Revise esta Hoja de Ruta en el Share Point");
                }

            }
            else if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
                ConfigureExport();
        }
        catch
        {
        }

    }
    private void ConfigureExport()
    {
        String filename = "Ingresos_" + DateTime.Now.ToShortDateString();
        rgIngresos.GridLines = GridLines.Both;
        rgIngresos.ExportSettings.FileName = filename;
        rgIngresos.ExportSettings.IgnorePaging = true;
        rgIngresos.ExportSettings.OpenInNewWindow = false;
        rgIngresos.ExportSettings.ExportOnlyData = true;
    }

    protected void cargarWindow(String titleBar, string navigateURL, int width, int heigth)
    {
        RadWindowManager1.Windows.Clear();
        RadWindow window1 = new RadWindow();
        window1.NavigateUrl = navigateURL;
        window1.VisibleOnPageLoad = true;
        window1.Title = titleBar;
        if (width > 0)
        {
            window1.Width = width;
            window1.Height = heigth;
            window1.Behaviors = WindowBehaviors.Move | WindowBehaviors.Pin | WindowBehaviors.Close;
        }
        else
        {
            window1.InitialBehaviors = WindowBehaviors.Maximize;
        }

        window1.Animation = WindowAnimation.FlyIn;
        window1.Modal = true;
        window1.DestroyOnClose = true;
        window1.VisibleStatusbar = false;
        RadWindowManager1.Windows.Add(window1);
    }

    private void registrarMensaje2(string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("alert('" + mensaje + "');");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "mensaje", sb.ToString(), false);
    }
    #endregion
}