﻿<%@ Application Language="C#" %>
<%@ Import Namespace="System.Web.Security" %>
<%@ Import Namespace="System.Security.Principal" %>
<%@ Import Namespace="System.Web.Routing" %>
<script RunAt="server">

    void Application_Start(object sender, EventArgs e)
    {
        // Code that runs on application startup
        RouteCollection routes = new RouteCollection();
        routes.Ignore("{resource}.axd/{*pathInfo}");
    }

    void Application_End(object sender, EventArgs e)
    {
        //  Code that runs on application shutdown
    }

    void Application_Error(object sender, EventArgs e)
    {

    }

    void Session_Start(object sender, EventArgs e)
    {
        // Code that runs when a new session is started
    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.
    }

    protected void Application_AuthenticateRequest(object sender, EventArgs e)
    {
        if (!(HttpContext.Current.User == null))
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                try
                {
                    FormsAuthenticationTicket formsAuthTicket;
                    HttpCookie httpCook;
                    GenericIdentity objGenericIdentity;
                    string[] roles;
                    httpCook = Context.Request.Cookies.Get(FormsAuthentication.FormsCookieName);
                    if (null == httpCook)
                    {
                        return;
                    }
                    formsAuthTicket = FormsAuthentication.Decrypt(httpCook.Value);
                    objGenericIdentity = new GenericIdentity(formsAuthTicket.Name);
                    roles = formsAuthTicket.UserData.Split(',');
                    GenericPrincipal genPrincipal = new GenericPrincipal(objGenericIdentity, roles);
                    HttpContext.Current.User = genPrincipal;
                }
                catch (Exception)
                {
                    HttpCookie httpCook = Context.Request.Cookies.Get(FormsAuthentication.FormsCookieName);
                    httpCook.Expires = DateTime.Now;
                    Context.Request.Cookies.Remove(FormsAuthentication.FormsCookieName);
                }
            }
        }
    }

    protected void Application_EndRequest(Object sender, EventArgs e)
    {
        string authCookie = FormsAuthentication.FormsCookieName;
        foreach (string sCookie in Response.Cookies)
        {
            // Just set the HttpOnly attribute on the Forms 
            // authentication cookie. Skip this check to set the attribute 
            // on all cookies in the collection

            if (sCookie.Equals(authCookie))
            {
                // Force HttpOnly to be added to the cookie header
                Response.Cookies[sCookie].Path += ";HttpOnly";
            }
        }
    }    
       
</script>
