﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using EntityFramework.Extensions;
using ObjetosDto;
using ObjetosDto.Data;
using Telerik.Web.UI;
using GrupoLis.Login;
using Toolkit.Core.Extensions;

public partial class InicioResponsabilidad : Utilidades.PaginaBase
{
    readonly AduanasDataContext _aduanasDc = new AduanasDataContext();
    private GrupoLis.Login.Login logAppAduanas;
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Inicio Responsabilidad";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        RadWindowManager1.Windows.Clear();
        SetGridFilterMenu(rgInstrucciones.FilterMenu);
        rgInstrucciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgInstrucciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Inicio De Responsabilidad", "Inicio De Responsabilidad");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
        }
    }

    private bool Anular { get { return tienePermiso("ANULAR"); } }
    private bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    private bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
    {
        if (e.Argument != "Rebind") return;
        rgInstrucciones.Rebind();
        Timer1.Enabled = true;
    }


    #region Aseguramiento De Servicio
    protected void rgAsistenteOperaciones_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        LlenarGrid();
    }

    private void LlenarGrid()
    {

        try
        {
            var fecha = DateTime.Now;

            fecha = fecha.AddMonths(-2);
            fecha = fecha.AddDays(-fecha.Day + 1);
            

            var instrucciones = _aduanasDc.Instrucciones.Where(w => w.IdEstadoFlujo != "99" && w.CodEstado != "100"
                                && w.CodPaisHojaRuta == "H" && w.Fecha >= Convert.ToDateTime(fecha)).ToList();

            var clietes = _aduanasDc.Clientes.Where(w => w.Eliminado == '0').ToList();

            var flujo = _aduanasDc.ProcesoFlujoCarga.Where(w => w.CodEstado != "9" && w.Eliminado == false).ToList();

            var codigos = _aduanasDc.Codigos.Where(w => w.Categoria == "ESTADO FLUJO CARGA AUTOMATICO" && w.Eliminado == '0').ToList();

            var flujoActual = flujo.Join(codigos, x => x.CodEstado, y => y.Codigo1, (x, y) => new { x = x, y = y })
                .Select(s => new FlujoVm()
                {
                    IdInstruccion = s.x.IdInstruccion,
                    Flujo = s.y.Descripcion,
                    IdFlujo = s.x.CodEstado
                }).ToList();

            var tiempos = _aduanasDc.TiemposFlujoCarga.Where(w => w.Eliminado == false && w.IdEstado == "21" && w.Fecha >= Convert.ToDateTime(fecha))
                .Select(s => s.IdInstruccion).ToList();

            var query = instrucciones.Where(w => !tiempos.Contains(w.IdInstruccion))
                .Join(clietes, i => i.IdCliente, c => c.CodigoSAP, (i, c) => new { i = i, c = c })
                .GroupJoin(flujoActual.Where(w => w.IdFlujo != "21"), i => i.i.IdInstruccion, f => f.IdInstruccion,
                    (i, f) => new { intruccion = i, flujo = f })
                .SelectMany(sm => sm.flujo.DefaultIfEmpty(new FlujoVm()), (sm, y) => new { sm = sm, y = y })
                .Select(s => new
                {
                    IdInstruccion = s.sm.intruccion.i.IdInstruccion,
                    Cliente = s.sm.intruccion.c.Nombre,
                    Proveedor = s.sm.intruccion.i.Proveedor,
                    CodRegimen = s.sm.intruccion.i.CodRegimen,
                    NoFactura = s.sm.intruccion.i.NumeroFactura,
                    Flujo = s.y.Flujo,
                    IdOficialCuenta = s.sm.intruccion.i.IdOficialCuenta
                }).ToList();


            if (User.IsInRole("Oficial de Cuenta"))
            {
                //cuando es Oficial de Cuenta el CodigoAduana es condicion
                var idUsuario = _aduanasDc.Usuarios.Where(u => u.IdUsuario == Convert.ToDecimal(Session["IdUsuario"].ToString()) && u.Eliminado == '0')
                    .Select(u => u.IdUsuario).FirstOrDefault();
                query = query.Where(q => q.IdOficialCuenta == idUsuario).ToList();
            }
            rgInstrucciones.DataSource = query;
        }
        catch(Exception ex) { }
    }

    protected void rgAsistenteOperaciones_ItemCommand(object sender, GridCommandEventArgs e)
    {
        
    }

    private void GuardarCambios()
    {
        try
        {
            var usuario = _aduanasDc.Usuarios.FirstOrDefault(w => w.IdUsuario == Session["IdUsuario"].ToString().ToDecimal() && w.Eliminado == '0');

            
            foreach (GridDataItem item in rgInstrucciones.Items)
            {
                TiemposFlujoCarga tfc = new TiemposFlujoCarga();
                CheckBox chbSeleccionado = (CheckBox)item.FindControl("chbSeleccionar");
                if (chbSeleccionado.Checked)
                {
                    var instruccion = item["IdInstruccion"].Text;
                    tfc.IdInstruccion = instruccion;
                    tfc.IdEstado = "21";
                    tfc.Fecha = DateTime.Now;
                    tfc.FechaIngreso = DateTime.Now;
                    tfc.Observacion = "Inicio de Responsabilidad por " + usuario.Nombre + " " + usuario.Apellido;
                    tfc.Eliminado = false;
                    tfc.IdUsuario = Session["IdUsuario"].ToString().ToInt();

                    _aduanasDc.TiemposFlujoCarga.InsertOnSubmit(tfc);
                    _aduanasDc.SubmitChanges();
                }
            }
            registrarMensaje("Tiempo Inicio de Responsabilidad sellado.");
            rgInstrucciones.Rebind();
        }
        catch (Exception ex){throw;}
    }

    protected void rgAsistenteOperaciones_Init(object sender, EventArgs e)
    {
        var menu = rgInstrucciones.FilterMenu;
        menu.Items.RemoveAt(rgInstrucciones.FilterMenu.Items.Count - 2);
    }

    private void RegistrarMensaje2(string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("alert('" + mensaje + "');");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "mensaje", sb.ToString(), false);
    }
    #endregion
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        rgInstrucciones.Rebind();
    }


     #region Conexion
    private void conectarAduanas()
    {
        if (logAppAduanas == null)
            logAppAduanas = new GrupoLis.Login.Login();
        if (!logAppAduanas.conectado)
        {
            logAppAduanas.tipoConexion = TipoConexion.SQL_SERVER;
            logAppAduanas.SERVIDOR = mParamethers.Get("Servidor");
            logAppAduanas.DATABASE = mParamethers.Get("DatabaseEsquemaTramites2");
            logAppAduanas.USER = mParamethers.Get("User");
            logAppAduanas.PASSWD = mParamethers.Get("Password");
            logAppAduanas.conectar();
        }
    }

    private void desconectarAduanas()
    {
        if (logAppAduanas.conectado)
            logAppAduanas.desconectar();
    }
    #endregion
    protected void rgAsistenteOperaciones_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem item = (GridDataItem)e.Item;

            TableCell ValorReal = item["ValorReal"];
            TableCell Proyectado = item["Monto"];

            ValorReal.Style["font-weight"] = "bold";

            if (Convert.ToDecimal(ValorReal.Text.Replace("L.", "").ToString()) < Convert.ToDecimal(Proyectado.Text.Replace("L.", "").ToString()))
                ValorReal.Style["color"] = "green";
            else if (Convert.ToDecimal(ValorReal.Text.Replace("L.", "").ToString()) > Convert.ToDecimal(Proyectado.Text.Replace("L.", "").ToString()))
                ValorReal.Style["color"] = "red";
        }
    }

    protected void btnGuardarTiempo_OnClick(object sender, ImageClickEventArgs e)
    {
        GuardarCambios();
    }
}