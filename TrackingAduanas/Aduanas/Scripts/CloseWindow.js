﻿var historyInicio = 0;

window.onload = function() {
    historyInicio = history.length;
}

function close() {
    var historyActual = history.length;
    if (historyActual == historyInicio) {
        history.go(-1);
    } else {
        var ret = (historyActual - historyInicio) + 1;
        history.go(-ret);
    }
   
}
