﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using Toolkit.Core.Extensions;

public partial class CambioFecha : Utilidades.PaginaBase
{

    readonly AduanasDataContext _aduanasDC = new AduanasDataContext();
    readonly EsquemaTramitesDataContext _esquemaDC = new EsquemaTramitesDataContext();

    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Mantenimiento Facturacion";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Mantenimiento Proceso Flujo Carga", "Mantenimiento Proceso Flujo Carga");
            LlenarComboBox();
            //rcbEstado.Visible = false;
            //btnUpdate.Visible = false;
            txtHojaRuta.Visible = true;
        }
    }

    private void LlenarComboBox()
    {
        rcbEstado.Items.Add(new RadComboBoxItem("Actualizar Fecha", "0"));
        rcbEstado.Items.Add(new RadComboBoxItem("Restaurar Fecha", "1"));
       
    }


    private Instrucciones BuscarHojaAduanas(string idHojaRuta)
    {
        var query = _aduanasDC.Instrucciones.FirstOrDefault(f => f.CodEstado != "100" && f.CodPaisHojaRuta == "H" && f.IdInstruccion == idHojaRuta.Trim());

        return query;
    }


    private EncabezadoEsquema BuscarHojaEsquema(string idHojaRuta)
    {
        var query = _esquemaDC.EncabezadoEsquemas.FirstOrDefault(f => f.Digitalizado == null  && f.IdHojaRuta == idHojaRuta.Trim());
        return query;
    }


    private void ActualizarFecha()
    {
        var hojaRutaAduana = BuscarHojaAduanas(txtHojaRuta.Text.Trim());
        var hojaRutaEsquema = BuscarHojaEsquema(txtHojaRuta.Text.Trim());
        FechasEditada _fe = new FechasEditada();

        try
        {
            if (rcbEstado.SelectedValue == "0")
            {

                //var instrucciones = new Instrucciones();
                //var esquema = new EncabezadoEsquema();


                _fe.IdInstruccion = hojaRutaAduana.IdInstruccion;
                _fe.FechaOriginal = hojaRutaAduana.Fecha;
                _fe.IdUsuario = Session["IdUsuario"].ToString().ToInt();

                _aduanasDC.FechasEditadas.InsertOnSubmit(_fe);


                hojaRutaAduana = _aduanasDC.Instrucciones.FirstOrDefault(
                        f => f.IdInstruccion == txtHojaRuta.Text.Trim() && f.CodEstado != "100");

                hojaRutaEsquema = _esquemaDC.EncabezadoEsquemas.FirstOrDefault(
                        f => f.IdHojaRuta == txtHojaRuta.Text.Trim() && f.Digitalizado == null);

                hojaRutaAduana.Fecha = DateTime.Now;
                hojaRutaEsquema.FchCreacion = DateTime.Now;

                _aduanasDC.SubmitChanges();
                _esquemaDC.SubmitChanges();
            }
            else if (rcbEstado.SelectedValue == "1")
            {
                var fechaOriginal = _aduanasDC.FechasEditadas.FirstOrDefault(f => f.IdInstruccion == txtHojaRuta.Text.Trim()).FechaOriginal;

                hojaRutaAduana = _aduanasDC.Instrucciones.FirstOrDefault(
                    f => f.IdInstruccion == txtHojaRuta.Text.Trim() && f.CodEstado != "100");

                hojaRutaEsquema = _esquemaDC.EncabezadoEsquemas.FirstOrDefault(
                        f => f.IdHojaRuta == txtHojaRuta.Text.Trim() && f.Digitalizado == null);

                hojaRutaAduana.Fecha = fechaOriginal;
                hojaRutaEsquema.FchCreacion = DateTime.Parse(fechaOriginal.ToString());


                _aduanasDC.SubmitChanges();
                _esquemaDC.SubmitChanges();
            }
            else if (rcbEstado.SelectedValue == "")
            {
                registrarMensaje("Debe de seleccionar una Operacion");
            }

            registrarMensaje("Fecha Actualizada Correctamente.");
        }
        catch (Exception)
        {
            
            throw;
        }
    }

    protected void btnUpdate_OnClick(object sender, EventArgs e)
    {
        ActualizarFecha();
        LlenarComboBox();
        txtHojaRuta.Visible = true;
        btnBuscar.Visible = true;
    }


    protected void btnBuscar_OnClick(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtHojaRuta.Text))
        {
            var hojaRutaAduana = BuscarHojaAduanas(txtHojaRuta.Text.Trim());
            var hojaRutaEsquema = BuscarHojaEsquema(txtHojaRuta.Text.Trim());

            if (hojaRutaAduana != null && hojaRutaEsquema != null)
            {
                rcbEstado.Visible = true;
                btnUpdate.Visible = true;
                lblEstado.Visible = true;
            }
            else
            {
                registrarMensaje("La Hoja de Ruta ingresada no exite o es incorrecta.");
                LlenarComboBox();
                txtHojaRuta.Visible = true;
                btnBuscar.Visible = true;
            }
        }
        else
        {
            registrarMensaje("Debe digitar una hoja de ruta.");
            LlenarComboBox();
            txtHojaRuta.Visible = true;
            btnBuscar.Visible = true;
        }
        
    }
}