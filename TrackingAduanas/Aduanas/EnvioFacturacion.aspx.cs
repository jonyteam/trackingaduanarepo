﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Configuration;
using Telerik.Web.UI;
using Microsoft.Office.Interop.Excel;

using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.IO;

public partial class CargarArchivo : Utilidades.PaginaBase
{
    string Ids = "0";
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Envio Facturacion";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgFacturacion.FilterMenu);
        rgFacturacion.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgFacturacion.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Hagamoda/Promoda", "Envio Facturacion");
            llenarGrid();
        }
    }

    protected bool Anular { get { return tienePermiso("ANULAR"); } }
    protected bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    protected bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void SetVisible(TableCellCollection cells, String commandName, bool visible)
    {
        foreach (TableCell cell in cells)
        {
            foreach (Control control in cell.Controls)
            {
                if (control is ImageButton)
                {
                    ImageButton btn = (ImageButton)control;

                    if (btn.CommandName == commandName)
                        btn.Visible = visible;
                }
            }
        }
    }

    protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
    {
        if (e.Argument == "Rebind")
        {
            rgFacturacion.Rebind();
        }
    }

    protected void btnEnviar_Click(object sender, EventArgs e)
    {
        try
        {
            GridDataItem editForm = (sender as RadButton).NamingContainer as GridDataItem;
            System.Web.UI.WebControls.CheckBox chkAgrupar = (System.Web.UI.WebControls.CheckBox)editForm.FindControl("chbAgrupar");
            string Hoja = editForm.Cells[4].Text;

            if (editForm.Cells[9].Text == "0.0000" && chkAgrupar.Checked == false)
            {
                EnviarCorreo(Hoja);
            }
            else
            {
                registrarMensaje("Faltan gastos por contabilizar, la diferencia tiene que ser igual a 0.00 por favor revise");
            }

        }
        catch (Exception)
        {

            throw;
        }

        finally { }
    }

    #region PARA ENVIO DE UNA SOLA HOJA DE RUTA
    protected void EnviarCorreo(string X)
    {
        LlenarExcell(X);
        conectar();
        CorreosBO C = new CorreosBO(logApp);
        C.LoadCorreosXPais(X.Substring(0, 1), "Facturacion");
        string Asunto = "Facturar Hoja de Ruta Numero " + X;
        string Cuerpo = "Este mensaje es generado automaticamente, favor no responderlo";
        string Copias = C.COPIAS;
        string NombreUsuario = "Tracking Aduanas";
        string NombredelArchivo = "Facturacion";
        string usuario = "trackingaduanas";
        string pass = "nMgtdA$7PaRjQphEYZBD";
        string para = C.CORREOS;
        sendMailConParametros(para, Asunto, Cuerpo, Copias, NombreUsuario, NombredelArchivo, usuario, pass, ".xlsx");
        rgFacturacion.Rebind();
    }
    protected void LlenarExcell(string Y)
    {
        string nameFile = "FacturacionT.xlsx";
        string nameDest = "Facturacion.xlsx";
        bool hayRegistro = false;
        Application.Lock();
        if (File.Exists("J:\\Files1\\Aduanas\\" + nameDest))
        {
            File.Delete("J:\\Files1\\Aduanas\\" + nameDest);
            File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
        }
        else
        {
            File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
        }

        //DIRECCION DE LA PLANTILLA
        var mTemplateFileName = @"J:\Files1\Aduanas\" + nameDest;

        var xlApp = new ApplicationClass() { DisplayAlerts = false, AskToUpdateLinks = false };
        var workbooks = xlApp.Workbooks;
        var workbook = workbooks.Open(mTemplateFileName, 2, false);

        try
        {
            conectar();
            #region Llenar Formato Facturacion
            //HOJA DE LA PLANTILLA       
            const string mInputWorkSheetName = "Datos";   //Hoja1
            //POSICIONAR LA HOJA A TRABAJAR
            var inputWorkSheet = workbook.Worksheets.Cast<Worksheet>().FirstOrDefault(w => w.Name == mInputWorkSheetName);
            int pos = 18;
            DetalleGastosBO DG = new DetalleGastosBO(logApp);
            DG.CargarGastosExellDeUnaHoja(Y);
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////Emcabezado de la Izquierda/////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            inputWorkSheet.Cells.Range["B" + 4].Value = DG.TABLA.Rows[0]["Cliente"].ToString(); // Cliente
            inputWorkSheet.Cells.Range["B" + 6].Value = DG.TABLA.Rows[0]["IdInstruccion"].ToString();// Hoja de Ruta
            inputWorkSheet.Cells.Range["B" + 7].Value = DG.TABLA.Rows[0]["Regimen"].ToString(); // Regimen
            inputWorkSheet.Cells.Range["B" + 8].Value = DG.TABLA.Rows[0]["Aduana"].ToString();// Aduan
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            /////////////////////////////////////Encabezado de la Derecha////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            inputWorkSheet.Cells.Range["F" + 4].Value = DG.TABLA.Rows[0]["Fauca/DUA"].ToString(); // Fauca o DUA(Poliza)
            inputWorkSheet.Cells.Range["F" + 5].Value = DG.TABLA.Rows[0]["Proveedor"].ToString();// Proveedor
            inputWorkSheet.Cells.Range["F" + 6].Value = DG.TABLA.Rows[0]["Transporte"].ToString(); // Transporte
            inputWorkSheet.Cells.Range["F" + 7].Value = DG.TABLA.Rows[0]["PaisOrigen"].ToString(); // Pais de Origen
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////Segundo Emcabezado de la Izquierda/////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            inputWorkSheet.Cells.Range["B" + 11].Value = DG.TABLA.Rows[0]["Correlativo"].ToString(); // Correlativo Electronico
            inputWorkSheet.Cells.Range["B" + 12].Value = DG.TABLA.Rows[0]["Producto"].ToString();// Productos de la Hoja
            inputWorkSheet.Cells.Range["B" + 13].Value = " "+DG.TABLA.Rows[0]["Factura"].ToString(); // Numero de Factura
            inputWorkSheet.Cells.Range["B" + 14].Value = DG.TABLA.Rows[0]["CIF"].ToString();// Valor CIF

            for (int j = 0; j < DG.totalRegistros; j++)
            {
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////Detalle Factura/////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                inputWorkSheet.Cells.Range["A" + (pos + j)].Value = DG.TABLA.Rows[j]["Cuenta"].ToString(); // Material
                inputWorkSheet.Cells.Range["B" + (pos + j)].Value = DG.TABLA.Rows[j]["Gasto"].ToString();// Gasto                                              
                inputWorkSheet.Cells.Range["C" + (pos + j)].Value = "1";//Unidades
                inputWorkSheet.Cells.Range["D" + (pos + j)].Value = "UND"; //UND
                inputWorkSheet.Cells.Range["E" + (pos + j)].Value = DG.TABLA.Rows[j]["ValorVentaDolares"].ToString();// Valor de Venta en Dolares
                if (DG.TABLA.Rows[j]["CodigoPaisCliente"].ToString() == "N" && DG.TABLA.Rows[j]["IdGasto"].ToString() == "28" && Y.Substring(0, 1) == "N")
                {
                    inputWorkSheet.Cells.Range["G" + (pos + j)].Value = "X";// Indicador de Impuesto para Nicaragua
                }
                inputWorkSheet.Cells.Range["H" + (pos + j)].Value = DG.TABLA.Rows[j]["Costo"].ToString();// Costo
            }
            DetalleGastosBO DG1 = new DetalleGastosBO(logApp);
            DG1.BuscarHojaParaAcutlizar(Y, Session["IdUsuario"].ToString(), DG.TABLA.Rows[0]["IdInstruccion"].ToString());
            #endregion
            workbook.Save();
            //llenarBitacora("Se generó la carga " + idCarga, Session["IdUsuario"].ToString());
        }
        catch (Exception ex)
        {
            //LogError(ex.Message, Session["IdUsuario"].ToString(), "GenerarArchivoSAPDinant");
        }
        finally
        {
            //CERRAR EL LIBRO DE TRABAJO y la aplicacion
            workbook.Close();
            xlApp.Quit();
            desconectar();
            Application.UnLock();
        }
        if (hayRegistro)
        {
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + nameDest);
            Response.Charset = "";
            Response.TransmitFile("J:\\Files1\\Aduanas\\" + nameDest);
            Response.Flush();
            Response.End();

        }
    }
    #endregion

    #region PARA ENVIO DE VARIAS HOJAS DE RUTA
    protected void EnviarCorreoVarios(string X, string Y)
    {
        LlenarExcellVarios(X);
        conectar();
        CorreosBO C = new CorreosBO(logApp);
        C.LoadCorreosXPais(X.Substring(5, 1), "Facturacion");
        string Asunto = "Facturar los Tramites Para el Cliente " + Y;
        string Cuerpo = "Este mensaje es generado automaticamente, favor no responderlo";
        string Copias = C.COPIAS;
        string NombreUsuario = "Tracking Aduanas";
        string NombredelArchivo = "FacturacionGrupal";
        string usuario = "trackingaduanas";
        string pass = "nMgtdA$7PaRjQphEYZBD";
        string para = C.CORREOS;
        sendMailConParametros(para, Asunto, Cuerpo, Copias, NombreUsuario, NombredelArchivo, usuario, pass, ".xlsx");
    }
    protected void LlenarExcellVarios(string Y)
    {
        string nameFile = "FacturacionGrupalT.xlsx";
        string nameDest = "FacturacionGrupal.xlsx";
        bool hayRegistro = false;
        Application.Lock();
        if (File.Exists("J:\\Files1\\Aduanas\\" + nameDest))
        {
            File.Delete("J:\\Files1\\Aduanas\\" + nameDest);
            File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
        }
        else
        {
            File.Copy("J:\\Files1\\Aduanas\\" + nameFile, "J:\\Files1\\Aduanas\\" + nameDest);
        }

        //DIRECCION DE LA PLANTILLA
        var mTemplateFileName = @"J:\Files1\Aduanas\" + nameDest;

        var xlApp = new ApplicationClass() { DisplayAlerts = false, AskToUpdateLinks = false };
        var workbooks = xlApp.Workbooks;
        var workbook = workbooks.Open(mTemplateFileName, 2, false);

        try
        {
            conectar();
            #region Llenar Formato Facturacion
            //HOJA DE LA PLANTILLA       
            const string mInputWorkSheetName = "Datos";   //Hoja1
            //POSICIONAR LA HOJA A TRABAJAR
            var inputWorkSheet = workbook.Worksheets.Cast<Worksheet>().FirstOrDefault(w => w.Name == mInputWorkSheetName);
            int pos = 18;
            DetalleGastosBO DG = new DetalleGastosBO(logApp);
            NumeracionesBO N = new NumeracionesBO(logApp);
            DG.CargarGastosExellAgrupados(Y);
            N.LoadNumeraciones2(DG.TABLA.Rows[0]["PAIS"].ToString());
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////Emcabezado de la Izquierda/////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            inputWorkSheet.Cells.Range["B" + 4].Value = DG.TABLA.Rows[0]["Aduana"].ToString(); // Aduana
            inputWorkSheet.Cells.Range["B" + 5].Value = DG.TABLA.Rows[0]["Clientes"].ToString();// Clientes
            inputWorkSheet.Cells.Range["B" + 7].Value = N.TABLA.Rows[0]["Numeraciones2"].ToString(); // Numeracion
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            /////////////////////////////////////Encabezado de la Derecha////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            inputWorkSheet.Cells.Range["B" + 10].Value = N.TABLA.Rows[0]["Numeraciones2"].ToString(); // Numeracion
            inputWorkSheet.Cells.Range["B" + 12].Value = N.TABLA.Rows[0]["Numeraciones2"].ToString(); // Numeracion
            inputWorkSheet.Cells.Range["B" + 13].Value = DG.TABLA.Rows[0]["CIF"].ToString();// Valor CIF

            for (int j = 0; j < DG.totalRegistros; j++)
            {
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////Detalle Factura/////////////////////////////////////////////////////////
                /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                inputWorkSheet.Cells.Range["A" + (pos + j)].Value = DG.TABLA.Rows[j]["Cuenta"].ToString(); // Material
                inputWorkSheet.Cells.Range["B" + (pos + j)].Value = DG.TABLA.Rows[j]["Gasto"].ToString();// Gasto                                              
                inputWorkSheet.Cells.Range["C" + (pos + j)].Value = DG.TABLA.Rows[j]["Cantidad"].ToString();// Cantidad
                inputWorkSheet.Cells.Range["D" + (pos + j)].Value = "UND"; //UND
                inputWorkSheet.Cells.Range["F" + (pos + j)].Value = DG.TABLA.Rows[j]["ValorDolar"].ToString();// Valor de Venta en Dolares
                if (DG.TABLA.Rows[j]["CodigoPaisCliente"].ToString() == "N" && DG.TABLA.Rows[j]["IdGasto"].ToString() == "28" && Y.Substring(0, 4) == "N")
                {
                    inputWorkSheet.Cells.Range["G" + (pos + j)].Value = "X";// Indicador de Impuesto para Nicaragua
                }
                inputWorkSheet.Cells.Range["H" + (pos + j)].Value = DG.TABLA.Rows[j]["Costo"].ToString();// Costo
            }

            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////Segunda Hoja//////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            var m_InputWorkSheetName2 = "Detalle";
            var inputWorkSheet2 = workbook.Worksheets.Cast<Worksheet>().FirstOrDefault(w => w.Name == m_InputWorkSheetName2);
            DetalleGastosBO DG1 = new DetalleGastosBO(logApp);
            DG1.CargarDetalleFacturacion(Y);
            int pos2 = 6;
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////Emcabezado////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            inputWorkSheet2.Cells.Range["B" + 1].Value = DG1.TABLA.Rows[0]["Cliente"].ToString(); // Aduana
            inputWorkSheet2.Cells.Range["B" + 2].Value = DG1.TABLA.Rows[0]["Aduana"].ToString();// Clientes
            inputWorkSheet2.Cells.Range["B" + 3].Value = DG1.TABLA.Rows[0]["Numeraciones2"].ToString(); // Numeracion
            for (int j = 0; j < DG1.totalRegistros; j++)
            {
                inputWorkSheet2.Cells.Range["A" + (pos2 + j)].Value = DG1.TABLA.Rows[j]["HojadeRuta"].ToString(); // HojaRuta
                inputWorkSheet2.Cells.Range["B" + (pos2 + j)].Value = DG1.TABLA.Rows[j]["Gasto"].ToString(); // Gasto
                inputWorkSheet2.Cells.Range["C" + (pos2 + j)].Value = DG1.TABLA.Rows[j]["Costo"].ToString(); // Costo
                inputWorkSheet2.Cells.Range["D" + (pos2 + j)].Value = DG1.TABLA.Rows[j]["VentaDolares"].ToString(); // Costo
            }
            DetalleGastosBO DG2 = new DetalleGastosBO(logApp);
            DG2.BuscarHojasyActualizarlas(Y, Session["IdUsuario"].ToString(), N.TABLA.Rows[0]["Numeraciones2"].ToString());
            NumeracionesBO N2 = new NumeracionesBO(logApp);
            N2.LoadNumeraciones(Y.Substring(5, 1));
            if (N2.totalRegistros > 0)
            {
                N2.NUMERACION = N2.NUMERACION + 1;
                N2.actualizar();
            }
            #endregion
            workbook.Save();
            //llenarBitacora("Se generó la carga " + idCarga, Session["IdUsuario"].ToString());
        }
        catch (Exception ex)
        {
            //LogError(ex.Message, Session["IdUsuario"].ToString(), "GenerarArchivoSAPDinant");
        }
        finally
        {
            //CERRAR EL LIBRO DE TRABAJO y la aplicacion
            workbook.Close();
            xlApp.Quit();
            desconectar();
            Application.UnLock();
        }
        if (hayRegistro)
        {
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("Content-Disposition", "attachment; filename=" + nameDest);
            Response.Charset = "";
            Response.TransmitFile("J:\\Files1\\Aduanas\\" + nameDest);
            Response.Flush();
            Response.End();

        }
    }
    #endregion

    protected void rgIngresos_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        llenarGrid();
    }
    private void llenarGrid()
    {
        try
        {
            conectar();
            InstruccionesBO i = new InstruccionesBO(logApp);
            DetalleGastosBO DG = new DetalleGastosBO(logApp);
            UsuariosBO u = new UsuariosBO(logApp);
            if (User.IsInRole("Administradores") && User.Identity.Name == "dabonilla" || User.IsInRole("Administradores") && User.Identity.Name == "aalvarado" || User.IsInRole("Administradores") && User.Identity.Name == "lramirez" || User.IsInRole("Administradores") && User.Identity.Name == "mnapky" || User.IsInRole("Administradores") && User.Identity.Name == "gcorrales" || User.IsInRole("Administradores") && User.Identity.Name == "ycaballero")
                DG.RevisionPreFacturacion();
            else if (User.IsInRole("Administrador Pais") || User.IsInRole("Administrador Pais SVGT")|| User.IsInRole("Gestores") || User.IsInRole("GestoresSVGT"))
            {
                u.loadUsuarioLogin(User.Identity.Name);
                DG.RevisionPreFacturacionXPais(u.CODPAIS);
            }
            rgFacturacion.DataSource = DG.TABLA;

        }
        catch (Exception)
        {

        }

        finally { desconectar(); }

    }
    //protected void btnGuardar_Click(object sender, ImageClickEventArgs e)
    //{
    //    string error = "", error2 = "", hojas = "'0", cliente = "", cliente2 = "", aduana = "", aduana2 = "";
    //    int contador = 0;
    //    foreach (GridDataItem grdItem in rgFacturacion.Items)
    //    {
    //        System.Web.UI.WebControls.CheckBox chkAgrupar = (System.Web.UI.WebControls.CheckBox)grdItem.FindControl("chbAgrupar");

    //        if (chkAgrupar.Checked)
    //        {
    //            if (grdItem.Cells[8].Text == "0.0000")
    //            {
    //                error = "";
    //                contador = contador + 1;
    //                hojas = hojas + "','" + grdItem.Cells[3].Text;
    //                cliente = grdItem.Cells[4].Text;
    //                aduana = grdItem.Cells[5].Text;
    //            }
    //            else
    //            {
    //                error = "X";
    //            }
    //            if (error == "X")
    //            {
    //                error2 = error2 + error;
    //            }
    //            if (contador == 1)
    //            {
    //                //La primera vuelta que de dejaremos el valor en la segunda aduana guardado para poder comparar
    //                aduana2 = aduana;
    //                cliente2 = cliente;
    //            }
    //            else
    //            {
    //                //Si en alguna de las vueltas un gasto no es de la misma aduana entonces no podra pasar
    //                if (aduana != aduana2 || cliente != cliente2)
    //                {
    //                    error2 = error2 + "1";
    //                }
    //            }
    //        }
    //    }
    //    if (error2 == "" && contador > 1)
    //    {
    //        EnviarCorreoVarios(hojas + "'", cliente);
    //        llenarGrid();
    //    }
    //    else
    //    {
    //        registrarMensaje("Favor Verifique Su Eleccion Ya Que Posiblemente Selecciono Alguna Hora de Ruta Que no Tiene Todos Los Documentos Recuperados");
    //    }
    //}
    protected void btnGuardar_Click1(object sender, ImageClickEventArgs e)
    {
        string error = "", error2 = "", hojas = "'0", cliente = "", cliente2 = "", aduana = "", aduana2 = "";
        int contador = 0;
        foreach (GridDataItem grdItem in rgFacturacion.Items)
        {
            System.Web.UI.WebControls.CheckBox chkAgrupar = (System.Web.UI.WebControls.CheckBox)grdItem.FindControl("chbAgrupar");

            if (chkAgrupar.Checked)
            {
                if (grdItem.Cells[9].Text == "0.0000")
                {
                    error = "";
                    contador = contador + 1;
                    hojas = hojas + "','" + grdItem.Cells[4].Text;
                    cliente = grdItem.Cells[5].Text;
                    aduana = grdItem.Cells[6].Text;
                }
                else
                {
                    error = "X";
                }
                if (error == "X")
                {
                    error2 = error2 + error;
                }
                if (contador == 1)
                {
                    //La primera vuelta que de dejaremos el valor en la segunda aduana guardado para poder comparar
                    aduana2 = aduana;
                    cliente2 = cliente;
                }
                else
                {
                    //Si en alguna de las vueltas un gasto no es de la misma aduana entonces no podra pasar
                    if (aduana != aduana2 || cliente != cliente2)
                    {
                        error2 = error2 + "1";
                    }
                }
            }
        }
        if (error2 == "" && contador > 1)
        {
            EnviarCorreoVarios(hojas + "'", cliente);
            llenarGrid();
        }
        else
        {
            registrarMensaje("Favor Verifique Su Eleccion Ya Que Posiblemente Selecciono Alguna Hora de Ruta Que no Tiene Todos Los Documentos Recuperados");
        }
    }
}