﻿
using System;
using System.Collections.Generic;
using System.EnterpriseServices.Internal;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;

/// <summary>
/// Summary description for ProcesoFlujoCargaGridVm
/// </summary>
public class ProcesoFlujoCargaGridVm
{
    public decimal Id { get; set; }
    public string IdInstruccion { get; set; }
    public string Cliente { get; set; }
    public string Proveedor { get; set; }
    public string CodRegimen { get; set; }
    public string NoFactura { get; set; }
    public string Correlativo { get; set; }
    public string ComprobantePago { get; set; }
    public string CanalSelectividad { get; set; }
    public string CodigoAduana { get; set; }
    public string Estado { get; set; }
    public DateTime? FechaUltimoSello { get; set; }
    public string Producto { get; set; }
    public string TiempoTranscurrido { get; set; }
    public decimal? IdTramitador { get; set; }

    public ProcesoFlujoCargaGridVm()
    { }
}