﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteCargaTrabajoDetalle.aspx.cs" Inherits="ReporteCargaTrabajoDetalle" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="miDiv" runat="server" class="panelCentrado2">
        <telerik:RadGrid ID="rgReporteCargaTrabajoDetalle" runat="server" AllowFilteringByColumn="false" 
            ShowFooter="true" ShowGroupFooter="true"  OnItemCommand="rgReporteCargaTrabajoDetalle_ItemCommand"
            AllowSorting="True" AutoGenerateColumns="false" GridLines="None" Width="100%" RenderMode="Lightweight"
            Height="420px" OnNeedDataSource="rgReporteCargaTrabajoDetalle_NeedDataSource"  AllowPaging="True"
            ShowStatusBar="True" PageSize="50">
            <HeaderContextMenu EnableTheming="True">
                <CollapseAnimation Duration="200" Type="OutQuint" />
            </HeaderContextMenu>
            <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
            <MasterTableView CommandItemDisplay="Top" HeaderStyle-Font-Bold="true" GroupHeaderItemStyle-CssClass="bold"
                NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay instrucciones definidas."
                GroupLoadMode="Client" ShowGroupFooter="true" AllowMultiColumnSorting="true">
                <GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="Cliente" FieldName="Cliente" FormatString="{0:D}"
                                HeaderValueSeparator=" : "></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldName="Cliente" SortOrder="Descending"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>
                <Columns>
                    <telerik:GridBoundColumn Aggregate="Count" DataField="Instruccion" HeaderText="Instruccion"
                                FooterText="Total Instrucciones : ">
                     </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Producto" HeaderText="Producto" UniqueName="Producto">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Regimen" HeaderText="Regimen" UniqueName="Regimen">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CanalSelectividad" HeaderText="Canal De Selectividad" UniqueName="CanalSelectividad">
                    </telerik:GridBoundColumn>
                </Columns>
                <CommandItemSettings ShowAddNewRecordButton="False" RefreshText=""
                    ShowExportToExcelButton="True" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px"></HeaderStyle>
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px"></HeaderStyle>
                </ExpandCollapseColumn>
            </MasterTableView>
            <HeaderStyle Width="180px" />
            <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                <Selecting AllowRowSelect="True" />
                <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                    DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                    DropHereToReorder="Suelte aquí para Re-Ordenar" />
                <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
            </ClientSettings>
            <FilterMenu EnableTheming="True">
                <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
            </FilterMenu>
            <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
            <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
        </telerik:RadGrid>
    </div>
</asp:Content>
