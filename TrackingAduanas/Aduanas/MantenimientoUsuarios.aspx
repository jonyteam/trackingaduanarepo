﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MantenimientoUsuarios.aspx.cs"
    Inherits="MantenimientoUsuarios" MasterPageFile="~/Site.master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadTabStrip ID="RadTabStrip1" runat="server" SelectedIndex="1" MultiPageID="RadMultiPage1">
        <Tabs>
            <telerik:RadTab runat="server" Text="Usuarios" PageViewID="pvUsuarios" />
            <telerik:RadTab runat="server" Text="Roles" PageViewID="pvRoles" Selected="True" />
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="RadMultiPage1" runat="server" SelectedIndex="1" Width="99%">
        <telerik:RadPageView ID="pvUsuarios" runat="server" Width="100%">
            <telerik:RadGrid ID="rgUsuarios" runat="server" AllowFilteringByColumn="True" AllowSorting="True"
                AutoGenerateColumns="False" GridLines="Horizontal" Width="100%" Height="550px"
                OnNeedDataSource="rgUsuarios_NeedDataSource" OnUpdateCommand="rgUsuarios_UpdateCommand"
                OnDeleteCommand="rgUsuarios_DeleteCommand" OnInsertCommand="rgUsuarios_InsertCommand"
                AllowPaging="True" ShowFooter="True" ShowStatusBar="True" PageSize="20" OnItemDataBound="rgUsuarios_ItemDataBound"
                OnItemCreated="rgUsuarios_ItemCreated" 
                OnItemCommand="rgUsuarios_ItemCommand" CellSpacing="0" Culture="es-ES">
                <HeaderContextMenu EnableTheming="True">
                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                </HeaderContextMenu>
                <PagerStyle NextPagesToolTip="Páginas Siguientes" NextPageToolTip="Página Siguiente"
                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                <MasterTableView DataKeyNames="IdUsuario,CodigoAduana" CommandItemDisplay="<%# Ingresar ? GridCommandItemDisplay.Top : GridCommandItemDisplay.None %>"
                    NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay Usuarios definidos.">
                    <CommandItemSettings AddNewRecordText="Agregar Usuario" RefreshText="Volver a Cargar Datos" />
                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridEditCommandColumn AutoPostBackOnFilter="True" CancelText="Cancelar"
                            EditText="Editar" InsertText="Insertar" UpdateText="Grabar" ButtonType="ImageButton">
                            <HeaderStyle Width="5%" />
                            <ItemStyle Width="5%" />
                        </telerik:GridEditCommandColumn>
                        <telerik:GridButtonColumn ConfirmText="¿Esta seguro de que desea eliminar este usuario?"
                            ConfirmDialogType="RadWindow" ConfirmTitle="Eliminar Usuario" UniqueName="DeleteColumn"
                            ButtonType="ImageButton" CommandName="Delete">
                            <HeaderStyle Width="5%" />
                            <ItemStyle Width="5%" />
                        </telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="Usuario" HeaderText="Usuario" UniqueName="Usuario">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Nombre" HeaderText="Nombre" UniqueName="Nombre">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Apellido" HeaderText="Apellido" UniqueName="Apellido">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Email" HeaderText="eMail" UniqueName="Email"
                            Aggregate="Count" FooterText="Cantidad: ">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="CodigoAduana" HeaderText="CodigoAduana" UniqueName="CodigoAduana"
                            Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridButtonColumn ButtonType="ImageButton" HeaderText="Resetear Password"
                            CommandName="Reset" ImageUrl="Images/16/user1_refresh_16.png" UniqueName="btnReset">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                        </telerik:GridButtonColumn>
                            <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn2 column"
                                    UniqueName="Esquema" AllowFiltering="False"   HeaderText="Esquema Tramite" >
                                    <ItemTemplate >
                                        <telerik:RadButton ID="ImageButton1" runat="server" CommandName="habilitar"                                             
                                            OnClientClick="radconfirm('Desea Habilitar Usuario en en Esquema ?',confirmCallBackSalvar, 300, 100); return false;"
                                            CommandArgument='<%#( Eval("Usuario").ToString())+","+Eval("esquema").ToString() %>'
                                            ToolTip='<%# (Eval("esquema").ToString() == "1" ? "Desactivar" : "Habilitar") %>'    Text='<%# (Eval("esquema").ToString() == "1" ? "Desactivar" : "Habilitar") %>'  
                                            Icon-PrimaryIconUrl='<%# (Eval("esquema").ToString() == "0" ? "~/Images/16/check2_16.png" : "~/Images/16/delete2_16.png") %>'
                                             OnCommand="ImageButton1_Command"                                          
                                             Width="100px"  >
                                            <Icon />
                                             </telerik:RadButton>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>

                        <telerik:GridBoundColumn DataField="IdUsuario" Display="False" FilterControlAltText="Filter IdUsuario column" HeaderText="IdUsuario" UniqueName="IdUsuario" Visible="False">
                        </telerik:GridBoundColumn>

                    </Columns>
                    <EditFormSettings EditFormType="Template" CaptionDataField="Categoria" CaptionFormatString="">
                        <EditColumn FilterControlAltText="Filter EditCommandColumn1 column" UniqueName="EditCommandColumn1">
                        </EditColumn>
                        <FormTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# ((Container is GridEditFormInsertItem) ? "Agregar Nuevo Usuario" : "Editar Usuario Existente") %>'
                                Font-Size="Medium" Font-Bold="true" />
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 10%">
                                    </td>
                                    <td style="width: 90%">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="lbUsuario" runat="server" Text="Usuario" Font-Bold="true" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadTextBox ID="edUsuario" runat="server" Skin='<%# Skin %>' Text='<%# Bind( "Usuario") %>'
                                            EmptyMessage="Escriba el Usuario aquí" Width="50%" MaxLength="50" />
                                        <asp:RequiredFieldValidator ID="rfUsuario" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="El Usuario no puede estar vacío" ControlToValidate="edUsuario" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="lbNombre" runat="server" Text="Nombre" Font-Bold="true" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadTextBox ID="edNombre" runat="server" Skin='<%# Skin %>' Text='<%# Bind( "Nombre") %>'
                                            EmptyMessage="Escriba el Nombre aquí" Width="50%" MaxLength="50" />
                                        <asp:RequiredFieldValidator ID="rfNombre" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="El Nombre no puede estar vacío" ControlToValidate="edNombre" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="Label2" runat="server" Text="Apellido" Font-Bold="true" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadTextBox ID="edApellido" runat="server" Skin='<%# Skin %>' Text='<%# Bind("Apellido") %>'
                                            MaxLength="50" EmptyMessage="Escriba el Apellido aquí" Width="50%" />
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="El Apellido no puede estar vacío" ControlToValidate="edApellido" />--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="Label3" runat="server" Text="Identidad" Font-Bold="true" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadTextBox ID="edIdentidad" runat="server" Skin='<%# Skin %>' Text='<%# Bind( "Identidad") %>'
                                            EmptyMessage="Escriba la Identidad aquí" Width="50%" MaxLength="17" />
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="La Identidad no puede estar vacío" ControlToValidate="edIdentidad" />--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="Label4" runat="server" Text="eMail" Font-Bold="true" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadTextBox ID="edEmail" runat="server" Skin='<%# Skin %>' Text='<%# Bind( "Email") %>'
                                            EmptyMessage="Escriba el eMail aquí" Width="50%" MaxLength="70" />
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="El eMail no puede estar vacío" ControlToValidate="edEmail" />--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="Label5" runat="server" Text="Celular" Font-Bold="true" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadNumericTextBox ID="edCelular" runat="server" Skin='<%# Skin %>' Text='<%# Bind( "Celular") %>'
                                            Width="50%" MaxLength="20" NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator=""
                                            EmptyMessage="Escriba el Celular aquí" />
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="El Celular no puede estar vacío" ControlToValidate="edCelular" />--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="Label6" runat="server" Text="Teléfono" Font-Bold="true" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadNumericTextBox ID="edTelefono" runat="server" Skin='<%# Skin %>' Text='<%# Bind( "Telefono") %>'
                                            NumberFormat-DecimalDigits="0" NumberFormat-GroupSeparator="" EmptyMessage="Escriba el Teléfono aquí"
                                            Width="50%" MaxLength="20" />
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="El Teléfono no puede estar vacío" ControlToValidate="edTelefono" />--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="lblExtension" runat="server" Text="Extensión" Font-Bold="true" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadNumericTextBox ID="edExtension" runat="server" NumberFormat-DecimalDigits="0"
                                            NumberFormat-GroupSeparator="" Skin='<%# Skin %>' Text='<%# Bind("Extension") %>'
                                            EmptyMessage="Escriba la extensión aquí" Width="50%" MaxLength="10">
                                        </telerik:RadNumericTextBox>
                                        <asp:RequiredFieldValidator ID="rvExtension" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="La extensión no puede estar vacía" ControlToValidate="edExtension" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="lblPais" runat="server" Text="País" Font-Bold="true" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadComboBox ID="edPais" runat="server" Skin='<%# Skin %>' DataTextField="Descripcion"
                                            Filter="Contains" DataValueField="Codigo" DataSource='<%# GetPaises() %>' SelectedValue='<%# Bind("CodPais") %>'
                                            EmptyMessage="Seleccione el País" Width="50%" CausesValidation="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="lblAduana" runat="server" Text="Aduana" Font-Bold="true" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadComboBox ID="edAduana" runat="server" Skin='<%# Skin %>' DataTextField="NombreAduana"
                                            DataValueField="CodigoAduana" EmptyMessage="Seleccione la aduana" Width="50%"
                                            CausesValidation="false" />
                                    </td>
                                </tr>
                            </table>
                            <asp:ImageButton ID="btnSave" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                AlternateText='<%# (Container is GridEditFormInsertItem) ? "Agregar Usuario" : "Actualizar Usuario" %>'
                                ImageUrl='Images/16/check2_16.png' />
                            &nbsp
                            <asp:ImageButton ID="btnCancel" runat="server" CommandName="Cancel" AlternateText="Cancelar"
                                ImageUrl='Images/16/delete2_16.png' CausesValidation="false" />
                        </FormTemplate>
                    </EditFormSettings>
                </MasterTableView>
                <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                    <Selecting AllowRowSelect="True" />
                    <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                        DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                        DropHereToReorder="Suelte aquí para Re-Ordenar" />
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                </ClientSettings>
                <FilterMenu EnableTheming="True">
                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                </FilterMenu>
                <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
            </telerik:RadGrid>
        </telerik:RadPageView>
        <telerik:RadPageView ID="pvRoles" runat="server" Width="100%">
            <telerik:RadGrid ID="rgRoles" runat="server" AllowFilteringByColumn="True" AllowSorting="True"
                AutoGenerateColumns="False" GridLines="Horizontal" Width="100%" Height="550px"
                OnNeedDataSource="rgRoles_NeedDataSource" OnUpdateCommand="rgRoles_UpdateCommand"
                OnDeleteCommand="rgRoles_DeleteCommand" OnInsertCommand="rgRoles_InsertCommand"
                AllowPaging="True" ShowFooter="True" ShowStatusBar="True" PageSize="20" ShowGroupPanel="True"
                OnItemDataBound="rgRoles_ItemDataBound">
                <HeaderContextMenu EnableTheming="True">
                    <CollapseAnimation Duration="200" Type="OutQuint" />
                </HeaderContextMenu>
                <GroupPanel Text="Arrastre el encabezado de una columna aquí para agrupar por esa columna">
                </GroupPanel><PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                    PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                    Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                <MasterTableView DataKeyNames="IdUsuario,IdRol" CommandItemDisplay="<%# Ingresar ? GridCommandItemDisplay.Top : GridCommandItemDisplay.None %>"
                    NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay Usuarios definidos."
                    GroupLoadMode="Client">
                    <CommandItemSettings AddNewRecordText="Agregar Usuario a Perfil" RefreshText="Volver a Cargar Datos" />
                    <RowIndicatorColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </RowIndicatorColumn>
                    <ExpandCollapseColumn>
                        <HeaderStyle Width="20px"></HeaderStyle>
                    </ExpandCollapseColumn>
                    <Columns>
                        <telerik:GridEditCommandColumn AutoPostBackOnFilter="True" CancelText="Cancelar"
                            EditText="Editar" InsertText="Insertar" UpdateText="Grabar" ButtonType="ImageButton">
                            <HeaderStyle Width="5%" />
                            <ItemStyle Width="5%" />
                        </telerik:GridEditCommandColumn>
                        <telerik:GridButtonColumn ConfirmText="¿Esta seguro de que desea eliminar este usuario del perfil?"
                            ConfirmDialogType="RadWindow" ConfirmTitle="Eliminar Usuario del Perfil" UniqueName="DeleteColumn"
                            ButtonType="ImageButton" CommandName="Delete">
                            <HeaderStyle Width="5%" />
                            <ItemStyle Width="5%" />
                        </telerik:GridButtonColumn>
                        <telerik:GridBoundColumn DataField="Usuario" HeaderText="Usuario" UniqueName="column1">
                            <HeaderStyle Width="26%" />
                            <ItemStyle Width="26%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="NombreCompleto" HeaderText="Nombre" UniqueName="column3"
                            Aggregate="Count" FooterText="Cantidad: ">
                            <HeaderStyle Width="26%" />
                            <ItemStyle Width="26%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="RolDescripcion" HeaderText="Perfil" UniqueName="column2">
                            <HeaderStyle Width="26%" />
                            <ItemStyle Width="26%" />
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn DataField="Eliminado" UniqueName="column4" HeaderText="Eliminado"
                            AllowFiltering="false">
                            <ItemTemplate>
                                <asp:ImageButton ID="btnActivo" runat="server" CommandName='<%# (Eval("Eliminado").ToString() == "0" ? "Desactivar" : "Activar") %>'
                                    CommandArgument='<%# String.Format("{0},{1}", Eval("IdUsuario").ToString(), Eval("IdRol").ToString()) %>'
                                    OnClick="btnActivo_Click" ImageUrl='<%# (Eval("Eliminado").ToString() == "0" ? "~/Images/16/check2_16.png" : "~/Images/16/delete2_16.png") %>' /></ItemTemplate>
                            <HeaderStyle Width="12%" />
                            <ItemStyle Width="12%" />
                        </telerik:GridTemplateColumn>
                    </Columns>
                    <GroupByExpressions>
                        <telerik:GridGroupByExpression>
                            <SelectFields>
                                <telerik:GridGroupByField FieldAlias="Perfil" FieldName="RolDescripcion"></telerik:GridGroupByField>
                            </SelectFields>
                            <GroupByFields>
                                <telerik:GridGroupByField FieldAlias="Perfil" FieldName="RolDescripcion"></telerik:GridGroupByField>
                            </GroupByFields>
                        </telerik:GridGroupByExpression>
                    </GroupByExpressions>
                    <EditFormSettings EditFormType="Template" CaptionDataField="ModuloDescripcion" CaptionFormatString="">
                        <EditColumn UniqueName="EditCommandColumn1">
                        </EditColumn>
                        <FormTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# ((Container is GridEditFormInsertItem) ? "Agregar Usuario al Perfil" : "Editar Usuario del Perfil") %>'
                                Font-Size="Medium" Font-Bold="true" />
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 10%">
                                    </td>
                                    <td style="width: 90%">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="Label2" runat="server" Text="Usuario" Font-Bold="true" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadComboBox ID="edUsuario" runat="server" DataSource='<%# GetUsuarios() %>'
                                            DataValueField="IdUsuario" DataTextField="NombreCompleto" SelectedValue='<%# Bind( "IdUsuario") %>'
                                            Width="50%" Enabled='<%# (Container is GridEditFormInsertItem) ? true : false %>'
                                            Font-Bold='<%# (Container is GridEditFormInsertItem) ? false : true %>' />
                                        <asp:RequiredFieldValidator ID="rfUsuario" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="El Usuario no puede estar vacío" ControlToValidate="edUsuario" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 10%">
                                        <asp:Label ID="Label3" runat="server" Text="Perfil" Font-Bold="true" />
                                    </td>
                                    <td style="width: 90%">
                                        <telerik:RadComboBox ID="edRol" runat="server" DataSource='<%# GetRoles() %>' DataTextField="Descripcion"
                                            DataValueField="IdRol" SelectedValue='<%# Bind( "IdRol") %>' Width="50%" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="El Perfil no puede estar vacío" ControlToValidate="edRol" />
                                    </td>
                                </tr>
                            </table>
                            <asp:ImageButton ID="btnSave" runat="server" CommandName='<%# (Container is GridEditFormInsertItem) ? "PerformInsert" : "Update" %>'
                                AlternateText='<%# (Container is GridEditFormInsertItem) ? "Agregar Usuario al Perfil" : "Actualizar Usuario del Perfil" %>'
                                ImageUrl='<%# GetTelerikResourceUrl("Grid.Update.gif") %>' />&#160;&nbsp<asp:ImageButton
                                    ID="btnCancel" runat="server" CommandName="Cancel" AlternateText="Cancelar" ImageUrl='<%# GetTelerikResourceUrl("Grid.Cancel.gif") %>'
                                    CausesValidation="false" /></FormTemplate>
                    </EditFormSettings>
                </MasterTableView><GroupingSettings CollapseTooltip="Contraer grupo" ExpandTooltip="Expandir grupo"
                    GroupContinuedFormatString="... continuación del grupo de la página anterior. "
                    GroupContinuesFormatString="El grupo continua en la siguiente página." GroupSplitDisplayFormat="Mostrando {0} de {1} elementos."
                    UnGroupTooltip="Arrastre fuera de la barra para desagrupar" />
                <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True" AllowDragToGroup="True">
                    <Selecting AllowRowSelect="True" />
                    <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                        DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                        DropHereToReorder="Suelte aquí para Re-Ordenar" />
                    <Scrolling AllowScroll="True" UseStaticHeaders="True" />
                </ClientSettings>
                <FilterMenu EnableTheming="True">
                    <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                </FilterMenu>
                <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
            </telerik:RadGrid>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgUsuarios">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgUsuarios" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rgRoles">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgRoles" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>
