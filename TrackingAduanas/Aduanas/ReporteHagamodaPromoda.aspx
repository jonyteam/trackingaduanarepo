﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteHagamodaPromoda.aspx.cs" Inherits="GenerarArchivoSAPDinant" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
                    //<![CDATA[
                    
                function pnlRequestStarted(ajaxPanel, eventArgs) {
                    try {
                        if ((eventArgs.EventTarget == "<%= btnSalvar.UniqueID %>")) {
                            eventArgs.EnableAjax = false;
                        }
                    }
                    catch (err)
                { }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                    }
                }
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 || args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }
                   
            </script>
        </telerik:RadScriptBlock>
        <table border="1" style="width: 55%">
            <tr>
                <td style="width: 15%">
                    <asp:Label ID="lblFechaInicio" runat="server" Text="Fecha Inicio:" ForeColor="Black"></asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadDatePicker ID="dpFechaInicio" runat="server" EnableTyping="False" Width="95%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="dpFechaInicio"
                        ErrorMessage="Ingrese Fecha" ForeColor="Red" />
                </td>
                <td style="width: 15%">
                    <asp:Label ID="lblFechaFinal" runat="server" Text="Fecha Final:" ForeColor="Black"></asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadDatePicker ID="dpFechaFinal" runat="server" EnableTyping="False" Width="95%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator runat="server" ID="reqName" ControlToValidate="dpFechaFinal"
                        ErrorMessage="Ingrese Fecha" ForeColor="Red" />
                </td>
                <td style="width: 15%">
                    <asp:Label ID="Label1" runat="server" Text="Cliente" ForeColor="Black"></asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadComboBox ID="cmbCliente" runat="server">
                        <Items>
                            <telerik:RadComboBoxItem Value="Seleccione" Text="Seleccione" 
                                Owner="cmbCliente" />
                            <telerik:RadComboBoxItem Value="8000825" Text="Hagamoda" Owner="cmbCliente" />
                            <telerik:RadComboBoxItem Value="8000837" Text="Promoda" Owner="cmbCliente" />
                            <telerik:RadComboBoxItem runat="server" Text="Iberomoda" Value="8000882" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td colspan="6" align="center">
                    <input id="iTipoFlete" runat="server" type="hidden" />
                    <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Images/24/view_24.png"
                        OnClick="btnBuscar_Click" />
                </td>
            </tr>
        </table>
        <br />
        <telerik:RadGrid ID="RadGrid1" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" CellSpacing="0" 
            GridLines="None" Width="2413px" onitemcommand="RadGrid1_ItemCommand" 
            onneeddatasource="RadGrid1_NeedDataSource" PageSize="20">
            <ClientSettings AllowColumnsReorder="True">
                <Selecting AllowRowSelect="True" />
            </ClientSettings>
             <ExportSettings HideStructureColumns="true" 
                FileName ="Reporte Promoda/Hagamoda" OpenInNewWindow="True">  <Excel Format ="Biff" />  </ExportSettings>
            <MasterTableView CommandItemDisplay="Top">
                <CommandItemSettings ShowExportToExcelButton="true" ExportToPdfText="Export to PDF" ShowAddNewRecordButton = "false" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                    Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                    Visible="True">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="IdInstruccion" 
                        FilterControlAltText="Filter IdInstruccion column" HeaderText="Hoja de Ruta" 
                        UniqueName="IdInstruccion">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Proveedor" 
                        FilterControlAltText="Filter Proveedor column" HeaderText="Proveedor" 
                        UniqueName="Proveedor">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Cliente" 
                        FilterControlAltText="Filter Nombre column" HeaderText="Cliente" 
                        UniqueName="Nombre">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Factura" 
                        FilterControlAltText="Filter DocumentoNo column" HeaderText="Factura" 
                        UniqueName="DocumentoNo">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ValorDocumentoEmbarque" 
                        FilterControlAltText="Filter DocumentoNo column" 
                        HeaderText="Valor Documento Embarque" UniqueName="DocumentoNo">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CantidadBultos" 
                        FilterControlAltText="Filter CantidadBultos column" 
                        HeaderText="Cantidad Bultos" UniqueName="CantidadBultos">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PesoKgs" 
                        FilterControlAltText="Filter PesoKgs column" HeaderText="PesoKgs" 
                        UniqueName="PesoKgs">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PesoMiami" 
                        FilterControlAltText="Filter PesoMiami column" HeaderText="Peso Miami" 
                        UniqueName="PesoMiami">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IngresoSwissport" 
                        FilterControlAltText="Filter IngresoSwissport column" 
                        HeaderText="Ingreso Swissport" UniqueName="IngresoSwissport">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="SalidaSwissport" 
                        FilterControlAltText="Filter SalidaSwissport column" 
                        HeaderText="Salida Swissport" UniqueName="SalidaSwissport">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Producto" 
                        FilterControlAltText="Filter Producto column" HeaderText="Producto" 
                        UniqueName="Producto">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ComienzoFlujo" 
                        FilterControlAltText="Filter GC1.Fecha column" HeaderText="Comienzo Flujo" 
                        UniqueName="GC1.Fecha">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ArribodeCarga" 
                        FilterControlAltText="Filter ArribodeCarga column" HeaderText="Arribo de Carga" 
                        UniqueName="ArribodeCarga">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ENVIODEPRELIQUIDACION" 
                        FilterControlAltText="Filter ENVIODEPRELIQUIDACION column" 
                        HeaderText="Envio de Preliquidacion" UniqueName="ENVIODEPRELIQUIDACION">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="APROBACIONDEPRELIQUIDACION" 
                        FilterControlAltText="Filter APROBACIONDEPRELIQUIDACION column" 
                        HeaderText="Aprobacion de Preliquidacion" 
                        UniqueName="APROBACIONDEPRELIQUIDACION">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ENVIODEBOLETIN" 
                        FilterControlAltText="Filter ENVIODEBOLETIN column" 
                        HeaderText="Envio de Boletin" UniqueName="ENVIODEBOLETIN">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PagodeImpuestos" 
                        FilterControlAltText="Filter PagodeImpuestos column" 
                        HeaderText="Pago de Impuestos" UniqueName="PagodeImpuestos">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Color" 
                        FilterControlAltText="Filter Color column" HeaderText="Color" 
                        UniqueName="Color">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="RevisionMercancia" 
                        FilterControlAltText="Filter RevisionMercancia column" 
                        HeaderText="Revision Mercancia" UniqueName="RevisionMercancia">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EmisionPaseSalida" 
                        FilterControlAltText="Filter EmisionPaseSalida column" 
                        HeaderText="Emision Pase Salida" UniqueName="EmisionPaseSalida">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EntregadeServicios" 
                        FilterControlAltText="Filter EntregadeServicios column" 
                        HeaderText="Entregade Servicios" UniqueName="EntregadeServicios">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IngresoDeposito" 
                        FilterControlAltText="Filter IngresoDeposito column" 
                        HeaderText="Ingreso a Deposito" UniqueName="IngresoDeposito">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EntregaCliente" 
                        FilterControlAltText="Filter EntregaCliente column" 
                        HeaderText="Entrega al Cliente" UniqueName="EntregaCliente">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Aforador" 
                        FilterControlAltText="Filter Aforador column" HeaderText="Aforador" 
                        UniqueName="Aforador">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TiempoProceso" 
                        FilterControlAltText="Filter TiempoProceso column" HeaderText="Tiempo Proceso" 
                        UniqueName="TiempoProceso">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TiempoProceso2" 
                        FilterControlAltText="Filter TiempoProceso2 column" 
                        HeaderText="Tiempo Proceso Arribo Carga" UniqueName="TiempoProceso2">
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        <br />
        <br />
        <table width="100%">
            <tr align="center">
                <td colspan="6" align="center">
                    <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="~/Images/gris.png"/>
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
    <%--<GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>--%>
</asp:Content>
