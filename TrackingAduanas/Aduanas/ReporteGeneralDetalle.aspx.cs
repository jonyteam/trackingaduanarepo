﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ObjetosDto;
using Telerik.Web.UI;
using Toolkit.Core.Extensions;
using Utilities.Web;

public partial class ReporteGeneralDetalle : Utilidades.PaginaBase
{
    readonly AduanasDataContext _aduanasDc = new AduanasDataContext();
    private string codAduana;
    private string regimen;
    private string fechaI;
    private string fechaF;
    private string tipoAduana;
    private string clientes;
    private string seleccion;
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "ReporteHagamodaPromoda";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SetGridFilterMenu(rgReporteGeneralDetalle.FilterMenu);
        rgReporteGeneralDetalle.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgReporteGeneralDetalle.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Reporte Intrucciones Finalizadas Detalle.", "Reporte Intrucciones Finalizadas Detalle.");
            //if (!(Anular || Ingresar || Modificar))
            //    redirectTo("Default.aspx");
        }

        codAduana = Request.QueryString["idaduana"];
        regimen = Request.QueryString["regimen"];
        fechaI = Request.QueryString["fI"];
        fechaF = Request.QueryString["fF"];
        tipoAduana = Request.QueryString["busqueda"];
        clientes = Request.QueryString["clientes"];
        seleccion = Request.QueryString["seleccion"];

    }

    private bool Anular { get { return tienePermiso("ANULAR"); } }
    private bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    private bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    public override bool CanGoBack { get { return false; } }

    #region Llenar Grid
    protected void rgReporteGeneralDetalle_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        LlenarGrid();
    }

    private void LlenarGrid()
    {
        conectar();
        InstruccionesBO i = new InstruccionesBO(logApp);
        string regimenRango = Regimen();
        try
        {
            string aduana = Aduana(codAduana);

            string buscarPor = seleccion.Substring(0, 1);
            string buscarPor2 = seleccion.Substring(2);

            i.ReporteGeneral(aduana, regimenRango, fechaI, fechaF, tipoAduana.ToInt(), buscarPor.ToInt(),
                buscarPor2.ToInt(), clientes, regimen);
            rgReporteGeneralDetalle.DataSource = i.TABLA;
            rgReporteGeneralDetalle.DataBind();
        }
        catch (Exception ex){}
        finally{desconectar();}
    }

    private string Regimen()
    {
        string regimenRango = "";
        if (regimen == "E")
        {
            regimenRango = "'1000' and '3155'";
        }
        else if (regimen == "I")
        {
            regimenRango = "'4000' and '7070'";
        }
        else if (regimen == "Tr")
        {
            regimenRango = "'8000' and '8970'";
        }
        else if(regimen == "T")
        {
            regimenRango = "'1000' and '8970'";
        }

        return regimenRango;
    }


    private string Aduana(string idAduana)
    {
        if (idAduana == "T" && tipoAduana == "1")
        {
            idAduana = "'014', '017', '020', '018', '021', '019', '015', '022'";
        }
        else if (idAduana == "T" && tipoAduana == "2")
        {
            idAduana = "'016', '024', '023'";
        }

        return idAduana;
    }

    private void ConfigureExport(string nombre)
    {
        rgReporteGeneralDetalle.AllowFilteringByColumn = false;
        String filename = nombre + "_" + DateTime.Now.ToShortDateString();
        rgReporteGeneralDetalle.GridLines = GridLines.Both;
        rgReporteGeneralDetalle.ExportSettings.FileName = filename;
        rgReporteGeneralDetalle.ExportSettings.IgnorePaging = true;
        rgReporteGeneralDetalle.ExportSettings.OpenInNewWindow = false;
        rgReporteGeneralDetalle.ExportSettings.ExportOnlyData = true;
    }

    //protected void rgAsistenteOperaciones_Init(object sender, EventArgs e)
    //{
    //    var menu = rgReporteGeneralDetalle.FilterMenu;
    //    menu.Items.RemoveAt(rgReporteGeneralDetalle.FilterMenu.Items.Count - 2);
    //}

    private void RegistrarMensaje2(string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("alert('" + mensaje + "');");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "mensaje", sb.ToString(), false);
    }

    protected void rgInstrucciones_ItemCommand(object sender, Telerik.Web.UI.GridCommandEventArgs e)
    {
        try
        {
            conectar();
            if (rgReporteGeneralDetalle.Items.Count <= 0 & e.CommandName == RadGrid.ExportToExcelCommandName)
                e.Canceled = true;
            else if (e.CommandName == Telerik.Web.UI.RadGrid.ExportToExcelCommandName)
            {
                ConfigureExport("ReporteGeneralInstruccionesFinalizadas");
            }
        }
        catch (Exception)
        { }
    }
    #endregion
}