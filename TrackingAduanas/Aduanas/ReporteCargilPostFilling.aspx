﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteCargilPostFilling.aspx.cs" Inherits="ReporteCargillPostFiling" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style12
        {
            width: 100%;
            height: 50px;
        }
        #tabla1 {
            margin-left: 425px;
            width: 450px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="pnlRequestStarted">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">

                function pnlRequestStarted(ajaxPanel, eventArgs) {
                    try {
                        if ((eventArgs.EventTarget == "<%= btnSalvar.UniqueID %>")) {
                            eventArgs.EnableAjax = false;
                        }
                    }
                    catch (err)
                { }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                    }
                }                                  
            </script>
        </telerik:RadScriptBlock>
       
        <br />
        <table border="1"  id="tabla1">
            <tr>
                <td style="width: 5%">
                    <asp:Label ID="lblInstruccion" runat="server" forecolor="Black" 
                        text="Instruccion:">
                </asp:Label>
                </td>
                <td style="width: 5%">
                    <telerik:RadTextBox runat="server" ID="txtInstruccion"></telerik:RadTextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" forecolor="Black" text="Cliente:"></asp:Label>
                </td>
                <td>
                    <telerik:RadComboBox ID="cmbCliente" runat="server" DataTextField="Nombre" 
                        DataValueField="CodigoSAP" Filter="StartsWith" Width="100%" Enabled="False">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="6">
                    <asp:ImageButton ID="btnBuscar" runat="server" 
                        imageurl="~/Images/24/view_24.png" onclick="btnBuscar_Click" />
                </td>
            </tr>
        </table>
        <br />
        <telerik:RadGrid ID="rgReporte" runat="server" AllowPaging="True" 
            AllowSorting="True" AutoGenerateColumns="False" BorderStyle="Solid" 
            CellSpacing="0" GridLines="None" 
            onneeddatasource="RadGrid1_NeedDataSource" PageSize="20" 
            style="margin-right: 0px" Width="2095px">
            <ClientSettings AllowColumnsReorder="True">
                <Selecting AllowRowSelect="True" />
            </ClientSettings>
            <ExportSettings FileName="Reporte" HideStructureColumns="true" 
                OpenInNewWindow="True">
                <Excel AutoFitImages="True" Format="Biff" />
            </ExportSettings>
            <MasterTableView CommandItemDisplay="Top" >
                <CommandItemSettings ExportToPdfText="Export to PDF" 
                    ShowAddNewRecordButton="false" ShowExportToExcelButton="false" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" 
                    Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" 
                    Visible="True">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="IdInstruccion" 
                        FilterControlAltText="Filter Instruccion column" HeaderText="Instruccion" 
                        UniqueName="IdInstruccion" FilterControlWidth="150px">
                        <ItemStyle Width="70px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="RegistrationNumber" 
                        FilterControlAltText="Filter RegistrationNumber column" 
                        HeaderText="Registration Number" UniqueName="RegistrationNumber" FilterControlWidth="200px">
                        <ItemStyle Width="120px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ExportingCountry" 
                        FilterControlAltText="Filter ExportingCountry column" HeaderText="Exporting Country" 
                        UniqueName="ExportingCountry">
                        <ItemStyle Width="70px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EntryType" 
                        FilterControlAltText="Filter EntryType column" HeaderText="Entry Type" 
                        UniqueName="EntryType">
                        <ItemStyle Width="90px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ImporterOfRecordNumber" 
                        FilterControlAltText="Filter ImporterOfRecordNumber column" HeaderText="Importer of Record Number" 
                        UniqueName="ImporterOfRecordNumber">
                        <ItemStyle Width="70px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ControlCarga" 
                        FilterControlAltText="Filter ControlCarga column" HeaderText="Cargo Control No." 
                        UniqueName="ControlCarga">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TotalValue" 
                        FilterControlAltText="Filter TotalValue column" HeaderText="Total Value" 
                        UniqueName="TotalValue">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TotalValueCurrency" 
                        FilterControlAltText="Filter TotalValueCurrency column" HeaderText="Total Value Currency" 
                        UniqueName="TotalValueCurrency">
                    </telerik:GridBoundColumn>
<%--                    <telerik:GridBoundColumn DataField="TasaCambio" 
                        FilterControlAltText="Filter TasaCambio column" HeaderText="Total TasaCambio Currency" 
                        UniqueName="TasaCambio">
                    </telerik:GridBoundColumn>--%>
                    <telerik:GridBoundColumn DataField="ExchangeRate" 
                        FilterControlAltText="Filter ExchangeRate column" HeaderText="Exchange Rate" 
                        UniqueName="ExchangeRate">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DateOfAcceptance" 
                        FilterControlAltText="Filter DateOfAcceptance column" HeaderText="Date of Acceptance" 
                        UniqueName="DateOfAcceptance">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EntryDate" 
                        FilterControlAltText="Filter EntryDate column" HeaderText="Entry Date" 
                        UniqueName="EntryDate">
                        <ItemStyle Width="140px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Freight" 
                        FilterControlAltText="Filter Freight column" HeaderText="Freight" 
                        UniqueName="Freight">
                        <ItemStyle Width="130px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FreightCurrency" 
                        FilterControlAltText="Filter FreightCurrency column" HeaderText="Freight Currency" 
                        UniqueName="FreightCurrency">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Insurance" 
                        FilterControlAltText="Filter Insurance column" 
                        HeaderText="Insurance" UniqueName="Insurance">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="InsuranceCurrency" 
                        FilterControlAltText="Filter InsuranceCurrency column" 
                        HeaderText="Insurance Currency" UniqueName="InsuranceCurrency">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Charges" 
                        FilterControlAltText="Filter OChargestros column" 
                        HeaderText="Charges" UniqueName="Charges">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ChargesCurrency" 
                        FilterControlAltText="Filter ChargesCurrency column" HeaderText="Charges Currency" 
                        UniqueName="ChargesCurrency">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Fees" 
                        FilterControlAltText="Filter Fees column" 
                        HeaderText="Fees" UniqueName="Fees">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="FeesCurrency" 
                        FilterControlAltText="Filter FeesCurrency column" 
                        HeaderText="Fees Currency" UniqueName="FeesCurrency">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="OtherTaxes" 
                        FilterControlAltText="Filter OtherTaxes column" 
                        HeaderText="Other Taxes" UniqueName="OtherTaxes">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="OtherTaxesCurrency" 
                        FilterControlAltText="Filter OtherTaxesCurrency column" 
                        HeaderText="Other Taxes Currency" UniqueName="OtherTaxesCurrency">
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <ItemStyle BorderColor="Black" BorderStyle="Solid" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        <br />
        <br/>
        
        <telerik:RadGrid runat="server" ID="rgGrid2" OnNeedDataSource="rgGrid2_OnNeedDataSource" AllowPaging="True"
            AllowSorting="True" AutoGenerateColumns="False" BorderStyle="Solid" CellSpacing="0" GridLines="None" 
            PageSize="20" style="margin-right: 0px;" Width="2095px">
            <ClientSettings AllowColumnsReorder="True">
                <Selecting AllowRowSelect="True"/>
            </ClientSettings>
            <ExportSettings FileName="Reporte" HideStructureColumns="True" OpenInNewWindow="True">
                <Excel AutoFitImages="True" Format="Biff"></Excel>
            </ExportSettings>
            <MasterTableView CommandItemDisplay="Top">
                <CommandItemSettings ExportToPdfText="Export To PDF" ShowAddNewRecordButton="False" ShowExportToExcelButton="False"></CommandItemSettings>
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator colum" Visible="True"></RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter Expand Column" Visible="True"></ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="IdInstruccion" 
                        FilterControlAltText="Filter Instruccion column" HeaderText="Instruccion" 
                        UniqueName="IdInstruccion" FilterControlWidth="150px">
                        <ItemStyle Width="70px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="GrossWeight" 
                        FilterControlAltText="Filter GrossWeight column" 
                        HeaderText="Gross weight" UniqueName="GrossWeight">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NetWeight" 
                        FilterControlAltText="Filter NetWeight column" 
                        HeaderText="Net weight" UniqueName="NetWeight">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NetWeightUnitOfMeasure" 
                        FilterControlAltText="Filter NetWeightUnitOfMeasure column" 
                        HeaderText="Net weight unit of measure" UniqueName="NetWeightUnitOfMeasure">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NetQuantity" 
                        FilterControlAltText="Filter NetQuantity column" 
                        HeaderText="Net Quantity" UniqueName="NetQuantity">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NetQuantityUnitOfMeasure" 
                        FilterControlAltText="Filter NetQuantityUnitOfMeasure column" 
                        HeaderText="Net Quantity unit of measure" UniqueName="NetQuantityUnitOfMeasure">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CountryOfOrigin" 
                        FilterControlAltText="Filter CountryOfOrigin column" 
                        HeaderText="Country of Origin" UniqueName="CountryOfOrigin">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CustomsStatusPlacement" 
                        FilterControlAltText="Filter CustomsStatusPlacement column" 
                        HeaderText="Customs Status Placement" UniqueName="CustomsStatusPlacement">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="HTSNumber" 
                        FilterControlAltText="Filter HTSNumber column" 
                        HeaderText="HTS Number" UniqueName="HTSNumber">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EnteredValue" 
                        FilterControlAltText="Filter EnteredValue column" 
                        HeaderText="Entered Value" UniqueName="EnteredValue">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EnteredValueCurrency" 
                        FilterControlAltText="Filter EnteredValueCurrency column" 
                        HeaderText="Entered Value Currency" UniqueName="EnteredValueCurrency">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DutyAmount" 
                        FilterControlAltText="Filter DutyAmount column" 
                        HeaderText="Duty Amount" UniqueName="DutyAmount">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DutyAmountCurrency" 
                        FilterControlAltText="Filter DutyAmountCurrency column" 
                        HeaderText="Duty Amount Currency" UniqueName="DutyAmountCurrency">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="VatAmount" 
                        FilterControlAltText="Filter VatAmount column" 
                        HeaderText="VAT Amount" UniqueName="VatAmount">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="VatAmountCurrency" 
                        FilterControlAltText="Filter VatAmountCurrency column" 
                        HeaderText="VAT Amount Currency" UniqueName="VatAmountCurrency">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TotalAmountInvoiced" 
                        FilterControlAltText="Filter TotalAmountInvoiced column" 
                        HeaderText="Party Relationship/Affiliation" UniqueName="TotalAmountInvoiced">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CurrencyOfTotalAmountInvoiced" 
                        FilterControlAltText="Filter CurrencyOfTotalAmountInvoiced column" 
                        HeaderText="Party Relationship/Affiliation" UniqueName="CurrencyOfTotalAmountInvoiced">
                    </telerik:GridBoundColumn>
                    <%--<telerik:GridBoundColumn DataField="" 
                        FilterControlAltText="Filter  column" 
                        HeaderText="Freight (Deductible)" UniqueName="">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="" 
                        FilterControlAltText="Filter  column" 
                        HeaderText="Freight (Deductible) Currency" UniqueName="">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ValorCIF" 
                        FilterControlAltText="Filter ValorCIF column" 
                        HeaderText="Total amount invoiced" UniqueName="ValorCIF">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="MonedaCIF" 
                        FilterControlAltText="Filter MonedaCIF column" 
                        HeaderText="Currency of total amount invoiced" UniqueName="MonedaCIF">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="" 
                        FilterControlAltText="Filter  column" 
                        HeaderText="Statistical Value Currency" UniqueName="Statistical Value Currency">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="" 
                        FilterControlAltText="Filter  column" 
                        HeaderText="Charges" UniqueName="Charges">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="" 
                        FilterControlAltText="Filter  column" 
                        HeaderText="Charges Currency" UniqueName="ChargesCurrency">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="" 
                        FilterControlAltText="Filter  column" 
                        HeaderText="Harbor Maintenance Fee (HMF)" UniqueName="HarborMaintenance">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="" 
                        FilterControlAltText="Filter  column" 
                        HeaderText="Harbor Maintenance Fee (HMF) Currency" UniqueName="HarborMaintenanceCurrency">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="" 
                        FilterControlAltText="Filter  column" 
                        HeaderText="Merchandise Processing Fee (MPF)" UniqueName="MerchandiseProcessingFee">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="" 
                        FilterControlAltText="Filter  column" 
                        HeaderText="Merchandise Processing Fee (MPF) Currency" UniqueName="Merchandise">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="" 
                        FilterControlAltText="Filter  column" 
                        HeaderText="Special Program Indicator" UniqueName="Indicator">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="" 
                        FilterControlAltText="Filter  column" 
                        HeaderText="Container Number" UniqueName="Numbrer">
                    </telerik:GridBoundColumn>--%>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <ItemStyle BorderColor="Black" BorderStyle="Solid" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        <table class="style12">
            <tr>
                <td style="text-align: center">
                     <asp:ImageButton ID="btnGuardar" runat="server" 
                         ImageUrl="~/Images/24/disk_blue_ok_24.png" 
                         OnClientClick="radconfirm('Esta seguro que Desea Generar el Reporte?',confirmCallBackSalvar, 300, 100); return false;" 
                         ToolTip="Salvar" Visible="False" />
                     <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="~/Images/gris.png" 
                         OnClick="btnSalvar_Click" />
                     <asp:Button ID="btnCargar" runat="server" Text="Cargar Archivo" 
                         Visible="false" />
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
    <%--<GroupByExpressions>
                    <telerik:GridGroupByExpression>
                        <SelectFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </SelectFields>
                        <GroupByFields>
                            <telerik:GridGroupByField FieldAlias="Proveedor" FieldName="Proveedor"></telerik:GridGroupByField>
                        </GroupByFields>
                    </telerik:GridGroupByExpression>
                </GroupByExpressions>--%>
</asp:Content>
