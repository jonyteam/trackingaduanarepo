﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="EnviarRemision.aspx.cs" Inherits="EnviarRemision" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" Width="100%" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart" Style="text-align: left">
        <telerik:RadGrid ID="rgInstrucciones" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            GridLines="None" Width="99.9%" Height="420px" CellSpacing="0" AllowPaging="True"
            OnNeedDataSource="rgInstrucciones_NeedDataSource" ShowFooter="True" ShowStatusBar="True"
            PageSize="400">
            <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
            <ClientSettings>
                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
            </ClientSettings>
            <MasterTableView>
                <CommandItemSettings ExportToPdfText="Export to PDF"></CommandItemSettings>
                <RowIndicatorColumn Visible="True" FilterControlAltText="Filter RowIndicator column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </RowIndicatorColumn>
                <ExpandCollapseColumn Visible="True" FilterControlAltText="Filter ExpandColumn column">
                    <HeaderStyle Width="20px"></HeaderStyle>
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridTemplateColumn FilterControlAltText="Filter TemplateColumn column" UniqueName="TemplateColumn">
                        <ItemTemplate>
                            <asp:CheckBox ID="chbAceptar" runat="server" />
                        </ItemTemplate>
                        <HeaderStyle Width="50px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column column" HeaderText="Instruccion No."
                        UniqueName="IdInstruccion" DataField="HojaRuta">
                        <HeaderStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Serie" FilterControlAltText="Filter Serie column"
                        HeaderText="Serie" UniqueName="Serie">
                        <HeaderStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CodEspecieFiscal" FilterControlAltText="Filter CodEspecieFiscal column"
                        HeaderText="CodigoSerie" UniqueName="CodEspecieFiscal">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column1 column" HeaderText="Cliente"
                        UniqueName="Nombre" DataField="Nombre">
                        <HeaderStyle Width="250px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column2 column" HeaderText="Factura"
                        UniqueName="NumeroFactura" DataField="NumeroFactura">
                        <HeaderStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column3 column" HeaderText="Correlativo"
                        UniqueName="NoCorrelativo" DataField="NoCorrelativo">
                        <HeaderStyle Width="120px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column4 column" HeaderText="Proveedor"
                        UniqueName="Proveedor" DataField="Proveedor">
                        <HeaderStyle Width="250px" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        <br />
        <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Images/24/disk_blue_ok_24.png"
            OnClick="btnGuardar_Click" OnClientClick="radconfirm('Esta seguro que Desea Generar el Reporte?',confirmCallBackSalvar, 300, 100); return false;"
            ToolTip="Salvar" Visible="true" />
    </telerik:RadAjaxPanel>
    <p>
    </p>
</asp:Content>
