﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="FcJefeDeAduanaEstados.aspx.cs" Inherits="FcJefeDeAduanaEstados" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="miDiv" runat="server" class="panelCentrado2">
        <table width="100%">
            <tr>
                <td>
                    <telerik:RadGrid ID="rgAsistenteOperaciones" runat="server" AllowFilteringByColumn="True"
                        AllowSorting="True" AutoGenerateColumns="False" GridLines="None" Width="100%"
                        Height="420px" OnNeedDataSource="rgAsistenteOperaciones_NeedDataSource" AllowPaging="True"
                        ShowFooter="True" ShowStatusBar="True" PageSize="20" OnInit="rgAsistenteOperaciones_Init"
                        OnItemCommand="rgAsistenteOperaciones_ItemCommand" OnItemDataBound="rgAsistenteOperaciones_ItemDataBound" OnDetailTableDataBind="rgAsistenteOperaciones_DetailTableDataBind">
                        <HeaderContextMenu EnableTheming="True">
                            <CollapseAnimation Duration="200" Type="OutQuint" />
                        </HeaderContextMenu>
                        <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                            PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                            Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
                        <MasterTableView DataKeyNames="Id,Correlativo,IdInstruccion" CommandItemDisplay="Top"
                            NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay instrucciones definidas."
                            GroupLoadMode="Client">
                            <CommandItemSettings ShowAddNewRecordButton="False" RefreshText=""
                                ShowExportToExcelButton="False" />
                            <RowIndicatorColumn>
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </RowIndicatorColumn>
                            <ExpandCollapseColumn>
                                <HeaderStyle Width="20px"></HeaderStyle>
                            </ExpandCollapseColumn>
                            <DetailTables>
                                <telerik:GridTableView DataKeyNames="IdInstruccion" DataMember="DetalleEstados"
                                    Width="100%" GridLines="Horizontal" Style="border-color: #d5b96a" CssClass="RadGrid2"
                                    NoDetailRecordsText="No hay registros." AllowFilteringByColumn="false" ShowFooter="false">
                                    <Columns>
                                        <telerik:GridBoundColumn SortExpression="IdInstruccion" HeaderText="IdInstruccion"
                                            HeaderButtonType="TextButton" DataField="IdInstruccion" Visible="False">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn SortExpression="Estado" HeaderText="Estado"
                                            HeaderButtonType="TextButton" DataField="Estado">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn SortExpression="Fecha" HeaderText="Fecha" HeaderButtonType="TextButton"
                                            DataField="Fecha">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn SortExpression="Observacion" HeaderText="Observacion" HeaderButtonType="TextButton"
                                            DataField="Observacion">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn SortExpression="Usuario" HeaderText="Usuario" HeaderButtonType="TextButton"
                                            DataField="Usuario">
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </telerik:GridTableView>
                            </DetailTables>
                            <Columns>
                                <telerik:GridBoundColumn DataField="Id" HeaderText="Id" UniqueName="Id" Visible="False"
                                    FilterControlWidth="70%">
                                    <HeaderStyle Width="90px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="IdInstruccion" HeaderText="IdInstruccion" UniqueName="Instruccion"
                                    FilterControlWidth="65%">
                                    <HeaderStyle Width="80px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Cliente" HeaderText="Cliente" UniqueName="Cliente"
                                    FilterControlWidth="80%">
                                    <HeaderStyle Width="130px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Correlativo" HeaderText="Correlativo" UniqueName="Correlativo"
                                    FilterControlWidth="75%">
                                    <HeaderStyle Width="90px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Proveedor" HeaderText="Proveedor" UniqueName="Proveedor" FilterControlWidth="70%">
                                    <HeaderStyle Width="130px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CodRegimen" HeaderText="Regimen" UniqueName="CodRegimen"
                                    FilterControlWidth="45%">
                                    <HeaderStyle Width="50px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="NoFactura" HeaderText="Factura" UniqueName="NoFactura"
                                    FilterControlWidth="60%">
                                    <HeaderStyle Width="70px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="CanalSelectividad" HeaderText="Canal de Selectividad" UniqueName="CanalSelectividad"
                                    FilterControlWidth="80%">
                                    <HeaderStyle Width="110px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Estado" HeaderText="Estado" UniqueName="Estado"
                                    FilterControlWidth="60%">
                                    <HeaderStyle Width="70px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="TiempoTranscurrido" HeaderText="Tiempo" UniqueName="TiempoTranscurrido"
                                    FilterControlWidth="60%">
                                    <HeaderStyle Width="120px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn HeaderText="Tramitador" AllowFiltering="false" ShowFilterIcon="false">
                                    <HeaderStyle Width="140px" />
                                    <ItemTemplate>
                                        <telerik:RadComboBox ID="edTramitador" runat="server" Width="92%"
                                            DataTextField="NombreCompleto" DataValueField="IdUsuario" EmptyMessage="Seleccione...">
                                        </telerik:RadComboBox>
                                        <asp:RequiredFieldValidator ID="rfvTramitador" runat="server" EnableClientScript="true"
                                            Display="Dynamic" ErrorMessage="*" ForeColor="Red" Font-Size="16pt" Font-Bold="True"
                                            ControlToValidate="edTramitador">
                                        </asp:RequiredFieldValidator>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="" AllowFiltering="false" ShowFilterIcon="false">
                                    <HeaderStyle HorizontalAlign="Center" Width="60px" />
                                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    <ItemTemplate>
                                        <telerik:RadButton ID="btnGuardar" runat="server" Text="Guardar" CommandName="Guardar">
                                        </telerik:RadButton>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="CodigoAduana" HeaderText="CodigoAduana" UniqueName="CodigoAduana"
                                    FilterControlWidth="80%" Visible="False">
                                    <HeaderStyle Width="140px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="IdTramitador" HeaderText="IdTramitador" UniqueName="IdTramitador"
                                    FilterControlWidth="80%" Visible="False">
                                    <HeaderStyle Width="140px" />
                                </telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                        <HeaderStyle Width="180px" />
                        <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                            <Selecting AllowRowSelect="True" />
                            <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                                DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                                DropHereToReorder="Suelte aquí para Re-Ordenar" />
                            <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
                        </ClientSettings>
                        <FilterMenu EnableTheming="True">
                            <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
                        </FilterMenu>
                        <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
                        <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
    </div>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="rgAsistenteOperaciones">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgAsistenteOperaciones" LoadingPanelID="LoadingPanel" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
</asp:Content>
