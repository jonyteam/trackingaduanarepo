﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using GrupoLis.Login;
using GrupoLis.Ebase;
using System.Collections.Specialized;
using System.Text;
using System.Configuration;
using System.Web.Security;

public partial class Login : System.Web.UI.Page
{
    private GrupoLis.Login.Login logApp;
    private NameValueCollection mParamethers;
    protected void Page_Load(object sender, EventArgs e)
    {
        mParamethers = ConfigurationManager.AppSettings;
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        try
        {
            conectar();
            UsuariosBO usuarios = new UsuariosBO(logApp);
            UsuariosRolesBO uRoles = new UsuariosRolesBO(logApp);
            BitacoraBO b = new BitacoraBO(logApp);

            usuarios.loadUsuarioLogin(txtUsuario.Text);
            if (usuarios.TABLA.Rows.Count > 0)
            {
                string idUsuario = usuarios.IDUSUARIO;
                if (Utilidades.PaginaBase.hashMD5(txtPassword.Text) == usuarios.PASSWORD)
                {
                    if (Request.Cookies.Get(FormsAuthentication.FormsCookieName) != null)
                    {
                        HttpCookie httpCook;
                        httpCook = Context.Request.Cookies.Get(FormsAuthentication.FormsCookieName);
                        Context.Request.Cookies.Remove(FormsAuthentication.FormsCookieName);
                        Context.Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
                    }

                    Session["IdUsuario"] = idUsuario;
                    Session["Usuario"] = usuarios.USUARIO;
                    Session["CodigoAduana"]= usuarios.CODIGOADUANA;
                    Session["Pais"] = usuarios.CODPAIS;
                    Session["Id_Solicitudes"] = "";
                    uRoles.loadRolesUsuario(idUsuario);
                    if (uRoles.TABLA.Rows.Count > 0)
                    {
                        string roles = "";
                        for (int i = 0; i < uRoles.TABLA.Rows.Count; i++)
                        {
                            roles += uRoles.TABLA.Rows[i]["Descripcion"].ToString() + ",";
                        }

                        FormsAuthenticationTicket formsAuthTicket = new FormsAuthenticationTicket(1, txtUsuario.Text, DateTime.Now, DateTime.Now.AddMinutes(35), true, roles, FormsAuthentication.FormsCookiePath);
                        string strEncryptedTicket = FormsAuthentication.Encrypt(formsAuthTicket);
                        HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, strEncryptedTicket);
                        if (formsAuthTicket.IsPersistent)
                            authCookie.Expires = formsAuthTicket.Expiration;
                        Response.Cookies.Add(authCookie);
                        Session["IdSession"] = HttpContext.Current.Request.Cookies.Get("ASP.NET_SessionId").Value;
                        //llenar la bitacora
                        b.loadBitacora("-1");
                        b.newLine();
                        b.IDUSUARIO = idUsuario;
                        b.ACCION = "Se ingresó al sistema";
                        b.IDDOCUMENTO = "";
                        b.FECHA = DateTime.Now.ToString();
                        b.commitLine();
                        b.actualizar();

                        Response.Redirect("Principal.aspx");
                    }
                    else
                    {
                        FailureText.Text = "Usuario esta inactivo en todos los roles";
                    }
                }
                else
                {
                    FailureText.Text = "Usuario o Password incorrecto!";
                }
            }
            else
                FailureText.Text = "Usuario no registrado";
        }
        catch (Exception ex)
        {
            FailureText.Text = ex.Message;
        }
        finally
        {
            desconectar();
        }

    }

    protected void conectar()
    {
        if (logApp == null)
            logApp = new GrupoLis.Login.Login();
        if (!logApp.conectado)
        {
            logApp.tipoConexion = TipoConexion.SQL_SERVER;
            logApp.SERVIDOR = mParamethers.Get("Servidor");
            logApp.DATABASE = mParamethers.Get("Database");
            logApp.USER = mParamethers.Get("User");
            logApp.PASSWD = mParamethers.Get("Password");
            logApp.conectar();
        }
    }

    protected void desconectar()
    {
        if (logApp.conectado)
            logApp.desconectar();
    }

    private void registrarMensaje(string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("alert('" + mensaje + "');");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "mensaje", sb.ToString(), false);
    }

}