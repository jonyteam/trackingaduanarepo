﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="GestionCargaConCorreo.aspx.cs" Inherits="GestionCarga" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>--%>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="requestStart">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">
            //<![CDATA[
                function requestStart(sender, args) {
                    if (args.get_eventTarget().indexOf("ExportToExcelButton") >= 0 || args.get_eventTarget().indexOf("ExportToPdfButton") >= 0 | args.get_eventTarget().indexOf("ExportToCsvButton") >= 0) {
                        args.set_enableAjax(false);
                    }
                }

                function handleScrolling(sender, args) {
                    //check if the items are scrolled to bottom and get additional items
                    if (args.get_isOnBottom()) {
                        var master = sender.get_masterTableView();
                        if (master.get_pageSize() < master.get_virtualItemCount()) {
                            //changing page size causes automatic rebind
                            master.set_pageSize(master.get_pageSize() + 20);
                        }
                    }
                }
                   //]]>   
            </script>
        </telerik:RadScriptBlock>
        <input id="edIdInstruccion" runat="server" type="hidden" />
        <input id="edIdEstadoFlujo" runat="server" type="hidden" />
        <input id="edEstadoFlujo" runat="server" type="hidden" />
        <input id="edCodPaisHojaRuta" runat="server" type="hidden" />
        <input id="edCodRegimen" runat="server" type="hidden" />
        <input id="edRegimen" runat="server" type="hidden" />
        <input id="edFecha" runat="server" type="hidden" />
        <input id="edHora" runat="server" type="hidden" />
        <input id="edObservacion" runat="server" type="hidden" />
        <input id="edCorrelativo" runat="server" type="hidden" />
        <input id="edImpuesto" runat="server" type="hidden" />
        <input id="edISV" runat="server" type="hidden" />
        <input id="edDAI" runat="server" type="hidden" />
        <input id="edSelectivo" runat="server" type="hidden" />
        <input id="edSTD" runat="server" type="hidden" />
        <input id="edColor" runat="server" type="hidden" />
        <input id="edTramitador" runat="server" type="hidden" />
        <input id="edAforador" runat="server" type="hidden"  />
        <input id="edProduccionConsumo" runat="server" type="hidden"  />
        <input id="edOtros" runat="server" type="hidden"  />
        <telerik:RadGrid ID="rgInstrucciones" runat="server" AllowFilteringByColumn="True"
            AllowSorting="True" AutoGenerateColumns="False" GridLines="None" Height="410px"
            OnNeedDataSource="rgInstrucciones_NeedDataSource" AllowPaging="True" ShowFooter="True"
            ShowStatusBar="True" PageSize="20" OnInit="rgInstrucciones_Init" OnItemCommand="rgInstrucciones_ItemCommand"
            OnItemDataBound="rgInstrucciones_ItemDataBound" CellSpacing="0">
            <PagerStyle NextPagesToolTip="Paginas Siguientes" NextPageToolTip="Página Siguiente"
                PagerTextFormat="Cambiar página: {4} &amp;nbsp;Mostrando página {0} de {1}, registros {2} a {3} de {5}."
                Mode="NextPrevAndNumeric" PrevPagesToolTip="Paginas Anteriores" PrevPageToolTip="Página Anterior" />
            <MasterTableView DataKeyNames="IdInstruccion,IdEstadoFlujo,CodPaisHojaRuta,EstadoFlujo,CodRegimen,Regimen"
                CommandItemDisplay="TopAndBottom" NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay instrucciones."
                GroupLoadMode="Client">
                <CommandItemSettings ShowAddNewRecordButton="false" RefreshText="Volver a Cargar Datos"
                    ShowExportToExcelButton="true" />
                <RowIndicatorColumn>
                    <HeaderStyle Width="20px"></HeaderStyle>
                </RowIndicatorColumn>
                <ExpandCollapseColumn>
                    <HeaderStyle Width="20px"></HeaderStyle>
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="IdInstruccion" HeaderText="Instruccion No." UniqueName="IdInstruccion"
                        FilterControlWidth="60%">
                        <HeaderStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Nombre" HeaderText="Cliente" UniqueName="Nombre"
                        FilterControlWidth="60%">
                        <HeaderStyle Width="200px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="NumeroFactura" 
                        FilterControlAltText="Filter column column" HeaderText="Numero de Factura" 
                        UniqueName="column" FilterControlWidth="125px">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ReferenciaCliente" HeaderText="Referencia Cliente"
                        UniqueName="ReferenciaCliente" FilterControlWidth="60%">
                        <HeaderStyle Width="120px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CodPaisHojaRuta" HeaderText="País" UniqueName="CodPaisHojaRuta"
                        FilterControlWidth="60%" Visible="false">
                        <HeaderStyle Width="140px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Pais" HeaderText="País" UniqueName="Pais" FilterControlWidth="60%">
                        <HeaderStyle Width="140px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CodRegimen" HeaderText="Código Régimen" UniqueName="CodRegimen"
                        FilterControlWidth="60%" Visible="false">
                        <ItemStyle Width="10%" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Regimen" HeaderText="Régimen" UniqueName="Regimen"
                        FilterControlWidth="60%">
                        <HeaderStyle Width="400px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EstadoFlujo" HeaderText="Estado Flujo Carga"
                        UniqueName="EstadoFlujo" FilterControlWidth="60%">
                        <ItemStyle Width="10%" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CodigoAduana" HeaderText="CodigoAduana" UniqueName="CodigoAduana"
                        FilterControlWidth="60%" Visible="false">
                        <ItemStyle Width="10%" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Etiqueta1" HeaderText="" UniqueName="Etiqueta1"
                        AllowFiltering="false" ShowFilterIcon="false">
                        <HeaderStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="" AllowFiltering="false" ShowFilterIcon="false">
                        <ItemTemplate>
                            <telerik:RadTextBox ID="edCorrelativo" runat="server">
                            </telerik:RadTextBox>
                            <br />
                            <telerik:RadNumericTextBox ID="edISV" Runat="server" EmptyMessage="ISV" 
                                NumberFormat-DecimalDigits="2" NumberFormat-DecimalSeparator="." 
                                NumberFormat-GroupSeparator="," Width="80px">
                            </telerik:RadNumericTextBox>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<telerik:RadNumericTextBox ID="edDAI" Runat="server" 
                                EmptyMessage="DAI" NumberFormat-DecimalDigits="2" 
                                NumberFormat-DecimalSeparator="." NumberFormat-GroupSeparator="," Width="80px">
                            </telerik:RadNumericTextBox>
                            <br />
                            <telerik:RadNumericTextBox ID="edSelectivo" Runat="server" 
                                EmptyMessage="Selectivo" NumberFormat-DecimalDigits="2" 
                                NumberFormat-DecimalSeparator="." NumberFormat-GroupSeparator="," Width="80px">
                            </telerik:RadNumericTextBox>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <telerik:RadNumericTextBox ID="edSTD" Runat="server" EmptyMessage="STD" 
                                NumberFormat-DecimalDigits="2" NumberFormat-DecimalSeparator="." 
                                NumberFormat-GroupSeparator="," Width="80px">
                            </telerik:RadNumericTextBox>
                            <br />
                            <telerik:RadNumericTextBox ID="edProduccionConsumo" Runat="server" 
                                EmptyMessage="Producción y Consumo" NumberFormat-DecimalDigits="2" 
                                NumberFormat-DecimalSeparator="." NumberFormat-GroupSeparator="," 
                                Width="80px">
                            </telerik:RadNumericTextBox>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <telerik:RadNumericTextBox ID="edOtros" Runat="server" 
                                EmptyMessage="Otros" NumberFormat-DecimalDigits="2" 
                                NumberFormat-DecimalSeparator="." NumberFormat-GroupSeparator="," Width="80px">
                            </telerik:RadNumericTextBox>
                            <br />
                            <telerik:RadComboBox ID="edColor" runat="server" Width="90%">
                            </telerik:RadComboBox>
                            <telerik:RadComboBox ID="edTramitador" runat="server" Width="90%">
                            </telerik:RadComboBox>
                        </ItemTemplate>
                        <FooterStyle Width="250px" />
                        <HeaderStyle Width="250px" />
                        <ItemStyle Width="250px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="Etiqueta2" HeaderText="" UniqueName="Etiqueta2"
                        AllowFiltering="false" ShowFilterIcon="false">
                        <HeaderStyle Width="80px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="" AllowFiltering="false" ShowFilterIcon="false">
                        <ItemTemplate>
                            <telerik:RadComboBox ID="edAforador" runat="server" Width="90%">
                            </telerik:RadComboBox>
                        </ItemTemplate>
                        <HeaderStyle Width="150px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="IdEstadoFlujo" HeaderText="IdEstadoFlujo" UniqueName="IdEstadoFlujo"
                        FilterControlWidth="60%" Visible="false">
                        <ItemStyle Width="10%" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Fecha" AllowFiltering="false" ShowFilterIcon="false">
                        <ItemTemplate>
                            <telerik:RadDatePicker ID="dtFechaSellado" runat="server" Width="50%" Culture="es-HN"
                                EnableTyping="False">
                                <Calendar ID="Calendar1" runat="server" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"
                                    UseRowHeadersAsSelectors="False">
                                </Calendar>
                                <DateInput ID="DateInput1" runat="server" DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"
                                    ReadOnly="True">
                                </DateInput><DatePopupButton HoverImageUrl="" ImageUrl="" />
                            </telerik:RadDatePicker>
                            <telerik:RadTimePicker ID="dtHoraSellado" runat="server" Width="45%" Culture="es-HN"
                                EnableTyping="True">
                                <TimeView ID="TimeView2" runat="server" CellSpacing="-1" Columns="6" Culture="es-HN"
                                    Interval="00:30:00" TimeFormat="t">
                                </TimeView>
                                <TimePopupButton HoverImageUrl="" ImageUrl="" />
                            </telerik:RadTimePicker>
                        </ItemTemplate>
                        <HeaderStyle Width="200px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Observación" AllowFiltering="false" ShowFilterIcon="false">
                        <ItemTemplate>
                            <telerik:RadTextBox ID="edObservacion" runat="server" Width="95%">
                            </telerik:RadTextBox>
                        </ItemTemplate>
                        <HeaderStyle Width="250px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridButtonColumn ButtonType="ImageButton" HeaderText="Sellar Estado" CommandName="Sellar"
                        ImageUrl="~/Images/24/lock2_24.png" UniqueName="btnSellar">
                        <ItemStyle Width="75px" />
                        <HeaderStyle Width="75px" />
                    </telerik:GridButtonColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <HeaderStyle Width="180px" />
            <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                <Selecting AllowRowSelect="True" />
                <ClientMessages PagerTooltipFormatString="Página &lt;b&gt;{0}&lt;/b&gt; de &lt;b&gt;{1}&lt;/b&gt; paginas"
                    DragToGroupOrReorder="Arrastre para Agrupar o Re-Ordenar" DragToResize="Arrastre para cambiar Tamaño"
                    DropHereToReorder="Suelte aquí para Re-Ordenar" />
                <Scrolling AllowScroll="True" UseStaticHeaders="True" SaveScrollPosition="True" />
            </ClientSettings>
            <FilterMenu EnableTheming="True">
                <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
            </FilterMenu>
            <SortingSettings SortedAscToolTip="Ascendente" SortedDescToolTip="Descendente" SortToolTip="Presione aquí para ordenar" />
            <StatusBarSettings LoadingText="Cargando, por favor espere..." ReadyText="Listo" />
        </telerik:RadGrid>
        <%--<telerik:RadGrid ID="rgInstrucciones" runat="server" AllowPaging="True" PageSize="14"
            AutoGenerateColumns="false" OnNeedDataSource="rgInstrucciones_NeedDataSource"
            OnInit="rgInstrucciones_Init" OnItemCommand="rgInstrucciones_ItemCommand" OnItemDataBound="rgInstrucciones_ItemDataBound">
            <PagerStyle Mode="NumericPages" />
            <MasterTableView TableLayout="Fixed" DataKeyNames="IdInstruccion,IdEstadoFlujo,CodPaisHojaRuta,EstadoFlujo,CodRegimen,Regimen"
                NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay instrucciones.">
                <Columns>
                    <telerik:GridBoundColumn DataField="IdInstruccion" HeaderText="Instruccion No." UniqueName="IdInstruccion"
                        FilterControlWidth="60%">
                        <HeaderStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IdTramite" HeaderText="Trámite No." UniqueName="IdTramite"
                        FilterControlWidth="60%">
                        <HeaderStyle Width="120px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Nombre" HeaderText="Cliente" UniqueName="Nombre"
                        FilterControlWidth="60%">
                        <HeaderStyle Width="200px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ReferenciaCliente" HeaderText="Referencia Cliente"
                        UniqueName="ReferenciaCliente" FilterControlWidth="60%">
                        <HeaderStyle Width="120px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CodPaisHojaRuta" HeaderText="País" UniqueName="CodPaisHojaRuta"
                        FilterControlWidth="60%" Visible="false">
                        <HeaderStyle Width="140px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Pais" HeaderText="País" UniqueName="Pais" FilterControlWidth="60%">
                        <HeaderStyle Width="140px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CodRegimen" HeaderText="Código Régimen" UniqueName="CodRegimen"
                        FilterControlWidth="60%" Visible="false">
                        <ItemStyle Width="10%" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Regimen" HeaderText="Régimen" UniqueName="Regimen"
                        FilterControlWidth="60%">
                        <HeaderStyle Width="400px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EstadoFlujo" HeaderText="Estado Flujo Carga"
                        UniqueName="EstadoFlujo" FilterControlWidth="60%">
                        <HeaderStyle Width="200px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CodigoAduana" HeaderText="CodigoAduana" UniqueName="CodigoAduana"
                        FilterControlWidth="60%" Visible="false">
                        <ItemStyle Width="10%" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Etiqueta1" HeaderText="" UniqueName="Etiqueta1"
                        AllowFiltering="false" ShowFilterIcon="false">
                        <HeaderStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="" AllowFiltering="false" ShowFilterIcon="false">
                        <ItemTemplate>
                            <telerik:RadTextBox ID="edCorrelativo" runat="server" Width="90%">
                            </telerik:RadTextBox>
                            <telerik:RadNumericTextBox ID="edImpuesto" runat="server" Width="90%" NumberFormat-DecimalDigits="2"
                                NumberFormat-DecimalSeparator="." NumberFormat-GroupSeparator=",">
                            </telerik:RadNumericTextBox>
                            <telerik:RadComboBox ID="edColor" runat="server" Width="90%">
                            </telerik:RadComboBox>
                            <telerik:RadComboBox ID="edTramitador" runat="server" Width="90%">
                            </telerik:RadComboBox>
                        </ItemTemplate>
                        <HeaderStyle Width="100px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="Etiqueta2" HeaderText="" UniqueName="Etiqueta2"
                        AllowFiltering="false" ShowFilterIcon="false">
                        <HeaderStyle Width="80px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="" AllowFiltering="false" ShowFilterIcon="false">
                        <ItemTemplate>
                            <telerik:RadComboBox ID="edAforador" runat="server" Width="90%">
                            </telerik:RadComboBox>
                        </ItemTemplate>
                        <HeaderStyle Width="150px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="IdEstadoFlujo" HeaderText="IdEstadoFlujo" UniqueName="IdEstadoFlujo"
                        FilterControlWidth="60%" Visible="false">
                        <HeaderStyle Width="80px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Fecha" AllowFiltering="false" ShowFilterIcon="false">
                        <ItemTemplate>
                            <telerik:RadDatePicker ID="dtFechaSellado" runat="server" Width="50%" Culture="es-HN"
                                EnableTyping="True">
                                <Calendar ID="Calendar1" runat="server" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"
                                    UseRowHeadersAsSelectors="False">
                                </Calendar>
                                <DateInput ID="DateInput1" runat="server" DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"
                                    ReadOnly="True">
                                </DateInput><DatePopupButton HoverImageUrl="" ImageUrl="" />
                            </telerik:RadDatePicker>
                            <telerik:RadTimePicker ID="dtHoraSellado" runat="server" Width="45%" Culture="es-HN"
                                EnableTyping="True">
                                <TimeView ID="TimeView2" runat="server" CellSpacing="-1" Columns="6" Culture="es-HN"
                                    Interval="00:30:00" TimeFormat="t">
                                </TimeView>
                                <TimePopupButton HoverImageUrl="" ImageUrl="" />
                            </telerik:RadTimePicker>
                        </ItemTemplate>
                        <HeaderStyle Width="200px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Observación" AllowFiltering="false" ShowFilterIcon="false">
                        <ItemTemplate>
                            <telerik:RadTextBox ID="edObservacion" runat="server" Width="95%">
                            </telerik:RadTextBox>
                        </ItemTemplate>
                        <HeaderStyle Width="250px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridButtonColumn ButtonType="ImageButton" HeaderText="Sellar Estado" CommandName="Sellar"
                        ImageUrl="~/Images/24/lock2_24.png" UniqueName="btnSellar">
                        <ItemStyle Width="75px" />
                        <HeaderStyle Width="75px" />
                    </telerik:GridButtonColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings>
                <Scrolling AllowScroll="true" UseStaticHeaders="true" />
                <ClientEvents OnScroll="handleScrolling" />
            </ClientSettings>
        </telerik:RadGrid>--%>
        <%--<telerik:RadGrid ID="rgInstrucciones" runat="server" AllowPaging="True" PageSize="14"
            AutoGenerateColumns="false" OnNeedDataSource="rgInstrucciones_NeedDataSource"
            OnInit="rgInstrucciones_Init" OnItemCommand="rgInstrucciones_ItemCommand" OnItemDataBound="rgInstrucciones_ItemDataBound"
            Height="410px">
            <PagerStyle Mode="NumericPages" />
            <MasterTableView TableLayout="Fixed" DataKeyNames="IdInstruccion,IdEstadoFlujo,CodPaisHojaRuta,EstadoFlujo,CodRegimen,Regimen"
                NoDetailRecordsText="No hay registros." NoMasterRecordsText="No hay instrucciones.">
                <Columns>
                    <telerik:GridBoundColumn DataField="IdInstruccion" HeaderText="Instruccion No." UniqueName="IdInstruccion"
                        FilterControlWidth="60%">
                        <HeaderStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="IdTramite" HeaderText="Trámite No." UniqueName="IdTramite"
                        FilterControlWidth="60%">
                        <HeaderStyle Width="120px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Nombre" HeaderText="Cliente" UniqueName="Nombre"
                        FilterControlWidth="60%">
                        <HeaderStyle Width="200px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ReferenciaCliente" HeaderText="Referencia Cliente"
                        UniqueName="ReferenciaCliente" FilterControlWidth="60%">
                        <HeaderStyle Width="120px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CodPaisHojaRuta" HeaderText="País" UniqueName="CodPaisHojaRuta"
                        FilterControlWidth="60%" Visible="false">
                        <HeaderStyle Width="140px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Pais" HeaderText="País" UniqueName="Pais" FilterControlWidth="60%">
                        <HeaderStyle Width="140px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CodRegimen" HeaderText="Código Régimen" UniqueName="CodRegimen"
                        FilterControlWidth="60%" Visible="false">
                        <ItemStyle Width="10%" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Regimen" HeaderText="Régimen" UniqueName="Regimen"
                        FilterControlWidth="60%">
                        <HeaderStyle Width="400px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EstadoFlujo" HeaderText="Estado Flujo Carga"
                        UniqueName="EstadoFlujo" FilterControlWidth="60%">
                        <HeaderStyle Width="200px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CodigoAduana" HeaderText="CodigoAduana" UniqueName="CodigoAduana"
                        FilterControlWidth="60%" Visible="false">
                        <ItemStyle Width="10%" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Etiqueta1" HeaderText="" UniqueName="Etiqueta1"
                        AllowFiltering="false" ShowFilterIcon="false">
                        <HeaderStyle Width="100px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="" AllowFiltering="false" ShowFilterIcon="false">
                        <ItemTemplate>
                            <telerik:RadTextBox ID="edCorrelativo" runat="server" Width="90%">
                            </telerik:RadTextBox>
                            <telerik:RadNumericTextBox ID="edImpuesto" runat="server" Width="90%" NumberFormat-DecimalDigits="2"
                                NumberFormat-DecimalSeparator="." NumberFormat-GroupSeparator=",">
                            </telerik:RadNumericTextBox>
                            <telerik:RadComboBox ID="edColor" runat="server" Width="90%">
                            </telerik:RadComboBox>
                            <telerik:RadComboBox ID="edTramitador" runat="server" Width="90%">
                            </telerik:RadComboBox>
                        </ItemTemplate>
                        <HeaderStyle Width="100px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="Etiqueta2" HeaderText="" UniqueName="Etiqueta2"
                        AllowFiltering="false" ShowFilterIcon="false">
                        <HeaderStyle Width="80px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="" AllowFiltering="false" ShowFilterIcon="false">
                        <ItemTemplate>
                            <telerik:RadComboBox ID="edAforador" runat="server" Width="90%">
                            </telerik:RadComboBox>
                        </ItemTemplate>
                        <HeaderStyle Width="150px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridBoundColumn DataField="IdEstadoFlujo" HeaderText="IdEstadoFlujo" UniqueName="IdEstadoFlujo"
                        FilterControlWidth="60%" Visible="false">
                        <ItemStyle Width="10%" />
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="Fecha" AllowFiltering="false" ShowFilterIcon="false">
                        <ItemTemplate>
                            <telerik:RadDatePicker ID="dtFechaSellado" runat="server" Width="50%" Culture="es-HN"
                                EnableTyping="False">
                                <Calendar ID="Calendar1" runat="server" UseColumnHeadersAsSelectors="False" ViewSelectorText="x"
                                    UseRowHeadersAsSelectors="False">
                                </Calendar>
                                <DateInput ID="DateInput1" runat="server" DateFormat="dd/MM/yyyy" DisplayDateFormat="dd/MM/yyyy"
                                    ReadOnly="True">
                                </DateInput><DatePopupButton HoverImageUrl="" ImageUrl="" />
                            </telerik:RadDatePicker>
                            <telerik:RadTimePicker ID="dtHoraSellado" runat="server" Width="45%" Culture="es-HN"
                                EnableTyping="True">
                                <TimeView ID="TimeView2" runat="server" CellSpacing="-1" Columns="6" Culture="es-HN"
                                    Interval="00:30:00" TimeFormat="t">
                                </TimeView>
                                <TimePopupButton HoverImageUrl="" ImageUrl="" />
                            </telerik:RadTimePicker>
                        </ItemTemplate>
                        <HeaderStyle Width="200px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Observación" AllowFiltering="false" ShowFilterIcon="false">
                        <ItemTemplate>
                            <telerik:RadTextBox ID="edObservacion" runat="server" Width="95%">
                            </telerik:RadTextBox>
                        </ItemTemplate>
                        <HeaderStyle Width="250px" />
                    </telerik:GridTemplateColumn>
                    <telerik:GridButtonColumn ButtonType="ImageButton" HeaderText="Sellar Estado" CommandName="Sellar"
                        ImageUrl="~/Images/24/lock2_24.png" UniqueName="btnSellar">
                        <ItemStyle Width="75px" />
                        <HeaderStyle Width="75px" />
                    </telerik:GridButtonColumn>
                </Columns>
            </MasterTableView>
            <ClientSettings>
                <Scrolling AllowScroll="True" EnableVirtualScrollPaging="True" UseStaticHeaders="True">
                </Scrolling>
                <ClientEvents OnScroll="handleScrolling" />
            </ClientSettings>
            <FilterMenu EnableTheming="True">
                <CollapseAnimation Type="OutQuint" Duration="200"></CollapseAnimation>
            </FilterMenu>
        </telerik:RadGrid>--%>
        <table width="100%">
            <tr>
                <td align="center">
                    <asp:ImageButton ID="btnLimpiar" runat="server" ImageUrl="~/Images/24/document_plain_24.png"
                        OnClick="btnLimpiar_Click" ToolTip="Limpiar Pantalla" />
                </td>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
</asp:Content>
