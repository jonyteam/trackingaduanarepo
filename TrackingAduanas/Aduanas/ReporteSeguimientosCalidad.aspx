﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="ReporteSeguimientosCalidad.aspx.cs" Inherits="ReporteSegumientoCalidad" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .style12 {
            width: 100%;
            height: 50px;
        }
        .auto-style1 {
            height: 48px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel"
        ClientEvents-OnRequestStart="pnlRequestStarted">
        <telerik:RadScriptBlock ID="RadScriptBlock1" runat="server">
            <script type="text/javascript">

                function pnlRequestStarted(ajaxPanel, eventArgs) {
                    try {
                        if ((eventArgs.EventTarget == "<%= btnSalvar.UniqueID %>")) {
                            eventArgs.EnableAjax = false;
                        }
                    }
                    catch (err)
                    { }
                }

                function confirmCallBackSalvar(arg) {
                    if (arg == true) {
                        var btn = document.getElementById('<%= btnSalvar.ClientID %>');
                        if (btn != null)
                            btn.click();
                    }
                }
            </script>
        </telerik:RadScriptBlock>
        <table border="1" style="width: 600px">
            <tr>
                <td style="width: 15%">
                    <asp:Label ID="lblFechaInicio" runat="server" Text="Fecha Inicio:" ForeColor="Black"></asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadDatePicker ID="dpFechaInicio" runat="server" EnableTyping="False" Width="95%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="dpFechaInicio"
                        ErrorMessage="Ingrese Fecha" ForeColor="Red" Display="Dynamic" ValidationGroup="Buscar" />
                </td>
                <td style="width: 15%">
                    <asp:Label ID="lblFechaFinal" runat="server" Text="Fecha Final:" ForeColor="Black"></asp:Label>
                </td>
                <td style="width: 22%">
                    <telerik:RadDatePicker ID="dpFechaFinal" runat="server" EnableTyping="False" Width="95%">
                    </telerik:RadDatePicker>
                    <asp:RequiredFieldValidator runat="server" ID="reqName" ControlToValidate="dpFechaFinal"
                        ErrorMessage="Ingrese Fecha" ForeColor="Red" Display="Dynamic" ValidationGroup="Buscar" />
                </td>
            </tr>
            <tr>
                <td align="center" style="text-align: left">
                    <input id="iTipoFlete" runat="server" type="hidden" />
                    <asp:Label ID="lblPais" runat="server" ForeColor="Black" Text="Pais:"></asp:Label>
                </td>
                <td align="center" style="text-align: left">
                    <telerik:RadComboBox ID="cmbPais" runat="server" Culture="es-ES" DataTextField="Descripcion" DataValueField="Codigo" Style="text-align: left" AutoPostBack="True" OnSelectedIndexChanged="cmbPais_SelectedIndexChanged">
                    </telerik:RadComboBox>
                </td>
                <td align="center" style="text-align: left">
                    <asp:Label ID="lblAduana" runat="server" ForeColor="Black" Text="Aduana:"></asp:Label>
                </td>
                <td align="center" style="text-align: left">
                    <telerik:RadComboBox ID="cmbAduana" runat="server" DataTextField="NombreAduana" DataValueField="CodigoAduana" Style="text-align: left">
                        <Items>
                            <telerik:RadComboBoxItem runat="server" Text="Cargill-Alcon" Value="8000423 and A.Division = 2 and A.CodRegimen in ('4000','5100','5600','4600','6010','4051')" />
                            <telerik:RadComboBoxItem runat="server" Text="Cargill-No Alcon" Value="8000423 and A.Division != 2 and A.CodRegimen in ('4000','4600','4051')" />
                            <telerik:RadComboBoxItem runat="server" Text="Perry" Value="1000102" />
                            <telerik:RadComboBoxItem runat="server" Text="Agribrands" Value="8400911" />
                        </Items>
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="4" style="text-align: center">
                    <asp:ImageButton ID="btnBuscar" runat="server" ImageUrl="~/Images/24/view_24.png" OnClick="btnBuscar_Click" ValidationGroup="Buscar" />
                </td>
            </tr>
        </table>
        <br />
        <telerik:RadGrid ID="grReporte" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            CellSpacing="0" GridLines="None" Width="100%"
            OnNeedDataSource="RadGrid1_NeedDataSource" PageSize="2000" BorderStyle="Solid"
            Culture="es-ES" AllowFilteringByColumn="True" AllowPaging="True">
            <ClientSettings AllowColumnsReorder="True" ReorderColumnsOnClient="True">
                <Selecting AllowRowSelect="True" />
                <Scrolling AllowScroll="True" UseStaticHeaders="True" />
            </ClientSettings>
            <ExportSettings FileName="Reporte" OpenInNewWindow="True">
                <Excel AutoFitImages="True" />
            </ExportSettings>
            <MasterTableView CommandItemDisplay="Top">
                <%-- <CommandItemTemplate>
            <asp:Image ID = "prueba" runat="server" ImageUrl ="~/Images/Prueba.jpg"
            AlternateText="Sushi Bar"
                    Width="100%"></asp:Image>
            </CommandItemTemplate>--%>
                <CommandItemSettings ShowExportToExcelButton="false" ExportToPdfText="Export to PDF"
                    ShowAddNewRecordButton="false" />
                <RowIndicatorColumn FilterControlAltText="Filter RowIndicator column" Visible="True">
                </RowIndicatorColumn>
                <ExpandCollapseColumn FilterControlAltText="Filter ExpandColumn column" Visible="True">
                </ExpandCollapseColumn>
                <Columns>
                    <telerik:GridBoundColumn DataField="IdInstruccion" FilterControlAltText="Filter IdInstruccion column" HeaderText="Id Instruccion" UniqueName="IdInstruccion" FilterControlWidth="78%">
                        <HeaderStyle Width="120px" />
                        <ItemStyle Width="120px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Cliente" FilterControlAltText="Filter Cliente column" HeaderText="Cliente" UniqueName="Cliente" FilterControlWidth="85%">
                        <HeaderStyle Width="180px" />
                        <ItemStyle Width="180px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Producto" FilterControlAltText="Filter Producto column" HeaderText="Producto" UniqueName="Producto" FilterControlWidth="85%">
                        <HeaderStyle Width="170px" />
                        <ItemStyle Width="170px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Tipocargamento" FilterControlAltText="Filter Tipocargamento column" HeaderText="Tipo Cargamento" UniqueName="Tipocargamento" FilterControlWidth="85%">
                        <HeaderStyle Width="150px" />
                        <ItemStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CodRegimen" FilterControlAltText="Filter CodRegimen column" HeaderText="Cod Regimen" UniqueName="CodRegimen" FilterControlWidth="60%">
                        <HeaderStyle Width="80px" />
                        <ItemStyle Width="80px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column column" UniqueName="EnvioDocAduana" FilterControlWidth="82%" DataField="EnvioDocAduana" HeaderText="Envio de Doc. Completo a Aduana">
                        <HeaderStyle Width="150px" />
                        <ItemStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn FilterControlAltText="Filter column1 column" UniqueName="RecepcionDocAduana" FilterControlWidth="82%" DataField="RecepcionDocAduana" HeaderText="Recepcion de Doc. Completos Aduana">
                        <HeaderStyle Width="150px" />
                        <ItemStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ArriboCarga" FilterControlAltText="Filter ArriboCarga column" HeaderText="Arribo Carga" UniqueName="ArriboCarga" FilterControlWidth="82%">
                        <HeaderStyle Width="150px" />
                        <ItemStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Manifiesto" FilterControlAltText="Filter Manifiesto column" HeaderText="Manifiesto" UniqueName="Manifiesto" FilterControlWidth="82%">
                        <HeaderStyle Width="150px" />
                        <ItemStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="DocumentosMinimos" FilterControlAltText="Filter DocumentosMinimos column" HeaderText="Documentos Minimos" UniqueName="DocumentosMinimos" FilterControlWidth="82%">
                        <HeaderStyle Width="150px" />
                        <ItemStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EnvioDocCentralizacion" FilterControlAltText="Filter column2 column" FilterControlWidth="82%" HeaderText="Envio de Doc. Competos a Centralizacion" UniqueName="EnvioDocCentralizacion">
                        <HeaderStyle Width="150px" />
                        <ItemStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="RecepcionDocCentralizacion" FilterControlAltText="Filter RecepcionDocCentralizacion column" HeaderText="Recepcion de Doc. Completos a Centralizacion" UniqueName="RecepcionDocCentralizacion">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ValidacionElectronica" FilterControlAltText="Filter ValidacionElectronica column" HeaderText="Validacion Electronica" UniqueName="ValidacionElectronica" FilterControlWidth="82%">
                        <HeaderStyle Width="150px" />
                        <ItemStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="PagoImpuesto" FilterControlAltText="Filter PagoImpuesto column" HeaderText="Pago Impuesto" UniqueName="PagoImpuesto" FilterControlWidth="82%">
                        <HeaderStyle Width="150px" />
                        <ItemStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="AsignacionCanalSelectividad" FilterControlAltText="Filter AsignacionCanalSelectividad column" HeaderText="Asignacion Canal Selectividad" UniqueName="AsignacionCanalSelectividad" FilterControlWidth="82%">
                        <HeaderStyle Width="150px" />
                        <ItemStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Color" FilterControlAltText="Filter Color column" HeaderText="Color" UniqueName="Color" FilterControlWidth="65%">
                        <HeaderStyle Width="80px" />
                        <ItemStyle Width="80px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="CitaOPC" FilterControlAltText="Filter CitaOPC column" HeaderText="Cita OPC" UniqueName="CitaOPC" FilterControlWidth="82%">
                        <HeaderStyle Width="150px" />
                        <ItemStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="RevisionMercancia" FilterControlAltText="Filter RevisionMercancia column" HeaderText="Revision Mercancia" UniqueName="RevisionMercancia" FilterControlWidth="82%">
                        <HeaderStyle Width="150px" />
                        <ItemStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EmisionPaseSalida" FilterControlAltText="Filter EmisionPaseSalida column" HeaderText="Emision Pase Salida" UniqueName="EmisionPaseSalida">
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="GatePass" FilterControlAltText="Filter GatePass column" HeaderText="Gate Pass" UniqueName="GatePass" FilterControlWidth="82%">
                        <HeaderStyle Width="150px" />
                        <ItemStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="EntregaServicio" FilterControlAltText="Filter EntregaServicio column" HeaderText="Entrega Servicio" UniqueName="EntregaServicio" FilterControlWidth="82%">
                        <HeaderStyle Width="150px" />
                        <ItemStyle Width="150px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="ObservacionGeneral" FilterControlAltText="Filter ObservacionGeneral column" HeaderText="Observacion General" UniqueName="ObservacionGeneral" FilterControlWidth="97%">
                        <HeaderStyle Width="900px" />
                        <ItemStyle Width="900px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="Gestor" FilterControlAltText="Filter Gestor column" HeaderText="Gestor" UniqueName="Gestor" FilterControlWidth="78%">
                        <HeaderStyle Width="120px" />
                        <ItemStyle Width="120px" />
                    </telerik:GridBoundColumn>
                    <telerik:GridBoundColumn DataField="TiempoTotalProceso" FilterControlAltText="Filter TiempoTotalProceso column" HeaderText="Tiempo Total Proceso" UniqueName="TiempoTotalProceso" FilterControlWidth="82%">
                        <HeaderStyle Width="100px" />
                        <ItemStyle Width="100px" />
                    </telerik:GridBoundColumn>
                </Columns>
                <EditFormSettings>
                    <EditColumn FilterControlAltText="Filter EditCommandColumn column">
                    </EditColumn>
                </EditFormSettings>
            </MasterTableView>
            <HeaderStyle BorderStyle="Solid" ForeColor="Black" />
            <FilterMenu EnableImageSprites="False">
            </FilterMenu>
        </telerik:RadGrid>
        <table class="style12">
            <tr>
                <td style="text-align: center" class="auto-style1">
                    <asp:ImageButton ID="btnGuardar" runat="server" ImageUrl="~/Images/24/disk_blue_ok_24.png"
                        OnClientClick="radconfirm('Esta seguro que Desea Generar el Reporte?',confirmCallBackSalvar, 300, 100); return false;"
                        ToolTip="Salvar" Visible="False" />
                    <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="~/Images/gris.png" OnClick="btnSalvar_Click" />
                    <asp:Button ID="btnCargar" runat="server" Text="Cargar Archivo" Visible="false" />
                </td>
            </tr>
        </table>
        <br />
        <br />
        <br />
        <br />
        <table width="100%">
            <tr align="center">
                <%--<td colspan="6" align="center">
                    <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="~/Images/gris.png"/>
                </td>--%>
            </tr>
        </table>
    </telerik:RadAjaxPanel>
    <%--<td colspan="6" align="center">
                    <asp:ImageButton ID="btnSalvar" runat="server" ImageUrl="~/Images/gris.png"/>
                </td>--%>
</asp:Content>
