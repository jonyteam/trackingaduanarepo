﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;
using ObjetosDto;
using ObjetosDto.Data;
using Telerik.Web.UI;
using GrupoLis.Login;

public partial class FcAseguramientoDeServicio : Utilidades.PaginaBase
{
    readonly AduanasDataContext _aduanasDc = new AduanasDataContext();
    private GrupoLis.Login.Login logAppAduanas;
    protected override void OnLoad(EventArgs e)
    {
        MODULO = "Aseguramiento De Servicio";
        base.OnLoad(e);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        RadWindowManager1.Windows.Clear();
        SetGridFilterMenu(rgAsistenteOperaciones.FilterMenu);
        rgAsistenteOperaciones.FilterItemStyle.Font.Size = FontUnit.XXSmall;
        rgAsistenteOperaciones.FilterMenu.Font.Size = FontUnit.XXSmall;
        if (!IsPostBack)
        {
            ((SiteMaster)Master).SetTitulo("Aseguramiento De Servicio", "Aseguramiento De Servicio");
            if (!(Anular || Ingresar || Modificar))
                redirectTo("Default.aspx");
        }

        if (SessionFileShare.SessionCreada(this))
        {
            var respuesta = SessionFileShare.GetRespuesta(this);
            foreach (var res in respuesta)
            {
                var estado = res.Estado;
                if (estado != 1) continue;
                var idSistema = res.IdSistema;
                foreach (var docResp in res.DocumentoResponseVms)
                {
                    //var documento = docResp.Documento;
                    var idDocumento = docResp.IdDocumento.ToString();
                    var noReferencia = docResp.NoReferencia;

                    var pfc = _aduanasDc.ProcesoFlujoCarga.SingleOrDefault(p => p.IdInstruccion == idSistema && p.Eliminado == false);
                    if (pfc == null) continue;
                    switch (idDocumento)
                    {
                        case "1":
                            pfc.Correlativo = noReferencia;
                            var b = new Bitacora
                            {
                                IdUsuario = Convert.ToDecimal(Session["IdUsuario"]),
                                Accion = "Boletin cargado",
                                IdDocumento = idSistema,
                                Fecha = DateTime.Now,
                            };
                            _aduanasDc.Bitacora.InsertOnSubmit(b);
                            break;
                        case "2":
                            pfc.ComprobantePago = noReferencia;
                            var b2 = new Bitacora
                            {
                                IdUsuario = Convert.ToDecimal(Session["IdUsuario"]),
                                Accion = "Comprobante de pago cargado",
                                IdDocumento = idSistema,
                                Fecha = DateTime.Now,
                            };
                            _aduanasDc.Bitacora.InsertOnSubmit(b2);
                            break;
                    }
                }
            }
            _aduanasDc.SubmitChanges();
            SessionFileShare.DestruirFileShare(this);
            Timer1.Enabled = true;
        }
    }

    private bool Anular { get { return tienePermiso("ANULAR"); } }
    private bool Ingresar { get { return tienePermiso("INGRESAR"); } }
    private bool Modificar { get { return tienePermiso("MODIFICAR"); } }

    protected void RadAjaxManager1_AjaxRequest(object sender, AjaxRequestEventArgs e)
    {
        if (e.Argument != "Rebind") return;
        rgAsistenteOperaciones.Rebind();
        Timer1.Enabled = true;
    }


    #region Aseguramiento De Servicio
    protected void rgAsistenteOperaciones_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
    {
        LlenarGrid();
    }

    private void LlenarGrid()
    {
        var regimenes = new List<string>() { "4000", "4600", "4070", "4030", "4050", "4051", "4053" };
        try
        {
            var query = _aduanasDc.ProcesoFlujoCarga.Where(p => p.Eliminado == false && p.CodEstado == "0" || p.RTClienteEnviado == false || p.RTClienteEnviado == null)
                .Join(_aduanasDc.Instrucciones.Select(i => new { i.CodEstado, i.IdInstruccion, i.CodRegimen, i.NumeroFactura, i.IdOficialCuenta, i.IdCliente })
                .Join(_aduanasDc.Codigos.Where(w => w.Categoria == "ASEGURAMIENTOS"), i => i.IdCliente, c => c.Codigo1, (i, c) => new { i = i, c = c })
                .Where(i => i.i.CodEstado != "100" && regimenes.Contains(i.i.CodRegimen) && i.c.Categoria == "ASEGURAMIENTOS"), p => p.IdInstruccion, i => i.i.IdInstruccion, (p, i) => new { p, i })
                .Select(pi => new ProcesoFlujoCargaGridVm
                {
                    Id = pi.p.Id,
                    IdInstruccion = pi.p.IdInstruccion,
                    Cliente = pi.p.Cliente,
                    Proveedor = pi.p.Proveedor,
                    CodRegimen = pi.i.i.CodRegimen,
                    NoFactura = pi.i.i.NumeroFactura,
                    Correlativo = (pi.p.Correlativo.ToString() == null) ? "" : pi.p.Correlativo.ToString(),
                    ComprobantePago = pi.p.ComprobantePago,
                    IdOficialCuenta = pi.i.i.IdOficialCuenta,
                    Digitalizado = (pi.p.Digitalizado.ToString() == null) ? "" : "1"
                })
                .ToList();

            if (User.IsInRole("Oficial de Cuenta"))
            {
                //cuando es Oficial de Cuenta el CodigoAduana es condicion
                var idUsuario = _aduanasDc.Usuarios.Where(u => u.IdUsuario == Convert.ToDecimal(Session["IdUsuario"].ToString()) && u.Eliminado == '0')
                    .Select(u => u.IdUsuario).FirstOrDefault();
                query = query.Where(q => q.IdOficialCuenta == idUsuario).ToList();
            }
            else if (!User.IsInRole("Administradores"))
            {
                query = new List<ProcesoFlujoCargaGridVm>();
            }

            rgAsistenteOperaciones.DataSource = query;
        }
        catch { }
    }

    protected void rgAsistenteOperaciones_ItemCommand(object sender, GridCommandEventArgs e)
    {
        try
        {
            var editedItem = e.Item as GridEditableItem;
            if (e.CommandName == "Subir")
            {
                if (editedItem != null)
                {
                    var idInstruccion =
                        editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdInstruccion"].ToString();
                    var fact = editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["NoFactura"];
                    var factura = "s/f";
                    if (fact != null)
                        factura = fact.ToString();

                    var documentoRequestVms = new List<DocumentoRequestVm>
                    {
                        new DocumentoRequestVm(1,"boletin"),
                        new DocumentoRequestVm(2,"comprobante de pago")
                    };

                    Timer1.Enabled = false;
                    //SessionFileShare.Create(userId: Session["IdUsuario"].ToString(), userName: User.Identity.Name, idDocumento: 1, documento: "boletin", idSistema: idInstruccion, page: this, identificador: factura);
                    SessionFileShare.Create(userId: Session["IdUsuario"].ToString(), userName: User.Identity.Name, documentoDtos: documentoRequestVms, idSistema: idInstruccion, page: this, identificador: factura);

                }
            }
            if (e.CommandName == "Descargar")
            {
                if (editedItem != null)
                {
                    var idInstruccion = editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["IdInstruccion"].ToString();
                    Timer1.Enabled = false;
                    SessionFileShare.CreateDownload(idSistema: idInstruccion, page: this);
                }
            }
            if (e.CommandName == "Enviar")
            {
                if (editedItem != null)
                {
                    Timer1.Enabled = false;
                    var id = Convert.ToDecimal(editedItem.OwnerTableView.DataKeyValues[e.Item.ItemIndex]["Id"]);
                    var fecha = (RadDatePicker)editedItem.FindControl("dtFechaSellado");
                    var hora = (RadTimePicker)editedItem.FindControl("dtHoraSellado");
                    if (fecha.SelectedDate.HasValue)
                    {
                        if (hora.SelectedDate.HasValue)
                        {
                            var query = _aduanasDc.ProcesoFlujoCarga.SingleOrDefault(p => p.Eliminado == false && p.Id == id);
                            if (query != null)
                            {
                                if (!String.IsNullOrEmpty(query.Correlativo) && !String.IsNullOrEmpty(query.ComprobantePago))
                                {
                                  //  query.CodEstado = query.IdInstruccion.Trim().Substring(1, 1) == "r" ? "2" : "1";

                                    query.CodEstado = query.IdInstruccion.Trim().Substring(1, 1) == "r" ? query.CodEstado : "1";
                                    query.RTClienteEnviado = true;
                                    var tfc = new TiemposFlujoCarga
                                    {
                                        IdInstruccion = query.IdInstruccion,
                                        IdEstado = "1",
                                        Fecha = Convert.ToDateTime(fecha.SelectedDate.Value.ToShortDateString() + " " + hora.SelectedDate.Value.ToShortTimeString()),
                                        FechaIngreso = DateTime.Now,
                                        Observacion = "Pago confirmado por aseguramiento de servicio",
                                        Eliminado = false,
                                        IdUsuario = Convert.ToDecimal(Session["IdUsuario"])
                                    };
                                    _aduanasDc.TiemposFlujoCarga.InsertOnSubmit(tfc);

                                    var b = new Bitacora
                                    {
                                        IdUsuario = Convert.ToDecimal(Session["IdUsuario"]),
                                        Accion = "Pago confirmado por aseguramiento de servicio",
                                        IdDocumento = query.IdInstruccion,
                                        Fecha = DateTime.Now,
                                    };
                                    _aduanasDc.Bitacora.InsertOnSubmit(b);

                                    _aduanasDc.SubmitChanges();
                                    RegistrarMensaje2("Registro enviado exitosamente");
                                    rgAsistenteOperaciones.Rebind();
                                    Timer1.Enabled = true;
                                }
                                else
                                {
                                    RegistrarMensaje2("Primero subir correlativo y comprobante de pago");
                                }
                            }
                            else
                            {
                                RegistrarMensaje2("Registro presenta error...favor comunique al administrador del sistema");
                            }
                        }
                        else
                            registrarMensaje("Hora no puede estar vacía");
                    }
                    else
                        registrarMensaje("Fecha no puede estar vacía");
                }
            }


            if (e.CommandName == "Agente")
            {

                string HR;
                conectarAduanas();
                EncabezadoEsquemaBO EN = new EncabezadoEsquemaBO(logAppAduanas);
                List<string> HojasRuta= new List<string>  ();    
                  foreach (GridDataItem grdItem in rgAsistenteOperaciones.Items)
                    {

                        CheckBox chkRango = (CheckBox)grdItem.FindControl("chbAgrupar");

                        if (chkRango.Checked)
                        {
                            //DATOS DE SELECCION 

                            HR = grdItem["Instruccion"].Text;
                                           
                            HojasRuta.Add(HR.Trim());

                        }}



                foreach (var item in HojasRuta)
	                {
                    EN.loadEncabezadoEsquema(item);
                   if ( EN.totalRegistros>0)

	                    {
		                    EN.IMPUESTODEI="3";
                            EN.actualizar();
	                    }

              
                     var pfc = _aduanasDc.ProcesoFlujoCarga.SingleOrDefault(p => p.IdInstruccion == item && p.Eliminado == false);
                  //   pfc.CodEstado ="-1";
                     if (item.Substring(0, 2) != "Hr")
                     {
                         pfc.CodEstado = "-1";
                         pfc.RTClienteEnviado = true;

                         _aduanasDc.SubmitChanges();
                     }
                     else
                     {
                         pfc.RTClienteEnviado = true;
                         _aduanasDc.SubmitChanges();
                     }
                   

                    var tfc = new TiemposFlujoCarga
                                    {
                                        IdInstruccion = item,
                                        IdEstado = "19",
                                        Fecha = DateTime.Now,
                                        FechaIngreso = DateTime.Now,
                                        Observacion = "Verificacion Aseguramiento de Servicio",
                                        Eliminado = false,
                        
                           IdUsuario = Convert.ToDecimal(Session["IdUsuario"])
                                    };



                       _aduanasDc.TiemposFlujoCarga.InsertOnSubmit(tfc);
                   
	                }


                _aduanasDc.SubmitChanges();
                 rgAsistenteOperaciones.Rebind();


            }

            if (rgAsistenteOperaciones.Items.Count >= 1 &
                (e.CommandName == RadGrid.ExportToExcelCommandName ||
                 e.CommandName == RadGrid.ExportToCsvCommandName))
                ConfigureExport("");
            else if (rgAsistenteOperaciones.Items.Count < 1 &
                     (e.CommandName == RadGrid.ExportToExcelCommandName ||
                      e.CommandName == RadGrid.ExportToCsvCommandName))
                e.Canceled = true;
        }
        catch
        {
        }
    }

    private void ConfigureExport(string descripcion)
    {
        System.Web.HttpPostedFileBase fileBase;
        var filename = descripcion + " " + DateTime.Now.ToShortDateString();
        rgAsistenteOperaciones.ExportSettings.FileName = filename;
        rgAsistenteOperaciones.ExportSettings.ExportOnlyData = true;
        rgAsistenteOperaciones.ExportSettings.IgnorePaging = true;
        rgAsistenteOperaciones.ExportSettings.OpenInNewWindow = true;
        rgAsistenteOperaciones.MasterTableView.ExportToExcel();
    }

    protected void rgAsistenteOperaciones_Init(object sender, EventArgs e)
    {
        var menu = rgAsistenteOperaciones.FilterMenu;
        menu.Items.RemoveAt(rgAsistenteOperaciones.FilterMenu.Items.Count - 2);
    }

    private void RegistrarMensaje2(string mensaje)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script>");
        sb.Append("alert('" + mensaje + "');");
        sb.Append("</script>");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "mensaje", sb.ToString(), false);
    }
    #endregion
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        rgAsistenteOperaciones.Rebind();
    }

    protected void btnCrearInstruccion_Click(object sender, EventArgs e)
    {
        AbrirVentanaInstruccion();
    }

    private void AbrirVentanaInstruccion()
    {
        var window1 = new RadWindow
        {
            NavigateUrl = "FcCrearInstruccionFaucaCliente.aspx",
            VisibleOnPageLoad = true,
            ID = "Instruccion",
            Width = 900,
            Height = 350,
            Animation = WindowAnimation.FlyIn,
            DestroyOnClose = true,
            VisibleStatusbar = false,
            Behaviors = WindowBehaviors.Close,
            Modal = true,
            OnClientClose = "OnClientClose"
        };
        RadWindowManager1.Windows.Add(window1);
    }


     #region Conexion
    private void conectarAduanas()
    {
        if (logAppAduanas == null)
            logAppAduanas = new GrupoLis.Login.Login();
        if (!logAppAduanas.conectado)
        {
            logAppAduanas.tipoConexion = TipoConexion.SQL_SERVER;
            logAppAduanas.SERVIDOR = mParamethers.Get("Servidor");
            logAppAduanas.DATABASE = mParamethers.Get("DatabaseEsquemaTramites2");
            logAppAduanas.USER = mParamethers.Get("User");
            logAppAduanas.PASSWD = mParamethers.Get("Password");
            logAppAduanas.conectar();
        }
    }

    private void desconectarAduanas()
    {
        if (logAppAduanas.conectado)
            logAppAduanas.desconectar();
    }
    #endregion
    protected void rgAsistenteOperaciones_ItemDataBound(object sender, GridItemEventArgs e)
    {
        if (e.Item is GridDataItem)
        {
            GridDataItem item = (GridDataItem)e.Item;

            TableCell ValorReal = item["ValorReal"];
            TableCell Proyectado = item["Monto"];

            ValorReal.Style["font-weight"] = "bold";

            if (Convert.ToDecimal(ValorReal.Text.Replace("L.", "").ToString()) < Convert.ToDecimal(Proyectado.Text.Replace("L.", "").ToString()))
                ValorReal.Style["color"] = "green";
            else if (Convert.ToDecimal(ValorReal.Text.Replace("L.", "").ToString()) > Convert.ToDecimal(Proyectado.Text.Replace("L.", "").ToString()))
                ValorReal.Style["color"] = "red";
        }
    }
}