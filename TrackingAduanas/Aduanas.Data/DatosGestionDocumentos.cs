//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aduanas.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class DatosGestionDocumentos
    {
        public decimal Item { get; set; }
        public string IdInstruccion { get; set; }
        public string NoManifiesto { get; set; }
        public string Empresa { get; set; }
        public string Motorista { get; set; }
        public string Celular { get; set; }
        public string Placa { get; set; }
        public string Furgon { get; set; }
        public string Cabezal { get; set; }
        public System.DateTime Fecha { get; set; }
        public string Eliminado { get; set; }
        public Nullable<decimal> IdUsuario { get; set; }
        public string ObservacionGeneral { get; set; }
    }
}
