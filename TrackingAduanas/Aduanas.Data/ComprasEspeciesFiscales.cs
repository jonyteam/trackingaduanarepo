//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aduanas.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class ComprasEspeciesFiscales
    {
        public decimal Item { get; set; }
        public string IdCompra { get; set; }
        public string IdEspecieFiscal { get; set; }
        public string CodPais { get; set; }
        public string CodigoAduana { get; set; }
        public string NumeroFactura { get; set; }
        public string RangoInicial { get; set; }
        public string RangoFinal { get; set; }
        public Nullable<int> Cantidad { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public string CodEstado { get; set; }
        public Nullable<decimal> IdUsuario { get; set; }
        public string TotalCompra { get; set; }
        public string Proveedor { get; set; }
        public string Carga { get; set; }
        public Nullable<System.DateTime> FechaCarga { get; set; }
    }
}
