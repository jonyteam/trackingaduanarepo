//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Aduanas.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Clientes
    {
        public decimal CodigoCliente { get; set; }
        public string CodigoSAP { get; set; }
        public string Nombre { get; set; }
        public string RTN { get; set; }
        public string Direccion { get; set; }
        public string OficialCuenta { get; set; }
        public string Contacto { get; set; }
        public string Telefono { get; set; }
        public string eMail { get; set; }
        public string CodigoPais { get; set; }
        public System.DateTime Fecha { get; set; }
        public string Eliminado { get; set; }
        public decimal IdUsuario { get; set; }
        public string CodigoPaisCliente { get; set; }
        public string Mensaje { get; set; }
        public string PermitirRemisiones { get; set; }
        public Nullable<bool> MarcadoFc { get; set; }
        public Nullable<decimal> IdOficialCuenta { get; set; }
        public Nullable<bool> PermitirFlujo { get; set; }
    }
}
